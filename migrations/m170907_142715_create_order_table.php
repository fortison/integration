<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m170907_142715_create_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('order', [
            'id' 								=> $this->primaryKey(),
			'offer_id' 							=> $this->integer(),
			'system_user_id' 					=> $this->integer(),
			'partner_status' 					=> $this->string(255),
			'comment' 							=> 'mediumtext',
			'system_status' 					=> $this->string(255),
			'request_data' 						=> 'longtext',
			'request_url' 						=> 'longtext',
			'request_timeout' 					=> $this->double(),
			'response_code' 					=> $this->integer(),
			'response_result' 					=> 'longtext',
			'response_error' 					=> 'longtext',
			'created_at' 						=> $this->integer(),
			'updated_at' 						=> $this->integer(),
			'request_at' 						=> $this->integer(),
			'processing' 						=> $this->integer(),
			'partner_order_id' 					=> $this->string(255),
			'system_order_id' 					=> $this->string(255),

			'incoming_data' 					=> 'longtext',
			'incoming_field_id' 				=> $this->string(),
			'incoming_field_campaign_id' 		=> $this->integer(),
			'incoming_field_name' 				=> $this->text(),
			'incoming_field_country' 			=> $this->string(),
			'incoming_field_phone' 				=> $this->string(),
			'incoming_field_tz' 				=> $this->integer(),
			'incoming_field_address' 			=> $this->text(),
			'incoming_field_user_comment' 		=> $this->text(),
			'incoming_field_cost' 				=> $this->double(),
			'incoming_field_cost_delivery' 		=> $this->double(),
			'incoming_field_landing_cost' 		=> $this->double(),
			'incoming_field_user_id' 			=> $this->integer(),
			'incoming_field_web_id' 			=> $this->integer(),
			'incoming_field_stream_id' 			=> $this->integer(),
			'incoming_field_product_id' 		=> $this->integer(),
			'incoming_field_offer_id' 			=> $this->integer(),
			'incoming_field_ip' 				=> $this->string(),
			'incoming_field_user_agent' 		=> $this->text(),
			'incoming_field_landing_currency'	=> $this->string(),
			'incoming_field_check_sum' 			=> $this->string(40),

			'integration_name'					=> $this->string(),
			'integration_module'				=> $this->string(),
        ]);
	
		$this->createIndex(
			'idx-order-system_order_id',
			'order',
			'system_order_id'
		);
		$this->createIndex(
			'idx-order-system_user_id',
			'order',
			'system_user_id'
		);
		$this->createIndex(
			'idx-order-integration_module',
			'order',
			'integration_module'
		);
		$this->createIndex(
			'idx-order-partner_order_id',
			'order',
			'partner_order_id'
		);
		$this->createIndex(
			'idx-order-system_user_id_and_system_order_id',
			'order',
			['system_user_id', 'partner_order_id']
		);
		$this->createIndex(
			'idx-order-system_status',
			'order',
			'system_status'
		);
		$this->createIndex(
			'idx-order-processing',
			'order',
			'processing'
		);
		$this->createIndex(
			'idx-order-updated_at',
			'order',
			'updated_at'
		);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
		$this->dropIndex(
			'idx-order-system_order_id',
			'order'
		);
		$this->dropIndex(
			'idx-order-system_user_id',
			'order'
		);
		$this->dropIndex(
			'idx-order-integration_module',
			'order'
		);
		$this->dropIndex(
			'idx-order-partner_order_id',
			'order'
		);
		$this->dropIndex(
			'idx-order-system_user_id_and_system_order_id',
			'order'
		);
		$this->dropIndex(
			'idx-order-system_status',
			'order'
		);
		$this->dropIndex(
			'idx-order-processing',
			'order'
		);
		$this->dropIndex(
			'idx-order-updated_at',
			'order'
		);
		
        $this->dropTable('order');
    }
}
