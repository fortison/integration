<?php

use yii\db\Migration;

/**
 * Handles the creation of table `error`.
 * Has foreign keys to the tables:
 *
 * - `order`
 */
class m180301_153640_create_error_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('error', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'category' => $this->string(),
            'type' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'text' => 'longtext',
            'var' => 'longtext',
            'status' => $this->integer()->defaultValue(1),
        ]);

        // creates index for column `order_id`
        $this->createIndex(
            'idx-error-order_id',
            'error',
            'order_id'
        );

        // add foreign key for table `order`
        $this->addForeignKey(
            'fk-error-order_id',
            'error',
            'order_id',
            'order',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `order`
        $this->dropForeignKey(
            'fk-error-order_id',
            'error'
        );

        // drops index for column `order_id`
        $this->dropIndex(
            'idx-error-order_id',
            'error'
        );

        $this->dropTable('error');
    }
}
