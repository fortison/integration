<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Sign in';
?>
<div class="login-box">
    <div class="login-logo">
        <b>Sign in</b>
    </div>

    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        
		<?php $form = ActiveForm::begin(); ?>

        <div class="form-group has-feedback">
            <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'class'=>'form-control'])->label(false) ?>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        
        <div class="form-group has-feedback">
			<?= $form->field($model, 'password')->passwordInput(['class'=>'form-control'])->label(false) ?>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox">
                    <label>
						<?= $form->field($model, 'rememberMe')->checkbox()?>
                    </label>
                </div>
            </div>
            <div class="col-xs-4">
				<?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
        </div>
	
		<?php ActiveForm::end(); ?>
	
    </div>
</div>
