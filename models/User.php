<?php

namespace app\models;

class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;

    private static $users = [
        '103' => [
            'id' => '103',
            'username' => 'yulia_terraleads',
            'password' => '12345678',
            'authKey' => 'sdfgrtgvsrtq',
            'accessToken' => 'wrtghytjutiyjq',
        ],
		'102' => [
			'id' => '102',
			'username' => 'yehor_terraleads',
			'password' => 'wutZGPv&^wwg6y8P',
			'authKey' => 'ssffrrgvsrtq',
			'accessToken' => 'wwtthhtjutiyjq',
		],
		'101' => [
            'id' => '101',
            'username' => 'bbc21249',
            'password' => '21249',
            'authKey' => 'qwe21249',
            'accessToken' => 'ewq21249',
        ],
        '100' => [
            'id' => '100',
            'username' => 'NagarD',
            'password' => 'VVk22',
            'authKey' => 'sdfgrtgvsrtg',
            'accessToken' => 'wrtghytjutiyjm',
        ],
        '99' => [
            'id' => '99',
            'username' => 'ramsalex',
            'password' => '777888',
            'authKey' => 'sdfgrtgvsrtfh5hrs5thg',
            'accessToken' => 'wrtghytjxfthdr56ehsdrt',
        ],
        '98' => [
            'id' => '99',
            'username' => 'web.apilead',
            'password' => 'web1234',
            'authKey' => 'sdfgrtgvsrtfh5hrs5thg',
            'accessToken' => 'thdr56ehwrghytjxfsdrtt',
        ],
        '97' => [
            'id' => '99',
            'username' => 'kosta.terraleads',
            'password' => '12345678qwe',
            'authKey' => 'h5hrs5sdfgrtgvsrtfthg',
            'accessToken' => 'tjxfsdthdr56ehwrghyrtt',
        ],
        '96' => [
            'id' => '96',
            'username' => 'gralya',
            'password' => 'zxcasdqwe1',
            'authKey' => 'tgvsrtfthgh5hrs5sdfgr',
            'accessToken' => 'ehwrghyrtttjxfsdthdr56',
        ],
		'95' => [
			'id' => '95',
			'username' => 'fortis',
			'password' => '02041997',
			'authKey' => 'tgvsrtfthgh5hrs5sdfgf',
			'accessToken' => 'ehwrghyrtttjxfsdthdr57',
		],
    ];


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
