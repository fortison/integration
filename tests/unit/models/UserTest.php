<?php
namespace tests\models;
use app\models\User;

class UserTest extends \Codeception\Test\Unit
{
    public function testFindUserById()
    {
        expect_that($user = User::findIdentity(100));
        expect($user->username)->equals('NagarD');

        expect_not(User::findIdentity(999));
    }

    public function testFindUserByAccessToken()
    {
        expect_that($user = User::findIdentityByAccessToken('wrtghytjutiyjm'));
        expect($user->username)->equals('NagarD');

        expect_not(User::findIdentityByAccessToken('non-existing'));
    }

    public function testFindUserByUsername()
    {
        expect_that($user = User::findByUsername('NagarD'));
        expect_not(User::findByUsername('not-admin'));
    }

    /**
     * @depends testFindUserByUsername
     */
    public function testValidateUser($user)
    {
        $user = User::findByUsername('NagarD');
        expect_that($user->validateAuthKey('sdfgrtgvsrtg'));
        expect_not($user->validateAuthKey('test102key'));

        expect_that($user->validatePassword('VVk22'));
        expect_not($user->validatePassword('123456'));
    }

}
