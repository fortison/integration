<?php

namespace app\modules\api\modules\terraleads\controllers;

use app\modules\apilead\common\traits\JsonTrait;
use Yii;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\web\Response;

class CparipController extends Controller
{
	use JsonTrait;
	
	const TERRA_URL = 'https://tl-api.com/api';
	
	public $incomingData;
	
	public function actionCreate()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		try {
			$incomingData 			= utf8_encode(Yii::$app->request->rawBody);
			$this->incomingData		= (object)$this->decode($incomingData, true);
			
			$data = $this->encode([
				'user_id' => $this->incomingData->user_id,
				'data'    => [
					'name' 			=> $this->incomingData->name,
					'country' 		=> $this->incomingData->country,
					'phone' 		=> $this->incomingData->phone,
					'offer_id' 		=> $this->incomingData->offer_id,
					'count' 		=> $this->incomingData->count ?? null,
					'tz' 			=> $this->incomingData->tz ?? null,
					'stream_id' 	=> $this->incomingData->stream_id ?? null,
					'address' 		=> $this->incomingData->address ?? null,
					'zip' 			=> $this->incomingData->zip ?? null,
					'region' 		=> $this->incomingData->region ?? null,
					'city' 			=> $this->incomingData->city ?? null,
					'user_agent' 	=> $this->incomingData->user_agent ?? null,
					'referer' 		=> $this->incomingData->referer ?? null,
					'utm_source' 	=> $this->incomingData->utm_source ?? null,
					'utm_medium' 	=> $this->incomingData->utm_medium ?? null,
					'utm_campaign' 	=> $this->incomingData->utm_campaign ?? null,
					'utm_term' 		=> $this->incomingData->utm_term ?? null,
					'utm_content' 	=> $this->incomingData->utm_content ?? null,
					'sub_id' 		=> $this->incomingData->sub_id ?? null,
					'sub_id_1' 		=> $this->incomingData->sub_id_1 ?? null,
					'sub_id_2' 		=> $this->incomingData->sub_id_2 ?? null,
					'sub_id_3' 		=> $this->incomingData->sub_id_3 ?? null,
					'sub_id_4' 		=> $this->incomingData->sub_id_4 ?? null,
				],
			]);
			
			$check_sum = sha1($data . $this->incomingData->api_key);
			
			$url = self::TERRA_URL . "/lead/create?check_sum={$check_sum}";
			
			$client = new Client();
			$response = $client->createRequest()
				->setMethod('POST')
				->addHeaders(['content-type' => 'application/json'])
				->setUrl($url)
				->setContent($data)
				->send();
			
			Yii::$app->response->statusCode = $response->statusCode;
			return $this->decode($response->getContent());
		} catch (\Exception $e) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			Yii::$app->response->statusCode = 500;
			return [
				'statusCode' => 500,
				'message'    => 'Connector problem',
				'error'      => $e->getMessage(),
			];
		}
	}
	
	public function beforeAction($action)
	{
		if ($action->id === 'create')
			$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}
}