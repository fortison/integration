<?php

namespace app\modules\api\modules\terraleads\controllers;

use app\modules\apilead\common\traits\JsonTrait;
use Yii;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\web\Response;

class ActionController extends Controller
{
	use JsonTrait;
	
	const TERRA_URL = 'https://tl-api.com/api';

	public function actionIndex(int $user_id, string $api_key, string $model = 'offer', string $method = 'list')
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		try {
			$data = $this->encode([
				'user_id' => $user_id,
				'data'    => [],
			]);
			$check_sum = sha1($data.$api_key);
			$url = self::TERRA_URL."/{$model}/{$method}?check_sum={$check_sum}";
			
			$client = new Client();
			$response = $client->createRequest()
				->setMethod('POST')
				->addHeaders(['content-type' => 'application/json'])
				->setUrl($url)
				->setContent($data)
				->setOptions(['timeout' => 60])
				->send();
			
			Yii::$app->response->statusCode = $response->statusCode;
			return $this->decode($response->getContent());
		} catch (\Exception $e) {
			Yii::$app->response->statusCode = 500;
			return [
				'status' => 'error',
				'error'  => $e->getMessage(),
				'data'   => null,
			];
		}
	}
	
	
	public function actionEasy(string $api_key, string $model = 'lead', string $method = 'create')
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		try {
			$data = mb_convert_encoding(Yii::$app->request->rawBody, 'UTF-8');
			$check_sum = sha1($data.$api_key);
			$url = self::TERRA_URL."/{$model}/{$method}?check_sum={$check_sum}";
			
			$client = new Client();
			$response = $client->createRequest()
				->setMethod('POST')
				->addHeaders(['content-type' => 'application/json'])
				->setUrl($url)
				->setContent($data)
				->send();
			
			Yii::$app->response->statusCode = $response->statusCode;
			return $this->decode($response->getContent());
		} catch (\Exception $e) {
			Yii::$app->response->statusCode = 500;
			return [
				'status' => 'error',
				'error'  => $e->getMessage(),
				'data'   => null,
			];
		}
	}
	
	public function actionTest(string $api_key, string $model = 'lead', string $method = 'create')
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		try {
			return mb_convert_encoding(Yii::$app->request->rawBody, 'UTF-8');
		} catch (\Exception $e) {
			Yii::$app->response->statusCode = 500;
			return [
				'status' => 'error',
				'error'  => $e->getMessage(),
			];
		}
	}
	
	public function beforeAction($action)
	{
		if ($action->id === 'easy' || $action->id == 'test') {
			$this->enableCsrfValidation = false;
		}
		
		return parent::beforeAction($action);
	}
}
