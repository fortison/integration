<?php

namespace app\modules\api\modules\terraleads\controllers;

use app\modules\apilead\common\traits\JsonTrait;
use Yii;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\web\Response;

class NeogaraController extends Controller
{
	use JsonTrait;
	
	public function actionLid()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		try {
			if (!empty($_POST)) {
				$data = $_POST;
			} else {
				$data = $_GET;
			}
			
			$apiKey = $data['apiKey'];

			$url = "https://admin.neogara.com/api/lid?apiKey={$apiKey}";
			
			$client = new Client();
			$response = $client->createRequest()
				->setMethod('POST')
				->addHeaders(['content-type' => 'application/json'])
				->setUrl($url)
				->setContent($this->encode($data))
				->send();
			
			Yii::$app->response->statusCode = $response->statusCode;
			return $this->decode($response->getContent());
		} catch (\Exception $e) {
			Yii::$app->response->statusCode = 500;
			return [
				'statusCode' => 500,
				'message'    => 'Connector problem',
				'error'      => $e->getMessage(),
			];
		}
	}
	
	public function actionStatuses()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		try {
			if (!empty($_POST)) {
				$data = $_POST;
			} else {
				$data = $_GET;
			}
			
			$url = "https://admin.neogara.com/api/statuses";
			
			$client = new Client();
			$response = $client->createRequest()
				->setMethod('GET')
				->addHeaders(['content-type' => 'application/json'])
				->setUrl($url)
				->setData($data)
				->send();
			
			Yii::$app->response->statusCode = $response->statusCode;
			return $this->decode($response->getContent());
		} catch (\Exception $e) {
			Yii::$app->response->statusCode = 500;
			return [
				'statusCode' => 500,
				'message'    => 'Connector problem',
				'error'      => $e->getMessage(),
			];
		}
	}
	
	public function actionClick()
	{
		try {
			if (!empty($_POST)) {
				$data = $_POST;
			} else {
				$data = $_GET;
			}
			
			$params = [
				'pid'      => $data['pid'],
				'offer_id' => $data['offer_id'],
				'sub1'     => $data['clickid'] ?? null,
				'sub2'     => $data['firstname'] ?? null,
				'sub3'     => $data['lastname'] ?? null,
				'sub4'     => $data['email'] ?? null,
				'sub5'     => $data['phone'] ?? null,
				'sub6'     => $data['ref'] ?? null,
				'sub7'     => $data['ip'] ?? null,
				'sub8'     => $data['country'] ?? null,
			];
			
			$url = "https://track.way2pc.com/click";
			
			$client = new Client();
			$response = $client->createRequest()
				->setMethod('GET')
				->setUrl($url)
				->setData($params)
				->send();
			
			Yii::$app->response->statusCode = $response->statusCode;
			return $response->getContent();
		} catch (\Exception $e) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			Yii::$app->response->statusCode = 500;
			return [
				'statusCode' => 500,
				'message'    => 'Connector problem',
				'error'      => $e->getMessage(),
			];
		}
	}
}
