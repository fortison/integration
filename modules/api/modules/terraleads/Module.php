<?php

namespace app\modules\api\modules\terraleads;

use Yii;

/**
 * terraleads module definition class
 */

class Module extends \yii\base\Module
{
	public $defaultRoute = 'action';
}
