## Создать лид

URL:
http://connectcpa.net/api/terraleads/action/easy?model=lead&method=create&api_key={api_key}

```
{api_key} - your api key
```

DATA:
```
{
    "user_id":<user_id>,
    "data":{
        "name":<user_name>,
        "country":<country_iso_code>,
        "phone":<user_phone>,
        "offer_id":<offer_id>
    }
}
```

## Посмотреть статус

URL:
http://connectcpa.net/api/terraleads/action/easy?model=lead&method=status&api_key={api_key}

```
{api_key} - your api key
```

DATA:
```
{
    "user_id":<user_id>,
    "data":{
        "id":<lead_id>
    }
}
```