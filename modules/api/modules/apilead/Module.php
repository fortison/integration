<?php

namespace app\modules\api\modules\apilead;

use Yii;

/**
 * apilead module definition class
 */

class Module extends \yii\base\Module
{
    public $defaultRoute = 'action';
}
