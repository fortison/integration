<?php

namespace app\modules\api\modules\apilead\controllers;

use app\common\models\OrderApilead;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\traits\JsonTrait;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\HttpException;

class ActionController extends Controller
{
	use JsonTrait;
	
    /**
     * @inheritdoc
     */
//    public function behaviors()
//    {
//        return [
//            'verbs' => [
//                'class' => VerbFilter::class,
//                'actions' => [
//                    '*' => ['GET'],
//                ],
//            ],
//			'corsFilter'  => [
//				'class' => \yii\filters\Cors::class,
//				'cors'  => [
//					// restrict access to domains:
//					'Origin'							=> ['*'],														// Від кого приймаємо запити. Масив з сайтами і їх протоколами.
//					'Access-Control-Request-Method'		=> ['GET', 'OPTION'],											// Дозводені методи
//					'Access-Control-Request-Headers'	=> ['*'],														// Дозволені заголовки
//					'Access-Control-Allow-Credentials'	=> true,
//					'Access-Control-Max-Age'			=> -1,															// Cache (seconds)
//
//				],
//			],
//        ];
//    }

	public function actionStatusView(int $systemUserId, int $systemOrderId, string $systemApiKey)
	{
		if (!is_int($systemOrderId))
    		throw new HttpException(400, "Param {systemOrderIds} must be int");
    	
    	if (!is_int($systemUserId))
    		throw new HttpException(400, "Param {systemUserId} must be int");
		
		$query = Yii::$app->db->createCommand('SELECT id FROM `order` WHERE system_order_id=:system_order_id')
			->bindValue(':system_order_id', $systemOrderId)
			->queryOne();
		
		$orderModel = OrderApilead::findOne($query['id']);
		
		if (!$orderModel) {
			return $this->encode((new ApileadStatusDataModel(true, [
				'comment' => "ERROR. Lead {{$systemOrderId}} not found for user {{$systemUserId}}",
			]))->getProperties());
		}
		
		if ($systemApiKey !== $orderModel->config->user->apiKey) {
			return $this->encode((new ApileadStatusDataModel(true, [
				'comment' => "ERROR. Wrong apiKey {{$systemApiKey}}",
			]))->getProperties());
		}
		
		if (isset($orderModel->config->user->postbackApiKey)) {
			return $this->encode((new ApileadStatusDataModel(true, [
				'comment' => "ERROR. Status works via postback",
			]))->getProperties());
		}
		
		try {
			/**@var $handler \app\modules\apilead\common\adv\handlers\base\BaseStatusHandler*/
			$handler = $orderModel->getStatusHandler();
			$responseModel = $handler->makeStatusRequest();
			$apileadStatusDataModel = $handler->getSystemStatusData($responseModel);
			
			return $this->encode($apileadStatusDataModel);
		} catch (\Exception $e) {
			return $this->encode((new ApileadStatusDataModel(true, [
				'comment' => "ERROR. {$e->getMessage()}",
			]))->getProperties());
		}
    }
	
	public function actionStatusResend(int $systemUserId, int $systemOrderId, string $systemApiKey)
	{
		
		if (!is_int($systemOrderId))
			throw new HttpException(400, "Param {systemOrderIds} must be int");
		
		if (!is_int($systemUserId))
			throw new HttpException(400, "Param {systemUserId} must be int");
		
		$query = Yii::$app->db->createCommand('SELECT id FROM `order` WHERE system_order_id=:system_order_id')
			->bindValue(':system_order_id', $systemOrderId)
			->queryOne();
		
		$orderModel = OrderApilead::findOne($query['id']);
		
		if (!$orderModel) {
			return $this->encode((new ApileadStatusDataModel(true, [
				'comment' => "ERROR. Lead {{$systemOrderId}} not found for user {{$systemUserId}}",
			]))->getProperties());
		}
		
		if ($systemApiKey !== $orderModel->config->user->apiKey) {
			return $this->encode((new ApileadStatusDataModel(true, [
				'comment' => "ERROR. Wrong apiKey {{$systemApiKey}}",
			]))->getProperties());
		}
		
		if (isset($orderModel->config->user->postbackApiKey)) {
			return $this->encode((new ApileadStatusDataModel(true, [
				'comment' => "ERROR. Status works via postback",
			]))->getProperties());
		}
		
		try {
			/**@var $handler \app\modules\apilead\common\adv\handlers\base\BaseStatusHandler */
			$handler = $orderModel->getStatusHandler();
			$apileadStatusDataModel = $handler->updateStatusInSystem(true);
			
			return $this->encode($apileadStatusDataModel);
		} catch (\Exception $e) {
			return $this->encode((new ApileadStatusDataModel(true, [
				'comment' => "ERROR. {$e->getMessage()}",
			]))->getProperties());
		}
	}
	
	public function actionStatusRansom(int $systemUserId, int $systemOrderId, string $systemApiKey)
	{
		
		if (!is_int($systemOrderId))
			throw new HttpException(400, "Param {systemOrderIds} must be int");
		
		if (!is_int($systemUserId))
			throw new HttpException(400, "Param {systemUserId} must be int");
		
		$query = Yii::$app->db->createCommand('SELECT id FROM `order` WHERE system_order_id=:system_order_id')
			->bindValue(':system_order_id', $systemOrderId)
			->queryOne();
		
		$orderModel = OrderApilead::findOne($query['id']);
		
		if (!$orderModel) {
			return $this->encode((new ApileadStatusDataModel(true, [
				'comment' => "ERROR. Lead {{$systemOrderId}} not found for user {{$systemUserId}}",
			]))->getProperties());
		}
		
		if ($systemApiKey !== $orderModel->config->user->apiKey) {
			return $this->encode((new ApileadStatusDataModel(true, [
				'comment' => "ERROR. Wrong apiKey {{$systemApiKey}}",
			]))->getProperties());
		}
		
		if (isset($orderModel->config->user->postbackApiKey)) {
			return $this->encode((new ApileadStatusDataModel(true, [
				'comment' => "ERROR. Status works via postback",
			]))->getProperties());
		}
		
		try {
			/**@var $handler \app\modules\apilead\common\adv\handlers\base\BaseStatusHandler */
			$handler = $orderModel->getStatusHandler();
			$responseModel = $handler->makeRansomStatusRequest();
			$apileadStatusDataModel = $handler->getRansomStatusData($responseModel);
			
			return $this->encode($apileadStatusDataModel);
		} catch (\Exception $e) {
			return $this->encode((new ApileadStatusDataModel(true, [
				'comment' => "ERROR. {$e->getMessage()}",
			]))->getProperties());
		}
	}

	public function actionDeleteLead(int $systemUserId, int $systemOrderId, string $systemApiKey)
	{
		if (!is_int($systemOrderId))
			throw new HttpException(400, "Param {systemOrderIds} must be int");

		if (!is_int($systemUserId))
			throw new HttpException(400, "Param {systemUserId} must be int");
		
		$query = Yii::$app->db->createCommand('SELECT id FROM `order` WHERE system_order_id=:system_order_id')
			->bindValue(':system_order_id', $systemOrderId)
			->queryOne();
		
		$orderModel = OrderApilead::findOne($query['id']);

		if (!$orderModel) {
			return $this->encode((new ApileadStatusDataModel(true, [
				'comment' => "ERROR. Lead {{$systemOrderId}} not found for user {{$systemUserId}}",
			]))->getProperties());
		}

		try {
			if ($orderModel->delete()) {
				return $this->encode(new ApileadStatusDataModel(true, ['status' => 'deleted']));
			}

			return $this->encode(new ApileadStatusDataModel(true, ['comment' => 'ERROR. Delete error']));

		} catch (\Exception $e) {
			return $this->encode((new ApileadStatusDataModel(true, [
				'comment' => "ERROR. {$e->getMessage()}",
			]))->getProperties());
		}
	}
}
