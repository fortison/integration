<?php

namespace app\modules\control\controllers;

use app\common\models\Error;
use app\common\models\ErrorSearch;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\traits\ErrorTrait;
use Yii;
use app\common\models\OrderApilead;
use app\common\models\OrderApileadSearch;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for OrderApilead model.
 */
class OrderController extends Controller
{
	use ErrorTrait;
	
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrderApilead models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderApileadSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

	public function actionProblem()
	{
		$searchModel = new OrderApileadSearch();
		$dataProvider = $searchModel->searchProblem(Yii::$app->request->queryParams);


		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionOutdate()
	{
		$searchModel = new OrderApileadSearch();
		$dataProvider = $searchModel->searchOutdate(Yii::$app->request->queryParams);


		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}
	
	

	public function actionError()
	{
		$params 		= Yii::$app->request->queryParams;
		$searchModel	= new OrderApileadSearch();

		$query 			= OrderApilead::getErrorOrdersQueryOld();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => Yii::$app->request->get('dynamicPageSize', 40),
			],
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
		]);
		
		$searchModel->load($params);
		
		if (!$searchModel->validate())
			return $dataProvider;

		$query->andFilterWhere([
			'offer_id' => $searchModel->offer_id,
			'system_user_id' => $searchModel->system_user_id,
		]);

//		$query->andFilterWhere(['between', 'created_at', $searchModel->createdTimeStart, $searchModel->createdTimeEnd]);
		$query->andFilterWhere(['between', 'updated_at', $searchModel->updatedTimeStart, $searchModel->updatedTimeEnd]);

		$query->andFilterWhere(['like', 'id', $searchModel->id])
			->andFilterWhere(['like', 'partner_status', $searchModel->partner_status])
			->andFilterWhere(['like', 'system_status', $searchModel->system_status])
			->andFilterWhere(['like', 'partner_order_id', $searchModel->partner_order_id])
			->andFilterWhere(['like', 'system_order_id', $searchModel->system_order_id])
			->andFilterWhere(['like', 'integration_name', $searchModel->integration_name]);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

    /**
     * Displays a single OrderApilead model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
    	$logModel = $this->findModel($id);
    	
		$params 		= Yii::$app->request->queryParams;
		$errorSearchModel	= new ErrorSearch();
//		$query 			= Log::find()->where(['var' => $id]);
		$query 			= $logModel->getError();
	
		$errorDataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
		]);
	
		$errorSearchModel->load($params);
	
		if (!$errorSearchModel->validate())
			return $errorDataProvider;
	
		$query->andFilterWhere([
			'created_at' => $errorSearchModel->created_at,
			'updated_at' => $errorSearchModel->updated_at,
		]);
	
		$query->andFilterWhere(['like', 'category', $errorSearchModel->category])
			->andFilterWhere(['like', 'text', $errorSearchModel->text]);
		
        return $this->render('view', [
            'model' => $logModel,
			'errorSearchModel' => $errorSearchModel,
			'errorDataProvider' => $errorDataProvider,
        ]);
    }

    /**
     * Creates a new OrderApilead model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OrderApilead();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing OrderApilead model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing OrderApilead model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	/**
	 * Перегляд стану поточного замовлення на стороні партнера
	 *
	 * @return mixed
	 */
	public function actionViewStatus()
	{
		$ids = Yii::$app->request->get('ids', []);
		
		$result = [];
		
		foreach ($ids as $id) {
			try {
				$orderModel = $this->findModel($id);
				
				if (isset($orderModel->config->user->postbackApiKey)) {
					$result[$id] = (new ApileadStatusDataModel(true, [
						'comment' => 'ERROR. Status works via postback',
					]))->getProperties();
					continue;
				}
				
				/**@var \app\modules\apilead\common\adv\handlers\base\BaseStatusHandler $handler*/
				$handler = $orderModel->getStatusHandler();
				
				$responseModel	= $handler->makeStatusRequest();
				$data = $handler->getSystemStatusData($responseModel);
				
				$result[$id] = $data->getProperties();
			} catch (\Exception $e) {
				$result[$id] = ['error' => $e->getMessage()];
			}
		}
		
		return $this->renderJson($result);
	}
	
	/**
	 * Перегляд, чи викуплено замовлення на стороні партнера
	 *
	 * @return mixed
	 */
	public function actionViewRansom()
	{
		$ids = Yii::$app->request->get('ids', []);
		
		$result = [];
		
		foreach ($ids as $id) {
			try {
				$orderModel = $this->findModel($id);
				
				if (isset($orderModel->config->user->postbackApiKey)) {
					$result[$id] = (new ApileadStatusDataModel(true, [
						'comment' => 'ERROR. Status works via postback',
					]))->getProperties();
					continue;
				}
				
				/**@var \app\modules\apilead\common\adv\handlers\base\BaseStatusHandler $handler*/
				$handler = $orderModel->getStatusHandler();
				
				$responseModel	= $handler->makeRansomStatusRequest();
				$data			= $handler->getRansomStatusData($responseModel);
				
				$result[$id] = $data->getProperties();
			} catch (\Exception $e) {
				$result[$id] = ['error' => $e->getMessage()];
			}
		}
		
		return $this->renderJson($result);
	}
	
	/**
	 * Відправити статус поточного замовлення в основну систему
	 *
	 * @return mixed
	 */
	public function actionUpdateStatus()
	{
		$ids = Yii::$app->request->get('ids', []);
		
		$result = [];
		
		foreach ($ids as $id) {
			try {
				$orderModel = $this->findModel($id);
				
				if (isset($orderModel->config->user->postbackApiKey)) {
					$result[$id] = (new ApileadStatusDataModel(true, [
						'comment' => 'ERROR. Status works via postback',
					]))->getProperties();
					continue;
				}
				
				/**@var \app\modules\apilead\common\adv\handlers\base\BaseStatusHandler $handler*/
				$handler = $orderModel->getStatusHandler();
				
				$sentData = $handler->updateStatusInSystem(true);
				
				$result[$id] = $sentData->getProperties();
			} catch (\Exception $e) {
				$result[$id] = ['error' => $e->getMessage()];
			}
		}
		
		return $this->renderJson($result);
	}
	
	/**
	 * Відправити статус поточного замовлення в основну систему
	 *
	 * @return mixed
	 */
	public function actionUpdateStatusByDate()
	{
		$from = Yii::$app->request->get('from');
		$to = Yii::$app->request->get('to');
		
		$fromTimestamp = strtotime($from . ' 00:00:00');
		$toTimestamp = strtotime($to . ' 23:59:59');
		
		$orders = OrderApilead::find()
			->innerJoin(Error::tableName(), 'error.order_id = order.id')
			->where(['error.status' => Error::STATUS_UNREAD])
			->andWhere(['>=', 'error.created_at', $fromTimestamp])
			->andWhere(['<=', 'error.created_at', $toTimestamp])
			->all();
		
		$result = [];
		
		foreach ($orders as $order) {
			try {
				$id = $order->id;
				
				$orderModel = $this->findModel($id);
				
				if (isset($orderModel->config->user->postbackApiKey)) {
					continue;
				}
				
				/**@var \app\modules\apilead\common\adv\handlers\base\BaseStatusHandler $handler*/
				$handler = $orderModel->getStatusHandler();
				
				$sentData = $handler->updateStatusInSystem(true);
			} catch (\Exception $e) {}
		}
		
		$result['status'] = 'OK';
		
		return $this->renderJson($result);
	}
	
	/**
	 * @return mixed
	 */
	public function actionUpdateStatusByAdv()
	{
		$from = Yii::$app->request->get('from');
		$to = Yii::$app->request->get('to');
		$adv = (int)Yii::$app->request->get('adv');
		
		$fromTimestamp = strtotime($from . ' 00:00:00');
		$toTimestamp = strtotime($to . ' 23:59:59');
		
		$orders = OrderApilead::find()
			->where(['system_status' => 'expect'])
			->andWhere(['>=', 'created_at', $fromTimestamp])
			->andWhere(['<=', 'created_at', $toTimestamp])
			->andWhere(['system_user_id' => $adv])
			->all();
		
		$result = [];
		
		foreach ($orders as $order) {
			try {
				$id = $order->id;
				
				$orderModel = $this->findModel($id);
				
				if (isset($orderModel->config->user->postbackApiKey)) {
					continue;
				}
				
				/**@var \app\modules\apilead\common\adv\handlers\base\BaseStatusHandler $handler*/
				$handler = $orderModel->getStatusHandler();
				
				$sentData = $handler->updateStatusInSystem(true);
			} catch (\Exception $e) {}
		}
		
		$result['status'] = 'OK';
		
		return $this->renderJson($result);
	}
	
	/**
	 * Отримує денний статусний журнал
	 *
	 * @param $id
	 * @return mixed
	 */
	public function actionViewStatusLog($id, $date)
	{
		$orderModel = $this->findModel($id);
		
		$timestamp = Yii::$app->formatter->asTimestamp($date);
		
		$file = $orderModel->config->getFullLogPath($timestamp) . 'status.log';
		
		if (is_file($file))
			return Html::encode(file_get_contents($file));
		
		return "Файлу не знайдено!";
	}

    /**
     * Finds the OrderApilead model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return OrderApilead the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrderApilead::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
