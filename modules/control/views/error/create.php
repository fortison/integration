<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\common\models\Error */

$this->title = 'Create Error';
$this->params['breadcrumbs'][] = ['label' => 'Errors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="error-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
