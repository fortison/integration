<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\common\models\Error;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\common\models\ErrorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Errors';
$this->params['breadcrumbs'][] = $this->title;

$makeReadUrl = Url::to(['make-read']);
$jsStatusUpdate = <<<JS

    function makeRead(ids)
    {
        if (ids === undefined) {
            ids = $('#grid').yiiGridView('getSelectedRows');
        }
        
        if( ids.length > 0 ){
            (function waterfall(i){
                if (i == ids.length) {
                    return false;
                }
                
                $.get( "{$makeReadUrl}", { ids: {'': ids[i]} }).done(function( data ) {
                    $("tr[data-key=" + ids[i] + "]").removeClass('bg-yellow');
                }).always(function() {
                    waterfall(i + 1);
                });
            })(0);
        }
        
        return false;
    }
JS;
$this->registerJs($jsStatusUpdate, \yii\web\View::POS_END);
?>
<div class="error-index box box-primary">
    <div class="box-header box-border">
        <div class="row">
            <div class="col-md-1">
                <a class="btn btn-app">
                    <i class="fa fa-eye" title="Прочитати" onclick="makeRead()"></i> Прочитати
                </a>
            </div>
            <div class="col-md-1 col-md-offset-10">
				<?= Html::dropDownList('dynamicPageSize', Yii::$app->request->get('dynamicPageSize', 20), [
					20 => 20,
					40 => 40,
					60 => 60,
					80 => 80,
				], [
					'form' => 'grid',
					'class' => 'form-control',
					'style' => "width: 80px; height: 60px; font-size: 19px; background-color: #f4f4f4; cursor: pointer;",
				])?>
            </div>
        </div>
    </div>
    
    <?php Pjax::begin(); ?>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'id' => 'grid',
            'dataProvider' => $dataProvider,
			'rowOptions' => function ($model) {
				if (!$model->isRead())
					return ['class' => 'bg-yellow'];
			},
            'filterModel' => $searchModel,
			'filterSelector' => 'select[name="dynamicPageSize"]',
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
				['class' => 'yii\grid\CheckboxColumn'],
	
				[
					'attribute' => 'order_id',
					'format' => 'raw',
					'value' => function($model) {
						return Html::a($model->order_id, ['order/view', 'id'=>$model->order_id], ['data-pjax'=>0]);
					}
				],
				[
					'attribute' => 'category',
					'value' => function($model) {
						preg_match('/(\w+)::/', $model->category, $matches);
						return $matches[1] ?? $model->category;
					}
				],
				[
					'attribute' => 'text',
					'value' => function($model) {
						return substr($model->text, 0, 400);
					}
				],
				[
					'attribute' => 'status',
					'format' => 'raw',
					'filter' => Error::getStatusList(),
					'value' => function($model) {
						return Error::getStatusLabel($model->status);
					}
				],
				// 'created_at',
				// 'type',
				// 'updated_at',
				// 'text:ntext',
				// 'var:ntext',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
