<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\common\models\Error */

$this->title = 'Update Error: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Errors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="error-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
