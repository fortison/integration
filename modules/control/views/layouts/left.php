<?
    use app\common\models\OrderApilead;
    use nahard\deploy\models\Deploy;
    use nahard\log\models\Log;
?>

<aside class="main-sidebar">
    <section class="sidebar">

        <!-- Sidebar user panel -->
<!--        <div class="user-panel">-->
<!--            <div class="pull-left image">-->
<!--                <img src="--><?//= $directoryAsset ?><!--/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>-->
<!--            </div>-->
<!--            <div class="pull-left info">-->
<!--                <p>Alexander Pierce</p>-->
<!---->
<!--                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->
<!--            </div>-->
<!--        </div>-->

        <!-- search form -->
<!--        <form action="#" method="get" class="sidebar-form">-->
<!--            <div class="input-group">-->
<!--                <input type="text" name="q" class="form-control" placeholder="Search..."/>-->
<!--              <span class="input-group-btn">-->
<!--                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>-->
<!--                </button>-->
<!--              </span>-->
<!--            </div>-->
<!--        </form>-->
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
					['label' => 'Замовлення', 'options' => ['class' => 'header']],
					[
						'label' => 'Всі',
						'active' => Yii::$app->controller->id == 'order' && (Yii::$app->controller->action->id == 'index' || Yii::$app->controller->action->id == 'view'),
						'url' => ['/control/order/index'],
						'icon' => 'shopping-cart',
					],
					[
						'label' => 'Затягнуті',
						'active' => Yii::$app->controller->action->id == 'outdate',
						'url' => ['/control/order/outdate '],
						'icon' => 'hourglass-end text-yellow',
						'template' => '<a href="{url}">{icon} {label} ' . OrderApilead::getOutdateOrdersCountWidget() . '</a>',
					],
					[
						'label' => 'Проблемні',
						'active' => Yii::$app->controller->action->id == 'problem',
						'url' => ['/control/order/problem'],
						'icon' => 'exclamation text-yellow text-center',
						'template' => '<a href="{url}">{icon} {label} ' . OrderApilead::getProblemOrdersCountWidget() . '</a>',
					],
					[
						'label' => 'Помилкові',
						'active' => Yii::$app->controller->action->id == 'error',
						'url' => ['/control/order/error '],
						'icon' => 'exclamation-triangle text-red',
						'template' => '<a href="{url}">{icon} {label} ' . OrderApilead::getErrorOrdersCountWidget() . '</a>',
					],
					['label' => 'Управління', 'options' => ['class' => 'header']],
					[
                        'label' => 'Розгортання',
                        'icon' => 'dropbox',
                        'encode' => false,
                        'url' => ['/deploy/deploy/index'],
                        'template' => '<a href="{url}">{icon} {label} ' . Deploy::getLastUnreadedPush() . '</a>',
                    ],
					[
                        'label' => 'Системні помилки',
                        'icon' => 'exclamation-triangle text-red',
                        'encode' => false,
                        'url' => ['/log/log/index'],
                        'template' => '<a href="{url}">{icon} {label} <span class="pull-right-container">' . Log::getFreshLogCountWidget() . '</a>',
                    ],
//					['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
//                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
//                    [
//                        'label' => 'Розробка',
//                        'icon' => 'share',
//                        'url' => '#',
//                        'items' => [
//                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
//                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
//							['label' => 'AdminLTE', 'url' => 'https://adminlte.io/themes/AdminLTE/index2.html', 'linkOptions' => ['target' => '_blank']],
//                        ],
//                    ],
                ],
            ]
        ) ?>

    </section>
</aside>
