<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\common\models\OrderApilead */

$this->title = 'Create Order Apilead';
$this->params['breadcrumbs'][] = ['label' => 'Order Apileads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-apilead-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
