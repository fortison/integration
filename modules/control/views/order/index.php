<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\common\models\OrderApilead;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\ButtonGroup;
use yii\web\View;
use edgardmessias\assets\nprogress\NProgressAsset;

NProgressAsset::register($this);

/* @var $this View */
/* @var $searchModel app\common\models\OrderApileadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Перелік замовлень';
$this->params['breadcrumbs'][] = $this->title;

$jsStatusUpdate = <<<JS

    function statusView(ids)
    {
        if (ids === undefined)
            ids = $('#grid').yiiGridView('getSelectedRows');
        
        $("#resultContainer").empty();
        
        if( ids.length > 0 ){
            (function waterfall(i){
                if (i == ids.length) {
                    return;
                }
                
                var url = "/control/order/view-status?ids[]=" + ids[i];

                $.ajax({
                    url: url,
                    type: 'get',
                    success: function(result) {
                        
                        var html = '';
                        
                        $("#modalBlockLabel").html("Результат дії");
                        
                        $.each(result, function(index, item) {
                            if ('error' in item) {
                                var errorHtml = getErrorNodeHtml(item.error);
                                
                                html += errorHtml;
                            } else {
                                var successHtml = getSuccessViewNodeHtml(item);
                               
                                html += successHtml;
                            }
                        });
                        
                        $("#resultContainer").append(html);
        
                        $('#modalBlock').modal();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        openErrorModal(xhr.responseText);
                    }
                }).always(function() {
                    waterfall(i + 1);
                });
            })(0);
        }
        
        return false;
    }

    function ransomView(ids)
    {
        if (ids === undefined)
            ids = $('#grid').yiiGridView('getSelectedRows');
        
        $("#resultContainer").empty();
        
        if( ids.length > 0 ){
            (function waterfall(i){
                if (i == ids.length) {
                    return;
                }
                
                var url = "/control/order/view-ransom?ids[]=" + ids[i];

                $.ajax({
                    url: url,
                    type: 'get',
                    success: function(result) {
                        
                        var html = '';
                        
                        $("#modalBlockLabel").html("Результат дії");
                        
                        $.each(result, function(index, item) {
                            if ('error' in item) {
                                var errorHtml = getErrorNodeHtml(item.error);
                                
                                html += errorHtml;
                            } else {
                                var successHtml = getSuccessRansomNodeHtml(item);
                               
                                html += successHtml;
                            }
                        });
                        
                        $("#resultContainer").append(html);
        
                        $('#modalBlock').modal();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        openErrorModal(xhr.responseText);
                    }
                }).always(function() {
                    waterfall(i + 1);
                });
            })(0);
        }
        
        return false;
    }
    
    function statusUpdate(ids)
    {
        if (ids === undefined)
            ids = $('#grid').yiiGridView('getSelectedRows');
        
        $("#resultContainer").empty();
        
        if( ids.length > 0 ){
            (function waterfall(i){
                if (i == ids.length) {
                    return;
                }
                
                var url = "/control/order/update-status?ids[]=" + ids[i];

                $.ajax({
                    url: url,
                    type: 'get',
                    success: function(result) {
                        
                        var html = '';
                        
                        $("#modalBlockLabel").html("Результат дії");
                        
                        $.each(result, function(index, item) {
                            if ('error' in item) {
                                var errorHtml = getErrorNodeHtml(item.error);
                                
                                html += errorHtml;
                            } else {
                                var successHtml = getSuccessUpdateNodeHtml(item);
                               
                                html += successHtml;
                            }
                        });
                        
                        $("#resultContainer").append(html);
        
                        $('#modalBlock').modal();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        openErrorModal(xhr.responseText);
                    }
                }).always(function() {
                    waterfall(i + 1);
                });
            })(0);
        }
        
        return false;
    }
    
    function getSuccessViewNodeHtml(order)
    {
        successViewNode = $('#successViewNode');
        
        html = successViewNode.html();
        html = html.replace('{systemId}', order.id);
        html = html.replace('{partnerId}', order.partnerId);
        html = html.replace('{systemStatus}', order.status);
        html = html.replace('{partnerStatus}', order.partnerStatus);
        html = html.replace('{partnerComment}', order.comment);
        
        return html;
    }
    
    function getSuccessRansomNodeHtml(order)
    {
        successRansomNode = $('#successRansomNode');
        
        html = successRansomNode.html();
        html = html.replace('{systemId}', order.id);
        html = html.replace('{partnerId}', order.partnerId);
        html = html.replace('{systemStatus}', order.status);
        html = html.replace('{partnerStatus}', order.partnerStatus);
        html = html.replace('{partnerComment}', order.comment);
        
        return html;
    }
    
    function getSuccessUpdateNodeHtml(order)
    {
        successUpdateNode = $('#successUpdateNode');
        
        html = successUpdateNode.html();
        html = html.replace('{systemId}', order.id);
        html = html.replace('{partnerId}', order.partnerId);
        html = html.replace('{systemStatus}', order.status);
        html = html.replace('{partnerStatus}', order.partnerStatus);
        html = html.replace('{partnerComment}', order.comment);
        
        return html;
    }
    
    function getErrorNodeHtml(message)
    {
        errorNode = $('#errorNode');
        
        html = errorNode.html();
        html = html.replace('{errorMessage}', message);
        
        return html;
    }
    
    function openErrorModal(message)
    {
        $("#modalBlockLabel").html("Виникла наступна помилка");
        
        var errorHtml = getErrorNodeHtml(message);
        
        $("#resultContainer").append(errorHtml);

        $('#modalBlock').modal();
    }
JS;

$this->registerJs($jsStatusUpdate, View::POS_END);

$this->registerCss("#nprogress .bar {
  background: yellow;
}");
?>

<div class="box collapsed-box">
    <div class="box-header box-border">
        <h3 class="box-title">Деталізований пошук</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                <i class="fa fa-plus"></i></button>
        </div>
    </div>
    <div class="box-body">
		<?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>
</div>

<div class="box">
    <div class="box-header box-border">
        <div class="row">
            <div class="col-md-1">
                <a class="btn btn-app">
                    <i class="fa fa-repeat" title="Оновлення статусу" onclick="statusUpdate()"></i> Оновити
                </a>
            </div>
            <div class="col-md-1">
                <a class="btn btn-app">
                    <i class="fa fa-eye" title="Поточний статус" onclick="statusView()"></i> Поточний
                </a>
            </div>
            <div class="col-md-1">
                <a class="btn btn-app">
                    <i class="fa fa-eye" title="Викуп" onclick="ransomView()"></i> Викуп
                </a>
            </div>
            <div class="col-md-1 col-md-offset-8">
				<?= Html::dropDownList('dynamicPageSize', Yii::$app->request->get('dynamicPageSize', 40), [
					20 => 20,
					40 => 40,
					60 => 60,
					80 => 80,
				], [
					'form' => 'grid',
					'class' => 'form-control',
					'style' => "width: 80px; height: 60px; font-size: 19px; background-color: #f4f4f4; cursor: pointer;",
				])?>
            </div>
        </div>
    </div>
    
    <div class="box-body">
        <?php Pjax::begin(['timeout' => 30000]); ?>
            <?= GridView::widget([
                'id' => 'grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => 'select[name="dynamicPageSize"]',
                'summary' => false,
                'columns' => [
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'checkboxOptions' => false,
                    ],
                    [
                        'attribute' => 'id',
                        'format' => 'raw',
                        'value' => function($model) {
                            $value = $model->id;
                            
                            if ($model->getError()->statusUnread()->exists())
                                $value .= '<span class="label pull-right bg-red">error</span>';
                            
                            return $value;
                        }
                    ],
                    [
                        'attribute' => 'system_order_id',
                        'label' => 'system_id',
                    ],
                    [
                        'attribute' => 'created_at',
                        'filter' => false,
                        'format' => 'date',
                        'label' => 'створено',
                    ],
                    [
                        'attribute' => 'system_status',
                        'filter' => OrderApilead::getStatusList(),
                        'label' => 'Статус',
                        'format' => 'html',
						'value' => function($model) {
							return "{$model->system_status}<br>{$model->partner_status}";
						}
                    ],
                    [
                        'attribute' => 'integration_module',
                        'filter' => OrderApilead::getLeadsList(),
                        'label' => 'integration_module',
                        'format' => 'html',
                        'value' => function($model) {
                            return "{$model->integration_module}<br>{$model->integration_name}";
                        }
                    ],
                    [
                        'attribute' => 'offer_id',
                        'label' => 'offer_id',
                    ],
                    'incoming_field_name:ntext',
                    'incoming_field_phone',
                    [
                        'attribute' => 'incoming_field_web_id',
                        'label' => 'web_id',
                    ],
                    [
                        'attribute' => 'system_user_id',
                        'label' => 'user_id',
                    ],
                    [
                        'attribute' => 'partner_order_id',
                        'label' => 'partner_order_id',
						'format' => 'html',
						'value' => function($model) {
							return wordwrap($model->partner_order_id, 20, "\n", true);
						}
                    ],
                    [
                        'attribute' => 'incoming_field_user_comment',
                        'label' => 'user_comment',
                    ],
                    'comment',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{group}',
                        'buttons' => [
                            'group' => function ($url, $model, $key) {
                                return ButtonGroup::widget([
                                    'encodeLabels' => false,
                                    'buttons' => [
                                        [
                                            'label' => Html::tag('span', '', ['class' => "glyphicon glyphicon-eye-open"]),
                                            'tagName' => 'a',
                                            'options' => [
                                                'class'=>'btn btn-default',
                                                'href' => \yii\helpers\Url::to(['view', 'id'=>$key]),
                                                'data-pjax' => 0,
                                            ]
                                        ],
                                        ButtonDropdown::widget([
                                            'split' => false,
                                            'label' => false,
                                            'dropdown' => [
                                                'items' => [
                                                    [
                                                        'label' => 'Поточний статус',
                                                        'url' => ['view-status', 'ids' => [$key]],
                                                        'linkOptions' => [
                                                            'id' => 'viewStatus',
                                                            'data' => [
                                                                'pjax' => '0',
                                                            ],
                                                            'onclick' => "return statusView([$key])",
                                                        ],
                                                    ],
                                                    [
                                                        'label' => 'Статус викупу',
                                                        'url' => ['view-status', 'ids' => [$key]],
                                                        'linkOptions' => [
                                                            'id' => 'viewStatus',
                                                            'data' => [
                                                                'pjax' => '0',
                                                            ],
                                                            'onclick' => "return ransomView([$key])",
                                                        ],
                                                    ],
                                                    [
                                                        'label' => 'Оновити статус',
                                                        'url' => ['update-status', 'ids' => [$key]],
                                                        'linkOptions' => [
                                                            'id' => 'statusUpdate',
                                                            'data' => [
                                                                'pjax' => '0',
//        														'confirm' => "Дійсно перевідправити статус замовлення {$model->system_order_id} до системи?",
                                                            ],
                                                            'onclick' => "return statusUpdate([$key])",
                                                        ],
                   
                                                    ],
//        											[
//        												'label' => \Yii::t('yii', 'View'),
//        												'url' => ['view', 'id' => $key],
//        											],
                                                    [
                                                        'label' => \Yii::t('yii', 'Update'),
                                                        'url' => ['update', 'id' => $key],
                                                        'v  isible' => true,  // if you want to hide an item based on a condition, use this
                                                    ],
        											[
        												'label' => \Yii::t('yii', 'Delete'),
        												'linkOptions' => [
        													'data' => [
        														'method' => 'post',
        														'confirm' => \Yii::t('yii', "Ви дійсно волієте видалити цей шмат лайна з ідентифікатором '{$model->system_order_id}'?"),
        													],
        												],
        												'url' => ['delete', 'id' => $key],
        												'visible' => true,   // same as above
        											],
                                                ],
                                                'options' => [
                                                    'class' => 'dropdown-menu-right', // right dropdown
                                                ],
                                            ],
                                            'options' => [
                                                'class' => 'btn-default',   // btn-success, btn-info, et cetera
                                            ],
                                        ])
                                    ]
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>

<div class="modal fade" id="modalBlock" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalBlockLabel"></h4>
            </div>
            
            <div id="resultContainer" class="modal-body">
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Зачинити</button>
<!--                <button type="button" class="btn btn-primary">Надіслати в систему</button>-->
            </div>
        </div>
    </div>
</div>

<div id="successViewNode" class="hidden">
    <table class="table table-bordered">
        <tr>
            <th></th>
            <th>Системна сторона</th>
            <th>Сторона партнера</th>
        </tr>
        <tr>
            <td>Ідентифікатор</td>
            <td><span>{systemId}</span></td>
            <td><span>{partnerId}</span></td>
        </tr>
        <tr>
            <td>Статус</td>
            <td><span>{systemStatus}</span></td>
            <td><span>{partnerStatus}</span></td>
        </tr>
        <tr>
            <td>Коментар</td>
            <td colspan="2"><span>{partnerComment}</span></td>
        </tr>
    </table>
    <hr>
</div>

<div id="successRansomNode" class="hidden">
    <table class="table table-bordered">
        <tr>
            <th></th>
            <th>Системна сторона</th>
            <th>Сторона партнера</th>
        </tr>
        <tr>
            <td>Ідентифікатор</td>
            <td><span>{systemId}</span></td>
            <td><span>{partnerId}</span></td>
        </tr>
        <tr>
            <td>Викуп</td>
            <td><span>{systemStatus}</span></td>
            <td><span>{partnerStatus}</span></td>
        </tr>
        <tr>
            <td>Коментар</td>
            <td colspan="2"><span>{partnerComment}</span></td>
        </tr>
    </table>
    <hr>
</div>

<div id="successUpdateNode" class="hidden">
    <table class="table table-bordered">
        <tr>
            <th></th>
            <th>Системна сторона</th>
            <th>Сторона партнера</th>
        </tr>
        <tr>
            <td>Ідентифікатор</td>
            <td><span>{systemId}</span></td>
            <td><span>{partnerId}</span></td>
        </tr>
        <tr>
            <td>Статус</td>
            <td><span>{systemStatus}</span></td>
            <td><span>{partnerStatus}</span></td>
        </tr>
        <tr>
            <td>Коментар</td>
            <td colspan="2"><span>{partnerComment}</span></td>
        </tr>
    </table>
    <hr>
</div>

<div id="errorNode" class="hidden">
    <p>
        <span>{errorMessage}</span>
        <hr>
    </p>
</div>