<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;
use edgardmessias\assets\nprogress\NProgressAsset;

NProgressAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\common\models\OrderApilead */

$this->registerCss("#nprogress .bar {
  background: yellow;
}");

$jsUpdateStatus = <<<JS
    function getStatusLog(id, date) {
        
        $.ajax({
            url: "/control/order/view-status-log?id="+id+"&date="+date,
            type: 'get',
            success: function(result){
                $("#statusLogContainerId").html(result);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $("#statusLogContainerId").html("Не вдалий запит!");
            }
        });
    }
JS;

$this->registerJs($jsUpdateStatus, \yii\web\View::POS_END);

$this->title = "Замовлення {$model->system_order_id}";
$this->params['breadcrumbs'][] = ['label' => 'Order Apileads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->system_order_id;
?>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_order_view" data-toggle="tab" aria-expanded="true">Замовлення</a></li>
        <li class=""><a href="#tab_order_error" data-toggle="tab" aria-expanded="false">Помилки</a></li>
        <li class=""><a href="#tab_request_log" data-toggle="tab" aria-expanded="false">Запит</a></li>
        <li class=""><a href="#tab_order_log" data-toggle="tab" aria-expanded="false">Журнал</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_order_view">
        
            <!--<h1><?//= Html::encode($this->title) ?></h1>
            <p>
                <?//= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?//= Html::a('Delete', ['delete', 'id' => $model->id], [
    //                'class' => 'btn btn-danger',
    //                'data' => [
    //                    'confirm' => 'Are you sure you want to delete this item?',
    //                    'method' => 'post',
    //                ],
    //            ]) ?>
            </p>-->
            
            <?if ($model->isStatusUnknown()):?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Невідомий статус!</h4>
                    Рекламодавець віддав нам невідомий статус. Потрібно уточнити у рекламодавця, що означає статус {<i><?=$model->partner_status?></i>}. <br>
                    Після чого внести його в файл конфігурації, що відповідає за інтеграцію <b><?=$model->integration_name?></b>
                </div>
            <?endif;?>
            <?if ($model->isProcessingFail()):?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Виникла помилка при опитуванні статусу!</h4>
                    Під час опитування статусу, виникла невідома помилка, що спричинила неможливість подальшого опитування. <br>
                    Помилка сталася більше години тому.
                </div>
            <?endif;?>
        
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'offer_id',
                    'system_user_id',
                    'system_order_id',
                    'partner_order_id',
                    'partner_status',
                    'system_status',
                    [
                        'attribute' => 'created_at',
                        'value' => function ($model) {
                            $relative = Yii::$app->formatter->asRelativeTime($model->created_at);
                            $datetime = Yii::$app->formatter->asDatetime($model->created_at);
                            return "{$datetime} ({$relative})";
                        }
                    ],
                    [
                        'attribute' => 'updated_at',
                        'value' => function ($model) {
                            $relative = Yii::$app->formatter->asRelativeTime($model->updated_at);
                            $datetime = Yii::$app->formatter->asDatetime($model->updated_at);
                            return "{$datetime} ({$relative})";
                        }
                    ],
                    [
                        'attribute' => 'request_at',
                        'value' => function ($model) {
                            if (!$model->request_at)
                                return 'Запитів не було.';
                            
                            $relative = Yii::$app->formatter->asRelativeTime($model->request_at);
                            $datetime = Yii::$app->formatter->asDatetime($model->request_at);
                            return "{$datetime} ({$relative})";
                        }
                    ],
                    'processing',
                    'comment:ntext',
                    'integration_name',
                    'integration_module',
    //                'request_data:ntext',
    //                [
    //                    'attribute' => 'request_data',
    //                    'format' => 'raw',
    //                    'value' => function($model) {
    //                        return "<span style='white-space: pre'>" . $model->request_data . "</span>";
    //                    }
    //                ],
    //                'request_url:ntext',
    //                'request_timeout',
    //                'response_code',
    //                'response_result:ntext',
    //                'response_error:ntext',
    //                'incoming_data:ntext',
    //                [
    //                    'attribute' => 'incoming_data',
    //                    'format' => 'raw',
    //                    'value' => function($model) {
    //                        return "<span style='white-space: pre'>" . $model->incoming_data . "</span>";
    //                    }
    //                ],
    //                'incoming_field_id',
    //                'incoming_field_campaign_id',
    //                'incoming_field_name:ntext',
    //                'incoming_field_country',
    //                'incoming_field_phone',
    //                'incoming_field_tz',
    //                'incoming_field_address:ntext',
    //                'incoming_field_user_comment:ntext',
    //                'incoming_field_cost',
    //                'incoming_field_cost_delivery',
    //                'incoming_field_landing_cost',
    //                'incoming_field_user_id',
    //                'incoming_field_web_id',
    //                'incoming_field_stream_id',
    //                'incoming_field_product_id',
    //                'incoming_field_offer_id',
    //                'incoming_field_ip',
    //                'incoming_field_user_agent:ntext',
    //                'incoming_field_landing_currency',
    //                'incoming_field_check_sum',
                ],
            ]) ?>
        </div>
        <div class="tab-pane" id="tab_order_error">
			<?Pjax::begin()?>
			<?= GridView::widget([
				'dataProvider' => $errorDataProvider,
				'rowOptions' => function ($model) {
					if (!$model->isRead())
						return ['class' => 'bg-yellow'];
				},
				'filterModel' => $errorSearchModel,
				'columns' => [
					['class' => 'yii\grid\SerialColumn'],
			
					'category',
					[
						'attribute' => 'text',
						'format' => 'ntext',
						'contentOptions' => ['style' => 'word-break:break-all'],
						'value' => function($model) {
							return substr($model->text, 0, 400);
						}
					],
					'created_at:datetime',
//            'id',
//            'level',
//            'updated_at:datetime',
//            'ip',
//            'var:ntext',
//            'referrer_url:ntext',
//            'request_url:ntext',
//            'status',
			
					[
                        'class' => 'yii\grid\ActionColumn',
						'template' => '{view}',
						'buttons' => [
							'view' => function ($model, $key, $index) {
								$icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-eye-open"]);
								$title = 'Переглянути';
								return Html::a($icon, ['error/view', 'id'=>$index], [
									'title' => $title,
									'aria-label' => $title,
                                    'data-pjax' => '0',
								]);
                            }
                        ],
                    ],
				],
			]); ?>
			<?Pjax::end()?>
        </div>
        <div class="tab-pane" id="tab_request_log">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <p>Надіслано:</p>
                        <p><?=Html::encode($model->request_url)?></p>
                        <p><pre><?=Html::encode($model->request_data)?></pre></p>
                    </div>
                    <div class="col-sm-6">
                        <p>Отримано:</p>
                        <br>
                        <p><pre><?=Html::encode($model->response_result)?></pre></p>
                        <p>HTTP code = <?=Html::encode($model->response_code)?></p>
                        <p>request time = <?=round(Html::encode($model->request_timeout), 2)?> сек.</p>
						<?if ($model->response_error):?>
                            <p><?=Html::encode($model->response_error)?></p>
						<?endif;?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <a href="#spoiler" data-toggle="collapse" class="btn btn-primary">Дані від Apilead</a>

                        <div class="collapse" id="spoiler">
                            <div>
                                <p><pre><?=Html::encode($model->incoming_data)?></pre></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="tab_order_log">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_order_log_bridge" data-toggle="tab" aria-expanded="true">Міст</a></li>
                    <li class=""><a href="#tab_order_log_status" data-toggle="tab" aria-expanded="false">Статус</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_order_log_bridge">
                        <pre>
<?= Html::encode(@file_get_contents($model->config->getFullLogPath($model->created_at) . 'bridge.log'))?>
                        </pre>
                    </div>
                    <div class="tab-pane" id="tab_order_log_status">
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-0">
                                <div class="input-group drp-container">
                                    <?=DateRangePicker::widget([
                                        'name'=>'date_log_status',
										'convertFormat'=>true,
                                        'value'=> Yii::$app->formatter->asDate('now'),
                                        'useWithAddon'=>true,
                                        'pluginOptions'=>[
											'locale'=>['format' => 'd.m.Y'],
                                            'singleDatePicker'=>true,
                                            'showDropdowns'=>true
                                        ],
										'pluginEvents' => [
//                                            'change' => "ajaxGetStatusLog(" . Yii::$app->formatter->asDate('now') . ")",
//                                            'change' => "console.log($(\"input[name=date_log_status]\").val())",
//                                            'change' => "console.log({$model->id})",
                                            'apply.daterangepicker' => "function() { getStatusLog({$model->id}, $(\"input[name = date_log_status]\").val()) }",
                                        ],
                                    ]);?>
                                    <span class="input-group-addon">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                    </span>
                                </div>
                            </div>

                            <div class="col-sm-5">
                                <span>Починаючи з <?=Yii::$app->formatter->asDate($model->created_at)?> та закінчуючи <?=Yii::$app->formatter->asDatetime($model->updated_at)?></span>
                            </div>
                        </div>
                        
                        <br>
                        
                        <pre id="statusLogContainerId"></pre>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
