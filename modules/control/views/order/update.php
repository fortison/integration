<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\common\models\OrderApilead */

$this->title = 'Update Order Apilead: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Order Apileads', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="order-apilead-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
