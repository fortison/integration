<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\common\models\OrderApilead;

/* @var $this yii\web\View */
/* @var $model app\common\models\OrderApilead */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-apilead-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'offer_id')->textInput() ?>

    <?= $form->field($model, 'system_user_id')->textInput() ?>

    <?= $form->field($model, 'partner_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'system_status')->dropDownList(OrderApilead::getStatusList()) ?>

    <?= $form->field($model, 'request_data')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'request_url')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'request_timeout')->textInput() ?>

    <?= $form->field($model, 'response_code')->textInput() ?>

    <?= $form->field($model, 'response_result')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'response_error')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>
    
    <?= $form->field($model, 'request_at')->textInput() ?>

    <?= $form->field($model, 'processing')->textInput() ?>

    <?= $form->field($model, 'partner_order_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'system_order_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'incoming_data')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'incoming_field_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'incoming_field_campaign_id')->textInput() ?>

    <?= $form->field($model, 'incoming_field_name')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'incoming_field_country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'incoming_field_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'incoming_field_tz')->textInput() ?>

    <?= $form->field($model, 'incoming_field_address')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'incoming_field_user_comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'incoming_field_cost')->textInput() ?>

    <?= $form->field($model, 'incoming_field_cost_delivery')->textInput() ?>

    <?= $form->field($model, 'incoming_field_landing_cost')->textInput() ?>

    <?= $form->field($model, 'incoming_field_user_id')->textInput() ?>

    <?= $form->field($model, 'incoming_field_web_id')->textInput() ?>

    <?= $form->field($model, 'incoming_field_stream_id')->textInput() ?>

    <?= $form->field($model, 'incoming_field_product_id')->textInput() ?>

    <?= $form->field($model, 'incoming_field_offer_id')->textInput() ?>

    <?= $form->field($model, 'incoming_field_ip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'incoming_field_user_agent')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'incoming_field_landing_currency')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'incoming_field_check_sum')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'integration_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
