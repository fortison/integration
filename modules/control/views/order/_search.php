<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\common\models\OrderApilead;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model app\common\models\OrderApileadSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-apilead-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-sm-6">
			<?= $form->field($model, 'system_user_id') ?>
			<?= $form->field($model, 'offer_id') ?>
            <?= $form->field($model, 'incoming_field_web_id') ?>
        </div>
        <div class="col-sm-6">
			<?= $form->field($model, 'partner_status') ?>
			<?= $form->field($model, 'system_status')->dropDownList(OrderApilead::getStatusList(), ['prompt'=>'']) ?>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-6">
			<?= $form->field($model, 'integration_name') ?>
            <br>
            <div class="form-group">
				<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
				<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>
        </div>
        <div class="col-sm-6">
			<?//= $form->field($model, 'created_at') ?>
            <div class="form-group field-orderapileadsearch-created_at">
                <label class="control-label" for="orderapileadsearch-created_at">Created At</label>
				<?= DateRangePicker::widget([
					'id' => 'orderapileadsearch-created_at',
					'attribute' => 'createdTimeRange',
					'model' => $model,
					'convertFormat'=>true,
					'pluginOptions'=>[
//						'timePicker'=>true,
						'locale'=>[
							'format'=>'d-m-Y',
						],
						'format'=>'DD-MM-YYYY',
						'opens'=>'left',
					]
				])?>
            </div>
			<?//= $form->field($model, 'updated_at') ?>
            <div class="form-group field-orderapileadsearch-updated_at">
                <label class="control-label" for="orderapileadsearch-updated_at">Updated At</label>
				<?= DateRangePicker::widget([
                    'id' => 'orderapileadsearch-updated_at',
					'attribute' => 'updatedTimeRange',
					'model' => $model,
					'convertFormat'=>true,
					'pluginOptions'=>[
						'locale'=>[
							'format'=>'d-m-Y',
						],
						'format'=>'DD-MM-YYYY',
						'opens'=>'left',
					]
				])?>
            </div>
        </div>
    </div>

    <?//= $form->field($model, 'request_data') ?>

    <?//= $form->field($model, 'request_url') ?>

    <?//= $form->field($model, 'request_timeout') ?>

    <?//= $form->field($model, 'response_code') ?>

    <?//= $form->field($model, 'response_result') ?>

    <?//= $form->field($model, 'response_error') ?>

    <?//= $form->field($model, 'partner_order_id') ?>

    <?//= $form->field($model, 'system_order_id') ?>

    <?//= $form->field($model, 'incoming_data') ?>

    <?//= $form->field($model, 'incoming_field_id') ?>

    <?//= $form->field($model, 'incoming_field_campaign_id') ?>

    <?//= $form->field($model, 'incoming_field_name') ?>

    <?//= $form->field($model, 'incoming_field_country') ?>

    <?//= $form->field($model, 'incoming_field_phone') ?>

    <?//= $form->field($model, 'incoming_field_tz') ?>

    <?//= $form->field($model, 'incoming_field_address') ?>

    <?//= $form->field($model, 'incoming_field_user_comment') ?>

    <?//= $form->field($model, 'incoming_field_cost') ?>

    <?//= $form->field($model, 'incoming_field_cost_delivery') ?>

    <?//= $form->field($model, 'incoming_field_landing_cost') ?>

    <?//= $form->field($model, 'incoming_field_user_id') ?>

    <?//= $form->field($model, 'incoming_field_web_id') ?>

    <?//= $form->field($model, 'incoming_field_stream_id') ?>

    <?//= $form->field($model, 'incoming_field_product_id') ?>

    <?//= $form->field($model, 'incoming_field_offer_id') ?>

    <?//= $form->field($model, 'incoming_field_ip') ?>

    <?//= $form->field($model, 'incoming_field_user_agent') ?>

    <?//= $form->field($model, 'incoming_field_landing_currency') ?>

    <?//= $form->field($model, 'incoming_field_check_sum') ?>

    <?php ActiveForm::end(); ?>

</div>
