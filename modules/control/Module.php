<?php

namespace app\modules\control;

use Yii;

/**
 * apilead module definition class
 */

class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\control\controllers';

    public $defaultRoute = 'order';
    public $layout = 'main';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
		
        Yii::$app->language = 'uk';
        // custom initialization code goes here
    }
}
