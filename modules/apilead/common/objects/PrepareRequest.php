<?php
/**
 * Created by PhpStorm.
 * User: nagard
 * Date: 09.02.18
 * Time: 15:12
 */

namespace app\modules\apilead\common\objects;

use yii\base\BaseObject;

class PrepareRequest extends BaseObject
{
	public $requestUrl;
	public $requestData;
	public $requestContent;
	public $requestMethod;
	public $requestFormat;
	public $requestHeaders;
	public $requestCookies;
	
	public $requestTimeout;
	
	public $responseFormat;
	
	public function setRequestUrl($url)
	{
		$this->requestUrl = $url;
	}
	
	public function setRequestData($data)
	{
		$this->requestData = $data;
	}
	
	public function setRequestContent($content)
	{
		$this->requestContent = $content;
	}
	
	public function setRequestMethod($method)
	{
		$this->requestMethod = $method;
	}
	
	public function setRequestFormat($format)
	{
		$this->requestFormat = $format;
	}
	
	public function setRequestHeaders($headers)
	{
		$this->requestHeaders = $headers;
	}
	
	public function setRequestCookies($cookies)
	{
		$this->requestCookies = $cookies;
	}
	
	public function setRequestTimeout($timeout)
	{
		$this->requestTimeout = $timeout;
	}
	
	public function getRequestUrl()
	{
		return $this->requestUrl;
	}
	
	public function getRequestData()
	{
		return $this->requestData;
	}
	
	public function getRequestContent()
	{
		return $this->requestContent;
	}
	
	public function getRequestMethod()
	{
		return $this->requestMethod;
	}
	
	public function getRequestFormat()
	{
		return $this->requestFormat;
	}
	
	public function getRequestHeaders()
	{
		return $this->requestHeaders;
	}
	
	public function getRequestCookies()
	{
		return $this->requestCookies;
	}
	
	public function getRequestTimeout()
	{
		return $this->requestTimeout;
	}
	
	# Response message settings

	public function setResponseFormat($format)
	{
		$this->responseFormat = $format;
	}

	public function getResponseFormat()
	{
		return $this->responseFormat;
	}
	
	/**
	 * Повертає попередні дані запиту у вигляді масиву для простішого подальшого викоритання через [], чи list.
	 *
	 * @return array
	 */
	public function getRequestAsArray()
	{
		$requestUrl			= $this->getRequestUrl();
		$requestData		= $this->getRequestData();
		$requestMethod		= $this->getRequestMethod();
		$requestFormat		= $this->getRequestFormat();
		$requestHeaders		= $this->getRequestHeaders();
		$requestCookies		= $this->getRequestCookies();
		$responseFormat		= $this->getResponseFormat();
		
		return [
			$requestUrl,
			$requestData,
			$requestMethod,
			$requestFormat,
			$requestHeaders,
			$requestCookies,
			$responseFormat,
		];
	}
}