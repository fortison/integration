<?php
/**
 * Created by PhpStorm.
 * User: nagard
 * Date: 09.02.18
 * Time: 15:12
 */

namespace app\modules\apilead\common\objects;

use yii\httpclient\Client;

class PreparePostRequestJson extends PreparePostRequest
{
	public function init()
	{
		parent::init();
		
		$this->requestFormat = Client::FORMAT_JSON;
	}
}