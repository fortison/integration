<?php

namespace app\modules\apilead\common\traits;

use app\modules\apilead\common\models\forms\ResponseModel;
use yii\helpers\FileHelper;
use Yii;
use yii\helpers\VarDumper;

/**
 * Trait LogTrait
 * @package app\modules\apilead\common\traits
 * @property \app\modules\apilead\common\objects\Config $config
 */
trait LogTrait
{
	/**
	 * Вносить запит до журналу замовлень
	 *
	 * @param ResponseModel $responseModel
	 * @param $requestUrl
	 * @param string $requestData
	 */
	public function logBridgeRequest(ResponseModel $responseModel, $requestUrl, $requestData = '')
	{
		$message = $this->getFormatMessage($responseModel, $requestUrl, $requestData);

		$this->writeBridge($message);
	}
	
	/**
	 * Вносить запит до журналу опитувань статусів
	 *
	 * @param ResponseModel $responseModel
	 * @param string $requestUrl
	 * @param string $requestData
	 */
	public function logStatusRequest(ResponseModel $responseModel, $requestUrl, $requestData = '')
	{
		$message = $this->getFormatMessage($responseModel, $requestUrl, $requestData);

		$this->writeStatus($message);
	}
	
	/**
	 * Логує запит від системи Apilead
	 *
	 * @param $incomingData array Масив вхідних даних
	 * @return void
	 */
	public function logApileadRequest($incomingData)
	{
		$logPath    = $this->config->getFullLogPath();
		$fileName   = $this->getFileName(false);
		
		$message = $this->getLogSeparatorLineTop() . $this->encode($incomingData) . PHP_EOL;
		
		$this->writeLine($logPath, $fileName, $message);
	}
	
	/**
	 * Вносить запис до журналу замовлень
	 *
	 * @param $message string Зміст запиту, що буде внесено до журналу
	 * @return void
	 */
	public function writeBridge($message)
	{
		$logPath    = $this->config->getFullLogPath();
		$fileName   = $this->getFileName(false);

		$this->writeLine($logPath, $fileName, $message);
	}
	
	/**
	 * Вносить запис до журналу опитувань статусів
	 *
	 * @param $message string Зміст запиту, що буде внесено до журналу
	 * @return void
	 */
	public function writeStatus($message)
	{
		$logPath    = $this->config->getFullLogPath();
		$fileName   = $this->getFileName();

		$this->writeLine($logPath, $fileName, $message);
	}

	public function logStatusRequestError(ResponseModel $responseModel)
	{

	}
	
	/**
	 * Вносить запис до журналу
	 *
	 * @param $filePath string Шлях до журналу запитів
	 * @param $fileName string Назва журналу запитів
	 * @param $message string Зміст запиту, що буде внесено до журналу
	 * @return void
	 */
	public function writeLine($filePath, $fileName, $message)
	{
		FileHelper::createDirectory($filePath);
		file_put_contents($filePath . $fileName, $message, FILE_APPEND);
	}
	
	/**
	 * Форматує повідомлення про запит у зручний для сприймання вигляд
	 *
	 * @param $responseModel ResponseModel
	 * @param $requestUrl
	 * @param $requestData
	 * @return string Відформатоване повідомлення
	 */
	public function getFormatMessage(ResponseModel $responseModel, $requestUrl, $requestData)
	{
		list($code, $error, $time, $content, $method) = $responseModel->getResponseAsArray();

		$url	= $requestUrl;
		$data	= VarDumper::export($requestData);

		return  $this->getLogSeparatorLineTop() .
				"[REQUEST_URL:          {$url}]" . PHP_EOL .
				"[REQUEST_METHOD:       {$method}]" . PHP_EOL .
				"[REQUEST_DATA:         {$data}]" . PHP_EOL .
				"[RESPONSE_HTTP_CODE:   {$code}]" . PHP_EOL .
				"[RESPONSE:             {$content}]" . PHP_EOL .
				"[CURL_ERROR:           {$error}]" . PHP_EOL .
				"[REQUEST_TIME:         {$time}]" . $this->getLogSeparatorLineBottom();
	}
	
	/**
	 * Повертає тип поточного запиту запиту
	 *
	 * @param bool $forStatus
	 * @return string
	 */
	public function getFileName($forStatus=true)
	{
		if ($forStatus)
			return 'status.log';
		return 'bridge.log';
	}

	public function getLogSeparatorLineTop()
	{
		$logDataTime    = Yii::$app->formatter->asDatetime('now');

		return  "========================================================================================================================" . PHP_EOL  .
				"================================================ {$logDataTime} ===================================================" . PHP_EOL  .
				"========================================================================================================================" .
				PHP_EOL . PHP_EOL . PHP_EOL;
	}

	public function getLogSeparatorLineBottom()
	{
		return  PHP_EOL . PHP_EOL . PHP_EOL .
				"========================================================================================================================" . PHP_EOL  .
				"========================================================================================================================" .
				PHP_EOL . PHP_EOL . PHP_EOL;
	}
}