<?php

namespace app\modules\apilead\common\traits;

use Yii;
use yii\web\Response;

trait ErrorTrait
{
	/**
	 * Повертає повідомлення про помилку.
	 *
	 * @param $data
	 * @param $statusCode integer
	 * @return string
	 */
	function renderJsonError($data, $statusCode=400)
	{
		Yii::$app->response->statusCode	= $statusCode;
		
		return $this->renderJson(['status' => 'error', 'message' => $data]);
	}
	
	/**
	 * Показує для системи Apilead ідентифікатор замовлення в системі партнера.
	 *
	 * @param mixed $id Унікальний ідентифікатор замовлення в системі партнера.
	 * @return mixed
	 */
	function renderJsonOk($id=null)
	{
		$data['status'] = 'ok';
		
		if ($id)
			$data['id'] = $id;
		
		return $this->renderJson($data);
	}
	
	/**
	 * Відображає маси в JSON форматі.
	 *
	 * @param $data mixed
	 * @return mixed
	 */
	function renderJson($data)
	{
		Yii::$app->response->format		= Response::FORMAT_JSON;
		
		return $data;
	}
}