<?php

namespace app\modules\apilead\common\traits;

use app\modules\apilead\common\models\forms\ResponseModel;
use app\modules\apilead\common\objects\PrepareRequest;
use yii\httpclient\Client;

/**
 * Trait RequestTrait
 * @package \app\modules\apilead\common\traits
 * @property \app\modules\apilead\common\objects\Config $config
 */
trait RequestTrait
{
	/**
	 * Загальний метод, що здійснює мостові запити.
	 *
	 * @param PrepareRequest $prepareRequest
	 * @return ResponseModel
	 */
	public function bridgeRequest(PrepareRequest $prepareRequest)
	{
		$responseModel = $this->makeRequest($prepareRequest);
		
		$requestUrl = $prepareRequest->getRequestUrl();
		$requestData = $prepareRequest->getRequestData();
		
		$this->logBridgeRequest($responseModel, $requestUrl, $requestData);
		
		return $responseModel;
	}
	
	/**
	 * Загальний метод, що здійснює статусні запити.
	 *
	 * @param PrepareRequest $prepareRequest
	 * @return ResponseModel
	 */
	public function statusRequest(PrepareRequest $prepareRequest)
	{
		$responseModel = $this->makeRequest($prepareRequest);
		
		$requestUrl = $prepareRequest->getRequestUrl();
		$requestData = $prepareRequest->getRequestData();
		
		$this->logStatusRequest($responseModel, $requestUrl, $requestData);
		
		return $responseModel;
	}
	
	/**
	 * Загальний метод, що здійснює запит.
	 *
	 * @param PrepareRequest $prepareRequest
	 * @return ResponseModel
	 */
	public function makeRequest(PrepareRequest $prepareRequest)
	{
		$start = microtime(true);
		
		$requestUrl		= $prepareRequest->getRequestUrl();
		$requestData	= $prepareRequest->getRequestData();
		$requestContent	= $prepareRequest->getRequestContent();
		$requestMethod	= $prepareRequest->getRequestMethod();
		$requestFormat	= $prepareRequest->getRequestFormat();
		$requestHeaders	= $prepareRequest->getRequestHeaders();
		$requestCookies	= $prepareRequest->getRequestCookies();
		$requestTimeout	= $prepareRequest->getRequestTimeout();
		
		$responseFormat = $prepareRequest->getResponseFormat();
		
		$client = new Client([
			'transport' => 'yii\httpclient\CurlTransport',
			'responseConfig' => [
				'format' => $responseFormat,
			],
		]);
		
		try {
			$response = $client->createRequest()
				->setUrl($requestUrl)
				->setMethod($requestMethod)
				->setFormat($requestFormat)
				->setData($requestData)
				->setContent($requestContent)
				->setHeaders($requestHeaders)
				->setCookies($requestCookies)
				->setOptions([
					'timeout'       => $requestTimeout,
					'sslVerifyPeer' => false,
					'sslVerifyHost' => false,
				])
				->send();
			
			$code       = $response->getStatusCode();
			$content    = $response->getContent();
//			$data		= $response->getData();
			$data		= "";
			$error		= "";
			$cookie		= $response->getCookies();
			
		} catch (\Exception $e) {
			$error 		= $e->getMessage();
			$code		= "";
			$content 	= "";
			$data		= "";
			$cookie 	= [];
		}
		
		$time       = microtime(true) - $start;
		
		$responseModel = new ResponseModel();
		$responseModel->setCode($code);
		$responseModel->setContent($content);
		$responseModel->setData($data);
		$responseModel->setTime($time);
		$responseModel->setMethod($requestMethod);
		$responseModel->setError($error);
		$responseModel->setCookie($cookie);
		
		return $responseModel;
	}
}
