<?php

namespace app\modules\apilead\common\adv\objects;

use yii\web\HttpException;

class User extends Configs
{
	/**
	 * Повертає api_key користувача системи Apilead.
	 *
	 * @return mixed
	 * @throws HttpException Якщо не для користувача не задано apiKey.
	 */
	public function getApiKey()
	{
		$apiKey = $this->array;
		
		if (!isset($apiKey['apileadApiKey']))
			throw new HttpException(405, 'apiKey must be set!');
		
		return $apiKey['apileadApiKey'];
	}
	
	/**
	 * Повертає api_key користувача для отримання статусів по постбеку.
	 *
	 * @return mixed
	 */
	public function getPostbackApiKey()
	{
		$apiKey = $this->array;
		
		return $apiKey['postbackApiKey'] ?? null;
	}
}