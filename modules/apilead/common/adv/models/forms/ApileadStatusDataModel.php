<?php
/**
 * Created by PhpStorm.
 * User: nagard
 * Date: 13.11.17
 * Time: 12:47
 */

namespace app\modules\apilead\common\adv\models\forms;

use Exception;
use yii\base\BaseObject;

/**
 * Class ApileadStatusDataModel
 *
 * @package app\modules\apilead\common\models\forms
 *
 * @property integer $id Унікальний ідентифікатор замовлення в системі Apilead
 * @property integer $status Статус замовлення в системі Apilead
 * @property integer $comment Комендар до замовлення в системі Apilead
 * @property integer $partnerStatus Статус замовлення в системі партнера
 * @property integer $apiKey api_key користувача в системі Apilead
 */
class ApileadStatusDataModel extends BaseObject
{
	public $id;
	public $status;
	public $comment;
	
	public $partnerId;
	public $partnerStatus;
	
	public $apiKey;


	public function __construct($apiKey='', array $config = [])
	{
		parent::__construct($config);
		
		if (!$apiKey)
			throw new Exception("Sorry, but apiKey not set!");
		
		$this->apiKey = $apiKey;
	}

	public function setId($value)
	{
		$this->id = $value;
	}

	public function setStatus($value)
	{
		$this->status = $value;
	}

	public function setComment($value)
	{
		$this->comment = $value;
	}
	
	/**
	 * В подальшому треба видалити та перейти на Partner
	 *
	 * @param $value
	 */
	public function setSourceStatus($value)
	{
		$this->setPartnerStatus($value);
	}
	
	public function setPartnerId($value)
	{
		$this->partnerId = $value;
	}

	public function setPartnerStatus($value)
	{
		$this->partnerStatus = $value;
	}
	
	/**
	 * Формуємо чексуму ліда
	 *
	 * @return string
	 * @throws Exception
	 */
	public function getCheckSum()
	{
		$id         = $this->id;
		$status     = $this->status;
		$comment    = $this->comment;
		$apiKey     = $this->apiKey;
		
		return sha1($id . $status . $comment . $apiKey);
	}
	
	/**
	 * Повертає підготовлений масив, що годиться, до відправлення в систему apilead
	 *
	 * @return array
	 */
	public function getStatusData()
	{
		return [
			'id'        => $this->id,
			'status'    => $this->status,
			'comment'   => $this->comment,
			'check_sum' => $this->checkSum,
		];
	}
	
	/**
	 * Повертає підготовлений масив із всіма властивостями
	 *
	 * @return array
	 */
	public function getProperties()
	{
		return [
			'id'			=> $this->id,
			'status'		=> $this->status,
			'comment'		=> $this->comment,
			'partnerId'		=> $this->partnerId,
			'partnerStatus'	=> $this->partnerStatus,
		];
	}
}