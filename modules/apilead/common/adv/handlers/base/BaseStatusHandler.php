<?php

namespace app\modules\apilead\common\adv\handlers\base;

use app\common\models\OrderApilead;
use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\models\forms\ResponseModel;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use app\modules\apilead\common\traits\JsonTrait;
use app\modules\apilead\common\traits\LogTrait;
use app\modules\apilead\common\traits\RequestTrait;
use Exception;

/**
 * Базовий клас від якого можуть наслідуватися всі обробники статусів кожної інтеграція
 *
 * @property \app\modules\apilead\common\adv\objects\Config $config Повна конфігурація для поточного замовлення
 * @property \app\common\models\OrderApilead $orderModel Модель поточного замовлення
 */

abstract class BaseStatusHandler extends \yii\base\BaseObject
{
	use JsonTrait;
	use RequestTrait;
	use LogTrait;
	
	public $config;
	public $orderModel;
	
	/**
	 * Метод відповідає за повну обробку і оновлення статусу замовлення.
	 *
	 * Основна метод, що буде викликаний в контролері (загальному обробнику).
	 * На цьому методі висить вся логіка обробки замовлень:
	 * * збереження актуальної інформації замовлення у себе.
	 * * надсилання актуального статусу в систему Apilead.
	 *
	 * Зазвичай, алгоритм обробки замовлення наступний:
	 *
	 * * Одержати відповідь від рекламодавця за допомогою методу $this->makeStatusRequest()
	 * * Сформувати модель, котру буде відправлено в систему Apilead за допомогою методу $this->getSystemStatusData()
	 * * Розпочати транзакцію
	 * * Оновити статус у себе в базі за допомогою методу $orderModel->updateOrder()
	 * * Оновити статус замовлення та його коментар в системі Apilead за допомогою методу $this->sendOrderStatusToApilead()
	 *
	 * @param bool $force
	 * @return ApileadStatusDataModel
	 */
	abstract function updateStatusInSystem($force=false);
	
	/**
	 * Метод робить запит до рекламодавця і повертає від нього не опрацьовану відповідь у вигляді наповненої моделі ResponseModel.
	 *
	 * @return ResponseModel
	 */
	abstract function makeStatusRequest();
	
	/**
	 * Запит для отримання викупу по визначеному замовленню.
	 * Метод робить запит до рекламодавця і повертає від нього не опрацьовану відповідь у вигляді наповненої моделі ResponseModel.
	 * Зазвичай статус викупу міститься у відповіді на запит отримання звичайного статусу, але в разі,
	 * якщо для отримання статусу викупу необхідно робити окремий запит, доведеться перевизначити цей метод в статусному обробкику.
	 *
	 * @return ResponseModel
	 */
	public function makeRansomStatusRequest():ResponseModel
	{
		return $this->makeStatusRequest();
	}
	
	/**
	 * Повертає модель ApileadStatusDataModel, дані з якої будуть відправлені для оновлення статусу замовлення в системі Apilead.
	 *
	 * @param ResponseModel $responseModel
	 * @return ApileadStatusDataModel
	 */
	abstract function getSystemStatusData(ResponseModel $responseModel);
	
	
	/**
	 * Повертає модель ApileadStatusDataModel, дані з якої будуть відправлені для оновлення статусу замовлення в системі Apilead.
	 *
	 * @param ResponseModel $responseModel
	 * @return ApileadStatusDataModel
	 */
	public function getRansomStatusData(ResponseModel $responseModel)
	{
		return new ApileadStatusDataModel(true, [
			'comment' => "ERROR. Ransom handler not implemented for '{$this->orderModel->integration_name}'",
		]);
	}
	
	/**
	 * Повертає актуальний статус замовлення, котрий підходить для відправлення в систему Apilead.
	 *
	 * @param string|integer $partnerStatus
	 * @return string
	 */
	public function getApileadStatusName($partnerStatus)
	{
		$statuses = $this->config->getStatuses();
		
		$expect		= $statuses[OrderApilead::STATUS_EXPECT]	?? [];
		$reject		= $statuses[OrderApilead::STATUS_REJECT]	?? [];
		$trash		= $statuses[OrderApilead::STATUS_TRASH]		?? [];
		$confirm	= $statuses[OrderApilead::STATUS_CONFIRM]	?? [];
		
		if (array_key_exists($partnerStatus, $expect))
			return OrderApilead::STATUS_EXPECT;
		
		elseif (array_key_exists($partnerStatus, $reject))
			return OrderApilead::STATUS_REJECT;
		
		elseif (array_key_exists($partnerStatus, $trash))
			return OrderApilead::STATUS_TRASH;
		
		elseif (array_key_exists($partnerStatus, $confirm))
			return OrderApilead::STATUS_CONFIRM;
		
		else
			return OrderApilead::STATUS_UNKNOWN;
	}
	
	/**
	 * Повертає актуальний статус замовлення, котрий підходить для відправлення в систему Apilead.
	 *
	 * @param string|integer $partnerRansomStatus
	 * @param string|integer $partnerSubRansomStatus
	 * @return string
	 */
	public function getRansomStatusName($partnerRansomStatus, $partnerSubRansomStatus=null)
	{
		$ransomStatuses = $this->config->getRansomStatuses();
		
		$confirm	= $ransomStatuses[OrderApilead::RANSOM_CONFIRM]	?? [];
		$reject		= $ransomStatuses[OrderApilead::RANSOM_REJECT]	?? [];
		$expect		= $ransomStatuses[OrderApilead::RANSOM_EXPECT]	?? [];
		
		if (array_key_exists($partnerRansomStatus, $reject))
			return OrderApilead::RANSOM_REJECT;
		
		elseif (array_key_exists($partnerRansomStatus, $expect))
			return OrderApilead::RANSOM_EXPECT;
		
		elseif (array_key_exists($partnerRansomStatus, $confirm))
			return OrderApilead::RANSOM_CONFIRM;
		
		else
			return OrderApilead::RANSOM_UNKNOWN;
	}
	
	/**
	 * Метод повертає статичний коментар, котрий прив'язаний до ідентифікатору статуса.
	 * Цей метод потрібен, якщо рекламодавець не віддає коментарів.
	 * Тому це залишається єдиним способом отримання коментарів.
	 *
	 * @param $statusName
	 * @throws Exception
	 * @return string
	 */
	public function getCommentByStatus($systemStatus, $partnetStatus)
	{
		throw new Exception('Перевизнач метод getCommentByStatus() у себе в обробнику!');
	}

	/**
	 * Відправляє актуальний статус до системи Apilead.
	 *
	 * В майбутньому треба буде ретельніше продумати логіку реагування на всілякі
	 * повернуті від apilead помилки.
	 *
	 * @param ApileadStatusDataModel $apileadStatusDataModel
	 * @param bool $needSendStatus Пташка, чи потрібно надсилати запит до системи Apilead
	 * @return ResponseModel|null
	 * @throws Exception якщо не вдалося відправити актуальний статус.
	 */
	public function sendOrderStatusToApilead(ApileadStatusDataModel $apileadStatusDataModel, $needSendStatus=true)
	{
		$orderModel			= $this->orderModel;
		$myId				= $orderModel->id ?? null;
		$apileadUserId		= $orderModel->system_user_id ?? null;
		$partnerId			= $orderModel->partner_order_id ?? null;
		$apileadId			= $orderModel->system_order_id ?? null;
		$apileadOfferId		= $orderModel->offer_id ?? null;
		
		if (!$needSendStatus) {
			$this->writeStatus("[DO NOT NEED SEND STATUS TO APILEAD: myId={$myId}, partnerId={$partnerId}, apileadId={$apileadId}]\n\n\n");
			return null;
		}
		
		$requestUrl     = $this->config->getUrlApileadOrderUpdate();
		$requestData    = $apileadStatusDataModel->getStatusData();
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestData($requestData);
		$prepareRequest->setRequestTimeout(60);
		
		$responseModel		= $this->statusRequest($prepareRequest);
		$responseCode		= $responseModel->getCode();
		$responseContent	= $responseModel->getContent();
		
		if ($responseCode == 200) {
			return $responseModel;
		} else {
			/*
			 * Якщо запит говорить, що статус в системі вже встановлений
			 * То ми цю помилку не прив'язуємо до замовлення.
			 * Це потрібно, щоб не було накопичення помилок.
			 * Пізніше треба впровадити складнішу логіку,
			 * котра буде реагувати на кількість подібних помилок,
			 * якщо така помилка є - не логуємо,
			 * а краще взагалі не робити жодних опитувань помилкових замовлень,
			 * у котрих ен або більше помилок.
			 */
			
			if ($responseCode == 400) {
				$jsonObject = $this->decode($responseContent);
				
				if (isset($jsonObject->error) && $jsonObject->error == 'Cannot change status')
					return null;
			}
			throw new OrderErrorException($myId, "Update remote apilead status error. [user:{$apileadUserId}] [offer:{$apileadOfferId}] [apileadId:{$apileadId}] [partnerId:{$partnerId}] [id:{$myId}]. [RESPONSE:{$responseContent}]");
//			Log::error("Update remote apilead status error. [user:{$apileadUserId}] [offer:{$apileadOfferId}] [apileadId:{$apileadId}] [partnerId:{$partnerId}] [id:{$myId}]. [RESPONSE:{$responseContent}]", $myId);
		}
	}
}