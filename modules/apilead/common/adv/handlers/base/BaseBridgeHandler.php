<?php

namespace app\modules\apilead\common\adv\handlers\base;

use app\common\models\OrderApilead;
use app\modules\apilead\common\models\forms\ResponseModel;
use app\modules\apilead\common\adv\objects\Config;
use app\modules\apilead\common\traits\LogTrait;
use app\modules\apilead\common\traits\RequestTrait;
use app\modules\apilead\common\traits\JsonTrait;
use yii\web\HttpException;

/**
 * Created by PhpStorm.
 * User: nagard
 * Date: 02.11.17
 * Time: 15:48
 *
 * @property \app\modules\apilead\common\adv\models\forms\ApiLeadAdvModel $incomingModel Дані замовлення, що прийшли від системи Apilead
 * @property Config $config Головна конфігурація поточного замовлення
 * @property ResponseModel $response Об'єкт запиту
 */
abstract class BaseBridgeHandler extends \yii\base\BaseObject
{
	use RequestTrait;
	use LogTrait;
	use JsonTrait;
	
	public $requestUrl;
	public $requestData;
	public $response;
	
	public $config;
	public $incomingModel;
	
	/**
	 * Підготовляє та надсилає замовлення до партнера.
	 *
	 * Отримує від нього відповідь, та зберігає всю наявну інформацію до загальної БД.
	 *
	 * @return integer|string Має повернути ідентифікатор замовлення в системі партнера.
	 */
	abstract public function createOrder();
	
	/**
	 * Повертає ідентифікатор замовлення на стороні партнера.
	 * Потрібен, щоб красиво реалізувати інтеграції рекламодавця 3437
	 * котрий є відгалуженням змичайного leadvertex.
	 *
	 * @param string $responseContent Тіло запиту.
	 * @return string|integer Ідентифікатор замовлення на стороні партнера
	 * @throws HttpException
	 */
	abstract public function getOrderAttributes($responseContent);
	
	/**
	 * Зберігає поточне замовлення з усіма даними, і нашими, і партнера до нашої загальної бази.
	 *
	 * @param $attributes
	 * @return OrderApilead
	 * @throws HttpException Якщо замовлення не вдалося зберегти
	 */
	function saveOrder($attributes)
	{
		$incomingModel 						= $this->incomingModel;
		$apileadModel 						= new OrderApilead;

		$apileadModel->integration_name		= $this->config->integrationName;
		$apileadModel->integration_module	= $this->config->moduleName;
		
		$apileadModel->offer_id				= $incomingModel->offer_id;
		$apileadModel->system_order_id		= $incomingModel->id;
		$apileadModel->system_user_id		= $incomingModel->user_id;
		
		$apileadModel->incoming_data					= $this->encode($incomingModel);
		
		$apileadModel->incoming_field_id				= $incomingModel->id ?? null;
		$apileadModel->incoming_field_campaign_id		= $incomingModel->campaign_id ?? null;
		$apileadModel->incoming_field_name				= $incomingModel->name ?? null;
		$apileadModel->incoming_field_country			= $incomingModel->country ?? null;
		$apileadModel->incoming_field_phone				= $incomingModel->phone ?? null;
		$apileadModel->incoming_field_tz				= $incomingModel->tz ?? null;
		$apileadModel->incoming_field_address			= $incomingModel->address ?? null;
		$apileadModel->incoming_field_user_comment		= $incomingModel->user_comment ?? null;
		$apileadModel->incoming_field_cost				= $incomingModel->cost ?? null;
		$apileadModel->incoming_field_cost_delivery		= $incomingModel->cost_delivery ?? null;
		$apileadModel->incoming_field_landing_cost		= $incomingModel->landing_cost ?? null;
		$apileadModel->incoming_field_user_id			= $incomingModel->user_id ?? null;
		$apileadModel->incoming_field_web_id			= $incomingModel->web_id ?? null;
		$apileadModel->incoming_field_stream_id			= $incomingModel->stream_id ?? null;
		$apileadModel->incoming_field_product_id		= $incomingModel->product_id ?? null;
		$apileadModel->incoming_field_offer_id			= $incomingModel->offer_id ?? null;
		$apileadModel->incoming_field_ip				= $incomingModel->ip ?? null;
		$apileadModel->incoming_field_user_agent		= $incomingModel->user_agent ?? null;
		$apileadModel->incoming_field_landing_currency	= $incomingModel->landing_currency ?? null;
		$apileadModel->incoming_field_check_sum			= $incomingModel->check_sum ?? null;
		
		$apileadModel->request_data			= $this->encode($this->requestData);
		$apileadModel->request_url			= $this->requestUrl;
		$apileadModel->request_timeout		= $this->response->getTime();

		$apileadModel->response_code		= $this->response->getCode();
		$apileadModel->response_result		= $this->response->getContent();
		$apileadModel->response_error		= $this->response->getError();
		
		$apileadModel->load($attributes, '');
		
		if ($apileadModel->save())
			return $apileadModel;
			
		throw new HttpException(400, "Order_id {$apileadModel->partner_order_id}=>{$incomingModel->id} not save to ApiLead DB" . $this->encode($apileadModel->getErrors()));
	}
	
	/**
	 * Пепевірка, чи вже існує, поточне замовлення.
	 * Якщо замовлення існує буде повернуто його модель.
	 *
	 * @return OrderApilead|null
	 */
	public function isOrderExist()
	{
		return OrderApilead::getUserOrder($this->incomingModel->user_id, $this->incomingModel->id, $this->config->moduleName);
	}
}