<?php
/**
 * Created by PhpStorm.
 * User: nagard
 * Date: 03.03.18
 * Time: 13:28
 */

namespace app\modules\apilead\common\exeptions;


use yii\base\UserException;

class OrderErrorException extends UserException
{
	/**
	 * @var int Внутрішній ідентифікатор замовлення.
	 */
	public $orderId;
	
	/**
	 * Constructor.
	 * @param int $orderId Внутрішній ідентифікатор замовлення.
	 * @param string $message error message
	 * @param int $code error code
	 * @param \Exception $previous The previous exception used for the exception chaining.
	 */
	public function __construct($orderId, $message = null, $code = 0, \Exception $previous = null)
	{
		$this->orderId = $orderId;
		parent::__construct($message, $code, $previous);
	}
	
	/**
	 * @return int Повертає внутрішній ідентифікатор замовлення.
	 */
	public function getOrderId()
	{
		return $this->orderId;
	}
}