<?php

namespace app\modules\apilead\common\web\handlers\base;

use app\modules\apilead\common\traits\LogTrait;
use app\modules\apilead\common\traits\RequestTrait;
use app\modules\apilead\common\traits\JsonTrait;

/**
 * Created by PhpStorm.
 * User: nagard
 * Date: 02.11.17
 * Time: 15:48
 *
 * @property \app\modules\apilead\common\web\models\forms\ApileadWebModel $incomingModel Дані замовлення, що прийшли від системи Apilead
 * @property \app\modules\apilead\common\web\objects\Config $config Головна конфігурація поточного замовлення
 * @property \app\modules\apilead\common\models\forms\ResponseModel $response Об'єкт запиту
 */
abstract class BaseBridgeHandler extends \yii\base\BaseObject
{
	use RequestTrait;
	use LogTrait;
	use JsonTrait;
	
	public $config;
	public $incomingModel;
	
	public function getRelatedStatus($systemStatus)
	{
		$statuses = $this->config->getStatuses();
		
		return $statuses[$systemStatus] ?? null;
	}
}