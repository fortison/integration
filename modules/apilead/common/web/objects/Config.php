<?php

namespace app\modules\apilead\common\web\objects;

use yii\base\Exception;
use Yii;
use yii\web\HttpException;

/**
 * @property Full $full
 * @property User $user
 */

class Config extends \yii\base\BaseObject
{
	public $full;
	public $user;
	
	public $integrationName;
	public $moduleName;
	public $userId;
	
	private $configFilePath;
	
	public function init()
	{
		$this->setConfigFilePath();
		if (!$this->isConfigFilePathExist())
			throw new Exception("Config file does not exist. File: {$this->integrationName}");
		$this->setFull();
		$this->setUser();
	}
	
	public function setConfigFilePath()
	{
		$integrationName = $this->getIntegrationName();
		$this->configFilePath = Yii::getAlias("@app/modules/{$this->moduleName}/config/web/{$integrationName}.php");
	}
	
	public function getConfigFilePath()
	{
		return $this->configFilePath;
	}
	
	public function isConfigFilePathExist()
	{
		return is_file($this->getConfigFilePath());
	}
	
	/**
	 * Повертає шлях до директорії з логами поточної інтеграції
	 *
	 * @param null|integer $timestamp Часова мітка.
	 * @return string
	 */
	public function getFullLogPath($timestamp = null)
	{
		if (!$timestamp)
			$timestamp = time();
		
		$brakeLogFolder = $this->getBrakeLogFolder();
		
		return Yii::getAlias('@runtime/apilead/logs/web' . DIRECTORY_SEPARATOR . Yii::$app->formatter->asDate($timestamp) . DIRECTORY_SEPARATOR . $brakeLogFolder . DIRECTORY_SEPARATOR);
	}
	
	public function setFull()
	{
		$full = require($this->getConfigFilePath());
		
		$this->full = new Full([
			'array' => $full,
		]);
	}
	
	public function setUser()
	{
		$user = $this->full->array[$this->userId] ?? null;
		
		if ($user === null)
			throw new Exception("User [{$this->userId}] not found in config file - {$this->integrationName}");
		
		$this->user = new User([
			'array' => $user,
		]);
	}
	
	/**
	 * Повертає максимальний час оцікування відповіді від партнера.
	 * Відноситься тільки до мостового запиту.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return null
	 */
	public function getBridgeRequestTimeout()
	{
		if ($this->user->getBridgeRequestTimeout())
			return $this->user->getBridgeRequestTimeout();
		elseif ($this->full->getBridgeRequestTimeout())
			return $this->full->getBridgeRequestTimeout();
		else
			return 5;
	}
	
	/**
	 * Повертає максимальний час оцікування відповіді від партнера.
	 * Відноситься тільки до статусного запиту.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return null
	 */
	public function getStatusRequestTimeout()
	{
		if ($this->user->getStatusRequestTimeout())
			return $this->user->getStatusRequestTimeout();
		elseif ($this->full->getStatusRequestTimeout())
			return $this->full->getStatusRequestTimeout();
		else
			return 5;
	}
	
	/**
	 * Перевіряє, чи активний міст.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return bool
	 */
	public function isBridgeEnabled()
	{
		if (!$this->full->isBridgeEnabled())
			return false;
		elseif (!$this->user->isBridgeEnabled())
			return false;
		else
			return true;
	}
	
	/**
	 * Перевіряє, чи активний обробник статусів.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return bool
	 */
	public function isStatusEnabled()
	{
		if (!$this->full->isStatusEnabled())
			return false;
		elseif (!$this->user->isStatusEnabled())
			return false;
		else
			return true;
	}
	
	/**
	 * Повертає назву поточної інтеграції
	 *
	 * @return string
	 */
	public function getIntegrationName()
	{
		$integrationName = $this->integrationName;
		
		if (!$integrationName)
			throw new HttpException(405, "integrationName must be set!");
		
		return $integrationName;
	}
	
	/**
	 * Повертає неймспейс обробника статусів поточної інтеграції
	 * Враховується повна логічна ієрархія.
	 *
	 * @return string
	 */
	public function getStatusHandler()
	{
		
		if ($this->user->getStatusHandler())
			$statusHandler = $this->user->getStatusHandler();
		else
			if ($this->full->getStatusHandler())
				$statusHandler = $this->full->getStatusHandler();
			else {																									// Якщо обробника ніде не вказано, формуємо його з назви інтеграції
				$integrationName  = $this->integrationName;
				$integrationHandlerName  = ucfirst($this->integrationName);
				$moduleName     = $this->moduleName;
				$statusHandler  = "app\\modules\\{$moduleName}\\handlers\\web\\{$integrationName}\\{$integrationHandlerName}StatusHandler";
			}
		
		return $statusHandler;
	}
	
	/**
	 * Повертає неймспейс обробника поточної інтеграції.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return string
	 */
	public function getBridgeHandler()
	{
		if ($this->user->getBridgeHandler())
			$bridgeHandler = $this->user->getBridgeHandler();
		else
			if ($this->full->getBridgeHandler())
				$bridgeHandler = $this->full->getBridgeHandler();
			else {																									// Якщо обробника ніде не вказано, формуємо його з назви інтеграції
				$integrationName  = $this->integrationName;
				$integrationHandlerName  = ucfirst($this->integrationName);
				$moduleName     = $this->moduleName;
				$bridgeHandler  = "app\\modules\\{$moduleName}\\handlers\\web\\{$integrationName}\\{$integrationHandlerName}BridgeHandler";
			}
		
		return $bridgeHandler;
	}
	
	/**
	 * Повертає посилання urlApileadOrderUpdate для оновлення статусів в системі Apilead.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return string
	 */
	public function getUrlApileadOrderUpdate()
	{
		if ($this->user->getUrlApileadOrderUpdate())
			$urlApileadOrderUpdate = $this->user->getUrlApileadOrderUpdate();
		else
			if ($this->full->getUrlApileadOrderUpdate())
				$urlApileadOrderUpdate = $this->full->getUrlApileadOrderUpdate();
			else
				$urlApileadOrderUpdate  = "http://al-api.com/api/lead/update";
		
		return $urlApileadOrderUpdate;
	}
	
	/**
	 * Повертає масив статусів.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return array
	 */
	public function getStatuses()
	{
		if ($this->user->getStatuses())
			$statuses = $this->user->getStatuses();
		else
			if ($this->full->getStatuses())
				$statuses = $this->full->getStatuses();
			else
				$statuses  = [];
		
		return $statuses;
	}
	
	/**
	 * Повертає масив сабстатусів.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return array
	 */
	public function getSubStatuses()
	{
		if ($this->user->getSubStatuses())
			$subStatuses = $this->user->getSubStatuses();
		else
			if ($this->full->getSubStatuses())
				$subStatuses = $this->full->getSubStatuses();
			else
				$subStatuses  = [];
		
		return $subStatuses;
	}
	
	/**
	 * Повертає посилання для передачі замовлення вебмайстру.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return string
	 * @throws Exception Якщо postbackUrl не задане
	 */
	public function getPostbackUrl()
	{
		if ($this->user->getPostbackUrl())
			$postbackUrl = $this->user->getPostbackUrl();
		else
			if ($this->full->getPostbackUrl())
				$postbackUrl = $this->full->getPostbackUrl();
			else
				throw new Exception("Value {postbackUrl} must be set. File: {$this->integrationName}");
		
		return $postbackUrl;
	}
	
	/**
	 * Повертає шлях до журналу.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return string
	 */
	public function getBrakeLogFolder()
	{
		$full	= $this->full->getBrakeLogFolder();
		$user	= $this->user->getBrakeLogFolder();
		
		if ($user !== null) {
			if ($user === true || !$user)
				$user = $this->userId;
			
			if ($full === true || !$full)
				$full = $this->integrationName;
			
			$brakeLogFolder = $full . DIRECTORY_SEPARATOR . $user;
		} else {
			if ($full === true || !$full)
				$full = $this->integrationName;
			
			$brakeLogFolder = $full;
		}
		
		return $brakeLogFolder;
	}
	
	/**
	 * Перевірка чи можна оновлювати статус замовлення.
	 *
	 * Це потрібно для зменшення часу виконання крону на опитування статусів,
	 * зменшення кількості зайвих запитів
	 * та зменшення розміру журналу, що дозволить зберігати журнал за ширший період.
	 *
	 * @param integer $createdAt Час коли замовлення було додане
	 * @param integer $updatedAt Час коли замовлення було оновлено
	 * @return bool
	 */
	public function isTimeToStatusRequest($createdAt, $updatedAt)
	{
		$time				= time();
		$initialDelay		= $this->getInitialDelay();
		$firstDayDelay		= $this->getFirstDayDelay();
		$secondDayDelay		= $this->getSecondDayDelay();
		$otherDayDelay		= $this->getOtherDayDelay();
		
		
		if ($createdAt == $updatedAt) {																					// Перші 20 хвилин немає сенсу опитувати замовлення
			echo("Щойно створене замовлення! ");
			if ($createdAt < $time - $initialDelay) {
				echo("Можна оновлювати. Вже минули перші {$initialDelay} сек. \n \n");
				return true;
			} else {
				$remaining = round(($updatedAt - ($time - $initialDelay)) / 60);
				echo("Не можна оновлювати. Лишилося {$remaining}хв. \n \n");
				return false;
			}
		} elseif ($createdAt > $time - 60*60*24) {																		// Перший день опитуємо замовлення кожні півгодини
			echo("Пішли перші 24 години! ");
			if ($updatedAt < ($time - $firstDayDelay)) {
				echo "Час для оновленяя настав. \n \n";
				return true;
			} else {
				$remaining = round(($updatedAt - ($time - $firstDayDelay)) / 60);
				echo "Ще не настав час для оновлення. Лишилося {$remaining}хв. \n\n";
				return false;
			}
		}elseif ($createdAt > $time - 60*60*48) {                                                                		// Другий день опитуємо замовлення кожні 3 години
			echo("Пішли другі 24 години! ");
			if ($updatedAt < $time - $secondDayDelay) {
				echo "Час для оновленяя настав. \n \n";
				return true;
			} else {
				$remaining = round(($updatedAt - ($time - $secondDayDelay)) / 60);
				echo "Ще не настав час для оновлення. Лишилося {$remaining}хв. \n\n";
				return false;
			}
		}elseif ($createdAt < $time) {																					// Третій і більше, опитуваня раз в день.
			echo("Минуло більше двох днів! ");
			if ($updatedAt < $time - $otherDayDelay) {
				echo "Час для оновлення настав. \n \n";
				return true;
			} else {
				$remaining = round(($updatedAt - ($time - $otherDayDelay)) / 60);
				echo "Ще не настав час для оновлення. Лишилося {$remaining}хв. \n\n";
				return false;
			}
		} else {
			echo("\nНевідомий діапозон дат.\n");
			return true;
		}
	}
	
	public function getInitialDelay()
	{
		$userInitialDelay = $this->user->getInitialDelay();
		$fullInitialDelay = $this->full->getInitialDelay();
		
		if ($userInitialDelay)
			return $userInitialDelay;
		elseif ($fullInitialDelay)
			return $fullInitialDelay;
		else
			return 60*20;
	}
	
	public function getFirstDayDelay()
	{
		$userFirstDayDelay = $this->user->getFirstDayDelay();
		$fullFirstDayDelay = $this->full->getFirstDayDelay();
		
		if ($userFirstDayDelay)
			return $userFirstDayDelay;
		elseif ($fullFirstDayDelay)
			return $fullFirstDayDelay;
		else
			return 60 * 30;
	}
	
	public function getSecondDayDelay()
	{
		$userSecondDayDelay = $this->user->getSecondDayDelay();
		$fullSecondDayDelay = $this->full->getSecondDayDelay();
		
		if ($userSecondDayDelay)
			return $userSecondDayDelay;
		elseif ($fullSecondDayDelay)
			return $fullSecondDayDelay;
		else
			return 60 * 60 * 3;
	}
	
	public function getOtherDayDelay()
	{
		$userOtherDayDelay = $this->user->getOtherDayDelay();
		$fullOtherDayDelay = $this->full->getOtherDayDelay();
		
		if ($userOtherDayDelay)
			return $userOtherDayDelay;
		elseif ($fullOtherDayDelay)
			return $fullOtherDayDelay;
		else
			return 60 * 60 * 24;
	}
}