<?php

namespace app\modules\apilead\controllers\adv;

use app\modules\apilead\common\objects\PreparePostRequestJson;
use app\modules\apilead\common\traits\RequestTrait;
use Yii;

class CpsBridgeController extends \yii\web\Controller
{
	use RequestTrait;
	
	public function actionIndex()
	{
		$user_id = Yii::$app->request->get('user_id');
		$offer_id = Yii::$app->request->get('offer_id');
		$country = 'RU';
		$name = 'cps_lead';
		$phone = (string) time();
		$api_key = Yii::$app->request->get('api_key');
		$utm_source = Yii::$app->request->get('utm_source');
		$utm_content = Yii::$app->request->get('utm_content');
		
		
		$requestData	= [
			'user_id' => $user_id,
			'name' => $name,
			'country' => $country,
			'phone' => $phone,
			'offer_id' => $offer_id,
			'check_sum' => sha1($user_id . $offer_id . $name . $phone . $api_key),
			'api_key' => $api_key,
			'utm_source' => $utm_source,
			'utm_content' => $utm_content,
//			'status' => 'confirm',
//			'chg_status' => 1,
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl('http://al-api.com/api/lead/create');
		$prepareRequest->setRequestData($requestData);
		$prepareRequest->setRequestTimeout(60);
		
		$responseModel		= $this->makeRequest($prepareRequest);
		$responseCode		= $responseModel->getCode();
		$responseContent	= $responseModel->getContent();
		
		print_r($responseContent);
	}
	
	public function beforeAction($action)
	{
		if ($action->id === 'index')
			$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}
}
