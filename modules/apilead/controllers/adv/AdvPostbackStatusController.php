<?php

namespace app\modules\apilead\controllers\adv;

use app\common\models\Error;
use app\common\models\OrderApilead;
use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\objects\Config;
use app\modules\apilead\common\traits\ErrorTrait;
use app\modules\apilead\common\traits\JsonTrait;
use app\modules\apilead\common\traits\LogTrait;
use nahard\log\helpers\Log;
use Exception;
use Yii;

class AdvPostbackStatusController extends \yii\web\Controller
{
	use JsonTrait;
	use ErrorTrait;
	use LogTrait;
	
	public function actionIndex()
	{
		$postbackApiKey		= Yii::$app->request->get('postbackApiKey');
		$systemOrderId		= Yii::$app->request->get('systemOrderId');
		$systemUserId		= Yii::$app->request->get('systemUserId');
		$partnerOrderId		= Yii::$app->request->get('partnerOrderId');
		$condition			= [];
		
		if (!$systemOrderId)
			return $this->renderJsonError('systemOrderId must be set!');
		
		$condition['integration_module'] = $this->module->id;
		$condition['system_order_id'] = $systemOrderId;
		
		if ($systemUserId)
			$condition['system_user_id'] = $systemUserId;
		
		if ($partnerOrderId)
			$condition['partner_order_id'] = $partnerOrderId;
		
		if (!$condition)																								// Якщо відсутні параметри для пошуку
			return $this->renderJsonError('Conditions for order must be set!');
		
		$orderModel = OrderApilead::findOne($condition);
		
		if (!$orderModel)																								// Якщо такого замовлення не існує
			return $this->renderJsonError("Wrong condition");
		
		try {
			$orderModel->startProcess();																				// Ставимо мітку, що цей запис в обробці
			
			$config = new Config([
				'integrationName' 	=> $orderModel->integration_name,
				'moduleName' 		=> $orderModel->integration_module,
				'userId' 			=> $orderModel->system_user_id,
				'offerId' 			=> $orderModel->offer_id,
			]);
			
			if ($config->user->getPostbackApiKey() !== $postbackApiKey)													// Якщо не сходяться ключі (Запит не відомо від кого)
				return $this->renderJsonError('Сan not check request');
			
			if (!$config->isStatusEnabled())																			// Якщо статуси вимкнені
				return $this->renderJsonError('Status disabled');
			
			/**@var $handler \app\modules\apilead\common\adv\handlers\base\BaseStatusHandler*/
			$handler = Yii::createObject([
				'class' 		=> $config->getStatusHandler(),
				'config'  		=> $config,
				'orderModel'	=> $orderModel,
			]);
			
			$handler->updateStatusInSystem();
			
//			print_r($apileadStatusDataModel->getProperties());															// Виводиво інформацію про замовлення в консоль
		} catch (OrderErrorException $e) {
			$message 	= $e->getMessage();
			$file 		= $e->getFile();
			$line 		= $e->getLine();
			$trace		= $e->getTraceAsString();
			
			$orderId	= $e->getOrderId();
			$text		= "{$message} \nfile: {$file}\nline: {$line}\n{$trace}";
			$category	= __METHOD__;
			
			Error::add($orderId, $text, $category);
		} catch (Exception $e) {
			$message 	= $e->getMessage();
			$file 		= $e->getFile();
			$line 		= $e->getLine();
			$trace		= $e->getTraceAsString();
			
			$orderId	= $orderModel->id ?? null;
			$text		= "{$message} \nfile: {$file}\nline: {$line}\n{$trace}";
			$category	= __METHOD__;
			
			Log::error($text, $orderId, $category);
		} finally {
			$orderModel->stopProcess();
		}
	}
	
	public function actionError()
	{
		$exception 	= Yii::$app->errorHandler->exception;
		$message 	= $exception->getMessage();
		$code 		= $exception->statusCode;
		$type 		= $exception->getCode();
		$file 		= $exception->getFile();
		$line 		= $exception->getLine();
		$trace 		= $exception->getTraceAsString();

//		$this->getBehavior('log')->writeError("\n[CODE: \t\t{$code}]\n[TYPE: \t\t{$type}]\n[MESSAGE: \t{$message}]\n[FILE: \t\t{$file}]\n[LINE: \t\t{$line}]\n[TRACE_BEGIN]\n{$trace}\n[TRACE_END]\n");

		return $this->renderJsonError($message, $code);
	}
	
	public function beforeAction($action)
	{
		if ($action->id === 'index')
			$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}
}
