<?php

namespace app\modules\apilead;

use yii\helpers\VarDumper;
use Yii;
use yii\web\ErrorHandler;

class Module extends \yii\base\Module
{
	/**
	 * @inheritdoc
	 */
//	public $controllerNamespace = 'app\modules\apilead\controllers';

//	public $defaultRoute = 'adv';
//	public $layout = 'main';

	public $disabled = false;

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();
		
		Yii::configure($this, [																							// Потрібно, коли звернення йде до модуля, коли звернення йде вже до контролера і екшина це не працює, тому там потрібно встановлювати обробник знову
			'components' => [
				'errorHandler' => [
					'class' => ErrorHandler::class,
					'errorAction'=>'apilead/adv/bridge/error',
				]
			],
		]);

		/** @var ErrorHandler $handler */
		$handler = $this->get('errorHandler');
		Yii::$app->set('errorHandler', $handler);
		$handler->register();
	}
}
