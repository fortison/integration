<?php
namespace app\modules\apilead\handlers\web\monsterleads;

use app\modules\apilead\common\web\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\helpers\Json;
use yii\web\HttpException;

class MonsterleadsBridgeHandler extends BaseBridgeHandler
{
	public function send()
	{
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		
		$incomingModel	= $this->incomingModel;
		
		$status			= $this->getRelatedStatus($incomingModel->status);

		$requestUrl	= $config->getPostbackUrl() . '?' . http_build_query([
			'api_key'	=> $configUser->apiKey,
			'format'	=> 'json',
		]);
		
		$requestData = [
			'api_key'	=> $configUser->apiKey,
			'lead_key'	=> $incomingModel->sub_id,
			'status'	=> $status,
			'comments'	=> $incomingModel->comment,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestData($requestData);

		$response	= $this->bridgeRequest($prepareRequest);
		
		if ($response->error)
			throw new HttpException('400', "{$response->error} [USER:{$incomingModel->user_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $response->getCode();
		$responseContent	= $response->getContent();
		
		if ($responseCode !== '200')
			throw new HttpException('400', "Partner error 1! Response code: {$responseCode}. Response: {$responseContent}");
		
		$json = $this->decode($responseContent);

		if (!$json->status === 'ok')
			throw new HttpException('400', "Partner error 2! Response code: {$responseCode}. Response: {$responseContent}");
	}
}