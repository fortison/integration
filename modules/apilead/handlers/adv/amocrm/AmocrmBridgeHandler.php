<?php
namespace app\modules\apilead\handlers\adv\amocrm;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\models\forms\ResponseModel;

class AmocrmBridgeHandler extends BaseBridgeHandler
{
	/**
	 * @inheritdoc
	 */
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		
		$incomingModel	= $this->incomingModel;
		
		$args = [$incomingModel, $config];
		
		$client		= new Client([
			'config'		=> $config,
		]);
		
//		exit('<pre>' . print_r(json_decode($client->getAccount()->content), true));
		
		$partnerOrderId		= $client->leadAdd($incomingModel);
		
		$this->response		= new ResponseModel;
		$this->requestUrl	= (string) $config->getUrlOrderAdd();
		$this->requestData	= 'Sent via client. See bridge log.';
		
		$orderAttributes = $this->getOrderAttributes($partnerOrderId);
		
		return $this->saveOrder($orderAttributes);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($partnerOrderId)
	{
		return ['partner_order_id' => (string) $partnerOrderId];
	}
}