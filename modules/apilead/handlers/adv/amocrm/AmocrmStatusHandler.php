<?php
namespace app\modules\apilead\handlers\adv\amocrm;

use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\handlers\base\BaseStatusHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\models\forms\ResponseModel;
use app\modules\apilead\common\objects\PrepareGetRequest;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use Yii;

class AmocrmStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$apileadStatusDataModel = $this->getSystemStatusData($responseModel);

		$needSendStatus = $this->orderModel->updateOrder($apileadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToApilead($apileadStatusDataModel, $needSendStatus);
		
		return $apileadStatusDataModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function makeStatusRequest()
	{
		$orderModel = $this->orderModel;
		
		$client = new Client([
			'config' => $this->config,
		]);
		
		return $client->leadInfo($orderModel->partner_order_id);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$partnerId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");
		
		$contentObject	= $this->decode($responseModel->content);
		
		if (!isset($contentObject->response->leads[0]))
			throw new OrderErrorException($orderId, "Wrong request {$partnerId}:{$apileadId}");
		
		$order = $contentObject->response->leads[0];
		
		if (!isset($order->status_id))
			throw new OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$apileadId}");
		
		$comment		= '';
		$partnerStatus 	= $order->status_id;
		$apileadStatus	= $this->getApileadStatusName($partnerStatus);
		
		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($partnerId);

		return $apileadStatusDataModel;
	}
}