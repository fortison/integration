<?php
/**
 * Created by PhpStorm.
 * User: nagard
 * Date: 26.03.18
 * Time: 15:48
 */

namespace app\modules\apilead\handlers\adv\amocrm;

use app\modules\apilead\common\objects\PrepareGetRequest;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use app\modules\apilead\common\traits\{
	JsonTrait,
	LogTrait,
	RequestTrait
};
use yii\base\BaseObject;
use yii\web\HttpException;

/**
 * @property \app\modules\apilead\common\adv\objects\Config $config Головна конфігурація поточного замовлення
 */
class Client extends BaseObject
{
	use RequestTrait;
	use LogTrait;
	use JsonTrait;
	
	public $config;																										// головна конфігурація замовлення
	
	public $cookie;
	
	public function __construct(array $config)
	{
		parent::__construct($config);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		$subDomain		= $configUser->subDomain;
		
		$requestUrl = "https://{$subDomain}.amocrm.ru/private/api/auth.php?type=json";
		$requestData = [
			'USER_LOGIN'=>$configUser->userLogin, 	#Ваш логин (электронная почта)
			'USER_HASH'=>$configUser->userHash 		#Хэш для доступа к API (смотрите в профиле пользователя)
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestData($requestData);
		
		$response	= $this->bridgeRequest($prepareRequest);
		
		if (!empty($response->error))
			throw new HttpException('405', "CURL: {$response->error}");
		
		$responseCode		= $response->getCode();
		$responseContent	= $response->getContent();
		
		if ($responseCode != 200 && $responseCode != 204)
			throw new HttpException('400', "Partner error! Unauthorized! Code: {$responseCode}. Response: {$responseContent}");
		
		$this->cookie = $response->getCookie();
		
//		exit(print_r($this->decode($this->getAccount()->content), true));
	}
	
	public function leadAdd($incomingModel)
	{
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		$subDomain		= $configUser->subDomain;
		
		$args = [$incomingModel, $config];

		$requestUrl = "https://{$subDomain}.amocrm.ru/private/api/v2/json/leads/set";
		$requestData['request']['leads']['add'] = [
			[
				'name'		=> $configOffer->url,
				'status_id'	=> $configUser->fieldFirstStatus, // Перша угода повинна мати початковий статус (Перший статус з переліку)
				'price'		=> $incomingModel->landing_cost,
				'pipeline_id' => $configOffer->pipelineId ?? '',
				'tags' 		=> $configOffer->tags ?? '',
			]
		];

		foreach ($configOffer->customFields ?? [] as $index => $customField) {
			$customFieldId = $customField['id'] ?? null;
			$customFieldValues = $customField['values'] ?? [];

			if (!$customFieldId || !$customFieldValues) {
				continue;
			}

			$requestData['request']['leads']['add'][0]['custom_fields'][$index]['id'] = $customFieldId;

			foreach ($customFieldValues as $valueIndex => $customFieldValue) {
				$value = $customFieldValue['value'] ?? null;

				if (!$value) {
					continue;
				}

				if (is_callable($value)) {
					$value = call_user_func_array($value, $args);
				}

				$requestData['request']['leads']['add'][0]['custom_fields'][$index]['values'][$valueIndex]['value'] = $value;

				$subtype = $customFieldValue['subtype'] ?? null;
				if ($subtype) {
					if (is_callable($subtype)) {
						$subtype = call_user_func_array($subtype, $args);
					}

					$requestData['request']['leads']['add'][0]['custom_fields'][$index]['values'][$valueIndex]['subtype'] = $subtype;
				}
			}
		}
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestData($requestData);
		$prepareRequest->setRequestCookies($this->cookie);
		
		$response	= $this->bridgeRequest($prepareRequest);
		
		if (!empty($response->error))
			throw new HttpException('400', "{$response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode		= $response->getCode();
		$responseContent	= $response->getContent();
		
		if ($responseCode != 200 && $responseCode != 204)
			throw new HttpException('400', "Partner error! Order not set! Code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getPartnerOrderId($responseContent);
		
		$requestUrl = "https://{$subDomain}.amocrm.ru/private/api/v2/json/contacts/set";
		$contacts['request']['contacts']['add'] = [
			[
				'name' => $incomingModel->name,
				'linked_leads_id' => [$partnerOrderId],
//				'company_name'=>'amoCRM',
				'custom_fields' => [
					[
						'id' => $configUser->fieldPhoneContact,
						'values' => [
							[
								'value' => $incomingModel->phone,
								'enum' => 'OTHER'
							],
						]
					]
				]
			]
		];
		$requestData = $contacts;
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestData($requestData);
		$prepareRequest->setRequestCookies($this->cookie);
		
		$response	= $this->bridgeRequest($prepareRequest);
		
		if (!empty($response->error))
			throw new HttpException('405', "CURL: {$response->error}");
		
		$responseCode		= $response->getCode();
		$responseContent	= $response->getContent();
		
		if ($responseCode != 200 && $responseCode != 204)
			throw new HttpException('400', "Partner error! Contact not set! Code: {$responseCode}. Response: {$responseContent}");
		
		return $partnerOrderId;
	}
	
	public function leadInfo($partnerId)
	{
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		$subDomain		= $configUser->subDomain;
		
		$requestUrl = "https://{$subDomain}.amocrm.ru/private/api/v2/json/leads/list?" . http_build_query(['id' => $partnerId]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestCookies($this->cookie);
		
		return $this->statusRequest($prepareRequest);
	}
	
	public function getAccount()
	{
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		$subDomain		= $configUser->subDomain;
		
		$requestUrl = "https://{$subDomain}.amocrm.ru/private/api/v2/json/accounts/current";
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestCookies($this->cookie);
		
		return $this->statusRequest($prepareRequest);
	}
	
	public function getPartnerOrderId($responseContent)
	{
		$responseLeadObject = $this->decode($responseContent);
		
		if (!isset($responseLeadObject->response->leads->add[0]->id))
			throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
		
		return $responseLeadObject->response->leads->add[0]->id;
	}
}