<?php
namespace app\modules\apilead\handlers\adv\lagoon;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class LagoonBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$phone = str_replace(['-', '+', '(', ')', ' '], '', $incomingModel->phone);
		
		$requestData = [
			'method' => 'addRequest',
			'id' => $incomingModel->id,
			'pers_info' => [
				'fio' => $incomingModel->name,
				'phone' => $phone,
			],
			'id1' => $incomingModel->id,
			'id2' => $incomingModel->wHash ?? '',
		];
		
		if (!empty($configUser->user_id)) {
			$requestData['user_id'] = $configUser->user_id;
		}
		
		if (!empty($configUser->api_key)) {
			$requestData['api_key'] = $configUser->api_key;
		}
		
		if (!empty($configOffer->flow)) {
			$requestData['flow'] = $configOffer->flow;
		}
		
		if (!empty($configOffer->offer_id)) {
			$requestData['offer_id'] = $configOffer->offer_id;
		}
		
		$this->requestUrl  = $config->getUrlOrderAdd();
		$this->requestData = $requestData;
		
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$id = $json->order_num ?? null;
		
		
		if (!empty($id))
			return ['partner_order_id' => (string) $id];
		
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}