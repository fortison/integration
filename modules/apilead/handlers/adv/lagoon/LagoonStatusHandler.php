<?php
namespace app\modules\apilead\handlers\adv\lagoon;

use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\handlers\base\BaseStatusHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\models\forms\ResponseModel;
use app\modules\apilead\common\objects\PrepareGetRequest;
use app\modules\apilead\common\objects\PreparePostRequestJson;

class LagoonStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$apileadStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($apileadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToApilead($apileadStatusDataModel, $needSendStatus);
		
		return $apileadStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		
		$orderModel = $this->orderModel;
		
		$requestUrl = $config->getUrlOrderInfo();
		$requestData = [
			'method' => 'getStatuses',
			'api_key' => (string) $configUser->api_key,
			'order_nums' => [$orderModel->partner_order_id],
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestData($requestData);
		
		return $this->statusRequest($prepareRequest);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$partnerId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content = $responseModel->content;
		$orderArr = $this->decode($content);
		
		if (isset($orderArr->error))
			throw new OrderErrorException($orderId, "Partner error! Response code: {$partnerId}:{$apileadId}");
		
		if (empty($orderArr[0]))
			throw new OrderErrorException($orderId, "Partner error! Response code: {$partnerId}:{$apileadId}");
		
		$order = $orderArr[0];
		
		if (empty($order->status))
			throw new OrderErrorException($orderId, "Partner error! Response code: {$partnerId}:{$apileadId}");
		
		$partnerStatus 		= (string) $order->status;
		$apileadStatus		= $this->getApileadStatusName($partnerStatus);
		$comment			= '';
		
		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($partnerId);
		
		return $apileadStatusDataModel;
	}
}