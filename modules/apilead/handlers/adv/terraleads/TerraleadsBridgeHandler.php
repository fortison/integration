<?php
namespace app\modules\apilead\handlers\adv\terraleads;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\helpers\Json;
use yii\web\HttpException;

class TerraleadsBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$requestData = [
			'user_id' => $configUser->userId,
			'data' => [
				'name' => $incomingModel->name,
				'phone' => $incomingModel->phone,
				'offer_id' => $configOffer->offerId,
				'country' => $incomingModel->country,
				'address' => $incomingModel->address,
				'email'	  => $incomingModel->email,
				'zip'	  => $incomingModel->zip,
				'city'	  => $incomingModel->geo_ip_city,
			]
		];
		
		$this->requestData = Json::encode($requestData);
		
		$checkSum = sha1($this->requestData . $configUser->apiKey);
		
		$this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
			'check_sum' => $checkSum,
		]);
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($requestData);
		$prepareRequest->setRequestContent($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error) {
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		}

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status = $json->status ?? null;
		$id		= $json->data->id ?? null;
		
		if ($status === 'ok')
			return ['partner_order_id' => (string) $id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}