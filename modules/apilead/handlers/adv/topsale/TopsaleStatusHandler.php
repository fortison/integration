<?php
namespace app\modules\apilead\handlers\adv\topsale;

use app\common\models\OrderApilead;
use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\handlers\base\BaseStatusHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\models\forms\ResponseModel;
use app\modules\apilead\common\objects\PrepareGetRequest;

class TopsaleStatusHandler extends BaseStatusHandler
{
	/**
	 * @inheritdoc
	 */
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$apileadStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($apileadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToApilead($apileadStatusDataModel, $needSendStatus);
		
		return $apileadStatusDataModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function makeStatusRequest()
	{
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		
		$orderModel = $this->orderModel;
		
		$requestUrl = $config->getUrlOrderInfo() . "?" . http_build_query([
				'pid'	=> $configUser->pid,
				'id'	=> $orderModel->partner_order_id,
			]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		
		return $this->statusRequest($prepareRequest);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$partnerId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$order = $this->decode($responseModel->content);
		
		if (!isset($order->status))
			throw new OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$apileadId}");
		
		if ($order->status == 'false')
			throw new OrderErrorException($orderId, "Partner status false {$partnerId}:{$apileadId}");
		
		if ($order->order_id != $partnerId)
			throw new OrderErrorException($orderId, "Wrong partner id {$partnerId}:{$apileadId}");
		
		$partnerStatus 	= (string) $order->status;
		$apileadStatus	= $this->getApileadStatusName($partnerStatus);
		$comment		= 'Site: ' . $order->site . '. Date order: '. $order->date_order . '.';
		
		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($partnerId);
		
		return $apileadStatusDataModel;
	}
}