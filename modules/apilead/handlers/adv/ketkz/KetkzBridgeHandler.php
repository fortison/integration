<?php
namespace app\modules\apilead\handlers\adv\ketkz;

use app\common\models\OrderApilead;
use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\web\HttpException;

class KetkzBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;

		$this->requestUrl	= $config->getUrlOrderAdd() . '?' . http_build_query(['uid' => $configUser->uid]);
		$this->requestData	= [
			'data' => Json::encode([
				'phone' => $incomingModel->phone,							// номер телефона заказчика товара (* Обязательное поле)
				"price" => $incomingModel->landing_cost,                   	// стоимость товара
				"order_id" => $incomingModel->id,              				// ID заявки для синхронизации
				"name" => $incomingModel->name,             				// Имя клиента
				'country' => $incomingModel->country,						// двухбуквенный (ISO 3166-1 alpha-2) код страны в нижнем регистре (* Обязательное поле)
				"offer" => "$configOffer->offer - 1",           			// наименование товара (* Обязательное поле)
				"secret" => $configUser->secret,                 			// ключ API (* Обязательное поле)
				"web" => $incomingModel->uHash ?: $incomingModel->wHash, 	// id веба который залил заявку (если такой есть),
				"status" => 0,
				"client_id" => 5,
			]),
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$success = $json->result->success ?? null;
		
		if ($success === 'TRUE')
			return ['partner_order_id' => (string) $json->result->id];
		
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}