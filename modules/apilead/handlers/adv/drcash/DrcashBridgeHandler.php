<?php
namespace app\modules\apilead\handlers\adv\drcash;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class DrcashBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'stream_code' 	=> (string) $configOffer->stream_code,
			'client' => [
				'name' => (string) $incomingModel->name,
				'phone' => (string) $incomingModel->phone,
			],
			'sub1' => (string) $incomingModel->id,
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestHeaders(
			[
				'Content-Type' => 'application/json',
				'Authorization' => 'Bearer ' . $configUser->key
			]
		);
		$prepareRequest->setRequestContent(json_encode($this->requestData));
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$partner_order_id = $json->uuid ?? null;
		
		if ($partner_order_id)
			return ['partner_order_id' => (string) $partner_order_id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}