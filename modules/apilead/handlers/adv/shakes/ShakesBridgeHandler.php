<?php

namespace app\modules\apilead\handlers\adv\shakes;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;
use Yii;

class ShakesBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;

		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$this->requestData = [
			"name"			=> $incomingModel->name,
			"phone"			=> $incomingModel->phone,
			"countryCode"	=> $incomingModel->country,
			"offerId"		=> $configOffer->offerId,
			"landingUrl"	=> $configOffer->landingUrl,
			"createdAt"		=> Yii::$app->formatter->asDatetime('now', 'Y-MM-dd H:m:s'),
//			"streamCode"	=> $incomingModel->wHash,
			"streamCode"	=> $configOffer->streamCode,
			"userAgent"		=> $incomingModel->user_agent ?: 'Mozilla/5.0 (Linux; Android 4.2.2; Lenovo A526 Build/JDQ39) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 YaBrowser/13.9.1500.3524 Mobile Safari/537.36',
			"ip"			=> $incomingModel->ip,
			"sub1"		=> $incomingModel->id,
			"sub2"		=> $incomingModel->user_id,
		];

		$this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
			'r'		=> 'api/order/in',
			'key'	=> $configUser->apiKey,
		]);
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$success	= $json->status ?? null;
		$id			= $json->response ?? null;
		
		if ($success === 'ok')
			return ['partner_order_id' => (string) $id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}