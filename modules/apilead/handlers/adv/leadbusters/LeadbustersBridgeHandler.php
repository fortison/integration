<?php
namespace app\modules\apilead\handlers\adv\leadbusters;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class LeadbustersBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;
		
		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$this->requestData = [
			'phone'        => $incomingModel->phone,
			'name'         => $incomingModel->name,
			'x_ip'		   => $incomingModel->ip ?? '127.0.0.1',
			'x_ua'		   => $incomingModel->user_agent ?? 'Mozilla/5.0',
			'x_url'		   => $configOffer->x_url,
			'subid1'       => $incomingModel->id,
			'subid2'       => $incomingModel->uHash ?: $incomingModel->wHash,
			'other'		   => [
				'address1' => $incomingModel->address ?? '',
				'country'  => $incomingModel->country,
			],
			
		];
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode < 200 && $responseCode > 210)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		if ($responseContent == '"Ok"')
			return [];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}