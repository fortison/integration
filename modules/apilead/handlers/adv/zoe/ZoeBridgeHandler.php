<?php
namespace app\modules\apilead\handlers\adv\zoe;

use app\common\models\OrderApilead;
use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\objects\PrepareGetRequest;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use yii\httpclient\Client;
use yii\web\HttpException;

class ZoeBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$address = $this->decode($incomingModel->address);
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData = [
//			'lastname' => $incomingModel->name,
			"name" 	   => $incomingModel->name,
			"phone"	   => $incomingModel->phone,
			"price1"   => $incomingModel->landing_cost,
			"street"   => $address->poulicni,
			"house"    => $address->number,
			'city'     => $address->mesto,
			"zip"      => $address->psc,
			"email"    => $address->email,
			"zoe"      => $configOffer->zoe,
			"country"  => mb_strtolower($incomingModel->country),
		];
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		print_r($responseContent);
		if ($responseContent != 'success insert') {
			$incomingModel = $this->incomingModel;
			$apiKey = $this->config->user->getApiKey();
			$errorMessage = $responseContent ?? '';
			
			$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
			$apileadStatusDataModel->setId($incomingModel->id);
			$apileadStatusDataModel->setStatus(OrderApilead::STATUS_TRASH);
			$apileadStatusDataModel->setComment($errorMessage);
			
			$requestUrl = $this->config->getUrlApileadOrderUpdate();
			$requestUrl = $requestUrl . '?' . http_build_query(['check_sum' => $apileadStatusDataModel->getCheckSum()]);
			$requestData = $apileadStatusDataModel->getStatusData();
			
			$prepareRequest = new PreparePostRequestJson();
			$prepareRequest->setRequestUrl($requestUrl);
			$prepareRequest->setRequestData($requestData);
			$prepareRequest->setResponseFormat(Client::FORMAT_JSON);
			
			$responseModel = $this->statusRequest($prepareRequest);
			
			if (!$responseModel->error && $responseModel->code == 200)
				return ['system_status' => OrderApilead::STATUS_TRASH];
			
			exit($responseModel->content);
			
		} else {
			return ['system_status' => OrderApilead::STATUS_CONFIRM];
		}
	}
}