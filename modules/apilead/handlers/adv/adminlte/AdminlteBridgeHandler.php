<?php
namespace app\modules\apilead\handlers\adv\adminlte;

use app\common\models\OrderApilead;
use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\objects\PreparePostRequest;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use yii\httpclient\Client;
use yii\web\HttpException;

class AdminlteBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestData	= [
			'id'    	 => $incomingModel->id, 				//Уникальный идентификатор заказа в рамках агентства (обязательный параметр)
			'wm'         => $incomingModel->getWHash(),         // 	Идентификатор вебмастера или другого источника на стороне агентства
			'offer'      => $configOffer->offer,             	// https://healthpower.pro/help/api.php#offers
			'ip'    	 => $incomingModel->ip,                	// ip пользователя
			'name' 		 => $incomingModel->name,
			'phone' 	 => $incomingModel->phone,
			'email' 	 => $incomingModel->email ?? '',
			'comm'		 => $incomingModel->user_comment,
		];
		
		$this->requestUrl = $config->getUrlOrderAdd() . '?id=' . $configUser->user . '-' . $configUser->key;
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestHeaders(['content-type' => 'application/x-www-form-urlencoded']);
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status = $json->status ?? '';
		
		if ($status == 'ok')
			return ['partner_order_id' => (string) $json->id];
		
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}