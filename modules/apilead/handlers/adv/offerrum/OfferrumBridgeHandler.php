<?php
namespace app\modules\apilead\handlers\adv\offerrum;

use app\common\models\OrderApilead;
use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\objects\PreparePostRequest;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use yii\httpclient\Client;
use yii\web\HttpException;

class OfferrumBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestData	= [
			'country'    => $incomingModel->country ?? null,   // страна доставки, если не будет передана - будет определена по IP адресу
			'fio'        => $incomingModel->name,              // Имя
			'phone'      => $incomingModel->phone,             // Телефон
			'user_ip'    => $incomingModel->ip,                // ip пользователя
			'user_agent' => $incomingModel->user_agent,        // UserAgent пользователя
			'order_time' => time(),             			   // timestamp времени заказа
		];
		
		$baseUrl = str_replace('{domain}', $configUser->domain, $config->getUrlOrderAdd());
		$this->requestUrl = $baseUrl . '?'
			. http_build_query([
				'key'	=> $configUser->key,
				'flow'	=> $configOffer->flow,
				'subid'	=> "{$incomingModel->id}:{$incomingModel->user_id}:::" . ($incomingModel->uHash ?: $incomingModel->wHash),
			]);
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestHeaders(['content-type' => 'application/x-www-form-urlencoded']);
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$response = $json->response;
		
		$status				= $response->status ?? null;
		$additionalStatus	= $response->additional_status ?? null;
		
		if ($additionalStatus >= 4) {

			$incomingModel	= $this->incomingModel;
			$apiKey     	= $this->config->user->getApiKey();
			$errorMessage	= $json->error_msg ?? '';

			$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
			$apileadStatusDataModel->setId($incomingModel->id);
			$apileadStatusDataModel->setComment($errorMessage);
			$apileadStatusDataModel->setStatus(OrderApilead::STATUS_TRASH);

			$requestUrl		= $this->config->getUrlApileadOrderUpdate();
			$requestData    = $apileadStatusDataModel->getStatusData();

			$prepareRequest = new PreparePostRequestJson();
			$prepareRequest->setRequestUrl($requestUrl);
			$prepareRequest->setRequestData($requestData);
			$prepareRequest->setResponseFormat(Client::FORMAT_JSON);

			$responseModel = $this->statusRequest($prepareRequest);

			if (!$responseModel->error && $responseModel->code == 200)
				return ['system_status'=>OrderApilead::STATUS_TRASH];

			exit($responseModel->content);
		}

		if ($status == 'ok')
			return ['partner_order_id' => (string) $response->order_id];
		
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}