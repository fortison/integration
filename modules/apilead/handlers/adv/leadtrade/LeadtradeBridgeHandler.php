<?php
namespace app\modules\apilead\handlers\adv\leadtrade;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class LeadtradeBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestData = [
			'lead'			=> "leadtrade.{$incomingModel->wHash}",
			'subid'			=> $incomingModel->id,
			'partner'		=> $configUser->partner,
			'api_key'		=> $configUser->apiKey,
			'webmaster_id'	=> $incomingModel->wHash,
			'domain'		=> "{$configOffer->offerId}.api.com", // обов'язково
			'data'			=> 	[
				'name'			=> $incomingModel->name,
				'index'			=> '',
				'adress'		=> $incomingModel->address ?: '',
				'phone'			=> $incomingModel->phone,
				'country'		=> $incomingModel->country,
				'city'			=> '',
				'ip'			=> $incomingModel->ip,
				'utc' 			=> '',
				'productsum'	=> $incomingModel->landing_cost, // or $model->cost
				'delivery'		=> $incomingModel->cost_delivery,
				'totalsum'		=> $incomingModel->landing_cost + $incomingModel->cost_delivery,
				'currency'		=> $incomingModel->landing_currency,
				'quantity'		=> 1, // кількість
				'dateadd'		=> '',
			]
		];
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status = $json->status ?? null;
		$id		= $json->number ?? null;
		
		if ($status === true)
			return ['partner_order_id' => (string) $id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}