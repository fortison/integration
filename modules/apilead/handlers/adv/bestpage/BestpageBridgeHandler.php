<?php

namespace app\modules\apilead\handlers\adv\bestpage;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class BestpageBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
				'api_key' => $configUser->api_key,
			]);
		
		$this->requestData	= [
			'name' 					=> $incomingModel->name,
			'phone' 				=> $incomingModel->phone,
			'product' 				=> $configOffer->product,
			'utm_term' 				=> $configOffer->utm_term,
			'address' 				=> $incomingModel->address ?? '',
			'comment' 				=> $incomingModel->user_comment ?? '',
			'externalWebmaster' 	=> $incomingModel->getWHash(),
			'price' 				=> $incomingModel->landing_cost,
			'payout' 				=> $configOffer->payout,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		$prepareRequest->setRequestHeaders(['Content-Type' => 'application/x-www-form-urlencoded']);
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	public function getOrderAttributes($responseContent)
	{
		
		$json = $this->decode($responseContent, true);
		if (!is_array($json)) {
			$json = $this->decode($json, true);
		}
		
		if (current($json) === 'OK') {
			return [
				'partner_order_id' => (string) key($json),
			];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}
