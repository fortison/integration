<?php


namespace app\modules\apilead\handlers\adv\bestpage;


use app\modules\apilead\common\adv\handlers\base\BaseStatusHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\models\forms\ResponseModel;
use app\modules\apilead\common\objects\PrepareGetRequest;

class BestpageStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$apileadStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($apileadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToApilead($apileadStatusDataModel, $needSendStatus);
		
		return $apileadStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		
		$orderModel = $this->orderModel;
		
		$requestUrl = $config->getUrlOrderInfo() . '?' . http_build_query(['ids' => $orderModel->partner_order_id, 'api_key' => $configUser->api_key]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		
		return $this->statusRequest($prepareRequest);
	}
	
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$partnerId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content		= $responseModel->content;
		$contentObject	= $this->decode($content);
		
		if (empty($contentObject->{$orderModel->partner_order_id})){
			throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
		}
		
		$order = $contentObject->{$orderModel->partner_order_id};
		
		if (!$order) {
			throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
		}
		
		$partnerStatus 		= $order->status ?? null;
		$apileadStatus		= $this->getApileadStatusName($partnerStatus);
		
		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($partnerId);
		
		return $apileadStatusDataModel;
	}
}