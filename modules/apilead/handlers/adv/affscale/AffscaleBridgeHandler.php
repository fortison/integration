<?php
namespace app\modules\apilead\handlers\adv\affscale;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class AffscaleBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestData	= [
			'goal_id'      => $configOffer->goal_id,
			'sub_id1'      => $incomingModel->getWHash(),
			'lastname' 	   => $incomingModel->name,
			'phone' 	   => $incomingModel->phone,
			'email' 	   => $incomingModel->email ?? '',
			'api-key'	   => $configUser->api_key,
			'aff_click_id' => $incomingModel->id,
		];
		
		$this->requestUrl = $config->getUrlOrderAdd() . '?api-key=' . $configUser->api_key;
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestHeaders(['content-type' => 'Content-Type: application/json']);
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status = $json->status ?? '';
		
		if ($status == 'success')
			return ['partner_order_id' => (string) $json->info->lead_id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}