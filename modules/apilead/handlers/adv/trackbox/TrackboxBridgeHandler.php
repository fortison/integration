<?php
namespace app\modules\apilead\handlers\adv\trackbox;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class TrackboxBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$name_arr = preg_split("/[\s,]+/", $incomingModel->name);
		$firstname = empty($name_arr[0]) ? 'Empty' : $name_arr[0];
		$lastname = empty($name_arr[1]) ? 'Empty' : $name_arr[1];
		
		$this->requestData	= [
			'ai'			=> $configOffer->ai,
			'ci'			=> $configOffer->ci,
			'gi'			=> $configOffer->gi,
			'userip'		=> $incomingModel->ip,
			'firstname'		=> $firstname,
			'lastname'		=> $lastname,
			'email'			=> $incomingModel->address ?? 'sometestmail@gmail.com',
			'password'		=> $configUser->password,
			'phone'			=> $incomingModel->phone,
			'so'			=> $incomingModel->user_comment ?? $configOffer->land_url,
			'sub'			=> $incomingModel->getWHash(),
			'MPC_1'			=> $incomingModel->id,
			'MPC_2'			=> $configOffer->MPC_2,
		];
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestHeaders([
			'content-type' 			=> 'Content-Type: application/json',
			'x-trackbox-username' 	=> $configUser->username,
			'x-trackbox-password' 	=> $configUser->password,
			'x-api-key' 			=> $configUser->api_key,
			]);
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = (object)$this->decode($responseContent);
		
		$status_code = $json->status ?? '';
		
		if ($status_code == true)
			return ['partner_order_id' => (string) $json->addonData->data->id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}