<?php
namespace app\modules\apilead\handlers\adv\cpahouse;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class CpahouseBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		
		$this->requestData	= [
			'api_key' 		=> $configUser->api_key,
			'id_webmaster' 	=> $configUser->id_webmaster,
			'name' 			=> $incomingModel->name,
			'phone' 		=> $incomingModel->phone,
			'id_offer' 		=> $configOffer->id_offer,
			'id_source' 	=> $configOffer->id_source,
			'id_stream' 	=> $configOffer->id_stream,
			'user_agent' 	=> $incomingModel->user_agent,
			'ip_address' 	=> $incomingModel->ip,
			'county_code' 	=> $incomingModel->country,
			'sub_id_1' 		=> $incomingModel->id,
			'sub_id_2' 		=> $incomingModel->getWHash(),
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		
		$status	= $json->result ?? null;
		
		if ($status === true) {
			return ['partner_order_id' => (string) $json->id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}