<?php
namespace app\modules\apilead\handlers\adv\monstercall;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class MonstercallBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;

		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		$args 			= [$incomingModel, $config];
		
		$this->requestData = [
			"partnerId"					=> $configUser->partnerId,
			"Name"						=> $incomingModel->name,
			"Phone"						=> $incomingModel->phone,
			"ProcessingType"			=> $configOffer->processingType,
			"TimeZone"					=> $incomingModel->tz,
			"ProductUpsellPackageId"	=> $configOffer->productUpsellPackageId,
			"IsJsonResult"				=> 'true',
		];
		
		if ($configOffer->products) {
			if (is_callable($configOffer->products)) {
				$products = call_user_func_array($configOffer->products, $args);
			} else {
				$products = $configOffer->products;
			}
			
			$this->requestData['Products'] = $products;
		}

		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($config->user->configs['urlOrderUpdate']);
		$prepareRequest->setRequestData([
			'callId' => $partnerOrderId['partner_order_id'],
			'reffId' => $configUser->reffId,
		]);
		$this->bridgeRequest($prepareRequest);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json	= $this->decode($responseContent);
		$id		= $json->OrderId ?? null;
		
		if ($id) {
			return ['partner_order_id' => (string) $id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}