<?php
namespace app\modules\apilead\handlers\adv\telesales;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PrepareGetRequest;
use yii\web\HttpException;

class TelesalesBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist()) {
			return $model;
		}
		
		$incomingModel	= $this->incomingModel;

		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;

		$this->requestUrl = $config->getUrlOrderAdd();
		
		$this->requestData = [
			'api_token'					=> $configUser->apiToken,
			'order_offer_id'			=> $configOffer->order_offer_id,
			'order_client_phone'		=> $incomingModel->phone,
			'order_client_name'			=> $incomingModel->name,
			'order_client_geo_ip'		=> $incomingModel->ip,
//			'order_offer_external_id'	=> $incomingModel->id,
//			'order_cc_process'			=> 1,
			'order_webmaster_id'		=> $configOffer->order_webmaster_id,														# ид рекламодателя в базе КЦ
		];
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$response	= $json->response ?? null;
		$id			= $json->data->order_id ?? null;
		
		if ($response == "success")
			return ['partner_order_id' => (string) $id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}