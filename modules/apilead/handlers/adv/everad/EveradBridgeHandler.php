<?php
namespace app\modules\apilead\handlers\adv\everad;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class EveradBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;

		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
	
		$this->requestData = [
			'name'			=> $incomingModel->name,
			'phone'			=> $incomingModel->phone,
			//  'address' => 'test',
			'country_code'	=> $incomingModel->country,
			'offer_id'		=> $configOffer->offerId,
			'id'			=> $incomingModel->id,
			'partner_id'	=> $incomingModel->wHash,
			'token'			=> $configUser->token,
			'lead_price'	=> $incomingModel->cost,
			'ip'			=> $incomingModel->ip,
		];

		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		if (!$responseContent)
			return [];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}