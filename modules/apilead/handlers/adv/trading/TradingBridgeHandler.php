<?php
namespace app\modules\apilead\handlers\adv\trading;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class TradingBridgeHandler extends BaseBridgeHandler
{
	
	public function generatePassword($length = 8){
		$chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
		$numChars = strlen($chars);
		$string = '';
		for ($i = 0; $i < $length; $i++) {
			$string .= substr($chars, rand(1, $numChars) - 1, 1);
		}
		return $string;
	}
	
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$name_arr = preg_split("/[\s,]+/", $incomingModel->name);
		$firstname = empty($name_arr[0]) ? 'Empty' : $name_arr[0];
		$lastname = empty($name_arr[1]) ? 'Empty' : $name_arr[1];
		
		$this->requestUrl	= $config->getUrlOrderAdd() . '?affiliate=' . $configUser->affiliate . '&token=' . $configUser->api_token;
		
		$this->requestData	= [
			'email' 		=> $incomingModel->address ?? $this->generatePassword(5) . '@mail.test',
			'password' 		=> $incomingModel->user_comment ?? $this->generatePassword(8),
			'firstName' 	=> $firstname,
			'lastName' 		=> $lastname,
			'country' 		=> $incomingModel->country,
			'phone' 		=> $incomingModel->phone,
			'reference' 	=> $incomingModel->getWHash(),
			'description' 	=> $incomingModel->id,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		
		$status	= $json->success ?? null;
		
		if ($status === true) {
			return ['partner_order_id' => (string) $json->result->id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}