<?php
namespace app\modules\apilead\handlers\adv\leadr;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class LeadrBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;
		
		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$this->requestData = [
			'api_key'     => $configUser->api_key,
			'user_id'     => $configUser->user_id,
			'geo'		  => $incomingModel->country,
			'offer_id'	  => $configOffer->offer_id,
			'flow_id'	  => $configOffer->flow_id,
			'ip'          => $incomingModel->ip ?? '127.0.0.1',
			'name'        => $incomingModel->name,
			'phone'       => $incomingModel->phone,
			'comment'     => $incomingModel->address ?? '',
			'user_agent'  => $incomingModel->user_agent ?? 'Mozilla/5.0',
			'referer'     => $configOffer->referer,
			'data1'       => $incomingModel->id,
			'web_id'      => $incomingModel->uHash ?: $incomingModel->wHash,
		];
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode < 200 && $responseCode > 210)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$this->checkResponseInvalidPhone($responseContent, 'duplicate');
		
		$json = $this->decode($responseContent);
		
		$code = $json->status_code ?? null;
		
		if ($code == 200) {
			return ['partner_order_id' => (string) $json->response->id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}