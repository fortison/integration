<?php
namespace app\modules\apilead\handlers\adv\sevenleads;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class SevenleadsBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		
		$this->requestData	= [
			'key'      		=> $configUser->api_key,
			'name' 			=> $incomingModel->name,
			'phone' 		=> $incomingModel->phone,
			'country' 		=> $incomingModel->country,
			'offer_id' 		=> $configOffer->offer_id,
			'products' 		=> $configOffer->products,
			'webmaster_id' 	=> $incomingModel->getWHash(),
			'click_id' 		=> $incomingModel->id,
			'uniqid' 		=> uniqid(),
			'ip' 			=> $incomingModel->ip,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl('http://connect.wowsale.info/backup.php');
		$prepareRequest->setRequestData($this->requestData);
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		
		$status	= $json->error ?? null;
		
		if ($status === 0) {
			return ['partner_order_id' => (string) $json->order_id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}