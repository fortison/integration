<?php
namespace app\modules\apilead\handlers\adv\center;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class CenterBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;

		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$this->requestData = [
			'pass' => $configUser->pass,
			'add' => $this->encode([
				'orders' => [
					[
						'web_id' => $incomingModel->getWHash() ?? '',
						'order_id' => $incomingModel->id,
						'name' => $incomingModel->name,
						'phone' => $incomingModel->phone,
						'ip' => $incomingModel->ip,
						'order_time' => time(),
						'order_source' => $configOffer->orderSource,
						'product_id' => $configOffer->productId,
					],
				],
			]),
		];

		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$result		= $json->success[0] ?? null;
		
		if ($result)
			return ['partner_order_id' => (string) $result->external_id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}