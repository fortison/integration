<?php
namespace app\modules\apilead\handlers\adv\retail\user_10066;

use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class RetailBridgeHandler extends \app\modules\apilead\handlers\adv\retail\RetailBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$args = [$incomingModel, $config];
		
		$order = [
			"externalId"		=> $incomingModel->id,
			"firstName"			=> $incomingModel->name,
			"phone"				=> $incomingModel->phone,
			"ip"				=> $incomingModel->ip,
			"customerComment" 	=> $incomingModel->address,
			"delivery" 			=> [
				"code" => "beta-pro",
			]
//			"items" 			=> [
//				[
//					"offer" 	=> [
//						"id" 	=> $configOffer->externalId,
//					]
//				]
//			],
//			"customFields" => [],
		];
		
		$orderMethod = $configOffer->orderMethod ?? null;
		if ($orderMethod) {
			$order['orderMethod'] = $orderMethod;
		}
		
		$offer = null;
		
		if (isset($configOffer->externalId)) {
			if (is_callable($configOffer->externalId))
				$externalId = call_user_func_array($configOffer->externalId, $args);
			else
				$externalId = $configOffer->externalId;
			
			if ($externalId) {
				$offer['externalId'] = $externalId;
			}
		}
		
		if (isset($configOffer->offerId)) {
			if (is_callable($configOffer->offerId))
				$offerId = call_user_func_array($configOffer->offerId, $args);
			else
				$offerId = $configOffer->offerId;
			
			if ($offerId) {
				$offer['id'] = $offerId;
			}
		}
		
		if ($offer) {
			$order['items'] = [
				[
					'offer' 	=> $offer
				]
			];
		}
		
		if (isset($configOffer->source)) {
			$order['source'] = [
				'source' => $configOffer->source,
			];
		}
		
		if (isset($configOffer->api_leads_aff_id)) {
			$order['customFields']['api_leads_aff_id'] = $incomingModel->uHash ?? $incomingModel->wHash;
		}
		
		if (isset($configOffer->orderFrom)) {
			if (is_callable($configOffer->orderFrom))
				$orderFrom = call_user_func_array($configOffer->orderFrom, $args);
			else
				$orderFrom = $configOffer->orderFrom;
			
			$order['customFields']['orderFrom'] = $orderFrom;
		}
		
//		$data = ["order" => '{"firstName":"test","phone":"380121111111","delivery":{"address":{"text":""},"code":"beta-pro"},"orderType":"eshop-individual","orderMethod":"1380","customFields":{"ip":"188.130.178.115","landing_url":"webshoppingnets.com\\/page\\/8967a5c1cf080e906ae4dc41d76f2663f5e2f9cc\\/","price_for_lead":"1990","country":null,"utm_source":"M1-shop","utm_content":297539,"m1_aff_id":297539,"ad_order_id":"m1_19236398"},"items":[{"initialPrice":"1990","productId":"1380","quantity":1,"offerId":"0"}],"source":{"medium":297539}}'];
		$data = ["order" => $this->encode($order, 256)];
		
		if (isset($configOffer->site)) {
			$data['site'] = $configOffer->site;
		}
		
		$this->requestData = [
			"url"    => $config->getUrlOrderAdd() . '?' . http_build_query(['apiKey' => $configUser->retailApiKey]),
			"token"	 => $configUser->proxy_token,
			"method" => 'POST',
			"data"   => $this->encode($data, 256),
		];

		$this->requestUrl = $configUser->proxy_url;
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestHeaders(['Content-Encoding' => 'UTF-8', 'Connection' => 'close']);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if (!empty($this->response->error))
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 201 && $responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (isset($json->success) && $json->success === true)
			return ['partner_order_id' => (string) $json->id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}
