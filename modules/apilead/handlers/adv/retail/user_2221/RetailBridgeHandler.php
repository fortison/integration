<?php
namespace app\modules\apilead\handlers\adv\retail\user_2221;

use app\modules\apilead\common\objects\PrepareGetRequest;
use yii\web\HttpException;

class RetailBridgeHandler extends \app\modules\apilead\handlers\adv\retail\RetailBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel		= $this->incomingModel;
		
		$args = [$incomingModel, $config];
		
		$this->requestData = [
			'fio'  => $incomingModel->name,
			'phone' => preg_replace('/\D/', '', $incomingModel->phone),
			'goods' => $configOffer->offerId,
			'ip'    => $incomingModel->ip,
			'target' => 'apilead',
			'additional' => $incomingModel->uHash ?: $incomingModel->wHash,
		];

		$this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query($this->requestData);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if (!empty($this->response->error))
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. RESPONSE: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * Повертає ідентифікатор замовлення на стороні партнера.
	 * Потрібен, щоб красиво реалізувати інтеграції рекламодавця 3437
	 * котрий є відгалуженням змичайного leadvertex.
	 *
	 * @param $responseContent
	 * @return mixed Ідентифікатор замовлення на стороні партнера
	 * @throws HttpException
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (!isset($json->responce))
			throw new HttpException('400', "Wrong partner response. {{$responseContent}}");
		
		if ($json->responce != 'success')
			throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
		
		return ['partner_order_id' => (string) $json->id];
	}
}