<?php
namespace app\modules\apilead\handlers\adv\retail\user_2221;

use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\handlers\base\BaseStatusHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\models\forms\ResponseModel;
use app\modules\apilead\common\objects\PrepareGetRequest;
use Yii;

class RetailStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$apileadStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($apileadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToApilead($apileadStatusDataModel, $needSendStatus);
		
		return $apileadStatusDataModel;
	}

	public function makeStatusRequest()
	{
		$requestUrl = $this->config->getUrlOrderInfo() . '?' . http_build_query([
			'method'	=> 'getstatus',
			'target'	=> 'apilead',
			'order_id' 	=> $this->orderModel->partner_order_id,
		]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		
		$data = $this->statusRequest($prepareRequest);

		return $data;
	}

	public function makeRansomStatusRequest():ResponseModel
	{
		$requestUrl = $this->config->getUrlOrderInfo() . '?' . http_build_query([
			'method'	=> 'getransom',
			'target'	=> 'apilead',
			'order_id' 	=> $this->orderModel->partner_order_id,
		]);

		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);

		$data = $this->statusRequest($prepareRequest);

		return $data;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$sourceId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$apileadId}");
		
		$content	= $responseModel->content;
		
		$contentObject = $this->decode($content);
		
		if (!isset($contentObject->responce))
			throw new OrderErrorException($orderId, "Wrong request {$sourceId}:{$apileadId}");
		
		if ($contentObject->responce != 'success')
			throw new OrderErrorException($orderId, "Status FALSE {$sourceId}:{$apileadId}");
		
		if (!isset($contentObject->data[0]))
			throw new OrderErrorException($orderId, "Order not found in response {$sourceId}:{$apileadId}");
		
		$order = $contentObject->data[0];
		
		if (!isset($order->status))
			throw new OrderErrorException($orderId, "Status not found for lead {$sourceId}:{$apileadId}");
		
		if ($callCount = $order->count_rining ?? null) {
			$callCount .= ' ';
		}

		$comment		= $callCount . $order->comment ?? '';
		$partnerStatus 	= $order->status;
		$apileadStatus	= $this->getApileadStatusName($partnerStatus);
		
		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($sourceId);
		
		return $apileadStatusDataModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getRansomStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$sourceId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$apileadId}");
		
		$content	= $responseModel->content;
		
		$contentObject = $this->decode($content);
		
		if (!isset($contentObject->responce))
			throw new OrderErrorException($orderId, "Wrong request {$sourceId}:{$apileadId}");
		
		if ($contentObject->responce !== 'success')
			throw new OrderErrorException($orderId, "Status FALSE {$sourceId}:{$apileadId}");
		
		if (!isset($contentObject->data[0]))
			throw new OrderErrorException($orderId, "Order not found in response {$sourceId}:{$apileadId}");
		
		$order = $contentObject->data[0];
		
		if (!isset($order->ransom))
			throw new OrderErrorException($orderId, "Status not found for lead {$sourceId}:{$apileadId}");
		
		$partnerRansomStatus 	= $order->ransom;
		$apileadRansomStatus	= $this->getRansomStatusName($partnerRansomStatus);
		
		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setStatus($apileadRansomStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerRansomStatus);
		$apileadStatusDataModel->setPartnerId($sourceId);
		
		return $apileadStatusDataModel;
	}
}