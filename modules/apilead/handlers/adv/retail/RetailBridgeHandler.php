<?php
namespace app\modules\apilead\handlers\adv\retail;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class RetailBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$args = [$incomingModel, $config];
		
		$order = [
			"externalId"		=> $incomingModel->id,
			"firstName"			=> $incomingModel->name,
			"phone"				=> preg_replace("/[^0-9]/", "", $incomingModel->phone),
			"ip"				=> $incomingModel->ip,
			"customerComment" 	=> $incomingModel->address,
//			"items" 			=> [
//				[
//					"offer" 	=> [
//						"id" 	=> $configOffer->externalId,
//					]
//				]
//			],
//			"customFields" => [],
		];
		
		$orderMethod = $configOffer->orderMethod ?? null;
		if ($orderMethod) {
			$order['orderMethod'] = $orderMethod;
		}
		
		$offer = null;
		
		if (isset($configOffer->externalId)) {
			if (is_callable($configOffer->externalId))
				$externalId = call_user_func_array($configOffer->externalId, $args);
			else
				$externalId = $configOffer->externalId;
			
			if ($externalId) {
				$offer['externalId'] = $externalId;
			}
		}
		
		if (isset($configOffer->offerId)) {
			if (is_callable($configOffer->offerId))
				$offerId = call_user_func_array($configOffer->offerId, $args);
			else
				$offerId = $configOffer->offerId;
			
			if ($offerId) {
				$offer['id'] = $offerId;
			}
		}
		
		if ($offer) {
			$order['items'] = [
				[
					'offer' 	=> $offer
				]
			];
		}

		if (isset($configOffer->source)) {
			$order['source'] = [
				'source' => $configOffer->source,
			];
		}

		if (isset($configOffer->api_leads_aff_id)) {
			$order['customFields']['api_leads_aff_id'] = $incomingModel->uHash ?? $incomingModel->wHash;
		}
		
		if (isset($configOffer->orderFrom)) {
			if (is_callable($configOffer->orderFrom))
				$orderFrom = call_user_func_array($configOffer->orderFrom, $args);
			else
				$orderFrom = $configOffer->orderFrom;

			$order['customFields']['orderFrom'] = $orderFrom;
		}
		
		$this->requestData = [
			"order" => $this->encode($order),
		];
		
		if (isset($configOffer->site)) {
			$this->requestData['site'] = $configOffer->site;
		}

		$this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
			'apiKey' => $configUser->retailApiKey,
		]);
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if (!empty($this->response->error))
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 201)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (isset($json->success) && $json->success === true)
			return ['partner_order_id' => (string) $json->id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}