<?php
namespace app\modules\apilead\handlers\adv\money4leads;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PrepareGetRequest;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class Money4leadsBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestData = [
			// ID оффера Вашего заказа
			'offerId' => $configOffer->offerId, // Обязательное поле
			// Имя покупателя из заявки
			'fullName' => $incomingModel->name, // Обязательное поле
			// Телефон покупателя из заявки
			'phone' => $incomingModel->phone,  // Обязательное поле
			// Ваш partner ID
			'partnerId' => $configUser->partnerId, // Обязательное поле
			// Обязательно указывайте цену в валюте Вашего гео
			// если у вас 1 рубль на ленде, то поле должно иметь вид  'price' => 1
			// если у вас 5 тенге на ленде, то поле должно иметь вид 'price' => 5
			'price' => $incomingModel->landing_cost, // Обязательное поле
			// Номер старны (1 — Россия, 2 — Беларусь, 3 — Украина, 4 — Казахстан)
			// или 2 символа ISO кода стран (RU, BY, UA, KZ и тд)
			'country' => $incomingModel->country, // Обязательное поле
			// Ваш Token для запросов в API
			'access-token' => $configUser->access_token, // Обязательное поле
			// Доп параметры, максимально 5
			'sub_id' => array($incomingModel->id,$incomingModel->getWHash(),$incomingModel->getUHash()), // Не обязательное поле
			// IP адресс покупателя
			'ip' => $incomingModel->ip, // Не обязательное поле
			// Комментарий к заказу
			'comment' => $incomingModel->user_comment ?? '', // Не обязательное поле
//			'is_rub' => '',
		];
		
		if (!empty($configOffer->splitId)){
			// ID потока // Не обязательное поле
			$this->requestData['splitId'] = $configOffer->splitId;
		}
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (!isset($json->data)){
			throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
		}
		
		$id	= $json->data->orderId ?? null;
		
		if ($id)
			return ['partner_order_id' => (string) $id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}