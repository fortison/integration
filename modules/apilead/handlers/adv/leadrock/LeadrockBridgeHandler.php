<?php

namespace app\modules\apilead\handlers\adv\leadrock;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PrepareGetRequest;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class LeadrockBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config = $this->config;
		$configUser = (object)$config->user->array;
		$configOffer = (object)$config->offer->array;
		
		$incomingModel = $this->incomingModel;
		
		$this->requestData = [
			'flow_url' 		=> $configOffer->flow_url,
			'user_phone' 	=> $incomingModel->phone,
			'user_name' 	=> $incomingModel->name,
			'ip' 			=> $incomingModel->ip,
			'ua' 			=> $incomingModel->user_agent,
			'api_key' 		=> $configUser->key,
			'country' 		=> $incomingModel->country,
			'sub1'			=> $incomingModel->id,
			'ajax' 			=> 1,
		];
		
		$this->requestUrl = $configOffer->flow_url . (strpos($configOffer->flow_url, '?') === false ? '?' : '&') . http_build_query($this->requestData);
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response = $this->bridgeRequest($prepareRequest);
		
		$responseContent = $this->response->getContent();
		
		
		$this->requestData['track_id'] = $this->response->content;
		$this->requestData['sign'] = sha1(http_build_query($this->requestData) . $configUser->secret);
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response = $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode = $this->response->getCode();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId = $this->getOrderAttributes($responseContent);
		
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (!empty($json)) {
			return ['partner_order_id' => (string) $json];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}