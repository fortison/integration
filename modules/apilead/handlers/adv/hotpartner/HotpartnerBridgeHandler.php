<?php
namespace app\modules\apilead\handlers\adv\hotpartner;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class HotpartnerBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$order = [
			'params' => $this->encode([
				"order" => [
					'country' => $incomingModel->country,
					'comment' => $configOffer->comment ?? '',
					'name' => $incomingModel->name,
					'telephone' => $incomingModel->phone,
					'order_items_attributes' => [
						[
							"product_id" => $configOffer->offerId,
							"price" => $incomingModel->landing_cost,
							"quantity" => 1
						]
					],
					// 'price_in_currency' => $model->landing_cost,
					'external_id' => $incomingModel->id,
					'sub_id' => $incomingModel->wHash,
				]
			])
		];
		
		$this->requestData = $order;

		$this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
			'token' => $configUser->token,
		]);
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status		= $json[0]->status ?? null;
		$message	= $json[0]->message ?? null;
		$data		= $json[0]->data ?? null;
		
		if ($status === 200 && $message == 'OK')
			return ['partner_order_id' => (string) $data->id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}