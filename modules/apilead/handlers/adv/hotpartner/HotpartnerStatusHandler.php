<?php
namespace app\modules\apilead\handlers\adv\hotpartner;

use app\common\models\OrderApilead;
use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\handlers\base\BaseStatusHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\models\forms\ResponseModel;
use app\modules\apilead\common\objects\PrepareGetRequest;
use Yii;

class HotpartnerStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$apileadStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($apileadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToApilead($apileadStatusDataModel, $needSendStatus);
		
		return $apileadStatusDataModel;
	}

	public function makeStatusRequest()
	{
		$requestUrl = $this->config->getUrlOrderInfo() . '?' . http_build_query([
			'token'		=> $this->config->user->array['token'],
			'order_id'	=> [$this->orderModel->partner_order_id],
		]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		
		$data = $this->statusRequest($prepareRequest);
		
		return $data;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$sourceId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content	= $responseModel->content;
		
		$contentObject = $this->decode($content);
		
		$status		= $contentObject->status ?? null;
		$message	= $contentObject->message ?? null;
		$order		= $contentObject->data[0]->order ?? null;
		
		if ($status !== 200 || $message != 'OK')
			throw new OrderErrorException($orderId, "Wrong request {$sourceId}:{$apileadId}");
		
		if (!$order)
			throw new OrderErrorException($orderId, "Order not found in response {$sourceId}:{$apileadId}");
		
		if (!isset($order->status))
			throw new OrderErrorException($orderId, "Status not found for lead {$sourceId}:{$apileadId}");
		
		$comment			= $order->comment ?? '';
		$partnerStatus 		= $order->status;
		$partnerSubStatus 	= $order->substatus;
		$apileadStatus		= $this->getApileadStatus($partnerStatus, $partnerSubStatus);
		
		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($sourceId);
		
		return $apileadStatusDataModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getRansomStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$sourceId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content	= $responseModel->content;
		
		$contentObject = $this->decode($content);
		
		$status		= $contentObject->status ?? null;
		$message	= $contentObject->message ?? null;
		$order		= $contentObject->data[0]->order ?? null;
		
		if ($status !== 200 || $message != 'OK')
			throw new OrderErrorException($orderId, "Wrong request {$sourceId}:{$apileadId}");
		
		if (!$order)
			throw new OrderErrorException($orderId, "Order not found in response {$sourceId}:{$apileadId}");
		
		if (!isset($order->status))
			throw new OrderErrorException($orderId, "Status not found for lead {$sourceId}:{$apileadId}");
		
		$comment			= $order->comment ?? '';
		$partnerSubStatus 	= $order->substatus;
		$apileadStatus		= $this->getRansomStatusName($partnerSubStatus);
		
		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerSubStatus);
		$apileadStatusDataModel->setPartnerId($sourceId);
		
		return $apileadStatusDataModel;
	}
	
	public function getApileadStatus($partnerStatus, $partnerSubStatus)
	{
		$subStatuses	= $this->config->getSubStatuses();
		$trash			= $subStatuses[OrderApilead::STATUS_TRASH];
		
		if (array_key_exists($partnerSubStatus, $trash))																# спочатку перевірка на треш через сабстатус
			return OrderApilead::STATUS_TRASH;
		
		return parent::getApileadStatusName($partnerStatus);
	}
}