<?php
namespace app\modules\apilead\handlers\adv\tlight;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class TlightBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestData = [
			"key" 			 => $configUser->apiKey,             	//- ключ доступа к API, сообщаем приватно
			"id" 			 => $incomingModel->id,              	//- id лида в вашей системе
			"offer_id" 		 => $configOffer->offerId,      		//- id оффера в нашей системе
			"name" 			 => $incomingModel->name,             	//- ФИО покупателя
			"phone" 		 => $incomingModel->phone,           	//- телефон покупателя
			"comments" 		 => $incomingModel->address,        	//- комментарии к заказу
			"country" 		 => $incomingModel->country,         	//- страна покупателя,
			"address" 		 => $incomingModel->address,          	//- адрес покупателя
			"tz" 			 => "+{$incomingModel->tz}",            //- временная зона, например +3 для Europe/Moscow
			"web_id" 		 => $incomingModel->wHash,          	//- id вебмастера в вашей системе (subaccount)
			"ip_address" 	 => $incomingModel->ip,
			"user_agent" 	 => $incomingModel->user_agent,
			
			"utm_source" 	 => $incomingModel->user_id,
			"utm_medium" 	 => '',
			"utm_campaign" 	 => '',
			"utm_content" 	 => '',
			"utm_term" 		 => '',
		];
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json	= $this->decode($responseContent);
		
		$id		= $json->id ?? null;
		
		if ($id)
			return ['partner_order_id' => (string) $json->id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}