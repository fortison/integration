<?php
namespace app\modules\apilead\handlers\adv\tlight;

use app\common\models\OrderApilead;
use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\handlers\base\BaseStatusHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\models\forms\ResponseModel;
use app\modules\apilead\common\objects\PrepareGetRequest;
use Yii;

class TlightStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$apileadStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($apileadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToApilead($apileadStatusDataModel, $needSendStatus);
		
		return $apileadStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$requestUrl = $this->config->getUrlOrderInfo() . "?" . http_build_query([
			'key'	=> $this->config->user->array['apiKey'],
			'id'		=> $this->orderModel->partner_order_id,
		]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		
		$responseModel = $this->statusRequest($prepareRequest);
		
		return $responseModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$partnerId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content	= $responseModel->content;
		
		$contentObject = $this->decode($content);
		
		$order		= $contentObject->leads[0] ?? null;
		
		if (!$order)
			throw new OrderErrorException($orderId, "Order not found in response {$partnerId}:{$apileadId}");
		
		$partnerStatus			= (string) $order->status;
		
		if ($order->trash_reason)
			$apileadStatus = OrderApilead::STATUS_TRASH;
		else
			$apileadStatus		= $this->getApileadStatusName($partnerStatus);
		
		$comment				= $order->comment;
		
		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($partnerId);
		
		return $apileadStatusDataModel;
	}
}