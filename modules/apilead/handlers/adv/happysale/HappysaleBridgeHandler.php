<?php
namespace app\modules\apilead\handlers\adv\happysale;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class HappysaleBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;

		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$data = $this->encode([
			"phone"			=> $incomingModel->phone,			// номер телефона заказчика товара *
			"price"			=> $incomingModel->landing_cost + $incomingModel->cost_delivery,	// стоимость товара + стоимость доставки
			"order_id"		=> $incomingModel->id,				// id заявки внешний для синхронизации
			"name"			=> $incomingModel->name,			// ФИО заказчика
			"country"		=> $incomingModel->country,			// Гео локация *
			"addr"			=> $incomingModel->address,			// адрес
			"offer"			=> $configOffer->name,				// наименование товара *
			"secret"		=> $configUser->secret,
			"web_id"		=> $incomingModel->wHash,			// id web-а
			"remote_ip"		=> $incomingModel->ip,				// ip клиента который сделал заказ
		]);
		
		$hash = hash('sha256', strlen($data) . md5($configUser->uid));
		
		$this->requestData = [
			'data' => $data,
		];

		$this->requestUrl = $config->getUrlOrderAdd() . "?" . http_build_query([
			'uid'	=> $configUser->uid,
			'hash'	=> $hash,
		]);
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json		= $this->decode($responseContent);
		$success	= $json->result->success ?? null;
		$id			= $json->result->id ?? null;
		
		if ($success == 'TRUE')
			return ['partner_order_id' => (string) $id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}