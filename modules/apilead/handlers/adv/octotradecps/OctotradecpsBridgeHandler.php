<?php
namespace app\modules\apilead\handlers\adv\octotradecps;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class OctotradecpsBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$args = [$incomingModel, $config];
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'api_key' => $configUser->api_key,
			'country_code' => $incomingModel->country,
			'name' => $incomingModel->name,
			'phone' => $incomingModel->phone,
			'target_hash' => $configOffer->target_hash,
			'ip' => $incomingModel->ip,
			'user_agent' => $incomingModel->user_agent,
			'referer' => $incomingModel->uHash ?: $incomingModel->wHash,
		];
		
		if (!empty($configOffer->address)) {
			if (is_callable($configOffer->address))
				$address = call_user_func_array($configOffer->address, $args);
			else
				$address = $configOffer->address;
			
			$this->requestData['address'] = $address;
		}
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status	= $json->status ?? null;
		
		if ($status === 'success') {
			return ['partner_order_id' => (string) $json->data->lead_hash];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}