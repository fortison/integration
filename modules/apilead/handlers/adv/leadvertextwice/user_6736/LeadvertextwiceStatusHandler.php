<?php
namespace app\modules\apilead\handlers\adv\leadvertextwice\user_6736;

use app\common\models\OrderApilead;
use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\models\forms\ResponseModel;
use app\modules\apilead\common\objects\PrepareGetRequest;
use Yii;

/**
 * Обробник статусів замовлень до інтеграції Leadvertext
 *
 * User: nagard
 * Date: 31.10.17
 * Time: 14:01
 */
class LeadvertextwiceStatusHandler extends \app\modules\apilead\handlers\adv\leadvertextwice\LeadvertextwiceStatusHandler
{
	/**
	 * @inheritdoc
	 */
	public function makeStatusRequest()
	{
		$requestUrl = $this->config->getUrlOrderInfo() . '?' . http_build_query([
			'webmasterID'	=> $this->config->user->array['webmasterID'],
			'token'			=> $this->config->offer->array['token'],
			'ids'			=> [$this->orderModel->partner_order_id],
		]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);

		$data = $this->statusRequest($prepareRequest);

		return $data;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$offerConfig = (object) $this->config->user->array;
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$sourceId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$apileadId}");
		
		$content		= $responseModel->content;
		
		try {
			$contentObject	= $this->decode($content);
		} catch (\Exception $e) {
			throw new OrderErrorException($orderId, "Decode json error: {$e->getMessage()}");
		}
		
		if (!isset($contentObject->{$sourceId}))
			throw new OrderErrorException($orderId, "Lead not found {$sourceId}:{$apileadId}");
		
		$contentObject = $contentObject->{$sourceId};
		
		if (!isset($contentObject->payment->status))
			throw new OrderErrorException($orderId, "Status is not available for you {$sourceId}:{$apileadId}");
		
		$comment		= $offerConfig->reasonCancel[$contentObject->reasonCancel] ?? '' . $contentObject->fields->comment ?? '';
		$partnerStatus 	= $contentObject->payment->status;
		$apileadStatus	= $this->getApileadStatusName($partnerStatus);
		
		if ($apileadStatus === OrderApilead::STATUS_REJECT && ($contentObject->isSpam === 1 || $contentObject->isDouble)) {
			$apileadStatus = OrderApilead::STATUS_TRASH;
		}

		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($sourceId);

		return $apileadStatusDataModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getRansomStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$offerConfig = (object) $this->config->user->array;
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$sourceId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$apileadId}");
		
		$content	= $responseModel->content;
		
		$contentObject = $this->decode($content);
		
		if (!isset($contentObject->{$sourceId}))
			throw new OrderErrorException($orderId, "Lead not found {$sourceId}:{$apileadId}");
		
		$contentObject = $contentObject->{$sourceId};
		
		if (!isset($contentObject->buyoutState))
			throw new OrderErrorException($orderId, "Status is not available for you {$sourceId}:{$apileadId}");
		
		$comment		= $offerConfig->reasonCancel[$contentObject->reasonCancel] ?? '' . $contentObject->fields->comment ?? '';
		$partnerStatus 	= $contentObject->buyoutState;
		$apileadStatus	= $this->getRansomStatusName($partnerStatus);

		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($sourceId);

		return $apileadStatusDataModel;
	}
}