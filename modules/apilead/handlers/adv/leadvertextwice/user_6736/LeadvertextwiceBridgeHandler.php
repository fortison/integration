<?php
namespace app\modules\apilead\handlers\adv\leadvertextwice\user_6736;

use yii\web\HttpException;

class LeadvertextwiceBridgeHandler extends \app\modules\apilead\handlers\adv\leadvertextwice\LeadvertextwiceBridgeHandler
{
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		if (!$responseContent || !is_numeric($responseContent))
			throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
		
		return ['partner_order_id' => (string) $responseContent];
	}
}