<?php
namespace app\modules\apilead\handlers\adv\blackdollar;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class BlackdollarBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;

		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'key' 		 => $configUser->key,
			'ip' 		 => $incomingModel->ip,
			'phone' 	 => $incomingModel->phone,
			'stream' 	 => $configOffer->stream,
			'comments '  => $incomingModel->user_comment,
			'clientname' => $incomingModel->name,
			'referer'	 => $configOffer->domain ?? '',
			'externalWebmaster' => $incomingModel->getWHash(),
			'externalId' => $incomingModel->id,
		];

		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);

		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);

		$status	= $json->status ?? null;
		
		if ($status == '200') {
			return ['partner_order_id' => (string) $json->id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}