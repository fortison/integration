<?php
namespace app\modules\apilead\handlers\adv\reklmaster;

use app\common\models\OrderApilead;
use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\handlers\base\BaseStatusHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\models\forms\ResponseModel;
use app\modules\apilead\common\objects\PrepareGetRequest;
use Yii;

class ReklmasterStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$apileadStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($apileadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToApilead($apileadStatusDataModel, $needSendStatus);
		
		return $apileadStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		
		$requestUrl = $config->getUrlOrderInfo() . '?' . http_build_query([
			'id' => "{$configUser->user}-{$configUser->apiKey}",
			'ids' => $this->orderModel->system_order_id,
		]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		
		$responseModel = $this->statusRequest($prepareRequest);
		
		return $responseModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$partnerId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content	= $responseModel->content;
		
		$contentObject = $this->decode($content);
		
		$order		= $contentObject->{$orderModel->system_order_id} ?? null;
		
		if (!$order)
			throw new OrderErrorException($orderId, "Order not found in response {$partnerId}:{$apileadId}");
		
		$partnerStatus			= (string) $order->status;
		$partnerReason			= $order->reason;
		$systemStatus			= $this->getSystemStatus($partnerStatus, $partnerReason);
		$comment				= $order->comment . " {$this->getReason($systemStatus, $partnerReason)}";
		
		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($systemStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($partnerId);
		
		return $apileadStatusDataModel;
	}
	
	public function getSystemStatus($partnerStatus, $partnerReason):string
	{
		$subStatuses	= $this->config->getSubStatuses();
		$trash			= $subStatuses[OrderApilead::STATUS_TRASH];
		
		if (array_key_exists($partnerReason, $trash))																# спочатку перевірка на треш через сабстатус
			return OrderApilead::STATUS_TRASH;
		
		return parent::getApileadStatusName($partnerStatus);
	}
	
	public function getReason($systemStatus, $partnerReason):string
	{
		$subStatuses	= $this->config->getSubStatuses();
		$reasonList		= $subStatuses[$systemStatus] ?? '';
		
		return $reasonList[$partnerReason] ?? '';
	}
}