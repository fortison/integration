<?php
namespace app\modules\apilead\handlers\adv\xrich;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PrepareGetRequest;
use yii\web\HttpException;

class XrichBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;

		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$data = serialize([
			'sale'			=> $incomingModel->id,
			'offer_id'		=> $configOffer->offerId,
			'info'			=> [
				'fio'			=> $incomingModel->name,
				'tel'			=> $incomingModel->phone,
				'address'		=> '',
				'email'			=> '',
				'noty'			=> '',
			],
		]);

		$this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
			'tocall'	=> 'sale',
			'data'		=> $data,
		]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status = $json->status ?? null;
		$id		= null;
		
		if ($status === 1)
			return ['partner_order_id' => $id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}