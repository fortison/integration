<?php

namespace app\modules\apilead\handlers\adv\lemonad;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class LemonadBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$this->requestData = [
			'offerId'    			=> $configOffer->offerId,
			'fio'        			=> $incomingModel->name,
			'phone'     			=> $incomingModel->phone,
			'email'      			=> $incomingModel->email,
			'ip'         			=> $incomingModel->ip,
			'clickid'    			=> $incomingModel->id,
			'utm_source' 			=> $incomingModel->web_id,
			'externalWebmasterId' 	=> $incomingModel->web_id,
		
		];
		
		$prepareRequest = new PreparePostRequest();
		
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$prepareRequest->setRequestHeaders(['X-Token' =>$configUser->apiKey]);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode < 200 or $responseCode > 210)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);

		return $this->saveOrder($partnerOrderId);
	}
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);

		$status = $json->result;
		
		$id = $json->id ?? '';
		
		if ($status == 'ok')
			return ['partner_order_id' => $id];
		
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}

