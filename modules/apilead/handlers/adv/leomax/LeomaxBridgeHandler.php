<?php
namespace app\modules\apilead\handlers\adv\leomax;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class LeomaxBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestData = [
			'id'      		=> $incomingModel->id,
			'offer_id'    	=> $configOffer->offerId,
			'name'          => $incomingModel->name,
			'phone'         => $incomingModel->phone,
			'cost'         	=> $incomingModel->cost,
			'api_key'       => $configUser->apiKey,
//			'campaign_id'   => $configOffer->campaignId,
			'web_id'       	=> $incomingModel->uHash ?: $incomingModel->wHash,
		];
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);

		$id		= $json->id ?? null;
		
		if ($id)
			return ['partner_order_id' => $id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}