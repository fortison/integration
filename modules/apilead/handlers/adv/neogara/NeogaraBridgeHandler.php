<?php
namespace app\modules\apilead\handlers\adv\neogara;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class NeogaraBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$name_arr = preg_split("/[\s,]+/", $incomingModel->name);
		$firstname = empty($name_arr[0]) ? 'Empty' : $name_arr[0];
		$lastname = empty($name_arr[1]) ? 'Empty' : $name_arr[1];
		
		$this->requestData	= [
			'pid'      		=> $configUser->pid,
			'offerId'		=> $configOffer->offerId,
			'firstname'		=> $firstname,
			'lastname'		=> $lastname,
			'email'			=> $incomingModel->address,
			'phone'			=> $incomingModel->phone,
			'ref'			=> $configOffer->land_url,
			'ip'			=> $incomingModel->ip,
			'sourceIp'		=> $incomingModel->ip,
			'country'		=> $incomingModel->country,
			'city'			=> $incomingModel->geo_ip_city,
			'sub1'			=> $incomingModel->id,
			'sub2'			=> $incomingModel->getWHash(),
		];
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestHeaders(['content-type' => 'Content-Type: application/json']);
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status = $json->result ?? '';
		
		if ($status == 'ok')
			return ['partner_order_id' => (string) $json->lidId];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}