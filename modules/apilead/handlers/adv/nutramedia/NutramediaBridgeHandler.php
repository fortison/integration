<?php
namespace app\modules\apilead\handlers\adv\nutramedia;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class NutramediaBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;
		
		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$this->requestData = [
			'offer_id'      => $configOffer->offer_id,
			'user_id'       => $configUser->user_id,
			'country'       => $incomingModel->country,
			'name'			=> $incomingModel->name,
			'phone'         => $incomingModel->phone,
			'api_key'		=> $configUser->api_key,
			'check_sum'   	=> md5($configUser->user_id . $configOffer->offer_id . $configUser->api_key),
			'web_id'    	=> $incomingModel->uHash ?: $incomingModel->wHash,
			'ip'    	    => $incomingModel->ip ?? '127.0.0.1',
			'agent'			=> $incomingModel->user_agent,
			'data1'			=> $incomingModel->id,
		];
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (isset($json->data) && isset($json->data->id))
			return ['partner_order_id' => (string) $json->data->id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}