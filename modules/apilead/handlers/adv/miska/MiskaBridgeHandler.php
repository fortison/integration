<?php
namespace app\modules\apilead\handlers\adv\miska;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class MiskaBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		$incomingModel	= $this->incomingModel;
		$args 			= [$incomingModel, $config];

		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'id' => $incomingModel->id,
			'name' => $incomingModel->name,
			'country' => $incomingModel->country,
			'phone' => $incomingModel->phone,
			'tz' => $incomingModel->tz,
			'web_id' => $incomingModel->uHash ?: $incomingModel->wHash,
			'ip' => $incomingModel->ip,
			'user_agent' => $incomingModel->user_agent,
			'goods_quantity' => 1,
			'expenses_lid' => $incomingModel->cost,
			'reff_id' => '3',
		];
		
		if ($configOffer->goods_type_model_id) {
			if (is_callable($configOffer->goods_type_model_id)) {
				$products = call_user_func_array($configOffer->goods_type_model_id, $args);
			} else {
				$products = $configOffer->goods_type_model_id;
			}
			
			$this->requestData['goods_type_model_id'] = $products;
		}
		
		if ($configOffer->goods_price) {
			if (is_callable($configOffer->goods_price)) {
				$products = call_user_func_array($configOffer->goods_price, $args);
			} else {
				$products = $configOffer->goods_price;
			}
			
			$this->requestData['goods_price'] = $products;
		}
		
		$prepareRequest = new PreparePostRequest;
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
//		$json = $this->decode($responseContent);
		
		return [];
		
//		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}