<?php
namespace app\modules\apilead\handlers\adv\sellaction;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class SellactionBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;

		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;

		$params		= $this->encode([
			"name"			=> $incomingModel->name,
			"phone"			=> $incomingModel->phone,
			"order_id"		=> $incomingModel->id,
			"tariff_id"		=> $configOffer->tariffId, //для оффера Parfum 690
			"ip"			=> $incomingModel->ip,
			"country"		=> $incomingModel->country,
//				"referrer"		=> null,
			"user_agent" 	=> $incomingModel->user_agent,
			"custom_param"	=> $incomingModel->address ?? $configOffer->сustomParam,
		]);

		$signature 	= hash('sha256', $params.$configUser->secret);

		$this->requestData = [
			'params'	=> $params,
			'signature'	=> $signature
		];

        $this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		if ($responseContent == "Success")
			return [];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}