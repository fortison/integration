<?php
namespace app\modules\apilead\handlers\adv\sellaction\extform;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\helpers\Json;
use yii\web\HttpException;

class SellactionBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;

		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$this->requestData = [
			"sauid"			=> $configOffer->sauid,
			"satid"			=> $configOffer->satid,
			"satprm"		=> $configOffer->satprm,
			"phone"		    => $incomingModel->phone,
			"name"		    => $incomingModel->name,
			"agent"		    => $incomingModel->user_agent,
			"ip"		    => $incomingModel->ip,
			"subid1"	    => $incomingModel->id,
			"subid2"	    => $incomingModel->wHash,
			"country"	    => $incomingModel->country,
		];

        $this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = (object) Json::decode($responseContent);
		
		$status = $json->success ?: false;
		
		if ($status)
			return ['partner_order_id' => (string) $json->action_id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}