<?php
namespace app\modules\apilead\handlers\adv\sellaction\extform;

use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\handlers\base\BaseStatusHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\models\forms\ResponseModel;
use app\modules\apilead\common\objects\PrepareGetRequest;

class SellactionStatusHandler extends BaseStatusHandler
{
    public function updateStatusInSystem($force=false)
    {
        $responseModel = $this->makeStatusRequest();

        $apileadStatusDataModel = $this->getSystemStatusData($responseModel);

        $needSendStatus = $this->orderModel->updateOrder($apileadStatusDataModel);

        if ($force)
            $needSendStatus = true;

        $this->sendOrderStatusToApilead($apileadStatusDataModel, $needSendStatus);

        return $apileadStatusDataModel;
    }

    public function makeStatusRequest()
    {
        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

		$date_from = date('Y-m-d', (time() - 60*60*24*30));
		$requestUrl = $config->getUrlOrderInfo() . '?' . http_build_query([
//			'sort' => '-date',
//			'per-page' => '1000',
//			'date-from' => $date_from,
			'sub_id1' => $this->orderModel->system_order_id,
		]);

        $prepareRequest = new PrepareGetRequest();
        $prepareRequest->setRequestUrl($requestUrl);
        $prepareRequest->setRequestHeaders(["auth-token" => $configUser->authToken]);
	
		return $this->statusRequest($prepareRequest);
    }

    /**
     * @inheritdoc
     */
    public function getSystemStatusData(ResponseModel $responseModel)
    {
        $apiKey         = $this->config->user->getApiKey();
        $orderModel     = $this->orderModel;
        $configOffer	= (object) $this->config->offer->array;
        $orderId	    = $orderModel->id;
        $apileadId	    = $orderModel->system_order_id;
        $partnerId	    = $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");

		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");

		$content		= $responseModel->content;

		$contentObject	= $this->decode($content);

		if ( empty($contentObject->data) )
			throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
		
		$order = null;

		foreach ( $contentObject->data as $data ){
			if( $data->sub_id1 == $apileadId ){
				$order = $data;
				break;
			}
		}

		if( empty($order) ){
			throw new OrderErrorException($orderId, "Partner error!. Response: Order not found by sub_id1={$apileadId}");
		}

		if (!isset($order->status))
			throw new OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$apileadId}");

		$partnerStatus 		= (string) $order->status;
		$apileadStatus		= $this->getApileadStatusName($partnerStatus);
		$comment			= $order->comment ?? '';

		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);

		return $apileadStatusDataModel;
    }
}