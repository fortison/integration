<?php
namespace app\modules\apilead\handlers\adv\tms;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class TmsBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$args = [$incomingModel, $config];
		
		$apikey = (empty($configOffer->apikey)) ? $configUser->apikey : $configOffer->apikey;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'apikey' 			=> $apikey,
			'orderId' 			=> $configOffer->orderId,
			'name' 				=> $incomingModel->name,
			'phone' 			=> $incomingModel->phone,
			'email' 			=> $incomingModel->email ?? '',
			'address' 			=> $incomingModel->address,
			'productName' 		=> $configOffer->productName,
			'quantity'			=> 1,
			'clickId'    		=> $incomingModel->id,
			'affid'    			=> $incomingModel->uHash ?: $incomingModel->wHash,
			'orderType'			=> 'CPA',
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status	= $json->code ?? 0;
		
		if ($status == 200) {
			return ['partner_order_id' => (string) $json->id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}