<?php
namespace app\modules\apilead\handlers\adv\reklpro;

use app\common\models\OrderApilead;
use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\handlers\base\BaseStatusHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\models\forms\ResponseModel;
use app\modules\apilead\common\objects\PreparePostRequest;
use Yii;

class ReklproStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$apileadStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($apileadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToApilead($apileadStatusDataModel, $needSendStatus);
		
		return $apileadStatusDataModel;
	}

	public function makeStatusRequest()
	{
		$requestUrl = $this->config->getUrlOrderInfo();
		
		$requestData = [
			'api_key'	=> $this->config->user->array['apiKey'],
			'ids'		=> $this->orderModel->partner_order_id,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestData($requestData);
		
		$responseModel = $this->statusRequest($prepareRequest);
		
		return $responseModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$sourceId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content	= $responseModel->content;
		
		$contentObject = $this->decode($content);
		
		$order		= $contentObject->status_code[0] ?? null;
		$statusText	= $contentObject->status_text ?? null;
		
		if ($statusText !== 200)
			throw new OrderErrorException($orderId, "Wrong request {$sourceId}:{$apileadId}");
		
		if (!$order)
			throw new OrderErrorException($orderId, "Order not found in response {$sourceId}:{$apileadId}");
		
		$partnerStatus			= $order->status->id;
		$apileadStatus			= $this->getApileadStatusName($partnerStatus);
		$partnerStatusTitle		= $order->status->title;
		$partnerSubStatus		= $order->sub_status->id;
		$partnerSubStatusTitle	= $order->sub_status->title;
		$callHistory			= $this->decode($order->call_history) ?: [];
		$comment				= "{$partnerStatusTitle} - {$partnerSubStatusTitle}\n";
		
		foreach ($callHistory as $item) {
			$comment .= "\n {$item->date} - {$item->text}";
		}
		
		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($sourceId);
		
		return $apileadStatusDataModel;
	}
	
	public function getApileadStatus($partnerStatus, $partnerSubStatus)
	{
		$subStatuses	= $this->config->getSubStatuses();
		$trash			= $subStatuses[OrderApilead::STATUS_TRASH];
		
		if (array_key_exists($partnerSubStatus, $trash))																# спочатку перевірка на треш через сабстатус
			return OrderApilead::STATUS_TRASH;
		
		return parent::getApileadStatusName($partnerStatus);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getRansomStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$sourceId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content	= $responseModel->content;
		
		$contentObject = $this->decode($content);
		
		$order		= $contentObject->status_code[0] ?? null;
		$statusText	= $contentObject->status_text ?? null;
		
		if ($statusText !== 200)
			throw new OrderErrorException($orderId, "Wrong request {$sourceId}:{$apileadId}");
		
		if (!$order)
			throw new OrderErrorException($orderId, "Order not found in response {$sourceId}:{$apileadId}");
		
		$partnerStatus			= $order->status->id;
		$partnerStatusTitle		= $order->status->title;
		$partnerSubStatus		= $order->sub_status->id;
		$partnerSubStatusTitle	= $order->sub_status->title;
		$callHistory			= $this->decode($order->call_history) ?: [];
		$comment				= "{$partnerStatusTitle} - {$partnerSubStatusTitle}\n";
		$systemStatus			= $this->getApileadStatusName($partnerStatus);
		$ransomStatus			= $this->getRansomStatusName($systemStatus, $partnerSubStatus);
		
		foreach ($callHistory as $item) {
			$comment .= "\n {$item->date} - {$item->text}";
		}
		
		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($ransomStatus);
		$apileadStatusDataModel->setPartnerStatus("{$partnerStatus}-{$partnerSubStatus}");
		$apileadStatusDataModel->setPartnerId($sourceId);
		
		return $apileadStatusDataModel;
	}
	
	public function getRansomStatusName($systemStatus, $partnerSubRansomStatus=null)
	{
		$ransomStatuses = $this->config->user->array['configs']['ransom'] ?? null;
		
		if ($systemStatus === OrderApilead::STATUS_CONFIRM) {
			if (array_key_exists($partnerSubRansomStatus, $ransomStatuses[OrderApilead::STATUS_CONFIRM])) {
				return OrderApilead::STATUS_CONFIRM;
			} elseif (array_key_exists($partnerSubRansomStatus, $ransomStatuses[OrderApilead::STATUS_EXPECT])) {
				return OrderApilead::STATUS_EXPECT;
			} elseif (array_key_exists($partnerSubRansomStatus, $ransomStatuses[OrderApilead::STATUS_REJECT])) {
				return OrderApilead::STATUS_REJECT;
			} else {
				return OrderApilead::STATUS_UNKNOWN;
			}
		}else {
			return OrderApilead::STATUS_UNKNOWN;
		}
	}
}