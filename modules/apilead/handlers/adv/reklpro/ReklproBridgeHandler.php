<?php
namespace app\modules\apilead\handlers\adv\reklpro;

use app\common\models\OrderApilead;
use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\objects\PreparePostRequest;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class ReklproBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestData = [
			'api_key'       => $configUser->apiKey,
			'offer_id'      => $configOffer->offerId,
			'geo_id'        => $configOffer->geoId,
			// 'flow_hash'     => ,
			'name'          => $incomingModel->name,
			'phone'         => $incomingModel->phone,
			'address'       => $incomingModel->address,
			'subid'         => $incomingModel->uHash ?? $incomingModel->wHash,
		];
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status		= $json->status_code ?? null;
		
		if ($status == 200)
			return ['partner_order_id' => (string) $json->status_text];
		elseif ($status == 410) {
			$incomingModel	= $this->incomingModel;
			$apiKey     	= $this->config->user->getApiKey();
			$errorMessage	= $json->status_text;
			
			$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
			$apileadStatusDataModel->setId($incomingModel->id);
			$apileadStatusDataModel->setComment($errorMessage);
			$apileadStatusDataModel->setStatus(OrderApilead::STATUS_TRASH);
			
			$requestUrl		= $this->config->getUrlApileadOrderUpdate();
			$requestData    = $apileadStatusDataModel->getStatusData();
			
			$prepareRequest = new PreparePostRequestJson();
			$prepareRequest->setRequestUrl($requestUrl);
			$prepareRequest->setRequestData($requestData);
			
			$responseModel = $this->statusRequest($prepareRequest);
			
			if (!$responseModel->error && $responseModel->code == 200)
				return ['system_status' => OrderApilead::STATUS_TRASH];
			
			exit("APILEAD RESPONSE:" . $responseModel->content);
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}