<?php
namespace app\modules\apilead\handlers\adv\azurecrm;

use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\handlers\base\BaseStatusHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\models\forms\ResponseModel;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use app\modules\apilead\common\objects\PrepareGetRequest;

class AzurecrmStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$apileadStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($apileadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToApilead($apileadStatusDataModel, $needSendStatus);
		
		return $apileadStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		
		$orderModel = $this->orderModel;
		
		$requestUrl = $config->getUrlOrderInfo();
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		
		$requestData = [
			'lead_id' 				=> $orderModel->partner_order_id,
			'company_id' 			=> md5($configUser->external_partners_id),
			'timestamp'				=> md5(Date('Y-m-d')),
			'api_key'				=> $configUser->api_key,
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestData($requestData);
		
		$responseModel = $this->statusRequest($prepareRequest);
		
		return $responseModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$partnerId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content		= $responseModel->content;
		
		$contentObject	= $this->decode($content);
		
		if (!isset($contentObject))
			throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
		
		$order = $contentObject->collected;
		
		if ($contentObject->collected == false)
			throw new OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$apileadId}");
		
		$partnerStatus 		= (string) $order;
		$apileadStatus		= $this->getApileadStatusName($partnerStatus);
		$comment			= '';
		
		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($partnerId);
		
		return $apileadStatusDataModel;
	}
}