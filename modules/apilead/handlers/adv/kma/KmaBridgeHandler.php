<?php
namespace app\modules\apilead\handlers\adv\kma;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class KmaBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData = [
			'channel' => $configOffer->channel,
			"name" => $incomingModel->name,
			"phone" => $incomingModel->phone,
			"ip" => $incomingModel->ip,
			'country' => $incomingModel->country,
			"referer" => $incomingModel->source,
			"ua" => $incomingModel->uHash ?: $incomingModel->wHash,
			"external_webmaster" => $incomingModel->wHash ?? '',
			"data1" => $incomingModel->wHash ?? '',
			"data2" => $incomingModel->id,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestHeaders(
			[
				'Content-Type' => 'application/x-www-form-urlencoded',
				'Authorization' => 'Bearer ' . $configUser->token
			]
		);
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$code = $json->code ?? null;
		
		
		if ($code == 0)
			return ['partner_order_id' => (string) $json->order];
		
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}