<?php
namespace app\modules\apilead\handlers\adv\broleads;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class BroleadsBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logApileadRequest($this->incomingModel->attributes);

		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;

		$incomingModel	= $this->incomingModel;
		$phone = preg_replace('/\D/', '', $incomingModel->phone);

		if ($incomingModel->country === 'ES') {
			$phone = strlen($phone) <= 9 ? "34{$phone}" : $phone;
		} elseif ($incomingModel->country === 'PT') {
			$phone = strlen($phone) <= 9 ? "+351{$phone}" : $phone;
		}

		$this->requestData = [
			'name'          => $incomingModel->name,
			'phone'         => $phone,
			'stream'     	=> $configOffer->stream,
			'landing'    	=> $configOffer->landing,
			'country'    	=> $incomingModel->country,
			'sub1'       	=> $incomingModel->id,
//			'referrer'       	=> $incomingModel->uHash ?: $incomingModel->wHash,
//			'address'    	=> $_POST['address'],
//			'email'      	=> $_POST['email'],
//			'language'   	=> $_POST['language'],
//			'comment'    	=> $_POST['comment'],
//			'ip'         	=> $_POST['ip'],
//			'user-agent' 	=> $_POST['user-agent'],
//			'time_preland' 	=> $_POST['time_preland'],
//			'time_land'  	=> $_POST['time_land'],
//			'timezone'   	=> $_POST['timezone'],
//			'sub2'       	=> $_POST['sub2'],
//			'sub3'       	=> $_POST['sub3'],
//			'sub4'       	=> $_POST['sub4'],
//			'clickid'    	=> $_POST['clickid']
		];

		$this->requestUrl = $config->getUrlOrderAdd();

		$prepareRequest = new PreparePostRequest;
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);

		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();

		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

		$partnerOrderId		= $this->getOrderAttributes($responseContent);

		return $this->saveOrder($partnerOrderId);
	}

	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		return [];
	}
}