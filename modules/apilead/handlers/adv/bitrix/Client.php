<?php
/**
 * Created by PhpStorm.
 * User: nagard
 * Date: 26.03.18
 * Time: 15:48
 */

namespace app\modules\apilead\handlers\adv\bitrix;

use app\modules\apilead\common\objects\PrepareGetRequest;
use app\modules\apilead\common\objects\PreparePostRequest;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use app\modules\apilead\common\traits\{
	JsonTrait,
	LogTrait,
	RequestTrait
};
use yii\base\BaseObject;
use yii\web\HttpException;

/**
 * @property \app\modules\apilead\common\adv\objects\Config $config Головна конфігурація поточного замовлення
 */
class Client extends BaseObject
{
	use RequestTrait;
	use LogTrait;
	use JsonTrait;
	
	public $config;																										// головна конфігурація замовлення
	
	public function leadAdd($incomingModel)
	{
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		$subDomain		= $configUser->subDomain;
		$contactId		= $this->contactAdd($incomingModel);
		$id				= $configUser->id;
		$apiKey			= $configUser->apiKey;
		
		$requestUrl		= "https://{$subDomain}.bitrix24.ru/rest/{$id}/{$apiKey}/crm.deal.add";
		
		$requestData	= [
			'fields' => [
				"TITLE" => $configOffer->offerName,
				"TYPE_ID" => "GOODS",
				"STAGE_ID" => "NEW",
//            "COMPANY_ID" => $configOffer->companyId,
				"CONTACT_ID" => $contactId,
				"OPENED" => "Y",
//            "ASSIGNED_BY_ID" => 1,
//				"PROBABILITY" => 30,
				"CURRENCY_ID" => "RUB",
				"OPPORTUNITY" => $incomingModel->landing_cost,
//            "BEGINDATE" => date2str(current),
//            "CLOSEDATE" => date2str(nextMonth)
			]
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestData($requestData);
		
		$response	= $this->bridgeRequest($prepareRequest);
		
		if (!empty($response->error))
			throw new HttpException('405', "Client add error. CURL: {$response->error}");
		
		$responseCode		= $response->getCode();
		$responseContent	= $response->getContent();
		
		if ($responseCode != 200 && $responseCode != 204)
			throw new HttpException('400', "Partner error! Error code: {$responseCode}. Response: {$responseContent}");
		
		return $this->getPartnerOrderId($responseContent);
	}
	
	public function leadInfo($partnerId)
	{
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		$subDomain		= $configUser->subDomain;
		$id				= $configUser->id;
		$apiKey			= $configUser->apiKey;
		
		$requestUrl = "https://{$subDomain}.bitrix24.ru/rest/{$id}/{$apiKey}/crm.deal.get" . '?' . http_build_query(['id' => $partnerId]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		
		return $this->statusRequest($prepareRequest);
	}
	
	public function contactAdd($incomingModel)
	{
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		$subDomain		= $configUser->subDomain;
		$id				= $configUser->id;
		$apiKey			= $configUser->apiKey;
		
		$requestUrl		= "https://{$subDomain}.bitrix24.ru/rest/{$id}/{$apiKey}/crm.contact.add";
		$requestData	= [
			'fields' => [
				"NAME" => $incomingModel->name,
				"OPENED" => "Y",
				"TYPE_ID" => "CLIENT",
				"SOURCE_ID" => "SELF",
				"PHONE" => [
					[
						"VALUE" => $incomingModel->phone,
						"VALUE_TYPE" => "WORK",
					],
				],
			],
//			'params' => ["REGISTER_SONET_EVENT" => "Y"],
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestData($requestData);
		
		$response	= $this->bridgeRequest($prepareRequest);
		
		if (!empty($response->error))
			throw new HttpException('405', "Client add error. CURL: {$response->error}");
		
		$responseCode		= $response->getCode();
		$responseContent	= $response->getContent();
		
		if ($responseCode != 200 && $responseCode != 204)
			throw new HttpException('400', "Partner error! Contact not set! Code: {$responseCode}. Response: {$responseContent}");
		
		$clientObject = $this->decode($responseContent);
		$clientId		= $clientObject->result ?? null;
		
		if (!$clientId)
			throw new HttpException('405', "Wrong clientId.");
		
		return $clientId;
	}
	
	public function getPartnerOrderId($responseContent)
	{
		$responseLeadObject = $this->decode($responseContent);
		
		if (!isset($responseLeadObject->result))
			throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
		
		return $responseLeadObject->result;
	}
}