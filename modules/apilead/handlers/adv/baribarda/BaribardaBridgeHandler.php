<?php
namespace app\modules\apilead\handlers\adv\baribarda;

use app\modules\apilead\common\exeptions\NoLogException;
use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class BaribardaBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;

		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$loginId		= $configUser->loginId;
		
		$this->requestData = $this->encode([
			"phone"				=> $incomingModel->phone,																// номер телефона заказчика товара *
			"price"				=> $incomingModel->landing_cost,														// стоимость товара
			"order_id"			=> $incomingModel->id,																	// id заявки внешний для синхронизации
			"name"				=> $incomingModel->name,																// ФИО заказчика
			"country"			=> $incomingModel->country,
			"description"		=> $incomingModel->address,
			"offer"				=> $configOffer->offerId,																// наименование товара *
			"secret"			=> $configUser->apiKey,																	// секретный ключ АПИ *
			"source"			=> $incomingModel->wHash,
//			"addr"				=> $incomingModel->landing_cost,
		]);
		
		$hashString = strlen($this->requestData) . md5($loginId);
		$hash = hash('sha256', $hashString);

		$this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
			'uid'	=> $loginId,
			'hash'	=> $hash,
		]);
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData(['data' => $this->requestData]);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$attributes		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($attributes);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$result		= $json->result ?? null;
		$status		= $result->success ?? null;
		$message	= $result->message ?? null;
		$id			= $result->id ?? null;
		
		if ($status === "TRUE") {
			return ['partner_order_id' => (string)$id];
		} else if ($status === "FALSE" && ($message === "Duplicate phone data" || preg_match('/Incorrect phone number/', $message))) {
			throw new NoLogException('400', "Wrong partnerOrderId. {{$responseContent}}");
			
//			if (preg_match('/[a-zA-z]/', $this->incomingModel->phone))
//				throw new HttpException('400', "Incorrect phone number. Number contains letters.");
//
//			$incomingModel	= $this->incomingModel;
//			$apiKey     	= $this->config->user->getApiKey();
//			$errorMessage	= $message;
//
//			$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
//			$apileadStatusDataModel->setId($incomingModel->id);
//			$apileadStatusDataModel->setComment($errorMessage);
//			$apileadStatusDataModel->setStatus(OrderApilead::STATUS_TRASH);
//
//			$requestUrl		= $this->config->getUrlApileadOrderUpdate();
//			$requestData    = $apileadStatusDataModel->getStatusData();
//
//			$prepareRequest = new PreparePostRequestJson();
//			$prepareRequest->setRequestUrl($requestUrl);
//			$prepareRequest->setRequestData($requestData);
//
//			$responseModel = $this->statusRequest($prepareRequest);
//
//			if (!$responseModel->error && $responseModel->code == 200)
//				return ['system_status' => OrderApilead::STATUS_TRASH];
//
//			exit("APILEAD RESPONSE:" . $responseModel->content);
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}