<?php
namespace app\modules\apilead\handlers\adv\darkleads;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class DarkleadsBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;

//		$postback_url = $config->getParameter('postbackUrl');

//		if( !empty($postback_url) ){
//            $postback_url = str_replace(['{order_id}'], [$incomingModel->id], $postback_url);
//        }
		
		$this->requestData = [
            'name'          => $incomingModel->name,
            'phone'         => $incomingModel->phone,
            'api_key'       => $configUser->apiKey,
            'user_ip'       => $incomingModel->ip,
            'product_id'    => $configOffer->product_id,
            'webname'       => $incomingModel->uHash ?: $incomingModel->wHash,
            'url'           => '',
            'order_id'      => $incomingModel->id,
//            'postback_url'  => $postback_url,
		];
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		$result = $json->result ?? null;
		
		if ($result === 'ok')
			return [];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}