<?php
namespace app\modules\apilead\handlers\adv\zdorov;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use yii\web\HttpException;

class ZdorovBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;

		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$client		= new Client([
			'config'		=> $config,
		]);
		
		$data = [
			"sostatus" => 'Новый',
			"sp_firstname" => $incomingModel->name,
			"sp_client_mobile" => $incomingModel->phone,
			'sp_offer' => $configOffer->relation,
			"sp_ip" => $incomingModel->ip,
			'language_landing' => "RU",
			"sp_country" => mb_strtoupper($incomingModel->country),
			"sp_utm_source" => 'Apilead',
			"sp_utm_content" => $incomingModel->wHash,
			'sp_net_so_number' => $incomingModel->id,
			'sp_lead_cost_pp' => $incomingModel->cost,
		];
		
		$data['currency_id'] = '21x1';
		$data['productid'] = '14x22979';
		$data['LineItems'][0]['quantity'] = 1;
		$data['LineItems'][0]['productid'] = '14x22979';
		$data['LineItems'][0]['unit_price'] = 0;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= 'Sent via client. See bridge log.';
		
		$this->response		= $client->leadAdd($data);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		if ($this->response->code != 200)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseObject = $this->decode($this->response->content);
		
		if ($responseObject->success !== true)
			throw new HttpException('400', "{$this->response->content} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$order = $responseObject->result;
		
		$attributes		= $this->getOrderAttributes($order);
		
		return $this->saveOrder($attributes);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($order)
	{
		return ['partner_order_id' => (string) $order->salesorder_custom];
	}
}