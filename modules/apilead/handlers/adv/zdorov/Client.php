<?php
namespace app\modules\apilead\handlers\adv\zdorov;

use app\modules\apilead\common\adv\objects\Config;
use app\modules\apilead\common\objects\PrepareGetRequest;
use app\modules\apilead\common\objects\PreparePostRequest;
use app\modules\apilead\common\traits\JsonTrait;
use app\modules\apilead\common\traits\LogTrait;
use app\modules\apilead\common\traits\RequestTrait;
use yii\base\BaseObject;
use yii\web\HttpException;

/**
 * @property Config $config Головна конфігурація поточного замовлення
 */
class Client extends BaseObject
{
	use RequestTrait;
	use LogTrait;
	use JsonTrait;
	
	public $config;																										// головна конфігурація замовлення
	
	public $sessionName;
	public $userId;
	
	public function __construct(array $config)
	{
		parent::__construct($config);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$url		= $config->getUrlOrderAdd();
		$userName	= $configUser->userName;
		$accessKey	= $configUser->accessKey;
		
		$requestUrl = $url . "?" . http_build_query([
			'operation' => 'getchallenge',
			'username'  => $userName,
		]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		
		$response	= $this->bridgeRequest($prepareRequest);
		
		if ($response->error)
			throw new HttpException('400', "{$response->error}");
		
		if ($response->code != 200)
			throw new HttpException('400', "{$response->error}");
		
		$responseObject = $this->decode($response->content);
		
		if ($responseObject->success !== true)
			throw new HttpException('400', "{$response->content}");
		
		$token = $responseObject->result->token;
		
		$requestData = [
			'operation' => 'login',
			'username'  => $userName,
			'accessKey' => md5($token . $accessKey)
		];
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($url);
		$prepareRequest->setRequestData($requestData);
		
		$response	= $this->bridgeRequest($prepareRequest);
		
		if ($response->error)
			throw new HttpException('400', "{$response->error}");
		
		if ($response->code != 200)
			throw new HttpException('400', "{$response->error}");
		
		$responseObject = $this->decode($response->content);
		
		if ($responseObject->success !== true)
			throw new HttpException('400', "{$response->content}");
		
		$this->sessionName	= $responseObject->result->sessionName;
		$this->userId		= $responseObject->result->userId;
	}
	
	public function leadAdd($data)
	{
		$data['assigned_user_id'] = $this->userId;
		
		$requestData = [
			'operation'   => 'create',
			'sessionName' => $this->sessionName,
			'elementType' => 'SalesOrder',
			'element'     => $this->encode($data),
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->config->getUrlOrderAdd());
		$prepareRequest->setRequestData($requestData);
		
		return  $this->bridgeRequest($prepareRequest);
	}
	
	public function leadInfo($partnerId)
	{
		$query = "SELECT * FROM SalesOrder WHERE salesorder_custom = '{$partnerId}';";
		
		$requestUrl = $this->config->getUrlOrderAdd() . "?" . http_build_query([
			'operation' => 'query',
			'sessionName'  => $this->sessionName,
			'query'  => $query,
		]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		return $this->bridgeRequest($prepareRequest);
	}
}