<?php
namespace app\modules\apilead\handlers\adv\leadsystemapp;

	use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class LeadsystemappBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;
		
		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$this->requestData = [
			'firstname'            => $incomingModel->name,
			'phone'                => $incomingModel->phone,
			'postalCode'	       => $incomingModel->zip,
			'city'			       => $incomingModel->geo_ip_city,
			'source'               => $configOffer->source ?? '',
			'externalLeadId'       => $incomingModel->id,
			'product'              => $configOffer->product,
			'price'                => $configOffer->price ?? '',
			'country'              => $configOffer->country ?? $incomingModel->country,
			'externalPartnerId'	   => $configUser->externalPartnerId,
			'ipAddress'            => $incomingModel->ip,
			'externalPartnerSubId' => $incomingModel->getWHash(),
		];
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestHeaders(['Content-Type' => 'application/x-www-form-urlencoded',
			'X-AUTH-TOKEN' => '32TkS4uowwe9xF5SWwGUXYAxCaGlMby1']);
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode < 200 && $responseCode > 210)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$code = $json->ok ?? false;
		
		if ($code == true) {
			return ['partner_order_id' => (string) $json->ccResponse->id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}