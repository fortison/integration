<?php
namespace app\modules\apilead\handlers\adv\affbay;

use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\handlers\base\BaseStatusHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\models\forms\ResponseModel;
use Yii;

class AffbayStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$this->logStatusRequest(new ResponseModel, Yii::$app->request->url, Yii::$app->request->post());
		
		$apiKey     		= $this->config->user->getApiKey();
		$orderModel			= $this->orderModel;
		$orderId			= $orderModel->id;
		$apileadId			= $orderModel->system_order_id;
		$partnerId			= $orderModel->partner_order_id;
		$partnerStatus 		= Yii::$app->request->get('partnerStatus');
		$comment 			= Yii::$app->request->get('comment');
		
		if (!isset($partnerStatus))
			throw new OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$apileadId}");
		
		$systemStatus		= $this->getApileadStatusName($partnerStatus);
		
		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($systemStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($partnerId);
		
		try {
			$this->sendOrderStatusToApilead($apileadStatusDataModel, true);
			$orderModel->system_status = $systemStatus;
			$orderModel->partner_status = $partnerStatus;
			$orderModel->comment = $comment;
			$orderModel->save();
		} catch (\Exception $e) {
			throw new OrderErrorException($orderId, "Error: {$e->getMessage()}");
		}
		
		return $apileadStatusDataModel;
	}
	
	public function makeStatusRequest(){}
	public function getSystemStatusData(ResponseModel $responseModel){}
}