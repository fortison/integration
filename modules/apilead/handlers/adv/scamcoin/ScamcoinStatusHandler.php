<?php
namespace app\modules\apilead\handlers\adv\scamcoin;

use app\common\models\OrderApilead;
use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\handlers\base\BaseStatusHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\models\forms\ResponseModel;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use Yii;

class ScamcoinStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$apileadStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($apileadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToApilead($apileadStatusDataModel, $needSendStatus);
		
		return $apileadStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$requestUrl = $this->config->getUrlOrderInfo() . '?' . http_build_query([
		'id' => "{$this->config->user->array['user']}-{$this->config->user->array['key']}"
	]);
		
		$requestData = [
			'ids' => [
				$this->orderModel->partner_order_id,
			]
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestData($requestData);
		
		return $this->statusRequest($prepareRequest);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$sourceId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$apileadId}");
		
		$content	= $responseModel->content;
		
		$contentObject = $this->decode($content);
		
		$order = $contentObject[0];
		
		if (!isset($order->stage))
			throw new OrderErrorException($orderId, "Wrong request {$sourceId}:{$apileadId}");
		
		if (!isset($order->stage))
			throw new OrderErrorException($orderId, "Status not found for lead {$sourceId}:{$apileadId}");
		
		$comment		= $order->reason_text ?? '';
		$partnerStatus 	= $order->stage;
		$apileadStatus	= $this->getApileadStatusName($partnerStatus);
		
		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($sourceId);
		
		return $apileadStatusDataModel;
	}
}