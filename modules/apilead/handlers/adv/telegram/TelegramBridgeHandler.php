<?php
namespace app\modules\apilead\handlers\adv\telegram;

use app\common\models\OrderApilead;
use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PrepareGetRequest;
use yii\web\HttpException;

class TelegramBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		$text =  "*id*               - " . $incomingModel->id .     "\n";
		$text .= ($configOffer->offer_name) ? ("*offer*          - " . $configOffer->offer_name . "\n") : '';
		$text .= "*name*        - " . $incomingModel->name .   		"\n";
		$text .= "*phone*       - " . $incomingModel->phone . 		"\n";
		$text .= "*comment* - " . $incomingModel->user_comment . "\n";
		$text .= "[trash](http://connectcpa.net/apilead/adv/adv-postback-status?systemUserId=" . $incomingModel->user_id ."&systemOrderId=" . $incomingModel->id . "&partnerStatus=trash&postbackApiKey=" . $configUser->postbackApiKey . ") ";
		$text .= "[reject](http://connectcpa.net/apilead/adv/adv-postback-status?systemUserId=" . $incomingModel->user_id . "&systemOrderId=" . $incomingModel->id . "&partnerStatus=reject&postbackApiKey=" . $configUser->postbackApiKey . ") ";
		$text .= "[expect](http://connectcpa.net/apilead/adv/adv-postback-status?systemUserId=" . $incomingModel->user_id . "&systemOrderId=" . $incomingModel->id . "&partnerStatus=expect&postbackApiKey=" . $configUser->postbackApiKey . ") ";
		$text .= "[confirm](http://connectcpa.net/apilead/adv/adv-postback-status?systemUserId=" . $incomingModel->user_id . "&systemOrderId=" . $incomingModel->id . "&partnerStatus=confirm&postbackApiKey=" . $configUser->postbackApiKey . ") ";
		
		$this->requestUrl = $config->getUrlOrderAdd() . "?" . http_build_query([
				'text'		=> $text,
				'chat_id'	=> $configUser->chat_id,
				'parse_mode'=> 'Markdown',
				]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		$status = $json->ok;
		
		if ($status == true || $status == 'true')
			return [];
		
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}