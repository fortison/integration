<?php
namespace app\modules\apilead\handlers\adv\silaguru;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PrepareGetRequest;
use yii\web\HttpException;

class SilaguruBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;

		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;

		$this->requestUrl = $config->getUrlOrderAdd() . $configOffer->offer . '?' . http_build_query([
				"phone" => $incomingModel->phone,
				"name" => $incomingModel->name,
				"ip" => $incomingModel->ip,
				"reseller" => $configOffer->reseller,
//                "wm" => $model->web_id,
				"wm" => $configOffer->stream,
				"extid" => $incomingModel->id,
				"ua" => $incomingModel->user_agent,
			]);
		
		$prepareRequest = new PrepareGetRequest;
		$prepareRequest->setRequestUrl($this->requestUrl);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$success	= $json->status ?? null;
		$id			= $json->id ?? null;
		
		if ($success === "success")
			return ['partner_order_id' => (string) $id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}