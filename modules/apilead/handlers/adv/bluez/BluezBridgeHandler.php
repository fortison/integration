<?php
namespace app\modules\apilead\handlers\adv\bluez;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class BluezBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;

		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
	
		$this->requestData = [
			"offer"		=> $configOffer->offerId,
			"name"		=> $incomingModel->name,
			"phone"		=> $incomingModel->phone,
			"id"		=> $incomingModel->id,
			"currency"	=> $incomingModel->landing_currency,
//		    "country" 	=> $incomingModel->country,
			"ip"		=> $incomingModel->ip,
		];

		$this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
			'id' => $configUser->id,
		]);
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status		= $json->status ?? null;
		$id			= $json->id ?? null;
		
		if ($status == 'ok')
			return ['partner_order_id' => (string) $id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}