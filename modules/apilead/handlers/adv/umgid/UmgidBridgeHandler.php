<?php
namespace app\modules\apilead\handlers\adv\umgid;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class UmgidBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$args = [$incomingModel, $config];
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'key'           => $configUser->key,
			'ip'  			=> $incomingModel->ip,
			'first_name' 	=> $incomingModel->name,
			'phone_input' 	=> $incomingModel->phone,
			'email' 	 	=> $incomingModel->email ?? '',
			'comment'  		=> $incomingModel->user_comment ?? '',
			'product_id' 	=> $configOffer->product_id,
			'offer_id' 		=> $configOffer->offer_id,
			'geo' 			=> $incomingModel->country ?? '',
			'payment_id'	=> $configOffer->payment_id,
			'subid1' 		=> $incomingModel->id,
			'subid2' 		=> $incomingModel->getWHash(),
		];
		
		if (!empty($configOffer->address)) {
			if (is_callable($configOffer->address))
				$address = call_user_func_array($configOffer->address, $args);
			else
				$address = $configOffer->address;
			
			$this->requestData['address'] = $address;
		}
		
		$prepareRequest = new PreparePostRequestJson();
//		$prepareRequest->setRequestHeaders(['Content-Type' => 'application/x-www-form-urlencoded']);
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestHeaders(['Content-Type' => 'application/json']);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status	= $json->http_code ?? 0;
		
		if ($status == 200) {
			return ['partner_order_id' => (string) $json->order_id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}