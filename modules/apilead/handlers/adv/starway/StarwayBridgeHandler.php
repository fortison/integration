<?php
namespace app\modules\apilead\handlers\adv\starway;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PrepareGetRequest;
use yii\web\HttpException;

class StarwayBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;

		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
			'api_key'		=> $configUser->apiKey,	// API-ключ из профиля вебмастера
			'offer_id'		=> $configOffer->offerId,	// ID товара в системе
			'flow_hash'		=> $configOffer->flowHash,	// хеш потока в системе
			'phone'			=> $incomingModel->phone,	//  Телефон покупателя
			'name'			=> $incomingModel->name,	// ФИО покупателя
			'ip'			=> $incomingModel->ip,	// IP покупателя
			'country'		=> $incomingModel->country,	// Страна
			'externalWebID' => substr($incomingModel->getWHash(), 1),  //ID внешнего вебмастера
		]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status		= $json->status ?? null;
		$id			= $json->response->request_id ?? null;
		
		if ($status === 'successful')
			return ['partner_order_id' => (string) $id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}