<?php
namespace app\modules\apilead\handlers\adv\corezoid;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class CorezoidBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;

		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		$args 			= [$incomingModel, $config];
		
		$this->requestData = [
			"id"						=> $incomingModel->id,
			"name"						=> $incomingModel->name,
			"phone"						=> $incomingModel->phone,
		];
		
		if ($configOffer->products) {
			if (is_callable($configOffer->products)) {
				$products = call_user_func_array($configOffer->products, $args);
			} else {
				$products = $configOffer->products;
			}
			
			$this->requestData['tovar']  = $products;
		}
		
		if ($configUser->identify) {
			if (is_callable($configUser->identify)) {
				$identify = call_user_func_array($configUser->identify, $args);
			} else {
				$identify = $configUser->identify;
			}
			
			$this->requestData['identify']  = $identify;
		}

		if ($configOffer->country) {
			if (is_callable($configOffer->country)) {
				$country = call_user_func_array($configOffer->country, $args);
			} else {
				$country = $configOffer->country;
			}

			$this->requestData['country']  = $country;
		}

		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		parse_str($responseContent, $array);
		
		$status = $array['request_proc'] ?? null;
		$result = $array['ops'] ?? null;
		
		$json	= $this->decode($result);
		$id		= $json->obj_id ?? null;

		if ($status === 'ok' && $id) {
			return ['partner_order_id' => (string)$id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}