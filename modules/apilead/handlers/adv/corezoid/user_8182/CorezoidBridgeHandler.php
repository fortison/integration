<?php
namespace app\modules\apilead\handlers\adv\corezoid\user_8182;

use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class CorezoidBridgeHandler extends \app\modules\apilead\handlers\adv\corezoid\CorezoidBridgeHandler
{
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json	= $this->decode($responseContent);
		$result = $json->result ?? null;
		
		if ($result === 'OK') {
			return [];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}