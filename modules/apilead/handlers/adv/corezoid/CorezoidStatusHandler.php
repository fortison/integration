<?php
namespace app\modules\apilead\handlers\adv\corezoid;

use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\handlers\base\BaseStatusHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\models\forms\ResponseModel;
use Yii;

class CorezoidStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel	= new ResponseModel;
		$requestUrl		= Yii::$app->request->url;
		$requestData	= Yii::$app->request->post();
		$this->logStatusRequest($responseModel, $requestUrl, $requestData);
		
		$apiKey     		= $this->config->user->getApiKey();
		$orderModel			= $this->orderModel;
		$orderId			= $orderModel->id;
		$terraleadsId		= $orderModel->system_order_id;
		$partnerId			= $orderModel->partner_order_id;
		$partnerStatus 		= Yii::$app->request->get('partnerStatus');
		$comment 			= Yii::$app->request->get('comment');
		
		if (!isset($partnerStatus))
			throw new OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$terraleadsId}");
		
		$systemStatus		= $this->getApileadStatusName($partnerStatus);
		
		$terraleadsStatusDataModel = new ApileadStatusDataModel($apiKey);
		$terraleadsStatusDataModel->setId($terraleadsId);
		$terraleadsStatusDataModel->setComment($comment);
		$terraleadsStatusDataModel->setStatus($systemStatus);
		$terraleadsStatusDataModel->setPartnerStatus($partnerStatus);
		$terraleadsStatusDataModel->setPartnerId($partnerId);
		
		try {
			$this->sendOrderStatusToApilead($terraleadsStatusDataModel, true);
			$orderModel->system_status = $systemStatus;
			$orderModel->partner_status = $partnerStatus;
			$orderModel->comment = $comment;
			$orderModel->save();
		} catch (\Exception $e) {
			throw new OrderErrorException($orderId, "Error: {$e->getMessage()}");
		}
		
		return $terraleadsStatusDataModel;
	}
	
	public function makeStatusRequest(){}
	public function getSystemStatusData(ResponseModel $responseModel){}
}