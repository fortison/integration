<?php
namespace app\modules\apilead\handlers\adv\teledirekt;

use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\handlers\base\BaseStatusHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\models\forms\ResponseModel;
use app\modules\apilead\common\objects\PrepareGetRequest;
use Yii;

class TeledirektStatusHandler extends BaseStatusHandler
{
    public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();

		$apileadStatusDataModel = $this->getSystemStatusData($responseModel);

		$needSendStatus = $this->orderModel->updateOrder($apileadStatusDataModel);

		if ($force)
			$needSendStatus = true;

		$this->sendOrderStatusToApilead($apileadStatusDataModel, $needSendStatus);

		return $apileadStatusDataModel;
	}

	public function makeStatusRequest()
	{
		$requestUrl = $this->config->getUrlOrderInfo() . '?' . http_build_query([
				'api_key'	=> $this->config->user->array['apiKey'],
				'user_id'	=> $this->config->user->array['userId'],
				'ids'		=> $this->orderModel->partner_order_id,
			]);

		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);

		$data = $this->statusRequest($prepareRequest);

		return $data;
	}

	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$sourceId	= $orderModel->partner_order_id;

		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$apileadId}");

		$content	= $responseModel->content;

		$contentObject = $this->decode($content);

		if (!isset($contentObject->status_code))
			throw new OrderErrorException($orderId, "Wrong request {$sourceId}:{$apileadId}");

		if ($contentObject->status_code !== 200)
			throw new OrderErrorException($orderId, "Status FALSE {$sourceId}:{$apileadId}");

		if (!isset($contentObject->response[0]))
			throw new OrderErrorException($orderId, "Order not found in response {$sourceId}:{$apileadId}");

		$order = $contentObject->response[0];

		if (!isset($order->status))
			throw new OrderErrorException($orderId, "Status not found for lead {$sourceId}:{$apileadId}");
		
		$comment		= $order->comment ?? '';
		$partnerStatus 	= $order->status;
		$apileadStatus	= $this->getApileadStatusName($partnerStatus);

		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($sourceId);
		
		return $apileadStatusDataModel;
	}
}