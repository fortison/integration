<?php
namespace app\modules\apilead\handlers\adv\arbitpro;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PrepareGetRequest;
use yii\web\HttpException;

class ArbitproBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;
		
		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$this->requestData = [
			'api_key'      => $configUser->api_key,
			'hash'         => $configOffer->hash,
			'phone'        => $incomingModel->phone,
			'fio'          => $incomingModel->name,
			'ip'		   => $incomingModel->ip ?? '127.0.0.1',
			'sub1'   	   => $incomingModel->id,
			'sub2'    	   => $incomingModel->uHash ?: $incomingModel->wHash,
			'ewid'		   => $incomingModel->getWHash(),
		];
		
		$this->requestUrl = $config->getUrlOrderAdd()  . '?' . http_build_query($this->requestData);

		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode < 200 && $responseCode > 210)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (isset($json->success) && $json->success === true)
			return ['partner_order_id' => (string) $json->transaction_id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}