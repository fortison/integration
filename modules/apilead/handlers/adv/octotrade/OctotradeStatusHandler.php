<?php
namespace app\modules\apilead\handlers\adv\octotrade;

use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\handlers\base\BaseStatusHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\models\forms\ResponseModel;
use app\modules\apilead\common\objects\PrepareGetRequest;

class OctotradeStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$apileadStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($apileadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToApilead($apileadStatusDataModel, $needSendStatus);
		
		return $apileadStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		
		$orderModel = $this->orderModel;
		
		$requestUrl = $config->getUrlOrderInfo() . "?" . http_build_query([
//			'date'		=> \Yii::$app->formatter->asDate($orderModel->created_at, 'YMMdd'),
			'pass'		=> $configUser->pass,
			'order_id'	=> $orderModel->system_order_id,
		]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		
		$responseModel = $this->statusRequest($prepareRequest);
		
		return $responseModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$partnerId	= $orderModel->partner_order_id;
		$ransomArr  = $this->config->full->configs['ransom'] ?? array();
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content		= $responseModel->content;
		
		$contentObject	= $this->decode($content);
		
		if (!isset($contentObject->orders[0]))
			throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
		
		$order			= $contentObject->orders[0];
		
//		if (!$order)
//			throw new OrderErrorException($orderId, "Order not found in response {$partnerId}:{$apileadId}");
		
		if (!isset($order->state_id))
			throw new OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$apileadId}");
		
		$ransomInfo = '';
		
		if (isset($order->repayment)) {
			$partnerRansomStatus = (string) $order->repayment ?? '';
			$ransomStatusName    = $this->getRansomStatusName($partnerRansomStatus);
			$ransomStatusValue 	 = (array_key_exists($ransomStatusName, $ransomArr))
				? ((array_key_exists($partnerRansomStatus, $ransomArr[$ransomStatusName]))
					? $ransomArr[$ransomStatusName][$partnerRansomStatus]
					: 'no data')
				: 'no data';
			$ransomInfo = '; Информация по выкупу: (PartnerRansomId-' .  ($partnerRansomStatus ?? 'no data') . ')(ApileadValue-' . $ransomStatusName . ')(PartnerValue-' . $ransomStatusValue . ')';
		}
		
		$partnerStatus 		 = (string) $order->state_id;
		$apileadStatus		 = $this->getApileadStatusName($partnerStatus);
		$comment			 = $order->call_comment;
//		$comment			 = 'Комментарий колл-центра: ' . ($order->call_comment ?? 'no data')
//			. $ransomInfo
//			. '; Трекномер заказа (в случае если он отправлен Почтой России): ' . ($order->track ?? 'no data');
		
		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($partnerId);
		
		return $apileadStatusDataModel;
	}
}