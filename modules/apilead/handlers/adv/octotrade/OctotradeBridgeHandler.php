<?php
namespace app\modules\apilead\handlers\adv\octotrade;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;
use Yii;

class OctotradeBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;

		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$phone = preg_replace('/\D/', '', $incomingModel->phone);
		$webId = $incomingModel->uHash ?: $incomingModel->wHash;
		
		$xml = <<<XML
<?xml version="1.0"?>
<orders>
	<order>
		<order_id>{$incomingModel->id}</order_id>
		<country>{$incomingModel->country}</country>
		<name>{$incomingModel->name}</name>
		<oblast></oblast>
		<city></city>
		<address>{$incomingModel->user_comment}</address>
		<email></email>
		<phone>{$phone}</phone>
		<ip>{$incomingModel->ip}</ip>
		<ua>{$incomingModel->user_agent}</ua>
		<uid>{$webId}</uid>
		<ref_source>{$incomingModel->source}</ref_source>
		<items>
			<item>
				<item_id>{$incomingModel->offer_id}</item_id>
				<price>{$incomingModel->landing_cost}</price>
				<quantity>1</quantity>
			</item>
		</items>
		<leadcost>{$incomingModel->cost}</leadcost>
	</order>
</orders>
XML;

		$this->requestData = [
			'date'	=> Yii::$app->formatter->asDate('now', 'YMMdd'),
			'pass'	=> $configUser->pass,
			'xml'	=> $xml,
		];

		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		libxml_use_internal_errors(true);
		$xml = simplexml_load_string($responseContent);
		
		if (!$xml) {
			throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
		}
		
		$id = (int) $xml->item->external_id ?? null;
		
		if ($id && is_int($id))
			return ['partner_order_id' => (string) $id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}