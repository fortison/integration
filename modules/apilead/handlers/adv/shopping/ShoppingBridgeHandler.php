<?php

namespace app\modules\apilead\handlers\adv\shopping;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use yii\web\HttpException;
use Yii;

class ShoppingBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;
		
		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$this->requestData = [
			"orders" => [
				"order" => [
					[
						"name"			=> $incomingModel->name,
						"phone"			=> $incomingModel->phone,
						"order_id"	    => $incomingModel->id,
						"country"		=> $incomingModel->country,
						"order_time"	=> time(),
						"order_source"	=> $configOffer->order_source,
						"summ_delivery"	=> $incomingModel->cost_delivery,
						"items" => [
							"item" => [
								[
									'item_id'  => $configOffer->item_id,
									'price'    => $incomingModel->landing_cost,
									'quantity' => 1,
								],
							],
						],
					],
				],
			],
		];
		
		$this->requestUrl = str_replace('{API_CODE}', $configUser->API_CODE, $config->getUrlOrderAdd());

		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$items	    = $json->items ?? null;
		
		if (!$items) {
			throw new HttpException('400', "Wrong partnerOrderId. Items empty: {{$responseContent}}");
		}
		
		$item = current($items) ?? null;
		
		if (!$item) {
			throw new HttpException('400', "Wrong partnerOrderId. Item empty: {{$responseContent}}");
		}
		
		$external_id = $item->external_id ?? null;
		
		if ($external_id)
			return ['partner_order_id' => (string) $external_id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}