<?php
namespace app\modules\apilead\handlers\adv\trafared;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class TrafaredBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;

		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$uid		= $configUser->uid;
		$secret		= $configUser->secret;
		$data		= $this->encode([
			'phone'			=> $incomingModel->phone,					// номер телефона заказчика товара (* Обязательное поле)
			'price'			=> $incomingModel->landing_cost,			// стоимость товара
			'order_id'		=> $incomingModel->id,						// ID заявки дл¤ синхронизации
			'name'			=> $incomingModel->name,					// ФИО заказчика товара
			'country'		=> strtolower($incomingModel->country),		// двухбуквенный (ISO 3166-1 alpha-2) код страны в нижнем регистре (* Обязательное поле)
			'addr'			=> $incomingModel->address,					// адрес
			'offer'			=> $configOffer->offerId,					// наименование товара (поле стандартизировано, техн. название дается в процессе согласования) (* Обязательное поле)
			'secret'		=> $secret									// ключ API (* Обязательное поле)
		]);
		
		$hashStr = strlen($data) . md5($uid);
		$hash = hash('sha256', $hashStr);
		
		$this->requestData = [
			'data' => $data,
		];

		$this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
			'uid'	=> $uid,
			'hash'	=> $hash,
		]);
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$success	= $json->result->success ?? null;
		$id			= $json->result->id ?? null;
		
		if ($success === "TRUE")
			return ['partner_order_id' => (string) $id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}