<?php
namespace app\modules\apilead\handlers\adv\monsterleads;

use app\common\models\OrderApilead;
use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\objects\PrepareGetRequest;
use app\modules\apilead\common\objects\PreparePostRequestJson;
use yii\httpclient\Client;
use yii\web\HttpException;

class MonsterleadsBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;

		$args = [$incomingModel, $config];

		$data = [
			'api_key' 			=> $configUser->apiKey,
			'tel'				=> $incomingModel->phone,
			'ip'				=> $incomingModel->ip,
			'code'				=> $configOffer->offerId,
			'traffic_type'		=> 0,
			'geo'				=> $incomingModel->country,
			'client'			=> $incomingModel->name,
			'adress'			=> $incomingModel->address,
			'foreign_value'		=> $incomingModel->id,
			'foreign_web_id'	=> $incomingModel->uHash ?: $incomingModel->wHash,
			'format'			=> 'json',
		];

		if (!empty($configOffer->comments)) {
			if (is_callable($configOffer->comments)) {
				$comments = call_user_func_array($configOffer->comments, $args);
			} else {
				$comments = $configOffer->comments;
			}

			$data['comments'] = $comments;
		}

		$this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query($data);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status		= $json->status ?? null;
		$errorCode	= $json->error_code ?? null;
		
		if ($status === 'ok')
			return ['partner_order_id' => (string) $json->lead_hash];
		else if ($status === 'error' && $errorCode == 438) {
			
			$incomingModel	= $this->incomingModel;
			$apiKey     	= $this->config->user->getApiKey();
			$errorMessage	= $json->error_msg ?? '';
			
			$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
			$apileadStatusDataModel->setId($incomingModel->id);
			$apileadStatusDataModel->setComment($errorMessage);
			$apileadStatusDataModel->setStatus(OrderApilead::STATUS_TRASH);
			
			$requestUrl		= $this->config->getUrlApileadOrderUpdate();
			$requestData    = $apileadStatusDataModel->getStatusData();
			
			$prepareRequest = new PreparePostRequestJson();
			$prepareRequest->setRequestUrl($requestUrl);
			$prepareRequest->setRequestData($requestData);
			$prepareRequest->setResponseFormat(Client::FORMAT_JSON);
			
			$responseModel = $this->statusRequest($prepareRequest);
			
			if (!$responseModel->error && $responseModel->code == 200)
				return ['system_status'=>OrderApilead::STATUS_TRASH];
			
			exit($responseModel->content);
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}