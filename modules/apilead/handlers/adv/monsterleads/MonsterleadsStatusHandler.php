<?php
namespace app\modules\apilead\handlers\adv\monsterleads;

use app\common\models\OrderApilead;
use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\handlers\base\BaseStatusHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\models\forms\ResponseModel;
use app\modules\apilead\common\objects\PrepareGetRequest;
use Yii;

class MonsterleadsStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$apileadStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($apileadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToApilead($apileadStatusDataModel, $needSendStatus);
		
		return $apileadStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$requestUrl = $this->config->getUrlOrderInfo() . '?' . http_build_query([
			'api_key'	=> $this->config->user->array['apiKey'],
			'lead_list' => $this->orderModel->partner_order_id,
			'search_by' => 'hash',
			'format'	=> 'json',
		]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		
		$data = $this->statusRequest($prepareRequest);
		
		return $data;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$sourceId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content		= $responseModel->content;
		
		$contentObject	= $this->decode($content);
		
		$order			= $contentObject[0] ?? null;
		
		if (!$order)
			throw new OrderErrorException($orderId, "Order not found in response {$sourceId}:{$apileadId}");
		
		if (!isset($order->status))
			throw new OrderErrorException($orderId, "Status not found for lead {$sourceId}:{$apileadId}");
		
		$partnerStatus 		= $order->status;
		$partnerSubStatus 	= $order->cancel_reason;
		$apileadStatus		= $this->getApileadStatus($partnerStatus, $partnerSubStatus);
		
		$comment = '';
		
		if ($apileadStatus == OrderApilead::STATUS_REJECT)
			$comment = $this->getApileadSubStatusComment($partnerSubStatus);
		
		$comment			.= $order->data4 ?? '';
		
		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($sourceId);
		
		return $apileadStatusDataModel;
	}
	
	public function getApileadStatus($partnerStatus, $partnerSubStatus)
	{
		$subStatuses	= $this->config->getSubStatuses();
		$trash			= $subStatuses[OrderApilead::STATUS_TRASH];
		
		if (array_key_exists($partnerSubStatus, $trash))																# спочатку перевірка на треш через підстатус
			return OrderApilead::STATUS_TRASH;
		
		return parent::getApileadStatusName($partnerStatus);
	}
	
	public function getApileadSubStatusComment($partnerSubStatus)
	{
		$subStatuses	= $this->config->getSubStatuses();
		$reject			= $subStatuses[OrderApilead::STATUS_REJECT];
		
		return $reject[$partnerSubStatus] ?? '';
	}
}