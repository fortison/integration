<?php
namespace app\modules\apilead\handlers\adv\mhordercreate;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class MhordercreateBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist()) {
			return $model;
		}

		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$args = [$incomingModel, $config];

		$this->requestData = [
            'name'          => $incomingModel->name,
            'phone'         => $incomingModel->phone,
            'self_key'      => $configUser->self_key,
            'area'    		=> $configOffer->area,
            'pn'    		=> $configOffer->pn,
			'transaction_id'=> $incomingModel->id,
            'ai'            => $incomingModel->uHash ?: $incomingModel->wHash,
		];
		
		if (!empty($configOffer->phone)) {
			if (is_callable($configOffer->phone))
				$phone = call_user_func_array($configOffer->phone, $args);
			else
				$phone = $configOffer->phone;

			$this->requestData['phone'] = $phone;
		}
		
		if (!empty($configOffer->address)) {
			if (is_callable($configOffer->address))
				$address = call_user_func_array($configOffer->address, $args);
			else
				$address = $configOffer->address;

			$this->requestData['address'] = $address;
		}

		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error) {
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		}

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200) {
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		}
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if ($json->message === 'ok') {
			return [];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}