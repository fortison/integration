<?php
namespace app\modules\apilead\handlers\adv\monadvert;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PrepareGetRequest;
use yii\web\HttpException;
use Yii;

class MonadvertBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;
		
		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
				'api_key' 			=> $configUser->api_key,
				'offer_id' 			=> $configOffer->offer_id,
				'click_id' 			=> $incomingModel->id,
				'name' 				=> $incomingModel->name,
				'phone' 			=> $incomingModel->phone,
				'address'			=> $incomingModel->address,
				'transaction_id'	=> $incomingModel->id,
				'affiliate_id'		=> $incomingModel->id,
				'payout'			=> $incomingModel->landing_cost,
			]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		$result = $json->msg;
		
		if (isset($json->msg) && $result == 'save data success!!')
			return ['partner_order_id' => ''];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}