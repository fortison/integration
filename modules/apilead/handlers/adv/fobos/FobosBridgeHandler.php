<?php
namespace app\modules\apilead\handlers\adv\fobos;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class FobosBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$xml = <<<XML
<?xml version="1.0"?>
<order>
   <fio>$incomingModel->name</fio>
   <address_region></address_region>
   <address_city></address_city>
   <address_street></address_street>
   <address_home></address_home>
   <address_apartment></address_apartment>
   <address_index></address_index>
   <phone>$incomingModel->phone</phone>
   <order_comment></order_comment>
   <country>$incomingModel->country</country>
   <lidprice>$incomingModel->cost</lidprice>
   <user_agent>$incomingModel->user_agent</user_agent>
   <user_ip>$incomingModel->ip</user_ip>
   <listProdData>
	   <prodId>{$configOffer->offerId}</prodId>
	   <price>$incomingModel->landing_cost</price>
	   <prodQuantity>1</prodQuantity>
   </listProdData>
   <partner_source_id>$incomingModel->wHash</partner_source_id>
</order>
XML;
		$this->requestData['pass'] 	= $configUser->pass[$configOffer->geo];
		$this->requestData['xml']	= $xml;
		
		$this->requestUrl = $config->getUrlOrderAdd() . "/{$configUser->partnerCode}";
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$data = simplexml_load_string($responseContent);
		
		if (!empty($data->orderId))
			return ['partner_order_id' => (string) $data->orderId];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}