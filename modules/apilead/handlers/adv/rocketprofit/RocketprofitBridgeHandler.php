<?php
namespace app\modules\apilead\handlers\adv\rocketprofit;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use yii\web\HttpException;

class RocketprofitBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist()) {
			return $model;
		}

		$incomingModel	= $this->incomingModel;

		$this->logApileadRequest($incomingModel->attributes);

		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;

		$this->requestUrl = $config->getUrlOrderAdd();
		$this->requestData = [
			'phone'			=> $incomingModel->phone,
			'name'			=> $incomingModel->name,
			'address'		=> $incomingModel->address,
			'country_code'	=> $incomingModel->country,
			'offer_id'		=> $configOffer->offerId,
			'id'			=> $incomingModel->id,
			'token'			=> $configUser->token,
			'partner_id'	=> $incomingModel->uHash ?: $incomingModel->wHash,
			'lead_price'	=> $incomingModel->cost,
			'ip'			=> $incomingModel->ip,
		];

		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);

		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();

		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

		$partnerOrderId		= $this->getOrderAttributes($responseContent);

		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		if ($responseContent === '')
			return [];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}