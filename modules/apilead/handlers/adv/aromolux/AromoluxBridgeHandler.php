<?php
namespace app\modules\apilead\handlers\adv\aromolux;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PrepareGetRequest;
use yii\web\HttpException;

class AromoluxBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;

		$this->logApileadRequest($incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		$offer			= 0;
		
		$data = array(
			"name" => preg_replace('/[^A-Za-zА-Яа-я0-9 _\-\+\&]/u','',$incomingModel->name),
			"phone" => preg_replace('/[^A-Za-zА-Яа-я0-9 _\-\+\&]/u','',$incomingModel->phone),
			"address" => preg_replace('/[^A-Za-zА-Яа-я0-9 _\-\+\&]/u','',$incomingModel->address), //$model->address,// str_replace(['&'], ['and'], $model->address),
			"purchases" => [
				[
					$incomingModel->id.'-'.$offer => $incomingModel->landing_cost.'-1',
				]
			]
		);

		$this->requestUrl = $config->getUrlOrderAdd() . "?" . http_build_query([
			'keyM'		=> $configUser->key,
			'orderM'	=> json_encode($data, JSON_UNESCAPED_UNICODE),
		]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status = $json->status ?? null;
		$id		= $json->order_id ?? null;
		
		if ($status == 'ok')
			return ['partner_order_id' => (string) $id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}