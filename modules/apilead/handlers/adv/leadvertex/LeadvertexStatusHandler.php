<?php
namespace app\modules\apilead\handlers\adv\leadvertex;

use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\handlers\base\BaseStatusHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\models\forms\ResponseModel;
use app\modules\apilead\common\objects\PrepareGetRequest;
use Yii;

/**
 * Обробник статусів замовлень до інтеграції Leadvertext
 *
 * User: nagard
 * Date: 31.10.17
 * Time: 14:01
 */
class LeadvertexStatusHandler extends BaseStatusHandler
{
	/**
	 * @inheritdoc
	 */
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$apileadStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($apileadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToApilead($apileadStatusDataModel, $needSendStatus);
		
		return $apileadStatusDataModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function makeStatusRequest()
	{
		$requestUrl = $this->config->getUrlOrderInfo() . '?' . http_build_query([
			'webmasterID'	=> $this->config->offer->array['webmasterID'] ?? $this->config->user->array['webmasterID'],
			'token'			=> $this->config->offer->array['token'],
			'ids'			=> $this->orderModel->partner_order_id,
		]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);

		$data = $this->statusRequest($prepareRequest);

		return $data;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$sourceId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$apileadId}");
		
		$content		= $responseModel->content;
		
		try {
			$contentObject	= $this->decode($content);
		} catch (\Exception $e) {
			throw new OrderErrorException($orderId, "Decode json error: {$e->getMessage()}");
		}
		
		if (!isset($contentObject->{$sourceId}))
			throw new OrderErrorException($orderId, "Lead not found {$sourceId}:{$apileadId}");
		
		$contentObject = $contentObject->{$sourceId};
		
//		if (!isset($contentObject->fields->comment))
//			throw new HttpException('405', "Comment not found for lead {$sourceId}:{$apileadId}");
		
		if (!isset($contentObject->status))
			throw new OrderErrorException($orderId, "Status is not available for you {$sourceId}:{$apileadId}");

		
		$comment		= $contentObject->fields->comment ?? '';
		$roboLog		= $contentObject->roboLog ?? '';
		$comment		= "comment: {$comment}; \nroboLog: {$roboLog}";
		$partnerStatus 	= (string) $contentObject->status;
		$apileadStatus	= $this->getApileadStatusName($partnerStatus);

		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($sourceId);

		return $apileadStatusDataModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getRansomStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$sourceId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$apileadId}");
		
		$content	= $responseModel->content;
		
		$contentObject = $this->decode($content);
		
		if (!isset($contentObject->{$sourceId}))
			throw new OrderErrorException($orderId, "Lead not found {$sourceId}:{$apileadId}");
		
		$contentObject = $contentObject->{$sourceId};
		
		if (!isset($contentObject->status))
			throw new OrderErrorException($orderId, "Status is not available for you {$sourceId}:{$apileadId}");
		
		$comment		= $contentObject->fields->comment ?? '';
		$partnerStatus 	= $contentObject->status;
		$apileadStatus	= $this->getRansomStatusName($partnerStatus);

		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($sourceId);

		return $apileadStatusDataModel;
	}
}