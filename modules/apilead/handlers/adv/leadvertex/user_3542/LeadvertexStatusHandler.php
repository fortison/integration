<?php
namespace app\modules\apilead\handlers\adv\leadvertex\user_3542;

use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\apilead\common\models\forms\ResponseModel;

/**
 * Обробник статусів замовлень інтеграції Leadvertext рекламодавця 3542
 *
 * User: nagard
 * Date: 31.10.17
 * Time: 14:01
 */
class LeadvertexStatusHandler extends \app\modules\apilead\handlers\adv\leadvertex\LeadvertexStatusHandler
{
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$sourceId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$apileadId}");
		
		$content		= $responseModel->content;
		
		try {
			$contentObject	= $this->decode($content);
		} catch (\Exception $e) {
			throw new OrderErrorException($orderId, "Decode json error: {$e->getMessage()}");
		}
		
		if (!isset($contentObject->{$sourceId}))
			throw new OrderErrorException($orderId, "Lead not found {$sourceId}:{$apileadId}");
		
		$contentObject = $contentObject->{$sourceId};

//		if (!isset($contentObject->fields->comment))
//			throw new HttpException('405', "Comment not found for lead {$sourceId}:{$apileadId}");
		
		if (!isset($contentObject->status))
			throw new OrderErrorException($orderId, "Status is not available for you {$sourceId}:{$apileadId}");
		
		$comment		= $contentObject->fields->additional4 ?? '';
		$partnerStatus 	= $contentObject->status;
		$apileadStatus	= $this->getApileadStatusName($partnerStatus);
		
		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($sourceId);
		
		return $apileadStatusDataModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getRansomStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$sourceId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$apileadId}");
		
		$content	= $responseModel->content;
		
		$contentObject = $this->decode($content);
		
		if (!isset($contentObject->{$sourceId}))
			throw new OrderErrorException($orderId, "Lead not found {$sourceId}:{$apileadId}");
		
		$contentObject = $contentObject->{$sourceId};
		
		if (!isset($contentObject->status))
			throw new OrderErrorException($orderId, "Status is not available for you {$sourceId}:{$apileadId}");
		
		$comment		= $contentObject->fields->additional4 ?? '';
		$partnerStatus 	= $contentObject->status;
		$apileadStatus	= $this->getRansomStatusName($partnerStatus);
		
		$apileadStatusDataModel = new ApileadStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($sourceId);
		
		return $apileadStatusDataModel;
	}
}