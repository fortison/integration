<?php
namespace app\modules\apilead\handlers\adv\leadvertex\user_4444;

use app\modules\apilead\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\apilead\common\objects\PreparePostRequest;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use yii\helpers\Json;
use yii\web\HttpException;
use Yii;

class LeadvertexBridgeHandler extends BaseBridgeHandler
{
	public function getPersName($webId)
	{
		if ($webId == '2602171') {
			return 'Тамара Глоба';
		}
		if ($webId == '10697') {
			return 'Алёна Курилова';
		}
		if ($webId == '13004') {
			return 'Баба Нина';
		}
		return null;
	}
	
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logApileadRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel		= $this->incomingModel;
		
		$args = [$incomingModel, $config];
		
		$this->requestData = [
			"fio" 			=> $incomingModel->name,
			"country" 		=> $incomingModel->country,
			"phone" 		=> $incomingModel->phone,
			"timezone" 		=> ($incomingModel->tz*60) * -1,
			"price" 		=> $incomingModel->landing_cost,
			"total" 		=> $incomingModel->landing_cost,
			'additional15'	=> $this->getPersName($incomingModel->user_comment),
			"ip" 			=> $incomingModel->ip,
			"quantity" 		=> 1,
		];
		
		if (!empty($configOffer->wmSumApi)) {
			if (is_callable($configOffer->wmSumApi))
				$wmSumApi = call_user_func_array($configOffer->wmSumApi, $args);
			else
				$wmSumApi = $configOffer->wmSumApi;
			
			$this->requestData['wmSumApi'] = $wmSumApi;
		}
		
		if (!empty($configOffer->referer)) {
			if (is_callable($configOffer->referer))
				$referer = call_user_func_array($configOffer->referer, $args);
			else
				$referer = $configOffer->referer;
			
			$this->requestData['referer'] = $referer;
		}
		
		if (!empty($configOffer->address)) {
			if (is_callable($configOffer->address))
				$address = call_user_func_array($configOffer->address, $args);
			else
				$address = $configOffer->address;
			
			$this->requestData['address'] = $address;
		}
		
		if ($configOffer->configs['parsePhone'] ?? null) {
			$phoneConfig = $configOffer->configs['parsePhone'];
			$phoneRegion = $incomingModel->country;
			
			if ($phoneConfig['region'] ?? null) {
				if (is_callable($phoneConfig['region'])) {
					$phoneRegion = call_user_func_array($phoneConfig['region'], $args);
				} else {
					$phoneRegion = $phoneConfig['region'];
				}
			}
			
			$phoneNumber = $incomingModel->phone;
			
			if (is_callable($phoneConfig['number'] ?? null)) {
				$phoneNumber = call_user_func_array($phoneConfig['number'], $args);
			}
			
			if ($phoneConfig['format'] === 'e164') {
				$phoneFormat = PhoneNumberFormat::E164;
			} elseif ($phoneConfig['format'] === 'national') {
				$phoneFormat = PhoneNumberFormat::NATIONAL;
			} elseif ($phoneConfig['format'] === 'rfc3966') {
				$phoneFormat = PhoneNumberFormat::RFC3966;
			} else {
				$phoneFormat = PhoneNumberFormat::INTERNATIONAL;
			}
			
			try {
				$phoneParser = PhoneNumberUtil::getInstance();
				$phoneProto = $phoneParser->parse($phoneNumber, $phoneRegion);
				$phoneNumber = $phoneParser->format($phoneProto, $phoneFormat);
			} catch (\Exception $e) {
				$phoneNumber = $incomingModel->phone;
			}
			
			$this->requestData['phone'] = $phoneNumber;
		}
		
		if (!empty($configOffer->country)) {
			if (is_callable($configOffer->country))
				$country = call_user_func_array($configOffer->country, $args);
			else
				$country = $configOffer->country;
			
			$this->requestData['country'] = $country;
		}
		
		if (!empty($configOffer->externalWebmaster)) {
			if (is_callable($configOffer->externalWebmaster))
				$externalWebmaster = call_user_func_array($configOffer->externalWebmaster, $args);
			else
				$externalWebmaster = $configOffer->externalWebmaster;
			
			$this->requestData['externalWebmaster'] = $externalWebmaster;
		}
		
		if (!empty($configOffer->externalID)) {
			if (is_callable($configOffer->externalID))
				$externalID = call_user_func_array($configOffer->externalID, $args);
			else
				$externalID = $configOffer->externalID;
			
			$this->requestData['externalID'] = $externalID;
		}
		
		if (!empty($configOffer->ip)) {
			if (is_callable($configOffer->ip))
				$ip = call_user_func_array($configOffer->ip, $args);
			else
				$ip = $configOffer->ip;
			
			$this->requestData['ip'] = $ip;
		}
		
		for ($i = 1; $i <= 25; $i++) {
			$field = 'additional' . $i;
			if (!empty($configOffer->$field)) {
				if (is_callable($configOffer->$field))
					$this->requestData[$field] = call_user_func_array($configOffer->$field, $args);
				else
					$this->requestData[$field] = $configOffer->$field;
			}
		}
		
		if (!empty($configOffer->domain)) {
			if (is_callable($configOffer->domain))
				$domain = call_user_func_array($configOffer->domain, $args);
			else
				$domain = $configOffer->domain;
			
			$this->requestData['domain'] = $domain;
		}
		
		if (!empty($configOffer->comment)) {
			if (is_callable($configOffer->comment))
				$comment = call_user_func_array($configOffer->comment, $args);
			else
				$comment = $configOffer->comment;
			
			$this->requestData['comment'] = $comment;
		}
		
		if (!empty($configOffer->goodId)) {
			if (is_callable($configOffer->goodId))
				$goodId = call_user_func_array($configOffer->goodId, $args);
			else
				$goodId = $configOffer->goodId;
			
			$this->requestData['goods'] = [[
				'goodID' => $goodId,
				'quantity' => 1,
				'price' => $incomingModel->landing_cost,
			]];
		}
		
		if (!empty($configOffer->price)) {
			if (is_callable($configOffer->price))
				$price = call_user_func_array($configOffer->price, $args);
			else
				$price = $configOffer->price;
			
			$this->requestData['price'] = $price;
		}
		
		if (!empty($configOffer->total)) {
			if (is_callable($configOffer->total))
				$total = call_user_func_array($configOffer->total, $args);
			else
				$total = $configOffer->total;
			
			$this->requestData['total'] = $total;
		}
		
		if (!empty($configOffer->region)) {
			if (is_callable($configOffer->region))
				$region = call_user_func_array($configOffer->region, $args);
			else
				$region = $configOffer->region;
			
			$this->requestData['region'] = $region;
		}
		
		if (!empty($configOffer->city)) {
			if (is_callable($configOffer->city))
				$city = call_user_func_array($configOffer->city, $args);
			else
				$city = $configOffer->city;
			
			$this->requestData['city'] = $city;
		}
		
		if (!empty($configOffer->email)) {
			if (is_callable($configOffer->email))
				$email = call_user_func_array($configOffer->email, $args);
			else
				$email = $configOffer->email;
			
			$this->requestData['email'] = $email;
		}
		
		if (!empty($configOffer->email)) {
			if (is_callable($configOffer->email))
				$email = call_user_func_array($configOffer->email, $args);
			else
				$email = $configOffer->email;
			
			$this->requestData['email'] = $email;
		}
		
		$this->requestUrl = $config->getUrlOrderAdd().'?'.http_build_query([
				'webmasterID' => $configOffer->webmasterID ?? $configUser->webmasterID,
				'token' => $configOffer->token,
			]);
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if (!empty($this->response->error))
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode} Response: {$responseContent}");
		
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (!isset($json->OK))
			throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
		
		return ['partner_order_id' => (string) $json->OK];
	}
}