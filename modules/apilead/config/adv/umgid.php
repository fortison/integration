<?php

/*
У файлі

https://umgid.com/streams_api/guide_view

zvv1404@gmail.com
No573VEKE7z1Uj5x1Dlq71abcKH8MSRL

http://connectcpa.net/apilead/adv/adv-postback-status?systemUserId=10432&partnerStatus=trash&systemOrderId={subid1}&comment=Статус ожидаемой цели 0 - обработка 1 - цель достигнута 2 - цель не достигнута: { main_status }; 1 - подтвержден 2 - отказ 3 - аннулирован (фрод): {target1}; 4 - выкуп 5 - не выкуп (отклонен): {target2}&postbackApiKey=fa6b405367dcacf64412e363a72e8651
 */

return [
	10432 => [
		'apileadApiKey'				=> 'fa6b405367dcacf64412e363a72e865e',
		'postbackApiKey'			=> 'fa6b405367dcacf64412e363a72e8651',
		'key' 						=> 'nh0su1cnz58ikv7a',
		
		'offers' => [
			
			4595 => [
				'offer_id'	=> 413,
				'payment_id' => 752,
				'product_id'=> 573,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4596 => [
				'offer_id'	=> 278,
				'payment_id' => 776,
				'product_id'=> 398,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3659 => [
				'offer_id'	=> 90,
				'payment_id' => 189,
				'product_id'=> 119,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3660 => [
				'offer_id'	=> 90,
				'payment_id' => 190,
				'product_id'=> 119,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3933 => [
				'offer_id'	=> 267,
				'payment_id' => 449,
				'product_id'=> 383,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4352 => [
				'offer_id'	=> 409,
				'payment_id' => 682,
				'product_id'=> 591,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4474 => [
				'offer_id'	=> 409,
				'payment_id' => 688,
				'product_id'=> 591,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'		=> '',
			],
			'reject'	=> [
				'reject'	=> '',
			],
			'expect'	=> [
				'expect'	=> '',
			],
			'confirm'	=> [
				'confirm'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://umgid.com/api/order_for_stream',
		'urlOrderInfo'		=> '',
	],
];

?>