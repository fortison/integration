<?php

/**
 ** https://scamcoin.me/help/api.php#ext
 */

return [
	
	12978 => [
		'apileadApiKey'				=> '918f8ffc13d31932149bf3ef27c3422e',
		'user' 						=> '31',
		'key' 						=> '88658f18580b73cdc89fd469b90dfb56',
		
		'offers' => [
			4514 => [
				'offer_id' 	=> '2',
				'flow'		=> '149',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4513 => [
				'offer_id' 	=> '8',
				'flow'		=> '116',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'	=> '',
			],
			'reject'	=> [
				'cancel'	=> '',
			],
			'expect'	=> [
				'wait'		=> '',
				'hold'		=> '',
			],
			'confirm'	=> [
				'approve'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://scamcoin.me/api/wm/push.json',
		'urlOrderInfo'		=> 'https://scamcoin.me/api/wm/lead.json',
	],
];

?>