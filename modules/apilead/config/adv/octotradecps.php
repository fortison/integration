<?php

/*
 * http://octotrade.ru/api_lead.php
 */

return [
	
	2323 => [
		'apileadApiKey'				=> '4695b36a26451cbd191ca6658bd71303',
		'pass'						=> 'sUNKCeoR3l22',
		
		'offers' => [
			2323 => [
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'https://octotrade.ru/api/',
			'urlOrderInfo'		=> 'https://octotrade.ru/api/',
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'		=> [
				4	=> '',
			],
			'reject'	=> [
				0	=> '',
			],
			'expect'	=> [
				1	=> '',
				2	=> '',
			],
			'confirm'	=> [
				3	=> '',
			],
		],
		'ransom' => [
			'confirm'	=> [
				'4'		=> 'Выкуплен',
			],
			'reject'	=> [
				'5'		=> 'Не выкуп (возврат)',
			],
			'expect'	=> [
				'1'		=> 'Заказ в пути',
				'2'     => 'Заказ ждет выкупа',
				'3'		=> 'Истекает срок хранения на почте',
			],
		],
		'brakeLogFolder'			=> true,
	],
];
