<?php

/*
---------------------------
ADD LEAD
---------------------------

URL: https://azurecrm.pl/api/add_lp_phone/PHONE_NUMBER_HERE/COUNTRY_ID_HERE/PRODUCT_ID_HERE/

All variables which are passed by the URL are in form mentioned above.
Additionally there should be passed 3 variables by POST:
- first_name
- last_name
- external_partners_id - must be number "2"


Example usage of API in Javascript:

$.ajax({
    type: "POST",
    url: "http://azurecrm.pl/api/add_lp_phone/666555444/4/23",
    dataType : "json",
    data: {
        first_name: 'Michael',
        last_name: 'Johnson',
        external_partners_id: 3
    },
    success: function (ret) {},
    complete: function () {},
    error: function (jqXHR, errorText, errorThrown) {}
});

---------------------------
GET STATUS
---------------------------

https://azurecrm.pl/api/check_external_lead
lead_id
company_id
 */

return [
	2323 => [
		'apileadApiKey'				=> '00000000000000000000000000000000',
		'external_partners_id'		=> 9,
		'api_key'					=> '610bd2afb05ef',
		
		
		'offers' => [
			2323 => [
				'product_id' => '23',
				'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'4' => 'trash',
			],
			'reject'	=> [
				'2' => 'rejected',
			],
			'expect'	=> [
				'1' => 'pending',
			],
			'confirm'	=> [
				'3' => 'collected',
			],
		],
		
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://azurecrm.pl/api/add_lp_phone',
		'urlOrderInfo'		=> 'https://azurecrm.pl/api/check_external_lead',
	],
];