<?php

/*
 * https://docs.google.com/document/d/1uoxzThVucIKSryoQ73SeYfqBy6EBS-i_yy5IPX_q8GU
 *
 * 0 - Россия;
 * 1 - Беларусь;
 * 2 - Казахстан;
 * 3 - Украина;
 * 4 - Киргизия,
 * 6 - Азербайджан,
 * 14 - Армения,
 * 15 - Узбекистан
 *
 */

return [
	
	3555 => [
		'apileadApiKey'				=> '445ddf4931e37877519cf6b2eadc0b23',
		'apiKey' 					=> '0a26bfbcc25fdeaa77140a9ca5537466',
		
		'offers' => [
			1562 => [
				'offerId'	=> 23,
				'geoId'		=> 2,
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3054 => [
				'offerId'	=> 1103,
				'geoId'		=> 14,
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1129 => [
				'offerId'	=> 70,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1837 => [
				'offerId'	=> 182,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1857 => [
				'offerId'	=> 201,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1859 => [
				'offerId'	=> 201,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1854 => [
				'offerId'	=> 57,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1855 => [
				'offerId'	=> 57,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			801 => [
				'offerId'	=> 70,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1880 => [
				'offerId'	=> 198,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1881 => [
				'offerId'	=> 198,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1882 => [
				'offerId'	=> 198,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1877 => [
				'offerId'	=> 193,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1878 => [
				'offerId'	=> 193,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1879 => [
				'offerId'	=> 193,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1885 => [
				'offerId'	=> 207,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1464 => [
				'offerId'	=> 79,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			168 => [
				'offerId'	=> 152,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1979 => [
				'offerId'	=> 193,
				'geoId'		=> 3,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1978 => [
				'offerId'	=> 198,
				'geoId'		=> 3,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2020 => [
				'offerId'	=> 302,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2021 => [
				'offerId'	=> 244,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2190 => [
				'offerId'	=> 244,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2054 => [
				'offerId'	=> 344,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2056 => [
				'offerId'	=> 344,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2002 => [
				'offerId'	=> 310,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			886 => [
				'offerId'	=> 25,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2084 => [
				'offerId'	=> 244,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2111 => [
				'offerId'	=> 331,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2092 => [
				'offerId'	=> 344,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2117 => [
				'offerId'	=> 410,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2143 => [
				'offerId'	=> 220,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2154 => [
				'offerId'	=> 220,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2158 => [
				'offerId'	=> 226,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2119 => [
				'offerId'	=> 226,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2145 => [
				'offerId'	=> 111,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2144 => [
				'offerId'	=> 111,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2168 => [
				'offerId'	=> 336,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2176 => [
				'offerId'	=> 482,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2177 => [
				'offerId'	=> 482,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2185 => [
				'offerId'	=> 476,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2184 => [
				'offerId'	=> 476,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2186 => [
				'offerId'	=> 476,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2148 => [
				'offerId'	=> 470,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2219 => [
				'offerId'	=> 470,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2217 => [
				'offerId'	=> 470,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2218 => [
				'offerId'	=> 470,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2151 => [
				'offerId'	=> 35,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2204 => [
				'offerId'	=> 502,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2229 => [
				'offerId'	=> 502,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2230 => [
				'offerId'	=> 502,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2233 => [
				'offerId'	=> 122,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2234 => [
				'offerId'	=> 122,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2236 => [
				'offerId'	=> 1103,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2237 => [
				'offerId'	=> 548,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2231 => [
				'offerId'	=> 262,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2215 => [
				'offerId'	=> 262,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2097 => [
				'offerId'	=> 361,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2245 => [
				'offerId'	=> 559,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2247 => [
				'offerId'	=> 400,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2248 => [
				'offerId'	=> 400,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2249 => [
				'offerId'	=> 400,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2312 => [
				'offerId'	=> 624,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2258 => [
				'offerId'	=> 564,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2259 => [
				'offerId'	=> 564,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2260 => [
				'offerId'	=> 624,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2267 => [
				'offerId'	=> 577,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2266 => [
				'offerId'	=> 577,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2273 => [
				'offerId'	=> 599,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2272 => [
				'offerId'	=> 599,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2276 => [
				'offerId'	=> 592,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2277 => [
				'offerId'	=> 592,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2278 => [
				'offerId'	=> 595,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2282 => [
				'offerId'	=> 638,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2283 => [
				'offerId'	=> 638,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2284 => [
				'offerId'	=> 638,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2285 => [
				'offerId'	=> 638,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2333 => [
				'offerId'	=> 512,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2334 => [
				'offerId'	=> 512,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2335 => [
				'offerId'	=> 512,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2355 => [
				'offerId'	=> 608,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2356 => [
				'offerId'	=> 608,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3297 => [
				'offerId'	=> 608,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2365 => [
				'offerId'	=> 707,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2297 => [
				'offerId'	=> 682,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2298 => [
				'offerId'	=> 682,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2275 => [
				'offerId'	=> 758,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2323 => [
				'offerId'	=> 638,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2282 => [
				'offerId'	=> 638,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2090 => [
				'offerId'	=> 256,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2390 => [
				'offerId'	=> 801,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2414 => [
				'offerId'	=> 185,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2415 => [
				'offerId'	=> 884,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2416 => [
				'offerId'	=> 884,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2441 => [
				'offerId'	=> 31,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2442 => [
				'offerId'	=> 31,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1460 => [
				'offerId'	=> 31,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2436 => [
				'offerId'	=> 178,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1566 => [
				'offerId'	=> 893,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2452 => [
				'offerId'	=> 892,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2201 => [
				'offerId'	=> 841,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2223 => [
				'offerId'	=> 752,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2124 => [
				'offerId'	=> 111,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],			
			2125 => [
				'offerId'	=> 293,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],			
			2468 => [
				'offerId'	=> 999,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],			
			2469 => [
				'offerId'	=> 999,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],			
			2470 => [
				'offerId'	=> 999,
				'geoId'		=> 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],			
			2471 => [
				'offerId'	=> 999,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],			
			2198 => [
				'offerId'	=> 727,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],			
			2473 => [
				'offerId'	=> 727,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],			
			2033 => [
				'offerId'	=> 293,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],			
			2493 => [
				'offerId'	=> 935,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],			
			995 => [
				'offerId'	=> 60,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],			
			2456 => [
				'offerId'	=> 935,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],			
			2505 => [
				'offerId'	=> 1117,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],			
			2515 => [
				'offerId'	=> 1117,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],			
			2514 => [
				'offerId'	=> 1117,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],			
			2516 => [
				'offerId'	=> 1117,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],			
			2053 => [
				'offerId'	=> 1142,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],			
			2500 => [
				'offerId'	=> 603,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1546 => [
				'offerId'	=> 24,
				'geoId'		=> 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2083 => [
				'offerId'	=> 1199,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2534 => [
				'offerId'	=> 353,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2547 => [
				'offerId'	=> 1118,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2552 => [
				'offerId'	=> 1118,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2524 => [
				'offerId'	=> 1118,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2536 => [
				'offerId'	=> 1118,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2537 => [
				'offerId'	=> 1118,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2541 => [
				'offerId'	=> 1118,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2149 => [
				'offerId'	=> 1091,
				'geoId'		=> 15,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1840 => [
				'offerId'	=> 1126,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2556 => [
				'offerId'	=> 1118,
				'geoId'		=> 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2603 => [
				'offerId'	=> 1247,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2607 => [
				'offerId'	=> 1390,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2608 => [
				'offerId'	=> 1247,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2609 => [
				'offerId'	=> 1247,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2530 => [
				'offerId'	=> 1118,
				'geoId'		=> 3,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2211 => [
				'offerId'	=> 293,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2635 => [
				'offerId'	=> 1239,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2636 => [
				'offerId'	=> 1239,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2637 => [
				'offerId'	=> 1239,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2638 => [
				'offerId'	=> 1239,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2412 => [
				'offerId'	=> 49,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2394 => [
				'offerId'	=> 893,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2647 => [
				'offerId'	=> 1306,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2639 => [
				'offerId'	=> 1306,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2615 => [
				'offerId'	=> 1239,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2676 => [
				'offerId'	=> 1406,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2678 => [
				'offerId'	=> 1332,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2679 => [
				'offerId'	=> 1332,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2680 => [
				'offerId'	=> 1332,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2131 => [
				'offerId'	=> 1406,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2601 => [
				'offerId'	=> 1229,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2695 => [
				'offerId'	=> 353,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2697 => [
				'offerId'	=> 367,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2364 => [
				'offerId'	=> 592,
				'geoId'		=> 3,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2544 => [
				'offerId'	=> 1361,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2707 => [
				'offerId'	=> 367,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2722 => [
				'offerId'	=> 367,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2743 => [
				'offerId'	=> 1306,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2744 => [
				'offerId'	=> 863,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2004 => [
				'offerId'	=> 1181,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2002 => [
				'offerId'	=> 354,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2094 => [
				'offerId'	=> 70,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2758 => [
				'offerId'	=> 1514,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2774 => [
				'offerId'	=> 42,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2783 => [
				'offerId'	=> 1501,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2699 => [
				'offerId'	=> 1501,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2784 => [
				'offerId'	=> 1501,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2786 => [
				'offerId'	=> 1501,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2785 => [
				'offerId'	=> 1501,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2788 => [
				'offerId'	=> 1301,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2789 => [
				'offerId'	=> 1301,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2790 => [
				'offerId'	=> 1072,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2794 => [
				'offerId'	=> 893,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2588 => [
				'offerId'	=> 1450,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			163 => [
				'offerId'	=> 298,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2811 => [
				'offerId'	=> 1556,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2812 => [
				'offerId'	=> 1556,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2813 => [
				'offerId'	=> 1556,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2814 => [
				'offerId'	=> 1556,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2815 => [
				'offerId'	=> 1556,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2819 => [
				'offerId'	=> 790,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2820 => [
				'offerId'	=> 790,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2828 => [
				'offerId'	=> 1561,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2829 => [
				'offerId'	=> 1561,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			499 => [
				'offerId'	=> 1531,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2835 => [
				'offerId'	=> 1561,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2836 => [
				'offerId'	=> 1561,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2837 => [
				'offerId'	=> 1561,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2843 => [
				'offerId'	=> 70,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2779 => [
				'offerId'	=> 1548,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2856 => [
				'offerId'	=> 1548,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1442 => [
				'offerId'	=> 36,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1478 => [
				'offerId'	=> 36,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2870 => [
				'offerId'	=> 1477,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2905 => [
				'offerId'	=> 1253,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2906 => [
				'offerId'	=> 1253,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2908 => [
				'offerId'	=> 589,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2909 => [
				'offerId'	=> 589,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2604 => [
				'offerId'	=> 1629,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2901 => [
				'offerId'	=> 413,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2926 => [
				'offerId'	=> 413,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2930 => [
				'offerId'	=> 413,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2929 => [
				'offerId'	=> 413,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2852 => [
				'offerId'	=> 1477,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2848 => [
				'offerId'	=> 1597,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2933 => [
				'offerId'	=> 1584,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2935 => [
				'offerId'	=> 1584,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2936 => [
				'offerId'	=> 1584,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2900 => [
				'offerId'	=> 413,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3015 => [
				'offerId'	=> 1807,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3027 => [
				'offerId'	=> 1849,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3028 => [
				'offerId'	=> 1849,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3029 => [
				'offerId'	=> 1849,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3030 => [
				'offerId'	=> 1849,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3031 => [
				'offerId'	=> 1849,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3035 => [
				'offerId'	=> 790,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2538 => [
				'offerId'	=> 1118,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1738 => [
				'offerId'	=> 164,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1739 => [
				'offerId'	=> 164,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2499 => [
				'offerId'	=> 1088,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3099 => [
				'offerId'	=> 244,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3100 => [
				'offerId'	=> 244,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2961 => [
				'offerId'	=> 1683,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1381 => [
				'offerId'	=> 893,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2649 => [
				'offerId'	=> 1945,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2559 => [
				'offerId'	=> 1952,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2567 => [
				'offerId'	=> 1952,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3110 => [
				'offerId'	=> 1952,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3112 => [
				'offerId'	=> 1534,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3113 => [
				'offerId'	=> 1534,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3114 => [
				'offerId'	=> 1534,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3115 => [
				'offerId'	=> 1534,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3116 => [
				'offerId'	=> 1534,
				'geoId'		=> 12,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3111 => [
				'offerId'	=> 1952,
				'geoId'		=> 12,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3137 => [
				'offerId'	=> 1988,
				'geoId'		=> 12,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3136 => [
				'offerId'	=> 1988,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3135 => [
				'offerId'	=> 1988,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3134 => [
				'offerId'	=> 1988,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3133 => [
				'offerId'	=> 1988,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2562 => [
				'offerId'	=> 1952,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3012 => [
				'offerId'	=> 1807,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3174 => [
				'offerId'	=> 1741,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3175 => [
				'offerId'	=> 1741,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3176 => [
				'offerId'	=> 1741,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3177 => [
				'offerId'	=> 1741,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3201 => [
				'offerId'	=> 2040,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3202 => [
				'offerId'	=> 2040,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2614 => [
				'offerId'	=> 1894,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3275 => [
				'offerId'	=> 2124,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3234 => [
				'offerId'	=> 2124,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3286 => [
				'offerId'	=> 2124,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3283 => [
				'offerId'	=> 2124,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3284 => [
				'offerId'	=> 2124,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2962 => [
				'offerId'	=> 1674,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2962 => [
				'offerId'	=> 1674,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3404 => [
				'offerId'	=> 1630,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3086 => [
				'offerId'	=> 1825,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3088 => [
				'offerId'	=> 1591,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3089 => [
				'offerId'	=> 1591,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3091 => [
				'offerId'	=> 1591,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2061 => [
				'offerId'	=> 381,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2062 => [
				'offerId'	=> 381,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3445 => [
				'offerId'	=> 2420,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3446 => [
				'offerId'	=> 2420,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3447 => [
				'offerId'	=> 2420,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3466 => [
				'offerId'	=> 2475,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3467 => [
				'offerId'	=> 2474,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3480 => [
				'offerId'	=> 1411,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2995 => [
				'offerId'	=> 1807,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3501 => [
				'offerId'	=> 1129,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3502 => [
				'offerId'	=> 1129,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3503 => [
				'offerId'	=> 1129,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3504 => [
				'offerId'	=> 1129,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3505 => [
				'offerId'	=> 1129,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3509 => [
				'offerId'	=> 2286,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3550 => [
				'offerId'	=> 2273,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2342 => [
				'offerId'	=> 157,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3611 => [
				'offerId'	=> 2539,
				'geoId'		=> 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3612 => [
				'offerId'	=> 2539,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3613 => [
				'offerId'	=> 2539,
				'geoId'		=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3024 => [
				'offerId'	=> 1547,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3681 => [
				'offerId'	=> 1547,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3682 => [
				'offerId'	=> 38,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3683 => [
				'offerId'	=> 38,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3684 => [
				'offerId'	=> 38,
				'geoId'		=> 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3092 => [
				'offerId'	=> 1088,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3614 => [
				'offerId'	=> 1445,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3724 => [
				'offerId'	=> 1445,
				'geoId'		=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3725 => [
				'offerId'	=> 1445,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3814 => [
				'offerId'	=> 2339,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1557 => [
				'offerId'	=> 86,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3858 => [
				'offerId'	=> 1466,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			214 => [
				'offerId'	=> 1719,
				'geoId'		=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		'configs' => [
			'statuses' => [
				'reject'	=> [
					'-1'	=> '',
				],
				'expect'	=> [
					'-2'	=> '',
				],
				'confirm'	=> [
					1	=> '',
				],
				'trash'	=> [
					2		=> '',
				],
			],
			'subStatuses' => [
				'reject'	=> [
//					'-1'	=> '',
					1		=> 'Клиент отказался',
					2		=> 'Клиент отказался подтверждать заказ, утверждает что нашел дешевле',
					3		=> 'Клиент отказался подтверждать заказ, не устроили условия доставки',
					6		=> 'Клиент отказался подтверждать заказ, утверждает что просто хотел проконсультироваться по товару',
					8		=> 'Абонент недоступен больше недели, не удалось дозвониться до клиента с 32х попыток',
					12	=> 'Клиент отказался подтверждать заказ, утверждает что его не устраивает цена, дорого...',
					13	=> 'Клиент отказался подтверждать заказ, утверждает что передумал',
					15	=> 'Не удалось подтвердить заказ. Клиент не говорит ни на одном из языков доступном нашему КЦ',
				],
				'expect'	=> [
//					'-2'		=> '',
					1		=> 'Новый заказ, ожидает обработки',
					4		=> 'Мы дозвонились до клиента, но он попросил дать ему время подумать ,установлен перезвон на комфортное для клиента время',
					5		=> 'Мы дозвонились до клиента, но он попросил перезвонить позже, установлен перезвон на комфортное для клиента время',
				],
				'confirm'	=> [
//					1	=> '',
					0		=> 'Заказ подтвержден',
					1		=> 'Заказ подтвержден',
					2		=> 'Посылка отправлена получателю, находится в пути',
					3		=> 'Клиент отказался выкупать посылку',
					6		=> 'Посылка выкуплена',
					7		=> 'Посылка частично выкуплена',
				],
				'trash'	=> [
//					2		=> '',
					1		=> 'Ошибка/Спам/Дубль',
					2		=> 'Отклонили заказ по причине не корректных данных в заявке',
					3		=> 'Дубль. Заказ отклонен по той причине что по данному клиенту уже имеется подтвержденный заказ на этот же оффер за последние 3 дня',
					4		=> 'Тестовая заявка(отклонена)',
					5		=> 'В оффере не поддерживается данное GEO(GEO определяется по коду телефона)',
					6		=> 'Отказ. Заказ пришлось отклонить по той причине что не имеется доставки в регион получателя',
					7		=> 'Отказ. Клиент отказывается предоставлять данные для подтверждения заказа',
					8		=> 'Клиент отказался подтверждать заказ, утверждает что не делал заказ',
					9		=> 'Фиктивная заявка. Клиент утверждает что ранее приобрел такой же товар, а теперь ищет продавца чтобы сделать возврат товара и своих денежных средств',
					10	=> 'В оффере не поддерживается данное GEO(GEO определяется по коду телефона)',
					11	=> 'Фиктивнaя заявка. Клиент оставил заявку что бы проконсультироватся по своей ранее оформленной заявке',
					12	=> 'В оффере не поддерживается данное GEO(GEO определяется по коду телефона)',
				],
			],
			'ransom' => [
				'reject'	=> [
					3	=> '',
				],
				'expect'	=> [
					0	=> '',
					1	=> '',
					2	=> '',
				],
				'confirm'	=> [
					6		=> 'Посылка выкуплена',
					7		=> 'Посылка частично выкуплена',
				],
			],
			'brakeLogFolder'			=> true,
			'bridgeRequestTimeout'		=> 60,
			'statusRequestTimeout'		=> 60,
			'urlOrderAdd'				=> 'http://rekl.pro/api_simple_order/',
			'urlOrderInfo'				=> 'http://rekl.pro/api_status/',
		],
	],
];

?>