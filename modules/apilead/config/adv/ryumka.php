<?php

/*
http://cc.salesup-crm.com/PostOrder.aspx эндпоинт

параметры:
partner_id - id партнера в нашей системе
product_id - id продукта в нашей системе
comment - комментарий к заказу
name - имя клиента
phone - телефон клиента
click_id - внешний id лида
outer_pid - внешний id вэбмастера
это все что вам нужно для отправки лидов нам.

при успешном добавлении, в ответе будет наш id добавленного заказа, в формате JSON
 */

return [
	
	4634 => [
		'apileadApiKey'				=> '7687a1980350973788ed002e82a98b19',
		'postbackApiKey'			=> '002e827687a97398b197819803508eda',
		'partnerId'					=> 13740,
		
		'offers' => [
			697 => [
				'productId'				=> 1356,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'http://cc.salesup-crm.com/PostOrder.aspx',
			'urlOrderInfo'		=> '',
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'		=> [
//				''		=> '',
			],
			'reject'	=> [
				'denied'		=> '',
			],
			'expect'	=> [
				'new'			=> '',
				'processing'	=> '',
			],
			'confirm'	=> [
				'confirmed'		=> '',
				'payed'			=> '',
				'not_redeemed'	=> '',
			],
		],
		'ransom' => [																								//  Для майбутнього функціоналу перевірки на викуп
			'confirm'	=> [
				'payed'			=> '',
			],
			'reject'	=> [
				'not_redeemed'	=> '',
			],
			'expect'	=> [
				'confirmed'		=> '',
				'new'			=> '',
			],
		],
		'brakeLogFolder'			=> true,
	],
];
?>