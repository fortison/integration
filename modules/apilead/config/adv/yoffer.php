<?php

/*
*	https://yoffer.me/help/api.php#ext
 */

return [
	
	12278 => [
		'apileadApiKey'				=> '17d30e0d4b132e3d5485e966ca001f6a',
//		'postbackApiKey' 			=> '1e5e51b5c6dff5ff6dc0aa1188f0ef5y',
		'api_key'					=> '8a063d920f9573ca10a0e3828b8fe79f',
		'api_id' 					=> '20',
		
		'offers' => [
			3909 => [
				'offer' 	=> '13',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3910 => [
				'offer' 	=> '13',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3911 => [
				'offer' 	=> '13',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4183 => [
				'offer' 	=> '13',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4184 => [
				'offer' 	=> '13',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4160 => [
				'offer' 	=> '12',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4161 => [
				'offer' 	=> '12',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4162 => [
				'offer' 	=> '12',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4009 => [
				'offer' 	=> '16',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4010 => [
				'offer' 	=> '16',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4012 => [
				'offer' 	=> '16',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4389 => [
				'offer' 	=> '14',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4390 => [
				'offer' 	=> '14',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4391 => [
				'offer' 	=> '14',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4392 => [
				'offer' 	=> '14',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4184 => [
				'offer' 	=> '13',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'		=> '',
			],
			'reject'	=> [
				'cancel'	=> '',
			],
			'expect'	=> [
				'wait'		=> '',
				'hold'		=> '',
			],
			'confirm'	=> [
				'approve'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://yoffer.me/api/ext/add.json',
		'urlOrderInfo'		=> 'https://yoffer.me/api/ext/list.json',
	],
];

?>