<?php

return [
	
	12723 => [
		'apileadApiKey'				=> '2b1846bf6556edab4b154c0e0257862a',
		'postbackApiKey'			=> '441ce6ef5f3aa7b4103511a9ff8e49fe0689acc6',
		'affiliate_id' 				=> '1280',
		
		'offers' => [
			4400 => [
				'goal_id'	=> '402',
				'domain' 	=> 'http://invest-sector.big-sale.biz',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4421 => [
				'goal_id'	=> '451',
				'domain' 	=> '',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4424 => [
				'goal_id'	=> '352',
				'domain' 	=> '',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4429 => [
				'goal_id'	=> '434',
				'domain' 	=> '',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'	=> '',
			],
			'reject'	=> [
				'reject'	=> '',
			],
			'expect'	=> [
				'expect'		=> '',
			],
			'confirm'	=> [
				'confirm'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://track.affcountry.com/s2s/new_lead/create_by_goal',
		'urlOrderInfo'		=> '',
	],
];

?>