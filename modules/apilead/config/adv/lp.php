<?php

/**
 * http://blog.lp-crm.biz/instruktsii/integratsiya-lendinga-s-lp-crm-2/
 *
 *
 * Login :APIlEAD
 * Pass: 12345678
 * http://oae.lp-crm.biz/
 */

return [
	//Naturkh.lp-crm.biz
	//Login arbitr
	//Pass 12345
	11517 => [
		'apileadApiKey'				=> '27371a25300347e04dd5f300a4361117',
		'key' 						=> '13a5dffec021824c9b0de89361150538',
		//'office'					=> 3,
		
		'offers' => [
			4063 => [
				'product_id' => 1,
				'product_price' => 0,
				'product_landing' => 'http://ketodiet.fast-slim.biz	/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash'	=> [
					'53'	=> 'Треш',
					'55'	=> 'фрод',
				],
				'reject'	=> [
					'3'	    => 'Новый',
					'13'	=> 'Отказ',
				],
				'expect'	=> [
					'3'		=> 'new',
					'58'	=> 'Перезвон',
					'57'	=> 'не ьерет труьку',
				],
				'confirm'	=> [
					'11'	=> 'Принято',
					'14'	=> 'Отправлено',
					'18'	=> 'Завершено',
					'31'	=> 'Возврат товара (склад)',
					'20'	=> 'Возврат товара (в пути)',
					'27'	=> 'Самовывоз',
					'32'	=> 'Обмен',
					'50'	=> 'Утилизация',
					'53'	=> 'Деньги в пути',
				],
			],
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'http://naturkh.lp-crm.biz/api/addNewOrder.html \'',
			'urlOrderInfo'		=> 'http://naturkh.lp-crm.biz/api/getOrdersByID.html',
		],
	],
	
	10826 => [
		'apileadApiKey'				=> '6e046c7c35b0dbed3788146aca28705e',
		'key' 						=> 'eeaab21d23ff142ecbc54b3ca3748e56',
		'office'					=> 3,
		
		'offers' => [
			3855 => [
				'product_id' => 5,
				'product_price' => 0,
				'product_landing' => 'http://erostan.healthy-market.net/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3908 => [
				'product_id' => 10,
				'product_price' => 0,
				'product_landing' => 'http://aroforte.all-tech.biz/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3912 => [
				'product_id' => 9,
				'product_price' => 0,
				'product_landing' => 'http://gipertony.healthy-market.net/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4180 => [
				'product_id' => 24,
				'product_price' => 0,
				'product_landing' => 'biomanix.big-sale.biz',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4181 => [
				'product_id' => 27,
				'product_price' => 0,
				'product_landing' => 'libido-drive.mans-power.net',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4182 => [
				'product_id' => 30,
				'product_price' => 0,
				'product_landing' => 'pantomax.all-tech.biz',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3716 => [
				'product_id' => 13,
				'product_price' => 0,
				'product_landing' => 'aresex.mans-power.net',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash'	=> [
					'52'	=> 'Треш',
					'57'	=> 'Недозвоны утиль',
				],
				'reject'	=> [
					'13'	=> 'Отказ',
				],
				'expect'	=> [
					'3'	    => 'Новый',
					'51'	=> 'Перезвонить',
					'54'    => 'Отправить позже',
				],
				'confirm'	=> [
					'11'	=> 'Принято',
					'14'	=> 'Отправлено',
					'18'	=> 'Завершено',
				],
			],
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'http://group.lp-crm.biz/api/addNewOrder.html\'',
			'urlOrderInfo'		=> 'http://group.lp-crm.biz/api/getOrdersByID.html',
		],
	],
	12594 => [
		'apileadApiKey'				=> 'd970c0be3a7f025832fb33b744908152',
		'key' 						=> '77d1cf7cb6a48843cbd95a469eca9030',
		'office'					=> 5,
		
		'offers' => [
			4341 => [
				'product_id' 	=> 12,
				'product_price' => 0,
				'product_landing' => 'http://erotrin.big-sale.biz/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash'	=> [
					'56'	=> 'Треш',
					'57'	=> 'Недозвоны утиль',
					'31'	=> 'Возврат товара (склад)',
				],
				'reject'	=> [
					'13'	=> 'Отказ',
				],
				'expect'	=> [
					'3'	    => 'Новый',
					'51'	=> 'Перезвон',
					'52'    => 'Отправить позже',
				],
				'confirm'	=> [
					'55'	=> 'Хол.Выкуп',
					'54'	=> 'Хол.Подтвержд',
					'53'	=> 'Заход',
					'11'	=> 'Принято',
					'14'	=> 'Отправлено',
					'18'	=> 'Завершено',
				],
			],
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'http://one.lp-crm.biz/api/addNewOrder.html\'',
			'urlOrderInfo'		=> 'http://one.lp-crm.biz/api/getOrdersByID.html',
		],
	],
	
	12351 => [
		'apileadApiKey'				=> '62f6151429a360301d2d6b99285b4d6e',
		'key' 						=> 'a10b386d810ac37c59a6e0ebc794a5dc',
		'office'					=> 1,
		
		'offers' => [
			4063 => [
				'product_id' => 5,
				'product_price' => 65,
				'product_landing' => 'http://ketodiet.fast-slim.biz/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash'	=> [
					'52'	=> 'Треш',
					'50'	=> 'Фрод',
					'57'	=> 'Недозвоны утиль',
				],
				'reject'	=> [
					'13'	=> 'Отказ',
				],
				'expect'	=> [
					'3'	    => 'Новый',
					'52'	=> 'Перезвонить',
					'54'    => 'Отправить позже',
				],
				'confirm'	=> [
					'11'	=> 'Принято',
					'14'	=> 'Отправлено',
					'18'	=> 'Завершено',
				],
			],
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'http://simplenatur.lp-crm.biz/api/addNewOrder.html\'',
			'urlOrderInfo'		=> 'http://simplenatur.lp-crm.biz/api/getOrdersByID.html',
		],
	],
	
	
	12231 => [
		'apileadApiKey'				=> '33f9dd972e254b0ef2a6d9b4da65e571',
		'key' 						=> 'eeaab21d23ff142ecbc54b3ca3748e56',
		'office'					=> 7,
		
		'offers' => [
			3855 => [
				'product_id' => 5,
				'product_price' => 0,
				'product_landing' => 'http://erostan.healthy-market.net/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3908 => [
				'product_id' => 10,
				'product_price' => 0,
				'product_landing' => 'http://aroforte.all-tech.biz/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3912 => [
				'product_id' => 9,
				'product_price' => 0,
				'product_landing' => 'http://gipertony.healthy-market.net/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash'	=> [
					'52'	=> 'Треш',
					'57'	=> 'Недозвоны утиль',
				],
				'reject'	=> [
					'13'	=> 'Отказ',
				],
				'expect'	=> [
					'3'	    => 'Новый',
					'51'	=> 'Перезвонить',
					'54'    => 'Отправить позже',
				],
				'confirm'	=> [
					'11'	=> 'Принято',
					'14'	=> 'Отправлено',
					'18'	=> 'Завершено',
				],
			],
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'http://group.lp-crm.biz/api/addNewOrder.html\'',
			'urlOrderInfo'		=> 'http://group.lp-crm.biz/api/getOrdersByID.html',
		],
	],
	
	10822 => [
		'apileadApiKey'				=> 'dc397e42e2c072c97d205982d3216c20',
		'key' 						=> '8527ef4bec47df593512ef6dd041f3e7',
		'office'					=> 3,
		
		'offers' => [
			2786 => [
				'product_id' => 9,
				'product_id' => 8,
				'product_price' => 890,
				'product_landing' => 'http:// /',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash'	=> [
					'53'	=> 'Треш',
				],
				'reject'	=> [
					'3'	    => 'Новый',
					'54'	=> 'Дубль',
					'13'	=> 'Отказ',
				],
				'expect'	=> [
					'3'		=> 'new',
					'51'	=> 'Недозвон',
				],
				'confirm'	=> [
					'11'	=> 'Принято',
					'14'	=> 'Отправлено',
					'52'	=> 'В отделении',
					'18'	=> 'Завершено',
					'31'	=> 'Возврат товара (склад)',
				],
			],
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'http://omniheat.lp-crm.biz/api/addNewOrder.html\'',
			'urlOrderInfo'		=> 'http://omniheat.lp-crm.biz/api/getOrdersByID.html',
		],
	],
	
	8161 => [
		'apileadApiKey'				=> '6145b7ee044fc79b55058281dfb91e0a',
		'key' 						=> '9a487ad3512e3e0116771a6c0da7f718',
		
		'offers' => [
			3101 => [
				'product_id' => 6,
				'product_price' => 749,
				'product_landing' => 'http://arctic-air.all-tech.biz/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3102 => [
				'product_id' => 5,
				'product_price' => 449,
				'product_landing' => 'http://lamzak.big-sale.biz/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	9739 => [
		'apileadApiKey'				=> 'fbf18f4e5e612bf6aba682f1978be07a',
		'key' 						=> '79883cdc437fd93379bcfe2cef07275f',
		'office'					=> 9,
		
		'offers' => [
			3641 => [
				'product_id' => 362,
				'product_price' => 129,
				'product_landing' => 'http://mask-ae.beauty-shops.biz/',
				
				'additional_1' => 'offer name', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			
			3642 => [
				'product_id' => 364,
				'product_price' => 199,
				'product_landing' => 'http://patch-ae.beauty-shops.biz/',
				
				'additional_1' => 'patch 3642', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			
			3643 => [
				'product_id' => 367,
				'product_price' => 149,
				'product_landing' => 'http://magicray.big-sales.xyz/',
				
				'additional_1' => 'magic ray 2 pcs', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			
			3655 => [
				'product_id' => 381,
				'product_price' => 129,
				'product_landing' => 'http://vclean-ae.big-sales.xyz/',
				
				'additional_1' => 'vclean-spot-ap', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			
			3656 => [
				'product_id' => 380,
				'product_price' => 149,
				'product_landing' => 'http://tape-ae.big-sales.xyz/',
				
				'additional_1' => 'skotch-ap', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			
			3657 => [
				'product_id' => 382,
				'product_price' => 149,
				'product_landing' => 'http://beard-ae.big-sales.xyz/',
				
				'additional_1' => 'beard-ap', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			
			3662 => [
				'product_id' => 399,
				'product_price' => 136,
				'product_landing' => 'mrelax-ae.big-sales.xyz',
				
				'additional_1' => 'massager-ap', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			
			3673 => [
				'product_id' => 415,
				'product_price' => 199,
				'product_landing' => 'icecream-en.big-sales.xyz/',
				
				'additional_1' => 'ice-cream-maker-ap', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			
			3691 => [
				'product_id' => 423,
				'product_price' => 179,
				'product_landing' => 'samo-ae-m.big-sales.xyz',
				
				'additional_1' => 'orthopedic-slippers-ap', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			
			3748 => [
				'product_id' => 484,
				'product_price' => 299,
				'product_landing' => 'airpro3.all-tech.biz',
				
				'additional_1' => 'air-pro3-ap', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			
			3780 => [
				'product_id' => 520,
				'product_price' => 169,
				'product_landing' => 'sterilizer-m-en.all-tech.biz',
				
				'additional_1' => 'sterilizer-ap', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			
			3779 => [
				'product_id' => 521,
				'product_price' => 139,
				'product_landing' => 'vegcutter-m-en.all-tech.biz',
				
				'additional_1' => 'vegcutter-ap', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			
			3781 => [
				'product_id' => 523,
				'product_price' => 169,
				'product_landing' => 'carbackseat.big-sales.xyz',
				
				'additional_1' => 'carbackseat-ap', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			
			3425 => [
				'product_id' => 524,
				'product_price' => 139,
				'product_landing' => 'carholder.all-tech.biz',
				
				'additional_1' => 'carholder-ap', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			
			3783 => [
				'product_id' => 525,
				'product_price' => 129,
				'product_landing' => 'riddex-m-en.all-tech.biz',
				
				'additional_1' => 'riddex-ap', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			
			3784 => [
				'product_id' => 527,
				'product_price' => 129,
				'product_landing' => 'drying-m-en.big-sales.xyz',
				
				'additional_1' => 'drying-ap', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			
			3782 => [
				'product_id' => 526,
				'product_price' => 149,
				'product_landing' => 'studioglow-m-en.all-tech.biz',
				
				'additional_1' => 'studioglow-ap', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			
			3785 => [
				'product_id' => 528,
				'product_price' => 129,
				'product_landing' => 'lazyholder-m-en.big-sales.xyz',
				
				'additional_1' => 'lazyholder-ap', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		],
		'configs' => [
			'statuses' => [
				'trash'	=> [
					'63'	=> 'Not Valid не валид',
				],
				'reject'	=> [
					'58'	=> 'Rejected Berd( отмена)',
					'13'	=> 'Отказ',
				],
				'expect'	=> [
					'3'		=> 'new',
					'11'	=> 'Принято',
					'64'	=> 'Pending',
					'77'	=> 'Pending',
					'73'	=> 'Pending',
					'66'	=> 'Pending',
					'67'	=> 'Pending',
					'68'	=> 'Pending',
					'69'	=> 'Pending',
					'75'	=> 'Pending',
					'76'	=> 'Pending',
				],
				'confirm'	=> [
					'57'	=> 'Not paid(подтвержен не выкуп)',
					'60'	=> 'Waiting for delivery( подтвержен)',
					'61'	=> 'Delivery in progress ( подтвержден,в процессе доставки- апрув)',
					'65'	=> 'SСHEDULED ORDERS подтвержен',
					'31'	=> 'Возврат товара (склад)',
					'70'	=> 'Waiting For Delivery Oman',
					'71'	=> 'Delivery in progress OMAN',
					'62'	=> 'Sucsess Delivery ',
					'72'	=> 'Suscess OMAN',
					'74'	=> 'Late delivery Call',
					'50'	=> 'Утилизация',
					'53'	=> 'Выплата CRT',
					'55'	=> 'Выплата Berd',
					'20'	=> 'Возврат товара (в пути)',
				],
			],
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'http://oae.lp-crm.biz/api/addNewOrder.html\'',
			'urlOrderInfo'		=> 'http://oae.lp-crm.biz/api/getOrdersByID.html',
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'51'	=> 'trashed',
			],
			'reject'	=> [
				'13'	=> 'cancelled',
			],
			'expect'	=> [
				'3'		=> 'new',
				'67'		=> 'new',
				'68'		=> 'new',
				'69'		=> 'new',
				'75'		=> 'new',
				'76'		=> 'new',
			],
			'confirm'	=> [
				'11'	=> 'approved',
				'14'	=> 'approved',
				'18'	=> 'approved',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'http://tovvarkacentre.lp-crm.biz/api/addNewOrder.html',
		'urlOrderInfo'		=> 'http://tovvarkacentre.lp-crm.biz/api/getOrdersByID.html',
	],
];

?>