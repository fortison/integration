<?php

/*
 * В файлі
 */

return [
    5399 => [
		'apileadApiKey'				=> 'bdefb61de1c98d03dd3bc4d7fea38788',
		'apiKey' 					=> '784089B4-FBBC-47E0-AD18-686B3B0C8B90',
		'postbackApiKey'			=> '8199a3c328078eda005302e827687a57',

		'offers' => [
			2645 => [
				'offerId'       	=> 2645,
				'campaignId'    	=> 2645,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2646 => [
				'offerId'       	=> 2646,
				'campaignId'    	=> 2646,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2650 => [
				'offerId'       	=> 2650,
				'campaignId'    	=> 2650,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2651 => [
				'offerId'       	=> 2651,
				'campaignId'    	=> 2651,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2652 => [
				'offerId'       	=> 2652,
				'campaignId'    	=> 2652,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2653 => [
				'offerId'       	=> 2653,
				'campaignId'    	=> 2653,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2655 => [
				'offerId'       	=> 2655,
				'campaignId'    	=> 2655,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2656 => [
				'offerId'       	=> 2656,
				'campaignId'    	=> 2656,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2588 => [
				'offerId'       	=> 2588,
				'campaignId'    	=> 2588,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2618 => [
				'offerId'       	=> 2618,
				'campaignId'    	=> 2618,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2699 => [
				'offerId'       	=> 2699,
				'campaignId'    	=> 2699,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2728 => [
				'offerId'       	=> 2728,
				'campaignId'    	=> 2728,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2737 => [
				'offerId'       	=> 2737,
				'campaignId'    	=> 2737,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2756 => [
				'offerId'       	=> 2756,
				'campaignId'    	=> 2756,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2761 => [
				'offerId'       	=> 2761,
				'campaignId'    	=> 2761,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2463 => [
				'offerId'       	=> 2463,
				'campaignId'    	=> 2463,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2889 => [
				'offerId'       	=> 2889,
				'campaignId'    	=> 2889,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2888 => [
				'offerId'       	=> 2888,
				'campaignId'    	=> 2888,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2887 => [
				'offerId'       	=> 2887,
				'campaignId'    	=> 2887,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2886 => [
				'offerId'       	=> 2886,
				'campaignId'    	=> 2886,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2900 => [
				'offerId'       	=> 2900,
				'campaignId'    	=> 2900,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2053 => [
				'offerId'       	=> 2053,
				'campaignId'    	=> 2053,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],

	'configs' => [
        'statuses' => [
            'trash'	=> [
                'Trash'	          	=> 'Спам/Некорректная заявка',
            ],
            'reject'	=> [
                'Reject'	        => 'Отказ',
            ],
            'expect'	=> [
                'Await'				=> 'В обработке',
            ],
            'confirm'	=> [
                'Confirm'	       	=> 'Подтвержден',
            ],
        ],

		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://ap.leomax.ru/cpa/orders',
//		'statusEnabled'		=> false,
	],
];