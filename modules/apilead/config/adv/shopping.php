<?php

/*
 * https://docs.google.com/document/d/1aq4VzdD5jg9rvfKxP72UjwRF0sJM17pkRXi8e3KdHUE/edit#
 */

return [
	
	12471 => [
		'apileadApiKey'				=> '609d1bbb917ca0ef4c3ebc47cfdd721c',
		'API_CODE'					=> 'Ogn5TOouxNpwgRVTlBhy3YjoI',
		
		'offers' => [
			4302 => [
				'order_source'	=> 'https://docs.google.com/',
				'item_id'	    =>  127,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4303 => [
				'order_source'	=> 'https://docs.google.com/',
				'item_id'	    =>  142,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4304 => [
				'order_source'	=> 'https://docs.google.com/',
				'item_id'	    =>  4103,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4305 => [
				'order_source'	=> 'https://docs.google.com/',
				'item_id'	    => 2465,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4306 => [
				'order_source'	=> 'https://docs.google.com/',
				'item_id'	    => 2932,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4307 => [
				'order_source'	=> 'https://docs.google.com/',
				'item_id'	    =>  2441,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4308 => [
				'order_source'	=> 'https://docs.google.com/',
				'item_id'	    => 139,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4309 => [
				'order_source'	=> 'https://docs.google.com/',
				'item_id'	    => 128,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4310 => [
				'order_source'	=> 'https://docs.google.com/',
				'item_id'	    => 3215,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4311 => [
				'order_source'	=> 'https://docs.google.com/',
				'item_id'	    => 73,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4312 => [
				'order_source'	=> 'https://docs.google.com/',
				'item_id'	    => 245,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4313 => [
				'order_source'	=> 'https://docs.google.com/',
				'item_id'	    => 4728,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2394 => [
				'order_source'	=> 'https://docs.google.com/',
				'item_id'	    => 2243,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4373 => [
				'order_source'	=> 'https://docs.google.com/',
				'item_id'	    => 80,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4452 => [
				'order_source'	=> 'https://docs.google.com/',
				'item_id'	    => 138,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2728 => [
				'order_source'	=> 'https://docs.google.com/',
				'item_id'	    => 2385,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'brakeLogFolder'	=> true,
//			'urlOrderAdd'		=> 'http://85.17.28.78/ci/gate/orderxml/{API_CODE}/orderjson_worker',
			'urlOrderAdd'		=> 'http://46.4.78.124/ci/gate/orderxml/{API_CODE}/orderjson_worker',
//			'urlOrderInfo'		=> 'http://85.17.28.78/ci/gate/getorderstatus/{API_CODE}/json',
			'urlOrderInfo'		=> 'http://46.4.78.124/ci/gate/getorderstatus/{API_CODE}/json',
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'4'	=> '',
			],
			'reject'	=> [
				'3'	=> '',
			],
			'expect'	=> [
				'2'		=> '',
			],
			'confirm'	=> [
				'1'	=> '',
			],
		],
		'brakeLogFolder'			=> true,
	],
];
?>