<?php

/*
 * https://toproi.biz/help/api.php#ext
 */

return [
	
	7114 => [
		'apileadApiKey'				=> '31820782177fe1f43e118223bb776514',
		'apiKey'					=> 'ba05daa114f822886a5b70946cb495ac',
		'user' 						=> 8,
		
		'offers' => [
			3280 => [
				'offerId' => 115,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3279 => [
				'offerId' => 115,
				
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2643 => [
				'offerId' => 13,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2926 => [
				'offerId' => 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			739 => [
				'offerId' => 14,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2843 => [
				'offerId' => 34,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3012 => [
				'offerId' => 35,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3024 => [
				'offerId' => 27,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3033 => [
				'offerId' => 23,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2625 => [
				'offerId' => 7,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3146 => [
				'offerId' => 49,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3105 => [
				'offerId' => 12,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3104 => [
				'offerId' => 30,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3614 => [
				'offerId' => 202,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3615 => [
				'offerId' => 18,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1548 => [
				'offerId' => 335,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3884 => [
				'offerId' => 288,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3885 => [
				'offerId' => 279,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2366 => [
				'offerId' => 259,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3376 => [
				'offerId' => 264,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3890 => [
				'offerId' => 264,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3891 => [
				'offerId' => 311,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3892 => [
				'offerId' => 305,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3893 => [
				'offerId' => 304,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3907 => [
				'offerId' => 311,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4592 => [
				'offerId' => 357,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4593 => [
				'offerId' => 357,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		'configs' => [
			'statuses' => [
				'reject'	=> [
					'cancel'	=> '',
				],
				'expect'	=> [
					'new'		=> '',
					'wait'		=> '',
				],
				'trash'	=> [
					'trash'		=> '',
				],
				'confirm'	=> [
					'approve'	=> '',
				],
			],
			'subStatuses' => [
				'expect'	=> [
				],
				'confirm'	=> [
				],
				'reject'	=> [
					2		=> 'Changed his mind',
					4		=> 'Requires certificate',
					9		=> 'Expensive',
					10		=> 'Not satisfied with delivery',
				],
				'trash'	=> [
					1		=> 'Incorrect phone',
					3		=> 'Did not order',
					5		=> 'Wrong GEO',
					6		=> 'Errors of fake',
					7		=> 'Duplicate order',
					8		=> 'Ordered elsewhere',
					11		=> 'Could not get through',
					12		=> 'Possibly fraud',
				],
			],

			'bridgeRequestTimeout'		=> 60,
			'statusRequestTimeout'		=> 60,
			'urlOrderAdd'				=> 'https://toproi.biz/api/ext/add.json',
			'urlOrderInfo'				=> 'https://toproi.biz/api/ext/list.json',
		],
	],
];

?>