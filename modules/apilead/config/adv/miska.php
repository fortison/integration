<?php

/**
 * Надсилає статус відразу до апіліда
 */

return [
	
	5935 => [
		'apileadApiKey'				=> '37e38152259c4074a49ddd1f06961859',
		'postbackApiKey'			=> '002e827687a975a3c325781980308eda',
		
		'offers' => [
			2705 => [
				'productList'				=> [
					'Заказать мишку “Серый с сердцем 40 см“' => [
						'modelId' => 9,
						'goodsPrice' => 3180,
					],
					'Заказать мишку “Белый с лентой 40 см”' => [
						'modelId' => 20,
						'goodsPrice' => 3180,
					],
					'Заказать мишку “Красный с лентой 40 см“' => [
						'modelId' => 19,
						'goodsPrice' => 3180,
					],
					'Заказать мишку “Серый с лентой 40 см“' => [
						'modelId' => 18,
						'goodsPrice' => 3180,
					],
					'Заказать мишку “Розовый с лентой 40 см“' => [
						'modelId' => 17,
						'goodsPrice' => 3180,
					],
					'Заказать мишку “Сиреневый с лентой 40 см“' => [
						'modelId' => 20,
						'goodsPrice' => 3180,
					],
					'Заказать мишку “Голубой с лентой 40 см“' => [
						'modelId' => 20,
						'goodsPrice' => 3180,
					],
					'Заказать мишку “Белый с сердцем 40 см“' => [
						'modelId' => 9,
						'goodsPrice' => 3490,
					],
					'Заказать мишку “Коричневый с сердцем 40 см“' => [
						'modelId' => 33,
						'goodsPrice' => 3490,
					],
					'Заказать мишку “Красный с сердцем 40 см“' => [
						'modelId' => 11,
						'goodsPrice' => 3490,
					],
					'Заказать мишку “Розовый с сердцем 40 см“' => [
						'modelId' => 13,
						'goodsPrice' => 3490,
					],
					'Заказать мишку “Подарочная упаковка 40 см“' => [
						'modelId' => 9,
						'goodsPrice' => 650,
					],
					'Заказать мишку “Белый с лентой 40 см“' => [
						'modelId' => 20,
						'goodsPrice' => 3180,
					],
				],
				'goods_type_model_id'		=> function ($model, $config) {
					$configOffer	= (object) $config->offer->array;
					$productName	= $model->address;
					return $configOffer->productList[$productName]['modelId'] ?? '20';
				},
				'goods_price'		=> function ($model, $config) {
					$configOffer	= (object) $config->offer->array;
					$productName	= $model->address;
					return $configOffer->productList[$productName]['goodsPrice'] ?? '3180';
				},
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'		=> [
				'trash'		=> '',
			],
			'reject'	=> [
				'reject'	=> '',
			],
			'expect'	=> [
				'expect'	=> '',
			],
			'confirm'	=> [
				'confirm'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'http://gq2.ru/new_lid.php',
		'urlOrderInfo'		=> '',
	],
];

?>