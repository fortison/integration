<?php

/**
 * https://servicelead.top/api
 *
 * http://connectcpa.net/apilead/adv/adv-postback-status?systemUserId=11318&partnerStatus={status}&systemOrderId={sub_id1}&postbackApiKey=0322f3a67db1a047bf632286d7197be55dc3
 */

return [
	//LOGIN: 10760
	//PASSWORD: 778R37Bj
	11318 => [
		'apileadApiKey'  => 'f3a67db1a047bf632286d7197be55dc4',
		'postbackApiKey' => 'f3a67db1a047bf632286d7197be55dc3',
		'idp' 			 => '49557258-ca10-0a02-5525686c0d727e9c',
		
		'offers' => [
			3971 => [
				'offer_id' => 22, //https://zapasnoylead.ru/get_offers
				'branch_id' => 0, //ИД филиала | https://servicelead.top/city
				'url' => 'example.com', //необязательное поле
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'trash' => 'фейк',
			],
			'reject' => [
				'rejected' => 'отклонен',
			],
			'expect' => [
				'pending' => 'в обработке',
			],
			'confirm' => [
				'approved' => 'принят',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://api.servicelead.top/lead',
		'urlOrderInfo'		=> '',
	],
];

?>