<?php

return [
	
	11436 => [
		'apileadApiKey'				=> '6c5e39675a3e91d03a0d8c186f8fedf2',
		'api_key' 					=> '2f0f45e478253637f44cdea7029422fd',
		
		'offers' => [
			4321 => [ // Red MaxMan Gel - AE
				'product' => 166746, //ID продукта
				'utm_term' => 'jzdc88cv', //поток КЦ
				'payout' => 15, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				9		=> 'Trash / Duplicate order',
				13		=> 'Test',
			],
			'reject'	=> [
				2	=> 'Cancelled',
			],
			'expect'	=> [
				0	=> 'Обработка',
			],
			'confirm'	=> [
				1	=> 'Confirmed',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://bestpage-sales.xyz/partners/leadvertex.php',
		'urlOrderInfo'		=> 'https://bestpage-sales.xyz/partners/status.php',
	],
];

?>