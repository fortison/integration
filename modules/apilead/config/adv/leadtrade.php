<?php

return [
	
	357 => [
		'apileadApiKey'				=> 'a7a261a82041d1485709d582a25a2962',
		'apiKey'					=> '37b6c75e677bfa73462aec95c7cf3249c611b8ea',
		'partner' 					=> 94,
		
		'offers' => [
			266 => [
				'offerId' => 97,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1727 => [
				'offerId' => 97,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			801 => [
				'offerId' => 97,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1838 => [
				'offerId' => 456,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1839 => [
				'offerId' => 458,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1840 => [
				'offerId' => 458,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			556 => [
				'offerId' => 88,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1860 => [
				'offerId' => 88,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			894 => [
				'offerId' => 88,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1861 => [
				'offerId' => 88,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1866 => [
				'offerId' => 470,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1464 => [
				'offerId' => 360,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1976 => [
				'offerId' => 479,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1977 => [
				'offerId' => 479,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1980 => [
				'offerId' => 479,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1030 => [
				'offerId' => 328,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1998 => [
				'offerId' => 479,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1999 => [
				'offerId' => 479,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1112 => [
				'offerId' => 364,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2071 => [
				'offerId' => 515,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2099 => [
				'offerId' => 496,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2101 => [
				'offerId' => 496,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2102 => [
				'offerId' => 496,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2114 => [
				'offerId' => 496,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2187 => [
				'offerId' => 555,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2103 => [
				'offerId' => 570,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2197 => [
				'offerId' => 567,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2220 => [
				'offerId' => 571,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2255 => [
				'offerId' => 597,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2241 => [
				'offerId' => 583,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2242 => [
				'offerId' => 583,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2265 => [
				'offerId' => 588,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2297 => [
				'offerId' => 598,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2298 => [
				'offerId' => 598,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2311 => [
				'offerId' => 618,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2489 => [
				'offerId' => 729,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3233 => [
				'offerId' => 1418,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		'configs' => [
			'statuses' => [
				'reject'	=> [
					3	=> '',
				],
				'expect'	=> [
					2		=> '',
				],
				'trash'	=> [
					4	=> '',
				],
				'confirm'	=> [
					1	=> '',
				],
			],

			'bridgeRequestTimeout'		=> 60,
			'statusRequestTimeout'		=> 60,
			'urlOrderAdd'				=> 'http://moneymakerz.ru/_shared/submit_form/',
			'urlOrderInfo'				=> 'http://moneymakerz.ru/xmlparse/xmlwriter/',
		],
	],
];

?>