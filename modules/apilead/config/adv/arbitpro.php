<?php

/**
 * https://arbitpro.ru/settings/api
 *
 * http://connectcpa.net/apilead/adv/adv-postback-status?systemUserId=11320&partnerStatus={status}&systemOrderId={sub1}&postbackApiKey=0e7b51b5c6dff5ff6dc0aa1188f0ef5z
 */

return [
	//apilead2021
	//advert@apilead.com
	11320 => [
		'apileadApiKey'  => '0e7b51b5c6dff5ff6dc0aa1188f0ef5e',
//		'postbackApiKey' => '0e7b51b5c6dff5ff6dc0aa1188f0ef5z',
		'api_key' 		 => '7i9Tal2TTfnRCBF8ouJDxf2lVsvP3Tdv',
		
		'offers' => [
			4298 => [
				'hash' => '15ceeb', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4301 => [
				'hash' => '221e1e', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3972 => [
				'hash' => '9604de0c', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3897 => [
				'hash' => '3bca8dac', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3979 => [
				'hash' => 'd64db311', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3846 => [
				'hash' => '577e46b', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3896 => [
				'hash' => '9757cd89', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3898 => [
				'hash' => 'c8b15f', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4179 => [
				'hash' => '5fa32', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4190 => [
				'hash' => '39b2e', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4213 => [
				'hash' => 'a78c7', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4242 => [
				'hash' => 'dc9e24', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4261 => [
				'hash' => '82da13', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4266 => [
				'hash' => 'f8c47868', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4274 => [
				'hash' => '3a0323e', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4297 => [
				'hash' => 'e8823', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3973 => [
				'hash' => 'a358e0b', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4505 => [
				'hash' => '8832514', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4506 => [
				'hash' => '8f011302', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4507 => [
				'hash' => 'fd56a', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4508 => [
				'hash' => 'f803b', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4523 => [
				'hash' => '28774ce7', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4524 => [
				'hash' => 'b4798ab', //уникальный API-ключ потока
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'13' => 'фейк',
			],
			'reject' => [
				'12' => 'отклонен',
			],
			'expect' => [
				'10' => 'в обработке',
			],
			'confirm' => [
				'11' => 'принят',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://api.arbitpro.ru/lead/add',
		'urlOrderInfo'		=> 'https://api.arbitpro.ru/lead/info',
	],
];

?>