<?php

return [
	12503 => [
		'apileadApiKey'  	=> '5f539fc657815841d21041402d64cb25',
		'api_key' 			=> 'ac00b8204a264f92a1cce82821210566',
		'postbackApiKey'	=> 'a49ddd1f0696185937e38151339c407',
		
		'offers' => [
			4322 => [
				'offer_id' => 'slimplus-apilead-368HKD',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4323 => [
				'offer_id' => 'slimplus-apilead-1398TWD',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4324 => [
				'offer_id' => 'slimplus-apilead-69SGD',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4325 => [
				'offer_id' => 'maxman-APIlead-1398TWD',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4326 => [
				'offer_id' => 'maxman-APIlead-298HKD',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4327 => [
				'offer_id' => 'maxman-APIlead-49SGD',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'trash' => '',
			],
			'reject' => [
				'reject' => '',
			],
			'expect' => [
				'expect' => '',
			],
			'confirm' => [
				'confirm' => '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://lnkmobile.com/apis/customer/orders/',
	],
];

?>