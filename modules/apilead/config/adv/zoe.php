<?php

/**
 * https://docs.google.com/spreadsheets/d/1XzZRwcpLCYnF1V-RnO-7_roZxiCXcw8ov-enJcSqKyk/edit?usp=sharing
 * https://api.maczro.com/db_get_test.php
 * https://api.maczro.com/?country=cz&phone=111&name=ima&lastname=lastname&street=street&house=house&city=city&zip=zip&email=test@test&zoe=30001&price1=100
 *
 * артикул zoe:
    30001 - машинка S
	30002 - машинка M
	30003 - машинка XL
	5000 - часы
	25000 -кошелёк
 */

return [
	10975 => [
		'apileadApiKey'	=> '21421ae9a415d9fe026d6133d0e75126',
		
		'offers' => [
			3914 => [
				'zoe' => '30001', //машинка S
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'confirm'	=> [
				'confirm' => 'у нас тут cpl',
			],
		],
		
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://api.maczro.com/',
		'urlOrderInfo'		=> '',
	],
];

?>