<?php

/*
*	https://admin.neogara.com/docs/
*	advert@apilead.com
*	7CfHC!susm4h!qK
 */

return [
	
	2323 => [
		'apileadApiKey'				=> '00000000000000000000000000000000',
		'postbackApiKey'			=> '06c3065cc9d5cc96dfd0848c678de777',
		'pid' 						=> 'kg2gm3',
		
		'offers' => [
			2323 => [
				'offerId'	=> '15',
				'land_url' 	=> 'land-url.test',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'	=> '',
			],
			'reject'	=> [
				'reject'	=> '',
			],
			'expect'	=> [
				'expect'		=> '',
			],
			'confirm'	=> [
				'confirm'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://admin.neogara.com/api/lid',
		'urlOrderInfo'		=> '',
	],
];

?>