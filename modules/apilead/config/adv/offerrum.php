<?php

/**
 *	https://my.offerrum.com
 * 	login	adv.apilead@gmail.com
 * 	pass	777888
 */

return [
	
	3715 => [
		'apileadApiKey'		=> '6659192bffb86b575a3c325e29953e3e',
		'postbackApiKey'	=> '002e827687a975a3c325781980308eda',
		'key' 				=> '5a035ca73e7ab74dda4c6e79eb5631c1ad9ea098230742',
		'domain'			=> 'offerrum.com',
		
		'offers' => [
			3326 => [
				'flow' => 'x5qM',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3403 => [
				'flow' => 'xnMN',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3399 => [
				'flow' => 'xpPd',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3375 => [
				'flow' => 'xpP6',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3334 => [
				'flow' => 'xpPb',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3017 => [
				'flow' => 'xpPa',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3064 => [
				'flow' => 'xpPH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3340 => [
				'flow' => 'xpPK',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3163 => [
				'flow' => 'xpPQ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3397 => [
				'flow' => 'xqd7',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2429 => [
				'flow' => 'xAgB',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2004 => [
				'flow' => 'xEuc',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1627 => [
				'flow' => 'xLB7',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2366 => [
				'flow' => 'xLBb',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2818 => [
				'flow' => 'xLBg',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2195 => [
				'flow' => 'xUgh',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4022 => [
				'flow' => 'A3E2',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2966 => [
				'flow' => 'A87E',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2201 => [
				'flow' => 'Avsh',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3002 => [
				'flow' => 'AKge',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4278 => [
				'flow' => 'CxGh',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4489 => [
				'flow' => 'CxGh',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4490 => [
				'flow' => 'CxGh',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4491 => [
				'flow' => 'CxGh',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4492 => [
				'flow' => 'CxGh',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4488 => [
				'flow' => 'CxGh',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3562 => [
				'flow' => 'CxGx',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4493 => [
				'flow' => 'CxGL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4494 => [
				'flow' => 'CxGL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4495 => [
				'flow' => 'CxGL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4496 => [
				'flow' => 'CxGL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'		=> '',
			],
			'reject'	=> [
				'declined'	=> '',
			],
			'expect'	=> [
				'waiting'	=> '',
			],
			'confirm'	=> [
				'approved'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://api.{domain}/webmaster/order/',
		'urlOrderInfo'		=> '',
	],
];

?>