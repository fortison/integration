<?php

return [
	
	1777 => [
		'apileadApiKey'				=> '940296b8b0bf6a1f7c6f432418038caf',
		'apiKey' 					=> 'fb85sh38kap6w83gen2ote',
		
		'offers' => [
			51 => [
				'shopDn' => 'ksb-store.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1007 => [
				'shopDn' => 'Apilead-Varicobooster',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1008 => [
				'shopDn' => 'Apilead-Varicobooster',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1009 => [
				'shopDn' => 'Apilead-splendour',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1010 => [
				'shopDn' => 'Apilead-splendour',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			421 => [
				'shopDn' => 'smoking.monastyr-chai.net',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2082 => [
				'shopDn' => 'tms.all-shops.biz',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2110 => [
				'shopDn' => 'minoxidil.beauty-shops.biz',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1974 => [
				'shopDn' => 'a-firefit.fast-slim.biz/?alstream=2vf',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2431 => [
				'shopDn' => 'minusize.fast-slim.biz',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2491 => [
				'shopDn' => 'turbofit.fast-slim.biz/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2535 => [
				'shopDn' => 'neoslim.fast-slim.biz',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2925 => [
				'shopDn' => 're-cardio.healthy-market.net',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4063 => [
				'shopDn' => 'ketodiet.fast-slim.biz',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4106 => [
				'shopDn' => 'lipocarnit2.big-sale.biz',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4370 => [
				'shopDn' => 'orsofit.big-sale.biz',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4487 => [
				'shopDn' => 'smoke-out.big-sale.biz',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4498 => [
				'shopDn' => 'liftensyn-ru.big-sale.biz', //Liftensyn
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4497 => [
				'shopDn' => 'normalize-ua.big-sale.biz', //Normalize
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4538 => [
				'shopDn' => 'urekto.big-sale.biz', //Уретроактив
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		'configs' => [
			'statuses' => [
				'reject'	=> [
					3	=> '',
				],
				'expect'	=> [
					1	=> '',
				],
				'confirm'	=> [
					2	=> '',
				],
				'trash'	=> [
					4		=> '',
				],
			],

			'brakeLogFolder'			=> true,
			'bridgeRequestTimeout'		=> 60,
			'statusRequestTimeout'		=> 60,
			'urlOrderAdd'				=> 'http://bm.bineks.ru/api/order/create',
			'urlOrderInfo'				=> 'http://bm.bineks.ru/api/apilead/getOrders',
		],
	],
];

?>