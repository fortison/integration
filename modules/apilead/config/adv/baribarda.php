<?php

return [
	
	3186 => [
		'apileadApiKey'				=> '0f139bcf5c9c6a917b8f58ed181fdb8b',
		'apiKey' 					=> '1de3542e8659837',
		'loginId' 					=> '11300467',
		
		'offers' => [
			1548 => [
				'offerId' => 'elka',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			515 => [
				'offerId' => 'instantly_ageless',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1636 => [
				'offerId' => 'scholl',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			409 => [
				'offerId' => 'hot_shapers_br',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			360 => [
				'offerId' => 'g-shock',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1883 => [
				'offerId' => 'iselfie',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			504 => [
				'offerId' => 'pandora',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1983 => [
				'offerId' => 'fly_bra',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1982 => [
				'offerId' => 'fly_bra',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2052 => [
				'offerId' => 'jbl_kolonka',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2058 => [
				'offerId' => 'butterfly',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2264 => [
				'offerId' => 'magnet_lash',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2174 => [
				'offerId' => 'bobby_bag',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2294 => [
				'offerId' => 'monkey_fingerlings',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2364 => [
				'offerId' => 'airpods',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2370 => [
				'offerId' => '1uah_gelmifag',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2371 => [
				'offerId' => '1uah_walnut_oil',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			881 => [
				'offerId' => 'portmone_baellery',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2397 => [
				'offerId' => 'draw_light',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1028 => [
				'offerId' => 'fito_depilation',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2507 => [
				'offerId' => '1uah_sustawin',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2522 => [
				'offerId' => 'fishergoman',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2546 => [
				'offerId' => 'perfect_smile',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2553 => [
				'offerId' => '48uah_propatent',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2594 => [
				'offerId' => 't2_tuner',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2677 => [
				'offerId' => '1uah_gelmiforte',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2754 => [
				'offerId' => 'termo_under_armour',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2776 => [
				'offerId' => 'low_draw_light',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2823 => [
				'offerId' => '1uah_giperoforte',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2824 => [
				'offerId' => '1uah_dialist',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2902 => [
				'offerId' => 'gold_beauty_bag',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2954 => [
				'offerId' => '1uah_venomax',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3097 => [
				'offerId' => '1uah_valgusmend',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3098 => [
				'offerId' => '48uah_lipoX9',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3141 => [
				'offerId' => 'black_board',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3151 => [
				'offerId' => '1uah_normaderm',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3359 => [
				'offerId' => 'termo_smartov',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'https://control.premiumcall.com.ua/api/send_order.php',
			'urlOrderInfo'		=> 'https://control.premiumcall.com.ua/api/get_orders.php',
		],
	],
	
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				5	=> '',
			],
			'reject'	=> [
				2	=> '',
				6	=> '',
				7	=> '',
				8	=> '',
				9	=> '',
			],
			'expect'	=> [
				0	=> '',
				3	=> '',
				4	=> '',
				10	=> '',
			],
			'confirm'	=> [
				1	=> '',
			],
		],
		'brakeLogFolder'			=> true,
	],
];

?>