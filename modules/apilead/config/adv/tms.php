<?php

return [
	11451 => [
		'apileadApiKey'		=> '1200e90985aa706e5171d62fb76317f6',
		'postbackApiKey'	=> '1200e90985aa706e5171d62fb76317f1',
		'apikey' 			=> 'CsIo9eEXJbXH7BGyw326KnojNWt1iwQk', //Id of agency
		
		'offers' => [
			4041 => [
				'productName' => 'amulet-th', // Type of lead depend on ProductName, compare name with lowercase
				'orderId'  => '3765',
				
				'configs' => [
					'urlOrderAdd'		=> 'https://connect-th.tmsapp.net/connect/new/ald',
					'brakeLogFolder'	=> true,
				],
			],
			4043 => [
				'productName' => 'duracore-th', // Type of lead depend on ProductName, compare name with lowercase
				'orderId'  => '3684',
				
				'configs' => [
					'urlOrderAdd'		=> 'https://connect-th.tmsapp.net/connect/new/ald',
					'brakeLogFolder'	=> true,
				],
			],
			4048 => [
				'productName' => 'viakore-th', // Type of lead depend on ProductName, compare name with lowercase
				'orderId'  => '3685',
				
				'configs' => [
					'urlOrderAdd'		=> 'https://connect-th.tmsapp.net/connect/new/ald',
					'brakeLogFolder'	=> true,
				],
			],
			4044 => [
				'productName' => 'everliftcream-th', // Type of lead depend on ProductName, compare name with lowercase
				'orderId'  => '3686',
				
				'configs' => [
					'urlOrderAdd'		=> 'https://connect-th.tmsapp.net/connect/new/ald',
					'brakeLogFolder'	=> true,
				],
			],
			4045 => [
				'productName' => 'havita-th', // Type of lead depend on ProductName, compare name with lowercase
				'orderId'  => '3687',
				
				'configs' => [
					'urlOrderAdd'		=> 'https://connect-th.tmsapp.net/connect/new/ald',
					'brakeLogFolder'	=> true,
				],
			],
			4046 => [
				'productName' => 'lavite-th', // Type of lead depend on ProductName, compare name with lowercase
				'orderId'  => '3688',
				
				'configs' => [
					'urlOrderAdd'		=> 'https://connect-th.tmsapp.net/connect/new/ald',
					'brakeLogFolder'	=> true,
				],
			],
			4047 => [
				'productName' => 'greenhearb-th', // Type of lead depend on ProductName, compare name with lowercase
				'orderId'  => '3689',
				
				'configs' => [
					'urlOrderAdd'		=> 'https://connect-th.tmsapp.net/connect/new/ald',
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'		=> '',
			],
			'reject'	=> [
				'reject'	=> '',
			],
			'expect'	=> [
				'hold'	=> '',
			],
			'confirm'	=> [
				'approve'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> '',
	],
];

?>