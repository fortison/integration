<?php

return [
	
	1059 => [
		'apileadApiKey'				=> 'db57dbcb8300e92aafac9b39119d3517',
		'hash'						=> 'LDSvMD5EkaBluu2gEgVX52',
		'utm' 						=> 'apilead',
		
		'offers' => [
			4124 => [
				'product_id' => '6254',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4125 => [
				'product_id' => '6692',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4128 => [
				'product_id' => '6407',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4129 => [
				'product_id' => '6749',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4397 => [
				'product_id' => '6752',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4396 => [
				'product_id' => '6603',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4395 => [
				'product_id' => '6751',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4394 => [
				'product_id' => '6750',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4398 => [
				'product_id' => '6461',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4399 => [
				'product_id' => '6581',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3761 => [
				'product_id' => '6246',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4438 => [
				'product_id' => '6406',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4439 => [
				'product_id' => '6703',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4440 => [
				'product_id' => '6291',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4441 => [
				'product_id' => '6263',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4442 => [
				'product_id' => '6753',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4443 => [
				'product_id' => '6470',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4444 => [
				'product_id' => '6661',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4445 => [
				'product_id' => '6408',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4446 => [
				'product_id' => '6743',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4447 => [
				'product_id' => '6744',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4448 => [
				'product_id' => '6405',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'5'		=> 'Возврат',
				'18'	=> 'Треш',
			
			],
			'reject'	=> [
				'6'		=> 'Отменён',
				'17'	=> 'Отказ',
				'7'		=> 'Удалён',
			],
			'expect'	=> [
				'0'		=> 'Новый',
				'1'		=> 'Обработан',
				'9'		=> 'Недозвон',
				'10'	=> 'Перезвон',
				'29'	=> 'Перезвон',
				'13'	=> 'Контрольный прозвон',
			],
			'confirm'	=> [
				'2'		=> 'Отправлен',
				'3'		=> 'Доставлен',
				'4'		=> 'Вручен',
				'8'		=> 'Оплачен',
				'14'	=> 'Ожидает отправки',
			
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'http://sell1.best/apipublic/newOrder',
		'urlOrderInfo'		=> 'http://sell1.best/apipublic/getstatus',
	],
];

?>