<?php

/**
 * док в чате
 *
 * n/d
 *
 * логін - n/d
 * пароль - n/d
 */


return [
	11612 => [
		'apileadApiKey' => '9c329df6085db3ca076e5f01f01ce20c',
		'pid' 			=> '1281a1246f131a6c6f199bbbad31fa65',
		
		'offers' => [
			4090 => [
				'product_id' => '798',
				'product' 	 => 'jeeptrand-md',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4091 => [
				'product_id' => '796',
				'product' 	 => 'woodclean-md',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4092 => [
				'product_id' => '797',
				'product' 	 => 'freshener-md',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash' => [
					'repeat' => '',
					'problem' => '',
					'error_delivery' => '',
					'return' => '',
					'trash'	=> '',
				],
				'reject' => [
					'cancel' => '',
				],
				'expect' => [
					'new' => '',
					'callback' => '',
				],
				'confirm' => [
					'ordered' => '',
					'paid' => '',
					'in_delivery' => '',
				],
			],
			'brakeLogFolder' => true,
			'urlOrderAdd' 	 => 'http://api.top-sale.online/api2/createorder',
			'urlOrderInfo' 	 => 'http://api.top-sale.online/api2/order',
		]
	],
	
	7628 => [
		'apileadApiKey' => '500ef368b14c543dff24c0a40367100e',
		'pid' 			=> '1281a1246f131a6c6f199bbbad31fa65',
		
		'offers' => [
			3649 => [
				'product_id' => '292',
				'product' 	 => 'woodclean-ro',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3692 => [
				'product_id' => '628',
				'product' 	 => 'wetleather-ro',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3693 => [
				'product_id' => '625',
				'product' 	 => 'wetleather-bg',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3765 => [
				'product_id' => '663',
				'product' 	 => 'cleaner-ro',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3766 => [
				'product_id' => '665',
				'product' 	 => 'gt08watch-ro',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3767 => [
				'product_id' => '666',
				'product' 	 => 'razorlessshaving-ro',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3768 => [
				'product_id' => '660',
				'product' 	 => 'nailcorrect-ro',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3354 => [
				'product_id' => '667',
				'product' 	 => 'microdvr-p-ro',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3538 => [
				'product_id' => '669',
				'product' 	 => 'microdvr-bg',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3789 => [
				'product_id' => '668',
				'product' 	 => 'oximetr-ro',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3817 => [
				'product_id' => '687',
				'product' 	 => 'starsky-ro',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3818 => [
				'product_id' => '688',
				'product' 	 => 'starsky-bg',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3953 => [
				'product_id' => '759',
				'product' 	 => 'woodclean-hu',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3954 => [
				'product_id' => '626',
				'product' 	 => 'woodclean-bg',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3193 => [
				'product_id' => '785',
				'product' 	 => 'starrysky-hu',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4073 => [
				'product_id' => '784',
				'product' 	 => 'stereobigger-hu',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4074 => [
				'product_id' => '786',
				'product' 	 => 'amst-hu',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4087 => [
				'product_id' => '664',
				'product' 	 => 'freshener-ro',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4088 => [
				'product_id' => '793',
				'product' 	 => 'freshener-bg',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4089 => [
				'product_id' => '794',
				'product' 	 => 'freshener-hu',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4227 => [
				'product_id' => '872',
				'product' 	 => 'ceas-ro',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4229 => [
				'product_id' => '871',
				'product' 	 => 'paint-ro',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4228 => [
				'product_id' => '868',
				'product' 	 => 'turbovyhlop-ro',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4290 => [
				'product_id' => '897',
				'product' 	 => 'paint-ro',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4291 => [
				'product_id' => '898',
				'product' 	 => 'paint-bg',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4292 => [
				'product_id' => '899',
				'product' 	 => 'paint-hu',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2626 => [
				'product_id' => '948',
				'product' 	 => 'amst-ro',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2701 => [
				'product_id' => '949',
				'product' 	 => 'amst-bg',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash' => [
					'repeat' => '',
					'problem' => '',
					'error_delivery' => '',
					'return' => '',
					'trash'	=> '',
				],
				'reject' => [
					'cancel' => '',
				],
				'expect' => [
					'new' => '',
					'callback' => '',
				],
				'confirm' => [
					'ordered' => '',
					'paid' => '',
					'in_delivery' => '',
				],
			],
			'brakeLogFolder' => true,
			'urlOrderAdd' 	 => 'http://api.top-sale.online/api2/createorder',
			'urlOrderInfo' 	 => 'http://api.top-sale.online/api2/order',
		]
	]
];

?>