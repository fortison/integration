<?php

/*
app.limonad.com
login 	=> advert@apilead.com
pass 	=> 12365487
*/

return [
	12478 => [
		'apileadApiKey'				=> '5fa34385ba54e164ac5ad5d81e43b295',
		'apiKey' 					=> 'd3ec8b436db8c4a59494a19f0e019478',
		'postbackApiKey'			=> '4a49ddd1f0696185937e38151337c407',
		
		'offers' => [
			4314 => [
				'offerId'           => '717153c1-8bc3-4305-ab16-dfbce8ec09a4', //Мьянма
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4315 => [
				'offerId'           => '454f9903-3ccc-4629-baef-5c2772fa8211', //Филиппины
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4316 => [
				'offerId'           => '9f6ff26d-1357-40ae-b52d-36ddd493af11', //Индонезия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4317 => [
				'offerId'           => '75755b40-52e4-4040-a71d-0f066c347d47', //Малайзия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4404 => [
				'offerId'           => '8df61a5f-bce1-4741-ab22-3b69a891be8f', //Колумбия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4405 => [
				'offerId'           => '4169ee54-a476-4041-b2b0-56e3d7233828', //Колумбия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4406 => [
				'offerId'           => 'bb2e43ec-3cbf-4692-aac5-5b0b4f2f316f', //Малайзия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4407 => [
				'offerId'           => '16920b3c-b86d-4197-ae2e-0f1bb49d665d', //Малайзия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4408 => [
				'offerId'           => '45bb468e-eee1-4e53-b974-dee34732cd70', //Малайзия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4409 => [
				'offerId'           => 'f4b82d52-d563-45b9-a364-0507d35a29b3', //Малайзия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4410 => [
				'offerId'           => '744bed40-ed70-4307-be0d-c3af0adb788f', //Малайзия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4411 => [
				'offerId'           => 'c3df6688-43ca-4471-a731-9a5c11f57cfe', //Малайзия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4412 => [
				'offerId'           => 'd3d26389-e42b-4cb0-aa92-643d5c9688d8', //Малайзия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4413 => [
				'offerId'           => 'e864abba-9904-472f-9557-4bd9d95647ac', //Малайзия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4414 => [
				'offerId'           => 'e864abba-9904-472f-9557-4bd9d95647ac', //Малайзия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4415 => [
				'offerId'           => 'bc5f3af4-7325-4f01-89be-271ce59e5201', //Малайзия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4416 => [
				'offerId'           => '9866f80a-f420-4e60-b111-91712e97d16d', //Малайзия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4417 => [
				'offerId'           => 'da4a4635-1a8c-4e88-86af-def2b330cdec', //Малайзия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4418 => [
				'offerId'           => '7bc42da4-75e9-4040-965d-decc27a83117', //Малайзия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4419 => [
				'offerId'           => '6609aa7f-0c54-4ea3-a032-10c77d8dc2a3', //Малайзия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4420 => [
				'offerId'           => 'ab2e16fd-8ae8-4496-a935-198c51d8f486', //Малайзия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4422 => [
				'offerId'           => '23259215-5e88-49ee-a905-9a87b895a077', //Малайзия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4423 => [
				'offerId'           => '37385aaf-ebda-444e-b2ce-7b9108fe3601', //Малайзия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
			4451 => [
				'offerId'           => 'fbb63b31-341e-4e70-b77b-9b02bc64ff1c', //Малайзия
				'configs' => [
					'brakeLogFolder'    => true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	    => [
				'trash'	          	=> '',
			],
			'reject'	=> [
				'reject'	        => '',
			],
			'expect'	=> [
				'expect'			=> '',
			],
			'confirm'	=> [
				'confirm'	       	=> '',
			],
		],
		
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'http://lead.limonad.com/api/v3/lead/add',
	],
];
