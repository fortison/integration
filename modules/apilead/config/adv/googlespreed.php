<?php

return [
	
	10 => [
		'apileadApiKey'				=> 'fa7f5e486616a7fe42ab71aa54326a75',
		'postbackApiKey'			=> '002e827687a975a3c325781980308eda',
		
		'offers' => [
			2630 => [
				'name' => 'entry.1778149045',
				'phone' => 'entry.795697132',
				
				'configs' => [
					'urlOrderAdd'		=> 'https://docs.google.com/forms/d/e/1FAIpQLSdfO7knSfR-nqvTMyKcKzKjey1QYGZl98ITwJFfpUDlPNOhhQ/formResponse ',
					'brakeLogFolder'	=> true,
				],
			],
			
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trashed'	=> '',
			],
			'reject'	=> [
				'cancelled'	=> '',
			],
			'expect'	=> [
				'new'		=> '',
			],
			'confirm'	=> [
				'approved'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
	],
];

?>
