<?php

/*
 * https://docs.google.com/spreadsheets/d/1kBrCITm7vDNfGdH9x8SczhQFfL5e1XFstuXnB5riC9k/edit#gid=0 - названия продуктов в happysale
 */

return [
	
	1253 => [
		'apileadApiKey'				=> 'e1933c2ee3405b5c652399ba31f921ad',
		'secret'					=> 'mmnuDB7gVoGS',
		'uid'						=> '83618158',  // Логін в системі happysale
		
		'offers' => [
			593 => [
				'name' => 'slim_combidress',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1733 => [
				'name' => 'korrektor_osanka',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			879 => [
				'name' => 'korrektor_osanka',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			893 => [
				'name' => 'revoskin',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1527 => [
				'name' => 'auto_scaner',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			898 => [
				'name' => 'ecopills',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			352 => [
				'name' => 'dior_earrings',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1643 => [
				'name' => 'dior_earrings',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			864 => [
				'name' => 'ocean_heart',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			869 => [
				'name' => 'platinus_v',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			199 => [
				'name' => 'zb_prostatic',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			895 => [
				'name' => 'jeggins',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			455 => [
				'name' => 'valgus_pro',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			456 => [
				'name' => 'fuel_shark_econom',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			457 => [
				'name' => 'water_saver',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			458 => [
				'name' => 'camomile_cream',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			459 => [
				'name' => 'cream_bust_salon',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1894 => [
				'name' => 'riddex',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			467 => [
				'name' => 'titan_gel',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			468 => [
				'name' => 'provocation_cream',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			282 => [
				'name' => 'fishhungry',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			155 => [
				'name' => 'choko_slim',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			482 => [
				'name' => 'abgymnic',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			210 => [
				'name' => 'zb_pain',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			522 => [
				'name' => 'easynosmoke',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			406 => [
				'name' => 'miracle_glow',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			521 => [
				'name' => 'gialuron',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			595 => [
				'name' => 'waist_trainer',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			596 => [
				'name' => 'сoffee_scrub',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			597 => [
				'name' => 'intoxic',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			599 => [
				'name' => 'shishka_stop',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			858 => [
				'name' => 'lamzak',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1409 => [
				'name' => 'maxilash',
				'country'	=> 'BY',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'http://happy-crm.ru/api/send_order.php',
			'urlOrderInfo'		=> 'http://happy-crm.ru/api/get_orders.php',
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				5		=> '',
				6		=> '',
			],
			'reject'	=> [
				2		=> '',
			],
			'expect'	=> [
				3		=> '',
				4		=> '',
				0		=> '',
			],
			'confirm'	=> [
				1		=> '',
			],
		],
		'brakeLogFolder'			=> true,
	],
];
?>