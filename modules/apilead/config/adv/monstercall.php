<?php

/*
 * https://docs.google.com/document/d/1MpQAwjfrvkBh9KrveuwkWzSAkxod5slGhIHn1njwAb8/edit
 */

return [
	
	6303 => [
		'apileadApiKey'				=> '285749ad71dbd4d18d517e1b8852a034',
		'apiKey'					=> '64e2a200-33d9-4e3c-b17c-bde674a2f841',
		'partnerId'					=> 20,
		'reffId'					=> 3,
		
		'offers' => [
			2808 => [
				'productUpsellPackageId'	=> 5,
				'processingType'			=> 3,
				'productList'				=> [
					'Заказать мишку “Серый с сердцем 40 см“' => [
						'productId' => 40,
						'productCost' => 3180,
					],
					'Заказать мишку “Белый с лентой 40 см”' => [
						'productId' => 33,
						'productCost' => 2980,
					],
					'Заказать мишку “Красный с лентой 40 см“' => [
						'productId' => 34,
						'productCost' => 2980,
					],
					'Заказать мишку “Серый с лентой 40 см“' => [
						'productId' => 35,
						'productCost' => 2980,
					],
					'Заказать мишку “Розовый с лентой 40 см“' => [
						'productId' => 36,
						'productCost' => 2980,
					],
					'Заказать мишку “Сиреневый с лентой 40 см“' => [
						'productId' => 37,
						'productCost' => 2980,
					],
					'Заказать мишку “Голубой с лентой 40 см“' => [
						'productId' => 38,
						'productCost' => 2980,
					],
					'Заказать мишку “Белый с сердцем 40 см“' => [
						'productId' => 29,
						'productCost' => 3180,
					],
					'Заказать мишку “Коричневый с сердцем 40 см“' => [
						'productId' => 39,
						'productCost' => 3180,
					],
					'Заказать мишку “Красный с сердцем 40 см“' => [
						'productId' => 31,
						'productCost' => 3180,
					],
					'Заказать мишку “Розовый с сердцем 40 см“' => [
						'productId' => 30,
						'productCost' => 3180,
					],
					'Заказать мишку “Подарочная упаковка 40 см“' => [
						'productId' => 32,
						'productCost' => 650,
					],
					'Заказать мишку “Белый с лентой 40 см“' => [
						'productId' => 33,
						'productCost' => 2980,
					],
				],
				'products'		=> function ($model, $config) {
					$configOffer	= (object) $config->offer->array;
					$productName	= $model->address;
					$productId		= $configOffer->productList[$productName]['productId'] ?? '33';
					$productCost	= $configOffer->productList[$productName]['productCost'] ?? '2980';
					
					return "{$productId};1;{$productCost}";
				},
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],

			2779 => [
				'productUpsellPackageId'	=> 5,
				'processingType'			=> 3,
				'productList'				=> [
					'Заказать мишку “Серый с сердцем 40 см“' => [
						'productId' => 40,
						'productCost' => 3180,
					],
					'Заказать мишку “Белый с лентой 40 см”' => [
						'productId' => 33,
						'productCost' => 2980,
					],
					'Заказать мишку “Красный с лентой 40 см“' => [
						'productId' => 34,
						'productCost' => 2980,
					],
					'Заказать мишку “Серый с лентой 40 см“' => [
						'productId' => 35,
						'productCost' => 2980,
					],
					'Заказать мишку “Розовый с лентой 40 см“' => [
						'productId' => 36,
						'productCost' => 2980,
					],
					'Заказать мишку “Сиреневый с лентой 40 см“' => [
						'productId' => 37,
						'productCost' => 2980,
					],
					'Заказать мишку “Голубой с лентой 40 см“' => [
						'productId' => 38,
						'productCost' => 2980,
					],
					'Заказать мишку “Белый с сердцем 40 см“' => [
						'productId' => 29,
						'productCost' => 3180,
					],
					'Заказать мишку “Коричневый с сердцем 40 см“' => [
						'productId' => 39,
						'productCost' => 3180,
					],
					'Заказать мишку “Красный с сердцем 40 см“' => [
						'productId' => 31,
						'productCost' => 3180,
					],
					'Заказать мишку “Розовый с сердцем 40 см“' => [
						'productId' => 30,
						'productCost' => 3180,
					],
					'Заказать мишку “Подарочная упаковка 40 см“' => [
						'productId' => 32,
						'productCost' => 650,
					],
					'Заказать мишку “Белый с лентой 40 см“' => [
						'productId' => 33,
						'productCost' => 2980,
					],
				],
				'products'		=> function ($model, $config) {
					$configOffer	= (object) $config->offer->array;
					$productName	= $model->address;
					$productId		= $configOffer->productList[$productName]['productId'] ?? '33';
					$productCost	= $configOffer->productList[$productName]['productCost'] ?? '2980';
					
					return "{$productId};1;{$productCost}";
				},
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2705 => [
				'productUpsellPackageId'	=> 5,
				'processingType'			=> 3,
				'productList'				=> [
					'Заказать мишку “Серый с сердцем 40 см“' => [
						'productId' => 40,
						'productCost' => 3180,
					],
					'Заказать мишку “Белый с лентой 40 см”' => [
						'productId' => 33,
						'productCost' => 2980,
					],
					'Заказать мишку “Красный с лентой 40 см“' => [
						'productId' => 34,
						'productCost' => 2980,
					],
					'Заказать мишку “Серый с лентой 40 см“' => [
						'productId' => 35,
						'productCost' => 2980,
					],
					'Заказать мишку “Розовый с лентой 40 см“' => [
						'productId' => 36,
						'productCost' => 2980,
					],
					'Заказать мишку “Сиреневый с лентой 40 см“' => [
						'productId' => 37,
						'productCost' => 2980,
					],
					'Заказать мишку “Голубой с лентой 40 см“' => [
						'productId' => 38,
						'productCost' => 2980,
					],
					'Заказать мишку “Белый с сердцем 40 см“' => [
						'productId' => 29,
						'productCost' => 3180,
					],
					'Заказать мишку “Коричневый с сердцем 40 см“' => [
						'productId' => 39,
						'productCost' => 3180,
					],
					'Заказать мишку “Красный с сердцем 40 см“' => [
						'productId' => 31,
						'productCost' => 3180,
					],
					'Заказать мишку “Розовый с сердцем 40 см“' => [
						'productId' => 30,
						'productCost' => 3180,
					],
					'Заказать мишку “Подарочная упаковка 40 см“' => [
						'productId' => 32,
						'productCost' => 650,
					],
					'Заказать мишку “Белый с лентой 40 см“' => [
						'productId' => 33,
						'productCost' => 2980,
					],
				],
				'products'		=> function ($model, $config) {
					$configOffer	= (object) $config->offer->array;
					$productName	= $model->address;
					$productId		= $configOffer->productList[$productName]['productId'] ?? '33';
					$productCost	= $configOffer->productList[$productName]['productCost'] ?? '2980';
					
					return "{$productId};1;{$productCost}";
				},
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'https://lk.monstercall.ru/api/Order/Add',
			'urlOrderInfo'		=> 'https://lk.monstercall.ru/api/Order/Get',
			'urlOrderUpdate'	=> 'http://gq2.ru/update_lid.php',
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				11 => 'Фейк',
			],
			'reject'	=> [
				10 => 'Отменён',
			],
			'expect'	=> [
				1 => 'Новый',
			],
			'confirm'	=> [
				2 => 'Подтверждён',
				3 => 'Сформирован',
				4 => 'Отправлен',
				5 => 'Доставлен',
				6 => 'Вручен',
				7 => 'Завершён',
				8 => 'Невыкуплен',
				9 => 'Возврат',
			],
		],
		'brakeLogFolder'			=> true,
	],
];
