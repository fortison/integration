<?php

/**
 * https://nutra.media/tools/api
 * web@apilead.com
 * apilead2021
 *
 * http://connectcpa.net/apilead/adv/adv-postback-status?systemUserId=11319&partnerStatus={status}&systemOrderId={data1}&postbackApiKey=9a9bdf18bdd40b6e71313a9c24c3a36a
 */

return [
	11319 => [
		'apileadApiKey'  => '9a9bdf18bdd40b6e71313a9c24c3a36b',
		'postbackApiKey' => '9a9bdf18bdd40b6e71313a9c24c3a36a',
		
		'api_key' 		 => '35cd9738cd42115d640de7367c861eaa',
		'user_id'        => 1098,
		
		'offers' => [
			4040 => [
				'offer_id' => 66,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'trash' => 'фейк',
			],
			'reject' => [
				'decline' => 'отклонен',
			],
			'expect' => [
				'waiting' => 'в обработке',
			],
			'confirm' => [
				'approved' => 'принят',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://api.nutra.media/api_web/addOrder',
		'urlOrderInfo'		=> '',
	],
];

?>