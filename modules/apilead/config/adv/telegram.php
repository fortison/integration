<?php

/**
 * chat_id в личном каб. apilead | https://apilead.com/acp/profile/update
 *
 * apilead/adv/adv-postback-status?systemUserId=6641&systemOrderId=3536&partnerStatus=trash&postbackApiKey=94a5e0f7305c9a0dsd28c148ed97186t
 */

return [
	
	6641 => [
		'apileadApiKey'				=> '94a5e0f7305c9a0d28c148ed97186a65',
		'postbackApiKey'			=> '94a5e0f7305c9a0dsd28c148ed97186t',
		'chat_id' 					=> '317597807',
		
		'offers' => [
			3536 => [
				'offer_name' => 3536,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3508 => [
				'offer_name' => 3508,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'	=> '',
			],
			'reject'	=> [
				'reject'	=> '',
			],
			'expect'	=> [
				'expect'		=> '',
			],
			'confirm'	=> [
				'confirm'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://api.telegram.org/bot450051445:AAGDUnDwPp0E_eWENa1prEjEhTdqQ8qAwH4/sendMessage',
		'urlOrderInfo'		=> '',
	],
];

?>