<?php

/*
 * https://www.acpa.space/help/api.php#ext
 */

return [
	
	7656 => [
		'apileadApiKey'				=> '935649e2b10f2614111b43afe4471c52',
		'apiKey'					=> '3aadeae3b9afba16202b10ca604667c8',
		'user' 						=> 208,
		
		'offers' => [
			2212 => [
				'offerId' => 36,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	8833 => [
		'apileadApiKey'				=> '6207b7d3e9512151775de5d72b1fba02',
		'apiKey'					=> '95a2955c52f5bea6c5018283b90cc1b7',
		'user' 						=> 254,
		
		'offers' => [
			3337 => [
				'offerId' => 47,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1839 => [
				'offerId' => 15,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3340 => [
				'offerId' => 49,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'reject'	=> [
				5		=> 'Cancelled',
			],
			'expect'	=> [
				1		=> 'New order',
				2		=> 'Processing',
				3		=> 'Callback',
				4		=> 'Hold',
				12		=> 'Deleted',
			],
			'trash'	=> [
			],
			'confirm'	=> [
				6		=> 'Packing',
				7		=> 'Sending',
				8		=> 'Transfer',
				9		=> 'Arrived',
				10		=> 'Completed',
				11		=> 'Return',
			],
		],
		'subStatuses' => [
			'expect'	=> [
			],
			'confirm'	=> [
			],
			'reject'	=> [
				2		=> 'Changed his mind',
				4		=> 'Requires certificate',
				9		=> 'Expensive',
				10		=> 'Not satisfied with delivery',
			],
			'trash'	=> [
				1		=> 'Incorrect phone',
				3		=> 'Did not order',
				5		=> 'Wrong GEO',
				6		=> 'Errors of fake',
				7		=> 'Duplicate order',
				8		=> 'Ordered elsewhere',
				11		=> 'Could not get through',
				12		=> 'Possibly fraud',
				13		=> 'Speaks different language',
				14		=> 'Product did not fit',
			],
		],
		
		'bridgeRequestTimeout'		=> 60,
		'statusRequestTimeout'		=> 60,
		'urlOrderAdd'				=> 'https://www.acpa.space/api/ext/add.json',
		'urlOrderInfo'				=> 'https://www.acpa.space/api/ext/list.json',
	],
	
];

?>