<?php

/**
 * @see http://demo-1.leadvertex.ru/webmaster/api.html
 */

return [
	
	6736 => [
		'apileadApiKey'				=> '08481af189e2619f757f09adecc53810',
		'webmasterID'				=> 5,
		
		'offers' => [
			2241 => [
				'token'				=> '7186342',
				'domain'            => 'http://ems-new2.big-sale.biz/',
				'externalID'		=> function ($model, $config) {
					return $model->id;
				},
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'additional2' => 'EMS trainer',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://art198-emstrainer2-ru2.leadgroup.su/api/webmaster/addOrder.html',
					'urlOrderInfo'		=> 'https://art198-emstrainer2-ru2.leadgroup.su/api/webmaster/v2/getOrdersByIds.html',
					'bridgeHandler' 	=> 'app\modules\apilead\handlers\adv\leadvertextwice\user_6736\LeadvertextwiceBridgeHandler',
					'statusHandler' 	=> 'app\modules\apilead\handlers\adv\leadvertextwice\user_6736\LeadvertextwiceStatusHandler',
				],
			],
			3211 => [
				'token'				=> '7186342',
				
				'domain'            => 'http://money-3.magic-present.net/?alstream=cur',
				'externalID'		=> function ($model, $config) {
					return $model->id;
				},
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'additional2' => 'Amulet 147',
				'additional5'				=> function ($model, $config) {
					return $model->address;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://art27-moneyamulet147-ru2.leadgroup.su/api/webmaster/addOrder.html',
					'urlOrderInfo'		=> 'https://art27-moneyamulet147-ru2.leadgroup.su/api/webmaster/v2/getOrdersByIds.html',
					'bridgeHandler' 	=> 'app\modules\apilead\handlers\adv\leadvertex\user_6736\LeadvertexBridgeHandler',
					'statusHandler' 	=> 'app\modules\apilead\handlers\adv\leadvertex\user_6736\LeadvertexStatusHandler',
				],
			],
			2665 => [
				'token'				=> '7186342',
				
				'domain'            => 'http://money-3.magic-present.net/?alstream=cur',
				'externalID'		=> function ($model, $config) {
					return $model->id;
				},
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'additional2' => 'Amulet 147',	
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://art27-moneyamulet147-2-ru2.leadgroup.su/api/webmaster/addOrder.html',
					'urlOrderInfo'		=> 'https://art27-moneyamulet147-2-ru2.leadgroup.su/api/webmaster/v2/getOrdersByIds.html',
					'bridgeHandler' 	=> 'app\modules\apilead\handlers\adv\leadvertex\user_6736\LeadvertexBridgeHandler',
					'statusHandler' 	=> 'app\modules\apilead\handlers\adv\leadvertex\user_6736\LeadvertexStatusHandler',
				],
			],
			2586 => [
				'token'				=> '7186342',
				
				'domain'            => 'http://money-3.magic-present.net/?alstream=cur',
				'externalID'		=> function ($model, $config) {
					return $model->id;
				},
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'additional2' => 'Amulet 147',	
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://art27-moneyamulet147-2-ru2.leadgroup.su/api/webmaster/addOrder.html',
					'urlOrderInfo'		=> 'https://art27-moneyamulet147-2-ru2.leadgroup.su/api/webmaster/v2/getOrdersByIds.html',
					'bridgeHandler' 	=> 'app\modules\apilead\handlers\adv\leadvertex\user_6736\LeadvertexBridgeHandler',
					'statusHandler' 	=> 'app\modules\apilead\handlers\adv\leadvertex\user_6736\LeadvertexStatusHandler',
				],
			],
			2339 => [
				'token'				=> '7186342',
				
				'domain'            => 'http://money-3.magic-present.net/?alstream=cur',
				'externalID'		=> function ($model, $config) {
					return $model->id;
				},
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'additional2' => 'Amulet 147',	
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://art27-moneyamulet147-2-ru2.leadgroup.su/api/webmaster/addOrder.html',
					'urlOrderInfo'		=> 'https://art27-moneyamulet147-2-ru2.leadgroup.su/api/webmaster/v2/getOrdersByIds.html',
					'bridgeHandler' 	=> 'app\modules\apilead\handlers\adv\leadvertex\user_6736\LeadvertexBridgeHandler',
					'statusHandler' 	=> 'app\modules\apilead\handlers\adv\leadvertex\user_6736\LeadvertexStatusHandler',
				],
			],
			3561 => [
				'token'				=> '7186342',
				
				'domain'            => 'http://money-3.magic-present.net/?alstream=cur',
				'externalID'		=> function ($model, $config) {
					return $model->id;
				},
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'additional2' => 'Amulet 0',	
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://art27-moneyamulet-free-ru2.leadgroup.su/api/webmaster/addOrder.html',
					'urlOrderInfo'		=> 'https://art27-moneyamulet-free-ru2.leadgroup.su/api/webmaster/v2/getOrdersByIds.html',
					'bridgeHandler' 	=> 'app\modules\apilead\handlers\adv\leadvertex\user_6736\LeadvertexBridgeHandler',
					'statusHandler' 	=> 'app\modules\apilead\handlers\adv\leadvertex\user_6736\LeadvertexStatusHandler',
				],
			],			
		],
		'reasonCancel' => [
			1 => 'Не заказывал',
			2 => 'Дорого',
			3 => 'Передумал',
			4 => 'Нет денег ',
			5 => 'Уезжает',
			6 => 'Уже купил',
			7 => 'Не туда попали',
			8 => 'На сайте другая цена',
			9 => 'Боится заказывать. Думает мошенники',
			10 => 'Отрицательные отзывы в интернете',
			11 => 'Долгая доставка',
			12 => 'Медицинские противопоказания',
			13 => 'Товар уже не интересен',
			14 => 'Муж, жена, родственники не разрешают',
			15 => 'Сделали заказ ради шутки',
			16 => 'Сделали заявку чтобы получить консультацию ',
			17 => 'Высокая цена доставки',
			18 => 'Сейчас не могу, в след. месяце сам сделаю новый заказ',
			19 => 'Недозвон в течении долгого времени',
			20 => 'Заказывал другой продукт ',
			21 => 'Не идёт на контакт, бросает трубку, агрессивно реагирует',
			99 => 'Другое',
		],
		'configs' => [
			'statuses' => [
				'confirm'	=> [
					'1'			=> '',
				],
				'reject' => [
					'-1'		=> '',
				],
				'expect' => [
					'0'			=> '',
				],
				'trash' => [
				],
			],
			'ransom' => [
				'confirm'	=> [
					'1'			=> '',
				],
				'reject'	=> [
					'-1'		=> '',
				],
				'expect'	=> [
					'0'			=> '',
				],
			],
		],
	],
	
	6817 => [
		'apileadApiKey'				=> '527dea04b3d3be8f978529488abde2b0',
		'webmasterID'				=> 18,

		'offers' => [
			3234 => [
				'token'					=> '7186342',
				'additional2'       	=> 'Аккумуляторный шуруповерт MAKITA',
				'externalWebmaster'			=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'domain'            	=> 'http://makita.all-tech.biz/',
				'goodId'           		=> '84985',	
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://boris1.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://boris1.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',	
				],
			],
		],
	],
	
	8409 => [
		'apileadApiKey'				=> 'c3f18b370233bd241d93151d63650878',
		'webmasterID'				=> 2,
		
		'offers' => [
			3152 => [
				'token'				=> 7186342,
				'goodId'           	=> '110456',
				'domain'            => 'http://car-dvr.auto-toolz.biz/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kapital-one.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kapital-one.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3252 => [
				'token'				=> 7186342,
				'goodId'           	=> '85870',
				'domain'            => 'http://creative-child.big-sale.biz/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://igrushka.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://igrushka.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3117 => [
				'token'				=> 7186342,
				'goodId'           	=> '110459',
				'domain'            => 'http://junsun.all-tech.biz/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kapital-one.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kapital-one.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],	
			3207 => [
				'token'				=> 7186342,
				'goodId'           	=> '144215',
				'domain'            => 'http://anticam.auto-toolz.biz/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://white-box.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://white-box.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],	
			3208 => [
				'token'				=> 7186342,
				'goodId'           	=> '70184',
				'domain'            => 'http://anticam.auto-toolz.biz/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kapital-one.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kapital-one.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],	
			3332 => [
				'token'				=> 7186342,
				'goodId'           	=> '110454',
				'domain'            => 'http://ztenubia.all-tech.biz/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kapital-one.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kapital-one.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],	
			3375 => [
				'token'				=> 7186342,
				'goodId'           	=> '110463',
				'domain'            => 'http://cartoy.big-sale.biz/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kapital-one.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kapital-one.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],	
			3435 => [
				'token'				=> 7186342,
				'goodId'           	=> '144244',
				'domain'            => 'http://jadod1990.all-tech.biz/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://white-box.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://white-box.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],	
			3411 => [
				'token'				=> 7186342,
				'goodId'           	=> '134575',
				'domain'            => 'http://fugicam.all-tech.biz/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://auto-ssn.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://auto-ssn.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],	
			3417 => [
				'token'				=> 7186342,
				'goodId'           	=> '144224',
				'domain'            => 'http://junsun.all-tech.biz/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://white-box.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://white-box.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3479 => [
				'token'				=> 7186342,
				'goodId'           	=> '120024',
				'domain'            => 'http://mi-note10.phonemartket.net/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kapital-mobi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kapital-mobi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3520 => [
				'token'				=> 7186342,
				'goodId'           	=> '119052',
				'domain'            => 'http://mate30pro.phonemartket.net/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kapital-mobi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kapital-mobi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3519 => [
				'token'				=> 7186342,
				'goodId'           	=> '118276',
				'domain'            => 'http://mate20pro.phonemartket.net/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kapital-mobi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kapital-mobi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],	
			3248 => [
				'token'				=> 7186342,
				'goodId'           	=> '128676',
				'domain'            => 'http://fujida3in1.all-tech.biz/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kapital-one.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kapital-one.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],	
			3243 => [
				'token'				=> 7186342,
				'goodId'           	=> '144231',
				'domain'            => 'http://neoline.all-tech.biz/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://white-box.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://white-box.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],	
			3573 => [
				'token'				=> 7186342,
				'goodId'           	=> '128677',
				'domain'            => 'http://maski.all-shops.biz/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kapital-one.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kapital-one.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],	
			3635 => [
				'token'				=> 7186342,
				'goodId'           	=> '86380',
				'domain'            => 'http://bluavido.all-tech.biz/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://auto-ssn.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://auto-ssn.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3207 => [
				'token'				=> 7186342,
				'goodId'           	=> '134587',
				'domain'            => 'http://anticam.auto-toolz.biz/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://auto-ssn.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://auto-ssn.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3666 => [
				'token'				=> 7186342,
				'goodId'           	=> '138811',
				'domain'            => 'http://mijiacar.all-tech.biz/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://auto-ssn.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://auto-ssn.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3667 => [
				'token'				=> 7186342,
				'goodId'           	=> '144248',
				'domain'            => 'http://bluavidoa30.all-tech.biz/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://white-box.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://white-box.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3668 => [
				'token'				=> 7186342,
				'goodId'           	=> '144252',
				'domain'            => 'http://combof5.all-tech.biz/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://white-box.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://white-box.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3669 => [
				'token'				=> 7186342,
				'goodId'           	=> '144251',
				'domain'            => 'http://',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://white-box.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://white-box.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3670 => [
				'token'				=> 7186342,
				'goodId'           	=> '144250',
				'domain'            => 'http://sho-me.all-tech.biz/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://white-box.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://white-box.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3685 => [
				'token'				=> 7186342,
				'goodId'           	=> '138761',
				'domain'            => 'http://aimanfun.all-tech.biz/',
				'additional1'				=> function ($model, $config) {
					return $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://auto-ssn.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://auto-ssn.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],			
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	
	
	
	'configs' => [
		'statuses' => [
			'confirm'	=> [   //ввввв
				'accepted'		=> '',
				'paid'			=> '',
				'full'			=> '',
				'shipped'		=> '',
			],
			'reject' => [
				'canceled'		=> '',
				'return'		=> '',
			],
			'expect' => [
				'processing'	=> '',
				'null' 			=> '',
			],
			'trash' => [
				'spam'			=> '',
			],
		],
		'subStatuses' => [
			'confirm'	=> ['accepted', 'paid'],
			'reject'	=> ['canceled', 'return'],
			'expect'	=> ['processing', 'null'],
			'trash'		=> ['spam'],
		],
		'ransom' => [																									//  Для майбутнього функціоналу перевірки на викуп
			'confirm'	=> [
				'paid'			=> '',
			],
			'reject'	=> [
				'return'		=> '',
			],
			'expect'	=> [
				'shipped'		=> '',
			],
		],
//		'urlApileadOrderUpdate'		=> 'http://al-api.com/api/lead/update',
//		'bridgeEnabled'				=> 1,																				// Чи активна ця інтеграція
//		'statusEnabled'				=> null,																			// Чи опрацьовувати статуси цієї інтеграції
//		'urlOrderAdd'				=> 'https://magnetlashes990.leadvertex.ru/addOrder.html',
//		'urlOrderInfo'				=> 'https://magnetlashes990.leadvertex.ru/getOrdersByIds.html',
		'bridgeRequestTimeout'		=> 60,
		'statusRequestTimeout'		=> 60,
		'enablePhoneChars'			=> false,
//		'brakeLogFolder'			=> '',
//		'statusHandler' 			=> 'app\modules\apilead\handlers\adv\leadvertex\LeadvertexStatusHandler',
//		'bridgeHandler' 			=> 'app\modules\apilead\handlers\adv\leadvertex\LeadvertexBridgeHandler',
	],
];