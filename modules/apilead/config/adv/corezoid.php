<?php

/*
 * Відсутня
 */

return [
	
	5935 => [
		'apileadApiKey'				=> '37e38152259c4074a49ddd1f06961859',
		'postbackApiKey'			=> '4a49ddd1f0696185937e38152259c407',
		
		'offers' => [
			2705 => [
				'productList'				=> [
					'Заказать мишку “Серый с сердцем 40 см“' => [
						'productCost' => 3180,
					],
					'Заказать мишку “Белый с лентой 40 см”' => [
						'productCost' => 2980,
					],
					'Заказать мишку “Красный с лентой 40 см“' => [
						'productCost' => 2980,
					],
					'Заказать мишку “Серый с лентой 40 см“' => [
						'productCost' => 2980,
					],
					'Заказать мишку “Розовый с лентой 40 см“' => [
						'productCost' => 2980,
					],
					'Заказать мишку “Сиреневый с лентой 40 см“' => [
						'productCost' => 2980,
					],
					'Заказать мишку “Голубой с лентой 40 см“' => [
						'productCost' => 2980,
					],
					'Заказать мишку “Белый с сердцем 40 см“' => [
						'productCost' => 3180,
					],
					'Заказать мишку “Коричневый с сердцем 40 см“' => [
						'productCost' => 3180,
					],
					'Заказать мишку “Красный с сердцем 40 см“' => [
						'productCost' => 3180,
					],
					'Заказать мишку “Розовый с сердцем 40 см“' => [
						'productCost' => 3180,
					],
					'Заказать мишку “Подарочная упаковка 40 см“' => [
						'productCost' => 650,
					],
					'Заказать мишку “Белый с лентой 40 см“' => [
						'productCost' => 2980,
					],
				],
				'products'		=> function ($model, $config) {
					$configOffer	= (object) $config->offer->array;
					$productName	= utf8_decode($model->address);
					$productCost	= $configOffer->productList[$productName]['productCost'] ?? '2980';
					
					return "{$productName};{$productCost} цена";
				},
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			
			2779 => [
				'productUpsellPackageId'	=> 5,
				'processingType'			=> 3,
				'productList'				=> [
					'Заказать мишку “Серый с сердцем 40 см“' => [
						'productCost' => 3180,
					],
					'Заказать мишку “Белый с лентой 40 см”' => [
						'productCost' => 2980,
					],
					'Заказать мишку “Красный с лентой 40 см“' => [
						'productCost' => 2980,
					],
					'Заказать мишку “Серый с лентой 40 см“' => [
						'productCost' => 2980,
					],
					'Заказать мишку “Розовый с лентой 40 см“' => [
						'productCost' => 2980,
					],
					'Заказать мишку “Сиреневый с лентой 40 см“' => [
						'productCost' => 2980,
					],
					'Заказать мишку “Голубой с лентой 40 см“' => [
						'productCost' => 2980,
					],
					'Заказать мишку “Белый с сердцем 40 см“' => [
						'productCost' => 3180,
					],
					'Заказать мишку “Коричневый с сердцем 40 см“' => [
						'productCost' => 3180,
					],
					'Заказать мишку “Красный с сердцем 40 см“' => [
						'productCost' => 3180,
					],
					'Заказать мишку “Розовый с сердцем 40 см“' => [
						'productCost' => 3180,
					],
					'Заказать мишку “Подарочная упаковка 40 см“' => [
						'productCost' => 650,
					],
					'Заказать мишку “Белый с лентой 40 см“' => [
						'productCost' => 2980,
					],
				],
				'products'		=> function ($model, $config) {
					$configOffer	= (object) $config->offer->array;
					$productName	= $model->address;
					$productCost	= $configOffer->productList[$productName]['productCost'] ?? '2980';
					
					return "{$productName};{$productCost} цена";
				},
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'https://www.corezoid.com/api/1/nvp/public/493263/c0c2b3a3c55046c9e6b60cd8f2a803c64b172b0a',
		],
	],
	
	6641 => [
		'apileadApiKey'				=> '94a5e0f7305c9a0d28c148ed97186a65',
		'postbackApiKey'			=> '94a5e0f7305c9a0d28c148ed97186a65',
		'offers' => [
			
			2786 => [
				'products'				=> function ($model, $config) {
					return $model->address;
				},
				'country' => 'UA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3506 => [
				'products'				=> function ($model, $config) {
					return $model->address;
				},
				'country' => 'RU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://www.corezoid.com/api/1/nvp/public/675877/513e94ebd7b560ef81d8eb51614d97706dbf8ba7',
				],
			],
			2897 => [
				'products'				=> function ($model, $config) {
					return $model->address;
				},
				'country' => 'KZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://www.corezoid.com/api/1/nvp/public/675877/513e94ebd7b560ef81d8eb51614d97706dbf8ba7',
				],
			],
			3581 => [
				'products'				=> function ($model, $config) {
					return $model->address;
				},
				'country' => 'UA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://www.corezoid.com/api/1/nvp/public/699883/50df3d1fb758f1febfe3af462c8daf15ba8ba67b',
				],
			],
		],
		
		'configs' => [
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'https://www.corezoid.com/api/1/nvp/public/675877/513e94ebd7b560ef81d8eb51614d97706dbf8ba7',
		],
	],
	
	
	
	8182 => [
		'apileadApiKey'				=> '20e7569a9d4fcc33977683d6a15a93a9',
		'postbackApiKey'			=> '20e7569a9d4fcc33977683d6a15a93a9',
		'identify'					=> 'gdVre3Hhj5Jfcn6Gg9hkF4jhu',
		
		'offers' => [
			
			1627 => [
				'products'				=> function ($model, $config) {
					return $model->address;
				},
				'country' => 'RU',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			
			3053 => [
				'products'				=> function ($model, $config) {
					return $model->address;
				},
				'country' => 'UA',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'https://callsystems.com.ua/gf5hgh4/dgdVBF4DC3GHDF/VBCGH.php',
			'bridgeHandler' 	=> 'app\modules\apilead\handlers\adv\corezoid\user_8182\CorezoidBridgeHandler',
			
			'statuses' => [
				'trash'		=> [
					'Спам/Дубль'			=> '',
					'Спам'					=> '',
					'Дубль'					=> '',
				],
				'reject'	=> [
					'Отказ'					=> '',
				],
				'expect'	=> [
					'Новый'					=> '',
					'Перезвон'				=> '',
					'Недозвон'				=> '',
				],
				'confirm'	=> [
					'Передано Рекламодателю'	=> '',
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'		=> [
				'trash'					=> '',
			],
			'reject'	=> [
				'reject'				=> '',
			],
			'expect'	=> [
				'expect'				=> '',
			],
			'confirm'	=> [
				'confirm'				=> '',
			],
		],
		'brakeLogFolder'			=> true,
	],
];