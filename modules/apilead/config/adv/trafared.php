<?php

/*
 * http://89.218.86.178:8081/api/doc.txt
 *
http://89.218.86.178:8081/api/send_order.php отправка данных о заявках на товар в систему
формат передачи даных - JSON
Данные передаютс¤ методом POST в переменную POST["data"]

Ответ сервера на запрос приходит как JSON строка
Пример ответа:
{"result":{"success":"FALSE","message":"Incorrect hash"}}
{"result":{"success":"TRUE","id":823421}}
Пример отправки данных (–Ќ–):
<?php
$uid = "11223344";                       // Логин в системе
$secret = "11223344";                    //  Ключ API

$data = array(
    "phone" => "79017777777",            // номер телефона заказчика товара (* Обязательное поле)
    "price" => "6400",                   // стоимость товара
    "order_id" => "123321",              // ID заявки дл¤ синхронизации
    "name" => "Gena Ivanov",             // ФИО заказчика товара
    "country" => "kz",                   // двухбуквенный (ISO 3166-1 alpha-2) код страны в нижнем регистре (* Обязательное поле)
    "addr" => "ул. —арайшык 38, јстана", // адрес
    "offer" => "brutalin",               // наименование товара (поле стандартизировано, техн. название дается в процессе согласования) (* Обязательное поле)
    "secret" => $secret                  // ключ API (* Обязательное поле)
);

$data = json_encode($data);
$hash_str = strlen($data) . md5($uid);
$hash = hash("sha256", $hash_str);
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://89.218.86.178:8081/api/send_order.php?uid=" . $uid . "&hash=" . $hash);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, array("data" => $data));
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
curl_setopt($ch, CURLOPT_HTTPHEADER, array("Pragma: no-cache"));
$result = array();
$result['EXE'] = curl_exec($ch);
$result['INF'] = curl_getinfo($ch);
$result['ERR'] = curl_error($ch);

var_dump($result); // Ответ API на запрос

---------------- Получение статусов и данных по заказу

http://89.218.86.178:8081/api/get_orders.php?uid=xxxxxxxx&s=secret&hash=hash_str

$_POST['data'] = '[{"id":440465},{"id":440466}]';  440466 - айди заказа в нашей системе который получен при отправке заказа в нашу систему

Статусы подтверждения (переменная status)

"0" "новая"
"1" "Подтвержден"
"2" "Отменён"
"3" "Перезвонить"
"4" "Недозвон"
"5" "Брак"
"6" "Уже получил заказ"
"7" "Черный список"
"8" "Заказано у конкурентов"
"10" "недозвон_ночь"

Статусы доставки (переменная send_status)

"0" "Отправлен"
"4" "Отказ"
"5" "Оплачен"

Статусы посылки (переменная status_kz)

"0" "Обработка"
"1" "Отложенная доставка"
"2" "На доставку"
"4" "Упакован на почте"
"5" "Заберет"
"6" "Упакован"
"7" "Хранение"
"8" "Упакован принят"
"9" "Обратная доставка отправлена"
"10" "Груз вручен"
"11" "Груз в дороге"
"13" "Получен"
"15" "Располовинен"
"14" "Нет товара"
"16" "Проверен"
"17" "Свежий"
"18" "Автоответчик"
"19" "Перезвонить"
"20" "Сделать замену"
"21" "Возврат денег"
"22" "На контроль"
"23" "Упакован добавочный"
"24" "Частичный возврат"
 */

return [
	
	2032 => [
		'apileadApiKey'				=> '2cf46b873ef685236186cb3ec4bf63c5',
		'secret'					=> '67e022c56882afe',
		'uid'						=> '74421777',
		
		'offers' => [
			1840 => [
				'offerId'		=> 'minoxidil',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1566 => [
				'offerId'		=> 'money-amulet',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2718 => [
				'offerId'		=> 'pullover',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2735 => [
				'offerId'		=> 'artificial_tree',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],			
		],
		
		'configs' => [
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'http://89.218.86.178:8081/api/send_order.php',
			'urlOrderInfo'		=> 'http://89.218.86.178:8081/api/get_orders.php',
		],
	],

	5034 => [
		'apileadApiKey'				=> '66fdc005c98ab6b21696a9a14bec9404',
		'secret'					=> '67e022c56882afe',
		'uid'						=> '74421777',
		
		'offers' => [
			1566 => [
				'offerId'		=> 'money-amulet',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2714 => [
				'offerId'		=> 'pullover',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2736 => [
				'offerId'		=> 'artificial_tree',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2829 => [
				'offerId'		=> 'berry',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3157 => [
				'offerId'		=> 'virgin_star',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'http://89.218.86.178:8081/api/send_order.php',
			'urlOrderInfo'		=> 'http://89.218.86.178:8081/api/get_orders.php',
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'		=> [
				5		=> '',
				6		=> '',
				7		=> '',
				8		=> '',
			],
			'reject'	=> [
				2			=> '',
			],
			'expect'	=> [
				0			=> '',
				3			=> '',
				4			=> '',
				10			=> '',
			],
			'confirm'	=> [
				1			=> '',
			],
		],
		'brakeLogFolder'			=> true,
	],
];
?>