<?php

/*
 * https://cpa.teledirekt.ru/help/api-dla-vebmasterov
 *
 * http://connectcpa.net/apilead/adv/adv-postback-status?systemOrderId={data1}&partnerStatus={status}&comment={comment}&postbackApiKey=8199a3c328078eda005302e827687a57
 */

return [
	5399 => [
		'apileadApiKey' => 'bdefb61de1c98d03dd3bc4d7fea38788',
		'userId' => '14953',
		'apiKey' => '2d140981be43a48e78e84bd4077fd108',
		
		'offers' => [
			2728 => [
				'offerId' => 844,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2463 => [
				'offerId' => 792,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2053 => [
				'offerId' => 807,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2618 => [
				'offerId' => 839,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2588 => [
				'offerId' => 792,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3291 => [
				'offerId' => 841,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3294 => [
				'offerId' => 822,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3293 => [
				'offerId' => 831,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3312 => [
				'offerId' => 827,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2699 => [
				'offerId' => 845,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3398 => [
				'offerId' => 863,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3441 => [
				'offerId' => 874,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3442 => [
				'offerId' => 873,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3567 => [
				'offerId' => 877,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3568 => [
				'offerId' => 871,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3569 => [
				'offerId' => 878,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3696 => [
				'offerId' => 885,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3695 => [
				'offerId' => 884,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3822 => [
				'offerId' => 886, 
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3847 => [
				'offerId' => 889, 
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				2 => 'trash',
			],
			'reject' => [
				-1 => 'cancel',
			],
			'expect' => [
				-2 => 'new',
				0 => 'pending',
			],
			'confirm' => [
				1 => 'approve',
				3 => 'guarant',
			],
		],
		
		'brakeLogFolder' => true,
		'urlOrderAdd' => 'http://landings.teledirekt.ru/order/api.php',
		'urlOrderInfo' => 'http://cpa.teledirekt.ru/api3/get_leads',
	],
];