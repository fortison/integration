<?php

/*
 * https://cpa.house/webmaster/api
 * advert@apilead.com
 * 5SniCNm!@9uDN6p
 * */

return [
	
	10133 => [
		'apileadApiKey'				=> 'b85c0cae014d1804de56be37beef1add',
		'postbackApiKey'			=> '421ce6ef5f3aa7b4103511a7ff8e49fe0689acc6',
		'api_key' 					=> 'VsUkzSJqStR7knaZV1qj2iHFsQbx79d3E8oinuzrKyocOFZcXYgwTpuA4tym9jmP',
		'id_webmaster'				=> '76377',
		
		'offers' => [
			4298 => [
				'id_offer'		=> '822',
				'id_source'		=> '8434',
				'id_stream' 	=> '44167',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4297 => [
				'id_offer'		=> '661',
				'id_source'		=> '8434',
				'id_stream' 	=> '44165',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4261 => [
				'id_offer'		=> '752',
				'id_source'		=> '8434',
				'id_stream' 	=> '44166',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4179 => [
				'id_offer'		=> '926',
				'id_source'		=> '8434',
				'id_stream' 	=> '44168',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4190 => [
				'id_offer'		=> '927',
				'id_source'		=> '8434',
				'id_stream' 	=> '44163',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2520 => [
				'id_offer'		=> '40',
				'id_source'		=> '8434',
				'id_stream' 	=> '45214',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4472 => [
				'id_offer'		=> '250',
				'id_source'		=> '8434',
				'id_stream' 	=> '46704',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4476 => [
				'id_offer'		=> '795',
				'id_source'		=> '8434',
				'id_stream' 	=> '48010',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4477 => [
				'id_offer'		=> '794',
				'id_source'		=> '8434',
				'id_stream' 	=> '48008',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4478 => [
				'id_offer'		=> '1009',
				'id_source'		=> '8434',
				'id_stream' 	=> '48017',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4479 => [
				'id_offer'		=> '1010',
				'id_source'		=> '8434',
				'id_stream' 	=> '48029',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4502 => [
				'id_offer'		=> '1056',
				'id_source'		=> '8434',
				'id_stream' 	=> '61770',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4571 => [
				'id_offer'		=> '1025',
				'id_source'		=> '8434',
				'id_stream' 	=> '77122',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4572 => [
				'id_offer'		=> '1240',
				'id_source'		=> '8434',
				'id_stream' 	=> '77124',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4575 => [
				'id_offer'		=> '385',
				'id_source'		=> '8434',
				'id_stream' 	=> '78202',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4576 => [
				'id_offer'		=> '275',
				'id_source'		=> '8434',
				'id_stream' 	=> '78203',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'fake'				=> '',
			],
			'reject'	=> [
				'rejected'			=> '',
			],
			'expect'	=> [
				'in_processing'		=> '',
				'new'				=> '',
			],
			'confirm'	=> [
				'accepted'			=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://api.cpa.house/method/send_order',
		'urlOrderInfo'		=> '',
	],
];
?>