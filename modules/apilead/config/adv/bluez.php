<?php

return [
	
	347 => [
		'apileadApiKey'				=> '788988cf0ab54edf2044266580f238d0',
		'id'						=> '659-89be9a0708e39d37698e899d19cdb2c2',
		
		'offers' => [
			743 => [
				'offerId' => 47,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			289 => [
				'offerId' => 39,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			804 => [
				'offerId' => 64,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			816 => [
				'offerId' => 64,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			815 => [
				'offerId' => 64,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			813 => [
				'offerId' => 39,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			49 => [
				'offerId' => 103,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			65 => [
				'offerId' => 108,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			150 => [
				'offerId' => 133,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1444 => [
				'offerId' => 160,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1445 => [
				'offerId' => 95,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1446 => [
				'offerId' => 95,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2610 => [
				'offerId' => 95,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1447 => [
				'offerId' => 95,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1448 => [
				'offerId' => 52,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1449 => [
				'offerId' => 52,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1450 => [
				'offerId' => 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1451 => [
				'offerId' => 124,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1452 => [
				'offerId' => 124,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1453 => [
				'offerId' => 67,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1454 => [
				'offerId' => 67,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1461 => [
				'offerId' => 67,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			431 => [
				'offerId' => 135,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1455 => [
				'offerId' => 135,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1456 => [
				'offerId' => 86,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1457 => [
				'offerId' => 86,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1458 => [
				'offerId' => 86,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			309 => [
				'offerId' => 86,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1459 => [
				'offerId' => 69,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1460 => [
				'offerId' => 69,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			493 => [
				'offerId' => 66,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			81 => [
				'offerId' => 66,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1424 => [
				'offerId' => 66,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			308 => [
				'offerId' => 86,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1839 => [
				'offerId' => 356,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'http://bluez.pro/api/ext/add.json',
			'urlOrderInfo'		=> 'http://bluez.pro/api/ext/list.json',
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
//				6	=> '',
			],
			'reject'	=> [
				5	=> '',
				12	=> '',
			],
			'expect'	=> [
				1	=> '',
				2	=> '',
				3	=> '',
				4	=> '',
			],
			'confirm'	=> [
				6	=> '',
				7	=> '',
				8	=> '',
				9	=> '',
				10	=> '',
				11	=> '',
			],
		],
		'brakeLogFolder'			=> true,
	],
];
?>