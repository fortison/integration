<?php

/*
В файлі

http://connectcpa.doc/terraleads/adv/adv-postback-status?systemUserId=8244&systemOrderId={ai}&partnerStatus={status}&comment={comment}&postbackApiKey=0302e82768005a3c3287a57819978eda

Area code

0:Taiwan
1:Malay
2:Thailand
3:Vietnam
4:China Mainland
5:Indonesia
6:Romania
7:Italy
8:Spain
9: Hong Kong
10.Singapore
11: DE

 */

return [
	8244 => [
		'apileadApiKey'				=> '5560b205773be5efa039cc2d7c66c082',
		'postbackApiKey'			=> '0302e82768005a3c3287a57819978eda',
		'self_key' 					=> 'DHCGOHJDJD',

		'offers' => [
            3048 => [
				'area'			=> 5,
				'pn'			=> 'ecsp6000032',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
            3049 => [
				'area'			=> 5,
				'pn'			=> 'ecsp6000035',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
            3070 => [
				'area'			=> 1,
				'pn'			=> 'ecsp2000132',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
            3074 => [
				'area'			=> 5,
				'pn'			=> 'ecsp6000069',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
            3128 => [
				'area'			=> 0,
				'pn'			=> 'ecsp1000514',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
            3257 => [
				'area'			=> 0,
				'pn'			=> 'ecsp1000744',

				'phone'			=> function ($model, $config) {
					return "{$model->phone}, {$model->address}";
				},

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
            3270 => [
				'area'			=> 1,
				'pn'			=> 'ecsp2000340',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
            3267 => [
				'area'			=> 1,
				'pn'			=> 'ecsp2000397',

				'address'			=> function ($model, $config) {
					return $model->address;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],

	'configs' => [
        'statuses' => [
            'trash'	=> [
                 'trash' 			=> '',
            ],
            'reject'	=> [
                'cancel'	        => '',
            ],
            'expect'	=> [
                'lead'				=> '',
            ],
            'confirm'	=> [
                'sale'	        	=> '',
            ],
        ],

		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'http://api.mhorderceate.com/api/post_create_bill',
//		'statusEnabled'		=> false,
	],
];