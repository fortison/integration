<?php

return [
	
	2118 => [
		'apileadApiKey'				=> '66fa1935074f3a7cb23d1dfbe9a03d5d',
		'partnerCode'				=> 'apilead',
		'pass'						=> [
			'RU'	=> 'lOnHXm3etZmVbJna',
			'KZ'	=> '48eda0e68eaf3956',
		],
		
		'offers' => [
			49 => [
				'offerId'	=> 2088,
				'geo'		=> 'RU',
				
				'configs'	=> [
					'brakeLogFolder'	=> true,
				],
			],
			703 => [
				'offerId' => 1075,
				'geo' => 'RU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			702 => [
				'offerId' => 2073,
				'geo' => 'RU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			734 => [
				'offerId' => 2013,
				'geo' => 'RU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			704 => [
				'offerId' => 2041,
				'geo' => 'RU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			705 => [
				'offerId' => 2269,
				'geo' => 'RU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2339 => [
				'offerId' => 2544,
				'geo' => 'RU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			952 => [
				'offerId' => 1467,
				'geo' => 'RU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			953 => [
				'offerId' => 1473,
				'geo' => 'RU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			954 => [
				'offerId' => 1476,
				'geo' => 'RU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			955 => [
				'offerId' => 1475,
				'geo' => 'RU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			956 => [
				'offerId' => 1460,
				'geo' => 'RU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			957 => [
				'offerId' => 1471,
				'geo' => 'RU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			958 => [
				'offerId' => 1477,
				'geo' => 'RU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			959 => [
				'offerId'	=> 1468,
				'geo'		=> 'RU',
				
				'configs'	=> [
					'brakeLogFolder'	=> true,
				],
			],
			960 => [
				'offerId'	=> 1469,
				'geo'		=> 'RU',
				
				'configs'	=> [
					'brakeLogFolder'	=> true,
				],
			],
			961 => [
				'offerId'	=> 1466,
				'geo'		=> 'RU',
				
				'configs'	=> [
					'brakeLogFolder'	=> true,
				],
			],
			1721 => [
				'offerId'	=> 2234,
				'geo'		=> 'RU',
				
				'configs'	=> [
					'brakeLogFolder'	=> true,
				],
			],
			2134 => [
				'offerId'	=> 2509,
				'geo'		=> 'RU',
				
				'configs'	=> [
					'brakeLogFolder'	=> true,
				],
			],
			2243 => [
				'offerId'	=> 2509,
				'geo'		=> 'RU',
				
				'configs'	=> [
					'brakeLogFolder'	=> true,
				],
			],
			1437 => [
				'offerId'	=> 734,
				'geo'		=> 'RU',
				
				'configs'	=> [
					'brakeLogFolder'	=> true,
				],
			],
		],
		'configs' => [
			'statuses' => [
				'reject'	=> [
					11	=> '',
					13	=> '',
				],
				'expect'	=> [
					1		=> '',
				],
				'trash'	=> [
				],
				'confirm'	=> [
					4	=> '',
					9	=> '',
				],
			],

			'brakeLogFolder'			=> true,
			'bridgeRequestTimeout'		=> 60,
			'statusRequestTimeout'		=> 60,
			'urlOrderAdd'				=> 'http://fobos.thorxml.com/cpa/income',
			'urlOrderInfo'				=> 'http://fobos.thorxml.com/API/Order/getOrdersStatus',
		],
	],
];

?>