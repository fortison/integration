<?php

return [
	
	669 => [
		'apileadApiKey'	=> '2368e439884643f279ea305891b6090f',
		'partnerId' 	=> 298789,
		'access_token'	=> '257c581bb5b2781475d6ea3e0aa02858',
		
		'offers' => [
			1892 => [
				'offerId' => 460, // ID оффера Вашего заказа (Обязательное поле)
//				'splitId' => 41928, // ID потока (Не обязательное поле)
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2048 => [
				'offerId' => 474,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2117 => [
				'offerId' => 474,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2192 => [
				'offerId' => 519,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2193 => [
				'offerId' => 519,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2194 => [
				'offerId' => 518,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2195 => [
				'offerId' => 483,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2215 => [
				'offerId' => 526,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		'configs' => [
			'statuses' => [
				'reject'	=> [
					2	=> 'отклонен',
				],
				'expect'	=> [
					0		=> 'в ожидании',
					3		=> 'в ожидании',
					6		=> 'в ожидании',
				],
				'trash'	=> [
					4	=> 'некоррект',
				],
				'confirm'	=> [
					1	=> 'подтвержден',
				],
			],

			'brakeLogFolder'			=> true,
			'bridgeRequestTimeout'		=> 60,
			'statusRequestTimeout'		=> 60,
			'urlOrderAdd'				=> 'https://api.m4leads.com/order/add',
			'urlOrderInfo'				=> 'https://api.m4leads.com/order/get-order-status',
		],
	],
];

?>