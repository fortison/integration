<?php
/**
 * https://www.amocrm.ru/developers
 */

return [
	
	3711 => [
		# https://pastebin.com/fivacVFF
		'apileadApiKey'				=> '774c8340e71442d33942386818eb550e',
		
		'userHash'             		=> '315c8d22c46b5fb293ea6c7c82f72fe5c92a17d2',
		'userLogin'             	=> 'crm_master@inbox.ru',
		'subDomain'             	=> 'bigmarket',
		
		'fieldPhoneContact'			=> 43750,
		'fieldFirstStatus'			=> 14775619,
		
		'offers' => [
			2934 => [
				'url' => 'http://razorless-m.big-sale.biz/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2737 => [
				'url' => 'http://starshower-m2.big-sale.biz/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3376 => [
				'url' => 'http://kidsart-m.big-sale.biz/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2236 => [
				'url' => 'http://ems-mob.big-sale.biz/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2276 => [
				'url' => 'http://airpods.all-tech.biz',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3965 => [
				'url' => 'http://perfume2.all-shops.biz/',
				'customFields' => [
					[
						'id' => 252175, #поле парфюм в інтерфейсі амо
						'values' => [
							[
								'value' => function ($model, $confid) {
									return $model->address ?? '';
								},
							],
						],
					],
				],
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],

		],
		'configs' => [
			'brakeLogFolder'	=> true,
			'statuses' => [
				'confirm'	=> [
					'14775622' => '',
					'142' => '',
					'14973925' => '',
					
				],
				'reject' => [
					'25356673' => '',
					
				],
				'expect' => [
					'37373896' => 'парфюм',
					'14775619' => '',
					'15388501' => '',
					'14960179' => '',
					'14992156' => '',
				],
				'trash' => [
					'143 '	   => '',
					'14778405' => '',
				],
			],
		],
	],
	
	3751 => [
		# https://pastebin.com/gwyMmzWx
		'apileadApiKey'				=> '29156e2189a5b2e53e394ff6dedc1335',
		
		'userHash'             		=> '3c1154f14a52aff152a3e5fa7b909d60',
		'userLogin'             	=> 'beauty.huda@yandex.ru',
		'subDomain'             	=> 'new59142c5939385',
		
		'fieldPhoneContact'			=> 92870,
		'fieldFirstStatus'			=> 14882935,
		
		'offers' => [
			1992 => [
				'url' => 'http://jo-malone.magic-present.net/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
			'statuses' => [
				'confirm'	=> [
					'14882929' => '',
					'142' => '',
				],
				'reject' => [
					'14882932' => '',
					'143' => '',
				],
				'expect' => [
					'14882935' => '',
				],
				'trash' => [
					'14882938' => '',
				],
			],
		],
	],
	
	3766 => [
		# https://pastebin.com/A4dhFsLg
		'apileadApiKey'				=> '1cf283791ee6ca08f32ad02a51cc0ae4',
		
		'userHash'             		=> 'b6f76e3051cc220edbc6912fde626b77',
		'userLogin'             	=> 'bigroupmd@yandex.ru',
		'subDomain'             	=> 'new591c1c31a5125',
		
		'fieldPhoneContact'			=> 126379,
		'fieldFirstStatus'			=> 14957566,
		
		'offers' => [
			1982 => [
				'url' => 'http://flybra.all-shops.biz/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
			'statuses' => [
				'confirm'	=> [
					'14957569' => '',
					'142' => '',
				],
				'reject' => [
					'14957575' => '',
					'143' => '',
				],
				'expect' => [
					'14957566' => '',
					'14984671' => '',
					'14957572' => '',
				],
				'trash' => [
					'14984674' => '',
				],
			],
		],
	],
	
	4358 => [
		# https://pastebin.com/sa9Gqrcw
		'apileadApiKey'				=> '7da010bed949b8b70d485d95f1e4285a',
		
		'userHash'             		=> '67c841d91c57a71c3d51522d7c58fcc8',
		'userLogin'             	=> 'kz-shop@list.ru',
		'subDomain'             	=> 'almatyg',
		
		'fieldPhoneContact'			=> 408183,
		'fieldFirstStatus'			=> 17622814,
		
		'offers' => [
			2331 => [
				'url' => 'http://magictrack.magic-present.net/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2418 => [
				'url' => 'http://jbl-pulse.all-tech.biz/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2421 => [
				'url' => 'http://baby-watch-q100.all-tech.biz/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2393 => [
				'url' => 'http://gt08m.smaartwatch.ru/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],							
		],
		'configs' => [
			'brakeLogFolder'	=> true,
			'statuses' => [
				'confirm'	=> [
					'17622817' => '',
					'17622823' => '',
					'142' => '',
					'143' => '',
				],
				'reject' => [
					'17622820' => '',
				],
				'expect' => [
					'17622814' => '',
					'17646304' => '',
				],
				'trash' => [
					'14882938' => '',
				],
			],
		],
	],
	
	4488 => [
		# https://pastebin.com/EjbbGCma
		'apileadApiKey'				=> '8ec3b23bc117585b694a3238d7b0b5a7',
		
		'userHash'             		=> 'fa183d3fdb62d352b3600fad55b4953f4725a6b2',
		'userLogin'             	=> 'tesakulmurza@gmail.com',
		'subDomain'             	=> 'tesakulmurza',
		
		'fieldPhoneContact'			=> 94273,
		'fieldFirstStatus'			=> 19578067,
		
		'offers' => [
			2393 => [
				'url' => 'http://gt08m.smaartwatch.ru/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2561 => [
				'url' => 'http://dz09.all-tech.biz/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2387 => [
				'url' => 'http://sw-watches.best-watches.net/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],			
		],
		'configs' => [
			'brakeLogFolder'	=> true,
			'statuses' => [
				'confirm'	=> [
					'19578070' => 'Подтвержден',
					'19578076' => 'Доставка',
					'19578232' => 'Получил(а)',
					'142' => 'Успешно реализовано',
					'143' => 'Закрыто и не реализовано',
//					'18571867' => 'Подтв. отказ',
				],
				'reject' => [
					'19578229' => 'Отказ',
				],
				'expect' => [
					'19578067' => 'В Оброботке',
					'19707220' => 'Перезвон',
					'19578073' => 'ожидание потвержден',
				],
				'trash' => [
//					'' => 'Асхат call center',
				],
			],
		],
	],
	
	5610 => [
		# https://pastebin.com/sa9Gqrcw
		'apileadApiKey'				=> 'd72af13c6ec1c56028e426a772ce26c2',
		
		'userHash'             		=> '63b24014f85a0231bcd24539aeaae7e44bb2af89',
		'userLogin'             	=> 'adv.apilead@gmail.com',
		'subDomain'             	=> 'new587f79254ee78',
		
		'fieldPhoneContact'			=> 1828140,
		'fieldFirstStatus'			=> 22581673,
		
		'offers' => [
			2705 => [
				'url' => 'http://bears.big-sale.biz/',
				'pipelineId' => 1453636,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],		
		'configs' => [
			'brakeLogFolder'	=> true,
			'statuses' => [
				'confirm'	=> [
					'142' => '',
					'22581676' => '',
				],
				'reject' => [
					'22581679' => '',
				],
				'expect' => [
					'22581673' => '',
				],
				'trash' => [
					'143' => '',
					'22582423' => '',
				],
			],
		],
	],
	
	
	5725 => [

		'apileadApiKey'				=> '683c772c8ddf98a9a47fda724cf97a81',
		
		'userHash'             		=> '7d0f386af6ee448294c05ce6e758c19fe620a7e4',
		'userLogin'             	=> 'adv.apilead@gmail.com',
		'subDomain'             	=> 'avcompany',
		
		'fieldPhoneContact'			=> 276365,
		'fieldFirstStatus'			=> 22619296,
		
		'offers' => [
			2580 => [
				'url' => 'http://money2.magic-present.net/',
				'pipelineId' => 1484737,
				'tags' => [363787],
				'customFields' => [
					[
						'id' => 478235, # додане користувацьку поле web_id в інтерфейсі амо
						'values' => [
							[
								'value' => function ($model, $confid) {
									return $model->uHash ?: $model->wHash;
								},
							],
						],
					],
					[
						'id' => 482909, # додав користувацьке поле address в інтерфейсі амо
						'values' => [
							[
								'value' => function ($model, $confid) {
									return $model->address;
								},
							],
						],
					],					
				],
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3011 => [
				'url' => 'http://money2.magic-present.net/',
				'pipelineId' => 1484737,
				'tags' => [363787],
				'customFields' => [
					[
						'id' => 478235, # додане користувацьку поле web_id в інтерфейсі амо
						'values' => [
							[
								'value' => function ($model, $confid) {
									return $model->uHash ?: $model->wHash;
								},
							],
						],
					],
					[
						'id' => 482909, # додав користувацьке поле address в інтерфейсі амо
						'values' => [
							[
								'value' => function ($model, $confid) {
									return $model->address;
								},
							],
						],
					],
				],
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3244 => [
				'url' => 'http://4dxpress-m.big-sale.biz/',
				'pipelineId' => 1484737,
				'tags' => [400613],
				'customFields' => [
					[
						'id' => 478235, # додане користувацьку поле web_id в інтерфейсі амо
						'values' => [
							[
								'value' => function ($model, $confid) {
									return $model->uHash ?: $model->wHash;
								},
							],
						],
					],
					[
						'id' => 482909, # додав користувацьке поле address в інтерфейсі амо
						'values' => [
							[
								'value' => function ($model, $confid) {
									return $model->address;
								},
							],
						],
					],
				],
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
			'statuses' => [
				'confirm'	=> [
					'22619299' => '',
					'22625035' => '',
					'22625038' => '',
					'22625041' => '',
					'24434464' => '',
					'142' => '',
				],
				'reject' => [
					'143' => '',
				],
				'expect' => [
					'22619296' => '',
					'23167792' => '',
					'23305633' => '',
					'23305639' => '',
					'23305645' => '',
					'23305636' => '',
					'25061725' => '',
				],
				'trash' => [
					'23261782' => '',
				],
			],
		],
	],
	
	9962 => [
		#https://pastebin.com/ZqcHQLHs
		'apileadApiKey'				=> '257d6e5b46fd0b06d671110670151b6a',
		
		'userHash'             		=> 'ecc1d8d2b4ad5c862cf33471ca0ff08c61ae96e2',
		'userLogin'             	=> 'dnevnikkuchera@gmail.com',
		'subDomain'             	=> 'galantsales',
		
		'fieldPhoneContact'			=> 323427,
		'fieldFirstStatus'			=> 30690277,
		
		'offers' => [
			3488 => [
				'url' => 'http://trokot.auto-toolz.biz/',
				'pipelineId' => 2171323,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
			'statuses' => [
				'confirm'	=> [
					'30756529' => '',
					'142'      => '',
					'30690298' => '',
					'30897634' => '',
				],
				'reject' => [
					'30690295' => '',
				],
				'expect' => [
					'30690277' => '',
					'30887098' => '',
					'30690283' => '',
					'31540186' => '',
				],
				'trash' => [
					'143' => '',
				],
			],
		],
	],
	
	/**
	 * topsaler2020@gmail.com
	 * Y2bo6BFM
	 */
	10488 => [
		#https://pastebin.com/CFcV8QzF
		'apileadApiKey'				=> 'd08d20a9dc120d0b64ef815db41d6d3e',
		
		'userHash'             		=> 'dd74ea89ecafd705a6b9308190055da58c20b2ee',
		'userLogin'             	=> 'topsaler2020@gmail.com',
		'subDomain'             	=> 'zerovit',
		
		'fieldPhoneContact'			=> 210707,
		'fieldFirstStatus'			=> 33350590,
		
		'offers' => [
			3792 => [
				'url' => 'http://0000000000/',
				'pipelineId' => 0000,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
			'statuses' => [
				'confirm'	=> [
					'34724470' => 'подтвержденная',
					'33406711' => 'В пути',
					'33406810' => 'В ожидании получения',
					'33406813' => 'Доставлено/Оплачено',
					'33406882' => 'Фактически получено',
					'142'	   => 'Успешно реализовано',
				],
				'reject' => [
					'33350596' => 'ОТКЛОНЕННАЯ',
					'33610528' => 'ОТКАЗ',
				],
				'expect' => [
					'33350587' => 'Неразобранное',
					'33350590' => 'ОЖИДАНИЕ',
					'33350593' => 'В РАБОТЕ',
					'34657228' => 'Недозвон',
				],
				'trash' => [
					'143' => 'Закрыто и не реализовано',
					'33350599' => 'треш',
				],
			],
		],
	],
	
	

	'configs' => [
//		'urlApileadOrderUpdate'		=> 'http://al-api.com/api/lead/update',
//		'bridgeEnabled'				=> 1,																				// Чи активна ця інтеграція
//		'statusEnabled'				=> null,																			// Чи опрацьовувати статуси цієї інтеграції
		'urlOrderAdd'				=> true,
		'urlOrderInfo'				=> true,
		'bridgeRequestTimeout'		=> 60,
//		'brakeLogFolder'			=> '',
//		'statusHandler' 			=> 'app\modules\apilead\handlers\adv\leadvertex\LeadvertexStatusHandler',
//		'bridgeHandler' 			=> 'app\modules\apilead\handlers\adv\leadvertex\LeadvertexBridgeHandler',
	]
];

?>