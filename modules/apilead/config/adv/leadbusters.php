<?php

/**
 * https://leadbusters.club/apidoc/index.html#api-Lead-lead_create
 * https://www.notion.so/API-LeadBusters-61496853918d42d2b8abdfee2d514232
 *
 * почта advert@apilead.com
 * пароль 15963254
 * http://connectcpa.net/apilead/adv/adv-postback-status?systemUserId=11374&partnerStatus={status}&systemOrderId={subid1}&postbackApiKey=dc8fdf31fe9bcd18e56131cd705fffa1
 */

return [
	11374 => [
		'apileadApiKey'  => 'dc8fdf31fe9bcd18e56131cd705fffa9',
		'postbackApiKey' => 'dc8fdf31fe9bcd18e56131cd705fffa1',
		
		'offers' => [
			3986 => [
				'x_url' => 'https://leadbusters.network/t/b65e7', //Tracking URL
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3992 => [
				'x_url' => 'https://leadbusters.network/t/0a6a9', //Tracking URL
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4101 => [
				'x_url' => 'https://leadbusters.club/t/e274c', //Tracking URL
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4105 => [
				'x_url' => 'https://leadbusters.club/t/a27c3', //Tracking URL
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4281 => [
				'x_url' => 'https://leadbusters.club/t/3e0f2', //Tracking URL
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4282 => [
				'x_url' => 'https://leadbusters.club/t/14ca5', //Tracking URL
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4283 => [
				'x_url' => 'https://leadbusters.club/t/6617b', //Tracking URL
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4284 => [
				'x_url' => 'https://leadbusters.club/t/af1bf', //Tracking URL
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4285 => [
				'x_url' => 'https://leadbusters.club/t/25741', //Tracking URL
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4286 => [
				'x_url' => 'https://leadbusters.club/t/a9599', //Tracking URL
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4287 => [
				'x_url' => 'https://leadbusters.club/t/a5bda', //Tracking URL
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4288 => [
				'x_url' => 'https://leadbusters.club/t/efc9e', //Tracking URL
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4289 => [
				'x_url' => 'https://leadbusters.club/t/4a522', //Tracking URL
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'trash' => 'фейк',
			],
			'reject' => [
				'reject' => 'отклонен',
			],
			'expect' => [
				'expect' => 'в обработке',
			],
			'confirm' => [
				'confirm' => 'принят',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://leadbusters.network/tracking/external/lead',
		'urlOrderInfo'		=> '',
	],
];

?>