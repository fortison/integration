<?php

/*
 * Документація за посиланням https://docs.google.com/document/d/1QBr0ptT1nauhj1eQpuCHLnUhltefxYqDwdfBVHStvik/edit
 * доп. дока https://telesales.docs.apiary.io/
 *
 * статусы https://apiucrm.ru/orders/statuses?api_token=393752ee6d68255d88aaa932d4b38e47&pretty=1
 */

return [
	
	10486 => [
		'apileadApiKey'				=> 'df3159f395d60a82037b77ac2fe22692',
		'apiToken'					=> '393752ee6d68255d88aaa932d4b38e47',
		
		'offers' => [
			3790 => [
				'order_offer_id' => '4554',
				'order_webmaster_id' => '12561',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3797 => [
				'order_offer_id' => '4772',
				'order_webmaster_id' => '12561',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3824 => [
				'order_offer_id' => '4928',
				'order_webmaster_id' => '12561',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3844 => [
				'order_offer_id' => '4949',
				'order_webmaster_id' => '12561',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3848 => [
				'order_offer_id' => '4685',
				'order_webmaster_id' => '12561',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3849 => [
				'order_offer_id' => '5005',
				'order_webmaster_id' => '12561',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3850 => [
				'order_offer_id' => '5010',
				'order_webmaster_id' => '12561',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3851 => [
				'order_offer_id' => '5046',
				'order_webmaster_id' => '12561',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3852 => [
				'order_offer_id' => '5048',
				'order_webmaster_id' => '12561',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'https://apiucrm.ru/order/store',
			'urlOrderInfo'		=> 'https://apiucrm.ru/orders/view',
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				15	=> 'Это ФРОД',
				17	=> 'Отклонен роботом',
				43	=> 'Это ФРОД',
				42	=> 'Долг',
				41	=> 'Доработки',
				32	=> 'Ожидание предоплаты',
				31	=> 'Это ФРОД',
				30	=> 'Недобросовестный клиент',
			],
			'reject'	=> [
				4	=> 'Отклонен',
			],
			'expect'	=> [
				1	=> 'Новый',
				5	=> 'В обработку',
				6	=> 'На проверку',
			],
			'confirm'	=> [
				3	=> 'Подтвержден',
			],
		],
		'brakeLogFolder'			=> true,
	],
];
?>