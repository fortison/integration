<?php

/**
 * https://docs.google.com/document/d/1OWKgGsgeHpya9B5b-ChKWK1YI2sVK4tSmYsv5m96gUk/edit
 *
 * http://connectcpa.net/apilead/adv/adv-postback-status?systemUserId=11873&systemOrderId={externalLeadId}&postbackApiKey=cf2f95cfbdca19826bf3c59a75025d62&partnerStatus=confirm
*/


return [
	11873 => [
		'apileadApiKey'  => 'cf2f95cfbdca19826bf3c59a75025d61',
		'postbackApiKey' => 'cf2f95cfbdca19826bf3c59a75025d62',
		
		'api_key' => '32TkS4uowwe9xF5SWwGUXYAxCaGlMby1',
		'externalPartnerId' => '26',
		
		'offers' => [
			4239 => [
				'product' => 'PRO81',
				'price'  => '47', //необязательное поле
				'source'  => '', //необязательное поле
				'country' => 'IT', //необязательное поле
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4240 => [
				'product' => 'PRO80',
				'price'  => '47', //необязательное поле
				'source'  => '', //необязательное поле
				'country' => 'IT', //необязательное поле
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'trash' => 'фейк',
			],
			'reject' => [
				'reject' => 'отклонен',
			],
			'expect' => [
				'expect' => 'в обработке',
			],
			'confirm' => [
				'confirm' => 'принят',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://leadsystemapp.com/api/lead/add-lead',
		'urlOrderInfo'		=> '',
	],
];

?>