<?php

/*
 * В файлі  http://emp.cab/cabinet/offers/view/HBWC8xZB8iMtMgNxn
 */

return [
	
	3886 => [
		'apileadApiKey'				=> 'a98fdff1459e4e2ade4e8e3e2a9b31db',
		
		'offers' => [
			2489 => [
				'offer' => 'HBWC8xZB8iMtMgNxn',
				'stream' => '5afd647a3dc24d1632cb8637',
				'reseller' => '595df3513dc24d651c730c5d',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'http://c.sila.guru/api/lead/',
			'urlOrderInfo'		=> 'http://c.sila.guru/api/status/',
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				3		=> '',
			],
			'reject'	=> [
				2	=> '',
			],
			'expect'	=> [
				0		=> '',
			],
			'confirm'	=> [
				1	=> '',
			],
		],
		'brakeLogFolder'			=> true,
	],
];
?>