<?php

/*
 * https://broleads.com/#/tools
 *
 * You can configure the Postback format with the following options:
date - date in format 2017-01-01 10:00:00
landing - landing code
offer - Offer ID
my_offer - My Offer ID
pay_sum - Webmaster reward
clickid - Click ID
sub1 - subaccount 1
sub2 - subaccount 2
sub3 - subaccount 3
sub4 - subaccount 4
status - event type: created, confirmed, canceled, trashed, valid
For example, a postback format is specified:
http://postbackurl.com/subscribe/notify?sub1={sub1}&sub2={sub2}&sub3={sub3}&sub4={sub4}&date={date}&my_offer={my_offer}&landing={landing}&offer={offer}&status={status}&clickid={clickid}
Result is postback:
http://postbackurl.com/subscribe/notify?sub1=123&sub2=123&sub3=123&sub4=123&date=2017-01-01+10:00:00&my_offer=34534&landing=1&offer=1&status=created&clickid=123
* If you fill the URL in the "Postback" field and do not specify a format with the parameters, the parameters will be
sent by default, as in the example.
 */

return [
	5793 => [
		'apileadApiKey'				=> '8a9985167acb016101d9c6ffcee8e23b',
		'postbackApiKey'			=> '8b197819803508eda002e827687a9739',

		'offers' => [
			2613 => [
				'stream'       	=> '695eTvqftL',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2885 => [
				'stream'       	=> 'phedWhBD3V',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3120 => [
				'stream'       	=> 'UfcgzR8m8s',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3119 => [
				'stream'       	=> 'W7BdwSG8dc',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3122 => [
				'stream'       	=> 'FriFrG9T9D',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3123 => [
				'stream'       	=> 'EjoFCRXgtD',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3124 => [
				'stream'       	=> 'ArBEHU8pDy',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3125 => [
				'stream'       	=> 'vSRxVHGcYj',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3129 => [
				'stream'       	=> 'vfg4nisHu2',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3130 => [
				'stream'       	=> '2fHhCLBu3T',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3131 => [
				'stream'       	=> 'SytSgCTiLt',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3132 => [
				'stream'       	=> 'JXAZ8x2tKi',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3184 => [
				'stream'       	=> 'dCYdQG8ju4',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3185 => [
				'stream'       	=> 'NfejosmDgU',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3188 => [
				'stream'       	=> 'Mn2Tm4YkPF',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3187 => [
				'stream'       	=> 'y24G8VTyjJ',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2720 => [
				'stream'       	=> 'N64aADQpwj',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2721 => [
				'stream'       	=> 'LanmxpfJLH',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3290 => [
				'stream'       	=> 'xByjAvZfaY',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3289 => [
				'stream'       	=> 'YF5YRmfyAi',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3300 => [
				'stream'       	=> 'hGqY3RZbUf',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3301 => [
				'stream'       	=> 'tk4E4RBUSP',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3307 => [
				'stream'       	=> 'Ze4CjPR8p5',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3308 => [
				'stream'       	=> 'Ybj98hyEoV',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3314 => [
				'stream'       	=> 'jJYr3C2V4p',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3315 => [
				'stream'       	=> 'vp5JLPgAeH',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3320 => [
				'stream'       	=> 'aTydFAf9ii',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3321 => [
				'stream'       	=> 'mBsKmjRwfg',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3329 => [
				'stream'       	=> 'SWhkX8BjTT',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3328 => [
				'stream'       	=> 'HChjyvj9fC',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3343 => [
				'stream'       	=> '8QQPNkY5JU',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3342 => [
				'stream'       	=> 'MScQ3W3EjE',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3345 => [
				'stream'       	=> 'Jcto8oLMyu',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3344 => [
				'stream'       	=> 'Bk9oCvGqS9',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3347 => [
				'stream'       	=> 'iQWibtjVtJ',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3346 => [
				'stream'       	=> 'CvTkq6qRTo',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3349 => [
				'stream'       	=> 'i3FZYqjxzd',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3348 => [
				'stream'       	=> 'JinSWV7oQM',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3367 => [
				'stream'       	=> 'YzRdBFUF6f',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3366 => [
				'stream'       	=> 'tkVFhT5yxN',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3365 => [
				'stream'       	=> 'HxVF92mHHc',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3364 => [
				'stream'       	=> '33gxfRpvzz',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3363 => [
				'stream'       	=> 'TQ3VUmGnz4',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3362 => [
				'stream'       	=> 'kC9HtDUCjg',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3428 => [
				'stream'       	=> 'muDVKC7oAu',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3429 => [
				'stream'       	=> 'rmBDHqcBmn',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3430 => [
				'stream'       	=> 'np6xSkoUTo',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3431 => [
				'stream'       	=> 'uAtepE2Us6',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3432 => [
				'stream'       	=> 'DhhAnd237E',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3433 => [
				'stream'       	=> 'ULMEfQmsph',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3575 => [
				'stream'       	=> 'PKCGNkWRBg',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3576 => [
				'stream'       	=> 'ztw6pZXy2f',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3577 => [
				'stream'       	=> 'GFMu3StUwn',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3578 => [
				'stream'       	=> 'tqNLRf4hjE',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3579 => [
				'stream'       	=> 'XcmAx5tc3N',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3580 => [
				'stream'       	=> 'qvB6FzA9RR',
				'landing'       => 'cccccccc.cc',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],

	'configs' => [
        'statuses' => [
            'trash'	=> [
                'trashed'	        => 'Спам/Некорректная заявка',
            ],
            'reject'	=> [
                'canceled'	        => 'Отказ',
            ],
            'expect'	=> [
                'created'			=> 'В обработке',
            ],
            'confirm'	=> [
                'valid'	       		=> 'Подтвержден',
				'confirmed'			=> 'Подтвержден',
            ],
        ],

		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://broleads.com/api/v1/order',
//		'statusEnabled'		=> false,
	],
];