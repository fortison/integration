<?php

return [
	
	//https://x-men.pro/
	12372 => [
		'apileadApiKey'		=> '62de189b03027dcffedd55dc98a6eb9f',
		'user'			    => '60',
		'key' 				=> '173072bb54c99e60861a598133184b25',
		
		
		'offers' => [
			4256 => [
				'offer' => '12', //Идентификатор оффера из списка (обязательный параметр) https://x-men.pro/help/api.php#offers
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4257 => [
				'offer' => '4', //Идентификатор оффера из списка (обязательный параметр) https://x-men.pro/help/api.php#offers
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4329 => [
				'offer' => '5', //Идентификатор оффера из списка (обязательный параметр) https://x-men.pro/help/api.php#offers
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'urlOrderAdd'		=> 'https://x-men.pro/api/ext/add.json',
			'urlOrderInfo'		=> 'https://x-men.pro/api/ext/list.json',
		],
	],
	
	//Ссылка для входа:
	//https://my.trirazat.com/
	10668 => [
		'apileadApiKey'		=> 'bbce47670d9f6633537d55b8663fb198',
		'user'			    => '9',
		'key' 				=> '66c560881f71667b0d43567d647cd9f0',
		
		
		'offers' => [
			4184 => [
				'offer' => '1', //Идентификатор оффера из списка (обязательный параметр) https://my.trirazat.com/api/wm/offers.json?id=9-66c560881f71667b0d43567d647cd9f0
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4183 => [
				'offer' => '1', //Идентификатор оффера из списка (обязательный параметр) https://my.trirazat.com/api/wm/offers.json?id=9-66c560881f71667b0d43567d647cd9f0
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3911 => [
				'offer' => '1', //Идентификатор оффера из списка (обязательный параметр) https://my.trirazat.com/api/wm/offers.json?id=9-66c560881f71667b0d43567d647cd9f0
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3910 => [
				'offer' => '1', //Идентификатор оффера из списка (обязательный параметр) https://my.trirazat.com/api/wm/offers.json?id=9-66c560881f71667b0d43567d647cd9f0
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3909 => [
				'offer' => '1', //Идентификатор оффера из списка (обязательный параметр) https://my.trirazat.com/api/wm/offers.json?id=9-66c560881f71667b0d43567d647cd9f0
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'urlOrderAdd'		=> 'https://my.trirazat.com/api/ext/add.json',
			'urlOrderInfo'		=> 'https://my.trirazat.com/api/ext/list.json',
		],
	],
	
	//healthpower.pro
	10397 => [
		'apileadApiKey'		=> '31b7ca78147b63cefabcb7c28d70d226',
		'user'			    => '63',
		'key' 				=> 'eb832bb3897f547d442c27a9c0cd016c',
		
		
		'offers' => [
			0000 => [
				'offer' => '1', //Идентификатор оффера из списка (обязательный параметр) https://healthpower.pro/help/api.php#offers
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'urlOrderAdd'		=> 'https://healthpower.pro/api/ext/add.json',
			'urlOrderInfo'		=> 'https://healthpower.pro/api/ext/list.json',
		],
	],
	
	/*
	 * http://tajik.host/
	 * Adv.apilead@gmail.com
	 * 1234
	 */
	10397 => [
		'apileadApiKey'		=> '31b7ca78147b63cefabcb7c28d70d226',
		'user'			    => '54',
		'key' 				=> '30f20013b0df38152286745f175fe941',
		
		
		'offers' => [
			3755 => [
				'offer' => '14',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3756 => [
				'offer' => '25',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3757 => [
				'offer' => '4',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3758 => [
				'offer' => '21',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3759 => [
				'offer' => '8',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3913 => [
				'offer' => '9',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'urlOrderAdd'		=> 'https://tajik.host/api/ext/add.json',
			'urlOrderInfo'		=> 'https://tajik.host/api/ext/list.json',
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash' => '',
			],
			'reject'	=> [
				'cancel'	=> '',
			],
			'expect'	=> [
				'wait'	=> '',
				'hold'	=> '',
			],
			'confirm'	=> [
				'approve'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
	],
];

?>