<?php

/*
 * https://dev.1c-bitrix.ru/api_help/sale/classes/csaleorder/csaleorder__getbyid.5cbe0078.php
 * Пріложtніє->вебхукі->создать_входящій
 * Права +CRM
 * із посилання беремо apiKey, subDomain та id
 *
 * https://b24-ulgrkf.bitrix24.ru/rest/1/p1l6nupzx8wgiild/methods			Доступні методи
 * https://b24-ulgrkf.bitrix24.ru/rest/1/p1l6nupzx8wgiild/crm.status.list	Перегляд статусів, нас цікавлять тільки "ENTITY_ID": "DEAL_STAGE", а в конфіг вносимо STATUS_ID
 *
 * Для зручного перегляду результатів використовуємо - https://jsonformatter.org/json-pretty-print
 *
 */

return [
	
	4707 => [
		'apileadApiKey'				=> '8783f479b5aeac8c2456012b3da585b2',
		'apiKey'					=> 'f7gostqsw2sicwmz',
		'id'						=> 1,
		'subDomain'					=> 'b24-do6qp6',
		
		'offers' => [
			2322 => [
				'offerName'		=> 'EMS Trainer 1990 рублей',
//				'companyId'		=> 123,
//				'offerId'		=> 0000,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash'	=> [
					'LOSE'						=> 'Сделка провалена',
				],
				'reject'	=> [
					'APOLOGY'					=> 'Отмена',
				],
				'expect'	=> [
					'NEW'						=> 'Новая',
					'EXECUTING'					=> 'В работе',
				],
				'confirm'	=> [
					'WON'						=> 'Сделка успешна',
				],
			],
			'brakeLogFolder'	=> true,
		],
	],
	4723 => [
		'apileadApiKey'				=> '28d5369bf35139519eb847730bb4b1c1',
		'apiKey'					=> 'p1l6nupzx8wgiild',
		'id'						=> 1,
		'subDomain'					=> 'b24-ulgrkf',
		
		'offers' => [
			2502 => [
				'offerName'		=> 'Колонка JBL Boombox mini',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2011 => [
				'offerName'		=> 'Смарт часы V8',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2131 => [
				'offerName'		=> 'Смарт часы SW 007',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2505 => [
				'offerName'		=> 'Колонка JBL Boombox mini',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash'	=> [
					'LOSE'						=> 'Сделка провалена',
					3							=> 'спам/дубль',
				],
				'reject'	=> [
					'APOLOGY'					=> 'Отмена',
				],
				'expect'	=> [
					'NEW'						=> 'Новая',
					'EXECUTING'					=> 'В работе',
				],
				'confirm'	=> [
					'WON'						=> 'Сделка успешна',
					4							=> 'Отправлен',
					1							=> 'Вручен',
				],
			],
			'brakeLogFolder'	=> true,
		],
	],


	8675 => [
		'apileadApiKey'				=> '26127e42e8ba5fa7047418ff8b3be5ae',
		'apiKey'					=> 'r1ix0fblih4p5gml',
		'id'						=> 1,
		'subDomain'					=> 'b24-zh4gyi',
		
		'offers' => [
			3259 => [
				'offerName'		=> 'Nasal Booster (Украина)',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash'	=> [
					'LOSE'						=> 'Сделка провалена',
					3							=> 'спам/дубль',
				],
				'reject'	=> [
					'APOLOGY'					=> 'Отмена',
				],
				'expect'	=> [
					'NEW'						=> 'Новая',
					'EXECUTING'					=> 'В работе',
				],
				'confirm'	=> [
					'WON'						=> 'Сделка успешна',
					4							=> 'Отправлен',
					1							=> 'Вручен',
				],
			],
			'brakeLogFolder'	=> true,
		],
	],
	
	'configs' => [

		'brakeLogFolder'			=> true,
//		'statusEnabled'				=> false,
	],
];
?>