<?php

/**
 * apidoc.leadreaktor.com
 */


return [
	
	2323 => [
		'apileadApiKey' => '00000000000000000000000000000000',
		'api_key' 	    => '16ac2a5f4f3c486da526b9892a560e8a',
		
		'offers' => [
			2323 => [
				'goods_id' => '116237',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash' => [
					'trash' => '',
				],
				'reject' => [
					'canceled' => '',
				],
				'expect' => [
					'waiting' => '',
				],
				'confirm' => [
					'approved' => '',
				],
			],
			'brakeLogFolder' => true,
			'urlOrderAdd' 	 => 'https://api-new.leadreaktor.com/api/order/create.php',
			'urlOrderInfo' 	 => 'https://stats.leadreaktor.com/api/get-status',
		]
	]
];

?>