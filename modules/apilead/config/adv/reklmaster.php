<?php

/*
 * https://reklmaster.ru/help/api.php#ext
 */

return [
	
	4518 => [
		'apileadApiKey'				=> 'ccc52453f9de570d29ab9710ad40d7fb',
		'apiKey'					=> '0401aea3c54bb9751a060f97d7905f5a',
		'user' 						=> 53,
		
		'offers' => [
			2882 => [
				'offerId' => 153,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2878 => [
				'offerId' => 153,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2932 => [
				'offerId' => 171,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		'configs' => [
			'statuses' => [
				'reject'	=> [
					5		=> 'Cancelled',
				],
				'expect'	=> [
					1		=> 'New order',
					2		=> 'Processing',
					3		=> 'Callback',
					4		=> 'Hold',
					12		=> 'Deleted',
				],
				'trash'	=> [
				],
				'confirm'	=> [
					6		=> 'Packing',
					7		=> 'Sending',
					8		=> 'Transfer',
					9		=> 'Arrived',
					10		=> 'Completed',
					11		=> 'Return',
				],
			],
			'subStatuses' => [
				'expect'	=> [
				],
				'confirm'	=> [
				],
				'reject'	=> [
					2		=> 'Changed his mind',
					4		=> 'Requires certificate',
					9		=> 'Expensive',
					10		=> 'Not satisfied with delivery',
				],
				'trash'	=> [
					1		=> 'Incorrect phone',
					3		=> 'Did not order',
					5		=> 'Wrong GEO',
					6		=> 'Errors of fake',
					7		=> 'Duplicate order',
					8		=> 'Ordered elsewhere',
					11		=> 'Could not get through',
					12		=> 'Possibly fraud',
				],
			],

			'bridgeRequestTimeout'		=> 60,
			'statusRequestTimeout'		=> 60,
			'urlOrderAdd'				=> 'https://reklmaster.ru/api/ext/add.json',
			'urlOrderInfo'				=> 'https://reklmaster.ru/api/ext/list.json',
		],
	],
];

?>