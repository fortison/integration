<?php
return [
	504 => [
		'apileadApiKey'		=> '8acd22723cbf0c5c98678f6bc1e4d133',
		'apiKey'			=> 'b15eaa202bad6ac57f1feaa0043d4c29',
		
		'configs' => [
			'postbackUrl'		=> 'http://api.monsterleads.pro/method/lead.edit',
			'brakeLogFolder'	=> true,
		],
	],
	
	'configs' => [
		'statuses' => [
			'accept'	=> 1,
			'expect'	=> 1,
			'fail'		=> 1,
			'confirm'	=> 2,
			'reject'	=> 3,
			'trash'		=> 10,
		],
	],
];