<?php

/*
API Url:    http://mancare.in/teraleads/lead/add
METHOD: POST
Content-type: application/json
API KEY:  YSV728HH26DGGS6D722626DGG6
Note:  1 -  affiliate_id must be 3.
Request Format
{
"api_key":"YSV728HH26DGGS6D722626DGG6",
"first_name": "customer name",
"phone": "10 digit phone no",
"email_id": "email id",
"affiliate_id": "3",
"sub1": "transaction id",
"sub2": "publisher id",
"sub3": "sub3 string"
}
Response Format
{
  "status": 200,
  "data": {
    "id": order id,
    "status": 1,
    "affiliate_id": 2
  },
  "status_message": "Successfully Lead generated"
}
 */

return [
	26970 => [
		'terraleadsApiKey'			=> '65706adc97b3034ad05301dfa5a1ad29',
		'postbackApiKey'			=> '65706adc97b3034ad05301dfa5a1ad28',
		'api_token' 				=> 'YSV728HH26DGGS6D722626DGG6',
		
		'offers' => [
			3625 => [
				'affiliate_id'	=> 3,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'		=> '',
			],
			'reject'	=> [
				'reject'	=> '',
			],
			'expect'	=> [
				'expect'	=> '',
			],
			'confirm'	=> [
				'confirm'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'http://mancare.in/teraleads/lead/add',
		'urlOrderInfo'		=> '',
	],
];

?>