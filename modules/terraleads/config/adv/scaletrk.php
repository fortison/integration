<?php

/*
 * https://developers.scaleo.io/#005f06ca-4db8-4879-a3e4-eadba6680597
*/

return [
	
	62349 => [
		'terraleadsApiKey'	=> 'cf7c53578ab9eb0be8f28216b5622fde',
		'postbackApiKey'	=> 'cf7c53578ab9eb0be8f28216b5622sme',
		'api_key'			=> '4aa2b41abc873995e237aa82736824168b537643',
		
		'offers' => [
			8910 => [//Prostalove - ID
				'goal_id' => '160',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8573 => [//Coco Collagen - VN
				'goal_id' => '124',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8572 => [//Diet Smart - VN
				'goal_id' => '122',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8571 => [//Maxi White - VN
				'goal_id' => '123',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8570 => [//FirstUp - VN
				'goal_id' => '120',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2323 => [
				'goal_id' => '79',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'	=> '',
			],
			'reject'	=> [
				'reject'	=> '',
			],
			'expect'	=> [
				'expect'	=> '',
			],
			'confirm'	=> [
				'confirm'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://auron.scaletrk.com/api/v2/affiliate/leads',
		'urlOrderInfo'		=> 'https://auron.scaletrk.com/api/v2/affiliate/leads',
	],
];

?>