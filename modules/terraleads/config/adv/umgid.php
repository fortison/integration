<?php

/*
У файлі

https://umgid.com/streams_api/guide_view

login: partners@terraleads.com
password 8hIjygbJcz5sQOHrxYK4j725T6XfgHke

http://connectcpa.net/terraleads/adv/adv-postback-status?systemUserId=23055&partnerStatus=trash&systemOrderId={ subid1 }&comment=Статус ожидаемой цели 0 - обработка 1 - цель достигнута 2 - цель не достигнута: { main_status }; Идентификатор цели первого уровня 1 - подтвержден 2 - отказ 3 - аннулирован (фрод): { target1 }; Идентификатор цели второго уровня 4 - выкуп 5 - не выкуп (отклонен): { target2 }&postbackApiKey=e947164f9dd0fff3e8329af99e0ade9d
 */

return [
	23055 => [
		'terraleadsApiKey'			=> 'b6f21d91136ed40d81c7c4a64ddf14d7',
		'postbackApiKey'			=> 'b6f21d91136ed40d81c7c4a64ddf1412',
		'key' 						=> 'gkky3jrh9ubm0d9g',
		
		'offers' => [
			2984 => [
				'product_id'=> 195,
				'offer_id'	=> 139,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2985 => [
				'product_id'=> 82,
				'offer_id'	=> 56,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2986 => [
				'product_id'=> 67,
				'offer_id'	=> 46,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2987 => [
				'product_id'=> 23,
				'offer_id'	=> 8,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'		=> '',
			],
			'reject'	=> [
				'reject'	=> '',
			],
			'expect'	=> [
				'expect'	=> '',
			],
			'confirm'	=> [
				'confirm'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://umgid.com/api/order_for_stream',
		'urlOrderInfo'		=> '',
	],
];

?>