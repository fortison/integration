<?php

return [
	39321 => [
		'terraleadsApiKey' 	=> '73b1009a21b1494e5a80032158e3f697',
		'x_api_key' 		=> 'FB351037626DC3AB45CD60CD5E3271C0',
		'x_token' 			=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7ImlkIjoiNCJ9LCJpYXQiOjE3MTIyMzU1OTgsImV4cCI6MTc0MzMzOTU5OH0.kBiATqgG76Pc0IpZdTf7WGxZGg4ruvJhBcWfpeE3w4M',
		
		'offers' => [
			6920 => [
				'product' => 'Slimnormal',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8785 => [
				'product' => 'Slimonex',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6921 => [
				'product' => 'Prostanormal',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6922 => [
				'product' => 'Gastronormal',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6923 => [
				'product' => 'Cardionormal',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6924 => [
				'product' => 'Venonormal',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6925 => [
				'product' => 'Hemonormal',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6926 => [
				'product' => 'Flexonormal',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6927 => [
				'product' => 'GoldSkin',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6928 => [
				'product' => '24K Gold Serum',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7488 => [
				'product' => 'DiaNormal',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7487 => [
				'product' => 'ImunoNormal',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'trash' => '',
			],
			'reject' => [
				'reject' => '',
			],
			'expect' => [
				'expect' => '',
			],
			'confirm' => [
				'confirm' => '',
			],
		],
		'brakeLogFolder' 	=> true,
		'urlOrderAdd' 		=> 'http://leadmonetize.net:80/cicool/api/tleads/add',
		'urlOrderInfo' 		=> 'http://leadmonetize.net:80/cicool/api/tleads/all?filters[0][co][0][vl]={lead_id}&filters[0][co][0][fl]=lead&filters[0][co][0][op]%3Dequal=equal',
	],
];

?>