<?php

return [
    17370 => [
        'terraleadsApiKey'			=> 'e947164f9dd0fff3e8329af99e0ade9c',
        'postbackApiKey'			=> 'e947164f9dd0fff3e8329af99e0ade9a',
        'api_key' 					=> '4552d551a4350b5f21e1116634987e0121304ec5',

        'offers' => [
            9362 => [// Tensilite - PH
                'goal_id'  => 576,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            9343 => [// Optalite - MY
                'goal_id'  => 533,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            9344 => [// Dozerex - MY
                'goal_id'  => 534,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            9337 => [// Trubonita - ID
                'goal_id'  => 553,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            9341 => [// Protolite - TH
                'goal_id'  => 522,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            9342 => [// Optifita - TH
                'goal_id'  => 428,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            9338 => [// DiafibrePro - MY
                'goal_id'  => 546,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            9339 => [// Varicap - MY
                'goal_id'  => 535,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            9340 => [// Ailen - VN
                'goal_id'  => 545,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            8095 => [// Parasotin - MY
                'goal_id'  => 239,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            8093 => [// Insinol - MY
                'goal_id'  => 241,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            8094 => [// Uranol - MY
                'goal_id'  => 240,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            8097 => [// Dazibet - MY
                'goal_id'  => 405,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            8144 => [// Flexinol - TH
                'goal_id'  => 4417,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            8169 => [// Truepower - VN
                'goal_id'  => 447,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            8579 => [// Burnbiofit-PH
                'goal_id'  => 478,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            8578 => [// Parasinol - PH
                'goal_id'  => 477,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            8577 => [// Durafin - PH
                'goal_id'  => 496,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            8113 => [// Maxiflex-PH
                'goal_id'  => 343,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            8139 => [// Hamerol - TH
                'goal_id'  => 261,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            8721 => [// Maax - TH
                'goal_id'  => 242,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            8112 => [// Prostosil - PH
                'goal_id'  => 406,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            8036 => [// Lipixgo - VN
                'goal_id'  => 407,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            8037 => [// Vitafit-VN
                'goal_id'  => 164,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            8038 => [// Activcare - VN
                'goal_id'  => 182,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            8039 => [// Varilin - VN
                'goal_id'  => 245,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7936 => [// GlucoCalm - PH
                'goal_id'  => 353,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7923 => [// Duracore - VN
                'goal_id'  => 289,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7924 => [// Penirum Pro+ - VN
                'goal_id'  => 128,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7800 => [// Keravits - ID
                'goal_id'  => 290,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7775 => [// Cellarin240 - ID (medium price)
                'goal_id'  => 411,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7776 => [// Hemonix240 - ID (medium price)
                'goal_id'  => 412,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7777 => [// Tensimin240 - ID (medium price)
                'goal_id'  => 413,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7778 => [// Verakose590 - TH (medium price)
                'goal_id'  => 316,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7779 => [// Hapanix590 - TH (medium price)
                'goal_id'  => 414,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7781 => [// Tansonus590 - TH (medium price)
                'goal_id'  => 415,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7782 => [// Maaxx590 - TH (medium price)
                'goal_id'  => 416,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7784 => [// Eyelab290 - VN (medium price)
                'goal_id'  => 417,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7676 => [// Diaclose - ID (medium price)
                'goal_id'  => 395,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7678 => [// Megamove - ID (medium price)
                'goal_id'  => 397,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7679 => [// Hemonix - ID (medium price)
                'goal_id'  => 398,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7680 => [// Cellarin - ID (medium price)
                'goal_id'  => 399,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7681 => [// JFlex - ID (medium price)
                'goal_id'  => 400,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7682 => [// Glucoactive - ID (medium price)
                'goal_id'  => 199,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7677 => [// Tensimin - ID (medium price)
                'goal_id'  => 271,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7347 => [// Jointlab - PH
                'goal_id'  => 354,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7346 => [// Hapanix - PH
                'goal_id'  => 284,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7344 => [// PenirumproHC - PH
                'goal_id'  => 293,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7345 => [// Glucoactive - PH
                'goal_id'  => 35300,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7391 => [// Neagrex - MY
                'goal_id'  => 279,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            7208 => [// DuracoreNo1 - TH
                'goal_id'  => 337,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            5381 => [// Puronix - ID
                'goal_id'  => 132,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            6598 => [// Movita - TH
                'goal_id'  => 275,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            5927 => [// Duracore - ID
                'goal_id'  => 187,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            3379 => [// Garcinia Optimal - VN
                'goal_id'  => 231,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            5434 => [// Duramax - TH
                'goal_id'  => 131,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            4469 => [// Prostanix - VN
                'goal_id'  => 110,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            4470 => [// Hemocure - VN
                'goal_id'  => 109,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],
            4561 => [// Calmerol - ID
                'goal_id'  => 108,

                'configs' => [
                    'brakeLogFolder'    => true,
                    ],
            ],

            1761 => [// deeper - TH

                'goal_id'  => 112,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            2883 => [// Stromax - ID

                'goal_id'  => 19,

                'configs' => [

                    'brakeLogFolder'    => true,
                ],
            ],

            4054 => [// Halipix - VN

                'goal_id'  => 88,

                'configs' => [

                    'brakeLogFolder'	=> true,
                ],
            ],
            4057 => [ // Insulab - VN

                'goal_id'  => 87,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4052 => [ // Wellgo - VN

                'goal_id'  => 86,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3822 => [ // Mencore - VN

                'goal_id'  => 74,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3382 => [ // Jointfine - VN

                'goal_id'  => 55,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3405 => [ // Venafix - VN

                'goal_id'  => 29,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3000 => [ // JointCure - VN

                'goal_id'  => 47,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3409 => [ // Varikose - VN tes

                'goal_id'  => 28,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            2339 => [ // Testoherb1hour - VN

                'goal_id'  => 27,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
             2338 => [ // Testoherb - VN
                'goal_id'  => 26,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            2340 => [ //  Slimherbal - VN
                'goal_id'  => 25,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            2649 => [ //   Penirex - VN
                'goal_id'  => 23,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3670 => [ //  Men Booster Coffee - VN
                'goal_id'  => 15,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            2877 => [ // JointLab - VN
                'goal_id'  => 13,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3156 => [ // HemiCare - VN
                'goal_id'  => 12,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            2999 => [ // Hapanix - VN
                'goal_id'  => 11,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            2879 => [ // GlucoActive - VN
                'goal_id'  => 10,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3056 => [ // Flexa - VN
                'goal_id'  => 9,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3043 => [ // Ecoclean - VN
                'goal_id'  => 8,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            2547 => [ // Detoxherb - VN
                'goal_id'  => 6,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3383 => [ // Beauty Slim - VN
                'goal_id'  => 5,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3384 => [ // Money Amulet - VN
                'goal_id'  => 3,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3248 => [ // Penirum A+ - VN
                'goal_id'  => 24,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4244 => [ // Liverherb - ID
                'goal_id'  => 103,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4150 => [ // Hairlab - ID
                'goal_id'  => 94,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4051 => [ // Cellarin - ID
                'goal_id'  => 93,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4194 => [ // Varikose - ID
                'goal_id'  => 92,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3287 => [ // Paralab - ID
                'goal_id'  => 76,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3604 => [ // EyeLab - ID
                'goal_id'  => 75,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3001 => [ // Megamove - ID
                'goal_id'  => 64,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3380 => [ // Hemolab - ID
                'goal_id'  => 63,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4192 => [ //  Jointlife - ID
                'goal_id'  => 62,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3219 => [ //  Maxiburn - ID
                'goal_id'  => 56,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3338 => [ // Tensilab - ID
                'goal_id'  => 20,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4191 => [ // Insulab - ID
                'goal_id'  => 18,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3181 => [ // Glucoactive - ID
                'goal_id'  => 17,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3002 => [ // Amaislim - ID
                'goal_id'  => 7,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4193 => [ // Havita - ID
                'goal_id'  => 22,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4555 => [ // Hapanix - ID
                'goal_id'  => 113,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4246 => [ //  Prosherb - TH
                'goal_id'  => 106,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4201 => [ //  Hafaz - TH
                'goal_id'  => 99,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4200 => [ // Viakore - TH
                'goal_id'  => 98,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4196 => [ // Garcinia - TH
                'goal_id'  => 90,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3895 => [ // Havita - TH
                'goal_id'  => 84,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4197 => [ // Bravits - TH
                'goal_id'  => 80,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3667 => [ // Wellgo - TH
                'goal_id'  => 44,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3668 => [ // Verakose - TH
                'goal_id'  => 43,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3377 => [ // Vernarex - TH	
                'goal_id'  => 40,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3608 => [ // Tansonus - TH
                'goal_id'  => 39,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3610 => [ // Multilan  - TH
                'goal_id'  => 37,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3609 => [ // Hapanix - TH
                'goal_id'  => 36,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3378 => [ // Greenherb - TH
                'goal_id'  => 35,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3445 => [ // Everlift - TH
                'goal_id'  => 34,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3376 => [ // Duracore - TH
                'goal_id'  => 33,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3605 => [ // DiaHerbal - TH
                'goal_id'  => 31,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3450 => [ // Puronix - TH
                'goal_id'  => 30,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4198 => [ // Money Amulet - TH
                'goal_id'  => 4,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            2057 => [ // Talismoney - TH
                'goal_id'  => 141,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3896 => [ // Lavite - TH
                'goal_id'  => 85,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3606 => [ // Truviga - TH
                'goal_id'  => 38,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4440 => [ // Testoxmen - ID
                'goal_id'  => 124,

                'configs' => [

                    'brakeLogFolder'    => true,
                ],
            ],
            4421 => [ // Prostanix - ID
                'goal_id'  => 229,

                'configs' => [

                    'brakeLogFolder'    => true,
                ],
            ],
            6065 => [ // Prostanix adult- ID
                'goal_id'  => 102,

                'configs' => [

                    'brakeLogFolder'    => true,
                ],
            ],
            4423 => [ // Lamora - ID
                'goal_id'  => 107,

                'configs' => [

                    'brakeLogFolder'    => true,
                ],
            ],
            4418 => [ // Upbust - ID
                'goal_id'  => 104,

                'configs' => [

                    'brakeLogFolder'    => true,
                ],
            ],
            4420 => [ //   Halipix - ID
                'goal_id'  => 101,

                'configs' => [

                    'brakeLogFolder'    => true,
                ],
            ],
            4553 => [ //   Lavite - ID
                'goal_id'  => 121,

                'configs' => [

                    'brakeLogFolder'    => true,
                ],
            ],
            4543 => [ //   Slimmax - ID
                'goal_id'  => 123,

                'configs' => [

                    'brakeLogFolder'    => true,
                ],
            ],
            4560 => [ //   Efferin - TH
                'goal_id'  => 120,

                'configs' => [

                    'brakeLogFolder'    => true,
                ],
            ],
        ],
    ],

    'configs' => [
        'statuses' => [
            'trash'	=> [
                'trash' 			=> '',
            ],
            'reject'	=> [
                'reject'	        => '',
            ],
            'expect'	=> [
                'expect'			=> '',
            ],
            'confirm'	=> [
                'confirm'	        => '',
            ],
        ],

        'brakeLogFolder'	=> true,
        'urlOrderAdd'		=> 'https://tracking.affscalecpa.com/api/v2/affiliate/leads',
//		'statusEnabled'		=> false,
    ],
];