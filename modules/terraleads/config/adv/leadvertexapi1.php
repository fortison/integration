<?php

/**
 * @see http://demo-1.leadvertex.ru/webmaster/api.html
 */

return [
    26894 => [
        'terraleadsApiKey'			=> 'ea58efe47f0ef262456c1a2dc858eb5a',
        'webmasterID'				=> 14,

        'offers' => [
            3616 => [ // Crystallex - HU
                'token'				=> 'terraleads',
                'goodId'			=> '1960',
                'domain'				=> 'http://hu-crystallex.beauty-goods.org/',
                'externalID' => function ($model, $config) {
                    return $model->id;
                },
                'additional15' => function ($model, $config) {
                    return $model->stream_id;
                },
                'externalWebmaster'		=> function ($model, $config) {
                    return $model->web_id;
                },
                'configs' => [
                    'brakeLogFolder'	=> true,
                    'urlOrderAdd'		=> 'http://art365-crystallex-hu-eupro.leadgroup.su/api/webmaster/addOrder.html',
                    'urlOrderInfo'		=> 'http://art365-crystallex-hu-eupro.leadgroup.su/api/webmaster/v2/getOrdersByIds.html',
                    'bridgeHandler' 	=> 'app\modules\terraleads\handlers\adv\leadvertexapi1\user_26894\Leadvertexapi1BridgeHandler',
                    'statusHandler' 	=> 'app\modules\terraleads\handlers\adv\leadvertexapi1\user_26894\Leadvertexapi1StatusHandler',
                ],
            ],
            3615 => [ // Crystallex - IT
                'token'				=> 'terraleads',
                'goodId'			=> '1957',
                'domain'				=> 'http://it-pf-crystallex.beauty-goods.org/',
                'externalID' => function ($model, $config) {
                    return $model->id;
                },
                'additional15' => function ($model, $config) {
                    return $model->stream_id;
                },
                'externalWebmaster'		=> function ($model, $config) {
                    return $model->web_id;
                },

                'configs' => [
                    'brakeLogFolder'	=> true,
                    'urlOrderAdd'		=> 'http://art365-crystallex-it-eupro.leadgroup.su/api/webmaster/addOrder.html',
                    'urlOrderInfo'		=> 'http://art365-crystallex-it-eupro.leadgroup.su/api/webmaster/v2/getOrdersByIds.html',
                    'bridgeHandler' 	=> 'app\modules\terraleads\handlers\adv\leadvertexapi1\user_26894\Leadvertexapi1BridgeHandler',
                    'statusHandler' 	=> 'app\modules\terraleads\handlers\adv\leadvertexapi1\user_26894\Leadvertexapi1StatusHandler',
                ],
            ],
            3617 => [ // Crystallex - RO
                'token'				=> 'terraleads',
                'goodId'			=> '1959',
                'domain'				=> 'http://hu-crystallex.beauty-goods.org/',
                'externalID' => function ($model, $config) {
                    return $model->id;
                },
                'additional15' => function ($model, $config) {
                    return $model->stream_id;
                },
                'externalWebmaster'		=> function ($model, $config) {
                    return $model->web_id;
                },
                'configs' => [
                    'brakeLogFolder'	=> true,
                    'urlOrderAdd'		=> 'http://art365-crystallex-ro-eupro.leadgroup.su/api/webmaster/addOrder.html',
                    'urlOrderInfo'		=> 'http://art365-crystallex-ro-eupro.leadgroup.su/api/webmaster/v2/getOrdersByIds.html',
                    'bridgeHandler' 	=> 'app\modules\terraleads\handlers\adv\leadvertexapi1\user_26894\Leadvertexapi1BridgeHandler',
                    'statusHandler' 	=> 'app\modules\terraleads\handlers\adv\leadvertexapi1\user_26894\Leadvertexapi1StatusHandler',
                ],
            ],
        ],
        'reasonCancel' => [
            1 => 'Не заказывал',
            2 => 'Дорого',
            3 => 'Передумал',
            4 => 'Нет денег ',
            5 => 'Уезжает',
            6 => 'Уже купил',
            7 => 'Не туда попали',
            8 => 'На сайте другая цена',
            9 => 'Боится заказывать. Думает мошенники',
            10 => 'Отрицательные отзывы в интернете',
            11 => 'Долгая доставка',
            12 => 'Медицинские противопоказания',
            13 => 'Товар уже не интересен',
            14 => 'Муж, жена, родственники не разрешают',
            15 => 'Сделали заказ ради шутки',
            16 => 'Сделали заявку чтобы получить консультацию ',
            17 => 'Высокая цена доставки',
            18 => 'Сейчас не могу, в след. месяце сам сделаю новый заказ',
            19 => 'Недозвон в течении долгого времени',
            20 => 'Заказывал другой продукт ',
            21 => 'Не идёт на контакт, бросает трубку, агрессивно реагирует',
            99 => 'Другое',
        ],
        'configs' => [
            'statuses' => [
                'confirm'	=> [
                    '1'			=> '',
                ],
                'reject' => [
                    '-1'		=> '',
                ],
                'expect' => [
                    '0'			=> '',
                ],
                'trash' => [
                ],
            ],
            'ransom' => [
                'confirm'	=> [
                    '1'			=> '',
                ],
                'reject'	=> [
                    '-1'		=> '',
                ],
                'expect'	=> [
                    '0'			=> '',
                ],
            ],
        ],
        //		'urlApileadOrderUpdate'		=> 'http://al-api.com/api/lead/update',
//		'bridgeEnabled'				=> 1,																				// Чи активна ця інтеграція
//		'statusEnabled'				=> null,																			// Чи опрацьовувати статуси цієї інтеграції
//		'urlOrderAdd'				=> 'https://magnetlashes990.leadvertex.ru/addOrder.html',
//		'urlOrderInfo'				=> 'https://magnetlashes990.leadvertex.ru/getOrdersByIds.html',
        'bridgeRequestTimeout'		=> 60,
        'statusRequestTimeout'		=> 60,
        'enablePhoneChars'			=> false,
//		'brakeLogFolder'			=> '',
//		'statusHandler' 			=> 'app\modules\apilead\handlers\adv\leadvertex\LeadvertexStatusHandler',
//		'bridgeHandler' 			=> 'app\modules\apilead\handlers\adv\leadvertex\LeadvertexBridgeHandler',
    ],
];
