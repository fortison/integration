<?php

/*
 * У чаті U3VwcG9ydG1hcnRFcGF5Q2FylA
 *
 * https://affiliate.nutrafirst.in
 * Kosta@TerraLeads.com
 * terra@123
 *
 * http://connectcpa.doc/terraleads/adv/adv-postback-status?systemUserId=18885&systemOrderId={sub1}&partnerStatus={status}&comment={comment}&postbackApiKey=2578198002a3c3e82708eda687a97503
 */

return [
	64095 => [
		'terraleadsApiKey'			=> '4eddc36aa261a5b14ac0c9d781ca4559',
		'api_key' 					=> '9c9b1cffe1c1c3f868b79a8aa0524b23',


		'offers' => [
			9127 => [ //Keto CPL - IN
				'offer_id'	=> 1272,//111
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9126 => [ //Keto CPL - IN (low price)
				'offer_id'	=> 1211,//111
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8999 => [ //Xtra Josh CPL - IN (low price)
				'offer_id'	=> 1171,//11
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	18885 => [
		'terraleadsApiKey'			=> 'fcbfb17301b337d95d8ea3f2f38d906d',
		'api_key' 					=> '9c9b1cffe1c1c3f868b79a8aa0524b23',


		'offers' => [
			2806 => [
				'offer_id'	=> 321,//11
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4016 => [
				'offer_id'	=> 111,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4075 => [ //EffectEro_FB traffic - IN
				'offer_id'	=> 141,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4076 => [ //EffectEro - IN
				'offer_id'	=> 131,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5121 => [ //Keto One_FB CPL - IN
				'offer_id'	=> 231,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'3'	=> 'Duplicate',
                'Trash' => '',
			],
			'reject'	=> [
				'5'	=> 'Declined',
				'Rejected' => '',
			],
			'expect'	=> [
				'4'	=> 'Hold',
				'Pending' => '',
			],
			'confirm'	=> [
				'7'	=> 'Approved',
				'Approved'=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://api.nutrafirst.in/affiliate/lead/add',
		'urlOrderInfo'		=> 'https://api.nutrafirst.in/affiliate/lead/pull_order_status',
	],
];

?>