<?php

return [
	49046 => [
        'terraleadsApiKey'			=> '773dc6fcedd975a2251b619e9a0d1aaf',
        'token' 			    	=> 'abf8e6d02ed3ea51a7124da4b3dd613d',

        'offers' => [
            3866 => [

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
        ],
        'configs' => [
            'statuses' => [
                'trash'	=> [
                    '9' =>'Wrong Number',
                    '14'=>'COD NOT AVAILABLE',
                    '10'=> 'Address Issues',
                    '11'=> 'Late Date Order',
                ],
                'reject'	=> [
                    '8'  =>'Cancel/Not Interested',
                    '17' => 'Incoming call',
                    '13' => 'Incoming Not Available',
                    '16' => 'Cancel',
                ],
                'expect'	=> [
                    '0' =>'New',
                    '12'=>'New',
                    '4' =>'Call Not Pickup',
                    '3' =>'Call after Sometimes',
                    '2' =>'Call Repet - 1',
                    '6' =>'Call Repet - 2',
                    '7' =>'Call Repet - 3',

                ],
                'confirm'	=> [
                    '1' =>'Customer',
                    '5' =>'Confirm Customer',
                    '15'=>'Order Place',
                ],
            ],
            'brakeLogFolder'	=> true,
            'urlOrderAdd'		=> 'http://crm.cool10wap.com/api/v1.0/create-leads',
            'urlOrderInfo'		=> 'http://crm.cool10wap.com/api/v1.0/get-mylead',
        ],
    ],
	
	27726 => [
        'terraleadsApiKey'			=> '3f82911435cad0732bd79e5a6a70e67b',
        'token' 			    	=> 'abf8e6d02ed3ea51a7124da4b3dd613d',

        'offers' => [
            3866 => [

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
        ],
        'configs' => [
            'statuses' => [
                'trash'	=> [
                    '9' =>'Wrong Number',
                    '14'=>'COD NOT AVAILABLE',
                    '10'=> 'Address Issues',
                    '11'=> 'Late Date Order',
                ],
                'reject'	=> [
                    '8'  =>'Cancel/Not Interested',
                    '17' => 'Incoming call',
                    '13' => 'Incoming Not Available',
                    '16' => 'Cancel',
                ],
                'expect'	=> [
                    '0' =>'New',
                    '12'=>'New',
                    '4' =>'Call Not Pickup',
                    '3' =>'Call after Sometimes',
                    '2' =>'Call Repet - 1',
                    '6' =>'Call Repet - 2',
                    '7' =>'Call Repet - 3',

                ],
                'confirm'	=> [
                    '1' =>'Customer',
                    '5' =>'Confirm Customer',
                    '15'=>'Order Place',
                ],
            ],
            'brakeLogFolder'	=> true,
            'urlOrderAdd'		=> 'http://crm.cool10wap.com/api/v1.0/create-leads',
            'urlOrderInfo'		=> 'http://crm.cool10wap.com/api/v1.0/get-mylead',
        ],
    ],

    29923 => [
        'terraleadsApiKey'			=> 'b34771348102b166d28e9991fe08a73d',
        'token' 			    	=> 'e7580b3774504b00238d2d886b2d32d1',

        'offers' => [
            4204 => [

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
        ],
        'configs' => [
            'statuses' => [
                'trash'	=> [
                    '13'=>'Wroung Number',
                    '9'=>'COD NOT AVAILABLE',
                    '10'=> 'Address Issues',
                    '11'=> 'Late Date Order',
                ],
                'reject'	=> [
                    '8'  =>'Cancel/Not Interested',
                    '17' => 'Incoming call',
                    '13' => 'Incoming Not Available',
                    '16' => 'Cancel',
                    '3' => 'Rejected',
                ],
                'expect'	=> [
                    '2' =>'New',
                    '5' =>'Call Not Pickup',
                    '7' =>'Call After Some Time',
                    '6' =>'Switch Off',
                    '12' =>'Future Order',
                    '14' =>'INA',
                ],
                'confirm'	=> [
                    '10' =>'Delivered',
                    '1' =>'Customer',
                    '4' =>'Approved',
                    '5' =>'Confirm Customer',
                    '11'=>'Order Place',
                ],
            ],
            'brakeLogFolder'	=> true,
            'urlOrderAdd'		=> 'https://titan69.in/crm/api/v1.0/create-leads',
            'urlOrderInfo'		=> 'https://titan69.in/crm/api/v1.0/get-mylead',
        ],
    ],
];


?>