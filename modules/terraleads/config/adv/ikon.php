<?php

/*
 * В файлі, статуси надсилають в теру самостійно.
 *
 * http://connectcpa.net/terraleads/adv/adv-postback-status?systemOrderId={sub1}&partnerStatus={status}&comment={comment}&postbackApiKey=002e827687a975a3c325781980308eda
 */

return [
	
	15344 => [
		'terraleadsApiKey'			=> '439702c558166ce5b1da9365d87dc12f',
		'postbackApiKey'			=> '002e827687a975a3c325781980308eda',
//		'publisher_id' 				=> 'terraleads',
//		'token' 					=> '4Jd38mO6frEqSzAD1ctwjNfqnUbMTK0g',
		
		'offers' => [
			4007 => [ //Gerliver - VN
				'flow_hash'	=> 'Y7DYoRXkSo',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3584 => [ //Hero Plus - PH
				'flow_hash'	=> 'rlcEOwDB0C',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3586 => [ //DTX - PH
				'flow_hash'	=> '97JhJRoCDt',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3585 => [ //Germany Gold Care - PH
				'flow_hash'	=> 'NJlfEaHBAw',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3583 => [ //Lipid Zero - PH
				'flow_hash'	=> '8oyNKZTXZY',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2208 => [
				'flow_hash'	=> 'hootpon',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2222 => [
				'flow_hash'	=> 'blacklatte-vn',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2228 => [
				'flow_hash'	=> 'greenspa-id',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2874	 => [
				'flow_hash'	=> 'k36VXyvv3g',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3067	 => [
				'flow_hash'	=> 'JS7XLPMtFl',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3066	 => [
				'flow_hash'	=> 'W4KTgB4fTt',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3061	 => [
				'flow_hash'	=> 'Ux3itOEopc',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3068	 => [
				'flow_hash'	=> 'cfpT0eR6hB',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3063	 => [
				'flow_hash'	=> 'afR2ONiIQL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3062	 => [
				'flow_hash'	=> 'C2vpdRbGb4',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3065	 => [
				'flow_hash'	=> '4zd5uacu7t',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3064	 => [
				'flow_hash'	=> 'LoM065gQPF',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3283	 => [
				'flow_hash'	=> 'TrcQ83rXnl',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3284	 => [
				'flow_hash'	=> '0D6fV93kzI',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3328	 => [ // Bioseefit 
				'flow_hash'	=> 'HdCLM1y9CX',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3330	 => [ // NaturKlin - ID 
				'flow_hash'	=> 'nIqTSRjzzv',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3325	 => [ // Glusure - VN
				'flow_hash'	=> 'Ap4Dj6RdyR',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3347	 => [ // Slim Mix - VN
				'flow_hash'	=> 'RhMQzPguuV',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3386	 => [ // Germany Gold Care - VN
				'flow_hash'	=> 'XhaemJNzzD',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3410	 => [ // Eco Calm - VN
				'flow_hash'	=> 'FXAItRU73J',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3423	 => [ // Lycium Serum - MY
				'flow_hash'	=> '1rY4l0Iuib',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3487	 => [ // Max Enhancer - ID
				'flow_hash'	=> 'GlZwxFvbE6',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4065	 => [ // Hermuno - ID
				'flow_hash'	=> 'JB7xnUJcL0',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'	=> '',
				'duplicate'	=> '',
			],
			'reject'	=> [
				'cancel'	=> '',
			],
			'expect'	=> [
				'create'	=> '',
			],
			'confirm'	=> [
				'approved'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://ls.cpaikon.net/v2/external/lead/accept',
		'urlOrderInfo'		=> '',
	],
];

?>