<?php

/**
 * You should send us leads using POST request with JSON data, like this
 * {
 * "url": "https://crm.stepmode.ba/apiNewLead.php?token=f247d04e0ea92a5c8be0f1c4e45188293395c4f3",
 * "params": {
 * "full_name": "BUYER_NAME",
 * "city": "",
 * "phone": "BUYER_PHONE",
 * "external": "",
 * "address": "",
 * "goodID": "GOOD_ID",
 * "quantity": "1",
 * "price": "PRODUCT_PRICE",
 * "additional1": "LEAD_ID",
 * "additional2": "",
 * "additional3": ""
 * }
 * }
 *
 * And later you can check lead status sending this request
 * https://crm.stepmode.ba/api_getOrderData.php?token=f247d04e0ea92a5c8be0f1c4e45188293395c4f3&lead_id=YOUR_LEAD_ID
 * (or we can send query to your URL on every first lead status change including accept, cancel and spam)
 */

return [
	49201 => [
		'terraleadsApiKey' => '32bc6e649d135aa389a417c047061f03',
		'token' => 'f247d04e0ea92a5c8be0f1c4e45188293395c4f3',
		
		'offers' => [
			9027 => [ // HairFlex - RS
				'good_id' => 13,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8994 => [ // Dialok - RS (low price)
				'good_id' => 89,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
            8983 => [ // Otorin - RS (low price)
				'good_id' => 62,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8988 => [ // Osteon - RS (low price)
				'good_id' => 54,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8979 => [ // Venisal gel - RS (low price)
				'good_id' => 32,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8975 => [ // Hemoren - RS (low price)
				'good_id' => 30,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8971 => [ // Noktal - RS (low price)
				'good_id' => 29,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7537 => [ // Hemorolok - RS
				'good_id' => 71,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7534 => [ // Dialok - RS
				'good_id' => 70,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8944 => [ // Kossalin Shampoo - RS (low price)1
				'good_id' => 34,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7540 => [ // Hypertin - RS
				'good_id' => 72,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8026 => [ // Grovi - RS
				'good_id' => 21,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8100 => [ // Osteon - RS
				'good_id' => 23,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8102 => [ // Noktal - RS
				'good_id' => 12,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7545 => [ // Reduslim - RS
				'good_id' => 74,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8252 => [ // Reduslim - RS (low price)
				'good_id' => 77,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8248 => [ // Biolift - RS (low price)
				'good_id' => 52,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8238 => [ // Herbolo - RS (low price)
				'good_id' => 48,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8234 => [ // Grovi - RS (low price)
				'good_id' => 43,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8208 => [ // Hypertin - RS (low price)
				'good_id' => 87,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7222 => [ // Otorin - RS
				'good_id' => 18,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5926 => [ // Hemoren - RS
				'good_id' => 15,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6699 => [ // Venisal gel - RS
				'good_id' => 25,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4142 => [ // Kossalin Shampoo - RS
				'good_id' => 7,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4544 => [ // Biolift - RS
				'good_id' => 4,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4986 => [ // Herbolo - RS
				'good_id' => 8,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash' => [
					'Double' => '',
					'Spam' => '',
					'PotentionalDouble' => '',
				],
				'reject' => [
					'Cancelled' => '',
				],
				'expect' => [
					'PersonalRecall' => '',
					'Processing' => '',
					'Recall' => '',
				],
				'confirm' => [
					'Accepted' => '',
				],
			],
			'brakeLogFolder' => true,
			'urlOrderAdd' => 'https://crm.stepmode.ba/apiNewLead.php',
			'urlOrderInfo' => 'https://crm.stepmode.ba/api_getOrderData.php',
		]
	],
	26274 => [
		'terraleadsApiKey' => '23a857c83157f552e88a9ea9baaa2e7c',
		'token' => 'f247d04e0ea92a5c8be0f1c4e45188293395c4f3',
		
		'offers' => [
			8689 => [ // Biolift - SI
				'good_id' => 4,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8690 => [ // Noktal - SI
				'good_id' => 12,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8844 => [ // Herbolo - SI
				'good_id' => 8,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8691 => [ // Hemoren - SI
				'good_id' => 15,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8692 => [ // Venisal gel - SI
				'good_id' => 25,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8693 => [ // Osteon - SI 
				'good_id' => 23,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8694 => [ // Grovi - SI
				'good_id' => 21,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9029 => [ // HairFlex - BA (low price)
				'good_id' => 60,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9028 => [ // HairFlex - HR
				'good_id' => 13,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9026 => [ // HairFlex - BA
				'good_id' => 13,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8695 => [ // Hairflex - SI
				'good_id' => 13,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9222 => [ // Biolift - HU
				'good_id' => 4,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9221 => [ // Noktal - HU
				'good_id' => 12,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9220 => [ // Herbolo - HU
				'good_id' => 8,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9219 => [ // Hemoren - HU
				'good_id' => 15,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9218 => [ // Venisal gel - HU
				'good_id' => 25,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9217 => [ // Osteon - HU
				'good_id' => 23,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9216 => [ // Grovi - HU
				'good_id' => 21,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9215 => [ // Hairflex - HU
				'good_id' => 13,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9214 => [ // Vivader - HU
				'good_id' => 119,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9213 => [ // Busthill - HU
				'good_id' => 75,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8966 => [ // Prostatin caps - BA (low price)
				'good_id' => 105,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8995 => [ // Dialok - ME (low price)
				'good_id' => 126,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8993 => [ // OtoCaps - BA (low price)
				'good_id' => 61,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8700 => [ // OtoCaps - BA
				'good_id' => 24,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8992 => [ // Dianorm - BA (low price)
				'good_id' => 109,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8990 => [ // Osteon - HR (low price)
				'good_id' => 84,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8987 => [ // Osteon - BA (low price)
				'good_id' => 55,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8986 => [ // Okulis - BA (low price)
				'good_id' => 51,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8978 => [ // Venisal gel - BA (low price)
				'good_id' => 56,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8980 => [ // Venisal gel - ME (low price)
				'good_id' => 57,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8981 => [ // Venisal gel - HR (low price)
				'good_id' => 86,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8984 => [ // Regulocaps - BA (low price)
				'good_id' => 69,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8977 => [ // Hemoren - HR (low price)
				'good_id' => 79,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8976 => [ // Hemoren - ME (low price)
				'good_id' => 90,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8974 => [ // Hemoren - BA (low price)
				'good_id' => 76,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8991 => [ // Noktal - ME (low price)
				'good_id' => 41,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8972 => [ // Noktal - HR (low price)
				'good_id' => 82,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8970 => [ // Noktal - BA (low price)
				'good_id' => 37,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7223 => [ // Reduslim caps - BA
				'good_id' => 64,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7223 => [ // Reduslim caps - BA
				'good_id' => 64,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7535 => [ // Dialok - ME
				'good_id' => 70,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7538 => [ // Hemorolok - ME
				'good_id' => 71,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8945 => [ // Kossalin Shampoo - BA (low price)
				'good_id' => 45,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8943 => [ // Kossalin Shampoo - ME (low price)
				'good_id' => 35,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8744 => [ // Vivader - BA
				'good_id' => 119,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8745 => [ // Vivader - HR
				'good_id' => 119,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8746 => [ // Vivader - BA (low price)
				'good_id' => 120,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8747 => [ // Vivader - HR (low price)
				'good_id' => 125,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7539 => [ // Hypertin - BA
				'good_id' => 117,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6577 => [ // Grovi - ME
				'good_id' => 21,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8099 => [ // Osteon - BA
				'good_id' => 23,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8407 => [ // Prostatin caps - BA
				'good_id' => 102,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6129 => [ // Kossalin Shampoo - ME
				'good_id' => 7,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6127 => [ // Herbolo - ME
				'good_id' => 8,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8101 => [ // Noktal - BA
				'good_id' => 12,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6126 => [ // Noktal - ME
				'good_id' => 12,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6130 => [ // Biolift - ME
				'good_id' => 4,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6128 => [ // Hemoren - ME
				'good_id' => 15,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8254 => [ // Reduslim - ME (low price)
				'good_id' => 78,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8253 => [ // Reduslim - HR (low price)
				'good_id' => 2111,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8251 => [ // Reduslim - BA (low price)
				'good_id' => 68,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8250 => [ // Biolift - ME (low price)
				'good_id' => 36,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8249 => [ // Biolift - HR (low price)
				'good_id' => 80,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8241 => [ // Biolift - BA (low price)
				'good_id' => 31,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8240 => [ // Herbolo - HR (low price)
				'good_id' => 83,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8239 => [ // Herbolo - ME (low price)
				'good_id' => 50,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8237 => [ // Herbolo - BA (low price)
				'good_id' => 49,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8236 => [ // Grovi - HR (low price)
				'good_id' => 81,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8235 => [ // Grovi - ME (low price)
				'good_id' => 40,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8233 => [ // Grovi - BA (low price)
				'good_id' => 39,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7604 => [ // Grovi - HR
				'good_id' => 21,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7606 => [ // Herbolo - HR
				'good_id' => 8,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7605 => [ // Osteon - HR
				'good_id' => 23,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7602 => [ // Noktal - HR
				'good_id' => 12,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7601 => [ // Hemoren - HR
				'good_id' => 15,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7600 => [ // Biolift - HR
				'good_id' => 4,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6653 => [ // Venisal gel - BA
				'good_id' => 25,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6544 => [ // Okulis - BA
				'good_id' => 19,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6545 => [ // Dianorm - BA
				'good_id' => 103,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6546 => [ // Otorin - BA
				'good_id' => 18,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6547 => [ // Grovi - BA
				'good_id' => 21,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5801 => [ // Hemoren - BA
				'good_id' => 15,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3524 => [ // Kossalin Shampoo - BA
				'good_id' => 7,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3611 => [ // Venoles - BA
				'good_id' => 0,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3868 => [ // Regulopres - BA
				'good_id' => 5,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5196 => [ // Regulocaps - BA
				'good_id' => 10,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4176 => [ // Biolift - BA
				'good_id' => 4,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4090 => [ // Burn Call - BA
				'good_id' => 6,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4820 => [ // Herbolo - BA
				'good_id' => 8,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash' => [
					'Double' => '',
					'Spam' => '',
					'PotentionalDouble' => '',
				],
				'reject' => [
					'Cancelled' => '',
				],
				'expect' => [
					'Processing' => '',
					'Recall' => '',
					'PersonalRecall' => '',
				],
				'confirm' => [
					'Accepted' => '',
					'Accept' => '',
				],
			],
			'brakeLogFolder' => true,
			'urlOrderAdd' => 'https://crm.stepmode.ba/apiNewLead.php',
			'urlOrderInfo' => 'https://crm.stepmode.ba/api_getOrderData.php',
		]
	]
];

?>