<?php

return [
	65794 => [
		'terraleadsApiKey' 	=> '5ece99edc3be7f2e5e8f101b5e8fe09b',
		'utm' 				=> 'Terraleads',
		'hash' 				=> 'WWSvWW534S12ASD98EgVX52',
		
		'offers' => [
			9286 => [ // PX-300 strong - GE1
				'product_id' => '7946',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9287 => [ // PX-300 strong - GE (low price)
				'product_id' => '7820',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9288 => [ // PX-300 strong - GE (free price)
				'product_id' => '7819',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9289 => [ // Mennex - GE
				'product_id' => '7988',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9290 => [ // Mennex - GE (low price)
				'product_id' => '7987',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9291 => [ // Mennex - GE (free price)
				'product_id' => '7981',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9292 => [ // Prostonix - GE
				'product_id' => '7947',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9293 => [ // Prostonix - GE (low price)
				'product_id' => '7905',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9294 => [ // Prostonix - GE (free price)
				'product_id' => '7904',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9295 => [ // Potens hardmax - GE
				'product_id' => '8740',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9296 => [ // Potens hardmax - GE (low price)
				'product_id' => '8739',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9297 => [ // Potens hardmax - GE (free price)
				'product_id' => '8738',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9298 => [ // Libidomatic - GE
				'product_id' => '8458',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9301 => [ // Libidomatic - GE (low price)
				'product_id' => '8457',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			9302 => [ // Libidomatic - GE (free price)
				'product_id' => '8456',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'18' 	=> 'Треш',
			],
			'reject' => [
				'6' 	=> 'Отменён',
				'7' 	=> 'Удалён',
				'17' 	=> 'Отказ',
			],
			'expect' => [
				'0' 	=> 'Новый',
				'9' 	=> 'Недозвон',
				'10' 	=> 'Перезвон',
				'29' 	=> 'Перезвон',
				'13' 	=> 'Контрольный прозвон',
			],
			'confirm' => [
				'2' 	=> 'Отправлен',
				'3' 	=> 'Доставлен',
				'4' 	=> 'Вручен',
				'5' 	=> 'Возврат',
				'8' 	=> 'Оплачен',
				'12' 	=> 'Вруч-Возврат',
				'14' 	=> 'Ожидает отправки',
				'1' 	=> 'Обработан',
			],
		],
		'brakeLogFolder' 	=> true,
		'urlOrderAdd' 		=> 'http://sell1.best/apipublic/newOrder',
		'urlOrderInfo' 		=> 'http://sell1.best/apipublic/getstatus',
	],
];
?>