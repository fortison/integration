<?php


/**
 * https://docs.google.com/document/d/1Kw_xW9GQcBhpnuVZo0MBRyTow3z0BYgnrSsf38xAqRE/edit#heading=h.oxkgmzoiqrb2
 */

return [

    26654 => [
        'terraleadsApiKey'  => 'acccd6dd43b140a0f583ff5184ba8898',
        'apiKey'            => 'c2f1e35d09a4ad89dfb85831a24a24ed',

        'offers' => [
            3614 => [ //Caboki -  AE
                'offerId' => 'pq144xk',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            3588 => [ //Beard Growth -  AE
                'offerId' => '84q0vkk',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            3587 => [ //Pure Pearls -  AE
                'offerId' => '068e1jq3y',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            4179 => [ //Pure Pearls -  OM
                'offerId' => 'zkkvq8',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
           4175 => [ // Envilash - OM
                'offerId' => 'ec83myd',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ], 
            4186 => [ // Maral gel - AE
                'offerId' => 'j6rpdj4t',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ], 
            4203 => [ // Pure Pearls - SA
                'offerId' => '0xz742p',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],   
        ],
    ],
    11389 => [
        'terraleadsApiKey'  => '3d440ac40193d3d9429f1066bd189926',
        'apiKey'            => '573ef45b8136e8b098a8cc6806697e5a',

        'offers' => [
            3466 => [ //MaxMan Cream - AE
                'offerId' => 'nqu2sc1rav',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            3467 => [ //Garcinia Extract - AE
                'offerId' => 'tp397zh',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            3468 => [ //PainGo Gel - AE
                'offerId' => '000000000',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            3811 => [ //RED Pain Relief Gel - AE
                'offerId' => 'gnfn5y1j',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            3802 => [ //24k Gold Primer Serum - AE
                'offerId' => 'd0y08he',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            4202 => [ //24k Gold Primer Serum - QA
                'offerId' => 'agv35hkgbh',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            4205 => [ //Fade Spots Serum - QA
                'offerId' => 'bk5rr9j1',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            4206 => [ //Men Beard Growth Oil - QA
                'offerId' => 'j0d3ccjv',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
        ],
    ],
    'configs' => [
        'statuses' => [
            'reject'	=> [
                3	=> '',
            ],
            'expect'	=> [
                1	=> '',
            ],
            'confirm'	=> [
                2	=> '',
            ],
        ],
        'subStatuses'	=> [
            'trash'	=> [
                1	=> 'Не заказывал',
                4	=> 'Hекорректные данные',
                5	=> 'Дубль',
                6	=> 'Неверное GEO',
                7	=> 'Нету доставки в этот регион',
                8    => 'Консультация по товару',
                9	=> 'Консультация по возврату',
                10	=> 'Абонент недоступен больше недели',
                11	=> 'Изменение GEO',
                13	=> 'Консультация по пред. заявке',
                14	=> 'Тест',
                17	=> 'Не предоставляет всех данных для подтверждения',
                18	=> 'не говорит по-русски',
                19	=> 'Не поддерживаемое GEO',
                20	=> '',
            ],
            'reject' => [
                2	 => 'Нашел дешевле',
                3	 => 'Не устр. дост.',
                12	 => 'Другая причина',
                15	 => 'Дорого',
                16	 => 'Передумал',
            ]
        ],
        'brakeLogFolder'	=> true,
        'urlOrderAdd'		=> 'http://api.monsterleads.pro/method/order.add',
        'urlOrderInfo'		=> 'http://api.monsterleads.pro/method/lead.list',
    ],
];

