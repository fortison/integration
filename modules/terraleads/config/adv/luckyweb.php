
<?php

return [

    27285 => [
        'terraleadsApiKey'			=> '686472381995617ca6825e6628e2750d',
        'apiKey'					=> '5f7ee3493cbd6524043170140437',

        'offers' => [
            3732 => [ //Money Amulet - BG
                'campaign_hash' =>'56be3f95-4e0c-407b-8a3a-ca5ad2217e5b',
                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3731 => [ //Money Amulet - IT
                'campaign_hash' =>'56be3f95-4e0c-407b-8a3a-ca5ad2217e5b',
                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3730 => [ //Money Amulet - ES
                'campaign_hash' =>'56be3f95-4e0c-407b-8a3a-ca5ad2217e5b',
                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
        ],

        'configs' => [
            'brakeLogFolder'	=> true,
            'urlOrderAdd'		=> 'https://lucky.online/api/v1/lead-create/webmaster',
            'urlOrderInfo'		=> 'https://lucky.online/api-webmaster/lead-status-batch.html',
        ],
    ],

    'configs' => [
        'statuses' => [
            'trash'	=> [
                'trash'		=> '',
            ],
            'reject'	=> [
                'rejected'	=> '',
            ],
            'expect'	=> [
                'new'		=> '',
            ],
            'confirm'	=> [
                'confirmed'	=> '',
            ],
        ],
        'brakeLogFolder'			=> true,
    ],
];
?>
