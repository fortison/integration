<?php

return [
    31053 => [
        'terraleadsApiKey'  => '3f6d887e0d8226250f0b468bf6ecfcaf',
        'postbackApiKey' => '3f6d887e0d8226250f0b468bf6ecfca1',

        'api_key' => '96639469d9b34d33fafd0242ecaa0c03',
        'user_id' =>  44707,

        'offers' => [
            7457 => [//NicotinEX - GR
                'flow_id'  => 16908,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            7458 => [//NicotinEX - CY
                'flow_id'  => 16909,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            7044 => [//Nanoflex - IT1
                'flow_id'  => 16867,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            7049 => [//Nanoflex - GR    Cyprus
                'flow_id'  => 16864,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            7048 => [//Nanoflex - GR
                'flow_id'  => 16863,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6411 => [//Fortuflex - BG
                'flow_id'  => 16812,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5368 => [//Fungostop + - GR
                'flow_id'  => 16786,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6386 => [//Fortunella - GR
                'flow_id'  => 16784,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6289 => [//OstyHealth - GR
                'flow_id'  => 16690,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6409 => [//Cardio Life - GR
                'flow_id'  => 16669,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6060 => [//Fortuflex Caps - IT
                'flow_id'  => 16456,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6061 => [//Fortuflex Caps - PT
                'flow_id'  => 16455,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6078 => [//Fortuflex - GR (CY)
                'flow_id'  => 16388,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6059 => [//Fortuflex - GR
                'flow_id'  => 16386,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6058 => [//Fortuflex - IT
                'flow_id'  => 16387,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4404 => [ //CardioFort - MX
//                'offer_id' => 1609,  //	ID оффера в системе
                'flow_id'  => 14820,  // 	ID потока в системе

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4443 => [//CardioFort - CO
                'flow_id'  => 14849,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4405 => [//Fortunella - MX
                'flow_id'  => 14945,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4527 => [//Fortunella - PE
                'flow_id'  => 14944,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4489 => [//Love Pendant - AL
                'flow_id'  => 14946,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4481 => [//Alpha pants - AL
                'flow_id'  => 14947,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4723 => [//Money Amulet - PT
                'flow_id'  => 15200,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4722 => [//Silk Liquid - PT
                'flow_id'  => 15201,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4721 => [//Silk Liquid - IT
                'flow_id'  => 15202,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4720 => [//Silk Liquid - ES
                'flow_id'  => 15203,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4724 => [//Love Pendant - IT
                'flow_id'  => 15204,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4725 => [//Japan Steel - IT
                'flow_id'  => 15205,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4729 => [//Epilage Men - PT
                'flow_id'  => 15206,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4728 => [//Epilage Men - IT
                'flow_id'  => 15207,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4727 => [//Epilage Men - ES
                'flow_id'  => 15208,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4732 => [//Epilage - PT
                'flow_id'  => 15210,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4731 => [//Epilage - IT
                'flow_id'  => 15211,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4730 => [//Epilage - ES
                'flow_id'  => 15209,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4837 => [//EcoLight Plus - PE
                'flow_id'  => 15228,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4855 => [//CardioFort - PE
                'flow_id'  => 15237,  //    ID потока в системе

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
        ],
    ],

    'configs' => [
        'statuses' => [
            'trash' => [
                'trash' => 'фейк',
            ],
            'reject' => [
                'cancelled' => 'отклонен',
            ],
            'expect' => [
                'waiting' => 'в обработке',
            ],
            'confirm' => [
                'approved' => 'принят',
                'guarant' => 'гарант аппрува',
            ],
        ],
        'brakeLogFolder'	=> true,
        'urlOrderAdd'		=> 'https://bestshop-product.ru/order/api.php',
        'urlOrderInfo'		=> '',
    ],
];

?>