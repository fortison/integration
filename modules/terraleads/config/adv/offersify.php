<?php

return [
	50759 => [
		'terraleadsApiKey'		=> '100e4cc9b04e78a1d3cc5748604d4861',
		'postbackApiKey'		=> '9be08976bdd19c25c981be46d94db6fb',
		'uid'					=> 'f51579c8-e52d-47b0-a05e-5e3ee5503556',
		'key'					=> 'f29f4fe4d9332f92b08838',
		
		'offers' => [
			1111 => [//Mini Aspirapolvere - IT
				'offer' 	=> 2114,
				'lp' 		=> 4891,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7410 => [//Buzz Trapper - IT
				'offer' 	=> 2250,
				'lp' 		=> 5080,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7020 => [//Cookware set - PL
				'offer' 	=> 2069,
				'lp' 		=> 4831,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7021 => [//Cookware set - IT
				'offer' 	=> 1955,
				'lp' 		=> 4846,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7029 => [//Mold Stop 3x1 - IT
				'offer' 	=> 2203,
				'lp' 		=> 5005,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7032 => [//Footfix - IT
				'offer' 	=> 2080,
				'lp' 		=> 4846,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash' => '',
			],
			'reject'	=> [
				'reject' => '',
			],
			'expect'	=> [
				'expect' => '',
			],
			'confirm'	=> [
				'confirm' => '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://api-pp.offersify.io/lead',
	],
];

?>