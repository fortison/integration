<?php

/*
our postaback:

http://connectcpa.net/terraleads/adv/adv-postback-status?&systemOrderId={session_uuid}&partnerStatus={status}&systemUserId=44580&postbackApiKey=ed25dec931067514c182bc120cc7fee2

{session_uuid} - session_uuid in your system, here I transfer lead in our system
{status} - lead status ('trash', 'reject', 'expect', 'confirm')
*/
return [
	
	44580 => [
		'terraleadsApiKey' => 'ed25dec931067514c182bc120cc7feee',
		'postbackApiKey'   => 'ed25dec931067514c182bc120cc7fee2',
		
		'offers' => [
			8733 => [//Venicold Gel - CZ (low price)
				'offer_code' 	=> 'VGL1CZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8732 => [//Venicold Gel - IT (low price)
				'offer_code' 	=> 'VGL1IT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8731 => [//Venicold Gel - ES (low price)
				'offer_code' 	=> 'VGL1ES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8730 => [//Venicold Gel - EE (low price)
				'offer_code' 	=> 'VGL1EE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8729 => [//Venicold Gel - DE (low price)
				'offer_code' 	=> 'VGL1DE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8728 => [//Venicold Gel - AT (low price)
				'offer_code' 	=> 'VGL1AT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8739 => [//Hotrifen - CZ (low price)
				'offer_code' 	=> 'HTL1CZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8738 => [//Hotrifen - IT (low price)
				'offer_code' 	=> 'HTL1IT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8736 => [//Hotrifen - DE (low price)
				'offer_code' 	=> 'HTL1DE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8735 => [//Hotrifen - EE (low price)
				'offer_code' 	=> 'HTL1EE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8734 => [//Hotrifen - AT (low price)
				'offer_code' 	=> 'HTL1AT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8618 => [//Perfecto Cream - CZ (low price)
				'offer_code' 	=> 'PCL1CZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8617 => [//Perfecto Cream - ES (low price)
				'offer_code' 	=> 'PCL1ES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8616 => [//Perfecto Cream - IT (low price)
				'offer_code' 	=> 'PCL1IT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8615 => [//Perfecto Cream - EE (low price)
				'offer_code' 	=> 'PCL1EE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8614 => [//Perfecto Cream - DE (low price)
				'offer_code' 	=> 'PCL1DE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8613 => [//Perfecto Cream - AT (low price)
				'offer_code' 	=> 'PCL1AT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			
		],
		
		'tokens' => [
			'PL' => '8f844ffc-335d-4350-bd29-16bd99224c83',
			'CZ' => 'ec8e200c-52f6-4873-bcd8-e8a1797d3752',
			'IT' => '14eb20a5-e6ba-4af9-bbd2-b1d5c6f11bdb',
			'SK' => 'f363cf88-b2fe-4848-ab4a-ac6ff9095298',
			'DE' => '24ebf363-a4f9-4f9d-86f5-b36418dfc18f',
			'ES' => '089669a5-6700-4127-af46-430eb5eac8f6',
			'HR' => '687e7c9a-26a5-4d2f-98af-6c95836245d2',
			'HU' => '22316a02-32c6-4f26-afa2-a2c76b9cd8b7',
			'AT' => '5215d618-0ad5-4578-adbe-6082a9dbd21a',
			'PT' => '44bb2293-b5bb-41d3-a035-3aa03c13a9ef',
			'SI' => 'e6b5a393-8f1b-42e2-bc89-078d13bd8631',
			'RO' => '37fe5577-971e-47bb-97b5-0fa282904d7f',
			'FR' => '3a2e5e17-ab28-4acb-bf5b-3d99cd90c34d',
			'EE' => '1cbeee49-4e3c-4f48-a318-fc049a699b45',
		]
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'trash' => '',
			],
			'reject' => [
				'reject' => '',
			],
			'expect' => [
				'expect' => '',
			],
			'confirm' => [
				'confirm' => '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://api.nsbox.pl/api/v3/short_orders',
		'urlOrderInfo'		=> '',
	],
];

?>