<?php


return [

    32442 => [
        'terraleadsApiKey'			=> 'e2eaeb3dc59810a84d719f34a7a64d8c',
        'token' 					=> 'ef1dbe19-f95f-49af-9a6b-7bda75640bf1',

        'offers' => [
            4707 => [ //Artrofix - RS
                'eop'	=> 'eFZUeUt3PT0',

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4700 => [ //Kaloriko - RS
                'eop'   => 'eFZUMUxnPT0',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4702 => [ //Kaloriko - BA
                'eop'   => 'eFZUMUlRPT0',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4701 => [ //Kaloriko - ME
                'eop'   => 'eFZUeUtRPT0',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5008 => [ //Kaloriko - MK
                'eop'   => 'siTgYbAgSWtM3xqONShGNJG_bok',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4704 => [ //Kardio Komplex - BA
                'eop'   => 'eFZUeUtnPT0',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4938 => [ //Kardio Komplex - RS
                'eop'   => 'siTgYbAgSWtM3xqONShGNJG5a4U',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4703 => [ //Kardio Komplex - ME
                'eop'   => 'siTgYbAgSWtM3xqONShGNJG5bI8',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4706 => [ //Optimus Gel - BA
                'eop'   => 'eFZUeUxBPT0',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4705 => [ //Optimus Gel - ME
                'eop'   => 'siTgYbAgSWtM3xqONShGNJG6Z4k',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5517 => [ //Calorico Forte - ZA
                'eop'   => 'siTgYbAgSWtM3xqONShGNJG5ao0',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            7625 => [ //SlimQ5 - ZA
                'eop'   => 'siTgYbAgSWtM3xqONShGNJO5a4U',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
        ],
    ],

    'configs' => [
        'statuses' => [
            'trash'	=> [
                'trash'	=> '',
                'fake'  => '',
            ],
            'reject'	=> [
                'denied'	=> '',
            ],
            'expect'	=> [
                'pending' => '',
                'hold' => '',
            ],
            'confirm'	=> [
                'confirmed'	=> '',
            ],
        ],
        'brakeLogFolder'	=> true,
        'urlOrderAdd'		=> 'https://app.herberi.com/api/publishers/orders',
        'urlOrderInfo'		=> 'https://app.herberi.com/api/publishers/orders/',
    ],
];

?>