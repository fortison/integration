<?php

/*
	В чате

	Lead
	CREATE
	https://publisher.jingol.com/api/lead/create/?user=98685923&key=28b47628d965ab7b3dc714ddec57e2f0b013c6fbc213b5e5dec83c773d0f46ab
	POST
	{ "user_id":"", "data":{ "name":"", "country":"", "phone":"", "offer_id":"" } }
	
	
	{
		"id": "0",
		"status": "accept"
	}
	
	STATUS
	https://publisher.jingol.com/api/lead/status/?user=98685923&key=28b47628d965ab7b3dc714ddec57e2f0b013c6fbc213b5e5dec83c773d0f46ab
	POST
	{ "user_id":"", "data":{ "id":"" } }
	
	
	{
		"id": "0",
		"status": "accept|expect|confirm|fail|reject",
		"comment": ""
	}
 */

return [
	23916 => [
		'terraleadsApiKey'			=> '42338016562a0c16403b14315817dc7e',
		'user'						=> '98685923',
		'key'						=> '28b47628d965ab7b3dc714ddec57e2f0b013c6fbc213b5e5dec83c773d0f46ab',
		
		'offers' => [
						3086 => [
				'offer_id' => 'aa6f148cdbad',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3087 => [
				'offer_id' => 'a04560bf18c4',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3150 => [
				'offer_id' => 'ca07d28c95f1',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash' 		=> '',
				'fail' 		=> '',
			],
			'reject'	=> [
				'reject'    => '',
			],
			'expect'	=> [
				'expect'   		 => '',
				'accept'   		 => '',
			],
			'confirm'	=> [
				'confirm'    	=> '',
			],
		],
		
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://publisher.jingol.com/api/lead/create/',
		'urlOrderInfo'		=> 'https://publisher.jingol.com/api/lead/status/',
//		'statusEnabled'		=> false,
	],
];