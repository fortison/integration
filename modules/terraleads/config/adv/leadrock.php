<?php

/*
 * leadrock.com
 *
 * 51467
 * yehor@terraleads.com
 * T3rr@L3@ds
 *
 * 48881
 * kosta@terraleads.com
 * Terra_adv_01_2023
 *
 */

return [
	
	48881 => [
		'terraleadsApiKey' 	=> '4a898db379e2e3d17aec222f6b28fb99',
		'postbackApiKey' 	=> '002e827687a975a3c385287980308rpg',
		'secret' 			=> '4a898db379e2e3d17aec222f6b28fb99',
		'key' 				=> '22550',
		
		'offers' => [
			6613 => [ //D-Norm - MX
				'flow_url' => 'https://leadrock.com/URL-B5280-CFD9F',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8070 => [ //Probiomax - MX
				'flow_url' => 'https://leadrock.com/URL-B2B6D-24590',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8104 => [ //Vitacaps Detox - MX (low price)
				'flow_url' => 'https://leadrock.com/URL-C656D-DD431',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7489 => [ //D-Norm - MX (low price)
				'flow_url' => 'https://leadrock.com/URL-86D1E-C80A4',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6614 => [ //Intuslim - MX
				'flow_url' => 'https://leadrock.com/URL-6EFE9-53708',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6615 => [ //Fortikux - MX
				'flow_url' => 'https://leadrock.com/URL-502DC-B10A0',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6632 => [ //Alfaman - MX
				'flow_url' => 'https://leadrock.com/URL-2A0ED-039C5',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6636 => [ //D-Norm - PE
				'flow_url' => 'https://leadrock.com/URL-C6TTM-NFPTT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6635 => [ //Diastine - CO
				'flow_url' => 'https://leadrock.com/URL-MVEEL-YULX1',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6631 => [ //Keratinorm - MX
				'flow_url' => 'https://leadrock.com/URL-LSRVY-5CUXA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6630 => [ //Keratinorm - GT
				'flow_url' => 'https://leadrock.com/URL-24528-77984',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6638 => [ //Optiprost - CL
				'flow_url' => 'https://leadrock.com/URL-T2FBP-S7C8Y',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6633 => [ //Revitaprost - MX
				'flow_url' => 'https://leadrock.com/URL-0M9KJ-WGVU6',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6634 => [ //Volumin - MX
				'flow_url' => 'https://leadrock.com/URL-OU8JW-T5QOR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6637 => [ //Volumin - PE
				'flow_url' => 'https://leadrock.com/URL-0II2P-C6MTG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3701 => [ //Keto Light + - MX
				'flow_url' => 'https://leadrock.com/URL-BYQNY-WTTPG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4941 => [ //Ostelife - MX
				'flow_url' => 'https://leadrock.com/URL-Z43CO-SZNN0',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		]
	],
	
	51467 => [
		'terraleadsApiKey' 	=> 'f55c7e8b4a8a1d95920d85e648364a12',
		'postbackApiKey' 	=> '002e827687a975a3c385287980803rpg',
		'secret' 			=> 'e7F743yL6mIR36cYnv6mN8RNcKSNWWlx',
		'key' 				=> '22550',
		
		'offers' => [
			4941 => [ //Ostelife - MX
				'flow_url' => 'https://leadrock.com/URL-644A2-F4D36',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7291 => [ //Gluconax - MX
				'flow_url' => 'https://leadrock.com/URL-BA895-874AC',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7288 => [ //Cardio A - MX
				'flow_url' => 'https://leadrock.com/URL-E46BE-D2FE2',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7293 => [ //Keto Black - MX
				'flow_url' => 'https://leadrock.com/URL-F49F4-8BF4D',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7295 => [ //Keto Black - GT
				'flow_url' => 'https://leadrock.com/URL-D7639-154A4',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7290 => [ //Cardio A - GT
				'flow_url' => 'https://leadrock.com/URL-A5571-169A0',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7298 => [ //GlucoFree - GT
				'flow_url' => 'https://leadrock.com/URL-141F8-D92E7',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7289 => [ //Cardio A - CL
				'flow_url' => 'https://leadrock.com/URL-79118-9D486',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7292 => [ //Gluconax - CL
				'flow_url' => 'https://leadrock.com/URL-8428B-C3E62',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7294 => [ //Keto Black - CL
				'flow_url' => 'https://leadrock.com/URL-F6028-2D2B4',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7297 => [ //Prostanol - CL
				'flow_url' => 'https://leadrock.com/URL-B55F8-6F91F',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7296 => [ //Prostanol - MX
				'flow_url' => 'https://leadrock.com/URL-0DBF7-DB526',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3701 => [ //Keto Light + - MX
				'flow_url' => 'https://leadrock.com/URL-18993-EE304',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		]
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'trash' => '',
			],
			'reject' => [
				'reject' => '',
			],
			'expect' => [
				'expect' => '',
			],
			'confirm' => [
				'confirm' => '',
			],
		],
		'brakeLogFolder' => true,
		'urlOrderAdd' => 'https://leadrock.com/api/v2/lead/save',
	]
];

?>