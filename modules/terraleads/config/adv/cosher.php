<?php

/*
 * https://docs.google.com/document/d/1ijFGXNf-i_YoH_7_Eufhyy_W1yXhgELCsI2atrDRY3o/edit#
 */

return [
	
	16111 => [
		'terraleadsApiKey'			=> '3d907af49b6d73c9a9dd5704b97c65e1',
		'apiKey' 					=> 'api_mss71rxz8324f',
		
		'offers' => [
			2224 => [
				'article' => 'power_gel_es',
				'name' => 'Power Gel ES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				14		=> 'Трэш',
				15		=> 'Трэш',
			],
			'reject'	=> [
				13	=> 'Отклонен',
			],
			'expect'	=> [
				11	=> 'Перезвонить',
				12	=> 'Недозвон',
			],
			'confirm'	=> [
				10	=> 'Одобрен',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://cosher.space/api/v1/leads/add',
		'urlOrderInfo'		=> 'https://cosher.space/api/v1/leads/status',
	],
];

?>