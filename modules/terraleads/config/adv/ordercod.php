<?php

/**
 * Ordercod
 * док в документе
 */

return [
	20709 => [
		'terraleadsApiKey'	=> 'dfc324b8a1fd063f418425ece5e7d3c4',
		'ids_partner' 		=> '23',
		
		'offers' => [
			7160 => [ //Parasi Cleaner - KE
				'country_code' => 'KE',
				'product_code' => 'PSC_KE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7115 => [ //Diabex - PH
				'country_code' => 'PH',
				'product_code' => 'DIX_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7603 => [ //Haciba - PH
				'country_code' => 'PH',
				'product_code' => 'HBE_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9395 => [ //Haciba Cystitis - PH
				'country_code' => 'PH',
				'product_code' => 'HAC_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9394 => [ //Haciba Kidney Support - PH
				'country_code' => 'PH',
				'product_code' => 'HKS_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7460 => [ //CollagenAX - PH (free price)
				'country_code' => 'PH',
				'product_code' => 'CLA4_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7459 => [ //Fungonal - PH (free price)
				'country_code' => 'PH',
				'product_code' => 'FUN1_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7431 => [ //Coracio (vision) - MY
				'country_code' => 'MY',
				'product_code' => 'COV_MY',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7430 => [ //Coracio (diabetes) - MY
				'country_code' => 'MY',
				'product_code' => 'COD_MY',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7153 => [ //Mega Slim Body - AE
				'country_code' => 'AE',
				'product_code' => 'MSB1_AE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7152 => [ //Optifix - AE
				'country_code' => 'AE',
				'product_code' => 'OPF1_AE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7151 => [ //Diabextan - AE
				'country_code' => 'AE',
				'product_code' => 'DBX1_AE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7150 => [ //HeartKeep - AE
				'country_code' => 'AE',
				'product_code' => 'HKP1_AE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6098 => [ //Optifix Mega Pack - KE
				'country_code' => 'KE',
				'product_code' => 'OPF2_KE',
				'quantity' => '3',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6095 => [ //Vigorense Mega Pack - KE
				'country_code' => 'KE',
				'product_code' => 'VIG2_KE',
				'quantity' => '3',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6094 => [ //Diabextan Mega Pack - KE
				'country_code' => 'KE',
				'product_code' => 'DBX2_KE',
				'quantity' => '3',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6093 => [ //HeartKeep Mega Pack - KE
				'country_code' => 'KE',
				'product_code' => 'HKP2_KE',
				'quantity' => '3',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6097 => [ //Mega Slim Body Mega Pack - KE
				'country_code' => 'KE',
				'product_code' => 'MSB2_KE',
				'quantity' => '3',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6808 => [ //CCrystal Vision - PH
				'country_code' => 'PH',
				'product_code' => 'CRY_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6807 => [ //CDiabetin - PH
				'country_code' => 'PH',
				'product_code' => 'DIA_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6809 => [ //Cardio Tonus - PH 
				'country_code' => 'PH',
				'product_code' => 'CAT_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6810 => [ //Prostamexil - PH
				'country_code' => 'PH',
				'product_code' => 'PRM_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6805 => [ //ElecTrick - PH
				'country_code' => 'PH',
				'product_code' => 'ELE_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6806 => [ //ElecTrick - MY
				'country_code' => 'MY',
				'product_code' => 'ELE_MY',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6802 => [ //Corasio - MY
				'country_code' => 'MY',
				'product_code' => 'COR_MY',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5935 => [ //CollagenAX Mega Pack - MA
				'country_code' => 'MA',
				'product_code' => 'CLA2_MA',
				'quantity' => '3',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5934 => [ //Vigorense Mega Pack - MA
				'country_code' => 'MA',
				'product_code' => 'VIG2_MA',
				'quantity' => '3',

				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5933 => [ //Diabextan Mega Pack - MA
				'country_code' => 'MA',
				'product_code' => 'DBX2_MA',
				'quantity' => '3',				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5932 => [ //HeartKeep Mega Pack - MA
				'country_code' => 'MA',
				'product_code' => 'HKP2_MA',
				'quantity' => '3',				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5931 => [ //Mega Slim Body Mega Pack - MA
				'country_code' => 'MA',
				'product_code' => 'MSB2_MA',
				'quantity' => '3',				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5669 => [ //#4615 - Fit&Sleep - PH
				'country_code' => 'PH',
				'product_code' => 'FAS_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5670 => [ //Fungonal
				'country_code' => 'PH',
				'product_code' => 'FUN_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5671 => [ //Urixan Active
				'country_code' => 'PH',
				'product_code' => 'URI_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5672 => [ //;GlucoSoft 
				'country_code' => 'PH',
				'product_code' => 'GLU_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5673 => [ //OcurePlus - PH; 
				'country_code' => 'PH',
				'product_code' => 'OCU_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5674 => [ //Hardica - PH; 
				'country_code' => 'PH',
				'product_code' => 'HAR_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5675 => [ //MuscleArt
				'country_code' => 'PH',
				'product_code' => 'MSA_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],

			5692 => [ //
				'country_code' => 'MA',
				'product_code' => 'DBX_MA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5691 => [ //
				'country_code' => 'PH',
				'product_code' => 'MAM_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5690 => [ //
				'country_code' => 'KE',
				'product_code' => 'OPF_KE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5688 => [ //; 
				'country_code' => 'KE',
				'product_code' => 'DBX_KE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5234 => [ //BustaMax - PH; 
				'country_code' => 'PH',
				'product_code' => 'BUS_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4517 => [ //Slim&Go - PH; 
				'country_code' => 'PH',
				'product_code' => 'SNG_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4518 => [ //Magne Foot - MY
				'country_code' => 'MY',
				'product_code' => 'MGF_MY',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4504 => [ //Vigorense - ΜΑ
				'country_code' => 'MA',
				'product_code' => 'VIG_MA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2675 => [
				'country_code' => 'PH',
				'product_code' => 'HWS_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2673 => [
				'country_code' => 'PH',
				'product_code' => 'CKP_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2674 => [
				'country_code' => 'PH',
				'product_code' => 'WWT_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2728 => [
				'country_code' => 'PH',
				'product_code' => 'MGF_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2729 => [
				'country_code' => 'PH',
				'product_code' => 'BFP_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2752 => [
				'country_code' => 'PH',
				'product_code' => 'BSB_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2753 => [
				'country_code' => 'PH',
				'product_code' => 'PSC_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2754 => [
				'country_code' => 'PH',
				'product_code' => 'EPM_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2761 => [
				'country_code' => 'PH',
				'product_code' => 'HKP_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2801 => [
				'country_code' => 'PH',
				'product_code' => 'OTT_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2802 => [
				'country_code' => 'PH',
				'product_code' => 'PPA_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2804 => [
				'country_code' => 'PH',
				'product_code' => 'MRX_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2867 => [
				'country_code' => 'PH',
				'product_code' => 'CFB_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2868 => [
				'country_code' => 'PH',
				'product_code' => 'MSB_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2989 => [
				'country_code' => 'KE',
				'product_code' => 'VIG_KE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2990 => [
				'country_code' => 'KE',
				'product_code' => 'HKP_KE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3035 => [
				'country_code' => 'IT',
				'product_code' => 'AFM_IT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3036 => [
				'country_code' => 'ES',
				'product_code' => 'AFM_ES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3037 => [
				'country_code' => 'HU',
				'product_code' => 'AFM_HU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3038 => [
				'country_code' => 'CZ',
				'product_code' => 'AFM_CZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3039 => [
				'country_code' => 'RO',
				'product_code' => 'AFM_RO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3040 => [
				'country_code' => 'HR',
				'product_code' => 'AFM_HR',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3041 => [
				'country_code' => 'GR',
				'product_code' => 'AFM_GR',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3189 => [
				'country_code' => 'PH',
				'product_code' => 'VSS_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3361 => [
				'country_code' => 'PH',
				'product_code' => 'VIG_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3439 => [ //AirFree Mask - PH
				'country_code' => 'PH',
				'product_code' => 'AIR_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3663 => [ //Biklar Warming Spray - PH
				'country_code' => 'PH',
				'product_code' => 'BIK1_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3664 => [ //BBiklar Warming Cream - PH
				'country_code' => 'PH',
				'product_code' => 'BIK_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3842 => [ //Organic Teatox Tea - KE
				'country_code' => 'KE',
				'product_code' => 'OTT_KE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3841 => [ //Verrulon - PH
				'country_code' => 'PH',
				'product_code' => 'VER_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3825 => [ //HeartKeep - KH
				'country_code' => 'KH',
				'product_code' => 'HKP_KH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3826 => [ //Mega Slim Body - KH
				'country_code' => 'KH',
				'product_code' => 'MSB_KH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3827 => [ //Organic Teatox Tea - KH
				'country_code' => 'KH',
				'product_code' => 'OTT_KH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3828 => [ //Vigorense - KH
				'country_code' => 'KH',
				'product_code' => 'VIG_KH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3019 => [ //Mega Slim Body - KE
				'country_code' => 'KE',
				'product_code' => 'MSB_KE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3971 => [ //Madoran - PH
				'country_code' => 'PH',
				'product_code' => 'MAD_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3979 => [ //Organic Teatox Tea - MA
				'country_code' => 'MA',
				'product_code' => 'OTT_MA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4037 => [ //Heart Keep - MA
				'country_code' => 'MA',
				'product_code' => 'HKP_MA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4036 => [ //Mega Slim Body - MA
				'country_code' => 'MA',
				'product_code' => 'MSB_MA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4028 => [ //Optifix - PH
				'country_code' => 'PH',
				'product_code' => 'OPF_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4029 => [ //Vitalbiotix - PH
				'country_code' => 'PH',
				'product_code' => 'VTB_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4182 => [ //DermaTea - PH
				'country_code' => 'PH',
				'product_code' => 'DRT_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4233 => [ //HeartKeep - MY
				'country_code' => 'MY',
				'product_code' => 'HKP_MY',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4378 => [ //Mega Slim Body - MY
				'country_code' => 'MY',
				'product_code' => 'MSB_MY',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4368 => [ //CollagenAX - MY
				'country_code' => 'MY',
				'product_code' => 'CLA_MY',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4376 => [ //Parasi Cleaner - PH
				'country_code' => 'PH',
				'product_code' => 'PSC_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4375 => [ //Aurifix - PH
				'country_code' => 'PH',
				'product_code' => 'AUR_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4373 => [ //CollagenAX - KE
				'country_code' => 'KE',
				'product_code' => 'CLA_KE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4383 => [ //Aurifix - KE
				'country_code' => 'KE',
				'product_code' => 'AUR_KE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4371 => [ //Aurifix - MA
				'country_code' => 'MA',
				'product_code' => 'AUR_MA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4377 => [ //CollagenAX - MA
				'country_code' => 'MA',
				'product_code' => 'CLA_MA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4407 => [ //Diabextan - PH
				'country_code' => 'PH',
				'product_code' => 'DBX_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4406 => [ //EasySleep - PH
				'country_code' => 'PH',
				'product_code' => 'ESL_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4382 => [ //CollagenAX - PH
				'country_code' => 'PH',
				'product_code' => 'CLA_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4486 => [ //EasySleep - MY
				'country_code' => 'MY',
				'product_code' => 'ESL_MY',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4488 => [ //NanoWash - MY
				'country_code' => 'MY',
				'product_code' => 'NNW_MY',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5054 => [ //HeltaHair
				'country_code' => 'PH',
				'product_code' => 'HLH_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5053 => [ //Teafy Tea 
				'country_code' => 'PH',
				'product_code' => 'TTS_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4866 => [ //Liverotox
				'country_code' => 'PH',
				'product_code' => 'LTX_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4865 => [ //PulmoTea
				'country_code' => 'PH',
				'product_code' => 'PUL_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4864 => [ //Nevrotin 1000
				'country_code' => 'PH',
				'product_code' => 'NEV_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4822 => [ //LungActive
				'country_code' => 'PH',
				'product_code' => 'LUN_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4821 => [ //Vascolex
				'country_code' => 'PH',
				'product_code' => 'VAS_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4810 => [ //Sleepzy
				'country_code' => 'PH',
				'product_code' => 'SLE_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4809 => [ //Liveromax
				'country_code' => 'PH',
				'product_code' => 'LIV_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5053 => [ //Teafy Tea
				'country_code' => 'PH',
				'product_code' => 'TTS_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5054 => [ //HeltaHair
				'country_code' => 'PH',
				'product_code' => 'HLH_PH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash'	=> [
					'4'	=> 'Not Valid',
				],
				'reject'	=> [
					'3'	=> 'Rejected',
				],
				'expect'	=> [
					'0'	=> 'Pending',
                    '1' => 'For Phone',
				],
				'confirm'	=> [
					'2'	=> 'Approved',
				],
			],
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'http://ordercod.com:8080/OrderSystem/resources/crud/save_order_info_imp',
			'urlOrderInfo'		=> 'http://ordercod.com:8080/OrderSystem/resources/report/report_partner_state',
		]
	]
];

?>