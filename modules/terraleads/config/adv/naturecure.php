<?php
return [
    27025 => [
        'terraleadsApiKey'			=> '2035f34187bf0241984561b4be08c604',
        'postbackApiKey'			=> '2035f34187bf0241984561b4be08c605',

        'offers' => [
            4236 => [ # Cure Maxx - IN
                'source' => 'terraleads',
                'campaign' => 'Cure Maxx',

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4239 => [ # Spirulina Powder - IN
                'source' => 'terraleads',
                'campaign' => 'Spirulina Powder',

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4240 => [ # Hairvedic Shampoo - IN
                'source' => 'terraleads',
                'campaign' => 'Hairvedic Shampoo',

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
        ],
    ],

    'configs' => [
        'statuses' => [
            'trash'	=> [
                'trash'		=> '',
            ],
            'reject'	=> [
                'reject'	=> '',
            ],
            'expect'	=> [
                'expect'	=> '',
            ],
            'confirm'	=> [
                'confirm'	=> '',
            ],
        ],
        'brakeLogFolder'	=> true,
        'urlOrderAdd'		=> 'https://api.naturecure.nutracrm.com/v1/',
        'urlOrderInfo'		=> '',
    ],
];

?>