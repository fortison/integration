<?php

/*
Фото в файле

1. http://connectcpa.net/terraleads/adv/adv-postback-status?systemUserId=21509&partnerStatus=expect&systemOrderId={offer_id}&{subid1}&status_of_order={status}&status_of_target={target}&postbackApiKey=0322s82768005a3c3287a57819978eda

2. http://connectcpa.net/terraleads/adv/adv-postback-status?systemUserId=21509&partnerStatus=confirm&systemOrderId={offer_id}&{subid1}&status_of_order={status}&status_of_target={target}&postbackApiKey=0322s82768005a3c3287a57819978eda

3. http://connectcpa.net/terraleads/adv/adv-postback-status?systemUserId=21509&partnerStatus=reject&systemOrderId={offer_id}&{subid1}&status_of_order={status}&status_of_target={target}&postbackApiKey=0322s82768005a3c3287a57819978eda

4. http://connectcpa.net/terraleads/adv/adv-postback-status?systemUserId=21509&partnerStatus=trash&systemOrderId={offer_id}&{subid1}&status_of_order={status}&status_of_target={target}&postbackApiKey=0322s82768005a3c3287a57819978eda

 */

return [
	21509 => [
		'terraleadsApiKey'			=> '2b603cb3d9414ddd97141be05e039eff',
		'postbackApiKey'			=> '0322s82768005a3c3287a57819978eda',
		'token' 					=> 'b6dbba9fa479f72f63820f02cc3565d5',
		
		'offers' => [
			6796 => [ //Massage Pillow - MX
				'flowkey' => 'qqJJElPG',
				'geoiso'  => 'MX',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6793 => [ //Zumba Hairstyler - MX
				'flowkey' => 'LJifHEVe',
				'geoiso'  => 'MX',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6791 => [ //Massage Pen - MX
				'flowkey' => 'KcEezvLT',
				'geoiso'  => 'MX',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6792 => [ //T9 Vintage Shaver - MX
				'flowkey' => 'HQh3yZbd',
				'geoiso'  => 'MX',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6795 => [ //Minicamera - MX
				'flowkey' => 'hkiqtMJn',
				'geoiso'  => 'MX',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6799 => [ //Baellerry Business - MX
				'flowkey' => 'OJm7Dv8q',
				'geoiso'  => 'MX',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6798 => [ //Swiss Army Watch - MX
				'flowkey' => 'YYSmJepA',
				'geoiso'  => 'MX',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6794 => [ //Portable Wi-Fi Camera - MX
				'flowkey' => 'txIQhsCv',
				'geoiso'  => 'MX',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6757 => [ //Soki Lumi Watch - MX
				'flowkey' => 'XDAfbERy',
				'geoiso'  => 'MX',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6253 => [ //Spin Brush - SA
				'flowkey' => 'HDpbvL5u',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6258 => [ //Telescope - SA
				'flowkey' => 'jlwbdh3s',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6254 => [ //Lest Slim - SA
				'flowkey' => '7VPUOLfL',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6256 => [ //Bino Foot massager - SA
				'flowkey' => '1W6cMZNG',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6255 => [ //Pico Projector - KW
				'flowkey' => 'OWzs1Dxr',
				'geoiso'  => 'KW',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6257 => [ //Screen Magnifier - OM
				'flowkey' => 'Xr4kCQKx',
				'geoiso'  => 'OM',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6259 => [ //Telescope - KE
				'flowkey' => '354WkVx7',
				'geoiso'  => 'KE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6263 => [ //Nano Tape - SA
				'flowkey' => 'fX39zDaV',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6261 => [ //Baellerry Business - QA
				'flowkey' => 'yRMEmRK1',
				'geoiso'  => 'QA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6260 => [ //Swiss Army Watch - SA
				'flowkey' => 'NIn8bYau',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6262 => [ //Pest Reject - JO
				'flowkey' => 'x7c7LgQF',
				'geoiso'  => 'JO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6410 => [ //Miracle Cleaner - SA
				'flowkey' => 'wHcxvgkU',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6223 => [ //Swiss Army Watch - QA
				'flowkey' => 'r2MaKBN2',
				'geoiso'  => 'QA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6222 => [ //Bestwin Watch - SA
				'flowkey' => 'Ecx6uzGz',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6221 => [ //Car Curtain - SA
				'flowkey' => '6lvhIGfu',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6220 => [ //Impulse Booster - SA
				'flowkey' => 'GjF9geky',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6219 => [ //Screen Magnifier - SA
				'flowkey' => 'd752Rkku',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5404 => [ //Yes Finishing Touch - SA
				'flowkey' => '4EyPCJ0i',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6088 => [ //Super Cleaner - KW
				'flowkey' => 'XEhWQxu1',
				'geoiso'  => 'KW',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5974 => [ //Swiss Army Watch - KW
				'flowkey' => 'rZLhOuUu',
				'geoiso'  => 'KW',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5395 => [ //Super Cleaner - SA
				'flowkey' => 'SQCIqCjR',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5973 => [ //Diamond Watch - KW
				'flowkey' => 'BiDfEKFm',
				'geoiso'  => 'KW',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5656 => [ //Minicamera - OM
				'flowkey' => 'hEs1cl3x',
				'geoiso'  => 'OM',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5403 => [ //Air Dragon - SA
				'flowkey' => 'Rn2oTo1P',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5653 => [
				'flowkey' => 'tQX4ay2C',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5652 => [
				'flowkey' => 'BH9Lw9vw',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5654 => [
				'flowkey' => '8Zou2MIM',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5390 => [ // 
				'flowkey' => 'rj6BIsUC',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5655 => [
				'flowkey' => 'Jhnnv3FJ',
				'geoiso'  => 'KW',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5656 => [
				'flowkey' => 'XPTbMwxB',
				'geoiso'  => 'OM',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2785 => [ //Baellerry Smart wallet - KE
				'flowkey' => 'tDRpGXhn',
				'geoiso'  => 'KE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2784 => [
				'flowkey' => 'ojMckhsB',
				'geoiso'  => 'NG',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2786 => [ // Binary Watch - JO
				'flowkey' => 'DnkBZfcZ',
				'geoiso'  => 'JO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2789 => [
				'flowkey' => '9midOhhD',
				'geoiso'  => 'KE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2787 => [
				'flowkey' => '9midOhhD',
				'geoiso'  => 'JO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2788 => [
				'flowkey' => 'NYxoPSGU',
				'geoiso'  => 'NG',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2813 => [
				'flowkey' => 'Uu8dSz77',
				'geoiso'  => 'NG',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2814 => [
				'flowkey' => 'eqzxmtvr',
				'geoiso'  => 'KE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2795 => [
				'flowkey' => 'iZ4rXAXo',
				'geoiso'  => 'AE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2793 => [
				'flowkey' => 'x51RaHQA',
				'geoiso'  => 'OM',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2822 => [
				'flowkey' => 'g6pRuY2m',
				'geoiso'  => 'OM',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2823 => [
				'flowkey' => 'EYipt74z',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2791 => [
				'flowkey' => 'jPreoi50',
				'geoiso'  => 'OM',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2882 => [
				'flowkey' => 'UxmqvWmk',
				'geoiso'  => 'EG',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2886 => [
				'flowkey' => 'wbxels0a',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2887 => [
				'flowkey' => 'OMhv0Xul',
				'geoiso'  => 'QA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
   			2812 => [
				'flowkey' => 'HVF2gB2u',
				'geoiso'  => 'JO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
   			2992 => [
				'flowkey' => 'g7wUjS76',
				'geoiso'  => 'KW',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
   			3072 => [
				'flowkey' => 'uJqKI3HJ',
				'geoiso'  => 'AE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
   			3073 => [
				'flowkey' => 'iA8gSVNe',
				'geoiso'  => 'OM',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
   			3057 => [
				'flowkey' => 'ysUoHHbd',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
   			3058 => [
				'flowkey' => 'Aywop4nA',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
   			2792 => [ //Baellerry Smart wallet - JO
				'flowkey' => 'TkJwzKT3',
				'geoiso'  => 'JO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3426 => [ //Baellerry Business - SA
				'flowkey' => 'XK9X6BSM',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3428 => [ //Scar Cream - SA
				'flowkey' => '1UtP4MK9',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3427 => [ //Mymi Patch - EG
				'flowkey' => 'PqDaPT3Q',
				'geoiso'  => 'EG',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3438 => [ // Pore Cleanser - JO
				'flowkey' => 'ew1ZRU0O',
				'geoiso'  => 'JO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3437 => [ //Pore Cleanser - AE
				'flowkey' => 'ew1ZRU0O',
				'geoiso'  => 'AE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3436 => [ //Baellerry Business - EG
				'flowkey' => 'AAPdIK9I',
				'geoiso'  => 'EG',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3491 => [ //Magnet Lashes - EG
				'flowkey' => 'q6iAS2x6',
				'geoiso'  => 'EG',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3492 => [ //Magnet Lashes - BH
				'flowkey' => 'q6iAS2x6',
				'geoiso'  => 'BH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3493 => [ //Magnet Lashes - JO
				'flowkey' => 'q6iAS2x6',
				'geoiso'  => 'JO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3490 => [ //3D Face and Body Massage Roller - SA
				'flowkey' => 'EhF2AQcS',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3489 => [ //3D Face and Body Massage Roller - EG
				'flowkey' => 'EhF2AQcS',
				'geoiso'  => 'EG',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3489 => [ //3D Face and Body Massage Roller - EG
				'flowkey' => 'EhF2AQcS',
				'geoiso'  => 'EG',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3494 => [ //Eyebrow Enhancer - SA
				'flowkey' => 'i5x23kRt',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3497 => [ //Slimming Tea - AE
				'flowkey' => 'DWQdp11x',
				'geoiso'  => 'AE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3498 => [ //Slimming Tea - OM
				'flowkey' => 'DWQdp11x',
				'geoiso'  => 'OM',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3942 => [ //Styler Wet 2 Dry - JO
				'flowkey' => 'l6xB58Eh',
				'geoiso'  => 'JO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3940 => [ //Sport Car Watch - KE
				'flowkey' => 'Nfp0EwtY',
				'geoiso'  => 'KE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3941 => [ //Steel Rage - KE
				'flowkey' => 'yqYYpkaD',
				'geoiso'  => 'KE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3727 => [ //Posture Support - EG
				'flowkey' => 'kW9j3IJV',
				'geoiso'  => 'EG',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4019 => [ //Scar Cream - AE
				'flowkey' => 'HeBuo8hE',
				'geoiso'  => 'AE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4249 => [ //Air Dragon - HU
				'flowkey' => 'tolTuvew',
				'geoiso'  => 'HU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4248 => [ //Air Dragon - RO
				'flowkey' => 'tolTuvew',
				'geoiso'  => 'RO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4360 => [ //Tiger Wrench - RO
				'flowkey' => 'BG969brt',
				'geoiso'  => 'RO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5401 => [ //TLoshall Scar Removal Peel Mask - AE
				'flowkey' => 'mlkI5fTE',
				'geoiso'  => 'AE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5400 => [ //Bushnell - NG
				'flowkey' => 'y1NSm58s',
				'geoiso'  => 'NG',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5057 => [ //Minicamera
				'flowkey' => 'vweq5Qx1',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5398 => [ //Massage Pillow
				'flowkey' => 'dwqUqZvc',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5061 => [ //Riddex
				'flowkey' => 'YPTszxzo',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5396 => [ //Smart Mini Wallet
				'flowkey' => 'ZYQoAUs5',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5399 => [ //Red Copper
				'flowkey' => 'thH8Jx9Z',
				'geoiso'  => 'SA',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash' 			=> '',
			],
			'reject'	=> [
				'reject'	        => '',
			],
			'expect'	=> [
				'expect'			=> '',
			],
			'confirm'	=> [
				'confirm'	        => '',
			],
		],
		
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'http://postback.advertfish.com/v1/api/order-create',
//		'statusEnabled'		=> false,
	],
];