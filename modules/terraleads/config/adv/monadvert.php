<?php
/*
 * https://monadvert.scaleo.app/
 * Login: anna@terraleads.com
 * Pass: WelcomeAnna
 */

return [
	18365 => [
		'terraleadsApiKey'			=> 'e142bb82b7d1623c9ec6c269a57e0356',
		'postbackApiKey'			=> 'e142bb82b7d1623c9ec6c269a57e0311',
		'api_key' 					=> '590aa68b5493c193ba12d22e1d3b0a81662beefb',
		'affiliate_id'				=> 307,
		
		'offers' => [
			6896 => [ // Diet Enzyme - MY 
				'offer_id' => 125,
				'goal_id'  => 375,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6504 => [ // Superman Tablet - SG 
				'offer_id' => 110,
				'goal_id'  => 330,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6505 => [ // Superman Tablet - MY 
				'offer_id' => 109,
				'goal_id'  => 327,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6506 => [ // Superman Tablet - TW 
				'offer_id' => 108,
				'goal_id'  => 324,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6507 => [ // Breast Enlargement Tea - SG 
				'offer_id' => 106,
				'goal_id'  => 318,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6508 => [ // Breast Enlargement Tea - MY 
				'offer_id' => 107,
				'goal_id'  => 321,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6509 => [ // Breast Enlargement Tea - TW 
				'offer_id' => 105,
				'goal_id'  => 315,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6331 => [ // Sex Enhance Ball - SG 
				'offer_id' => 100,
				'goal_id'  => 300,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6332 => [ // Sex Enhance Ball - MY 
				'offer_id' => 101,
				'goal_id'  => 303,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6330 => [ // Sex Enhance Ball - TW 
				'offer_id' => 99,
				'goal_id'  => 297,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6201 => [ // Sex Essence Tablet - SG 
				'offer_id' => 87,
				'goal_id'  => 261,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6200 => [ // Sex Essence Tablet - MY 
				'offer_id' => 88,
				'goal_id'  => 264,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6202 => [ // Sex Essence Tablet - TW 
				'offer_id' => 'essencetablet-TL-1398TWD',
				'goal_id'  => 255,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6053 => [ // Lotus Slim Tea - SG 
				'offer_id' => 'lotuslim-TL-69SGD',
				'goal_id'  => 249,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6052 => [ // Lotus Slim Tea - MY 
				'offer_id' => 'lotuslim-TL-199RM',
				'goal_id'  => 252,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6054 => [ // Lotus Slim Tea - TW 
				'offer_id' => 'lotuslim-TL-1398TWD',
				'goal_id'  => 243,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5834 => [ // Super Sex Ball - SG 
				'offer_id' => 'supersexball-TL-69SGD',
				'goal_id'  => 117,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5832 => [ // Super Sex Ball - MY 
				'offer_id' => 'supersexball-TL-199RM',
				'goal_id'  => 204,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5833 => [ // Super Sex Ball - TW 
				'offer_id' => 'supersexball-TL-1398TWD',
				'goal_id'  => 111,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5915 => [ // Super Sex Ball - HK 
				'offer_id' => 'supersexball-TL-368HKD',
				'goal_id'  => 114,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5747 => [ // Slimming Tablet - MY 
				'offer_id' => 'Slimtablet-TL-199RM',
				'goal_id'  => 240,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5746 => [ // Slimming Tablet - TW 
				'offer_id' => 'Slimtablet-TL-1398TWD',
				'goal_id'  => 231,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5745 => [ // Bitter Melon Pills - MY 
				'offer_id' => 'Bittermelonpills-TL-179RM',
				'goal_id'  => 216,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5744 => [ // Bitter Melon Pills - TW 
				'offer_id' => 'Bittermelonpills-TL-1098TWD',
				'goal_id'  => 207,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5742 => [ // Barley Green Powder - MY 
				'offer_id' => 'barleygreen-TL-179RM',
				'goal_id'  => 228,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5743 => [ // Barley Green Powder - TW 
				'offer_id' => 'barleygreen-TL-1098TWD',
				'goal_id'  => 219,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5460 => [ // Slim Probiotics - MY 
				'offer_id' => 'slimprobiotics-TL-179RM',
				'goal_id'  => 201,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5387 => [ // Slim Probiotics - TW 
				'offer_id' => 'slimprobiotics-TL-1098TWD',
				'goal_id'  => 147,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5459 => [ // Slim Herb - MY 
				'offer_id' => 'slimherb-TL-199RM',
				'goal_id'  => 180,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5352 => [ // Slim Herb - TW 
				'offer_id' => 'slimherb-TL-1398TWD',
				'goal_id'  => 138,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5353 => [ // Slim Herb - HK 
				'offer_id' => 'slimherb-TL-368HKD',
				'goal_id'  => 141,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5087 => [ // Slimming Tea - SG 
				'offer_id' => 'Slimming-TL-69SGD',
				'goal_id'  => 63,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5457 => [ // Slimming Tea - MY 
				'offer_id' => 'slimtea-TL-199RM',
				'goal_id'  => 189,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3813 => [ // Slimming Tea - TW 
				'offer_id' => 'Slimming-TL-1398-TW',
				'goal_id'  => 57,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5414 => [ // Lubian Geinseng Ball - SG 
				'offer_id' => 'ginsengball-TL-69SGD',
				'goal_id'  => 171,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5461 => [ // Lubian Geinseng Ball - MY 
				'offer_id' => 'ginsengball-TL-199RM',
				'goal_id'  => 174,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5413 => [ // Lubian Geinseng Ball - TW 
				'offer_id' => 'ginsengball-TL-1398TWD',
				'goal_id'  => 165,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5455 => [ // Pennis Booster Ball - MY 
				'offer_id' => 'BoosterBall-TL-239RM',
				'goal_id'  => 183,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4818 => [ // Pennis Booster Ball - TW
				'offer_id' => 'boosterBall-TL-1598TWD',
				'goal_id'  => 3,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5093 => [ // Erection Ball - SG 
				'offer_id' => 'Erectionball-TL-69SGD',
				'goal_id'  => 90,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5454 => [ // Erection Ball - MY
				'offer_id' => 'Erection-TL-199RM',
				'goal_id'  => 195,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4564 => [ // Erection Ball - TW
				'offer_id' => 'Erectionball-TL-1398TWD',
				'goal_id'  => 15,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5088 => [ // LubianBall - SG
				'offer_id' => 'LubianBall-TL-69SGD',
				'goal_id'  => 93,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4063 => [ // LubianBall - MY
				'offer_id' => 'LubianBall-TL-199RM',
				'goal_id'  => 177,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3734 => [ // LubianBall - TW
				'offer_id' => 'LubianBall-TL-1398-TW',
				'goal_id'  => 33,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3946 => [ // Lubian Cream - TW
				'offer_id' => 'LubianCream-TL-1398-TW',
				'goal_id'  => 27,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4062 => [ // Lubian Cream - MY
				'offer_id' => 'LubianCream-TL-199RM',
				'goal_id'  => 186,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'trash' => '',
			],
			'reject' => [
				'reject' => '',
			],
			'expect' => [
				'expect' => '',
			],
			'confirm' => [
				'confirm' => '',
			],
		],
		
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://monadvert.scaletrk.com/api/v2/affiliate/leads',
//		'statusEnabled'		=> false,
	],
];