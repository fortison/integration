<?php

return [
	
	30177 => [
		'terraleadsApiKey'			=> 'c63519e556478fb5c1d59608fb7f5b36',
		'api_key' 					=> '322650e1328739dbca646008305dd95e',
		
		'offers' => [
			6362 => [ // DeepHair 1+1 - SA
				'product' => 185943, //ID продукта
				'utm_term' => 'puaaeuc45', //поток КЦ
				'payout' => 22, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6361 => [ // Anti-Wrinkle Moisturizing Serum 1+1 - SA
				'product' => 185944, //ID продукта
				'utm_term' => 'h0mab496z', //поток КЦ
				'payout' => 30, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6360 => [ // RED Pain Relief Ge 1+1 - SA
				'product' => 185942, //ID продукта
				'utm_term' => '6vg481p', //поток КЦ
				'payout' => 30, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6612 => [ // Maca Root (prostatitis) - SA
				'product' => 190238, //ID продукта
				'utm_term' => '5jy6hr', //поток КЦ
				'payout' => 22, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6537 => [ // Maca Root - SA
				'product' => 186454, //ID продукта
				'utm_term' => '7nx6jauam', //поток КЦ
				'payout' => 22, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6288 => [ // Levi (potency) - DZ
				'product' => 179669, //ID продукта
				'utm_term' => 'nhdtsfrq', //поток КЦ
				'payout' => 8, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5921 => [ // Neo Pain Relief - DZ
				'product' => 177970, //ID продукта
				'utm_term' => '24yzw9pdr', //поток КЦ
				'payout' => 8, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5840 => [ // Levi red (potency) - SA
				'product' => 181404, //ID продукта
				'utm_term' => 'q2zws40cu', //поток КЦ
				'payout' => 18, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5645 => [ // Roomie Pro - SA
				'product' => 174960, //ID продукта
				'utm_term' => '031mpq5ahp', //поток КЦ
				'payout' => 15, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5637 => [ // Roomie Pro - KW
				'product' => 176528, //ID продукта
				'utm_term' => '0trhhkk0', //поток КЦ
				'payout' => 16, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5636 => [ // Roomie Pro - QA
				'product' => 176527, //ID продукта
				'utm_term' => 'j57xhprxe', //поток КЦ
				'payout' => 22, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5521 => [ // Flawless - QA
				'product' => 161262, //ID продукта
				'utm_term' => 'xc05dyj', //поток КЦ
				'payout' => 13, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5544 => [ // Pest Reject CPL - KW
				'product' => 173024, //ID продукта
				'utm_term' => 'ufft4t7z', //поток КЦ
				'payout' => 12, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5484 => [ // Knee Knight - KW
				'product' => 166155, //ID продукта
				'utm_term' => 'a96yb', //поток КЦ
				'payout' => 18, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5465 => [ // DeepHair - SA
				'product' => 178793, //ID продукта
				'utm_term' => '04z4tr', //поток КЦ
				'payout' => 22, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5471 => [ // Pest Reject 1+1 - KW
				'product' => 173024, //ID продукта
				'utm_term' => 'ufft4t7z', //поток КЦ
				'payout' => 17, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5472 => [ // Pest Reject 1+1 - QA
				'product' => 173025, //ID продукта
				'utm_term' => 't0tk2mu', //поток КЦ
				'payout' => 22, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5194 => [ // Pain Relief - SA
				'product' => 175294, //ID продукта
				'utm_term' => '7dtvkf', //поток КЦ
				'payout' => 20, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5157 => [ // BearD - KW
				'product' => 170489, //ID продукта
				'utm_term' => 'f0f4hw22', //поток КЦ
				'payout' => 14, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5349 => [ // Levi (weightloss) - SA
				'product' => 178045, //ID продукта
				'utm_term' => '5bwzdb', //поток КЦ
				'payout' => 18, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5348 => [ // Levi (parasites) - SA
				'product' => 172597, //ID продукта
				'utm_term' => 'b0z0pcv', //поток КЦ
				'payout' => 18, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5572 => [ // Levi (testosterone) - SA
				'product' => 178045, //ID продукта
				'utm_term' => '5bwzdb', //поток КЦ
				'payout' => 18, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5564 => [ // Levi (potency) - SA
				'product' => 178046, //ID продукта
				'utm_term' => '988tuuw', //поток КЦ
				'payout' => 18, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5563 => [ // Levi (vision) - SA
				'product' => 179049, //ID продукта
				'utm_term' => '6n1k16eq', //поток КЦ
				'payout' => 18, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5156 => [ // Lush Up CC - KW
				'product' => 170493, //ID продукта
				'utm_term' => 'a0tw1ww7w', //поток КЦ
				'payout' => 15, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5155 => [ // MAXremover - KW
				'product' => 170490, //ID продукта
				'utm_term' => 'kft05hfj', //поток КЦ
				'payout' => 15, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5154 => [ // Smart Watch X - KW
				'product' => 166157, //ID продукта
				'utm_term' => '9ymnu9n86e', //поток КЦ
				'payout' => 18, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4568 => [ // Anti-Wrinkle Moisturizing Serum - KW
				'product' => 170914, //ID продукта
				'utm_term' => '3e70a3j', //поток КЦ
				'payout' => 16, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5011 => [ // PowerFit Pro - KW
				'product' => 178475, //ID продукта
				'utm_term' => '2cq2w1dwwj', //поток КЦ
				'payout' => 20, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5010 => [ // PowerFit Pro - QA
				'product' => 178474, //ID продукта
				'utm_term' => 'xpn1f1w', //поток КЦ
				'payout' => 20, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5009 => [ // Pest Reject 1+1 - SA
				'product' => 160636, //ID продукта
				'utm_term' => 'qh23qe', //поток КЦ
				'payout' => 16, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4989 => [ // RED Pain Relief Gel - NG
				'product' => 173709, //ID продукта
				'utm_term' => 'gu2tvkvcc', //поток КЦ
				'payout' => 9, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4990 => [ // Weight control - NG 
				'product' => 173708, //ID продукта
				'utm_term' => 'wqy1yfsn0', //поток КЦ
				'payout' => 9, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4985 => [ // BearD - NG
				'product' => 170040, //ID продукта
				'utm_term' => 'xa3dnemaq', //поток КЦ
				'payout' => 9, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4984 => [ // MAXremover - NG
				'product' => 170041, //ID продукта
				'utm_term' => 'bnbrxd', //поток КЦ
				'payout' => 8, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4983 => [ // Lush Up CC - NG
				'product' => 170037, //ID продукта
				'utm_term' => ' ra9gr842', //поток КЦ
				'payout' => 10, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4956 => [ // RED Pain Relief Gel - SA
				'product' => 179588, //ID продукта
				'utm_term' => 'hc8b09d', //поток КЦ
				'payout' => 27, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4958 => [ // MAXremover - SA
				'product' => 171233, //ID продукта
				'utm_term' => '7gsvg5un2', //поток КЦ
				'payout' => 20, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4959 => [ // Lush Up CC - SA
				'product' => 170918, //ID продукта
				'utm_term' => 'vqbws', //поток КЦ
				'payout' => 20, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4957 => [ // Anti-Wrinkle Moisturizing Serum - SA
				'product' => 179589, //ID продукта
				'utm_term' => 'q3jgaf', //поток КЦ
				'payout' => 25, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4546 => [ // Anti-hair loss - KW
				'product' => 170284, //ID продукта
				'utm_term' => 'yr8mxgy7h', //поток КЦ
				'payout' => 20, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4184 => [ // Microtouch Solo - SA
				'product' => 163449, //ID продукта
				'utm_term' => 'ddwtwk647', //поток КЦ
				'payout' => 10, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4185 => [ // Pest Reject - SA
				'product' => 163450, //ID продукта
				'utm_term' => 'wd6v06q', //поток КЦ
				'payout' => 14, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4502 => [ // 111Miracle Cooper - SA
				'product' => 157835, //ID продукта
				'utm_term' => 'sr03d52n', //поток КЦ
				'payout' => 15, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4503 => [ // Magnet Fix - SA
				'product' => 155635, //ID продукта
				'utm_term' => 'xywnsd8', //поток КЦ
				'payout' => 15, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4595 => [ // Knee Knight - SA
				'product' => 161563, //ID продукта
				'utm_term' => '1y8xhh', //поток КЦ
				'payout' => 15, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4397 => [ // Miracle Cooper - QA
				'product' => 161265, //ID продукта
				'utm_term' => 'upbd5n7ped', //поток КЦ
				'payout' => 16, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
            4396 => [ // Pest Reject - QA
				'product' => 165468, //ID продукта
				'utm_term' => 'csxq5ty3', //поток КЦ
				'payout' => 20, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
            4395 => [ // Magnet Fix - QA
				'product' => 154558, //ID продукта
				'utm_term' => 'wfemsa4', //поток КЦ
				'payout' => 20, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4631 => [ //Knee Knight - QA
				'product' => 165346, //ID продукта
				'utm_term' => '72gdj7', //поток КЦ
				'payout' => 18, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4630 => [ // Night Vision Glasses - QA
				'product' => 162765, //ID продукта
				'utm_term' => 's1y4phfg', //поток КЦ
				'payout' => 18, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4507 => [ // Magnet Fix - OM
				'product' => 150890, //ID продукта
				'utm_term' => 'dtq8uhr5x', //поток КЦ
				'payout' => 12, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4508 => [ // Pest Reject - OM
				'product' => 164871, //ID продукта
				'utm_term' => 'wwyuw2b', //поток КЦ
				'payout' => 12, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4510 => [ // Night Vision Glasses - OM
				'product' => 166114, //ID продукта
				'utm_term' => 'bpjx3v', //поток КЦ
				'payout' => 12, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4632 => [ // Knee Knight - OM
				'product' => 165520, //ID продукта
				'utm_term' => 'n4d7ff68p', //поток КЦ
				'payout' => 12, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4613 => [ // Pest Reject - KW
				'product' => 158113, //ID продукта
				'utm_term' => '0mhw7jgba', //поток КЦ
				'payout' => 21, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4611 => [ // Magnet Fix - KW
				'product' => 158115, //ID продукта
				'utm_term' => '9v48a9', //поток КЦ
				'payout' => 20, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4609 => [ // Miracle Cooper - KW
				'product' => 163774, //ID продукта
				'utm_term' => '6eutzbh6c', //поток КЦ
				'payout' => 18, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4607 => [ // Night Vision Glasses - KW
				'product' => 163771, //ID продукта
				'utm_term' => 'u8ug51y94v', //поток КЦ
				'payout' => 22, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4608 => [ // Night Vision Glasses - AE
				'product' => 165725, //ID продукта
				'utm_term' => 'fp3p4u', //поток КЦ
				'payout' => 19, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4612 => [ // Pest Reject - AE
				'product' => 162677, //ID продукта
				'utm_term' => 'r3hbzx7b', //поток КЦ
				'payout' => 20, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4610 => [ // Miracle Cooper - AE
				'product' => 167563, //ID продукта
				'utm_term' => 'y2ehekftn', //поток КЦ
				'payout' => 20, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4606 => [ // Magnet Fix - BH
				'product' => 158113, //ID продукта
				'utm_term' => '0mhw7jgba', //поток КЦ
				'payout' => 17, //выплата веба
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				9		=> 'Трэш',
				13		=> 'Test',
			],
			'reject'	=> [
				2	=> 'Отклонен',
			],
			'expect'	=> [
				0	=> 'Обработка',
			],
			'confirm'	=> [
				1	=> 'Одобрен',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://submitter.tech/leadvertex.php',
		'urlOrderInfo'		=> 'https://submitter.tech/partners/status.php',
	],
];

?>