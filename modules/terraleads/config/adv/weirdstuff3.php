<?php

return [

    14402 => [
        'terraleadsApiKey'			=> '658dd83652253a7b36b9c69676183976',
        'api_key' 					=> 'gqfoaLhdrvzdqlSh2vfCHfTF0zrj36jKtgllKeWZBTNldg6BtF',

        'offers' => [
            2236 => [ //Longjack XXXL - NG
                'product' => 'longjack-xxxl', //поток КЦ

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            2210 => [ //Express Fat Burner Premium - NG
                'product' => 'express-fat-burner', //поток КЦ

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4323 => [ //Normatone - NG
                'product' => 'normatone', //поток КЦ

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            2963 => [ //Alpha Beast - NG
                'product' => 'alpha-beast', //поток КЦ

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            2174 => [ //Ultra Clean Premium Detox - NG
                'product' => 'ultraclean-detox', //поток КЦ

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
        ],
    ],

    'configs' => [
        'statuses' => [
            'trash'	=> [
                'trash' => '',
            ],
            'reject'	=> [
                'rejected'	=> '',
            ],
            'expect'	=> [
                'hold'	=> '',
            ],
            'confirm'	=> [
                'approved'	=> '',
            ],
        ],
        'brakeLogFolder'	=> true,
        'urlOrderAdd'		=> 'https://crm-nigeria.weirdstuff.io/api/order-create',
        'urlOrderInfo'		=> 'https://crm-nigeria.weirdstuff.io/api/order-status',
    ],
];
