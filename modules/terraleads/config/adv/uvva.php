<?php

/*
 *	Учетка создана:
 *	https://my.uvva.club/
 *	terraleads@uvva.club
 *	L2SbLbA2m7
 *
 *	http://connectcpa.net/terraleads/adv/adv-postback-status?postbackApiKey=4a6401b80d5f359b5910a36f99fa2081&systemUserId=41492&systemOrderId={utm_source}&partnerStatus={status}&comment={comment}
 */

return [
	41492 => [
		'terraleadsApiKey' => '4a6401b80d5f359b5910a36f99fa208b',
		'postbackApiKey' => '4a6401b80d5f359b5910a36f99fa2081',
		'token' => 'aoFpA2KXcNv814bDTN6zqh7-u8mmDRYx',
		
		'offers' => [
			8722 => [ //Bee's power - GE
				'offer_id' => '65',
				'lead_information' => 'cardio-zdorov',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8278 => [ //Erectil - GE
				'offer_id' => '65',
				'lead_information' => 'erectil',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7651 => [ //Zdorov Intimate Cream - IR
				'offer_id' => '65',
				'lead_information' => 'intim_iran',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7905 => [ //Auto Cleaner - GE
				'offer_id' => '65',
				'lead_information' => 'avto-cleaner',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7903 => [ //Polyroll - GE
				'offer_id' => '65',
				'lead_information' => 'polyrol_ge',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7902 => [ //Protective Foil - GE
				'offer_id' => '65',
				'lead_information' => 'folga-kitchen',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7901 => [ //Sani Clean - GE
				'offer_id' => '65',
				'lead_information' => 'sani-clean',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7900 => [ //Street Lamp - GE
				'offer_id' => '65',
				'lead_information' => 'sun_lamp',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7899 => [ //Tuz Cushion - GE
				'offer_id' => '65',
				'lead_information' => 'tuz',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7898 => [ //Woodclean - GE
				'offer_id' => '65',
				'lead_information' => 'woodclean',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7441 => [ //Zdorov Hemorrhoids Gel - IR
				'offer_id' => '65',
				'lead_information' => 'gemoroy_irn',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7061 => [ //Diabex - GE
				'offer_id' => '65',
				'lead_information' => 'diabex',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7062 => [ //Cardio NRJ - GE
				'offer_id' => '65',
				'lead_information' => 'cardio',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7063 => [ //Ostelife - GE
				'offer_id' => '65',
				'lead_information' => 'osterlife',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7064 => [ //BGermixil - GE
				'offer_id' => '65',
				'lead_information' => 'germixil',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7065 => [ //Alfagen - GE
				'offer_id' => '65',
				'lead_information' => 'alfagen',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6939 => [ //Bee Power Cream - GE
				'offer_id' => '65',
				'lead_information' => 'sustavi',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6328 => [ //Bushnell - AM
				'offer_id' => '65',
				'lead_information' => 'monocular-am',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6327 => [ //SaniClean - AM
				'offer_id' => '65',
				'lead_information' => 'sani-clean_am',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6326 => [ //Plastic Restore - AM
				'offer_id' => '65',
				'lead_information' => 'plastic-recovery_am',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6092 => [ //Zdorov Hemorrhoids Gel - GE
				'offer_id' => '65',
				'lead_information' => 'gemoroy_ge',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6091 => [ //Zdorov Prostate Gel - GE
				'offer_id' => '65',
				'lead_information' => 'krem_prost',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5822 => [ //Zdorov Intimate Cream - GE
				'offer_id' => '65',
				'lead_information' => 'intim_light_ge',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5821 => [ //Zdorov Joints Cream - GE
				'offer_id' => '65',
				'lead_information' => 'sustavi',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5820 => [ //Chopper - GE
				'offer_id' => '65',
				'lead_information' => 'chopper',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5819 => [ //Tactical Glasses - GE
				'offer_id' => '65',
				'lead_information' => 'tactical-glasses',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5818 => [ //Spine - GE
				'offer_id' => '65',
				'lead_information' => 'spine',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5817 => [ //ZOstio - GE
				'offer_id' => '65',
				'lead_information' => 'ostio',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5816 => [ //Nicerdicerplus - GE
				'offer_id' => '65',
				'lead_information' => 'nicerdicerplus',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5815 => [ //ZMonocular Watch - GE
				'offer_id' => '65',
				'lead_information' => 'monocular-watch',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5814 => [ //ZLapsherezka - GE
				'offer_id' => '65',
				'lead_information' => 'lapsherezka',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5813 => [ //Espander - GE
				'offer_id' => '65',
				'lead_information' => 'espander',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5812 => [ //Crossbody - GE
				'offer_id' => '65',
				'lead_information' => 'crossbody',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'7' => 'STATUS_CRM_INVALID',
				'50' => 'STATUS_TRASH',
				'-1' => 'STATUS_DELETED',
				'-10' => 'STATUS_DUPLICATE',
			],
			'reject' => [
				'3' => 'STATUS_CRM_CANCEL',
			],
			'expect' => [
				'0' => 'STATUS_NEW',
				'1' => 'STATUS_CRM',
				'2' => 'STATUS_CRM_RECALL',
			],
			'confirm' => [
				'5' => 'STATUS_CRM_APPROVED',
				'10' => 'STATUS_CRM_TO_DELIVERY',
				'15' => 'STATUS_DELIVERY',
				'17' => 'STATUS_DELIVERY_CANCEL',
				'20' => 'STATUS_COMPLETE',
			],
		],
		
		'brakeLogFolder' => true,
		'urlOrderAdd' => 'https://api.uvva.club/lead/create',
//		'statusEnabled'		=> false,
	],
];