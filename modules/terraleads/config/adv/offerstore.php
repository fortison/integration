<?php

/*
 * Документация по подключению: https://offer.store/help/api.php#ext
 * API токен: 1135-9071ff6b1d3dd60b308b337a9c4cdafd
 * Ссылка для входа: https://offer.store/?recoverpass=46f99243eeef38117205d29577b2db2edee15414bcc5e3006455707ba0f6bfa55bf
 * URL отправки: https://api.offer.store/ext/add.json?id=1135-9071ff6b1d3dd60b308b337a9c4cdafd
 * URL проверки: https://api.offer.store/ext/list.json?id=1135-9071ff6b1d3dd60b308b337a9c4cdafd&ids={ids}
*/

return [
	
	51655 => [
		'terraleadsApiKey'			=> 'd225581222f8379e092a6f9aabf5e52c',
		'token' 					=> '1135-9071ff6b1d3dd60b308b337a9c4cdafd',
		
		
		'offers' => [
			9336 => [//Liluama - PE
				'offer'	=> 787,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9313 => [//Papaya cleanse (liver) - CO
				'offer'	=> 783,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9312 => [//Papaya cleanse (gastriris) - CO
				'offer'	=> 782,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9311 => [//Papaya cleanse - CO
				'offer'	=> 781,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9310 => [//Flexacil - CO
				'offer'	=> 780,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9309 => [//Bururan (prostatitis) - CO
				'offer'	=> 790,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9308 => [//Bururan (potency) - CO
				'offer'	=> 725,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9307 => [//Furoma - CO
				'offer'	=> 726,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9306 => [//Kettochi - CO
				'offer'	=> 727,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8812 => [//Hairex - PE
				'offer'	=> 325,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8718 => [//Zeaxan - CL
				'offer'	=> 515,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8717 => [//Zeaxan - PE
				'offer'	=> 119,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8716 => [//Testostirol - PE
				'offer'	=> 210,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8715 => [//Glyconorm - CL
				'offer'	=> 550,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6998 => [//Glyconorm - PE
				'offer'	=> 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8713 => [//Gialuron - PE
				'offer'	=> 9,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6996 => [//Alphaman - PE
				'offer'	=> 302,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7935 => [//Papaya Cleanse - CL
				'offer'	=> 514,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6997 => [//Papaya Cleanse - PE1
				'offer'	=> 88,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7016 => [//Flexacil - PE
				'offer'	=> 5,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7455 => [//TurboSlim - CL
				'offer'	=> 486,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7454 => [//TurboSlim - PE
				'offer'	=> 486,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7349 => [//Slim Fit - CL
				'offer'	=> 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7348 => [//Slim Fit - PE
				'offer'	=> 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7016 => [//Flexacil - PE
				'offer'	=> 5,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7000 => [//Cardiox - PE
				'offer'	=> 3,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6999 => [//Cardiox - CL
				'offer'	=> 3,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7014 => [//Bioprost (potency) - PE
				'offer'	=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7015 => [//Bioprost (potency) - CL
				'offer'	=> 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7013 => [//Bioprost (prostatitis) - CL
				'offer'	=> 7,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7012 => [//Bioprost (prostatitis) - PE
				'offer'	=> 7,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'		=> 'треш',
			],
			'reject'	=> [
				'cancel'	=> 'заказ отменён',
			],
			'expect'	=> [
				'wait'		=> 'заказ в обработке',
				'hold'		=> 'холд',
			],
			'confirm'	=> [
				'approve'	=> 'заказ принят',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://api.offer.store/ext/add.json',
		'urlOrderInfo'		=> 'https://api.offer.store/ext/list.json',
	],
];

?>