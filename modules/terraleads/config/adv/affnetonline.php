<?php

/**
 * For integration:

API endpoint Terraleads: https://affnetonline.com/api/v1/order/create/
Methode: GET
Parameters:
net:   1900 (should be fixed for Terraleads)
offerid: 71005
fid:  Affiliate ID
clickid: Click ID
name: Order name
phone: Order phone
apikey - hFaEWwwUuTfYGKurwFtypdJgAM (should be fixed for Terraleads)

Exemple
affnetonline.com/api/v1/order/create/?net=1700&fid=1684684&clickid=154684684684&offerid=&name=name1&phone=0641684864&apikey=hFaEWwwUuTfYGKurwFtypdJgAM

Please inform us when you perform tests.
 */

return [
	49966 => [
		'terraleadsApiKey'			=> 'd6de85670de0a1fc5f2734dd6bfaa0e8',
		'postbackApiKey'			=> 'd6de85670de0a1fc5f2734dd6bfaa0f8',
		'apikey'					=> 'hFaEWwwUuTfYGKurwFtypdJgAM',
		'net'						=> 1900,
		
		'offers' => [
			7650 => [//Keraplant - EG
				'offerid' => 71010,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6826 => [
				'offerid' => 71005,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2323 => [
				'offerid' => 71005,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'trash' => '',
			],
			'reject' => [
				'rejected' => '',
				'reject' => '',
			],
			'expect' => [
				'new' => '',
			],
			'confirm' => [
				'approved' => '',
				'confirm' => '',
			],
		],
		
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://affnetonline.com/api/v1/order/create',
		'urlOrderInfo'		=> '',
	],
];