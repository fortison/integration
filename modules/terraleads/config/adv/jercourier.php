<?php

return [
	27068 => [
		'terraleadsApiKey' 	=> 'e9bf987475b33efe934431714e11b751',
		'postbackApiKey'	=> 'e9bf987475b33efe934431714e11b752',
		
		'offers' => [
			3662 => [
				'api_key' 	=> '2b3bd16645bf7876856a061845d54f8e',
				'offer_id' 	=> 'P001|CALENTRAS',
				
				'configs' => [
					'urlOrderAdd' 		=> 'https://jercourier.sigella.net:10600/sigella/documentos/query_20200131081747.php',
					'brakeLogFolder' 	=> true,
				],
			],
			3669 => [
				'api_key' 	=> '2b3bd16645bf7876856a061845d54f8e',
				'offer_id' 	=> 'P001|CALENTRAS',
				
				'configs' => [
					'urlOrderAdd' 		=> 'https://romanux.sigella.net:10600/sigella/documentos/query_20200131081747.php',
					'brakeLogFolder' 	=> true,
				],
			],
			3746 => [
				'api_key' 	=> '2b3bd16645bf7876856a061845d54f8e',
				'offer_id' 	=> 'P000|PARAFRIX',
				
				'configs' => [
					'urlOrderAdd' 		=> 'https://jercourier.sigella.net:10600/sigella/documentos/query_20200131081747.php',
					'brakeLogFolder' 	=> true,
				],
			],
			3745 => [
				'api_key' 	=> '2b3bd16645bf7876856a061845d54f8e',
				'offer_id' 	=> 'P000|PARAFRIX',
				
				'configs' => [
					'urlOrderAdd' 		=> 'https://romanux.sigella.net:10600/sigella/documentos/query_20200131081747.php',
					'brakeLogFolder' 	=> true,
				],
			],
			3947 => [ //Parafrix - MX
				'api_key' 	=> '2b3bd16645bf7876856a061845d54f8e',
				'offer_id' 	=> 'P009|PARAFRIX',
				
				'configs' => [
					'urlOrderAdd' 		=> 'https://tradextray.sigella.net:10600/tradextray/documentos/query_20220819173954.php',
					'brakeLogFolder' 	=> true,
				],
			],
			3768 => [ //Calentras - MX
				'api_key' 	=> '2b3bd16645bf7876856a061845d54f8e',
				'offer_id' 	=> 'P009|CALENTRAS',
				
				'configs' => [
					'urlOrderAdd' 		=> 'https://tradextray.sigella.net:10600/tradextray/documentos/query_20220819173954.php',
					'brakeLogFolder' 	=> true,
				],
			],
			3769 => [ //Hialuronika - MX
				'api_key' 	=> '2b3bd16645bf7876856a061845d54f8e',
				'offer_id' 	=> 'P002|HIALURONIKA',
				
				'configs' => [
					'urlOrderAdd' 		=> 'https://tradextray.sigella.net:10600/tradextray/documentos/query_20220819173954.php',
					'brakeLogFolder' 	=> true,
				],
			],
			6961 => [ //Xplendia - MX
				'api_key' 	=> '2b3bd16645bf7876856a061845d54f8e',
				'offer_id' 	=> 'P013|XPLENDIA',
				
				'configs' => [
					'urlOrderAdd' 		=> 'https://tradextray.sigella.net:10600/tradextray/documentos/query_20220819173954.php',
					'brakeLogFolder' 	=> true,
				],
			],
			6615 => [ //Fortikux - MX
				'api_key' 	=> '2b3bd16645bf7876856a061845d54f8e',
				'offer_id' 	=> 'P007|FORTIKUX',
				
				'configs' => [
					'urlOrderAdd' 		=> 'https://tradextray.sigella.net:10600/tradextray/documentos/query_20220819173954.php',
					'brakeLogFolder' 	=> true,
				],
			],
			4027 => [ //ForteSbelt - CO
				'api_key' 	=> '2b3bd16645bf7876856a061845d54f8e',
				'offer_id' 	=> 'P004|FORTESBELT',
				
				'configs' => [
					'urlOrderAdd' 		=> 'https://romanux.sigella.net:10600/sigella/documentos/query_20200131081747.php',
					'brakeLogFolder' 	=> true,
				],
			],
			3770 => [ //Hialuronika - CO
				'api_key' 	=> '2b3bd16645bf7876856a061845d54f8e',
				'offer_id' 	=> 'P004|HIALURONIKA',
				
				'configs' => [
					'urlOrderAdd' 		=> 'https://romanux.sigella.net:10600/sigella/documentos/query_20200131081747.php',
					'brakeLogFolder' 	=> true,
				],
			],
			3771 => [ //Hialuronika - PE
				'api_key' 	=> '2b3bd16645bf7876856a061845d54f8e',
				'offer_id' 	=> 'P002|HIALURONIKA',
				
				'configs' => [
					'urlOrderAdd' 		=> 'https://jercourier.sigella.net:10600/sigella/documentos/query_20200131081747.php',
					'brakeLogFolder' 	=> true,
				],
			],
			5839 => [ //Hialuronika - CR
				'api_key' 	=> '2b3bd16645bf7876856a061845d54f8e',
				'offer_id' 	=> 'P002, HIALURONIKA',
				
				'configs' => [
					'urlOrderAdd' 		=> 'https://tradextray.sigella.net:10600/tradextray/documentos/query_20220317105241.php',
					'brakeLogFolder' 	=> true,
				],
			],
			6825 => [ //Teznova Znova - CR
				'api_key' 	=> '2b3bd16645bf7876856a061845d54f8e',
				'offer_id' 	=> 'P006, TEZNOVA',
				
				'configs' => [
					'urlOrderAdd' 		=> 'https://tradextray.sigella.net:10600/tradextray/documentos/query_20220317105241.php',
					'brakeLogFolder' 	=> true,
				],
			],
			6929 => [ //HIALURONIKA - PA
				'api_key' 	=> '2b3bd16645bf7876856a061845d54f8e',
				'offer_id' 	=> 'P002|HIALURONIKA',
				
				'configs' => [
					'urlOrderAdd' 		=> 'https://tradextray.sigella.net:10600/tradextray/documentos/query_20220405165834.php',
					'brakeLogFolder' 	=> true,
				],
			],
			4417 => [
				'api_key' 	=> '22f31dfa93c18df45638f28d8c2a1',
				'offer_id' 	=> 'P001|SISALE',
				
				'configs' => [
					'urlOrderAdd' 		=> 'https://pilarescaribe.sigella.net:10600/pilarescaribe/documentos/query_20210514113812.php',
					'brakeLogFolder' 	=> true,
				],
			],
			6077 => [ //Novadermika Novadelika Set - MX
				'api_key' 	=> '22f31dfa93c18df45638f28d8c2a1',
				'offer_id' 	=> 'P004|NOVADERMIKA',
				
				'configs' => [
					'urlOrderAdd' 		=> 'https://pilarescaribe.sigella.net:10600/pilarescaribe/documentos/query_20210514113812.php',
					'brakeLogFolder' 	=> true,
				],
			],
		],
	],
	'configs' => [
		'statuses' => [
			'trash' => [
				'trash' => '',
			],
			'reject' => [
				'reject' => '',
			],
			'expect' => [
				'expect' => '',
			],
			'confirm' => [
				'confirm' => '',
			],
		],
		'brakeLogFolder' 	=> true,
		'urlOrderAdd' 		=> '',
		'urlOrderInfo' 		=> '',
	],
];

?>