<?php

/**
 * http://oae.lp-crm.biz/help_api
 *
 *
 * Login: tera
 * Pass: 987654321
 * http://oae.lp-crm.biz/
 */

return [
	23301 => [
		'terraleadsApiKey'			=> '63c0475eed1f67c2788e4c0594010e45',
		'key' 						=> '79883cdc437fd93379bcfe2cef07275f',
		'office'					=> 14, //Отдел (офис), созданный в CRM (id)
		
		'offers' => [
			3363 => [
				'product_id' => 470,
				'product_price' => 139,
				'product_landing' => 'http://multi-hair-comb-ae.beauty-goods.biz/',
				
				'additional_1' => 'Multifunctional Hair Comb', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3362 => [
				'product_id' => 425,
				'product_price' => 129,
				'product_landing' => 'http://ae-m-massagepillow.beauty-goods.biz/',
				
				'additional_1' => 'Massage pillow', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3364 => [
				'product_id' => 430,
				'product_price' => 129,
				'product_landing' => 'http://ae-vcleanspot.beauty-goods.biz/',
				
				'additional_1' => 'v clean spot (buy 1 get 1 free) 20 pcs set', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3365 => [
				'product_id' => 429,
				'product_price' => 139,
				'product_landing' => 'http://magic-brush-set-ae.beauty-goods.biz/',
				
				'additional_1' => 'Magic brush+10 Pcs v clean spot', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3366 => [
				'product_id' => 428,
				'product_price' => 149,
				'product_landing' => 'http://ae-collagencream.beauty-goods.biz/',
				
				'additional_1' => 'Collagen cream', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3415 => [
				'product_id' => 488,
				'product_price' => 149,
				'product_landing' => 'http://goods-sale.com/steam-brush/',
				
				'additional_1' => 'Steam brush', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3416 => [
				'product_id' => 487,
				'product_price' => 299,
				'product_landing' => 'http://goods-sale.com/skin-wand-tl/',
				
				'additional_1' => 'LED Skin Tightening 5 in 1', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3415 => [
				'product_id' => 488,
				'product_price' => 149,
				'product_landing' => 'http://ae-steambrush.beauty-goods.biz/',
				
				'additional_1' => 'Steam brush', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3416 => [
				'product_id' => 487,
				'product_price' => 299,
				'product_landing' => 'http://ae-skintightening.beauty-goods.biz/',
				
				'additional_1' => 'LED Skin Tightening 5 in 1', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3424 => [
				'product_id' => 516,
				'product_price' => 18,
				'product_landing' => 'http://v-shaped-face-mask-om.beauty-goods.org/',
				
				'additional_1' => 'V-SHAPED FACE MASK', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3476 => [ //Vclean Spot - OM
				'product_id' => 543,
				'product_price' => 16,
				'product_landing' => 'http://om-m-vcleanspot.beauty-goods.biz/',
				
				'additional_1' => 'Vclean Spot', //название офера (на английском)
				
				'additional_2' => function ($model, $config) { // цену(просто цифрами)
					return $model->landing_cost;
				},
				
				'additional_3' => function ($model, $config) {
					return $model->country;
				},
				
				'additional_4' => function ($model, $config) {
					return $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		],
		'configs' => [
			'statuses' => [
				'trash'	=> [
					'63'	=> 'Not Valid не валид',
				],
				'reject'	=> [
					'58'	=> 'Rejected Berd( отмена)',
					'13'	=> 'Отказ',
				],
				'expect'	=> [
					'3'		=> 'new',
					'11'	=> 'Принято',
					'64'	=> 'Pending',
					'77'	=> 'Pending',
					'73'	=> 'Pending',
					'66'	=> 'Pending',
					'67'	=> 'Pending',
					'68'	=> 'Pending',
					'69'	=> 'Pending',
					'75'	=> 'Pending',
					'76'	=> 'Pending',
				],
				'confirm'	=> [
					'57'	=> 'Not paid(подтвержен не выкуп)',
					'60'	=> 'Waiting for delivery( подтвержен)',
					'61'	=> 'Delivery in progress ( подтвержден,в процессе доставки- апрув)',
					'65'	=> 'SСHEDULED ORDERS подтвержен',
					'31'	=> 'Возврат товара (склад)',
					'70'	=> 'Waiting For Delivery Oman',
					'71'	=> 'Delivery in progress OMAN',
					'62'	=> 'Sucsess Delivery ',
					'72'	=> 'Suscess OMAN',
					'74'	=> 'Late delivery Call',
					'50'	=> 'Утилизация',
					'53'	=> 'Выплата CRT',
					'55'	=> 'Выплата Berd',
					'20'	=> 'Возврат товара (в пути)',
				],
			],
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'http://oae.lp-crm.biz/api/addNewOrder.html\'',
			'urlOrderInfo'		=> 'http://oae.lp-crm.biz/api/getOrdersByID.html',
		],
	],
];

?>