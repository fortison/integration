<?php

/*
 * https://docs.google.com/document/d/15MoyXlydKplgAm1sTM5P47IR8C27Hhmh7MOtDh8Q8l4/mobilebasic
 */

return [
	
	21065 => [
		'terraleadsApiKey'			=> '2840e49648a38e444293ad406e9ef3f3',
		'api_key' 					=> '2ee77b422540f4466b4f60b3f4ca17d0',
		
		'offers' => [
			6980 => [ //Magnetic Hearts (test) - RO
				'goods_id' => 2555,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6982 => [ //Car Towel (test) - RO
				'goods_id' => 2593,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7024 => [ //Cup Holder Car (test) - RO
				'goods_id' => 2614,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7025 => [ //Faucet Extender (test) - RO
				'goods_id' => 2624,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7026 => [ //Show Clean Wipes (test) - RO
				'goods_id' => 2628,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7027 => [ //CleaningKit7in1 (test) - RO
				'goods_id' => 2572,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7028 => [ //Ultra Matte Lipstick (test) - RO
				'goods_id' => 2579,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6983 => [ //Crocodile Lighter - RO
				'goods_id' => 2518,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6981 => [ //Anti-Rain Agent - RO
				'goods_id' => 2486,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6985 => [ //Car Gradient Sticker - BG
				'goods_id' => 2564,
				'country_id' => 1081,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6984 => [ //Car Gradient Sticker - RO
				'goods_id' => 2564,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6986 => [ //Brow Stamp - RO
				'goods_id' => 2528,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6967 => [ //Wifi Camera 360 - IT
				'goods_id' => 2284,
				'country_id' => 1801,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6966 => [ //Wifi Camera 360 - BG
				'goods_id' => 2284,
				'country_id' => 1081,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6965 => [ //Wifi Camera 360 - RO
				'goods_id' => 2284,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6709 => [ //Cushion VENZEN - RO (low price)
				'goods_id' => 2424,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6698 => [ //Waist trainer - RO
				'goods_id' => 2011,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5190 => [ //3D Smart Magnifier - BG
				'goods_id' => 2077,
				'country_id' => 1081,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5189 => [ //3D Smart Magnifier - RO
				'goods_id' => 2077,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6414 => [ //Flame Heater - RO
				'goods_id' => 2156,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6415 => [ //Handy heater - BG
				'goods_id' => 2156,
				'country_id' => 1081,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6416 => [ //Handy Heater - CZ
				'goods_id' => 2156,
				'country_id' => 1109,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5347 => [ //Handy Heater - SK
				'goods_id' => 2156,
				'country_id' => 1300,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6418 => [ //Flame Heater - HU
				'goods_id' => 2156,
				'country_id' => 1500,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6419 => [ //Flame Heater - IT
				'goods_id' => 2156,
				'country_id' => 1801,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6286 => [ //Cushion VENZEN - BG
				'goods_id' => 2424,
				'country_id' => 1081,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6287 => [ //Cushion VENZEN - RO
				'goods_id' => 2424,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6237 => [ //Auto Logo Light - RO
				'goods_id' => 2444,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5543 => [ //Mini Sealer - RO
				'goods_id' => 2431,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5467 => [ //HD Smart Camera - RO
				'goods_id' => 2245,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2755 => [
				'goods_id' => 2147,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2233 => [
				'goods_id' => 2095,
				'country_id' => 1081,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2279 => [
				'goods_id' => 2095,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2286 => [
				'goods_id' => 2095,
				'country_id' => 1801,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2285 => [
				'goods_id' => 2095,
				'country_id' => 1109,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2231 => [
				'goods_id' => 2061,
				'country_id' => 1081,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2256 => [
				'goods_id' => 2061,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2258 => [
				'goods_id' => 2061,
				'country_id' => 1109,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2853 => [
				'goods_id' => 2079,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2854 => [
				'goods_id' => 2079,
				'country_id' => 1081,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2855 => [
				'goods_id' => 2079,
				'country_id' => 1109,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2856 => [
				'goods_id' => 2079,
				'country_id' => 1300,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2857 => [
				'goods_id' => 2079,
				'country_id' => 1801,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2871 => [
				'goods_id' => 2141,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2872 => [
				'goods_id' => 2141,
				'country_id' => 1081,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2888 => [
				'goods_id' => 2069,
				'country_id' => 1081,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2889 => [
				'goods_id' => 2069,
				'country_id' => 1109,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2890 => [
				'goods_id' => 2069,
				'country_id' => 1801,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2891 => [
				'goods_id' => 2069,
				'country_id' => 1300,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2892 => [
				'goods_id' => 2069,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2893 => [
				'goods_id' => 2052,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2894 => [
				'goods_id' => 2052,
				'country_id' => 1081,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2895 => [
				'goods_id' => 2052,
				'country_id' => 1808,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2896 => [
				'goods_id' => 2052,
				'country_id' => 1500,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2897 => [
				'goods_id' => 2052,
				'country_id' => 1109,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2898 => [
				'goods_id' => 2052,
				'country_id' => 1300,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2899 => [
				'goods_id' => 2052,
				'country_id' => 1801,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3094 => [
				'goods_id' => 2141,
				'country_id' => 1300,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3095 => [
				'goods_id' => 2141,
				'country_id' => 1109,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2235 => [
				'goods_id' => 2125,
				'country_id' => 1081,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			3096 => [
				'goods_id' => 2125,
				'country_id' => 1300,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			3097 => [
				'goods_id' => 2125,
				'country_id' => 1109,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3107 => [
				'goods_id' => 2095,
				'country_id' => 1300,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3108 => [
				'goods_id' => 2095,
				'country_id' => 1804,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3127 => [
				'goods_id' => 2190,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3128 => [
				'goods_id' => 2190,
				'country_id' => 1109,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3122 => [
				'goods_id' => 2151,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3123 => [
				'goods_id' => 2151,
				'country_id' => 1081,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3124 => [
				'goods_id' => 2151,
				'country_id' => 1109,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3125 => [
				'goods_id' => 2151,
				'country_id' => 1300,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3126 => [
				'goods_id' => 2151,
				'country_id' => 1801,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3121 => [
				'goods_id' => 2069,
				'country_id' => 1804,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3340 => [
				'goods_id' => 2199,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3341 => [
				'goods_id' => 2190,
				'country_id' => 1801,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3339 => [
				'goods_id' => 2208,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3397 => [
				'goods_id' => 2208,
				'country_id' => 1801,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3398 => [
				'goods_id' => 2208,
				'country_id' => 1109,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3421 => [ //Lazy Holder - RO
				'goods_id' => 2219,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3465 => [ //Dazzling White - RO
				'goods_id' => 2224,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3486 => [ //Fitness Bracelet M5 - RO	
				'goods_id' => 2229,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
// 		UV Sterilizer - RO BG CZ SK IT
			3481 => [ //UV Sterilizer - RO	
				'goods_id' => 2139,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3482 => [ //UV Sterilizer - BG	
				'goods_id' => 2139,
				'country_id' => 1081,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3483 => [ //UV Sterilizer - CZ	
				'goods_id' => 2139,
				'country_id' => 1109,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3484 => [ //UV Sterilizer - SK	
				'goods_id' => 2139,
				'country_id' => 1300,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3485 => [ //UV Sterilizer - IT	
				'goods_id' => 2139,
				'country_id' => 1801,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3949 => [ //Steam Brush - IT
				'goods_id' => 2055,
				'country_id' => 1801,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4170 => [ //Heart Tonic - HU
				'goods_id' => 2042,
				'country_id' => 1500,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4172 => [ //Heart Tonic - BG
				'goods_id' => 2042,
				'country_id' => 1081,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],		
			4173 => [ //Heart Tonic - RO
				'goods_id' => 2042,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4758 => [ //Tactic Pants - HU
				'goods_id' => 2128,
				'country_id' => 1500,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4757 => [ //Tactic Pants - IT
				'goods_id' => 2128,
				'country_id' => 1801,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4756 => [ //Tactic Pants - SK
				'goods_id' => 2128,
				'country_id' => 1300,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4755 => [ //Tactic Pants - CZ
				'goods_id' => 2128,
				'country_id' => 1109,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4754 => [ //Tactic Pants - BG
				'goods_id' => 2128,
				'country_id' => 1081,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4753 => [ //Tactic Pants - RO
				'goods_id' => 2128,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4752 => [ //Military Suit - RO
				'goods_id' => 2136,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4751 => [ //Military Suit - HU
				'goods_id' => 2136,
				'country_id' => 1500,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4750 => [ //Military Suit - CZ
				'goods_id' => 2136,
				'country_id' => 1109,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4763 => [ //Tactic Lantern - HU
				'goods_id' => 2103,
				'country_id' => 1500,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4762 => [ //Tactic Lantern - IT
				'goods_id' => 2103,
				'country_id' => 1801,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4761 => [ //Tactic Lantern - CZ
				'goods_id' => 2103,
				'country_id' => 1109,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4760 => [ //Tactic Lantern - BG
				'goods_id' => 2103,
				'country_id' => 1081,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4759 => [ //Tactic Lantern - RO
				'goods_id' => 2103,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4769 => [ //Led lent - HU
				'goods_id' => 2288,
				'country_id' => 1500,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4768 => [ //Led lent - IT
				'goods_id' => 2288,
				'country_id' => 1801,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4767 => [ //Led lent - SK
				'goods_id' => 2288,
				'country_id' => 1300,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4766 => [ //Led lent - CZ
				'goods_id' => 2288,
				'country_id' => 1109,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4765 => [ //Led lent - BG
				'goods_id' => 2288,
				'country_id' => 1081,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4764 => [ //Led lent - RO
				'goods_id' => 2288,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4782 => [ //Magic Band Scotch - HU
				'goods_id' => 2132,
				'country_id' => 1500,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4781 => [ //Magic Band Scotch - IT
				'goods_id' => 2132,
				'country_id' => 1801,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4780 => [ //Magic Band Scotch - SK
				'goods_id' => 2132,
				'country_id' => 1300,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4779 => [ //Magic Band Scotch - CZ
				'goods_id' => 2132,
				'country_id' => 1109,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4778 => [ //Magic Band Scotch - BG
				'goods_id' => 2132,
				'country_id' => 1081,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4777 => [ //Magic Band Scotch - RO	
				'goods_id' => 2132,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4815 => [ //Magic Wax - IT
				'goods_id' => 2213,
				'country_id' => 1801,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],		
			4814 => [ //Magic Wax - SK	
				'goods_id' => 2213,
				'country_id' => 1300,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],		
			4813 => [ //Magic Wax - CZ	
				'goods_id' => 2213,
				'country_id' => 1109,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],		
			4812 => [ //Magic Wax - BG
				'goods_id' => 2213,
				'country_id' => 1081,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],		
			4811 => [ //Magic Wax - RO	
				'goods_id' => 2213,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5186 => [ //Vibro Action - RO	
				'goods_id' => 2394,
				'country_id' => 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],																		
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'				=> 'not existing user, wrong phone number and etc.',
			],
			'reject'	=> [
				'cancelled'			=> 'user rejected the order',
			],
			'expect'	=> [
				'hold'				=> 'order is still in process',
				'new'				=> 'new order',
			],
			'confirm'	=> [
				'confirmed'			=> 'order is confirmed and we consider it paid',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'http://fitexpress.co/api/v1/partner/neworder',
		'urlOrderInfo'		=> 'http://fitexpress.co/api/v1/partner/status',
	],
];