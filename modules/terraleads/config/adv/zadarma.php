<?php

return [
	46154 => [
		'terraleadsApiKey' 		=> '64bfdfa4cf72eba2e29d50c25710d4e3',
		'token' 				=> '776a30e5-236e-4d06-9db4-824d1bbdcd61',
		
		'offers' => [
			2222 => [
				'shop_id' 		=> 1,
				'project_id' 	=> 1,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		],
	],

	'configs' => [
		'statuses' => [
			'trash' => [
				'7' => '',
				'8' => '',
			],
			'reject' => [
				'4' => '',
			],
			'expect' => [
				'1' => '',
				'2' => '',
				'3' => '',
			],
			'confirm' => [
				'5' => '',
				'6' => '',
				'9' => '',
				'10' => '',
				'11' => '',
			],
		],
		
		'brakeLogFolder' 	=> true,
		'urlOrderAdd' 		=> 'https://premiumcall.voiptime.net/api/v2/order/create',
		'urlOrderInfo'		=> 'https://premiumcall.voiptime.net/api/v2/order/list',
	],
];