<?php


return [
    28978 => [
        'terraleadsApiKey'				=> '538e223c79a55ff5755c1e55cc025d05',
        'postbackApiKey'		    	=> '538e223c79a55ff5755c1e55cc025d06',
        'token' 				    	=> 'bc879e39dfd847442d011f1052061442',

        'offers' => [
            4061 => [ //Prostatricum PLUS - AT
                'offer_id'	=> 77,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4255 => [ //Prostatricum PLUS - IT
                'offer_id'  => 77,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4060 => [ //Eretron Aktiv - AT
                'offer_id'  => 6,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4256 => [ //Eretron Aktiv - IT
                'offer_id'  => 6,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4393 => [ //Eretron Aktiv - Fl
                'offer_id'  => 6,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4059 => [ //Artrolux+ - AT
                'offer_id'  => 9,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4275 => [ //Artrolux+ - IT
                'offer_id'  => 9,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4058 => [ //Keton Aktiv - AT
                'offer_id'  => 84,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4274 => [ //Keton Aktiv - IT
                'offer_id'  => 84,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4387 => [ //Slim4vit - CH
                'offer_id'  => 4,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4386 => [ //Slim4vit - Fl
                'offer_id'  => 4,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4385 => [ //Prostatricum - Fl
                'offer_id'  => 5,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4384 => [ //Prostatricum - CH
                'offer_id'  => 5,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
             4388 => [ //Idealis - CH
                'offer_id'  => 10,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4328 => [ //Idealis - IT
                'offer_id'  => 10,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4329 => [ //Urogun - IT
                'offer_id'  => 111,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4330 => [ //Prostatix Ultra - IT
                'offer_id'  => 78,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4359 => [ //Cystinorm - IT
                'offer_id'  => 117,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
        ],
    ],

    'configs' => [
        'statuses' => [
            'trash'	=> [
                'trash'		=> '',
            ],
            'reject'	=> [
                'reject'	=> '',
            ],
            'expect'	=> [
                'expect'	=> '',
            ],
            'confirm'	=> [
                'confirm'	=> '',
            ],
        ],
        'brakeLogFolder'	=> true,
        'urlOrderAdd'		=> 'http://api.webvork.com/v1/new-lead',
        'urlOrderInfo'		=> '',
    ],
];

?>