<?php

return [
	
	50637 => [
		'terraleadsApiKey'			=> '838000f23cd6990f2151526652c0093c',
		'api_key' 					=> 'ISLa384d9afbd6baaef49a1233ddad76279592dd506',
		'user_id' 					=> '277d496d-74eb-4404-aa2a-078d559ccf06',
		
		'offers' => [
			9202 => [ //Cordless Vac - PL
				'offer' 	=> 867,
				'lp' 		=> 1742,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9201 => [ //XL Juicer - PL
				'offer' 	=> 383,
				'lp' 		=> 772,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9103 => [ //Bubble Spray 2x1 - ES
				'offer' 	=> 859,
				'lp' 		=> 1726,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9102 => [ //Power Blender 2x1 - ES
				'offer' 	=> 956,
				'lp' 		=> 1920,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9101 => [ //Stepluxe Summer - ES
				'offer' 	=> 1029,
				'lp' 		=> 2064,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9100 => [ //Power Grow Comb - ES
				'offer' 	=> 1038,
				'lp' 		=> 2082,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9050 => [ //Chef O Matic Multigrill - PT
				'offer' 	=> 710,
				'lp' 		=> 1427,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9048 => [ //Vacuum Sealer - PT
				'offer' 	=> 1022,
				'lp' 		=> 2050,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9049 => [ //Mini Print - ES
				'offer' 	=> 963,
				'lp' 		=> 1934,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8586 => [ //Vibratwin - HR
				'offer' 	=> 530,
				'lp' 		=> 1067,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8591 => [ //Robot Vac - ES
				'offer' 	=> 634,
				'lp' 		=> 1275,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8590 => [ //Oil Free Fryer - RO
				'offer' 	=> 111,
				'lp' 		=> 174,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8589 => [ //Xl Juicer - ES
				'offer' 	=> 214,
				'lp' 		=> 432,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8588 => [ //Vizmaxx Autofocus - PT
				'offer' 	=> 742,
				'lp' 		=> 1492,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8587 => [ //Fast Mixer - PT
				'offer' 	=> 429,
				'lp' 		=> 911,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8585 => [ //Vibratwin - RO
				'offer' 	=> 776,
				'lp' 		=> 1560,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8584 => [ //Total Painter - RO
				'offer' 	=> 86,
				'lp' 		=> 143,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8583 => [ //Slim Jeggings - ES
				'offer' 	=> 4,
				'lp' 		=> 101,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8582 => [ //Comfortisse bra set - ES
				'offer' 	=> 902,
				'lp' 		=> 1812,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6936 => [ //RXB Green Coffee - SK
				'offer' 	=> 35,
				'lp' 		=> 329,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6935 => [ //RXB Complex - SK
				'offer' 	=> 54,
				'lp' 		=> 354,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6934 => [ //RXB Male - PT
				'offer' 	=> 40,
				'lp' 		=> 423,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6932 => [ //RXB Male - ES
				'offer' 	=> 212,
				'lp' 		=> 428,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6933 => [ //RXB Male - SK
				'offer' 	=> 46,
				'lp' 		=> 341,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'		=> [
				'STATUS_DOUBLE' 			=> '',
			],
			'reject'	=> [
				'STATUS_CANCELED'			=> '',
				'STATUS_RETURNED'			=> '',
			],
			'expect'	=> [
				'STATUS_PENDING'			=> '',
				'STATUS_CALL_BACK'			=> '',
				'STATUS_POTENTIAL_DOUBLE'	=> '',
			],
			'confirm'	=> [
				'STATUS_CONFIRMED'			=> '',
				'STATUS_SHIPPED'			=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://www.islaffiliate.com/api/v1/leads/new',
		'urlOrderInfo'		=> 'https://www.islaffiliate.com/api/v1.1/leads/',
	],
];

?>