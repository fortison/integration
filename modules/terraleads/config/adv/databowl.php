<?php

/*
	В чате

	http://connectcpa.net/terraleads/adv/adv-postback-status?systemOrderId={f_133_order_id}&systemUserId=22745&partnerStatus={status}&postbackApiKey=90d0c398c27d28ee91d573258dsb6b23
 */

return [
	2323 => [
		'terraleadsApiKey'			=> '00000000000000000000000000000000',
		'postbackApiKey'			=> '90d0c398c27d28ee91d573258dsb6b23',
		'cid'						=> '1',
		'sid'						=> '100',
		
		'offers' => [
			2323 => [
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash' 		=> '',
			],
			'reject'	=> [
				'reject'    => '',
			],
			'expect'	=> [
				'expect'   		 => '',
			],
			'confirm'	=> [
				'confirm'    	=> '',
			],
		],
		
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://primal.databowl.com/api/v1/lead',
//		'statusEnabled'		=> false,
	],
];