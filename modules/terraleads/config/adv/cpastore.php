<?php
/*
===API for creating leads===Should be sent via POST method to https://crm.phprocessing.info/modules/Webforms/capture.php urlencoded or multipart/form-data are acceptableBody fields: (** All fields must be URL encoded **)* __vtrftk=sid:f792580f877b050bd5cf42ac2ba63be3a20e5370 - hardcoded* publicid=25b0468e4bf86f9c9ce267898829bccb - hardcoded* urlencodeenable=1 - hardcoded* name=Nf - hardcoded* leadstatus=New - hardcoded* leadsource=Nf - hardcoded* firstname=Garry - First Name* lastname=Potter - Last Name* cf_852=French - Language, can be Spanish, German, Italian, French or English, other languages should be defined as English* email=qq1234@bb.com - E-Mail, we check unique of lead by this field* phone=+123456789 - Phone number in format E.164 e.g. +972541232323* country=France - Country* cf_904=http://your-domain.com/ipn-status.php?leadId=&signature= - notification URL,  will be replaced to lead ID, signature is sha1(LeadID + 'topScretPa$$wd')Full example using cUrl:curl -XPOST -d '__vtrftk=sid:f792580f877b050bd5cf42ac2ba63be3a20e5370,1504253022&publicid=25b0468e4bf86f9c9ce267898829bccb&urlencodeenable=1&name=Terra&firstname=qq&lastname=ww&leadstatus=New&cf_852=French&email=qq1234@bb.com&phone=+123456789&country=France&cf_902=1&leadsource=Terra&cf_904=http%3A%2F%2Fyour-domain.com%2Fipn-status.php%3FleadId%3D%3Cid%3E%26signature%3D%3Csignature%3E' https://crm.phprocessing.info/modules/Webforms/capture.phpВід:Arseniy CTO здесь ничего сложного нет, если есть какие-то дополнительные поля, например campaignId или наоборот, чего-то нет, скажите мне я могу адаптировать под васВід:Arseniy CTO касательно нотификаций по продажам, мы можем посылать вам сразу же по факту продажи по URL указанному в поле cf_904 или предоставить вам ссылку для поллинга
*/

return [
	
	28041 => [
		'terraleadsApiKey'			=> '1bbee3d7688c1675d4fcf92775878660',
		'apiKey'					=> '6AF22B846038C1ED4A60262696F59D2A',
		'partnerId' 				=> '2033',
		
		'offers' => [
			8346 => [//Alcodont - MD (low price)
				'item_id' => '578',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8243 => [//Prostanol - MD (low price)
				'item_id' => '630',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8242 => [//Keto Black - MD (low price)
				'item_id' => '629',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8127 => [//Gluconax - MD (low price)
				'item_id' => '627',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8126 => [//Cystenon - MD (low price)
				'item_id' => '628',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7187 => [//Varicone - MD (low price)
				'item_id' => '609',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7186 => [//OstyHealth - MD (low price)
				'item_id' => '608',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7185 => [//Erectil - MD (low price)
				'item_id' => '607',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7178 => [//Fungent - MD
				'item_id' => '603',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5786 => [//Maksilan - MD
				'item_id' => '5000',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5785 => [//Recardio - MD
				'item_id' => '4710',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5784 => [//NormaDerm - MD
				'item_id' => '4650',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5783 => [//Costaflex - MD
				'item_id' => '3210',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3944 => [
				'item_id' => '454',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5716 => [//Potencialex low price - MD
				'item_id' => '433',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5719 => [//Potencialex - MD
				'item_id' => '4330',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5717 => [ //Rasputnica low price - MD
				'item_id' => '431',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5718 => [ //Rasputnica - MD
				'item_id' => '4310',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5715 => [//Keto Genetic low price- MD
				'item_id' => '434',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5720 => [//Keto Genetic - MD
				'item_id' => '4340',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5714 => [//Nerosistem low price - MD
				'item_id' => '435',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5721 => [//Nerosistem 7 - MD
				'item_id' => '4350',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5713 => [//Pharaon low price- MD
				'item_id' => '455',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5722 => [//Pharaon - MD
				'item_id' => '4550',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'8' => 'треш',
			],
			'reject'	=> [
				'7' => 'Отказ',
				'4' => 'Отмена',
			],
			'expect'	=> [
				'1' => 'Новый',
				'2' => 'В обработке',
			],
			'confirm'	=> [
				'5' => 'Отправлен',
				'6' => 'Доставлен',
				'3' =>  'Заказ принят',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://cpa24store.com/api-orders/',
		'urlOrderInfo'		=> 'https://cpa24store.com/api-orders/',
	],
];

?>