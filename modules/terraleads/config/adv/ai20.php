<?php

/*
this is our API add lead

https://www.innovamax.life/addlead?api_key={API_KEY}

off={OFF}
net={NET}
aff={AFFID}
cid={CLICKID}
firstname={firstname}
lastname={lastname}
email={email}
phone={phone}
address={address}
city={city}
zipcode={zipcode}
qty={qty}
note={note}
loc={country code}
source={User-Agent}


With this call API:
https://www.innovamax.life/getleadstatus?api_key={API_KEY}&id={LEAD_ID}
you can check the status of leads by passing the API_key of the Ai20 profile and the lead id.

you will get response like this:{"data":"2022-08-23T00:57:06.131Z","lead_Id":2572348,"status":"approved"}

our statuses are: approved, pending, rejected.

https://app.ai20.network/login
julia@terraleads.com
terra2022
 */

return [
	47773 => [
		'terraleadsApiKey'			=> '6eba483fe498fced959485a5df04ee1c',
		'api_key' 					=> '43EA701B72194895B82B61E9113EE5CA',
		'net'						=> '3217826',
		
		'offers' => [
			2222 => [
				'off'	=> 3001873,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],

	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'		=> '',
			],
			'reject'	=> [
				'rejected'	=> '',
			],
			'expect'	=> [
				'pending'	=> '',
			],
			'confirm'	=> [
				'approved'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://www.innovamax.life/addlead',
		'urlOrderInfo'		=> 'https://www.innovamax.life/getleadstatus',
	],
];

?>