<?php


return [
	
	35966 => [//test
		'terraleadsApiKey'			=> '7c93f12d98589e6332b55f584a283f02',
		'apiKey' 					=> 'eLCthQlFEttoFtgXRaTIoA0Uj2bfCZHB+/DXkTpKYhhB21y9AgYY9QUfwVwnTLjuV/BELdLZhoYP3lzGDpGSmeu1yPYDaywtBR1SxHWeQg4=',
		'apiKey1' 					=> 'eLCthQlFEttoFtgXRaTIoA0Uj2bfCZHB+/DXkTpKYhhB21y9AgYY9QUfwVwnTLjuV/BELdLZhoYP3lzGDpGSmeu1yPYDaywtBR1SxHWeQg4=',
		
		'offers' => [
			5914 => [ //CardioFix - КЕ
				'goods_id' => 'CFK',
				'user_id' => 'terraleads',
				'subaffiliateid' => 'TerraLeds',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5016 => [ //Flexofit - RW
				'goods_id' => 'FXR',
				'user_id' => 'terraleads',
				'subaffiliateid' => 'TerraLeds',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5015 => [ //Flexofit - UG
				'goods_id' => 'FXU',
				'user_id' => 'terraleads',
				'subaffiliateid' => 'TerraLeds',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5113 => [ //Cardio NRJ - UG
				'goods_id' => 'CNU',
				'user_id' => 'terraleads',
				'subaffiliateid' => 'TerraLeds',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5112 => [ //Cardio NRJ - KE
				'goods_id' => 'CNK',
				'user_id' => 'terraleads',
				'subaffiliateid' => 'TerraLeds',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'Trash'				=> '',
				'Duplicate'				=> '',
				'Invalid_Phone_Number'				=> '',
				'Fake_Order'				=> '',
				'Trash_Other_Reason'				=> '',
			],
			'reject'	=> [
				'Health_Issues'			=> '',
				'Expensive'			=> '',
				'Cancelled'			=> '',
				'Consultation'			=> '',
				'Changed_Mind'			=> '',
				'Cannot_Reach_Client'			=> '',
				'Cancelled_Other_Reason'			=> '',
			],
			'expect'	=> [
				'Recall'				=> '',
				'Hold'					=> '',
				'hold'					=> '',
				'Hold '					=> '',
				'Call_Rejected'			=> '',
				'Pending' 				=> '',
				'Following'				=> '',
				'editing'				=> '',
			],
			'confirm'	=> [
				'Confirmed'			=> '',
				'Buyout_approved'	=> '',
				'Buyout_hold'		=> '',
				'Buyout_rejected'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://www.orderspc.com/terraleads/api/darajacambiar/afficheranunciar',
		'urlOrderInfo'		=> "https://www.orderspc.com/terraleads/api/darajacambiar/demandedestatutList",
	],
];