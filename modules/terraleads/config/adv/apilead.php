<?php

/*
 * https://apilead.com/docs/API_WEB.pdf
 */

return [
	
	32413 => [
		'terraleadsApiKey'		    => '96f9fee6691bc1dcb3bc2f24cbbfba9c',
		'apiKey'					=> '0d77640ef4bab23bd03f43f7889e3955',
		'userId' 					=> 12360,
		
		'offers' => [
			4712 => [ //Arctic Air - RO
				'offerId' => 3039,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			]
		],
		'configs' => [
			'statuses' => [
				'reject'	=> [
					'reject'	=> '',
				],
				'expect'	=> [
					'accept'	=> '',
					'expect'	=> '',
				],
				'trash'	=> [
					'trash'		=> ''
				],
				'confirm'	=> [
					'confirm'	=> '',
				],
			],
			
			'bridgeRequestTimeout'		=> 60,
			'statusRequestTimeout'		=> 60,
			'urlOrderAdd'				=> 'http://al-api.com/api/lead/create',
			'urlOrderInfo'				=> 'http://al-api.com/api/lead/status',
		],
	],
];

?>