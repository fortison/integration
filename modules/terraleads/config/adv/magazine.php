<?php

return [
	
	33428 => [
		'terraleadsApiKey' => 'ad5c5364de6c59f1ea50f295950d12f1',
		'postbackApiKey' => 'ad5c5364de6c59f1ea50f295950d12f2',
		'apiKey' => 'W0J0N!N!CYW0DY',
		'partner_name' => 'TerraLeads',
		
		'offers' => [
			6749 => [ //Skinatrex - PL
				'product_id' => 13,
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4880 => [ //Diabetover - PL
				'product_id' => 1,
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5600 => [ //Diabetizer - PL
				'product_id' => 1,
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4930 => [ //Dr. Penigreat - PL
				'product_id' => 3,
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		],
		
		'configs' => [
			'brakeLogFolder' => true,
			
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'trash' => '',
			],
			'reject' => [
				'rejected' => '',
			],
			'expect' => [
				'new' => '',
			],
			'confirm' => [
				'confirmed' => '',
			],
		],
		'brakeLogFolder' => true,
		'urlOrderAdd' => 'http://zamowieniacrm.pl/api/partners/TerraLeads:-W0J0N!N!CYW0DY-/addLead',
	],
];
?>
