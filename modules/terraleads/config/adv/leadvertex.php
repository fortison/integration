<?php

/**
 * @see http://demo-1.leadvertex.ru/webmaster/api.html
 */

return [
	65298 => [
		'terraleadsApiKey'			=> '9fdf249dd0b837722f8c6380cb5a4a25',
		'webmasterID'				=> 3,
		'offers' => [
			8904 => [ //Prostatin+ - RS
				'token'				=> 'terraleads',
				'goodId'           	=> '209294',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://biosvet.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://biosvet.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8903 => [ //Capsil - RS
				'token'				=> 'terraleads',
				'goodId'           	=> '209298',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://biosvet.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://biosvet.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8902 => [ //Immuno Flex - RS
				'token'				=> 'terraleads',
				'goodId'           	=> '209034',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://biosvet.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://biosvet.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	64600 => [
		'terraleadsApiKey'			=> '20687e9b8b494355e26b2ab3d092d860',
		'webmasterID'				=> 7,
		'offers' => [
			8860 => [ //D-Fit - MY
				'token'				=> 'terraleads',
				'goodId'           	=> '202099',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://mygeo.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://mygeo.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8859 => [ //Power Plus - MY
				'token'				=> 'terraleads',
				'goodId'           	=> '200002',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://mygeo.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://mygeo.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5123 => [ //HorseMEN - MY
				'token'				=> 'terraleads',
				'goodId'           	=> '196130',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://mygeo.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://mygeo.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5122 => [ //UltraHot - MY
				'token'				=> 'terraleads',
				'goodId'           	=> '196131',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://mygeo.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://mygeo.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	67123 => [
		'terraleadsApiKey'			=> '57d5c472f5216242bafb99195c0ee23b',
		'webmasterID'				=> 1,
		'offers' => [
			9423 => [ //Tribulus Eropulse - KW
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '31',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9422 => [ //Tribulus Eropulse - JO
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '22',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9421 => [ //Tribulus Eropulse - LB
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '40',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9420 => [ //Saw Palmetto ProstaAktiv - KW
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '37',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9419 => [ //Saw Palmetto ProstaAktiv - JO
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '28',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9418 => [ //Saw Palmetto ProstaAktiv - LB
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '46',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9417 => [ //Milk Thistle Cardioactive - KW
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '36',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9416 => [ //Milk Thistle Cardioactive - JO
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '27',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9415 => [ //Milk Thistle Cardioactive - LB
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '45',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9414 => [ //Metabooster Slim - KW
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '35',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9413 => [ //Metabooster Slim - JO
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '26',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9412 => [ //Metabooster Slim - LB
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '44',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9411 => [ //Glucosamine Arthroflex - KW
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '34',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9410 => [ //Glucosamine Arthroflex - JO
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '25',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9409 => [ //Glucosamine Arthroflex - LB
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '43',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9408 => [ //Epimedium Eroctin - KW
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '29',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9407 => [ //Epimedium Eroctin - JO
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '15',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9406 => [ //Epimedium Eroctin - LB
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '38',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9405 => [ //Betaglucan Diabex - KW
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '33',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9404 => [ //Betaglucan Diabex - JO
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '24',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9403 => [ //Betaglucan Diabex - LB
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '42',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9402 => [ //Keto BHB Premium - PK
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '14',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9401 => [ //Black Latte - HU
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MDY4ZWM0ZThmNDE5ODFkZGUyYTczNGNmZGFjNmEwIiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NjgiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjQifX0.gEuLUtmVe0xzX1dr4LmAW6UvhKhLT7vuYfAvq5BiaEU',
				'goodId'           	=> '16',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://468-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	66210 => [
		'terraleadsApiKey'			=> '65aa47392ff815716c37432dfdcc9735',
		'webmasterID'				=> 1,
		'offers' => [
			9130 => [
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImY1MmM5MjdkYTFmMGQwYzQ5OTBjMTAwZjlkMTNhMTc5IiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI1OTIiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjEifX0.-2RTJDssrRv5GQFpbaaxdVlld7rsERt8BCvxKHcKmIE',
				'goodId'           	=> '1',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://592-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://592-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	65916 => [
		'terraleadsApiKey'			=> 'bd35876e10d05892efa6047d4f7a8a23',
		'webmasterID'				=> 28,
		'offers' => [
			9208 => [ //Azempic + - KZ (free price)
				'token'				=> 'terraleads',
				'goodId'           	=> '206047',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://hotleads-kz.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://hotleads-kz.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9207 => [ //Azempic + - KZ (low price)
				'token'				=> 'terraleads',
				'goodId'           	=> '206047',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://hotleads-kz.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://hotleads-kz.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9206 => [ //Fit Graype - KZ (free price)
				'token'				=> 'terraleads',
				'goodId'           	=> '208293',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://hotleads-kz.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://hotleads-kz.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9205 => [ //Fit Graype - KZ (low price)
				'token'				=> 'terraleads',
				'goodId'           	=> '208293',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://hotleads-kz.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://hotleads-kz.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9204 => [ //Power of Samurai - KZ (free price)
				'token'				=> 'terraleads',
				'goodId'           	=> '208290',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://hotleads-kz.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://hotleads-kz.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9203 => [ //Power of Samurai - KZ (low price)
				'token'				=> 'terraleads',
				'goodId'           	=> '208290',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://hotleads-kz.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://hotleads-kz.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9057 => [ //Azempic + - KZ
				'token'				=> 'terraleads',
				'goodId'           	=> '206047',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://hotleads-kz.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://hotleads-kz.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9058 => [ //Fit Graype - KZ
				'token'				=> 'terraleads',
				'goodId'           	=> '208293',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://hotleads-kz.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://hotleads-kz.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9059 => [ //Power of Samurai - KZ
				'token'				=> 'terraleads',
				'goodId'           	=> '208290',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://hotleads-kz.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://hotleads-kz.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	64558 => [
		'terraleadsApiKey'			=> '93290982951c50d988ad4629c2159b82',
		'webmasterID'				=> 1,
		'offers' => [
			8822 => [ //Presmin - DO
				'token'				=> 'terraleads',
				'goodId'           	=> '205759',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://dominicana.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://dominicana.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	60642 => [
		'terraleadsApiKey'			=> '6401c728119c018a54887835265c0196',
		'webmasterID'				=> 2,
		'offers' => [
			9200 => [ //Maca Panax Ginseng - BD
				'token'				=> 'terraleads',
				'goodId'           	=> '41061',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://dme-6.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://dme-6.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8962 => [ //Hamerol - BD
				'token'				=> 'terraleads',
				'goodId'           	=> '40894',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://dme-6.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://dme-6.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8720 => [ //DME-6 - BD
				'token'				=> 'terraleads',
				'goodId'           	=> '203923',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://dme-6.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://dme-6.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	58358 => [
		'terraleadsApiKey'			=> '9a29acd5ae309b2a179523f056a11963',
		'webmasterID'				=> 2,
		'offers' => [
			9381 => [ //Velora Retinol Serum - PK
				'token'				=> 'terraleads',
				'goodId'           	=> '208979',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://velora-cosmatics.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://velora-cosmatics.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9380 => [ //Velora Vitamin C Serum - PK
				'token'				=> 'terraleads',
				'goodId'           	=> '208976',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://velora-cosmatics.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://velora-cosmatics.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8888 => [ //Velora Night Cream - PK (low price)
				'token'				=> 'terraleads',
				'goodId'           	=> '205994',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://velora-cosmatics.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://velora-cosmatics.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8665 => [ //Velora Night Cream - PK
				'token'				=> 'terraleads',
				'goodId'           	=> '204059',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://velora-cosmatics.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://velora-cosmatics.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	52444 => [
		'terraleadsApiKey'			=> 'bc828f025a57d6ee5c754d758c638601',
		'webmasterID'				=> 8,
		'offers' => [
			9211 => [ //Hyper+ - TN (low price)
				'token'				=> 'terraleads',
				'goodId'           	=> '208910',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://vitx.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://vitx.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9210 => [ //Vitaminex - TN (low price)
				'token'				=> 'terraleads',
				'goodId'           	=> '208909',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://vitx.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://vitx.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9209 => [ //ArtiRelax - TN (low price)
				'token'				=> 'terraleads',
				'goodId'           	=> '208911',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://vitx.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://vitx.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8604 => [ //ArtiRelax - TN
				'token'				=> 'terraleads',
				'goodId'           	=> '195833',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://vitx.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://vitx.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8603 => [ //Vitaminex - TN
				'token'				=> 'terraleads',
				'goodId'           	=> '202407',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://vitx.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://vitx.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8601 => [ //Hyper+ - TN
				'token'				=> 'terraleads',
				'goodId'           	=> '200436',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://vitx.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://vitx.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	40232 => [
		'terraleadsApiKey'			=> '4c425753b8d26c740a12c169d7197014',
		'webmasterID'				=> 2,
		'offers' => [
			8521 => [
				'token'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2RlLmJhY2tlbmQuc2FsZXNyZW5kZXIuY29tLyIsImF1ZCI6IkNQQSIsImp0aSI6ImQ2MDJlMzUyYjNlNDdlMDUwOWE5NGNkZjA0ZmEwZWY3IiwidHlwZSI6IndlYm1hc3Rlcl9hcGkiLCJjaWQiOiI0NTMiLCJyZWYiOnsiYWxpYXMiOiJ3ZWJtYXN0ZXIiLCJpZCI6IjIifX0.hpukIj9I1cO_PeqG_VRUUIoBjmV4C-qge34jViJz2jc',
				'goodId'           	=> '1',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://453-1.de.backend.salesrender.com/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://453-1.de.backend.salesrender.com/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	57805 => [
		'terraleadsApiKey'			=> 'bfd3e9a03b9b5166b980d1ebc1ce6e1d',
		'webmasterID'				=> 2,
		
		'offers' => [
			8071 => [ //Gluco Shield - PK
				'token'				=> 'terraleads',
				'goodId'           	=> '201129',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://johnp.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://johnp.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8199 => [ //Rock Hard - PK
				'token'				=> 'terraleads',
				'goodId'           	=> '202688',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://johnp.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://johnp.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8198 => [ //Max Energy - PK
				'token'				=> 'terraleads',
				'goodId'           	=> '202687',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://johnp.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://johnp.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8197 => [ //BP Control - PK
				'token'				=> 'terraleads',
				'goodId'           	=> '202686',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://johnp.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://johnp.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	56185 => [
		'terraleadsApiKey'			=> '4464df893320bf3f875b3285076b1d7a',
		'webmasterID'				=> 10,
		
		'offers' => [
			9386 => [ //Optimac - JO111
				'token'				=> 'terraleads',
				'goodId'           	=> '196020',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9373 => [ //Hemout - LB
				'token'				=> 'terraleads',
				'goodId'           	=> '188699',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9374 => [ //Hemout - JO
				'token'				=> 'terraleads',
				'goodId'           	=> '196021',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9375 => [ //Hemout - AE
				'token'				=> 'terraleads',
				'goodId'           	=> '195753',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9376 => [ //Hemout - QA
				'token'				=> 'terraleads',
				'goodId'           	=> '195766',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9377 => [ //Hemout - KW
				'token'				=> 'terraleads',
				'goodId'           	=> '195772',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9378 => [ //Hemout - OM
				'token'				=> 'terraleads',
				'goodId'           	=> '195760',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9372 => [ //Mor Ginseng - OM
				'token'				=> 'terraleads',
				'goodId'           	=> '204836',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9371 => [ //Mor Ginseng - KW
				'token'				=> 'terraleads',
				'goodId'           	=> '204834',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9370 => [ //Mor Ginseng - QA
				'token'				=> 'terraleads',
				'goodId'           	=> '204835',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9369 => [ //Mor Ginseng - AE
				'token'				=> 'terraleads',
				'goodId'           	=> '204833',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8345 => [ //Green Label Serum - LY
				'token'				=> 'terraleads',
				'goodId'           	=> '193536',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8344 => [ //Cosakondrin - LY
				'token'				=> 'terraleads',
				'goodId'           	=> '187102',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8343 => [ //Hemout - LY
				'token'				=> 'terraleads',
				'goodId'           	=> '188700',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8342 => [ //Hitman - LY
				'token'				=> 'terraleads',
				'goodId'           	=> '189964',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8619 => [ //Mor Ginseng - LY
				'token'				=> 'terraleads',
				'goodId'           	=> '204729',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8770 => [ //Mor Ginseng - LB
				'token'				=> 'terraleads',
				'goodId'           	=> '204756',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8771 => [ //Mor Ginseng - JO
				'token'				=> 'terraleads',
				'goodId'           	=> '204837',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7922 => [ //Perst - BH
				'token'				=> 'terraleads',
				'goodId'           	=> '195775',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7921 => [ //Perst - OM
				'token'				=> 'terraleads',
				'goodId'           	=> '195757',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7920 => [ //Perst - QA
				'token'				=> 'terraleads',
				'goodId'           	=> '195763',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7919 => [ //Optimac - BH
				'token'				=> 'terraleads',
				'goodId'           	=> '194136',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7918 => [ //Optimac - QM
				'token'				=> 'terraleads',
				'goodId'           	=> '195759',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7917 => [ //Optimac - QA
				'token'				=> 'terraleads',
				'goodId'           	=> '195765',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7722 => [ //Perst - LB
				'token'				=> 'terraleads',
				'goodId'           	=> '195525',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7721 => [ //Perst - LY
				'token'				=> 'terraleads',
				'goodId'           	=> '195524',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7720 => [ //Perst - SA
				'token'				=> 'terraleads',
				'goodId'           	=> '188701',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7719 => [ //Perst - KW
				'token'				=> 'terraleads',
				'goodId'           	=> '195769',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7718 => [ //Optimac - LB
				'token'				=> 'terraleads',
				'goodId'           	=> '188701',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7717 => [ //Optimac - LY
				'token'				=> 'terraleads',
				'goodId'           	=> '188702',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7716 => [ //Optimac - SA
				'token'				=> 'terraleads',
				'goodId'           	=> '194133',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7715 => [ //Optimac - KW
				'token'				=> 'terraleads',
				'goodId'           	=> '193934',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kiwi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	53864 => [
		'terraleadsApiKey'			=> 'd247b434579efbe80c4d82fc0d110077',
		'webmasterID'				=> 2,
		
		'offers' => [
			7450 => [ //Power Of Thor Plus - IN
				'token'				=> 'terraleads',
				'goodId'           	=> '39574',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://powerofthorplus.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://powerofthorplus.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	62743 => [
		'terraleadsApiKey'			=> 'ed6ea4287e8148580e44ca610d39c62d',
		'webmasterID'				=> 5,
		
		'offers' => [
			8580 => [ //Diaboost - NG
				'token'				=> 'terraleads',
				'goodId'           	=> '198791',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nigeria.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nigeria.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8649 => [ //Neoritm - GH
				'token'				=> 'terraleads',
				'goodId'           	=> '198791',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ghana2.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ghana2.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
		54118 => [
		'terraleadsApiKey'			=> '877e63bbbab49d529bdd3fb0f7c55077',
		'webmasterID'				=> 7,
		
		'offers' => [
			8528 => [ //A-Ridules - DZ full 1
				'token'				=> 'terraleads',
				'goodId'           	=> '202477',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nervana3plus.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nervana3plus.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8232 => [ //A-Ridules - DZ
				'token'				=> 'terraleads',
				'goodId'           	=> '202739',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nervana3plus.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nervana3plus.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8527 => [ //Prostactive - DZ full
				'token'				=> 'terraleads',
				'goodId'           	=> '200187',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nervana3plus.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nervana3plus.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8231 => [ //Prostactive - DZ
				'token'				=> 'terraleads',
				'goodId'           	=> '202738',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nervana3plus.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nervana3plus.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8230 => [ //Clareene plus - DZ
				'token'				=> 'terraleads',
				'goodId'           	=> '202737',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nervana3plus.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nervana3plus.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8229 => [ //Bio slim - DZ
				'token'				=> 'terraleads',
				'goodId'           	=> '202736',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nervana3plus.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nervana3plus.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8228 => [ //Bio gyne - DZ
				'token'				=> 'terraleads',
				'goodId'           	=> '202735',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nervana3plus.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nervana3plus.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8523 => [ //Bio Epimedium - DZ full
				'token'				=> 'terraleads',
				'goodId'           	=> '199543',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nervana3plus.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nervana3plus.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8227 => [ //Bio Epimedium - DZ
				'token'				=> 'terraleads',
				'goodId'           	=> '202733',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nervana3plus.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nervana3plus.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8226 => [ //Testo complex - DZ
				'token'				=> 'terraleads',
				'goodId'           	=> '202732',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nervana3plus.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nervana3plus.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	54036 => [
		'terraleadsApiKey'			=> '075e0a8ad72370210c0dcf6ca4352a62',
		'webmasterID'				=> 2,
		
		'offers' => [
			7456 => [ //El Vengador - CO
				'token'				=> 'terraleads',
				'goodId'           	=> '196675',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://elvengador.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://elvengador.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	52250 => [
		'terraleadsApiKey'			=> '11e8fec7921a369a11d79b00ef347ea3',
		'webmasterID'				=> 1,
		
		'offers' => [
			7319 => [ //Iraqi Watch - IQ
				'token'				=> 'terraleads',
				'goodId'           	=> '39419',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://smartwatt500.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://smartwatt500.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	53642 => [
		'terraleadsApiKey'			=> '480f593d5ec53f68779238bbd93fc06d',
		'webmasterID'				=> 5,
		
		'offers' => [
			7699 => [ //Slim Sutra - IN
				'token'				=> 'terraleads',
				'goodId'           	=> '198606',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://diabacore.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://diabacore.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7641 => [ //Cardio Vault - IN
				'token'				=> 'terraleads',
				'goodId'           	=> '199070',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://diabacore.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://diabacore.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7560 => [ //Diabacore - IN
				'token'				=> 'terraleads',
				'goodId'           	=> '195834',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://diabacore.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://diabacore.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7559 => [ //Vigorcore - IN
				'token'				=> 'terraleads',
				'goodId'           	=> '196252',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://thorpromax.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://thorpromax.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8948 => [ //Vigorcore - IN (low price)
				'token'				=> 'terraleads',
				'goodId'           	=> '208210',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://thorpromax.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://thorpromax.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7383 => [ //PsoriFix - IN
				'token'				=> 'terraleads',
				'goodId'           	=> '197677',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://diabacore.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://diabacore.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	63684 => [
		'terraleadsApiKey'			=> '4bb6c6523efb408cfebdadaa5cc796fc',
		'webmasterID'				=> 3,
		
		'offers' => [
			8719 => [ // Libidex - IN
				'token'				=> 'terraleads',
				'goodId'           	=> '204147',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://believeinveda.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://believeinveda.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	7586 => [
		'terraleadsApiKey'			=> 'e12634478bac2a16cf5abb6770724ecf',
		'webmasterID'				=> 1,
		
		'offers' => [
			3300 => [ // U
				'token'				=> 'terraleads',
				'goodId'           	=> '188202',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://mobiglide.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://mobiglide.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	48033 => [
		'terraleadsApiKey'			=> 'c16c3589344e82c32070cda2bb6d16b7',
		'webmasterID'				=> 1,
		
		'offers' => [
			7042 => [ // Hammer X Plus - IN
				'token'				=> 'terraleads',
				'goodId'           	=> '189839',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://hammerxplus1.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://hammerxplus1.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6992 => [ // Keto 7x - IN
				'token'				=> 'terraleads',
				'goodId'           	=> '194052',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://keto7x.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://keto7x.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6744 => [ // Diabetes Care - IN
				'token'				=> 'terraleads',
				'goodId'           	=> '192426',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://diabetescare1.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://diabetescare1.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
		49595 => [
		'terraleadsApiKey'			=> '39e976ef735caa830081a0f9358993ac',
		'webmasterID'				=> 1,
		
		'offers' => [
			7115 => [ // Diabex - PH
				'token'				=> 'terraleads',
				'goodId'           	=> '195827',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ph.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ph.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7114 => [ // Cardio NRJ - PH
				'token'				=> 'terraleads',
				'goodId'           	=> '195830',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ph.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ph.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6976 => [ // Alfagen - PH
				'token'				=> 'terraleads',
				'goodId'           	=> '194276',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ph.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ph.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6765 => [ // Getridox - PH
				'token'				=> 'terraleads',
				'goodId'           	=> '192498',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ph.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ph.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6766 => [ // Mad bizzon - PH
				'token'				=> 'terraleads',
				'goodId'           	=> '192497',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ph.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ph.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	47002 => [
		'terraleadsApiKey'			=> '9f196b3213b01d4deebe56bf0879a16d',
		'webmasterID'				=> 1,
		
		'offers' => [
			7252 => [ // Renalin - HU
				'token'				=> 'terraleads',
				'goodId'           	=> '193652',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://supremepharmatech.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://supremepharmatech.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7162 => [ // Astragalus - HU
				'token'				=> 'terraleads',
				'goodId'           	=> '192821',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://supremepharmatech.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://supremepharmatech.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7163 => [ // Curcumin - HU
				'token'				=> 'terraleads',
				'goodId'           	=> '192764',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://supremepharmatech.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://supremepharmatech.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7164 => [ // Glutathione - HU1
				'token'				=> 'terraleads',
				'goodId'           	=> '192822',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://supremepharmatech.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://supremepharmatech.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
            6987 => [ // Erexper - HU
				'token'				=> 'terraleads',
				'goodId'           	=> '193651',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://supremepharmatech.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://supremepharmatech.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6978 => [ // Oftalmin - HU
				'token'				=> 'terraleads',
				'goodId'           	=> '193654',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://supremepharmatech.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://supremepharmatech.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6778 => [ // Weight Factor - HU
				'token'				=> 'terraleads',
				'goodId'           	=> '192477',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://supremepharmatech.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://supremepharmatech.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6848 => [ // Aspermin - HU
				'token'				=> 'terraleads',
				'goodId'           	=> '192478',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://supremepharmatech.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://supremepharmatech.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6847 => [ // Skin day - HU
				'token'				=> 'terraleads',
				'goodId'           	=> '192479',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://supremepharmatech.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://supremepharmatech.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	44579 => [
		'terraleadsApiKey'			=> '610236d863d44985ed1cb46acf288ff9',
		'webmasterID'				=> 2,
		
		'offers' => [
			6086 => [ // Ultra Power Capsules - IN
				'token'				=> 'terraleads',
				'goodId'           	=> '185246',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ultrapower.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ultrapower.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	44575 => [
		'terraleadsApiKey'			=> '184e1524f1b8beea87f0d360c6fe4277',
		'webmasterID'				=> 1,
		
		'offers' => [
			6084 => [
				'token'				=> 'terraleads',
				'goodId'           	=> '184469',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://vskenterprise.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://vskenterprise.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	36446 => [
		'terraleadsApiKey'			=> '8f7f8b210604cd39289314f3f2726093',
		'webmasterID'				=> 5,
		
		'offers' => [
			7930 => [//Ginseng - EG
				'token'				=> 'terraleads',
				'goodId'           	=> '200061',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://healthy-cure.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://healthy-cure.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9160 => [//Slim It - EG
				'token'				=> 'terraleads',
				'goodId'           	=> '208435',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://healthy-cure.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://healthy-cure.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8402 => [//Hammer of Thor - AE
				'token'				=> 'terraleads',
				'goodId'           	=> '203295',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://uae-leadhouse.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://uae-leadhouse.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8416 => [//Belly-fat Burner - AE
				'token'				=> 'terraleads',
				'goodId'           	=> '195675',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://uae-leadhouse.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://uae-leadhouse.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7273 => [//Performix - AE
				'token'				=> 'terraleads',
				'goodId'           	=> '190777',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://uae-leadhouse.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://uae-leadhouse.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7272 => [//Belly-fat Burner - OM
				'token'				=> 'terraleads',
				'goodId'           	=> '195518',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://uae-leadhouse.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://uae-leadhouse.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7271 => [//Hammer Of Thor - OM
				'token'				=> 'terraleads',
				'goodId'           	=> '191589',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://uae-leadhouse.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://uae-leadhouse.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6413 => [//Erecit - EG
				'token'				=> 'terraleads',
				'goodId'           	=> '186386',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://healthy-cure.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://healthy-cure.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6195 => [//Melsya - SA
				'token'				=> 'terraleads',
				'goodId'           	=> '184875',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://uae-leadhouse.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://uae-leadhouse.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5437 => [//Garcinia Extract - EG
				'token'				=> 'terraleads',
				'goodId'           	=> '178460',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://healthy-cure.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://healthy-cure.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4092 => [//Hammer of Thor - EG
				'token'				=> 'terraleads',
				'goodId'           	=> '178462',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://healthy-cure.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://healthy-cure.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5553 => [//Hendel Forex - EG
				'token'				=> 'terraleads',
				'goodId'           	=> '181618',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://healthy-cure.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://healthy-cure.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5448 => [//Xtra-man Cream - EG
				'token'				=> 'terraleads',
				'goodId'           	=> '178463',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://healthy-cure.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://healthy-cure.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5912 => [//lavomasaga Gel - EG
				'token'				=> 'terraleads',
				'goodId'           	=> '181672',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://healthy-cure.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://healthy-cure.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5919 => [//Deuslim - EG
				'token'				=> 'terraleads',
				'goodId'           	=> '182123',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://healthy-cure.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://healthy-cure.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5711 => [//Pysillium - EG
				'token'				=> 'terraleads',
				'goodId'           	=> '178461',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'externalID' => function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://healthy-cure.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://healthy-cure.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	41761 => [
		'terraleadsApiKey'			=> 'da2005b644d48d4f5a9e64f569a14295',
		'webmasterID'				=> 1,
		
		'offers' => [
			6918 => [
				'token'				=> 'terraleads',
				'goodId'           	=> '190871',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://terraleads1.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://terraleads1.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6134 => [
				'token'				=> 'terraleads',
				'goodId'           	=> '184868',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://terraleads1.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://terraleads1.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6012 => [
				'token'				=> 'terraleads',
				'goodId'           	=> '182367',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://terraleads1.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://terraleads1.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5723 => [
				'token'				=> 'terraleads',
				'goodId'           	=> '182365',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://terraleads1.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://terraleads1.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	41589 => [
		'terraleadsApiKey'			=> '48e3773cb06b8bed3965e23dea9d5e4a',
		'webmasterID'				=> 4,
		
		'offers' => [
			5857 => [
				'token'				=> 'terraleads',
				'goodId'           	=> '181145',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://thor123.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://thor123.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6422 => [
				'token'				=> 'terraleads',
				'goodId'           	=> '186463',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://thor123.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://thor123.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	42018 => [
		'terraleadsApiKey'			=> 'eaac473e5814132a140b826e082281d6',
		'webmasterID'				=> 4,
		
		'offers' => [
			9113 => [ // Paraclean - AL	Kosovo
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '203837',
				
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9112 => [ // Paraclean - AL
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '203836',
				
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8769 => [ // HyperGuard - AL Kosovo
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '204259',
				
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8768 => [ // HyperGuard - AL
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '204258',
				
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8405 => [ // SkinGlow - AL
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '203842',
				
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8406 => [ // SkinGlow - AL (Kosovo)
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '203843',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8403 => [ // Hemcare - AL
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '203840',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8404 => [ // Hemcare - AL (Kosovo)
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '203841',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8157 => [ // Prostanol - AL
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '202670',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8158 => [ // Prostanol - AL kosovo
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '205019',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7668 => [ // Cystenon - AL (Kosovo)
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '200041',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7667 => [ // Cystenon - AL (Albania)
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '200042',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7666 => [ // Visitec - AL (Kosovo)
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '200043',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7665 => [ // Visitec - AL (Albania)
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '200044',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7670 => [ // Gluconax - AL (Kosovo)
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '199810',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7669 => [ // Gluconax - AL (Albania)
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '199809',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7276 => [ // Metafix - AL (Kosovo)
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '194237',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7275 => [ // Metafix - AL (Albania)
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '194236',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7251 => [ // Mucoped - AL (Kosovo)
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '194584',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7250 => [ // Mucoped - AL (Albania)
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '194583',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7249 => [ // EasyFlow - AL (Kosovo)
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '194582',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7248 => [ // EasyFlow - AL (Albania)
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '194581',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7247 => [ // Versatil Pro - AL (Kosovo)
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '194239',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7246 => [ // Versatil Pro - AL (Albania)
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '194238',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7245 => [ // Erobeast - AL (Kosovo)
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '194241',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7244 => [ // Erobeast - AL (Albania)
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '194240',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6708 => [ // Fortex - AL (Kosovo)
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '191911',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6707 => [ // Prostatol - AL (Kosovo)
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '191915',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6702 => [ // Fortex - AL
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '191910',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6701 => [ // Prostatol - AL
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '191914',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6477 => [
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '186224',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6476 => [
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '186885',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6325 => [
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '186030',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6216 => [
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '185328',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5849 => [
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '184516',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5794 => [
				'token'				=> '8tVbFhQNsmH7BEe',
				'goodId'           	=> '181244',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimfit-al.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	40746 => [
		'terraleadsApiKey'			=> '34ab3d267744b84bf37acf455e78d8d9',
		'webmasterID'				=> 4,
		
		'offers' => [
			5882 => [ // Hairtox - OM
				'token'				=> 'terraleads',
				'goodId'           	=> '182247',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://menacom.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://menacom.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			6031 => [ // BigBang - GN
				'token'				=> 'terraleads',
				'goodId'           	=> '184696',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			6030 => [ // BigBang - ML
				'token'				=> 'terraleads',
				'goodId'           	=> '184217',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			6009 => [ // Rex Cream - SN
				'token'				=> 'terraleads',
				'goodId'           	=> '180590',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			6011 => [ // Rex Cream - CI
				'token'				=> 'terraleads',
				'goodId'           	=> '180592',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			6028 => [ // Rex Cream - GN
				'token'				=> 'terraleads',
				'goodId'           	=> '183946',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			6010 => [ // Rex Cream - ML
				'token'				=> 'terraleads',
				'goodId'           	=> '180591',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5881 => [ // Epifyar - OM
				'token'				=> 'terraleads',
				'goodId'           	=> '182233',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://menacom.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://menacom.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5880 => [ // Just Hair - OM
				'token'				=> 'terraleads',
				'goodId'           	=> '182232',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://menacom.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://menacom.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5871 => [ // Episoft - CI
				'token'				=> 'terraleads',
				'goodId'           	=> '180588',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5870 => [ // Episoft - SN
				'token'				=> 'terraleads',
				'goodId'           	=> '180587',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5869 => [ // Episoft - ML
				'token'				=> 'terraleads',
				'goodId'           	=> '180589',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5874 => [ // Viaxi Gel - CI
				'token'				=> 'terraleads',
				'goodId'           	=> '181640',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5873 => [ // Viaxi Gel - SN
				'token'				=> 'terraleads',
				'goodId'           	=> '181641',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5872 => [ // Viaxi Gel - ML
				'token'				=> 'terraleads',
				'goodId'           	=> '181642',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5879 => [ // Viaxi Breast Cream - SN (Mali
				'token'				=> 'terraleads',
				'goodId'           	=> '181645',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5878 => [ // Viaxi Breast Cream - SN (Cote 
				'token'				=> 'terraleads',
				'goodId'           	=> '181644',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5877 => [ // Viaxi Breast Cream - SN (Senegal)
				'token'				=> 'terraleads',
				'goodId'           	=> '181643',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5604 => [ // 
				'token'				=> 'terraleads',
				'goodId'           	=> '180394',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5603 => [ // 
				'token'				=> 'terraleads',
				'goodId'           	=> '180297',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5602 => [ // 
				'token'				=> 'terraleads',
				'goodId'           	=> '180395',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ecomafrica.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	39318 => [
		'terraleadsApiKey'			=> '724567b690b89e61aeddc8de6d55fc55',
		'webmasterID'				=> 8,
		
		'offers' => [
			6503 => [ // Tibettea parasites (low price) - AZ
				'token'				=> 'terraleads',
				'goodId'           	=> '108560',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://leadwave-7.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://leadwave-7.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			6502 => [ // Tibettea potency (low price) - AZ
				'token'				=> 'terraleads',
				'goodId'           	=> '106490',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://leadwave-5.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://leadwave-5.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			6501 => [ // Diason Tea (low price) - AZ
				'token'				=> 'terraleads',
				'goodId'           	=> '179721',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://leadwave-38.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://leadwave-38.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			6306 => [ // Hiperson Tea (low price) - AZ
				'token'				=> 'terraleads',
				'goodId'           	=> '180265',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://leadwave-11.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://leadwave-11.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			6008 => [ // Chondro Doll - AZ
				'token'				=> 'terraleads',
				'goodId'           	=> '183974',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://leadwave-98.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://leadwave-98.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5925 => [ // Afromen Gel - AZ
				'token'				=> 'terraleads',
				'goodId'           	=> '179100',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://leadwave-95.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://leadwave-95.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5830 => [ // Afromen Tea low price - AZ
				'token'				=> 'terraleads',
				'goodId'           	=> '179725',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://leadwave-21.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://leadwave-21.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5686 => [ // Diason Tea - AZ
				'token'				=> 'terraleads',
				'goodId'           	=> '179721',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://leadwave-40.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://leadwave-40.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5592 => [ // Tibettea potency - AZ
				'token'				=> 'terraleads',
				'goodId'           	=> '106490',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://leadwave-75.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://leadwave-75.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5568 => [ // Money Amulet - AZ
				'token'				=> 'terraleads',
				'goodId'           	=> '106396',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://leadwave-3.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://leadwave-3.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	40604 => [
		'terraleadsApiKey'			=> '8f1b648c469f3c93bc9cb564ebfd5557',
		'webmasterID'				=> 7,
		
		'offers' => [
			5548 => [ // 
				'token'				=> 'terraleads',
				'goodId'           	=> '177432',

				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://clareenedz.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://clareenedz.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5549 => [ // 
				'token'				=> 'terraleads',
				'goodId'           	=> '172171',

				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://clareenedz.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://clareenedz.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5550 => [ // 
				'token'				=> 'terraleads',
				'goodId'           	=> '176324',

				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://clareenedz.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://clareenedz.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	32614 => [
		'terraleadsApiKey'			=> '86d304995f6e9edbc715601208c75f7f',
		'webmasterID'				=> 1,
		
		'offers' => [
			8033 => [ // GoSperm - PK
				'token'				=> 'terraleads',
				'goodId'           	=> '200971',

				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://herbaloil.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://herbaloil.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],

			6062 => [ // Messo Forte - PK
				'token'				=> 'terraleads',
				'goodId'           	=> '182126',

				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://herbaloil.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://herbaloil.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			4783 => [ // Extra Hard Herbal Oil - PK
				'token'				=> 'terraleads',
				'goodId'           	=> '172456',

				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://herbaloil.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://herbaloil.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5477 => [ // Daibo Control - PK
				'token'				=> 'terraleads',
				'goodId'           	=> '178252',

				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://herbaloil.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://herbaloil.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			5219 => [ // Herbal Action - PK
				'token'				=> 'terraleads',
				'goodId'           	=> '175602',

				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://herbaloil.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://herbaloil.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			4900 => [
				'token'				=> 'terraleads',
				'goodId'           	=> '171985',

				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://herbaloil.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://herbaloil.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4943 => [ //
				'token'				=> 'terraleads',
				'goodId'           	=> '171770',

				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://herbaloil.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://herbaloil.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5466 => [ //
				'token'				=> 'terraleads',
				'goodId'           	=> '178789',

				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://herbaloil.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://herbaloil.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	31645 => [
		'terraleadsApiKey'			=> 'a7064cca45a36e90ae31976f7771a2ff',
		'webmasterID'				=> 1,
		
		'offers' => [
			4806 => [ // Male enhancer Pro - IN
				'token'				=> 'terraleads',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://maleenhancer-2021.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://maleenhancer-2021.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	37947 => [
		'terraleadsApiKey'			=> '84a3ef139c3b9dadefbac5970db98b75',
		'webmasterID'				=> 609,
		
		'offers' => [
			5463 => [ // Slimbiotic - TR
				'token'				=> 'terraleads',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimbiotic-tr.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimbiotic-tr.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6291 => [ // Lukcy Lure - KZ
				'token'				=> 'terraleads',
				'ip'				=> '2.134.123.38',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://lucky-lure.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://lucky-lure.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6292 => [ // Kazahstan Watch - KZ
				'token'				=> 'terraleads',
				'ip'				=> '2.134.123.38',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://baellerry-kz-watch.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://baellerry-kz-watch.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6293 => [ // VenuSocks - KZ
				'token'				=> 'terraleads',
				'ip'				=> '2.134.123.38',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://zip-sox.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://zip-sox.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6294 => [ // Flying Fairy - KZ 
				'token'				=> 'terraleads',
				'ip'				=> '2.134.123.38',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://fairy-kz.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://fairy-kz.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	31892 => [
		'terraleadsApiKey'			=> '8bb95e28805ba79d70bd0957980c6bef',
		'webmasterID'				=> 3,
		
		'offers' => [
			5392 => [ // Power of Thor Max - IN
				'token'				=> 'terraleads',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://potm.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://potm.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	40490 => [
		'terraleadsApiKey'			=> '9834eaaa32b63e07a31b708ec96175d1',
		'webmasterID'				=> 1,
		
		'offers' => [
			6619 => [ // Yarshagumba - NP
				'token'				=> 'terraleads',
				'goodId'           	=> '190860',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://y.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://y.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6005 => [ // Alfagen - NP
				'token'				=> 'terraleads',
				'goodId'           	=> '184864',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://hairloss.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://hairloss.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4847 => [ // Maral Gel - BD
				'token'				=> 'terraleads',
				'goodId'           	=> '182390',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://mgbd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://mgbd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5552 => [ // Maral Gel - NP
				'token'				=> 'terraleads',
				'goodId'           	=> '179258',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://mgnp.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://mgnp.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6358 => [ // Maral Gel CPL - NP
				'token'				=> 'terraleads',
				'goodId'           	=> '186306',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://hairloss.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://hairloss.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		    //5552 => [  //Maral Gel - NP
				//'token'				=> 'terraleads',
				//'goodId'           	=> '179258',
				 //'externalWebmaster'		=> function ($model, $config) {
                  //  return $model->uHash ?: $model->wHash;
                //},
				//'configs' => [
				//	'brakeLogFolder'	=> true,
				//	'urlOrderInfo'		=> 'https://hairloss.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				//],
			//],
			5826 => [ // Sevich Hair Oil - NP
				'token'				=> 'terraleads',
				'goodId'           	=> '186774',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://h.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://h.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	40742 => [
		'terraleadsApiKey'			=> 'a30705405862e966e255027c4521ca8a',
		'webmasterID'				=> 4,
		
		'offers' => [
			5966 => [ // Green Fit - CO
				'token'				=> 'terraleads',
				'goodId'           	=> '181497',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5967 => [ // Prostagen - CO
				'token'				=> 'terraleads',
				'goodId'           	=> '181302',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5968 => [ // Biodermalix - CO
				'token'				=> 'terraleads',
				'goodId'           	=> '181566',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5969 => [ // Carditone - CO
				'token'				=> 'terraleads',
				'goodId'           	=> '182329',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5970 => [ // LipoSystem - CO
				'token'				=> 'terraleads',
				'goodId'           	=> '181565',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5971 => [ // Vibracionica - CO
				'token'				=> 'terraleads',
				'goodId'           	=> '178900',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5972 => [ // Optipure - co
				'token'				=> 'terraleads',
				'goodId'           	=> '182043',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5809 => [ // HairEX - CO
				'token'				=> 'terraleads',
				'goodId'           	=> '181301',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4911 => [ // Bendax - CO
				'token'				=> 'terraleads',
				'goodId'           	=> '178898',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5570 => [ // Xtra Man - CO
				'token'				=> 'terraleads',
				'goodId'           	=> '179417',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3852 => [ // Crystalix - CO
				'token'				=> 'terraleads',
				'goodId'           	=> '176808',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5213 => [ // Enterotox - CO
				'token'				=> 'terraleads',
				'goodId'           	=> '178897',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://naturallvid.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	38548 => [
		'terraleadsApiKey'			=> '6dcbfb84e2185c7216d1315b16b8edb5',
		'webmasterID'				=> 2,
		
		'offers' => [
			5351 => [ // Hammer Power Pro - IN
				'token'				=> 'terraleads',
				'goodId'           	=> '173986',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://herbshealthcare.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://herbshealthcare.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	38704 => [
		'terraleadsApiKey'			=> 'b946be40ea410452e5a3276ebb1d35ea',
		'webmasterID'				=> 5,
		
		'offers' => [
			5066 => [ // Maral Gel - DZ
				'token'				=> 'terraleads',
				'goodId'           	=> '177325',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://hamdi2021.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://hamdi2021.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	38984 => [
		'terraleadsApiKey'			=> 'c578c8905ccd63a9c3c6a3e44b15c0a9',
		'webmasterID'				=> 2,
		
		'offers' => [
			4968 => [ // Xtreme man - CO
				'token'				=> 'terraleads',
				'goodId'           	=> '177471',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://tecnologia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://tecnologia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5724 => [ // Xtreme man low price - CO
				'token'				=> 'terraleads',
				'goodId'           	=> '177471',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://tecnologia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://tecnologia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4967 => [ // Bioprost - CO
				'token'				=> 'terraleads',
				'goodId'           	=> '177472',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://tecnologia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://tecnologia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	34894 => [
		'terraleadsApiKey'			=> 'd29eee76726f58c72c1a369f0645369f',
		'webmasterID'				=> 1,
		
		'offers' => [
			2843 => [ // RockErect - CI
				'token'				=> 'terraleads',
				'goodId'           		=> '172673',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://healthtradegroup-civ.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://healthtradegroup-civ.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	35288 => [
		'terraleadsApiKey'			=> '1f6470d3727963a74923e4fccd857c9a',
		'webmasterID'				=> 3,
		
		'offers' => [
			0000 => [ // Bioprost - CO
				'token'				=> 'terraleads',
				'goodId'           		=> '172127',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ideasecommerce.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ideasecommerce.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			0000 => [ // Xtreme man - CO
				'token'				=> 'terraleads',
				'goodId'           		=> '170623',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ideasecommerce.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ideasecommerce.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	28004 => [
		'terraleadsApiKey'			=> '2ccd2bacc3ff2c31b95b0ae477ab4e8b',
		'webmasterID'				=> 3,
		
		'offers' => [
			4921 => [ // Erectol Mass - PE
				'token'				=> 'terraleads',
				'goodId'           		=> '172447',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://terra-erectolmass.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://terra-erectolmass.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4969 => [ // Fatachet Kem Grass - PE
				'token'				=> 'terraleads',
				'goodId'           		=> '172223',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://terra-erectolmass.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://terra-erectolmass.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	45507 => [
		'terraleadsApiKey'			=> 'e7227944a1e21e9b37b1567874ce30c4',
		'webmasterID'				=> 3,
		
		'offers' => [
			6217 => [ // Noxida - ID
				'token'				=> 'terraleads',
				'goodId'           		=> '185283',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://telmark.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://telmark.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6218 => [ // Esamax - ID
				'token'				=> 'terraleads',
				'goodId'           		=> '185282',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://telmark.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://telmark.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6455 => [ // Hiperol - ID
				'token'				=> 'terraleads',
				'goodId'           		=> '186742',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://telmark.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://telmark.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4065 => [ // Hermuno - ID
				'token'				=> 'terraleads',
				'goodId'           		=> '186743',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://telmark.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://telmark.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	30566 => [
		'terraleadsApiKey'			=> '5c4ef6a04f512937e5839ffc70efbe93',
		'webmasterID'				=> 3,
		
		'offers' => [
			7007 => [ //Diabet Pro - MM
				'token'				=> 'terraleads',
				'goodId'           		=> '194075',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://myanmar.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://myanmar.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7019 => [ //UltraHot - SG
				'token'				=> 'terraleads',
				'goodId'           		=> '173843',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://singapore.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://singapore.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6948 => [ //OptoCare - MY
				'token'				=> 'terraleads',
				'goodId'           		=> '193793',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://6001.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://6001.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6917 => [ //Green Slim - MM
				'token'				=> 'terraleads',
				'goodId'           		=> '185190',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://myanmar.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://myanmar.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6207 => [ //I-Care - SG
				'token'				=> 'terraleads',
				'goodId'           		=> '183608',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://singapore.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://singapore.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6206 => [ //I-Care - MY
				'token'				=> 'terraleads',
				'goodId'           		=> '183606',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://6001.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://6001.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6205 => [ //Slim Fit - SG
				'token'				=> 'terraleads',
				'goodId'           		=> '183607',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://singapore.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://singapore.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6204 => [ //Slim Fit - MY
				'token'				=> 'terraleads',
				'goodId'           		=> '183605',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://6001.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://6001.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5957 => [ //HorseMEN - MM
				'token'				=> 'terraleads',
				'goodId'           		=> '184147',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://myanmar.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://myanmar.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5447 => [ //Green Coffee Bean - MY
				'token'				=> 'terraleads',
				'goodId'           		=> '177384',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://6001.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://6001.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5127 => [ //Green Coffee Bean - SG
				'token'				=> 'terraleads',
				'goodId'           		=> '173841',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://singapore.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://singapore.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5122 => [ //UltraHot - MY
				'token'				=> 'terraleads',
				'goodId'           		=> '168787',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://6001.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://6001.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5123 => [ //HorseMEN - MY
				'token'				=> 'terraleads',
				'goodId'           		=> '173852',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://6001.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://6001.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5129 => [ //HorseMEN - SG
				'token'				=> 'terraleads',
				'goodId'           		=> '173851',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://singapore.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://singapore.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5124 => [ //V-Cano - SG
				'token'				=> 'terraleads',
				'goodId'           		=> '173850',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://singapore.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://singapore.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5125 => [ //B-Flexer - SG
				'token'				=> 'terraleads',
				'goodId'           		=> '173842',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://singapore.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://singapore.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5128 => [ //Maxi Strong - SG
				'token'				=> 'terraleads',
				'goodId'           		=> '173838',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://singapore.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://singapore.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	33461 => [
		'terraleadsApiKey'			=> '50cc7ff1062018b5346795be220601e7',
		'webmasterID'				=> 2,
		
		'offers' => [
			4848 => [ // Bionature Anti Cigarette - DZ
				'token'				=> 'terraleads',
				'goodId'           		=> '171265',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bionature.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bionature.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4851 => [ // Bionature Hair Serum - SA
				'token'				=> 'terraleads',
				'goodId'           		=> '000000',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bionature.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bionature.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4853 => [ // Bionature Hair Serum - KW
				'token'				=> 'terraleads',
				'goodId'           		=> '000000',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bionature.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bionature.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4852 => [ // Bionature Hair Serum - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '000000',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bionature.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bionature.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	33501 => [
		'terraleadsApiKey'			=> 'bd16d1307a4ca4bfec4c48875811e1b1',
		'webmasterID'				=> 3,
		
		'offers' => [
			4856 => [ // Herbal Gel Minceur - MA
				'token'				=> 'terraleads',
				'goodId'           		=> '171410',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://pandol.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://pandol.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4854 => [ // Fume Elixir - MA
				'token'				=> 'terraleads',
				'goodId'           		=> '171408',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://pandol.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://pandol.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4850 => [ // Miracle Oil - MA
				'token'				=> 'terraleads',
				'goodId'           		=> '171411',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://pandol.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://pandol.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4849 => [ // Herbo Respir - MA
				'token'				=> 'terraleads',
				'goodId'           		=> '171409',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://pandol.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://pandol.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4923 => [ // Rhino Gold Oil - MA
				'token'				=> 'terraleads',
				'goodId'           		=> '172117',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://pandol.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://pandol.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4922 => [ // Harmonica - MA
				'token'				=> 'terraleads',
				'goodId'           		=> '172118',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://pandol.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://pandol.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4879 => [ // Black Snake Oil
				'token'				=> 'terraleads',
				'goodId'           		=> '171779',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://pandol.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://pandol.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	32063 => [
		'terraleadsApiKey'			=> '0c4dd85250d7577dbbd6f2156bb29ca0',
		'webmasterID'				=> 569,
		
		'offers' => [
			7581 => [ // Visitec - KZ
				'token'				=> 'terraleads',
				'goodId'           		=> '39663',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://visitec.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://visitec.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7644 => [ // Erectil Cream - KZ
				'token'				=> 'terraleads',
				'goodId'           		=> '39666',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://erectil-cream.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://erectil-cream.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7516 => [ // Alfagen - KZ
				'token'				=> 'terraleads',
				'goodId'           		=> '39667',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://alfagen-kz.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://alfagen-kz.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7515 => [ // Gluconax - KZ
				'token'				=> 'terraleads',
				'goodId'           		=> '39665',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://gluconax.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://gluconax.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7514 => [ // Erectil - KZ
				'token'				=> 'terraleads',
				'goodId'           		=> '39660',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://erectil-tablets.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://erectil-tablets.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7513 => [ // Cardio A - KZ
				'token'				=> 'terraleads',
				'goodId'           		=> '39659',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cardio.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cardio.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7512 => [ // Diabex - KZ
				'token'				=> 'terraleads',
				'goodId'           		=> '39658',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://diabex.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://diabex.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7256 => [ // Leptigen Meridian Diet - KZ1
				'token'				=> 'terraleads',
				'goodId'           		=> '34718',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://leptigen-diet.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://leptigen-diet.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7221 => [ // Wow Bra - KZ
				'token'				=> 'terraleads',
				'goodId'           		=> '35133',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://angel-bra.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://angel-bra.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7220 => [ // Transformer - KZ
				'token'				=> 'terraleads',
				'goodId'           		=> '36226',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://robot-car.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://robot-car.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7219 => [ // Tiger Wrench - KZ
				'token'				=> 'terraleads',
				'goodId'           		=> '33646',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://tiger-wrench.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://tiger-wrench.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4651 => [ // Mycelix - IT
				'token'				=> 'terraleads',
				'goodId'           		=> '32292',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://burzh-mycelix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://burzh-mycelix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4650 => [ // Getsize - IT
				'token'				=> 'terraleads',
				'goodId'           		=> '31774',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://de-getsize.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://de-getsize.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4649 => [ // Getsize - DE
				'token'				=> 'terraleads',
				'goodId'           		=> '31774',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://de-getsize.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://de-getsize.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3296 => [ // Getsize - RO
				'token'				=> 'terraleads',
				'goodId'           		=> '31774',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://de-getsize.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://de-getsize.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4662 => [ // Personal Slim - DE
				'token'				=> 'terraleads',
				'goodId'           		=> '32288',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://burzh-personal-slim.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://burzh-personal-slim.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4708 => [ //  Personal Slim - IT
				'token'				=> 'terraleads',
				'goodId'           		=> '164311',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://it-personal-slim.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://it-personal-slim.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	31539 => [
		'terraleadsApiKey'			=> 'b437706ae55d8ba26ce8c9a8b0255e38',
		'webmasterID'				=> 5,
		
		'offers' => [
			4505 => [ // Dr Skin Care - BD
				'token'				=> 'terraleads',
				'goodId'           		=> '161962',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://drskincare.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://drskincare.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	45234 => [
		'terraleadsApiKey'			=> '98174188a0f368ad7183e3cc2c803b8a',
		'webmasterID'				=> 4,
		
		'offers' => [
			6251 => [ // BioMan Power - CO
				'token'				=> 'terraleads',
				'goodId'           		=> '182040',
				'coment'            => 'prostatitis',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutrabase.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutrabase.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6349 => [ // BioMan Power - CO
				'token'				=> 'terraleads',
				'goodId'           		=> '182040',
				'coment'            => 'potency',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutrabase.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutrabase.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],

		31708 => [
		'terraleadsApiKey'			=> '9486b5aa0e4694c984d8cc35e65e92ec',
		'webmasterID'				=> 9,
		
		'offers' => [
			4528 => [ // King X max - VN
				'token'				=> 'terraleads',
				'goodId'           		=> '167727',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://vn0001.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://vn0001.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4529 => [ // Hondrocream - VN
				'token'				=> 'terraleads',
				'goodId'           		=> '167729',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://vn0001.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://vn0001.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4530 => [ // Kenton diet - VN
				'token'				=> 'terraleads',
				'goodId'           		=> '167730',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://vn0001.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://vn0001.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4531 => [ // Cardiol - VN
				'token'				=> 'terraleads',
				'goodId'           		=> '167731',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://vn0001.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://vn0001.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4532 => [ // Dianol - VN
				'token'				=> 'terraleads',
				'goodId'           		=> '167732',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://vn0001.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://vn0001.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4533 => [ // Clean vision - VN
				'token'				=> 'terraleads',
				'goodId'           		=> '167733',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://vn0001.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://vn0001.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4534 => [ // Detoxant - VN
				'token'				=> 'terraleads',
				'goodId'           		=> '167735',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://vn0001.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://vn0001.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	29556 => [
		'terraleadsApiKey'			=> '2a0037de4e3e9888ea9dc70823e539c1',
		'webmasterID'				=> 6,
		
		'offers' => [
			4087 => [ // Dr.Maxx Pro - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '162074',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutralytes.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutralytes.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	31087 => [
		'terraleadsApiKey'			=> '6448328070deb8065b898f2cd35fe80d',
		'webmasterID'				=> 3,
		
		'offers' => [
			4401 => [ // Energy 365 - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '34073',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://energy365.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://energy365.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	30727 => [
		'terraleadsApiKey'			=> '2dd2310ace0b6872967e1f5f43fa2a63',
		'webmasterID'				=> 14,
		
		'offers' => [
			4331 => [ // Anand - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '166121',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://simpleways.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://simpleways.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	44358 => [
		'terraleadsApiKey'			=> 'ce2fbf9c06360da601b62538766e9948',
		'webmasterID'				=> 27,
		
		'offers' => [
			5988 => [ // EroForce - KH
				'token'				=> 'terraleads',
				//'goodId'           		=> '166121',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cpame.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cpame.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	47939 => [
		'terraleadsApiKey'			=> '3f77a3acbfae29f73265e37ccdb956b5',
		'webmasterID'				=> 5,
		
		'offers' => [
			6679 => [ // Detonic - KE
				'token'				=> 'terraleads',
				'goodId'           		=> '188322',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://komfyshop.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://komfyshop.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6680 => [ // Detonic - TZ
				'token'				=> 'terraleads',
				'goodId'           		=> '188322',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://komfyshop.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://komfyshop.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6681 => [ // CardiLine - KE
				'token'				=> 'terraleads',
				'goodId'           		=> '188205',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://komfyshop.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://komfyshop.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6682 => [ // CardiLine - GH
				'token'				=> 'terraleads',
				'goodId'           		=> '192198',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://komfyshop.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://komfyshop.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6683 => [ // CardiLine - TZ
				'token'				=> 'terraleads',
				'goodId'           		=> '188205',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://komfyshop.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://komfyshop.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	47953 => [
		'terraleadsApiKey'			=> 'baa4a7c6df855c90285747eba43a2a0f',
		'webmasterID'				=> 5,
		
		'offers' => [
			6482 => [ // Delta Man - SA
				'token'				=> 'terraleads',
				'goodId'           		=> '185983',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://silkroad.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://silkroad.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6853 => [ // Delta Pro - LB
				'token'				=> 'terraleads',
				'goodId'           		=> '187104',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://silkroad.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://silkroad.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6608 => [ // Delta Man - LB
				'token'				=> 'terraleads',
				'goodId'           		=> '185983',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://silkroad.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://silkroad.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	48707 => [
		'terraleadsApiKey'			=> '4a9f6af630cb2ed82155a7cc99a075ce',
		'webmasterID'				=> 31,
		
		'offers' => [
			6585 => [ // MalPlus - CI
				'token'				=> 'terraleads',
				'goodId'           		=> '188065',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africanewpro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africanewpro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6584 => [ // MalPlus - SN
				'token'				=> 'terraleads',
				'goodId'           		=> '188064',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africanewpro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africanewpro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	40438 => [
		'terraleadsApiKey'			=> '166690fb1de522bde00e9a6f40e85dae',
		'webmasterID'				=> 5,
		
		'offers' => [
			8177 => [ //Cystenon - LT
				'token'					=> 'terraleads',
				'offer_name'			=> 'Cystenon - LT',
				'goodId'           		=> '205461',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9186 => [ //Reliver - LT (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Reliver - LT (low price)',
				'goodId'           		=> '208424',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9185 => [ //Reliver - LV (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Reliver - LV (low price)',
				'goodId'           		=> '208425',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9184 => [ //Cystenon - LT (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Cystenon - LT (low price)',
				'goodId'           		=> '208422',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9183 => [ //Cystenon - LV (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Cystenon - LV (low price)',
				'goodId'           		=> '208423',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9182 => [ //Mennex - LT (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Mennex - LT (low price)',
				'goodId'           		=> '208414',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9181 => [ //Mennex - LV (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Mennex - LV (low price)',
				'goodId'           		=> '208415',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9180 => [ //OstyHealth - LT (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'OstyHealth - LT (low price)',
				'goodId'           		=> '208418',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9179 => [ //OstyHealth - LV (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'OstyHealth - LV (low price)',
				'goodId'           		=> '208419',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9178 => [ //Keto Light - LT (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Keto Light - LT (low price)',
				'goodId'           		=> '208416',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9177 => [ //Keto Light - LV (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Keto Light - LV (low price)',
				'goodId'           		=> '208417',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9176 => [ //Cardio A - LT (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Cardio A - LT (low price)',
				'goodId'           		=> '208412',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9175 => [ //Cardio A - LV (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Cardio A - LV (low price)',
				'goodId'           		=> '208413',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9137 => [ //Collagent Premium - LT (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Collagent Premium - LT (low price)',
				'goodId'           		=> '209327',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9136 => [ //Collagent Premium - LV (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Collagent Premium - LV (low price)',
				'goodId'           		=> '209328',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9134 => [ //Collagent Premium - LV
				'token'					=> 'terraleads',
				'offer_name'			=> 'Collagent Premium - LV',
				'goodId'           		=> '209326',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9135 => [ //Collagent Premium - LT
				'token'					=> 'terraleads',
				'offer_name'			=> 'Collagent Premium - LT',
				'goodId'           		=> '209325',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9132 => [ //Collagent Premium - LV 5 EUR(low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Collagent Premium - LV 5 EUR (low price)',
				'goodId'           		=> '209321',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9133 => [ //Collagent Premium - LT 5 EUR(low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Collagent Premium - LT 5 EUR (low price)',
				'goodId'           		=> '209320',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9129 => [ //Mennex 5 EUR - LV (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Mennex 5 EUR - LV (low price)',
				'goodId'           		=> '208703',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			9128 => [ //Mennex 5 EUR - LT (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Mennex 5 EUR - LT (low price)',
				'goodId'           		=> '208702',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8789 => [ //Diabex - LT 
				'token'					=> 'terraleads',
				'offer_name'			=> 'Diabex - LT',
				'goodId'           		=> '205463',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8790 => [ //Diabex - LV
				'token'					=> 'terraleads',
				'offer_name'			=> 'Diabex - LV',
				'goodId'           		=> '205464',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6518 => [ //Germivir - LT (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Germivir - LT (low price)',
				'goodId'           		=> '205450',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6517 => [ //Germivir - LV (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Germivir - LV (low price)',
				'goodId'           		=> '205451',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8176 => [ //Cystenon - LV
				'token'					=> 'terraleads',
				'offer_name'			=> 'Cystenon - LV',
				'goodId'           		=> '205462',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8175 => [ //Cystenon - LV (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Cystenon - LV (low price)',
				'goodId'           		=> '202650',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8174 => [ //Cystenon - LT (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Cystenon - LT (low price)',
				'goodId'           		=> '202648',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8181 => [ //Reliver - LV
				'token'					=> 'terraleads',
				'offer_name'			=> 'Reliver - LV',
				'goodId'           		=> '202655',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8180 => [ //Reliver - LT
				'token'					=> 'terraleads',
				'offer_name'			=> 'Reliver - LT',
				'goodId'           		=> '202653',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8179 => [ //Reliver - LT (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Reliver - LT (low price)',
				'goodId'           		=> '202652',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8178 => [ //Reliver - LV (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Reliver - LV (low price)',
				'goodId'           		=> '202654',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8182 => [ //Psoryden - LT
				'token'					=> 'terraleads',
				'offer_name'			=> 'Psoryden - LT',
				'goodId'           		=> '202657',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8184 => [ //Psoryden - LV
				'token'					=> 'terraleads',
				'offer_name'			=> 'Psoryden - LV',
				'goodId'           		=> '202659',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8183 => [ //Psoryden - LT (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Psoryden - LT (low price)',
				'goodId'           		=> '202656',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8185 => [ //Psoryden - LV (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Psoryden - LV (low price)',
				'goodId'           		=> '202658',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7393 => [ //Cardio A - LV (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Cardio Life - LV (low price)',
				'goodId'           		=> '197645',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7965 => [ //Cardio A - LV 
				'token'					=> 'terraleads',
				'offer_name'			=> 'Cardio A - LV',
				'goodId'           		=> '205460',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7966 => [ //Cardio A - LT 
				'token'					=> 'terraleads',
				'offer_name'			=> 'Cardio A - LT',
				'goodId'           		=> '205459',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7963 => [ //OstyHealth - LT 
				'token'					=> 'terraleads',
				'offer_name'			=> 'OstyHealth - LT',
				'goodId'           		=> '200687',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7964 => [ //OstyHealth - LV
				'token'					=> 'terraleads',
				'offer_name'			=> 'OstyHealth - LV',
				'goodId'           		=> '200688',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7962 => [ //Big Size - LV
				'token'					=> 'terraleads',
				'offer_name'			=> 'Big Size - LV',
				'goodId'           		=> '200692',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7961 => [ //Big Size - LT
				'token'					=> 'terraleads',
				'offer_name'			=> 'Big Size - LT',
				'goodId'           		=> '200691',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5867 => [ //Erectil - LV
				'token'					=> 'terraleads',
				'offer_name'			=> 'Erectil - LV',
				'goodId'           		=> '200690',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5884 => [ //Erectil - LT
				'token'					=> 'terraleads',
				'offer_name'			=> 'Erectil - LT',
				'goodId'           		=> '200689',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6512 => [ //Fungostop + - LV (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Fungostop + - LV (low price)',
				'goodId'           		=> '200232',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6513 => [ //Fungostop + - LT (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Fungostop + - LT (low price)',
				'goodId'           		=> '200233',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7394 => [ //Cardio A - LT (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Cardio A - LT (low price)',
				'goodId'           		=> '197644',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6433 => [ //Cardio Life - LV (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Cardio Life - LV (low price)',
				'goodId'           		=> '187962',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6448 => [ //Cardio Life - LT (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Cardio Life - LT (low price)',
				'goodId'           		=> '187962',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6754 => [ //Prostanol - LV (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Prostanol - LV (low price)',
				'goodId'           		=> '192463',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6755 => [ //Prostanol - LT (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Prostanol - LT (low price)',
				'goodId'           		=> '192463',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6469 => [ //Keto Light - LT (low price) (ru)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Keto Light - LT (low price) (ru)',
				'goodId'           		=> '187955',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6470 => [ //Keto Light - LV (low price) (ru)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Keto Light - LV (low price) (ru)',
				'goodId'           		=> '187955',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6471 => [ //Keto Light - EE (low price) (ru)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Keto Light - EE (low price) (ru)',
				'goodId'           		=> '187955',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6472 => [ //Keto Light - LT (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Keto Light - LT (low price)',
				'goodId'           		=> '187971',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6473 => [ //Keto Light - LV (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Keto Light - LV (low price)',
				'goodId'           		=> '187971',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6474 => [ //Big Size - LT (low price) (ru)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Big Size - LT (low price) (ru)',
				'goodId'           		=> '187958',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6475 => [ //Big Size - LV (low price) (ru)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Big Size - LV (low price) (ru)',
				'goodId'           		=> '187958',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6483 => [ //Big Size - EE (low price) (ru)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Big Size - EE (low price) (ru)',
				'goodId'           		=> '187958',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6484 => [ //Big Size - LT (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Big Size - LT (low price)',
				'goodId'           		=> '203903',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6485 => [ //Big Size - LV (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Big Size - LV (low price)',
				'goodId'           		=> '187972',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6486 => [ //OstyHealth - LT (low price) (ru)
				'token'					=> 'terraleads',
				'offer_name'			=> 'OstyHealth - LT (low price) (ru)',
				'goodId'           		=> '187959',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6487 => [ //OstyHealth - LV (low price) (ru)
				'token'					=> 'terraleads',
				'offer_name'			=> 'OstyHealth - LV (low price) (ru)',
				'goodId'           		=> '187959',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6488 => [ //OstyHealth - EE (low price) (ru)
				'token'					=> 'terraleads',
				'offer_name'			=> 'OstyHealth - EE (low price) (ru)',
				'goodId'           		=> '187959',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6489 => [ //OstyHealth - LT (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'OstyHealth - LT (low price)',
				'goodId'           		=> '188297',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6490 => [ //OstyHealth - LV (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'OstyHealth - LV (low price)',
				'goodId'           		=> '188297',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6493 => [ //Erectil - LT (low price) (ru)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Erectil - LT (low price) (ru)',
				'goodId'           		=> '187960',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6494 => [ //Erectil - LV (low price) (ru)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Erectil - LV (low price) (ru)',
				'goodId'           		=> '187960',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6495 => [ //Erectil - EE (low price) (ru)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Erectil - EE (low price) (ru)',
				'goodId'           		=> '187960',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6491 => [ //Erectil - LT (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Erectil - LT (low price)',
				'goodId'           		=> '187961',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6492 => [ //Erectil - LV (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Erectil - LV (low price)',
				'goodId'           		=> '187961',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5532 => [ //Keto Star - LT
				'token'					=> 'terraleads',
				'offer_name'			=> 'Keto Star - LT',
				'goodId'           		=> '175459',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5531 => [ //Keto Star - LV
				'token'					=> 'terraleads',
				'offer_name'			=> 'Keto Star - LV',
				'goodId'           		=> '175460',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5541 => [ //Provimax - LT
				'token'					=> 'terraleads',
				'offer_name'			=> 'Provimax - LT',
				'goodId'           		=> '178669',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5540 => [ //Provimax - LV
				'token'					=> 'terraleads',
				'offer_name'			=> 'Provimax - LV',
				'goodId'           		=> '188017',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5539 => [ //Provimax - EE
				'token'					=> 'terraleads',
				'offer_name'			=> 'Provimax - EE',
				'goodId'           		=> '178674',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5538 => [ //Dzi Bead - LT
				'token'					=> 'terraleads',
				'offer_name'			=> 'Dzi Bead - LT',
				'goodId'           		=> '175572',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5537 => [ //Dzi Bead - LV
				'token'					=> 'terraleads',
				'offer_name'			=> 'Dzi Bead - LV',
				'goodId'           		=> '175573',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5536 => [ //Dzi Bead - EE
				'token'					=> 'terraleads',
				'offer_name'			=> 'Dzi Bead - EE',
				'goodId'           		=> '175574',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5529 => [ //Money Amulet - LT
				'token'					=> 'terraleads',
				'offer_name'			=> 'Money Amulet - LT',
				'goodId'           		=> '175561',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5528 => [ //Money Amulet - LV
				'token'					=> 'terraleads',
				'offer_name'			=> 'Money Amulet - LV',
				'goodId'           		=> '175562',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5527 => [ //Money Amulet - EE
				'token'					=> 'terraleads',
				'offer_name'			=> 'Money Amulet - EE',
				'goodId'           		=> '175563',
				'externalWebmaster'		=> function ($model, $config) {
					 return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5530 => [ //Keto Star - EE
				'token'					=> 'terraleads',
				'offer_name'			=> 'Keto Star - EE',
				'goodId'           		=> '187955',
				 'externalWebmaster'		=> function ($model, $config) {
					 return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6431 => [ //Diabex low price - LV
				'token'					=> 'terraleads',
				'offer_name'			=> 'Diabex low price - LV',
				'goodId'           		=> '186974',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6445 => [ //Diabex low price - LT
				'token'					=> 'terraleads',
				'offer_name'			=> 'Diabex low price - LT (low price)',
				'goodId'           		=> '189902',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6432 => [ //Diabex low price (ru) - LV
				'token'					=> 'terraleads',
				'offer_name'			=> 'Diabex low price (ru) - LV',
				'goodId'           		=> '186844',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6446 => [ //Diabex low price (ru) - LT
				'token'					=> 'terraleads',
				'offer_name'			=> 'Diabex low price (ru) - LT',
				'goodId'           		=> '186843',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6447 => [ //Diabex low price (ru) - EE
				'token'					=> 'terraleads',
				'offer_name'			=> 'Diabex low price (ru) - EE',
				'goodId'           		=> '186845',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6450 => [ //Cardio Life low price (ru) - EE
				'token'					=> 'terraleads',
				'offer_name'			=> 'Cardio Life low price (ru) - EE',
				'goodId'           		=> '187166',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6449 => [ //Cardio Life low price (ru) - LT
				'token'					=> 'terraleads',
				'offer_name'			=> 'Cardio Life low price (ru) - LT',
				'goodId'           		=> '187164',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6434 => [ //Cardio Life low price (ru) - LV
				'token'					=> 'terraleads',
				'offer_name'			=> 'Cardio Life low price (ru) - LV',
				'goodId'           		=> '187165',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6969 => [ //Erectil - FI (low price) (ru)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Erectil - FI (low price) (ru)',
				'goodId'           		=> '194862',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6970 => [ //OstyHealth - FI (low price) (ru)
				'token'					=> 'terraleads',
				'offer_name'			=> 'OstyHealth - FI (low price) (ru)',
				'goodId'           		=> '194863',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6971 => [ //Big Size - FI (low price) (ru)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Big Size - FI (low price) (ru)',
				'goodId'           		=> '194864',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6972 => [ //Keto Light - FI (low price) (ru)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Keto Light - FI (low price) (ru)',
				'goodId'           		=> '194865',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6973 => [ //Cardio Life - FI (low price) (ru)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Cardio Life - FI (low price) (ru)',
				'goodId'           		=> '194866',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6974 => [ //Diabex - FI (low price) (ru)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Diabex - FI (low price) (ru)',
				'goodId'           		=> '194867',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7144 => [ //Diabex - FI (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Diabex - FI (low price)',
				'goodId'           		=> '198491',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7395 => [ //Cardio A - FI (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Cardio A - FI (low price)',
				'goodId'           		=> '198494',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7148 => [ //OstyHealth - FI (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'OstyHealth - FI (low price',
				'goodId'           		=> '198492',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7149 => [ //Erectil - FI (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Erectil - FI (low price)',
				'goodId'           		=> '198493',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7146 => [ //Keto Light - FI (low price)
				'token'					=> 'terraleads',
				'offer_name'			=> 'Keto Light - FI (low price)',
				'goodId'           		=> '198496',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7626 => [ //OstyHealth - FI
				'token'					=> 'terraleads',
				'offer_name'			=> 'OstyHealth - FI',
				'goodId'           		=> '198480',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5734 => [ //Keto Light + - FI
				'token'					=> 'terraleads',
				'offer_name'			=> 'Keto Light + - FI',
				'goodId'           		=> '198484',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5736 => [ //Erectil - FI
				'token'					=> 'terraleads',
				'offer_name'			=> 'Erectil - FI',
				'goodId'           		=> '198481',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7627 => [ //Cardio A - FI
				'token'					=> 'terraleads',
				'offer_name'			=> 'Cardio A - FI',
				'goodId'           		=> '198482',
				'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://cmd.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	30920 => [
		'terraleadsApiKey'			=> '00af6f3ae29cc6eb69a50ba46f52ff61',
		'webmasterID'				=> 8,
		
		'offers' => [
			6581 => [ // Varistop - RS
				'token'				=> 'terraleads',
				'goodId'           		=> '37434',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://varistop.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://varistop.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6582 => [ // Gemmostop - RS
				'token'				=> 'terraleads',
				'goodId'           		=> '36851',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://gemostop.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://gemostop.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6580 => [ // Pantodex - RS
				'token'				=> 'terraleads',
				'goodId'           		=> '35351',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://pantofex.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://pantofex.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6579 => [ // Mikofortduo - RS
				'token'				=> 'terraleads',
				'goodId'           		=> '38238',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://mikofort-duo1.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://mikofort-duo1.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6578 => [ // Arthrozdrav - RS
				'token'				=> 'terraleads',
				'goodId'           		=> '38070',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://artrozdrav.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://artrozdrav.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4370 => [ // Artrosan - RS
				'token'				=> 'terraleads',
				'goodId'           		=> '31941',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://artrosan1.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://artrosan1.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4369 => [ // Saluven - RS
				'token'				=> 'terraleads',
				'goodId'           		=> '32459',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://saluven.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://saluven.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	32121 => [
		'terraleadsApiKey'			=> '3353a801eb1c786a40a920f585e67141',
		'webmasterID'				=> 5,
		
		'offers' => [
			4636 => [ // Ayurvedic 69 Booster - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '169192',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://69boostercrm.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://69boostercrm.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4635 => [ // SlimFit Pro - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '169191',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://69boostercrm.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://69boostercrm.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	30099 => [
		'terraleadsApiKey'			=> '5aab13c27400eb747a93de5be529f8cf',
		'webmasterID'				=> 31,
		
		'offers' => [
			4339 => [ // Ostio - UZ
				'token'				=> 'terraleads',
				'goodId'           		=> '47876',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://uzbekistan.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://uzbekistan.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4344 => [ // Xpress Control 4D - UZ
				'token'				=> 'terraleads',
				'goodId'           		=> '35269',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://uzbekistan.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://uzbekistan.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4345 => [ // Soft Skin - UZ
				'token'				=> 'terraleads',
				'goodId'           		=> '163902',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://uzbekistan.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://uzbekistan.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4346 => [ // Zumba 4in1 - UZ
				'token'				=> 'terraleads',
				'goodId'           		=> '73797',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://uzbekistan.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://uzbekistan.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4347 => [ // Vibroaction - UZ
				'token'				=> 'terraleads',
				'goodId'           		=> '121232',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://uzbekistan.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://uzbekistan.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	29278 => [
		'terraleadsApiKey'			=> 'bb3d0be6016e1552879c2d24f2b5a338',
		'webmasterID'				=> 1,
		
		'offers' => [
			4081 => [ // Holerostit - BA
				'token'				=> 'terraleads',
				'goodId'           		=> '',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://importiva.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://importiva.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	27266 => [
		'terraleadsApiKey'			=> '12ca8fdffd6ac2e997e8f8edd85bc1c9',
		'webmasterID'				=> 2,
		
		'offers' => [
			6132	 => [ // Steelovil - EG
				'token'				=> 'terraleads',
				'goodId'           		=> '32445',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://organic-healthy.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://organic-healthy.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5450	 => [ // Slim latte - EG
				'token'				=> 'terraleads',
				'goodId'           		=> '177770',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimlatte.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimlatte.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4962	 => [ // Alfagen - EG
				'token'				=> 'terraleads',
				'goodId'           		=> '172227',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://alfagencream.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://alfagencream.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4910 => [ // Dirmy chill - EG
				'token'				=> 'terraleads',
				'goodId'           		=> '169703',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://dirmy-chell.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://dirmy-chell.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4003 => [ // Green coffee fit - EG
				'token'				=> 'terraleads',
				'goodId'           		=> '155283',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://greencoffeefit.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://greencoffeefit.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4575 => [ // Vitality - EG
				'token'				=> 'terraleads',
				'goodId'           		=> '167630',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://vitaliti.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://vitaliti.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5302 => [ // Moringa - EG
				'token'				=> 'terraleads',
				'goodId'           		=> '167630',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://moringa-oleifera.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://moringa-oleifera.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4669 => [ // Torino drago - EG
				'token'				=> 'terraleads',
				'goodId'           		=> '169704',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://torino-drago.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://torino-drago.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4672 => [ // Easy move - EG
				'token'				=> 'terraleads',
				'goodId'           		=> '165626',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://esymove.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://esymove.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4047 => [ // #3018 - Monina Cream - EG
				'token'				=> 'terraleads',
				'goodId'           		=> '155284',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://monina-cream.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://monina-cream.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
            4210 => [ // Qeffect - EG
                'token'				    => 'terraleads',
                'goodId'           		=> '163871',
                'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
                'configs' => [
                    'brakeLogFolder'	=> true,
                    'urlOrderAdd'		=> 'https://qeffect.leadvertex.ru/api/webmaster/v2/addOrder.html',
                    'urlOrderInfo'		=> 'https://qeffect.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
                ],
            ],
			4088 => [ // Torpedo - EG
				'token'				=> 'terraleads',
				'goodId'           		=> '155485',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://torbedo2.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://torbedo2.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
            ],
        ],
	],
	27622 => [
		'terraleadsApiKey'			=> 'c848dba94aad4695848f044ace5a1d8d',
		'webmasterID'				=> 2,
		
		'offers' => [
			3862 => [ // Monster Pro - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '156578',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://monsterpro1.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://monsterpro1.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4042 => [ // Fit Odin - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '33090',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://fitodin1.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://fitodin1.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	27614 => [
		'terraleadsApiKey'			=> 'c2d556d6e0204490f0e68635c8dde9cb',
		'webmasterID'				=> 1,
		
		'offers' => [
			3838 => [ // 69 Booster - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '155640',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://69booster.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://69booster.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	31628 => [
		'terraleadsApiKey'			=> 'e61a1e65319962a52e6f35e69b5f616a',
		'webmasterID'				=> 1,
		
		'offers' => [
			4641 => [ // Tiger_X - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '34498',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://tigerx.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://tigerx.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	26939 => [ // TRIRAZAT - RU
		'terraleadsApiKey'			=> '1ef7e3bdd60dccbe83ca53c14f4e9b76',
		'webmasterID'				=> 2,
		
		'offers' => [
			3953 => [ // Aminocarnit - PL
				'token'				=> 'terraleads',
				'goodId'           		=> '156950',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3952 => [ // Aminocarnit - LT
				'token'				=> 'terraleads',
				'goodId'           		=> '157858',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3951 => [ // Aminocarnit - LV
				'token'				=> 'terraleads',
				'goodId'           		=> '157859',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3950 => [ // Aminocarnit - BY
				'token'				=> 'terraleads',
				'goodId'           		=> '157857',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3819 => [ // Money Amulet - RU (BY)
				'token'				=> 'terraleads',
				'goodId'           		=> '153381',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3803 => [ // Money Amulet - RU (FI)
				'token'				=> 'terraleads',
				'goodId'           		=> '000000',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3805 => [ // Money Amulet - RU (LV)
				'token'				=> 'terraleads',
				'goodId'           		=> '00000',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3804 => [ // Money Amulet - RU (LT)
				'token'				=> 'terraleads',
				'goodId'           		=> '000000',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3820 => [ // Bronix Beauty - RU (BY)
				'token'				=> 'terraleads',
				'goodId'           		=> '153989',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3808 => [ // Bronix Beauty - RU (FI)
				'token'				=> 'terraleads',
				'goodId'           		=> '155230',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3810 => [ // Bronix Beauty - RU (LV)
				'token'				=> 'terraleads',
				'goodId'           		=> '155229',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3809 => [ // Bronix Beauty - RU (LT)
				'token'				=> 'terraleads',
				'goodId'           		=> '155228',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3818 => [ // Codirex - RU (BY)
				'token'				=> 'terraleads',
				'goodId'           		=> '153419',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3677 => [ // Codirex - RU (FI)
				'token'				=> 'terraleads',
				'goodId'           		=> '153787',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3640 => [ // Codirex - RU (LV)
				'token'				=> 'terraleads',
				'goodId'           		=> '153791',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3639 => [ // Codirex - RU (LT)
				'token'				=> 'terraleads',
				'goodId'           		=> '153422',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3817 => [ // Biosistem - RU (BY)
				'token'				=> 'terraleads',
				'goodId'           		=> '153228',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3638 => [ // Biosistem - RU (FI)
				'token'				=> 'terraleads',
				'goodId'           		=> '153760',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3637 => [ // Biosistem - RU (LV)
				'token'				=> 'terraleads',
				'goodId'           		=> '153766',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3636 => [ // Biosistem - RU (LT)
				'token'				=> 'terraleads',
				'goodId'           		=> '153227',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3832 => [ // Biomanix - FI длинная форма
				'token'				=> 'terraleads',
				'goodId'           		=> '156797',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
//				'postIndex' => function ($model, $config) {
//					return $model->zip;
//				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3816 => [ // Biomanix - RU (BY)
				'token'				=> 'terraleads',
				'goodId'           		=> '153332',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3635 => [ // Biomanix - RU (FI)
				'token'				=> 'terraleads',
				'goodId'           		=> '153768',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3634 => [ // Biomanix - RU (LV)
				'token'				=> 'terraleads',
				'goodId'           		=> '153773',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3633 => [ // Biomanix - RU (LT)
				'token'				=> 'terraleads',
				'goodId'           		=> '153399',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3815=> [ // Instaflex - RU (BY)
				'token'				=> 'terraleads',
				'goodId'           		=> '153413',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3632=> [ // Instaflex - RU (Fi)
				'token'				=> 'terraleads',
				'goodId'           		=> '153781',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3631 => [ // Instaflex - RU (LV)
				'token'				=> 'terraleads',
				'goodId'           		=> '153785',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3630 => [ // Instaflex - RU (LT)
				'token'				=> 'terraleads',
				'goodId'           		=> '153411',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3814 => [ // Talia - RU (BY)
				'token'				=> 'terraleads',
				'goodId'           		=> '153409',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3629 => [ // Talia - RU (FI)
				'token'				=> 'terraleads',
				'goodId'           		=> '153775',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3628 => [ // Talia - RU (LV)
				'token'				=> 'terraleads',
				'goodId'           		=> '153779',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3627 => [ // Talia - RU (LT)
				'token'				=> 'terraleads',
				'goodId'           		=> '153402',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bronix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	26603 => [
		'terraleadsApiKey'			=> '9ba24563e031936a082ba5d85d055099',
		'webmasterID'				=> 4,
		
		'offers' => [
			3575 => [ // Royal Bust - RO
				'token'				=> 'terraleads',
				'goodId'           		=> '151748',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutriomedia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutriomedia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3576 => [ // Smile Pen - RO
				'token'				=> 'terraleads',
				'goodId'           		=> '151843',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutriomedia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutriomedia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3577 => [ // Nikosan - RO
				'token'				=> 'terraleads',
				'goodId'           		=> '151841',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutriomedia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutriomedia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3578 => [ // Anticelulitic - RO
				'token'				=> 'terraleads',
				'goodId'           		=> '151847',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutriomedia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutriomedia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3579 => [ // Snorest - RO
				'token'				=> 'terraleads',
				'goodId'           		=> '151844',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutriomedia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutriomedia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3580 => [ // Alcoolprotect - RO
				'token'				=> 'terraleads',
				'goodId'           		=> '151846',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutriomedia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutriomedia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3581 => [ // Activslim - RO
				'token'				=> 'terraleads',
				'goodId'           		=> '150851',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutriomedia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutriomedia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3582 => [ // Superpotent - RO
				'token'				=> 'terraleads',
				'goodId'           		=> '150853',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutriomedia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutriomedia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	26894 => [
		'terraleadsApiKey'			=> 'ea58efe47f0ef262456c1a2dc858eb5a',
		'webmasterID'				=> 14,
		
		'offers' => [
			3616 => [ // Crystallex - HU
				'token'				=> 'terraleads',
				'goodId'			=> '1960',
                'comment'				=> 'http://hu-crystallex.beauty-goods.org/',
                'externalID' => function ($model, $config) {
                    return $model->id;
                },
                'additional15' => function ($model, $config) {
                    return $model->stream_id;
                },
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'http://art365-crystallex-hu-eupro.leadgroup.su/api/webmaster/addOrder.html',
					'urlOrderInfo'		=> 'http://art365-crystallex-hu-eupro.leadgroup.su/api/webmaster/getOrdersByIds.html',
                    'bridgeHandler' 	=> 'app\modules\terraleads\handlers\adv\leadvertex\user_26894\LeadvertexBridgeHandler',
                    'statusHandler' 	=> 'app\modules\terraleads\handlers\adv\leadvertex\user_26894\LeadvertexStatusHandler',
				],
			],
			3615 => [ // Crystallex - IT
				'token'				=> 'terraleads',
				'goodId'			=> '1957',
                'comment'				=> 'http://it-pf-crystallex.beauty-goods.org/',
                'externalID' => function ($model, $config) {
                    return $model->id;
                },
                'additional15' => function ($model, $config) {
                    return $model->stream_id;
                },
                'externalWebmaster'		=> function ($model, $config) {
                    return $model->web_id;
                },

				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'http://art365-crystallex-it-eupro.leadgroup.su/api/webmaster/addOrder.html',
					'urlOrderInfo'		=> 'http://art365-crystallex-it-eupro.leadgroup.su/api/webmaster/getOrdersByIds.html',
				],
			],
			3617 => [ // Crystallex - RO
				'token'				=> 'terraleads',
				'goodId'			=> '1959',
                'comment'				=> 'http://ro-pf-crystallex.beauty-goods.org/',
                'externalID' => function ($model, $config) {
                    return $model->id;
                },
                'additional15' => function ($model, $config) {
                    return $model->stream_id;
                },
                'externalWebmaster'		=> function ($model, $config) {
                    return $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'http://art365-crystallex-ro-eupro.leadgroup.su/api/webmaster/addOrder.html',
					'urlOrderInfo'		=> 'http://art365-crystallex-ro-eupro.leadgroup.su/api/webmaster/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	
 	27421 => [
		'terraleadsApiKey'			=> '4d5917c9c6feae9ec19a93dcc43922a2',
		'webmasterID'				=> 1,
		
		'offers' => [
			3775 => [ // Fermedies Sperm Booster - NG
				'token'				=> 'terraleads',
				'goodId'           		=> '154813',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://fertil24.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://fertil24.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3776 => [ // Fermedies Care for Women - NG
				'token'				=> 'terraleads',
				'goodId'           		=> '154814',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://fertil24.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://fertil24.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3777 => [ // Fermedies Sex Booster - NG
				'token'				=> 'terraleads',
				'goodId'           		=> '154816',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://fertil24.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://fertil24.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	
	26513 => [
		'terraleadsApiKey'			=> 'ac07a6b49e632596460601b97f4b0111',
		'webmasterID'				=> 14,
		
		'offers' => [
			3568 => [ // ProXpower - IN
				'token'				=> 'terraleads',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://drsanyasi.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://drsanyasi.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	28184 => [
		'terraleadsApiKey'			=> 'f3fcab200b60d77a868a068e6a7df3c0',
		'webmasterID'				=> 5,
		
		'offers' => [
			8882 => [ // Maxx Force Capsules - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '162194',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://deepaktl.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://deepaktl.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4726 => [ // Shilajit Capsules - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '162194',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://deepaktl.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://deepaktl.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	30042 => [
		'terraleadsApiKey'			=> '3fc42152018515901d641276ad584878',
		'webmasterID'				=> 2,
		
		'offers' => [
			4180 => [ // Feelbars - IN
				'token'				=> 'terraleads',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://feelbars.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://feelbars.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	30428 => [
		'terraleadsApiKey'			=> 'e61aa0da4534cd20bcb0e7a8d021e9d2',
		'webmasterID'				=> 3,
		
		'offers' => [
			4247 => [ // Maxx Force - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '161330',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://rohit.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://rohit.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	31716 => [
		'terraleadsApiKey'			=> 'd92a7e91ad206d56663e1f411793ecd4',
		'webmasterID'				=> 1,
		
		'offers' => [
			4509 => [ // Kohinoor Advenced Plus - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '184285',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kohinoorplus.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kohinoorplus.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7235 => [ // Kohinoor Advenced Plus CPL - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '184285',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kohinoorplus.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kohinoorplus.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4997 => [ // Insulux - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '173885',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kohinoorplus.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kohinoorplus.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6425 => [ // Cardiocure - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '186715',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kohinoorplus.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kohinoorplus.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	43616 => [
		'terraleadsApiKey'			=> '660d894fdccf0603c501e869d10e4fe2',
		'webmasterID'				=> 5,
		
		'offers' => [
			6346 => [ // Male Force - IN
				'token'					=> 'terraleads',
//				'goodId'           		=> '166449',
				'ip'					=> function ($model, $config) {
					return $model->ip;
				},
				 'externalWebmaster'	=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutrrays.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutrrays.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	32270 => [
		'terraleadsApiKey'			=> '849963c72801e172ace700812f068c83',
		'webmasterID'				=> 2,
		
		'offers' => [
			4666 => [ // Green Coffee Beans Powder - NG
				'token'				=> 'terraleads',
				'goodId'           		=> '166449',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://trimmex.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://trimmex.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	35062 => [
		'terraleadsApiKey'			=> 'fd8f00fcc9b3c969bb0ab0af34fa1892',
		'webmasterID'				=> 2,
		
		'offers' => [
			
			4944 => [ // Green Coffee Beans Powder - GH
				'token'				=> 'terraleads',
				'goodId'           		=> '172670',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ghana.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ghana.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	31873 => [
		'terraleadsApiKey'			=> 'ef1003eaaa936c5c361895a3af3fe92b',
		'webmasterID'				=> 5,
		
		'offers' => [
			4535 => [ // Xtreme Maxx Plus - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '165319',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutrahouse.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutrahouse.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	35554 => [
		'terraleadsApiKey'			=> '69f24938393c71a3e201527ea662f365',
		'webmasterID'				=> 3,
		
		'offers' => [
			4972 => [ // Melasma - BD
				'token'				=> 'terraleads',
				'goodId'           		=> '172121',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://drskincare.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://drskincare.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5657 => [ // MaxHerb - BD
				'token'				=> 'terraleads',
				'goodId'           		=> '178385',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://maxherb.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://maxherb.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	30129 => [
		'terraleadsApiKey'			=> 'd75f5c92b8d53fc653173ae9d964e5ec',
		'webmasterID'				=> 7,
		
		'offers' => [
			4187 => [ // Dream Night - IN
				'token'				=> 'terraleads',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://dreamnight.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://dreamnight.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	27501 => [
		'terraleadsApiKey'			=> '81f617df3f87fc0379c9a92a119f701b',
		'webmasterID'				=> 2,
		
		'offers' => [
			4169 => [ // Evyman - IN
				'token'				=> 'terraleads',
				 'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://evyman.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://evyman.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	11389 => [ // AE Влад
		'terraleadsApiKey'			=> '3d440ac40193d3d9429f1066bd189926',
		'webmasterID'				=> 8,

		'offers' => [
			3466 => [ // MaxMen Gel - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '143919',
                'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'ip'					=> '193.254.217.70',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://terraleads.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://terraleads.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3467 => [ // Garcinia Extract - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '143918',
                'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'ip'					=> '193.254.217.70',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://terraleads.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://terraleads.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3468 => [ //  PainGo Gel - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '143920',
                'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->web_id;
                },
				'ip'					=> '193.254.217.70',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://terraleads.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://terraleads.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	26240 => [
		'terraleadsApiKey'			=> '4ba1bd1264c53c5ae81d8a88795a1759',
		'webmasterID'				=> 3,
		
		'offers' => [
			3149 => [
				'token'				=> 'terraleads',
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://claudio.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://claudio.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	26274 => [
		'terraleadsApiKey'			=> '23a857c83157f552e88a9ea9baaa2e7c',
		'webmasterID'				=> 2,
		
		'offers' => [
			6653 => [ // Venisal gel - BA
				'token'				=> 'terraleads',
				'goodId'			=> '190825',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6544 => [ // Okulis - BA
				'token'				=> 'terraleads',
				'goodId'			=> '185633',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6545 => [ // Dianorm - BA
				'token'				=> 'terraleads',
				'goodId'			=> '186223',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6546 => [ // Otorin - BA
				'token'				=> 'terraleads',
				'goodId'			=> '185531',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6547 => [ // Grovi - BA
				'token'				=> 'terraleads',
				'goodId'			=> '186490',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5801 => [ // Hemoren - BA
				'token'				=> 'terraleads',
				'goodId'			=> '181431',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3524 => [ // Kossalin Shampoo - BA
				'token'				=> 'terraleads',
				'goodId'			=> '151029',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4142 => [ // Kossalin Shampoo - RS
				'token'				=> 'terraleads',
				'goodId'			=> '151029',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3611 => [ // Venoles - BA
				'token'				=> 'terraleads',
				'goodId'			=> '152728',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3868 => [ // Regulopres - BA
				'token'				=> 'terraleads',
				'goodId'			=> '156071',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5196 => [ // Regulocaps - BA
				'token'				=> 'terraleads',
				'goodId'			=> '172705',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4176 => [ // Biolift - BA
				'token'				=> 'terraleads',
				'goodId'			=> '162991',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4544 => [ // Biolift - RS
				'token'				=> 'terraleads',
				'goodId'			=> '162991',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4090 => [ // Burn Call - BA
				'token'				=> 'terraleads',
				'goodId'			=> '156484',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4820 => [ // Herbolo - BA
				'token'				=> 'terraleads',
				'goodId'			=> '172704',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4986 => [ // Herbolo - RS
				'token'				=> 'terraleads',
				'goodId'			=> '172704',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	49201 => [
		'terraleadsApiKey'			=> '32bc6e649d135aa389a417c047061f03',
		'webmasterID'				=> 2,
		
		'offers' => [
			5926 => [ // Hemoren - RS
				'token'				=> 'terraleads',
				'goodId'			=> '181431',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			6699 => [ // Venisal gel - RS
				'token'				=> 'terraleads',
				'goodId'			=> '190825',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					],
			],
			4142 => [ // Kossalin Shampoo - RS
				'token'				=> 'terraleads',
				'goodId'			=> '151029',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4544 => [ // Biolift - RS
				'token'				=> 'terraleads',
				'goodId'			=> '162991',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4986 => [ // Herbolo - RS
				'token'				=> 'terraleads',
				'goodId'			=> '172704',
				'externalWebmaster'	=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://kossalin.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	19826 => [
		'terraleadsApiKey'			=> 'd9959a1746d837470cde6c37a273d654',
		'webmasterID'				=> 4,
		
		'offers' => [
			2843 => [
				'token'				=> 't7dg4n7tgf4wy38sx54',
				'goodId'           		=> '110401',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bionutecpartners.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bionutecpartners.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			2844 => [
				'token'				=> 't7dg4n7tgf4wy38sx54',
				'goodId'           		=> '110423',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bionutecpartners.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bionutecpartners.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	25790 => [ // i.dealshop@yahoo.com
		'terraleadsApiKey'			=> 'adddbd80181fb2f2c9e72a78b7fcc0b5',
		'webmasterID'				=> 14,
		
		'offers' => [
			3411 => [ // Vigraviton - ID
				'token'				=> 'terraleads',
				'goodId'           		=> '109959',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://i-dealshop.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://i-dealshop.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	30133 => [ // yogesh.joshi2605@gmail.com
		'terraleadsApiKey'			=> 'e35f590326d7282f565a0e409346dc21',
		'webmasterID'				=> 2,
		
		'offers' => [
			4190 => [ //  LevelX - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '195494',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://lwecare.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://lwecare.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	23301 => [
		'terraleadsApiKey'			=> '63c0475eed1f67c2788e4c0594010e45',
		'webmasterID'				=> 7,
		
		'offers' => [
			4580 => [ //Tooth polisher - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '168406',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Tooth polisher - AE',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4581 => [ //Silicone cover set - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '168410',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'silicone cover set',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4582 => [ //5 Second Fix - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '168411',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => '5 second fix',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4583 => [ //Hula hoop - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '168413',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'hula hoop',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4584 => [ //Facial Cleanser - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '168414',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'facial cleanser',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4585 => [ //Power push - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '168415',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'compete power push',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4586 => [ //Ultra chill - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '168416',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'ultra chill',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4587 => [ //Absorbing stone carpet - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '168417',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'bath mat',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4588 => [ //Mommy bag - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '168418',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'diaper mommy bag',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4589 => [ //Abdominal wheel - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '168419',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'abdominal wheel',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4590 => [ //Robot vacuum cleaner - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '168420',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'robot vacuum cleaner',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4592 => [ //Spotlight star sky - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '168422',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'sportlight star sky',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4593 => [ //Fruit Plate - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '168423',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'fruit plate',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4599 => [ //Sticky Buddy - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '168424',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'sticky buddy',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4600 => [ //Ceramic cup - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '168425',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'ceramic mug',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4601 => [ //Ear Vacuum Cleaner - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '168426',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'electric ear vacuum cleaner',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3603 => [ //Flex Tape - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '152990',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Flex Tape',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3602 => [ //Sani Sticks - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '152989',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Sani Sticks',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3601 => [ //Dispenser and Magic Brush - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '152991',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Dispenser+ Magic Brush',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3600 => [ //Magic Ray set - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '152992',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Magic Ray 2 pc set',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3362 => [ //3362 - Massage pillow - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '151542',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Massage Pillow',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3363 => [ //3363 - Multifunctional Hair Comb - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '151541',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Hair Comb',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3364 => [ //3364 - Vclean Spot - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '151540',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Vclean Spot',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3365 => [ //#3365 - Magic Brush Set - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '151539',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Vclean Spot',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3367 => [ //3367 - Cansin - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '151538',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Cansin',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3416 => [ //3416 - LED Skin Tightening 5 in 1 - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '151536',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'LED Skin Tightening 5 in 1',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3415 => [ //#3415 - Steam brush - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '151537',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Steam Brush',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3456 => [ //3456 - Smart Cleaning Brush - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '151532',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Smart Cleaning Brush',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3459 => [ //3459 - Mini Car Refrigerator - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '151529',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Mini Car Refrigerator',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3461 => [ //Facial Massager - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '151527',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Facial Massager',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3462 => [ //3462 - Antibacterial Washing Machine Cleaner - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '151526',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Tablets for Washing Machine',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			
			3759 => [ //Skin Scrubber - OM
				'token'				=> 'terraleads',
				'goodId'           		=> '156446',
                'country'			=> 'OMAN',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Skin Scrubber',

				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],

			3774 => [ //Multifunctional Hair Comb - OM

				'token'				=> 'terraleads',
				'goodId'           		=> '156447',
                'country'			=> 'OMAN',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Multifunctional Hair Comb',

				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',

				],

			],

			3760 => [ //Flawless legs - OM

				'token'				=> 'terraleads',
				'goodId'           		=> '156448',
                'country'			=> 'OMAN',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Flawless legs',
				'configs' => [

					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
					
				],

			],

			3762 => [ //Flawless body - OM

				'token'				=> 'terraleads',
				'goodId'           		=> '156450',
                'country'			=> 'OMAN',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Flawless Body',

				'configs' => [

					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',

				],

			],

			3761 => [ //Magic Ray set - OM

				'token'				=> 'terraleads',
				'goodId'           		=> '156449',
                'country'			=> 'OMAN',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Magic Ray 2 Pcs',

				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',

				],

			],

			3763 => [ //YES! Hair Remover - OM

				'token'				=> 'terraleads',
				'goodId'           		=> '156452',
                'country'			=> 'OMAN',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'YES! Hair Remover',

				'configs' => [

					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',

				],

			],

			3764 => [ //Facial Massager - OM

				'token'				=> 'terraleads',
				'goodId'           		=> '156453',
                'country'			=> 'OMAN',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Flawless Facial Massager',

				'configs' => [

					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',

				],

			],
			3877 => [ //Flawless Brows - SA

				'token'				=> 'terraleads',
				'goodId'           		=> '158120',
                'country'			=> 'KSA',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Brows Removal',

				'configs' => [

					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',

				],

			],
			4023 => [ //Steam Brush - OM

				'token'				=> 'terraleads',
				'goodId'           		=> '159550',
                'country'			=> 'OMAN',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Steam brush',

				'configs' => [

					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',

				],

			],
			4149 => [ //Vitamin Serum - AE

				'token'				=> 'terraleads',
				'goodId'           		=> '163299',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Vitamin Serum 1+1',

				'configs' => [

					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',

				],

			],
			3869 => [ //3869 - Flawless - SA

				'token'				=> 'terraleads',
				'goodId'           		=> '155279',
                'country'			=> 'KSA',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Hair trimmer',

				'configs' => [

					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',

				],

			],
			3057 => [ //Pore Cleanser - SA

				'token'				=> 'terraleads',
				'goodId'           		=> '157711',
                'country'			=> 'KSA',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Vacuum Skin Cleaner',

				'configs' => [

					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',

				],

			],
			4351 => [ //Multifunctional Hair Comb - SA

				'token'				=> 'terraleads',
				'goodId'           		=> '156123',
                'country'			=> 'KSA',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Beard styler',

				'configs' => [

					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',

				],

			],
			4362 => [ //Hair curler - SA

				'token'				=> 'terraleads',
				'goodId'           		=> '165239',
                'country'			=> 'KSA',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Hair curler',

				'configs' => [

					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',

				],

			],
			4361 => [ //Super Arms - SA

				'token'				=> 'terraleads',
				'goodId'           		=> '165238',
                'country'			=> 'KSA',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Super Arms',

				'configs' => [

					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',

				],

			],
			4355 => [ //Massage Brush Spa Pro - SA

				'token'				=> 'terraleads',
				'goodId'           		=> '165150',
                'country'			=> 'KSA',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Massage brush spa pro',

				'configs' => [

					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',

				],

			],
			4353 => [ //PediFeet - SA

				'token'				=> 'terraleads',
				'goodId'           		=> '165151',
                'country'			=> 'KSA',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'electric pumice',

				'configs' => [

					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',

				],

			],
			4152 => [ //Gold Caviar Cream - AE

				'token'				=> 'terraleads',
				'goodId'           		=> '163298',
                'country'			=> 'United Arab Emirates',
				'additional1' => 'cod',
    			'additional2' => '1',
    			'additional3' => 'FRAGILE, VERY IMPORTANT',
    			'additional4' => 'test@gmail.com',
    			'additional7' => 'Freckle Gold Caviar Cream 1+1',

				'configs' => [

					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://goods-market.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',

				],

			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	25074 => [
		'terraleadsApiKey'			=> '8407f56a156020bc99561058582b84bc',
		'webmasterID'				=> 2,
		
		'offers' => [
			3304 => [
				'token'					=> 'terraleads',
				'goodId'           		=> '138471',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://shopanov.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://shopanov.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	24143 => [
		'terraleadsApiKey'			=> 'af3e2f87f7c9d32a5888930aca862a67',
		'webmasterID'				=> 1,
		
		'offers' => [
			5254 => [//Coralift - IT
				'token'					=> 'terraleads',
				'goodId'           		=> '190169',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6274 => [//Diet Drops - IT
				'token'					=> 'terraleads',
				'goodId'           		=> '190170',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4991 => [//Germivir - IT
				'token'					=> 'terraleads',
				'goodId'           		=> '190172',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4970 => [//Keto Black - IT
				'token'					=> 'terraleads',
				'goodId'           		=> '190173',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			2956 => [ //Keto Light + - IT
				'token'					=> 'terraleads',
				'goodId'           		=> '190174',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5476 => [//Cardio Life - IT
				'token'					=> 'terraleads',
				'goodId'           		=> '186831',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5547 => [//Diabex - IT
				'token'					=> 'terraleads',
				'goodId'           		=> '186832',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5928 => [//Big Size - IT
				'token'					=> 'terraleads',
				'goodId'           		=> '186830',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5024 => [//Erectil - IT
				'token'					=> 'terraleads',
				'goodId'           		=> '186829',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6393 => [//Tantril - IT
				'token'					=> 'terraleads',
				'goodId'           		=> '165789',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3149 => [//Insunat - IT
				'token'					=> 'terraleads',
				'goodId'           		=> '141480',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4250 => [//Reduslim - IT
				'token'					=> 'terraleads',
				'goodId'           		=> '162860',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4950 => [//FormVit - IT
				'token'					=> 'terraleads',
				'goodId'           		=> '141481',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5470 => [//Breast 4+ - IT
				'token'					=> 'terraleads',
				'goodId'           		=> '178668',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://fenix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	31969 => [
		'terraleadsApiKey'			=> 'cfb66d5e31954ce243aadef3a49c3d38',
		'webmasterID'				=> 1,
		
		'offers' => [
			4847 => [
				'token'					=> 'terraleads',
				'goodId'           		=> '171303',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://maralgel.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://maralgel.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4952 => [
				'token'					=> 'terraleads',
				'goodId'           		=> '175103',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://maralgel.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://maralgel.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5325 => [
				'token'					=> 'terraleads',
				'goodId'           		=> '177175',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://maralgel.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://maralgel.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5468 => [ //VigRX Capsules - BD
				'token'					=> 'terraleads',
				'goodId'           		=> '178539',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://maralgel.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://maralgel.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5475 => [ //Keto BHB - BD
				'token'					=> 'terraleads',
				'goodId'           		=> '178538',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://maralgel.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://maralgel.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6801 => [ //Booletroof Coffee Slim Fit -BD
				'token'					=> 'terraleads',
				'goodId'           		=> '195629',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->web_id;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://maralgel.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://maralgel.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	22843 => [
		'terraleadsApiKey'			=> '630a028ed01f336b87741de92c5ddf8d',
		'webmasterID'				=> 4,
		
		'offers' => [
			1111 => [
				'token'				=> 'token',
				'goodId'           		=> 'goodid',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://newproject.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://newproject.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	14402 => [
		'terraleadsApiKey'			=> '658dd83652253a7b36b9c69676183976',
		'webmasterID'				=> 3,

		'offers' => [
			2963 => [ // Alpha Beast - NG
				'token'				=> 'terraleads',
				'goodId'           		=> '150811',
                'additional4'		=> function ($model, $config) {
                    return $model->id;
                },
                'additional5'		=> function ($model, $config) {
                    return $model->web_id;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://superdeal.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://superdeal.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'statuses' => [
				'confirm'	=> [
					
					'paid'			=> '',
					'full'			=> '',
					'shipped'		=> '',
					'canceled'		=> '',
					'return'		=> '',
				],
				'reject' => [
	
				],
				'expect' => [
					'processing'	=> '',
					'null' 			=> '',
					'accepted'		=> '',
				],
				'trash' => [
					'spam'			=> '',
				],
			],
			'brakeLogFolder'	=> true,
		],
	],
	25512 => [
		'terraleadsApiKey'			=> '328f31b4234244732c4fc0e8fabf9dc1',
		'webmasterID'				=> 10,
		
		'offers' => [
			6452 => [
				'token'				=> 'terraleads',
				'goodId'           		=> '178106',
                'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutracod.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutracod.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3375 => [
				'token'				=> 'terraleads',
				'goodId'           		=> '136042',
                'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutracod.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutracod.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			2960 => [
				'token'				=> 'terraleads',
				'goodId'           		=> '137777',
                'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutracod.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutracod.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6015 => [ //Penirun - ID
				'token'				=> 'terraleads',
				'goodId'           		=> '184347',
                'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutracod.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutracod.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3713 => [ //Enerflex - AR
				'token'				=> 'terraleads',
				'goodId'           		=> '154417',
                'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutracod.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutracod.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3538 => [ //Penirun - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '150876',
                'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutracod.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutracod.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4498 => [ //Aveda Hair Oil - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '167077',
                'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutracod.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutracod.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4554 => [ //Motto Keto - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '165072',
                'externalWebmaster'		=> function ($model, $config) {
                    return $model->uHash ?: $model->wHash;
                },
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutracod.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutracod.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	23338 => [
		'terraleadsApiKey'			=> '15a77cf254e3aa1ff4bd785eff9e5588',
		'webmasterID'				=> 1,
		
		'offers' => [
			6651 => [ // Prosta-G - KH
				'token'				=> 'terraleads',
				'goodId'           		=> '180532',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6540 => [ // Diabexis (diabetes) - KH
				'token'				=> 'terraleads',
				'goodId'           		=> '180715',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4501 => [ // Cardio Forte
				'token'				=> 'terraleads',
				'goodId'           		=> '167322',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://philippines.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://philippines.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4500 => [ // Dominator
				'token'				=> 'terraleads',
				'goodId'           		=> '167323',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://philippines.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://philippines.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4499 => [ // Keto
				'token'				=> 'terraleads',
				'goodId'           		=> '167324',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://philippines.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://philippines.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3033 => [ // CHoco mia - kh
				'token'				=> 'terraleads',
				'goodId'           		=> '124483',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3335 => [ // CPretty Cowry - KH
				'token'				=> 'terraleads',
				'goodId'           		=> '137392',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3333 => [ // Pretty Cowry - KH
				'token'				=> 'terraleads',
				'goodId'           		=> '140211',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3824 => [ //  Talismoney - KH

				'token'				=> 'terraleads',
				'goodId'           		=> '140316',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;

				},

				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			
			3391 => [ // Detox - KH
				'token'				=> 'terraleads',
				'goodId'           		=> '141643',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3404 => [ // Titan Premium - KH
				'token'				=> 'terraleads',
				'goodId'           		=> '151297',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3856 => [ // Keto - KH
				'token'				=> 'terraleads',
				'goodId'           		=> '157206',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4045 => [ // Hammer of thor - KH
				'token'				=> 'terraleads',
				'goodId'           		=> '160570',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4973 => [ // Hammer of thor - KH
				'token'				=> 'terraleads',
				'goodId'           		=> '173890',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5212 => [ // Prostaffect
				'token'				=> 'terraleads',
				'goodId'           		=> '175484',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5206 => [ // EffectEro - KH
				'token'				=> 'terraleads',
				'goodId'           		=> '175482',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5205 => [ // Alfagen - KH
				'token'				=> 'terraleads',
				'goodId'           		=> '175483',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5474 => [ // Diabex - KH
				'token'				=> 'terraleads',
				'goodId'           		=> '178636',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5601 => [ // Diabex - KH
				'token'				=> 'terraleads',
				'goodId'           		=> '178635',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5788 => [ // Vison - KH
				'token'				=> 'terraleads',
				'goodId'           		=> '181401',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5890 => [ // Diabexis - KH
				'token'				=> 'terraleads',
				'goodId'           		=> '182091',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://chocomia.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	36038 => [
		'terraleadsApiKey'			=> 'c0b10bc7c444a3e61c0b7bdc2014e34c',
		'webmasterID'				=> 2,
		
		'offers' => [
			9360 => [ // Capsmal - SN
				'token'				=> 'terraleads',
				'goodId'           		=> '208790',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8856 => [ // Hypeno - UG1
				'token'				=> 'terraleads',
				'goodId'           		=> '204420',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ugandapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ugandapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8857 => [ // Visamore - UG
				'token'				=> 'terraleads',
				'goodId'           		=> '204421',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ugandapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ugandapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8858 => [ // Dulrelief Cream - UG
				'token'				=> 'terraleads',
				'goodId'           		=> '204423',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ugandapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ugandapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8855 => [ // Loosaaf - CI
				'token'				=> 'terraleads',
				'goodId'           		=> '190394',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7493 => [ // Visamore - CI
				'token'				=> 'terraleads',
				'goodId'           		=> '190765',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7639 => [ // Visamore - GA
				'token'				=> 'terraleads',
				'goodId'           		=> '190768',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			8005 => [ // Dulrelief Cream - TD
				'token'				=> 'terraleads',
				'goodId'           		=> '197923',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://tchadpro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://tchadpro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7906 => [ // Prosmal - DZ
				'token'				=> 'terraleads',
				'goodId'           		=> '197366',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7804 => [ // Loosaf Drops - CG
				'token'				=> 'terraleads',
				'goodId'           		=> '196535',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://congobrazaviile.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://congobrazaviile.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7802 => [ // Dulrelief Cream - CG
				'token'				=> 'terraleads',
				'goodId'           		=> '196534',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://congobrazaviile.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://congobrazaviile.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7640 => [ // Fulldiab - GA
				'token'				=> 'terraleads',
				'goodId'           		=> '194197',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7655 => [ // Dulrelief Cream - BJ
				'token'				=> 'terraleads',
				'goodId'           		=> '196666',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7654 => [ // Hypeno - BJ
				'token'				=> 'terraleads',
				'goodId'           		=> '196663',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7653 => [ // Visamore - BJ
				'token'				=> 'terraleads',
				'goodId'           		=> '196665',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6600 => [ // Prostalife Drops (prostatitis) - GA
				'token'				=> 'terraleads',
				'goodId'           		=> '190070',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7597 => [ // Prostic Creme - SN
				'token'				=> 'terraleads',
				'goodId'           		=> '198291',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7596 => [ // Fulldiab - SN
				'token'				=> 'terraleads',
				'goodId'           		=> '193839',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7595 => [ // Hypeno - SN
				'token'				=> 'terraleads',
				'goodId'           		=> '190197',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6584 => [ // MalPlus - SN
				'token'				=> 'terraleads',
				'goodId'           		=> '188064',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7018 => [ // Dulrelief Cream - SN
				'token'				=> 'terraleads',
				'goodId'           		=> '192882',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7442 => [ // Prostic Creme - CI
				'token'				=> 'terraleads',
				'goodId'           		=> '197268',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7354 => [ // Fulldiab - CI
				'token'				=> 'terraleads',
				'goodId'           		=> '193838',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7353 => [ // Hypeno - CI
				'token'				=> 'terraleads',
				'goodId'           		=> '190196',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7017 => [ // Dulrelief Cream - CI
				'token'				=> 'terraleads',
				'goodId'           		=> '192881',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6587 => [ // HyperTen - TN
				'token'				=> 'terraleads',
				'goodId'           		=> '186447',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://tunisiapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://tunisiapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6586 => [ // X-Male Gel - TN
				'token'				=> 'terraleads',
				'goodId'           		=> '184283',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://tunisiapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://tunisiapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6597 => [ // Painlaf Cream - GA
				'token'				=> 'terraleads',
				'goodId'           		=> '188586',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6596 => [ // Painlaf Cream - BF
				'token'				=> 'terraleads',
				'goodId'           		=> '188151',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6595 => [ // Painlaf Cream - SN
				'token'				=> 'terraleads',
				'goodId'           		=> '188149',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6594 => [ // Painlaf Cream - CI
				'token'				=> 'terraleads',
				'goodId'           		=> '188148',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6529 => [ // Prostalife Drops (prostatitis) - CI
				'token'				=> 'terraleads',
				'goodId'           		=> '185533',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6528 => [ // Prostalife Drops (prostatitis) - NE
				'token'				=> 'terraleads',
				'goodId'           		=> '186043',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6559 => [ // Maldox - SN
				'token'				=> 'terraleads',
				'goodId'           		=> '184951',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6561 => [ // Prostalife Drops (potency) - CI
				'token'				=> 'terraleads',
				'goodId'           		=> '186240',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6562 => [ // Prostalife Drops (potency) - SN
				'token'				=> 'terraleads',
				'goodId'           		=> '186241',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6563 => [ // Prostalife Drops (potency) - ML
				'token'				=> 'terraleads',
				'goodId'           		=> '186242',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6564 => [ // Prostalife Drops (potency) - NE
				'token'				=> 'terraleads',
				'goodId'           		=> '186243',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6382 => [ // Voltapain Cream - LY
				'token'				=> 'terraleads',
				'goodId'           		=> '185587',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://libyapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://libyapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6381 => [ // HyperTen - DZ
				'token'				=> 'terraleads',
				'goodId'           		=> '185698',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6379 => [ // Prostalife Drops - ML
				'token'				=> 'terraleads',
				'goodId'           		=> '185837',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6380 => [ // Prostalife Drops - SN
				'token'				=> 'terraleads',
				'goodId'           		=> '185700',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6377 => [ // MalPlus - MA
				'token'				=> 'terraleads',
				'goodId'           		=> '185903',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://moroccopro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://moroccopro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5897 => [ // Malodox - ML
				'token'				=> 'terraleads',
				'goodId'           		=> '184952',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6282 => [ // X-Nalo Gel - NE
				'token'				=> 'terraleads',
				'goodId'           		=> '185389',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6250 => [ // Prostamen - DZ
				'token'				=> 'terraleads',
				'goodId'           		=> '185076',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5896 => [ // X-Nalo Gel - SN
				'token'				=> 'terraleads',
				'goodId'           		=> '181771',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5895 => [ // X-Nalo Gel - CI
				'token'				=> 'terraleads',
				'goodId'           		=> '180447',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://africapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5892 => [ // XXL Men Cream - DZ
				'token'				=> 'terraleads',
				'goodId'           		=> '181991',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5844 => [ // Voltapain Cream - TN
				'token'				=> 'terraleads',
				'goodId'           		=> '185936',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://tunisiapro.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://tunisiapro.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5709 => [ // X-Male Gel - DZ
				'token'				=> 'terraleads',
				'goodId'           		=> '180371',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5708 => [ // Flekosteel - DZ
				'token'				=> 'terraleads',
				'goodId'           		=> '180744',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5066 => [ // 
				'token'				=> 'terraleads',
				'goodId'           		=> '180533',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4848 => [ // 
				'token'				=> 'terraleads',
				'goodId'           		=> '171265',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bionature.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bionature.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5034 => [ // 
				'token'				=> 'terraleads',
				'goodId'           		=> '180772',
				'additional1' => '2ahdxuqhgd',

				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5033 => [ // 
				'token'				=> 'terraleads',
				'goodId'           		=> '172073',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bionature.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bionature.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5031 => [ // Biohira Hair Serum - KW
				'token'				=> 'terraleads',
				'goodId'           		=> '180111',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5030 => [ // Biohira Hair Serum - AE
				'token'				=> 'terraleads',
				'goodId'           		=> '180112',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5023 => [ // 
				'token'				=> 'terraleads',
				'goodId'           		=> '172066',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bionature.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bionature.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5438 => [ //Voltapain Cream - DZ
				'token'				=> 'terraleads',
				'goodId'           		=> '178419',
				'additional1' => '22030001',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5635 => [ // Rex Сream - SA
				'token'				=> 'terraleads',
				'goodId'           		=> '179987',
				'additional1' => 'Wr5kTi9HnY0',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nada.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	40781 => [
		'terraleadsApiKey'			=> 'b7c8c5c319ed7e71e334bd039f62e114',
		'webmasterID'				=> 3,
		
		'offers' => [
			5590 => [ // 
				'token'				=> 'terraleads',
				'goodId'           		=> '179907',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://umpoverseas.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://umpoverseas.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
32617 => [
		'terraleadsApiKey'			=> '5be279f2e07f6fa5078ad202979e9810',
		'webmasterID'				=> 2,
		
		'offers' => [
			5424 => [ // 
				'token'				=> 'terraleads',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://creamdz.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://creamdz.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	25331 => [
		'terraleadsApiKey'			=> '8aafaae057e00d038bfa1b818c4cfeff',
		'webmasterID'				=> 10,
		
		'offers' => [
			7586 => [ // Philola - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '198305',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			7587 => [ // Andicellix - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '198306',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6745 => [ // Varix - TH (low price)
				'token'				=> 'terraleads',
				'goodId'           		=> '163612',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://lowprice590.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://lowprice590.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6746 => [ // B-Joint - TH (low price)
				'token'				=> 'terraleads',
				'goodId'           		=> '163611',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://lowprice590.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://lowprice590.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6384 => [ // Diacard - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '182044',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6336 => [ // Varix - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '163612',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6335 => [ // B-Joint - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '163611',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6334 => [ // Rich Skin - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '168276',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5903 => [ // Genesis - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '175113',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5902 => [ // Oclarizin - TH	
				'token'				=> 'terraleads',
				'goodId'           		=> '174780',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3695 => [ // Acvit (vision) - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '174780',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5856 => [ // Elsie - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '37295',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			1761 => [ // Deeper - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '178991',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3337 => [ // Helmina - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '37294',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5303 => [ // Back Pro - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '37293',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4006 => [ // MPower - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '57688',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4005 => [ // Fortamin - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '37296',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			2057 => [ // Talismoney - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '57691',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4480 => [ //  Money Amulet - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '174852',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			4004 => [ // T-Chrome - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '63972',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			2059 => [ // Choco Mia - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '169259',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3697 => [ // Acvit (hemorrhoids) - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '169257',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3694 => [ // Acvit (potency) - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '170357',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3693 => [ // Acvit (hair) - TH 
				'token'				=> 'terraleads',
				'goodId'           		=> '175615',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5052 => [ // Sundos (cardio) - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '157905',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5051 => [ // Sundos (diabetes) - TH
				'token'				=> 'terraleads',
				'goodId'           		=> '157905',
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://bookdoctor.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	
	24343 => [
		'terraleadsApiKey'			=> '0b00ea561ce495313b5156d678f9ff97',
		'webmasterID'				=> 1,
		
		'offers' => [
			3595 => [ // vinix 590000
				'token'				=> 'terraleads',
				'goodId'           		=> '134841',
				
				'configs' => [
					
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://vinix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://vinix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3165 => [ // vinix
				'token'				=> 'terraleads',
				'goodId'           		=> '134841',
				
				'configs' => [
					
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://vinix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://vinix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3406 => [ //Alavax - КR
				'token'				=> 'terraleads',
				'goodId'           		=> '141875',
				
				'configs' => [
					
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://vinix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://vinix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3641 => [ // ManyeoSlim - KR
				'token'				=> 'terraleads',
				'goodId'           		=> '157571',
				
				'configs' => [
					
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://vinix.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://vinix.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'statuses' => [
				'confirm'	=> [
					
					'paid'			=> '',
					'full'			=> '',
					'shipped'		=> '',
				],
				'reject' => [
					'canceled'		=> '',
					'return'		=> '',
				],
				'expect' => [
					'processing'	=> '',
					'null' 			=> '',
					'accepted'		=> '',
				],
				'trash' => [
					'spam'			=> '',
				],
			],
			
			'brakeLogFolder'	=> true,
		],
	],
	23979 => [
		'terraleadsApiKey'			=> '4107888c41d69705facd20b6ef9b3c49',
		'webmasterID'				=> 19,
		
		'offers' => [
			3100 => [
				'token'				=> 'terraleads',
				'goodId'           		=> '134792',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://new1.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://new1.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3101 => [
				'token'				=> 'terraleads',
				'goodId'           		=> '134792',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://new1.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://new1.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3102 => [
				'token'				=> 'terraleads',
				'goodId'           		=> '134792',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://new1.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://new1.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3103 => [
				'token'				=> 'terraleads',
				'goodId'           		=> '134792',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://new1.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://new1.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3104 => [
				'token'				=> 'terraleads',
				'goodId'           		=> '134792',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://new1.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://new1.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	23934 => [
		'terraleadsApiKey'			=> 'ec790f6c36adf6653feca50d70235991',
		'webmasterID'				=> 6,
		
		'offers' => [
			3093 => [
				'token'				=> 'terraleads',
				'goodId'           		=> '120526',
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ketopl.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ketopl.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	42973 => [
		'terraleadsApiKey'			=> '8ab26a93fcb762d85539a12111479c8e',
		'webmasterID'				=> 14,
		
		'offers' => [
			5858 => [//Rituals Scrub - TR
				'token'				=> 'terraleads',
				//'goodId'           		=> '120526',
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://17.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://17.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5859 => [//Cordis Meridian - TR
				'token'				=> 'terraleads',
				//'goodId'           		=> '120526',
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://17.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://17.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5860 => [//Imperium Potencium - TR
				'token'				=> 'terraleads',
				//'goodId'           		=> '120526',
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://17.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://17.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5861 => [//DermaNew Hair - TR
				'token'				=> 'terraleads',
				//'goodId'           		=> '120526',
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://17.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://17.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5862 => [//   Dialine - TR
				'token'				=> 'terraleads',
				//'goodId'           		=> '120526',
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://17.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://17.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	43254 => [
		'terraleadsApiKey'			=> '893ee4a576d5b93aa007f9876f620700',
		'webmasterID'				=> 6,
		
		'offers' => [
			5891 => [//PowerUp Powder - IN
				'token'				=> 'terraleads',
				//'goodId'           		=> '120526',
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://drcashrvbm.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://drcashrvbm.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			6821 => [//Raksh Power Max - IN
				'token'				=> 'terraleads',
				//'goodId'           		=> '120526',
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://drcashrvbm.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://drcashrvbm.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	23917 => [ // David Mark
		'terraleadsApiKey'			=> '8ee15cd22d3eb193ca9998d076bbe83d',
		'webmasterID'				=> 30,
		
		'offers' => [
			3085 => [ // Rolex Cellini - KH
				'token'				=> 'terraleads',
				'goodId'           		=> '140869',

				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://trafoba-traffury.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://trafoba-traffury.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3394 => [ // Viga Spray - KH
				'token'				=> 'terraleads',
				'goodId'           		=> '174635',

				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://trafoba.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://trafoba.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3393 => [ // Money Amulet - KH
				'token'				=> 'terraleads',
				'goodId'           		=> '140805',

				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://trafoba-traffury.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://trafoba-traffury.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3976 => [ // Money Amulet - SG
				'token'				=> 'terraleads',
				'goodId'           		=> '158966',

				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://trafoba-traffury.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://trafoba-traffury.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3392 => [ // Electricity Saving Box - KH
				'token'				=> 'terraleads',
				'goodId'           		=> '140806',

				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://trafoba-traffury.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://trafoba-traffury.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	22745 => [
		'terraleadsApiKey'			=> 'f73081c021e823e2d7fec1fac9683280',
		'webmasterID'				=> 3,
		
		'offers' => [
			2997 => [
				'token'				=> 'terraleads',
				
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimingo-daily-go.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimingo-daily-go.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			2996 => [
				'token'				=> 'terraleads',
				'goodId'           		=> '124131',
				
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://peak-edge.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://peak-edge.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3324 => [
				'token'				=> 'terraleads',
				'goodId'           		=> '138570',
				
				'externalWebmaster'		=> function ($model, $config) {
					return $model->uHash ?: $model->wHash;
				},
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://4mega.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://4mega.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	18606 => [
		'terraleadsApiKey'			=> '84e1e7020b049c439fd20e060f57702c',
		'webmasterID'				=> 6,
		
		'offers' => [
			2501 => [
				'token'				=> 'terraleads',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://dtugroup.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://dtugroup.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	20808 => [
		'terraleadsApiKey'			=> 'ca4a1d058a5e4bcf7fca1bfb847194d3',
		'webmasterID'				=> 3,
		
		'offers' => [
			3596 => [ //Performax - ID
				'token'				=> 'terraleads',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://drcash.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://drcash.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	36393 => [
		'terraleadsApiKey'			=> 'd2e68d52f472371c3419e3373d38183c',
		'webmasterID'				=> 2,
		
		'offers' => [
			5078 => [ //NutraZure Keto - IN
				'token'				=> 'terraleads',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://ket01.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://ket01.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	42878 => [
		'terraleadsApiKey'			=> '0d85ed7890c1d341ac796174d2c4b170',
		'webmasterID'				=> 2,
		
		'offers' => [
			5831 => [ //Black Panthar - IN  
				'token'				=> 'terraleads',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://nutraads.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://nutraads.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	33781 => [
		'terraleadsApiKey'			=> '261c58a22f276c1ea68782313c3ee9c5',
		'webmasterID'				=> 4,
		
		'offers' => [
			4891 => [ //Vedic 69 - IN
				'token'				=> 'terraleads',
				'goodId'           		=> '163335',
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://vedic.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://vedic.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	19894 => [
		'terraleadsApiKey'			=> 'bed42270f57b08bebad1f729f26ba04e',
		'webmasterID'				=> 30,
		
		'offers' => [
			5588 => [ //W-loss - GR
				'token'				=> 'terraleads',
				'goodId'           		=> '178208',
				'webmasterID'				=> 30,

				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://w-loss-gr-cc.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://w-loss-gr-cc.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5587 => [ //W-loss - IT
				'token'				=> 'terraleads',
				'goodId'           		=> '178208',
				'webmasterID'				=> 30,

				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://w-loss-it-cc.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://w-loss-it-cc.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			5586 => [ //W-loss - ES
				'token'				=> 'terraleads',
				'goodId'           		=> '178208',
				'webmasterID'				=> 30,

				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://w-loss-es-cc.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://w-loss-es-cc.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			2560 => [
				'token'				=> 'terraleads',
				'goodId'           		=> '91084',
				'webmasterID'				=> 30,

				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://keto-cc-it.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://keto-cc-it.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			2572 => [
				'token'				=> 'terraleads',
				'goodId'           		=> '81666',
				'webmasterID'				=> 30,

				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimagic-it-cc.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimagic-it-cc.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			2573 => [
				'token'				=> 'terraleads',
				'goodId'           		=> '81666',
				'webmasterID'				=> 30,
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimagic-es-cc.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimagic-es-cc.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			2574 => [
				'token'				=> 'terraleads',
				'goodId'           		=> '93126',
				'webmasterID'				=> 30,
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimagic-gr-cc.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimagic-gr-cc.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3160 => [
				'token'				=> 'terraleads',
				'goodId'           		=> '91084',
				'webmasterID'				=> 30,
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://keto-es-cc.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://keto-es-cc.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3162 => [
				'token'				=> 'terraleads',
				'goodId'           		=> '91085',
				'webmasterID'				=> 30,
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://l-traxyn-es-cc.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://l-traxyn-es-cc.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3161 => [
				'token'				=> 'terraleads',
				'goodId'           		=> '91085',
				'webmasterID'				=> 30,
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://l-traxyn-it-cc.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://l-traxyn-it-cc.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3164 => [
				'token'				=> 'terraleads',
				'goodId'           		=> '118684',
				'webmasterID'				=> 30,
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimjet-es-cc.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimjet-es-cc.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
			3163 => [
				'token'				=> 'terraleads',
				'goodId'           		=> '118684',
				'webmasterID'				=> 30,
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://slimjet-it-cc.leadvertex.ru/api/webmaster/v2/addOrder.html',
					'urlOrderInfo'		=> 'https://slimjet-it-cc.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html',
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
		],
	],
	
	22981 => [
		'terraleadsApiKey'			=> '59c56b68204b097e881b74fb6cfa695a',
		'webmasterID'				=> 10,
		
		'offers' => [
			0000 => [
				'token'				=> '11111',
				'goodId'           	=> '108466',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			0000 => [
				'token'				=> '11111',
				'goodId'           	=> '118844',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		'configs' => [
			'brakeLogFolder'	=> true,
			
			'urlOrderAdd'		=> 'https://getlean.leadvertex.ru/api/webmaster/v2/addOrder.html',
			'urlOrderInfo'		=> 'https://getlean.leadvertex.ru/api/webmaster/v2/getOrdersByIds.html'
		],
	],
	
	'configs' => [
		'statuses' => [
			'confirm'	=> [   //ввввв
				'accepted'		=> '',
				'paid'			=> '',
				'full'			=> '',
				'shipped'		=> '',
			],
			'reject' => [
				'canceled'		=> '',
				'return'		=> '',
			],
			'expect' => [
				'processing'	=> '',
				'null' 			=> '',
			],
			'trash' => [
				'spam'			=> '',
			],
		],
		'subStatuses' => [
			'confirm'	=> ['accepted', 'paid'],
			'reject'	=> ['canceled', 'return'],
			'expect'	=> ['processing', 'null'],
			'trash'		=> ['spam'],
		],
		'ransom' => [																									//  Для майбутнього функціоналу перевірки на викуп
			'confirm'	=> [
				'paid'			=> '',
			],
			'reject'	=> [
				'return'		=> '',
			],
			'expect'	=> [
				'shipped'		=> '',
			],
		],
//		'urlApileadOrderUpdate'		=> 'http://al-api.com/api/lead/update',
//		'bridgeEnabled'				=> 1,																				// Чи активна ця інтеграція
//		'statusEnabled'				=> null,																			// Чи опрацьовувати статуси цієї інтеграції
//		'urlOrderAdd'				=> 'https://magnetlashes990.leadvertex.ru/addOrder.html',
//		'urlOrderInfo'				=> 'https://magnetlashes990.leadvertex.ru/getOrdersByIds.html',
		'bridgeRequestTimeout'		=> 60,
		'statusRequestTimeout'		=> 60,
		'enablePhoneChars'			=> false,
//		'brakeLogFolder'			=> '',
//		'statusHandler' 			=> 'app\modules\apilead\handlers\adv\leadvertex\LeadvertexStatusHandler',
//		'bridgeHandler' 			=> 'app\modules\apilead\handlers\adv\leadvertex\LeadvertexBridgeHandler',
	],
];
