<?php


return [
	
    27521 => [
        'terraleadsApiKey'			=> 'b1c6bea906f6f6e04da297f8ec1ef240',
        'apiKey' 					=> 'e751607daacd7f3b6a5bb24fdebea8cc',
        'postbackApiKey'			=> 'b1c6bea906f6f6e04da297f8ec1ef241',

        'offers' => [
            9365 => [//HemorrEx - IN
                'offerId'           => 'e8e6d419-1cf2-4c23-bf20-79ff99959fe3',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            9361 => [//PRO 365 Detox - IN
                'offerId'           => '1aa14a37-e02b-4fb9-8ff3-a031436a5bf2',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6535 => [//Stability - MY
                'offerId'           => '75755b40-52e4-4040-a71d-0f066c347d47',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6589 => [//WonderFit - CL
                'offerId'           => '40f058e8-f5bf-4402-8fb2-a91461342a2f',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5809 => [//HairEX - CO
                'offerId'           => '460089e4-bafd-4e56-ae28-76bc5cf32925',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6543 => [//Arthromax gel - SG
                'offerId'           => '0383f3e8-aee5-4f3b-a277-08a06e97ad00',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6249 => [//Nopal - MAa
                'offerId'           => '08190ad9-2985-4e6b-ab90-8ee847d73e68',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6247 => [//Ultraprost caps - MA
                'offerId'           => '42419835-0ec6-48a7-988a-29a51475d921',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6246 => [//Crystalix - MA
                'offerId'           => 'b104feac-545b-4baa-b0ba-1b2d59e67790',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6245 => [//Cardioton - MA
                'offerId'           => 'f846c5e3-386f-4450-82f9-2cc4af78179a',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6300 => [//MycoDermin spray - TN
                'offerId'           => '1f8538d5-34ab-4da0-b12a-96aede47b7f3',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6298 => [//Liposystem - TN
                'offerId'           => 'c5dd01ad-ed0b-444c-95f6-bfb4288dfeaf',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6297 => [//Epidermix - TN
                'offerId'           => '76f4fef9-7805-4ad4-90f9-5a0c9de6ca85',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6296 => [//Giperium Powder - TN
                'offerId'           => '9fd25fc41-d2e3-469d-a88f-d3954bda78ea',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6244 => [//Giperium caps - GT
                'offerId'           => '9d73e390-7551-4ea3-8f20-c1584cbdde9f',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6248 => [//Gluco Pro - MA
                'offerId'           => '16920b3c-b86d-4197-ae2e-0f1bb49d665d',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6295 => [//Gluco Pro - TN
                'offerId'           => 'd663c0fc-24a0-4a42-a657-bebc6426538d',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5324 => [//VariGO - TN
                'offerId'           => '3e28bd8b-3778-4130-bef5-f1c320bdac5a',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5323 => [//Biodermalix - TN
                'offerId'           => '3fb16442-fbde-442e-9f96-1572ea49e434',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5843 => [//Amulet - LA
                'offerId'           => 'ad907856-6686-4e1e-a8ca-9aa2629b9cee',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5842 => [//Gluco PRO - LA
                'offerId'           => 'fe0eba2e-e575-49bb-a44a-4b95fd03b472',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5841 => [//ArthroMAX - LA
                'offerId'           => 'a731d55c-1c25-492d-b7a4-45356edda76c',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5838 => [//Crystalix - LA
                'offerId'           => '84b72ea6-6c05-45d2-ad79-eedd96d35c96',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5837 => [//Cardioton - LA
                'offerId'           => 'e84aef2f-3e17-4af1-8f18-bced41ce03a2',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5836 => [//ViaGrain - LA
                'offerId'           => '96121e6f-621f-4aac-b512-145b84d3e941',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5835 => [//Specialift - LA
                'offerId'           => 'aa0499ca-6279-40d8-a86b-f0bf2cce2118',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5749 => [//Crystalix - MY
                'offerId'           => '66daf6bf-2558-4b46-93e6-9b333d38ae0f',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5748 => [//Crystalix - TN
                'offerId'           => '078d9dfc-3a0e-4c34-9939-f4d99d3727bf',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5731 => [//Insu Herbal - GT
                'offerId'           => '90e38cc3-edc2-49a1-a72a-04918925d3a0',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5730 => [//Gluco Pro - GT
                'offerId'           => 'ffcfefe6-f35b-4ebd-bec3-b6d443b95971',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5712 => [//Ultraprost caps - PH
                'offerId'           => 'ffe953e0-1a94-4c41-8fce-f015c6061c11',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5215 => [//Ketolipozin - IQ
                'offerId'           => '8b28c537-d2ef-41eb-ac9f-edd328e0238d',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5213 => [//Enterotox - CO_
                'offerId'           => '8df61a5f-bce1-4741-ab22-3b69a891be8f',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5220 => [//Cafe Verde - EC
                'offerId'           => '35dd8a05-9315-4d65-bbcf-edb2eaa3ad46',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4177 => [//Crystalix - IN
                'offerId'           => 'edd9139a-943f-4da3-9dc7-9c63e4294b2d',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4178 => [//Cardioton - IN
                'offerId'           => '0c81db7c-4927-4cdb-86e7-668e8cd611c8',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5012 => [//Biodermalix - CL
                'offerId'           => 'da4a4635-1a8c-4e88-86af-def2b330cdec',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4987 => [//Enterofort - EC
                'offerId'           => 'd9442682-f7f2-4c79-ba82-14a15c1a9172',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3887 => [//Dietica - EG
                'offerId'           => '16a73a79-e8ef-4dc2-8630-d867cdc80661',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3891 => [//Maxup Caps - MA
                'offerId'           => '08cf3ce6-a966-4d95-b077-550a05b8d0e4',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3821 => [
                'offerId'       	=> 'e0002027-cbc1-4069-841f-82634c4ea437',
                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3835 => [ //Green Coffee - MM
                'offerId'           => '0928a7cc-fbbc-4d01-850b-1446f1db6c58',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3834 => [ //GHammer of Thor - MM
                'offerId'           => '924ce2d1-b45f-43f0-9d3e-b815f1df93c8',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3847 => [ //Rhythmengix - CO
                'offerId'           => 'b332ed65-adaf-49f3-a92b-305662625479',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3857 => [ //Prostazex - PE
                'offerId'           => '96467c94-26dc-41d5-8fa9-b7d25c5d09a4',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3852 => [ //Crystalix - CO
                'offerId'           => 'f4b82d52-d563-45b9-a364-0507d35a29b3',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            3836 => [ //Stability - MM
                'offerId'           => '717153c1-8bc3-4305-ab16-dfbce8ec09a4',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            3849 => [ //Flexibility - PK
                'offerId'           => 'c1755290-7f3e-4055-ab62-928fabdb38ce',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            3850 => [ //Giperium - IR
                'offerId'           => '143847bc-1f23-4af4-a230-4474da5bbd07',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            3851 => [ //Goji Berry - IR
                'offerId'           => 'b6e2f23e-27f0-4755-b549-4e564e35e0de',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            3848 => [ //Giperium - PK
                'offerId'           => 'bce776cf-87f2-465b-afcc-b888999c91b6',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            3934 => [ // Dietica - IN
                'offerId'           => 'e038ce93-8609-4c07-9471-beefc1e1a63d',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            3935 => [ // Green Coffee - NP
                'offerId'           => '472be40d-e7e1-40de-a056-7601d4e78aa9',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            3939 => [ // Neurodiet - MA
                'offerId'           => 'f2a46db4-02ec-4696-a8dc-c7cbdf7ad3c3',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            3933 => [ // Dozex - PH
                'offerId'           => '9c4fe23b-37ea-4954-945b-29067b0d5c0e',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            3932 => [ // Green Coffee - IN
                'offerId'           => 'ddac4eaf-a46d-41f0-beda-fd282cc6b742',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            3931 => [ // Dietica - ZA
                'offerId'           => '07b20279-9c35-447d-ac63-f0d49270937f',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            3930 => [ // Giperium - LK
                'offerId'           => 'e1f8ebdc-a0ea-4d08-9eef-5d7a313a2d46',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            3929 => [ // Flexibility - LK
                'offerId'           => 'd78789d7-3a2c-4095-b0c8-6725d8b2252f',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            3948 => [ // Maxup caps - ZA
                'offerId'           => '58d4eec3-2e3e-4b42-be5f-f4461e389ba6',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            3954 => [ //Vascularix - CL
                'offerId'           => '37385aaf-ebda-444e-b2ce-7b9108fe3601',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            3955 => [ //Dietica - CL
                'offerId'           => '6609aa7f-0c54-4ea3-a032-10c77d8dc2a3',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4912 => [ //Prolibidox (potency) - CL
                'offerId'           => '02c38b0a-3c6c-4bd4-9733-0eeffac57f53',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4030 => [ //Prolibidox - CL
                'offerId'           => 'eb429b02-686f-4983-be0a-5aaccd5d411d',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4073 => [ //Flexibility MY
                'offerId'           => 'aab43c72-6b52-44c9-b947-5bdcf59b9e70',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4083 => [ //Dozex caps - MY
                'offerId'           => '572e5488-3d38-488b-902f-281db751414a',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4089 => [ //Giperium - BD
                'offerId'           => 'ad573e59-2cc5-4d08-b376-3173d18a1a73',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4102 => [ //Cardioton - PE
                'offerId'           => 'a90fbfbc-1e7d-43bb-ab05-341e1bcf34c6',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4084 => [   //Dietica - MY
                'offerId'           => 'a2f20d3b-59e5-4395-9f47-27403e5a029a',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4252 => [   //Dietica fizzy - PH
                'offerId'           => '6eb00b0e-a01b-4f54-9657-3e2fcf871a5c',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4258 => [   //Lumenix - CL
                'offerId'           => '23259215-5e88-49ee-a905-9a87b895a077',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4251 => [   //Cardioton - PH
                'offerId'           => 'eddf0512-f04d-4933-8bb7-dad88aacfd16',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4253 => [   //Dietica - PH
                'offerId'           => 'c1df9d09-7b8d-40d2-8ba9-6814abe09a3d',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4257 => [   //Flexibility - NG
                'offerId'           => 'f53a5bad-728e-4ed9-8521-83bff557c592',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4189 => [   //Flexibility - PH
                'offerId'           => '8a1bdf32-946c-4bef-80da-a523f172c4a9',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4254 => [   //Green coffee
                'offerId'           => '073a60aa-d975-4ab6-979a-79e71f2adeb5',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4475 => [   //Ultraprost - MY
                'offerId'           => 'f5fe44b9-e442-4a3d-81ca-a340f1be2b68',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4514 => [   //Crystalix - EG
                'offerId'           => '21dcdc49-0d15-4e0f-8190-45c12534211f',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4515 => [   //Flexibility Powder - EG
                'offerId'           => '52013d3f-e636-430f-b040-db7477e14269',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4516 => [   //Giperium - EG
                'offerId'           => '307f48c3-b652-4f8d-ac4e-0d1f28f5c336',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4578 => [   //Green Coffee - TN
                'offerId'           => '580a45c6-0504-446f-91dd-efb7bad9cd45',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4570 => [   //Amulet - PH
                'offerId'           => 'f652012f-b0d6-47d2-bee0-83e2440b5fff',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4602 => [   //Maxup caps - PK
                'offerId'           => '9bf42ee7-59d8-4983-957f-f01a6725d3bd',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4692 => [   //Crystalix - AR 
                'offerId'           => '3e8596df-f4bd-48e7-8c49-481c9e3ae3d0',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4678 => [   //Bendax - CO
                'offerId'           => 'e864abba-9904-472f-9557-4bd9d95647ac',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
            4911 => [   //Bendax 100 - CO
                'offerId'           => 'e864abba-9904-472f-9557-4bd9d95647ac',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
           4667 => [   //Enterofort - PE
                'offerId'           => 'bc5f3af4-7325-4f01-89be-271ce59e5201',
                'configs' => [
                    'brakeLogFolder'    => true,
                ],  
            ],
           4686 => [   //Maxup caps - CO
                'offerId'           => '46e2145f-618b-46f7-a167-1eb9746411d4',
                'configs' => [
                    'brakeLogFolder'    => true, 
                ],  
            ],
           4673 => [   //Green Coffee - GT
                'offerId'           => '99ba2c18-795d-4bdf-b172-0bf47ecbfd4d',
                'configs' => [
                    'brakeLogFolder'    => true, 
                ],  
            ],
           4674 => [   //Gluco PRO - PE
                'offerId'           => 'bb2e43ec-3cbf-4692-aac5-5b0b4f2f316f',
                'configs' => [
                    'brakeLogFolder'    => true, 
                ],  
            ],
           4685 => [   //Ketolipozin - PE
                'offerId'           => '6f84d601-3250-4461-bb70-ff7308081c33',
                'configs' => [
                    'brakeLogFolder'    => true, 
                ],  
            ],
            4749 => [   //Giperium Powder - ec
                'offerId'           => '83207e04-f849-4395-a5c7-5dbfb72e8776',
                'configs' => [
                    'brakeLogFolder'    => true, 
                ],  
            ],
            4791 => [   //Ultraprost - SG
                'offerId'           => '1b2e7869-11a8-4c00-912a-a6513e3f5826',
                'configs' => [
                    'brakeLogFolder'    => true, 
                ],  
            ],
            4784 => [   //Maxup gel - SG
                'offerId'           => '410c083c-1b28-4c34-8f87-cc1df77d747a',
                'configs' => [
                    'brakeLogFolder'    => true, 
                ],  
            ],
            4790 => [   //Erogenix - SG
                'offerId'           => 'a3108677-db8e-46b7-b479-5dd5287398d5',
                'configs' => [
                    'brakeLogFolder'    => true, 
                ],  
            ],
            4787 => [   //Crystalix - SG
                'offerId'           => 'fb47b364-795d-495c-8f5a-b633840851af',
                'configs' => [
                    'brakeLogFolder'    => true, 
                ],  
            ],
            4786 => [   //Cardioton - SG
                'offerId'           => 'dd707775-44aa-4b34-8482-dbadc91fa35b',
                'configs' => [
                    'brakeLogFolder'    => true, 
                ],  
            ],
            4789 => [   //Bustup - SG
                'offerId'           => 'da521263-0c5f-4c5b-ba1c-127d63e48b12',
                'configs' => [
                    'brakeLogFolder'    => true, 
                ],  
            ],
            4788 => [   //Flexibility cream - SG
                'offerId'           => '5abdb6fb-9b31-4992-8792-6d14d8eeb6a2',
                'configs' => [
                    'brakeLogFolder'    => true, 
                ],  
            ],
            4792 => [   //Insider - SG
                'offerId'           => '9d3917a6-6fe0-4545-aba4-39d6b15fd5b7',
                'configs' => [
                    'brakeLogFolder'    => true, 
                ],  
            ],
            4817 => [   //Cardioton - ZA
                'offerId'           => '4889aa66-f106-47fd-aaeb-6ce0cd38f681',
                'configs' => [
                    'brakeLogFolder'    => true, 
                ],  
            ],
            4867 => [   //ArthroMAX - CL
                'offerId'           => 'ab2e16fd-8ae8-4496-a935-198c51d8f486',
                'configs' => [
                    'brakeLogFolder'    => true, 
                ],  
            ],
            4795 => [   //Enterofort - CL
                'offerId'           => '9866f80a-f420-4e60-b111-91712e97d16d',
                'configs' => [
                    'brakeLogFolder'    => true, 
                ],  
            ],
            4808 => [   //Gluco PRO - CL
                'offerId'           => '45bb468e-eee1-4e53-b974-dee34732cd70',
                'configs' => [
                    'brakeLogFolder'    => true, 
                ],  
            ],
            4793 => [   //Flex Active - IR
                'offerId'           => '293ab8fd-9d74-40d9-8e89-356ddff30ebf',
                'configs' => [
                    'brakeLogFolder'    => true, 
                ],  
            ],
            4946 => [   //Maxup gel - PH
                'offerId'           => '5876bfb5-5f26-4de9-8784-ec27e3449920',
                'configs' => [
                    'brakeLogFolder'    => true, 
                ],  
            ],
        ],
    ],

    'configs' => [
        'statuses' => [
            'trash'	    => [
                'trash'	          	=> '',
            ],
            'reject'	=> [
                'reject'	        => '',
            ],
            'expect'	=> [
                'expect'			=> '',
            ],
            'confirm'	=> [
                'confirm'	       	=> '',
            ],
        ],

        'brakeLogFolder'	=> true,
        'urlOrderAdd'		=> 'https://sendmelead.com/api/v3/lead/add',
//		'statusEnabled'		=> false,
    ],
];