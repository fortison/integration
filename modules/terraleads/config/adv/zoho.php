<?php

/*
 * Для генераціх нового токену
 * https://accounts.zoho.com/apiauthtoken/nb/create?SCOPE=ZohoCRM/crmapi&EMAIL_ID=info@biodakler.com&PASSWORD=Zoho787lidI
 * https://accounts.zoho.com/u/h#sessions/userauthtoken
 *
 * https://www.zoho.com/crm/help/api/insertrecords.html
 * https://www.zoho.com/crm/help/api/getrecords.html
 *
 * Налаштування постбеків
 * https://crm.zoho.com/crm/org676738239/settings/workflow-rules # постбеки
 * Automation -> Action -> Webhooks -> Configure Webhook
 *
 * Для роботи по новому апі
 * https://www.zoho.com/crm/help/api/v2/
 * https://accounts.zoho.com/developerconsole
 */

return [
	
	13916 => [
		'terraleadsApiKey'			=> '250a9731063fb13f0d60bfc931ac0347',
		'postbackApiKey' 			=> '3f0d60bfc931ac0347250a9731063fb1',
		'authtoken' 				=> '83e30f26469dc6eb84f6196ad8edd735',												# Чортівня для старої версії інтеграції

		'clientId'					=> '1000.82YJG848W4T333151C0OILQ0WG4IR0',											# Для роботи по новому апі
		'clientSecret'				=> 'a8d08c9ed307dce4f0fcc9043d08a1ff05520c0168',
		'redirectUrl'				=> 'http://connectcpa.net/terraleads/adv/extra-zoho',

		'offers' => [
			1912 => [
				'company' => 'ChocoLite - LT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2173 => [
				'company' => 'Strongup - LV',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2172 => [
				'company' => 'Strongup - LT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2171 => [
				'company' => 'Fungonis - LV',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2170 => [
				'company' => 'Fungonis - LT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2169 => [
				'company' => 'Moveflex - LV',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2168 => [
				'company' => 'Moveflex - LT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2167 => [
				'company' => 'Papillor - LV',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2166 => [
				'company' => 'Papillor - LT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2175 => [
				'company' => 'ChocoLite - LV',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2184 => [
				'company' => 'ChocoLite - EE',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2213 => [
				'company' => 'Ostelife - LV',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1907 => [
				'company' => 'Ostelife - LT',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1909 => [
				'company' => 'FizzySlim - LT',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2225 => [
				'company' => 'Choco Lite - FI',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2215 => [
				'company' => 'Ostelife - EE',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2545 => [
				'company' => 'Fizzy Slim - FI',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2320 => [
				'company' => 'Ostelife - FI',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2334 => [
				'company' => 'Fizzy Slim - LV',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2333 => [
				'company' => 'Choco Lite - NL',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2393 => [
				'company' => 'Choco Lite - BE',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2421 => [
				'company' => 'Reaction - LV',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2003 => [
				'company' => 'ReAction - LT',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2422 => [
				'company' => 'Reaction - EE',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],

	15929 => [
		'terraleadsApiKey'			=> '90d0c398c27d28ee91d573258deb6b25',
		'postbackApiKey' 			=> '1ac0347250a973f0d60bfc9331063fb1',
		'authtoken' 				=> 'aebeb85fb6b7a5153a83b710bfa2aa1e',

		'offers' => [
			2270 => [
				'company' => 'Kyöko Uno - PL',
				'external' => 'Terraleads',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2458 => [
				'company' => 'Magnetic Insoles - PL',
				'external' => 'Terraleads',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2457 => [
				'company' => 'Kyöko Uno - IT',
				'external' => 'Terraleads',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2475 => [
				'company'  => 'Hip and Knee Magnetic Belt - PL',
				'external' => 'Terraleads',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2621 => [
				'company' => 'Pumpkin Seed Pro - PL',
				'external' => 'Terraleads',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2620 => [
				'company' => 'Chlorogenic - PL',
				'external' => 'Terraleads',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2619 => [
				'company' => 'Diet N1 - PL',
				'external' => 'Terraleads',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2683 => [
				'company' => 'Audio Stimulator - PL, Patches',
				'external' => 'Terraleads',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2684 => [
				'company' => 'Neumann Knee care - IT, Magnetic Band',
				'external' => 'Terraleads',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2682 => [
				'company' => 'Audio Stimulator - IT, Patches',
				'external' => 'Terraleads',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2681 => [
				'company' => 'Pumpkin Seed PRO - IT',
				'external' => 'Terraleads',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2680 => [
				'company' => 'Magnetic Insoles - IT',
				'external' => 'Terraleads',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],

		'configs' => [
			'statuses' => [
				'trash'		=> [
					'tresh'				=> '',
				],
				'reject'	=> [
					'reject'			=> '',
				],
				'expect'	=> [
					'expect'			=> '',
				],
				'confirm'	=> [
					'confirm'			=> '',
				],
			],
		],
	],
	20816 => [
		'terraleadsApiKey'			=> 'ad16e43702a791c7edb8c3f484a1f245',
		'postbackApiKey' 			=> '1ac0347250a973f0d61bfc9331063fb1',
		'authtoken' 				=> '996a791cbe020bf4e87505161d5f8a2b',

		'offers' => [
			2274 => [
				'company' => 'Hair Straightener 4 in 1 - ES',
				'external' => 'Terraleads',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			
		],

		'configs' => [
			'statuses' => [
				'trash'		=> [
					'Junk Lead'				=> '',
				],
				'reject'	=> [
					'Not Qualified'			=> '',
				],
				'expect'	=> [
					'Attempted to Contact'	=> '',
					'Contact in Future'		=> '',
				],
				'confirm'	=> [
					'confirmed'				=> '',
				],
			],
		],
	],
	
	16963 => [
		'terraleadsApiKey'			=> '3824224b2bcc4600771ea7cb70d7521d',
		'postbackApiKey' 			=> '50a973f0d61ac034720bfc9331063fb1',
		'authtoken' 				=> '32a5cb45596d234a6f4d200aaee0d1ef',

		'offers' => [
			2270 => [
				'company' => 'Spirulina',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2271 => [
				'company' => 'Green coffee',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],

	'configs' => [
		'statuses' => [
			'trash'		=> [
				'Junk Lead'				=> '',
				'Not Contacted'			=> '',
			],
			'reject'	=> [
				'Lost Lead'				=> '',
				'Not Qualified'			=> '',
			],
			'expect'	=> [
				''						=> '',
				'Attempted to Contact'	=> '',
				'Contact in Future'		=> '',
				'Contacted'				=> '',
			],
			'confirm'	=> [
				'Pre Qualified'			=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://crm.zoho.com/crm/private/xml/Leads/insertRecords',
//		'urlOrderInfo'		=> 'https://www.zohoapis.com/crm/v2/Leads',
//		'urlOrderInfo'		=> 'https://crm.zoho.com/crm/private/xml/Leads/getRecords',
	],
];