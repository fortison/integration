<?php

/*
 * В файла API.rar
 *
 * http://connectcpa.net/apilead/adv/adv-postback-status?systemUserId=16040&systemOrderId={click_id}&partnerStatus={status}&comment={comment}&postbackApiKey=75325781a3c9002e827687a980308eda
 */

return [

	25404 => [
		'terraleadsApiKey'			=> 'af9a895b0d3bcc1e0eb5e0bef15bd768',
		'postbackApiKey'			=> '75325781a3c1002e827687a980308eda',
		'appId'						=> '00474810309584611072',
		'appSecret'					=> '0abb0decb0ab0947d439cb9692f12d0b3f13c559',

		'offers' => [			

		],
	],
	
	16040 => [
		'terraleadsApiKey'			=> 'f88d0c668354c423bcc8d3b77c7b8dcd',
		'postbackApiKey'			=> '75325781a3c9002e827687a980308eda',
		'appId'						=> '01005469977562113322',
		'appSecret'					=> '8491fad2297f06c8f0141e83efb05615a61089eb',
		
		'offers' => [
			9012 => [ //Kitchen Set 11 - GR
			        'offerId' => 42928,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 49193,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			9011 => [ //Kitchen Set 11 - RO
			        'offerId' => 42929,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 49194,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			9010 => [ //Kitchen Set 11 - PL
			        'offerId' => 42930,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 49195,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			9009 => [ //Kitchen Set 11 - SI
			        'offerId' => 42931,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 49196,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			9008 => [ //Kitchen Set 11 - HU
			        'offerId' => 42932,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 49197,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			9007 => [ //Kitchen Set 11 - HR
			        'offerId' => 42933,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 49198,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			9006 => [ // Kitchen Set 11 - BG
			        'offerId' => 42934,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 49199,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			9002 => [ //Riddex Pex Repelling Aid - RO
		        'offerId' => 42898,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 49149,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],9001 => [ //Pest Reject - RO
		        'offerId' => 42897,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 49148,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8783 => [ //Golf Toilet Brush - HU
		        'offerId' => 42525,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 48687,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8782 => [ //Golf Toilet Brush - PL
		        'offerId' => 42524,
		        'country' => 'PL',
		        'currency' => 'PLN',
		        'payoutId' => 48686,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8781 => [ //Golf Toilet Brush - SK
		        'offerId' => 42523,
		        'country' => 'SI',
		        'currency' => 'EUR',
		        'payoutId' => 48685,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8780 => [ //Golf Toilet Brush - CZ
		        'offerId' => 42522,
		        'country' => 'HR',
		        'currency' => 'EUR',
		        'payoutId' => 48684,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8779 => [ //Golf Toilet Brush - SI
		        'offerId' => 42521,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 48683,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8778 => [ //Golf Toilet Brush - HR
		        'offerId' => 42520,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 48682,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8777 => [ //Golf Toilet Brush - BG
		        'offerId' => 42519,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 48681,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8636 => [ //Fascial Gun Massager - HU
		        'offerId' => 42444,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 48604,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8635 => [ //Fascial Gun Massager - PL
		        'offerId' => 42443,
		        'country' => 'PL',
		        'currency' => 'PLN',
		        'payoutId' => 48603,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8634 => [ //Fascial Gun Massager - SI
		        'offerId' => 42442,
		        'country' => 'SI',
		        'currency' => 'EUR',
		        'payoutId' => 48602,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8633 => [ //Fascial Gun Massager - HR
		        'offerId' => 42439,
		        'country' => 'HR',
		        'currency' => 'EUR',
		        'payoutId' => 48599,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8631 => [ //Fascial Gun Massager - GR
		        'offerId' => 42438,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 48598,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8630 => [ //Fascial Gun Massager - RO
		        'offerId' => 42437,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 48597,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8629 => [ //Fascial Gun Massager - BG
		        'offerId' => 42436,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 48596,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8658 => [ //Drone 998PRO - HR
		        'offerId' => 42450,
		        'country' => 'HR',
		        'currency' => 'EUR',
		        'payoutId' => 48610,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8657 => [ //Drone 998PRO - SI
		        'offerId' => 42449,
		        'country' => 'SI',
		        'currency' => 'EUR',
		        'payoutId' => 48609,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8656 => [ //Drone 998PRO - HU
		        'offerId' => 42448,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 48608,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8655 => [ //Drone 998PRO - SK
		        'offerId' => 42447,
		        'country' => 'SK',
		        'currency' => 'EUR',
		        'payoutId' => 48607,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8654 => [ //Drone 998PRO - CZ
		        'offerId' => 42446,
		        'country' => 'CZ',
		        'currency' => 'CZK',
		        'payoutId' => 48606,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8653 => [ //Drone 998PRO - PL
		        'offerId' => 42445,
		        'country' => 'PL',
		        'currency' => 'PLN',
		        'payoutId' => 48605,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8413 => [ //Drone 998PRO - GR
		        'offerId' => 41654,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 47760,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8412 => [ //Drone 998PRO - RO
		        'offerId' => 41653,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 47759,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8425 => [ //OnePower Readers - HR
		        'offerId' => 41652,
		        'country' => 'HR',
		        'currency' => 'EUR',
		        'payoutId' => 47758,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8424 => [ //OnePower Readers - SI
		        'offerId' => 41651,
		        'country' => 'SI',
		        'currency' => 'EUR',
		        'payoutId' => 47757,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8423 => [ //OnePower Readers - CZ
		        'offerId' => 41650,
		        'country' => 'CZ',
		        'currency' => 'CZK',
		        'payoutId' => 47756,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8422 => [ //HOnePower Readers - SK
		        'offerId' => 41649,
		        'country' => 'SK',
		        'currency' => 'EUR',
		        'payoutId' => 47755,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8421 => [ //OnePower Readers - PL
		        'offerId' => 41648,
		        'country' => 'PL',
		        'currency' => 'PLN',
		        'payoutId' => 47754,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8420 => [ //OnePower Readers - HU
		        'offerId' => 41647,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 47753,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8419 => [ //OnePower Readers - GR
		        'offerId' => 41646,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 47752,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8418 => [ //OnePower Readers - RO
		        'offerId' => 41645,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 47751,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8417 => [ //OnePower Readers - BG
		        'offerId' => 41644,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 47750,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8628 => [ //Hurricane Spin Scrubber - HU
		        'offerId' => 41643,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 47749,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8627 => [ //Hurricane Spin Scrubber - PL
		        'offerId' => 41642,
		        'country' => 'PL',
		        'currency' => 'PLN',
		        'payoutId' => 47748,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8626 => [ //Hurricane Spin Scrubber - CZ
		        'offerId' => 41641,
		        'country' => 'CZ',
		        'currency' => 'CZK',
		        'payoutId' => 47747,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8625 => [ //Hurricane Spin Scrubber - SK
		        'offerId' => 41640,
		        'country' => 'SK',
		        'currency' => 'EUR',
		        'payoutId' => 47746,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8624 => [ //Hurricane Spin Scrubber - SI
		        'offerId' => 41639,
		        'country' => 'SI',
		        'currency' => 'EUR',
		        'payoutId' => 47745,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8623 => [ //Hurricane Spin Scrubber - HR
		        'offerId' => 41638,
		        'country' => 'HR',
		        'currency' => 'EUR',
		        'payoutId' => 47744,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8622 => [ //Hurricane Spin Scrubber - GR
		        'offerId' => 41637,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 47743,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8621 => [ //Hurricane Spin Scrubber - RO
		        'offerId' => 41636,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 47742,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8620 => [ //Hurricane Spin Scrubber - BG
		        'offerId' => 41635,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 47741,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8326 => [ //Pro Wax 100 Mask - HU
		        'offerId' => 41396,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 47749,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8325 => [ //Pro Wax 100 Mask - PL
		        'offerId' => 41395,
		        'country' => 'PL',
		        'currency' => 'PLN',
		        'payoutId' => 47481,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8324 => [ //Pro Wax 100 Mask - HR
		        'offerId' => 41394,
		        'country' => 'HR',
		        'currency' => 'EUR',
		        'payoutId' => 47480,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8323 => [ //Pro Wax 100 Mask - SI
		        'offerId' => 41393,
		        'country' => 'SI',
		        'currency' => 'EUR',
		        'payoutId' => 47479,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8322 => [ //Pro Wax 100 Mask - CZ
		        'offerId' => 41392,
		        'country' => 'CZ',
		        'currency' => 'CZK',
		        'payoutId' => 47478,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8321 => [ //Pro Wax 100 Mask - SK
		        'offerId' => 41391,
		        'country' => 'SK',
		        'currency' => 'EUR',
		        'payoutId' => 47477,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8320 => [ //Pro Wax 100 Mask - RO
		        'offerId' => 41390,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 47476,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8319 => [ //Pro Wax 100 Mask - BG
		        'offerId' => 41389,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 47475,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8318 => [ //Pro Wax 100 Mask - GR
		        'offerId' => 41388,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 47474,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8518 => [ //FOutdoor Wifi Camera - SI
		        'offerId' => 41386,
		        'country' => 'SI',
		        'currency' => 'EUR',
		        'payoutId' => 47472,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8519 => [ //Outdoor Wifi Camera - HR
		        'offerId' => 41387,
		        'country' => 'HR',
		        'currency' => 'EUR',
		        'payoutId' => 47473,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8517 => [ //Flavorwave Turbo Oven - HU
		        'offerId' => 41251,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 47337,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8516 => [ //Flavorwave Turbo Oven - PL
		        'offerId' => 41250,
		        'country' => 'PL',
		        'currency' => 'PLN',
		        'payoutId' => 47336,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8515 => [ //Flavorwave Turbo Oven - SK
		        'offerId' => 41249,
		        'country' => 'SK',
		        'currency' => 'EUR',
		        'payoutId' => 47335,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8514 => [ //Flavorwave Turbo Oven - CZ
		        'offerId' => 41248,
		        'country' => 'CZ',
		        'currency' => 'CZK',
		        'payoutId' => 47334,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8513 => [ //Flavorwave Turbo Oven - SI
		        'offerId' => 41247,
		        'country' => 'SI',
		        'currency' => 'EUR',
		        'payoutId' => 47333,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8512 => [ //Flavorwave Turbo Oven - HR
		        'offerId' => 41246,
		        'country' => 'HR',
		        'currency' => 'EUR',
		        'payoutId' => 47332,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8511 => [ //Flavorwave Turbo Oven - GR
		        'offerId' => 41245,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 47331,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8510 => [ //Flavorwave Turbo Oven - RO
		        'offerId' => 41244,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 47330,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8509 => [ //Flavorwave Turbo Oven - BG
		        'offerId' => 41243,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 47329,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8536 => [ //3D Pen - HU
		        'offerId' => 40806,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 46802,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8535 => [ //3D Pen - PL
		        'offerId' => 40805,
		        'country' => 'PL',
		        'currency' => 'PLN',
		        'payoutId' => 46801,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8534 => [ //3D Pen - SI
		        'offerId' => 40801,
		        'country' => 'SI',
		        'currency' => 'EUR',
		        'payoutId' => 46797,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8533 => [ //3D Pen - HR
		        'offerId' => 40800,
		        'country' => 'HR',
		        'currency' => 'EUR',
		        'payoutId' => 46796,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8532 => [ //3D Pen - CZ
		        'offerId' => 40799,
		        'country' => 'CZ',
		        'currency' => 'CZK',
		        'payoutId' => 46795,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8531 => [ //3D Pen - SK
		        'offerId' => 40795,
		        'country' => 'SK',
		        'currency' => 'EUR',
		        'payoutId' => 46791,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8530 => [ //3D Pen - GR
		        'offerId' => 40792,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 46788,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8529 => [ //3D Pen - RO
		        'offerId' => 40791,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 46787,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8458 => [ //Air Fryer - HU
		        'offerId' => 40553,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 46549,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8457 => [ //Air Fryer - PL
		        'offerId' => 40552,
		        'country' => 'PL',
		        'currency' => 'PLN',
		        'payoutId' => 46548,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8456 => [ //Air Fryer - SK
		        'offerId' => 40551,
		        'country' => 'SK',
		        'currency' => 'EUR',
		        'payoutId' => 46547,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8455 => [ //Air Fryer - CZ
		        'offerId' => 40550,
		        'country' => 'CZ',
		        'currency' => 'CZK',
		        'payoutId' => 46546,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8454 => [ //Air Fryer - HR
		        'offerId' => 40549,
		        'country' => 'HR',
		        'currency' => 'EUR',
		        'payoutId' => 46545,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8453 => [ //Air Fryer - SI
		        'offerId' => 40548,
		        'country' => 'SI',
		        'currency' => 'EUR',
		        'payoutId' => 46544,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8452 => [ //Air Fryer - GR
		        'offerId' => 40547,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 46543,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8451 => [ //Air Fryer - RO
		        'offerId' => 40546,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 46542,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8450 => [ //Air Fryer - BG
		        'offerId' => 40545,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 46541,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8437 => [ //Slim and Lift Bra 3 in 1 - RO
		        'offerId' => 40234,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 46216,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8436 => [ //Slim and Lift Bra 3 in 1 - BG
		        'offerId' => 40233,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 46215,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8435 => [ //Slim and Lift Bra 3 in 1 - GR
		        'offerId' => 40232,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 46214,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8434 => [ //Slim and Lift Bra - RO
		        'offerId' => 40231,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 46213,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8433 => [ //Slim and Lift Bra - BG
		        'offerId' => 40230,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 46212,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8432 => [ //Slim and Lift Bra - GR
		        'offerId' => 40229,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 46211,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8449 => [ //Instant Leak Stop - SK
		        'offerId' => 40228,
		        'country' => 'SK',
		        'currency' => 'EUR',
		        'payoutId' => 46210,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8448 => [ //Instant Leak Stop - PL
		        'offerId' => 40227,
		        'country' => 'PL',
		        'currency' => 'PLN',
		        'payoutId' => 46209,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8447 => [ //Instant Leak Stop - HU
		        'offerId' => 40226,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 46208,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8446 => [ //Instant Leak Stop - SI
		        'offerId' => 40225,
		        'country' => 'SI',
		        'currency' => 'EUR',
		        'payoutId' => 46207,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8445 => [ //Instant Leak Stop - GR
		        'offerId' => 40224,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 46206,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8444 => [ //Instant Leak Stop - RO
		        'offerId' => 40223,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 46205,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8443 => [ //Mini Drone - HU
		        'offerId' => 40212,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 46192,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8442 => [ //Mini Drone - PL
		        'offerId' => 40211,
		        'country' => 'PL',
		        'currency' => 'PLN',
		        'payoutId' => 46191,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8441 => [ //Mini Drone - HR
		        'offerId' => 40210,
		        'country' => 'HR',
		        'currency' => 'EUR',
		        'payoutId' => 46190,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8440 => [ //Mini Drone - SI
		        'offerId' => 40209,
		        'country' => 'SI',
		        'currency' => 'EUR',
		        'payoutId' => 46189,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8439 => [ //Mini Drone - CZ
		        'offerId' => 40208,
		        'country' => 'CZ',
		        'currency' => 'CZK',
		        'payoutId' => 46188,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8438 => [ //SMini Drone - SK
		        'offerId' => 40207,
		        'country' => 'SK',
		        'currency' => 'EUR',
		        'payoutId' => 46187,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8427 => [ //Mini Drone - RO
		        'offerId' => 40206,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 46186,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8426 => [ //Mini Drone - GR
		        'offerId' => 40205,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 46185,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8414 => [ //Golf Toilet Brush - GR
		        'offerId' => 40184,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 46164,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8415 => [ //Golf Toilet Brush - RO
		        'offerId' => 40183,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 46163,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8313 => [ //SQ11 Camera - GR
		        'offerId' => 40149,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 46129,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8314 => [ //SQ11 Camera - SI
		        'offerId' => 40150,
		        'country' => 'SI',
		        'currency' => 'EUR',
		        'payoutId' => 46130,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8315 => [ //CSQ11 Camera - HR - TL
		        'offerId' => 40151,
		        'country' => 'HR',
		        'currency' => 'EUR',
		        'payoutId' => 46131,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8316 => [ //SQ11 Camera - PL - TL
		        'offerId' => 40152,
		        'country' => 'PL',
		        'currency' => 'PLN',
		        'payoutId' => 46132,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8317 => [ //SQ11 Camera - HU - TL
		        'offerId' => 40153,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 46133,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8328 => [ //Cordless Chainsaw - GR
		        'offerId' => 40046,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 45962,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8329 => [ //Cordless Chainsaw - RO
		        'offerId' => 40047,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 45963,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8298 => [ //Floating Globus - BG
		        'offerId' => 40018,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 45922,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8297 => [ //Floating Globus - GR
		        'offerId' => 40017,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 45921,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8296 => [ //Floating Globus - RO
		        'offerId' => 40016,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 45920,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8299 => [ //Solar Induction Lamp - GR
		        'offerId' => 40004,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 45908,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8300 => [ //Solar Induction Lamp - RO
		        'offerId' => 40005,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 45909,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8301 => [ //Solar Induction Lamp - HU
		        'offerId' => 40006,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 45910,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8302 => [ //Solar Induction Lamp - CZ
		        'offerId' => 40007,
		        'country' => 'CZ',
		        'currency' => 'CZK',
		        'payoutId' => 45911,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8303 => [ //Solar Induction Lamp - SK
		        'offerId' => 40008,
		        'country' => 'SK',
		        'currency' => 'EUR',
		        'payoutId' => 45912,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8295 => [ //Drone 998PRO - BG
		        'offerId' => 40002,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 45906,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8281 => [ //Mini Drone - BG
		        'offerId' => 39970,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 45864,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8280 => [ //3D Pen - BG
		        'offerId' => 39969,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 45863,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			6090 => [ //Bulby Solar Lamp - RO
		        'offerId' => 39939,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 45830,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			6089 => [ //Bulby Solar Lamp - GR
		        'offerId' => 39938,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 45829,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			2892 => [ //SQ11 Camera - RO
		        'offerId' => 39934,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 45825,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			2891 => [ //SQ11 Camera - SK
		        'offerId' => 39933,
		        'country' => 'SK',
		        'currency' => 'EUR',
		        'payoutId' => 45824,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			2889 => [ //SQ11 Camera - CZ
		        'offerId' => 39932,
		        'country' => 'CZ',
		        'currency' => 'CZK',
		        'payoutId' => 45823,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			2888 => [ //SQ11 Camera - BG
		        'offerId' => 39931,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 45822,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8194 => [ //Outdoor Wifi Camera - HU
		        'offerId' => 39501,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 45385,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8193 => [ //Outdoor Wifi Camera - PL
		        'offerId' => 39500,
		        'country' => 'PL',
		        'currency' => 'PLN',
		        'payoutId' => 45384,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8192 => [ //Outdoor Wifi Camera - GR
		        'offerId' => 39499,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 45383,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8191 => [ //Outdoor Wifi Camera - RO
		        'offerId' => 39498,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 45382,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8190 => [ //Outdoor Wifi Camera - BG
		        'offerId' => 39497,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 45381,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8172 => [ //Laser Light - SK
		        'offerId' => 39409,
		        'country' => 'SK',
		        'currency' => 'EUR',
		        'payoutId' => 45290,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8173 => [ //Laser Light - CZ
		        'offerId' => 39408,
		        'country' => 'CZ',
		        'currency' => 'CZK',
		        'payoutId' => 45289,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8106 => [ //Mystery Box- HU
		        'offerId' => 38928,
		        'country' => 'PL',
		        'currency' => 'PLN',
		        'payoutId' => 44765,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8081 => [ //Mystery Box- HU
		        'offerId' => 38920,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 44757,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8080 => [ //Mystery Box - SK
		        'offerId' => 38921,
		        'country' => 'SK',
		        'currency' => 'EUR',
		        'payoutId' => 44758,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8079 => [ //Mystery Box - CZ
		        'offerId' => 38922,
		        'country' => 'CZ',
		        'currency' => 'CZK',
		        'payoutId' => 44759,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8078 => [ //Mystery Box - SI
		        'offerId' => 38923,
		        'country' => 'SI',
		        'currency' => 'EUR',
		        'payoutId' => 44760,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8077 => [ //Mystery Box - HR
		        'offerId' => 38924,
		        'country' => 'HR',
		        'currency' => 'EUR',
		        'payoutId' => 44761,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8076 => [ //Mystery Box - BG
		        'offerId' => 38925,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 44762,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8075 => [ //Mystery Box - RO
		        'offerId' => 38926,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 44763,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8074 => [ //Mystery Box - GR
		        'offerId' => 38927,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 44764,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8047 => [ //Magic Glow - SK
		        'offerId' => 37927,
		        'country' => 'SK',
		        'currency' => 'EUR',
		        'payoutId' => 43666,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8046 => [ //Magic Glow - CZ
		        'offerId' => 37926,
		        'country' => 'CZ',
		        'currency' => 'CZK',
		        'payoutId' => 43665,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8045 => [ //Magic Glow - HU
		        'offerId' => 37925,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 43664,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8042 => [ //Magic Glow - RO
		        'offerId' => 37924,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 43663,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8041 => [ //Magic Glow - GR
		        'offerId' => 37923,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 43662,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			2258 => [ //Flawless - CZ
		        'offerId' => 37916,
		        'country' => 'CZ',
		        'currency' => 'CZK',
		        'payoutId' => 43655,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			3110 => [ //Flawless - SK
		        'offerId' => 37915,
		        'country' => 'SK',
		        'currency' => 'EUR',
		        'payoutId' => 43654,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			7957 => [ //Flame Heater - PL
		        'offerId' => 36920,
		        'country' => 'PL',
		        'currency' => 'PLN',
		        'payoutId' => 42621,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			7956 => [ //Flame Heater - HR
		        'offerId' => 36919,
		        'country' => 'HR',
		        'currency' => 'EUR',
		        'payoutId' => 42620,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			7955 => [ //Flame Heater - GR
		        'offerId' => 36918,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 42619,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			7954 => [ //Flame Heater - SI
		        'offerId' => 36917,
		        'country' => 'SI',
		        'currency' => 'EUR',
		        'payoutId' => 42618,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			6418 => [ //Flame Heater - HU
		        'offerId' => 36916,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 42617,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			6417 => [ //Flame Heater - SK
		        'offerId' => 36915,
		        'country' => 'SK',
		        'currency' => 'EUR',
		        'payoutId' => 42616,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			6416 => [ //Flame Heater - CZ
		        'offerId' => 36914,
		        'country' => 'CZ',
		        'currency' => 'CZK',
		        'payoutId' => 42615,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			6415 => [ //Flame Heater - BG
		        'offerId' => 36913,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 42614,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			6414 => [ //Flame Heater - RO
		        'offerId' => 36912,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 42613,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			2313 => [ //HD Glasses - HU
		        'offerId' => 195,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 320,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
			8035 => [ // HD Glasses - CZ
				'offerId' => 37586,
				'country' => 'CZ',
				'currency' => 'CZK',
				'payoutId' => 43318,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8034 => [ // HD Glasses - SK
				'offerId' => 37585,
				'country' => 'SK',
				'currency' => 'EUR',
				'payoutId' => 43317,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7854 => [ // SpotFlop - RO
				'offerId' => 36103,
				'country' => 'RO',
				'currency' => 'RON',
				'payoutId' => 41775,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7853 => [ // SpotFlop - SK
				'offerId' => 36102,
				'country' => 'SK',
				'currency' => 'EUR',
				'payoutId' => 41774,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7852 => [ // SpotFlop - CZ
				'offerId' => 36101,
				'country' => 'CZ',
				'currency' => 'CZK',
				'payoutId' => 41773,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7851 => [ // SpotFlop - GR
				'offerId' => 36100,
				'country' => 'GR',
				'currency' => 'EUR',
				'payoutId' => 41772,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7850 => [ // SpotFlop - PL
				'offerId' => 36099,
				'country' => 'PL',
				'currency' => 'PLN',
				'payoutId' => 41771,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7849 => [ // SpotFlop - BG
				'offerId' => 36098,
				'country' => 'BG',
				'currency' => 'BGN',
				'payoutId' => 41770,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7848 => [ // SpotFlop - SI
				'offerId' => 36097,
				'country' => 'SI',
				'currency' => 'EUR',
				'payoutId' => 41769,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7847 => [ // SpotFlop - HR
				'offerId' => 36096,
				'country' => 'HR',
				'currency' => 'EUR',
				'payoutId' => 41768,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7814 => [ // SolarChamp 300W - RO
				'offerId' => 35499,
				'country' => 'RO',
				'currency' => 'RON',
				'payoutId' => 41171,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7815 => [ // SolarChamp 300W - HR
				'offerId' => 35500,
				'country' => 'HR',
				'currency' => 'EUR',
				'payoutId' => 41172,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7816 => [ // SolarChamp 300W - SI
				'offerId' => 35501,
				'country' => 'SI',
				'currency' => 'EUR',
				'payoutId' => 41173,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7817 => [ // SolarChamp 300W - CZ
				'offerId' => 35502,
				'country' => 'CZ',
				'currency' => 'CZK',
				'payoutId' => 41174,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7818 => [ // SolarChamp 300W - HU
				'offerId' => 35503,
				'country' => 'HU',
				'currency' => 'HUF',
				'payoutId' => 41175,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7819 => [ // SolarChamp 300W - PL
				'offerId' => 35504,
				'country' => 'PL',
				'currency' => 'PLN',
				'payoutId' => 41176,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7820 => [ // SolarChamp 300W - SK
				'offerId' => 35505,
				'country' => 'SK',
				'currency' => 'EUR',
				'payoutId' => 41177,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],

			5346 => [ // Handy Heater - CZ
				'offerId' => 35429,
				'country' => 'CZ',
				'currency' => 'CZK',
				'payoutId' => 41092,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			5347 => [ // SHandy Heater - SK
				'offerId' => 35430,
				'country' => 'SK',
				'currency' => 'EUR',
				'payoutId' => 41093,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			7793 => [ // SolarChamp 300W - GR
				'offerId' => 35408,
				'country' => 'GR',
				'currency' => 'EUR',
				'payoutId' => 41071,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4276 => [ //Hand Juicer - GR
			        'offerId' => 34667,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 40305,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3752 => [
			        'offerId' => 16950,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 20497,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3753 => [
			        'offerId' => 16951,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 20498,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3754 => [
			        'offerId' => 16952,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 20499,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3755 => [
			        'offerId' => 16954,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 20501,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3756 => [
			        'offerId' => 16955,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 20502,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3757 => [
			        'offerId' => 16956,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 20503,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3758 => [
			        'offerId' => 16957,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 20504,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4131 => [
			        'offerId' => 16958,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 20505,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4132 => [
			        'offerId' => 16959,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 20506,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4133 => [
			        'offerId' => 16960,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 20507,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4260 => [ //Kitchen Set 3 - GR
			        'offerId' => 16961,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 20508,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4261 => [ //Kitchen Set 3 - RO
			        'offerId' => 16962,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 20509,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4262 => [ //Kitchen Set 3 - PL
			        'offerId' => 16963,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 20510,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4263 => [ //Kitchen Set 3 - SI
			        'offerId' => 16964,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 20511,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4264 => [ //Kitchen Set 3 - HU
			        'offerId' => 16965,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 20512,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4265 => [ //Kitchen Set 3 - HR
			        'offerId' => 16966,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 20513,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4266 => [ // Kitchen Set 3 - BG
			        'offerId' => 16967,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 20514,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4267 => [
			        'offerId' => 16969,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 20516,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4268 => [
			        'offerId' => 16970,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 20517,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4269 => [
			        'offerId' => 16971,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 20518,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4270 => [
			        'offerId' => 16972,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 20519,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4271 => [
			        'offerId' => 16973,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 20520,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4272 => [
			        'offerId' => 16974,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 20521,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4273 => [
			        'offerId' => 16975,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 20522,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4283 => [
			        'offerId' => 16976,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 20523,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4284 => [
			        'offerId' => 16977,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 20524,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4285 => [
			        'offerId' => 16978,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 20525,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4286 => [
			        'offerId' => 37922,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 43661,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4287 => [
			        'offerId' => 16980,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 20527,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4288 => [
			        'offerId' => 16981,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 20528,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4289 => [
			        'offerId' => 16982,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 20529,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4290 => [
			        'offerId' => 16983,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 20530,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4291 => [
			        'offerId' => 16984,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 20531,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4292 => [
			        'offerId' => 16985,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 20532,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4293 => [
			        'offerId' => 16986,
			        'country' => 'Sl',
			        'currency' => 'EUR',
			        'payoutId' => 20533,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4294 => [
			        'offerId' => 16987,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 20534,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4295 => [
			        'offerId' => 16988,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 20535,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4296 => [
			        'offerId' => 16989,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 20536,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4297 => [
			        'offerId' => 16990,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 20537,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4298 => [
			        'offerId' => 16991,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 20538,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4299 => [
			        'offerId' => 16992,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 20539,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4300 => [
			        'offerId' => 16993,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 20540,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4302 => [
			        'offerId' => 16994,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 20541,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4303 => [
			        'offerId' => 16995,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 20542,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4304 => [
			        'offerId' => 16996,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 20543,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4305 => [
			        'offerId' => 16997,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 20544,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4306 => [
			        'offerId' => 16998,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 20545,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4307 => [
			        'offerId' => 16999,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 20546,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4308 => [
			        'offerId' => 17000,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 20547,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4309 => [
			        'offerId' => 17012,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 20563,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4310 => [
			        'offerId' => 17013,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 20566,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4311 => [
			        'offerId' => 17014,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 20567,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4312 => [
			        'offerId' => 17015,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 20568,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4313 => [
			        'offerId' => 17016,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 20569,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4314 => [
			        'offerId' => 17018,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 20571,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4315 => [
			        'offerId' => 17017,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 20570,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4316 => [
			        'offerId' => 17019,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 20572,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4317 => [
			        'offerId' => 17020,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 20575,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4318 => [
			        'offerId' => 17021,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 20576,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4319 => [
			        'offerId' => 17022,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 20577,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4320 => [
			        'offerId' => 17023,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 20578,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4321 => [
			        'offerId' => 17024,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 20581,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4322 => [
			        'offerId' => 17025,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 20582,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4199 => [ //Heart Support - RO
			        'offerId' => 8418,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 11065,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4124 => [ //Spin Scrubber
			        'offerId' => 7297,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 9708,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4123 => [ //Spin Scrubber
			        'offerId' => 7296,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 9707,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4122 => [ //Spin Scrubber
			        'offerId' => 7295,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 9706,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4121 => [ //Spin Scrubber
			        'offerId' => 7294,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 9705,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4120 => [ //Spin Scrubber
			        'offerId' => 7293,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 9704,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4119 => [ //Spin Scrubber
			        'offerId' => 7292,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 9703,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4118 => [ //Spin Scrubber
			        'offerId' => 7291,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 9702,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4110 => [ //Magnetic Back Corrector - RO
			        'offerId' => 7290,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 9701,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4109 => [ //Magnetic Back Corrector - GR
			        'offerId' => 7289,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 9700,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4136 => [ //Vacuum Ear Cleaner 
			        'offerId' => 7288,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 9699,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4135 => [ //Vacuum Ear Cleaner 
			        'offerId' => 7287,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 9698,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4134 => [ //Vacuum Ear Cleaner 
			        'offerId' => 7286,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 9697,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4129 => [ //Home fitness Revoflex Xtreme - GR - TL
			        'offerId' => 7277,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 9687,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4128 => [ //Home fitness Revoflex Xtreme - RO - TL
			        'offerId' => 7276,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 9686,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3880 => [ //Pulse Oximeter
			        'offerId' => 4666,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 6477,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3881 => [//Pulse Oximeter
			        'offerId' => 4667,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 6478,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3882 => [//Pulse Oximeter
			        'offerId' => 4668,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 6479,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3883 => [//Pulse Oximeter
			        'offerId' => 4669,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 6480,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3884 => [//Pulse Oximeter
			        'offerId' => 4670,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 6481,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3885 => [//Pulse Oximeter
			        'offerId' => 4671,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 6482,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3886 => [//Pulse Oximeter
			        'offerId' => 4672,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 6483,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3897 => [//Tree Dazzler
			        'offerId' => 4639,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 6450,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3898 => [//Tree Dazzler
			        'offerId' => 4640,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 6451,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3899 => [//Tree Dazzler
			        'offerId' => 4641,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 6452,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3900 => [//Tree Dazzler
			        'offerId' => 4642,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 6453,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3901 => [//Tree Dazzler
			        'offerId' => 4643,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 6454,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3902 => [//Tree Dazzler
			        'offerId' => 4644,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 6455,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3903 => [//Tree Dazzler
			        'offerId' => 4645,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 6456,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3904 => [//Led Hose
			        'offerId' => 4646,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 6457,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3905 => [//Led Hose
			        'offerId' => 4647,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 6458,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3906 => [//Led Hose
			        'offerId' => 4648,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 6459,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3907 => [//Led Hose
			        'offerId' => 4649,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 6460,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3908 => [//Led Hose
			        'offerId' => 4650,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 6461,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3909 => [//Led Hose
			        'offerId' => 4651,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 6462,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3910 => [//Led Hose
			        'offerId' => 4652,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 6463,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3911 => [//Bushnell Binoculars
			        'offerId' => 4653,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 6464,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3912 => [//Bushnell Binoculars
			        'offerId' => 4654,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 6465,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3913 => [//Bushnell Binoculars
			        'offerId' => 4655,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 6466,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3914 => [//Bushnell Binoculars
			        'offerId' => 4656,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 6467,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3915 => [//Bushnell Binoculars
			        'offerId' => 4657,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 6468,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3916 => [//Bushnell Binoculars
			        'offerId' => 4658,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 6469,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3917 => [//Bushnell Binoculars
			        'offerId' => 4659,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 6470,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3918 => [ //Talking Parrot
			        'offerId' => 4660,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 6471,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3919 => [//Talking Parrot
			        'offerId' => 4661,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 6472,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3920 => [//Talking Parrot
			        'offerId' => 4662,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 6473,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3921 => [//Talking Parrot
			        'offerId' => 4663,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 6474,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3922 => [//Talking Parrot
			        'offerId' => 4664,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 6475,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3923 => [//Talking Parrot
			        'offerId' => 4665,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 6476,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3894 => [//Magic Pad - PL
			        'offerId' => 4605,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 6402,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3871 => [ //Led Solar Lamp 180W
			        'offerId' => 4598,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 6380,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3872 => [//Led Solar Lamp 180W
			        'offerId' => 4599,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 6381,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3874 => [//Led Solar Lamp 180W
			        'offerId' => 4600,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 6382,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3873 => [//Led Solar Lamp 180W
			        'offerId' => 4601,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 6383,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3875 => [//Led Solar Lamp 180W
			        'offerId' => 4602,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 6384,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],


			3720 => [ //Sup Game Box 
			        'offerId' => 4261,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 5900,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3721 => [//Sup Game Box 
			        'offerId' => 4267,
			        'country' => 'BG',
			        'currency' => 'RON',
			        'payoutId' => 5906,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3722 => [//Sup Game Box 
			        'offerId' => 4268,
			        'country' => 'RO',
			        'currency' => 'BGN',
			        'payoutId' => 5907,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3723 => [//Sup Game Box 
			        'offerId' => 4269,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 5908,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3724 => [//Sup Game Box 
			        'offerId' => 4270,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 5909,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3725 => [//Sup Game Box 
			        'offerId' => 4271,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 5910,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3726 => [//Sup Game Box 
			        'offerId' => 4272,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 5911,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3737 => [ //Facial Massager
		        'offerId' => 4246,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 5870,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
			3738 => [//Facial Massager
			        'offerId' => 4247,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 5871,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3739 => [//Facial Massager
			        'offerId' => 4248,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 5872,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3740 => [//Facial Massager
			        'offerId' => 4249,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 5873,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3741 => [//Facial Massager
			        'offerId' => 4250,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 5874,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],//Facial Massager
			3742 => [
			        'offerId' => 4251,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 5875,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3736 => [//Steam Brush - PL
			        'offerId' => 4245,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 5869,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3719 => [//Instagram Selfie Light - PL
		        'offerId' => 4209,
		        'country' => 'PL',
		        'currency' => 'PLN',
		        'payoutId' => 5783,
		        'configs' => [
		            'brakeLogFolder'        => true,
	        	],
			],
			3718 => [//Instagram Selfie Light
			        'offerId' => 4239,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 5847,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3717 => [//Instagram Selfie Light
			        'offerId' => 4240,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 5848,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3716 => [//Instagram Selfie Light
			        'offerId' => 4241,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 5849,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3715 => [//Instagram Selfie Light
			        'offerId' => 4242,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 5850,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3714 => [ //Instagram Selfie Light
			        'offerId' => 4243,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 5851,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3623 => [
			        'offerId' => 4165,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 5667,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3622 => [
			        'offerId' => 4166,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 5668,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3621 => [
			        'offerId' => 4167,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 5669,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3620 => [
			        'offerId' => 4170,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 5672,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3619 => [
			        'offerId' => 4171,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 5673,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3618 => [
			        'offerId' => 4172,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 5674,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3511 => [
			        'offerId' => 4107,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 5508,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3510 => [
			        'offerId' => 4108,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 5509,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3509 => [
			        'offerId' => 4109,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 5510,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3508 => [
			        'offerId' => 4110,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 5511,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3507 => [
			        'offerId' => 4111,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 5512,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3506 => [
			        'offerId' => 4112,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 5513,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3523 => [
			        'offerId' => 4101,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 5501,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3522 => [
			        'offerId' => 4102,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 5502,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3521 => [
			        'offerId' => 4103,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 5503,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3520 => [
			        'offerId' => 4104,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 5504,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3519 => [
			        'offerId' => 4105,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 5505,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3518 => [
			        'offerId' => 4106,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 5506,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3516 => [ // Earwax - SI
				'offerId' => 4096,
				'country' => 'SI',
				'currency' => 'EUR',
				'payoutId' => 5496,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3515 => [ // Earwax - HR
				'offerId' => 4099,
				'country' => 'HR',
				'currency' => 'EUR',
				'payoutId' => 5499,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3517 => [ // Earwax - PL
				'offerId' => 4100,
				'country' => 'PL',
				'currency' => 'PLN',
				'payoutId' => 5500,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3513 => [ // Earwax - RO
				'offerId' => 4098,
				'country' => 'RO',
				'currency' => 'RON',
				'payoutId' => 5498,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3512 => [ // Earwax - BG
				'offerId' => 4097,
				'country' => 'BG',
				'currency' => 'BGN',
				'payoutId' => 5497,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3514 => [ // Earwax - GR
				'offerId' => 4095,
				'country' => 'GR',
				'currency' => 'EUR',
				'payoutId' => 5495,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3531 => [ // Professional Steam Straightener - PL
				'offerId' => 4061,
				'country' => 'PL',
				'currency' => 'PLN',
				'payoutId' => 5428,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3532 => [ // Professional Steam Straightener - SI
				'offerId' => 4062,
				'country' => 'SI',
				'currency' => 'EUR',
				'payoutId' => 5429,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3533 => [ // Professional Steam Straightener - HR
				'offerId' => 4063,
				'country' => 'HR',
				'currency' => 'EUR',
				'payoutId' => 5430,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3534 => [ // Professional Steam Straightener - GR
				'offerId' => 4064,
				'country' => 'GR',
				'currency' => 'EUR',
				'payoutId' => 5431,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3535 => [ // Professional Steam Straightener - RO
				'offerId' => 4065,
				'country' => 'RO',
				'currency' => 'RON',
				'payoutId' => 5432,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3536 => [ // Professional Steam Straightener - BG
				'offerId' => 4066,
				'country' => 'BG',
				'currency' => 'BGN',
				'payoutId' => 5433,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3537 => [ // Professional Steam Straightener - HU
				'offerId' => 38657,
				'country' => 'HU',
				'currency' => 'HUF',
				'payoutId' => 44433,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2740 => [ // Laser Lamp - BG
				'offerId' => 4026,
				'country' => 'BG',
				'currency' => 'BGN',
				'payoutId' => 5383,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2739 => [ // Laser Lamp - PL
				'offerId' => 4025,
				'country' => 'PL',
				'currency' => 'PLN',
				'payoutId' => 5382,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2738 => [ // Laser Light - HU
				'offerId' => 37920,
				'country' => 'HU',
				'currency' => 'HUF',
				'payoutId' => 43659,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2737 => [ // Laser Lamp - RO
				'offerId' => 4024,
				'country' => 'RO',
				'currency' => 'RON',
				'payoutId' => 5381,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2736 => [ // Laser Lamp - HR
				'offerId' => 4023,
				'country' => 'HR',
				'currency' => 'EUR',
				'payoutId' => 5380,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2735 => [ // Laser Lamp - GR
				'offerId' => 4022,
				'country' => 'GR',
				'currency' => 'EUR',
				'payoutId' => 5379,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2734 => [ // Laser Lamp - SI
				'offerId' => 4021,
				'country' => 'SI',
				'currency' => 'EUR',
				'payoutId' => 5378,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2750 => [ // Magic Tracks - BG
				'offerId' => 4015,
				'country' => 'BG',
				'currency' => 'BGN',
				'payoutId' => 5372,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2749 => [ // Magic Tracks - PL
				'offerId' => 4016,
				'country' => 'PL',
				'currency' => 'PLN',
				'payoutId' => 5373,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2748 => [ // Magic Tracks - HU
				'offerId' => 38414,
				'country' => 'HU',
				'currency' => 'HUF',
				'payoutId' => 44181,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2747 => [ // Magic Tracks - RO
				'offerId' => 4017,
				'country' => 'RO',
				'currency' => 'RON',
				'payoutId' => 5374,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2746 => [ // Magic Tracks - HR
				'offerId' => 4018,
				'country' => 'HR',
				'currency' => 'EUR',
				'payoutId' => 5375,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2745 => [ // Magic Tracks - GR
				'offerId' => 4019,
				'country' => 'GR',
				'currency' => 'EUR',
				'payoutId' => 5376,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2744 => [ // Magic Tracks - SI
				'offerId' => 4020,
				'country' => 'SI',
				'currency' => 'EUR',
				'payoutId' => 5377,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3407 => [ // HD powerful binocular - RO
				'offerId' => 3910,
				'country' => 'RO',
				'currency' => 'RON',
				'payoutId' => 5244,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3408 => [ // HD powerful binocular - BG
				'offerId' => 3911,
				'country' => 'BG',
				'currency' => 'BGN',
				'payoutId' => 5245,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2726 => [ //Sonic Pic - SI
				'offerId' => 3884,
				'country' => 'SI',
				'currency' => 'EUR',
				'payoutId' => 5203,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2220 => [
				'offerId' => 84,
				'country' => 'BG',
				'currency' => 'BGN',
				'payoutId' => 143,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2226 => [
				'offerId' => 86,
				'country' => 'ES',
				'currency' => 'EUR',
				'payoutId' => 149,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2227 => [
				'offerId' => 87,
				'country' => 'GR',
				'currency' => 'EUR',
				'payoutId' => 156,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2229 => [
				'offerId' => 89,
				'country' => 'BG',
				'currency' => 'BGN',
				'payoutId' => 158,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2230 => [
				'offerId' => 94,
				'country' => 'BG',
				'currency' => 'BGN',
				'payoutId' => 163,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2231 => [
				'offerId' => 99,
				'country' => 'BG',
				'currency' => 'BGN',
				'payoutId' => 168,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2232 => [
				'offerId' => 101,
				'country' => 'ES',
				'currency' => 'EUR',
				'payoutId' => 170,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2233 => [
				'offerId' => 104,
				'country' => 'BG',
				'currency' => 'BGN',
				'payoutId' => 173,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2235 => [
				'offerId' => 125,
				'country' => 'BG',
				'currency' => 'BGN',
				'payoutId' => 194,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2234 => [
				'offerId' => 120,
				'country' => 'BG',
				'currency' => 'BGN',
				'payoutId' => 189,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2252 => [
				'offerId' => 37918,
				'country' => 'GR',
				'currency' => 'EUR',
				'payoutId' => 43657,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2261 => [
				'offerId' => 102,
				'country' => 'HR',
				'currency' => 'EUR',
				'payoutId' => 171,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2267 => [
				'offerId' => 109,
				'country' => 'BG',
				'currency' => 'BGN',
				'payoutId' => 178,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2248 => [
				'offerId' => 113,
				'country' => 'HR',
				'currency' => 'EUR',
				'payoutId' => 182,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2243 => [
				'offerId' => 112,
				'country' => 'GR',
				'currency' => 'EUR',
				'payoutId' => 181,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2269 => [
				'offerId' => 111,
				'country' => 'HR',
				'currency' => 'EUR',
				'payoutId' => 180,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2268 => [
				'offerId' => 110,
				'country' => 'ES',
				'currency' => 'EUR',
				'payoutId' => 179,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2249 => [
				'offerId' => 173,
				'country' => 'SI',
				'currency' => 'EUR',
				'payoutId' => 285,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2244 => [
				'offerId' => 170,
				'country' => 'HU',
				'currency' => 'HUF',
				'payoutId' => 282,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2272 => [
				'offerId' => 130,
				'country' => 'BG',
				'currency' => 'BGN',
				'payoutId' => 199,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2273 => [
				'offerId' => 155,
				'country' => 'BG',
				'currency' => 'BGN',
				'payoutId' => 266,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2274 => [
				'offerId' => 156,
				'country' => 'ES',
				'currency' => 'EUR',
				'payoutId' => 267,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2275 => [
				'offerId' => 161,
				'country' => 'BG',
				'currency' => 'BGN',
				'payoutId' => 272,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2276 => [
				'offerId' => 157,
				'country' => 'GR',
				'currency' => 'EUR',
				'payoutId' => 268,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2277 => [
				'offerId' => 158,
				'country' => 'HR',
				'currency' => 'EUR',
				'payoutId' => 269,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2278 => [
				'offerId' => 133,
				'country' => 'HR',
				'currency' => 'EUR',
				'payoutId' => 202,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2257 => [
				'offerId' => 166,
				'country' => 'IT',
				'currency' => 'EUR',
				'payoutId' => 278,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2286 => [
				'offerId' => 160,
				'country' => 'IT',
				'currency' => 'EUR',
				'payoutId' => 271,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2279 => [
				'offerId' => 103,
				'country' => 'RO',
				'currency' => 'RON',
				'payoutId' => 172,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2256 => [
				'offerId' => 98,
				'country' => 'RO',
				'currency' => 'RON',
				'payoutId' => 167,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2251 => [
				'offerId' => 164,
				'country' => 'RO',
				'currency' => 'RON',
				'payoutId' => 276,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2250 => [
				'offerId' => 163,
				'country' => 'IT',
				'currency' => 'EUR',
				'payoutId' => 274,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2281 => [
		        'offerId' => 38666,
		        'country' => 'HR',
		        'currency' => 'EUR',
		        'payoutId' => 44472,
		        'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2262 => [
		        'offerId' => 172,
		        'country' => 'SI',
		        'currency' => 'EUR',
		        'payoutId' => 284,
		        'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2288 => [
		        'offerId' => 171,
		        'country' => 'SI',
		        'currency' => 'EUR',
		        'payoutId' => 283,
		        'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3107 => [
		        'offerId' => 38669,
		        'country' => 'SK',
		        'currency' => 'EUR',
		        'payoutId' => 44475,
		        'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2282 => [
		        'offerId' => 168,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 280,
		        'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2255 => [
		        'offerId' => 37917,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 43656,
		        'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2266 => [
		        'offerId' => 108,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 177,
		        'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2265 => [
		        'offerId' => 167,
		        'country' => 'IT',
		        'currency' => 'EUR',
		        'payoutId' => 279,
		        'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2298 => [
		        'offerId' => 174,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 294,
		        'configs' => [
					'brakeLogFolder'	=> true,
				],

			],
			2289 => [
			    'offerId' => 176,
			    'country' => 'SI',
			    'currency' => 'EUR',
			    'payoutId' => 298,
			    'configs' => [
			        'brakeLogFolder'        => true,
			    ],
			],
			2290 => [
			    'offerId' => 177,
			    'country' => 'SI',
			    'currency' => 'EUR',
			    'payoutId' => 299,
			    'configs' => [
			        'brakeLogFolder'        => true,
			    ],
			],
			2291 => [
			    'offerId' => 178,
			    'country' => 'SI',
			    'currency' => 'EUR',
			    'payoutId' => 300,
			    'configs' => [
			        'brakeLogFolder'        => true,
			    ],
			],
			2292 => [
			    'offerId' => 179,
			    'country' => 'SI',
			    'currency' => 'EUR',
			    'payoutId' => 301,
			    'configs' => [
			        'brakeLogFolder'        => true,
			    ],
			],
			2293 => [
			    'offerId' => 117,
			    'country' => 'ES',
			    'currency' => 'EUR',
			    'payoutId' => 186,
			    'configs' => [
			        'brakeLogFolder'        => true,
			    ],
			],
			2294 => [
			    'offerId' => 115,
			    'country' => 'BG',
			    'currency' => 'BGN',
			    'payoutId' => 184,
			    'configs' => [
			        'brakeLogFolder'        => true,
			    ],
			],
			2295 => [
			    'offerId' => 116,
			    'country' => 'GR',
			    'currency' => 'EUR',
			    'payoutId' => 185,
			    'configs' => [
			        'brakeLogFolder'        => true,
			    ],
			],
			2303 => [
			    'offerId' => 180,
			    'country' => 'BG',
			    'currency' => 'BGN',
			    'payoutId' => 302,
			    'configs' => [
			        'brakeLogFolder'        => true,
			    ],
			],
			2304 => [
			    'offerId' => 181,
			    'country' => 'BG',
			    'currency' => 'BGN',
			    'payoutId' => 303,
			    'configs' => [
			        'brakeLogFolder'        => true,
			    ],
			],
			2296 => [
			    'offerId' => 132,
			    'country' => 'ES',
			    'currency' => 'EUR',
			    'payoutId' => 201,
			    'configs' => [
			        'brakeLogFolder'        => true,
			    ],
			],
			2301 => [
			    'offerId' => 182,
			    'country' => 'GR',
			    'currency' => 'EUR',
			    'payoutId' => 304,
			    'configs' => [
			        'brakeLogFolder'        => true,
			    ],
			],
			2300 => [
			    'offerId' => 183,
			    'country' => 'HR',
			    'currency' => 'EUR',
			    'payoutId' => 305,
			    'configs' => [
			        'brakeLogFolder'        => true,
			    ],
			],
			2299 => [
			    'offerId' => 184,
			    'country' => 'BG',
			    'currency' => 'BGN',
			    'payoutId' => 306,
			    'configs' => [
			        'brakeLogFolder'        => true,
			    ],
			],
			2305 => [
			    'offerId' => 175,
			    'country' => 'BG',
			    'currency' => 'BGN',
			    'payoutId' => 297,
			    'configs' => [
			        'brakeLogFolder'        => true,
			    ],
			],
			2246 => [
			    'offerId' => 185,
			    'country' => 'PL',
			    'currency' => 'PLN',
			    'payoutId' => 307,
			    'configs' => [
			        'brakeLogFolder'        => true,
			    ],
			],
			2306 => [
			    'offerId' => 186,
			    'country' => 'PL',
			    'currency' => 'PLN',
			    'payoutId' => 308,
			    'configs' => [
			        'brakeLogFolder'        => true,
			    ],
			],
			2307 => [
			    'offerId' => 187,
			    'country' => 'PL',
			    'currency' => 'PLN',
			    'payoutId' => 309,
			    'configs' => [
			        'brakeLogFolder'        => true,
			    ],
			],
			2308 => [
			    'offerId' => 188,
			    'country' => 'PL',
			    'currency' => 'PLN',
			    'payoutId' => 310,
			    'configs' => [
			        'brakeLogFolder'        => true,
			    ],
			],
			2253 => [
			    'offerId' => 189,
			    'country' => 'PL',
			    'currency' => 'PLN',
			    'payoutId' => 311,
			    'configs' => [
			        'brakeLogFolder'        => true,
			    ],
			],
			2283 => [
			    'offerId' => 190,
			    'country' => 'PL',
			    'currency' => 'PLN',
			    'payoutId' => 312,
			    'configs' => [
			        'brakeLogFolder'        => true,
			    ],
			],
			2309 => [
		        'offerId' => 192,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 316,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
			2310 => [
		        'offerId' => 193,
		        'country' => 'IT',
		        'currency' => 'EUR',
		        'payoutId' => 317,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
			2312 => [
		        'offerId' => 126,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 195,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
				2313 => [
		        'offerId' => 195,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 320,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
				2314 => [
		        'offerId' => 196,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 321,
		        'configs' => [
		            'brakeLogFolder'        => true,
	       		 ],
			],
				2284 => [
		        'offerId' => 38667,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 44473,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
				2285 => [
		        'offerId' => 38668,
		        'country' => 'CZ',
		        'currency' => 'CZK',
		        'payoutId' => 44474,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
				2316 => [
		        'offerId' => 197,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 322,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
				2317 => [
		        'offerId' => 124,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 193,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
				2337 => [
		        'offerId' => 202,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 381,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
					2342 => [
		        'offerId' => 204,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 386,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
					2341 => [
		        'offerId' => 203,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 385,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
					2326 => [
		        'offerId' => 205,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 391,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
						2344 => [
		        'offerId' => 206,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 396,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
						2345 => [
		        'offerId' => 207,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 397,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
						2346 => [
		        'offerId' => 208,
		        'country' => 'HR',
		        'currency' => 'EUR',
		        'payoutId' => 398,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
						2347 => [
		        'offerId' => 209,
		        'country' => 'SI',
		        'currency' => 'EUR',
		        'payoutId' => 399,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
						2348 => [
		        'offerId' => 210,
		        'country' => 'Pl',
		        'currency' => 'PLN',
		        'payoutId' => 400,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
						2349 => [
		        'offerId' => 211,
		        'country' => 'ES',
		        'currency' => 'EUR',
		        'payoutId' => 401,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
						2350 => [
		        'offerId' => 212,
		        'country' => 'IT',
		        'currency' => 'EUR',
		        'payoutId' => 402,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
						2351 => [
		        'offerId' => 213,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 403,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
			2324 => [
		        'offerId' => 38660,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 44450,
		        'configs' => [
		            'brakeLogFolder'        => true,
	     	   ],
			],
						2325 => [
		        'offerId' => 215,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 405,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
						2327 => [
		        'offerId' => 216,
		        'country' => 'ES',
		        'currency' => 'EUR',
		        'payoutId' => 406,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
						2328 => [
		        'offerId' => 217,
		        'country' => 'HR',
		        'currency' => 'EUR',
		        'payoutId' => 407,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
					2352 => [
		        'offerId' => 91,
		        'country' => 'ES',
		        'currency' => 'EUR',
		        'payoutId' => 160,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
					2353 => [
		        'offerId' => 90,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 159,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
					2354 => [
		        'offerId' => 92,
		        'country' => 'HR',
		        'currency' => 'EUR',
		        'payoutId' => 161,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
					2355 => [
		        'offerId' => 218,
		        'country' => 'IT',
		        'currency' => 'EUR',
		        'payoutId' => 408,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
					2356 => [
		        'offerId' => 219,
		        'country' => 'PL',
		        'currency' => 'PLN',
		        'payoutId' => 409,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
					2357 => [
		        'offerId' => 88,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 157,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
			2358 => [
		        'offerId' => 220,
		        'country' => 'HU',
		        'currency' => 'HUF',
		        'payoutId' => 410,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
			2359 => [
		        'offerId' => 221,
		        'country' => 'SI',
		        'currency' => 'EUR',
		        'payoutId' => 411,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],

			2362 => [
			        'offerId' => 225,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 416,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2363 => [
			        'offerId' => 326,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 591,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2364 => [
			        'offerId' => 223,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 414,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2361 => [
			        'offerId' => 222,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 413,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2332 => [
			        'offerId' => 128,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 197,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2331 => [
			        'offerId' => 106,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 175,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2323 => [
			        'offerId' => 226,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 421,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2367 => [
			        'offerId' => 228,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 423,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2365 => [
			        'offerId' => 227,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 422,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2366 => [
			        'offerId' => 229,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 424,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2369 => [
			        'offerId' => 230,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 425,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2370 => [
			        'offerId' => 231,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 426,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2373 => [
			        'offerId' => 232,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 427,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2368 => [
			        'offerId' => 233,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 428,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2374 => [
			        'offerId' => 234,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 429,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2371 => [
			        'offerId' => 236,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 431,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2372 => [
			        'offerId' => 235,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 430,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],

			2379 => [
			        'offerId' => 239,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 440,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2375 => [
			        'offerId' => 237,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 437,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],

			2378 => [
			        'offerId' => 242,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 444,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2377 => [
			        'offerId' => 241,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 443,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2376 => [
			        'offerId' => 240,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 442,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2383 => [
			        'offerId' => 249,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 464,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2384 => [
			        'offerId' => 248,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 463,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2385 => [
			        'offerId' => 247,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 462,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2386 => [
			        'offerId' => 246,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 461,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2387 => [
			        'offerId' => 245,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 460,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2380 => [
			        'offerId' => 244,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 459,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2402 => [
			        'offerId' => 251,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 475,
			        'configs' => [
			            'brakeLogFolder'        => true,
       				 ],
			],
			2399 => [
			        'offerId' => 252,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 476,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2398 => [
			        'offerId' => 253,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 477,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2397 => [
			        'offerId' => 254,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 478,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2396 => [
			        'offerId' => 255,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 479,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2395 => [
			        'offerId' => 256,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 480,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2406 => [
			        'offerId' => 257,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 481,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2405 => [
			        'offerId' => 258,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 482,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2404 => [
			        'offerId' => 259,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 483,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2403 => [
			        'offerId' => 260,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 484,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2411 => [
			        'offerId' => 261,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 485,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2412 => [
			        'offerId' => 262,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 486,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2413 => [
			        'offerId' => 263,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 487,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2407 => [
			        'offerId' => 264,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 490,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2408 => [
			        'offerId' => 265,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 491,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2409 => [
			        'offerId' => 266,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 492,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2410 => [
			        'offerId' => 267,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 493,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2415 => [
			        'offerId' => 268,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 494,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2414 => [
			        'offerId' => 269,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 495,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2416 => [
			        'offerId' => 270,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 496,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2437 => [
			        'offerId' => 271,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 504,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2419 => [
			        'offerId' => 273,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 506,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2424 => [
			        'offerId' => 274,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 507,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2425 => [
			        'offerId' => 275,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 508,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2427 => [
			        'offerId' => 276,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 509,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2453 => [
			        'offerId' => 277,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 510,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2434 => [
			        'offerId' => 272,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 505,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2435 => [
			        'offerId' => 278,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 513,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2426 => [
			        'offerId' => 279,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 514,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2428 => [
			        'offerId' => 280,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 515,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2429 => [
			        'offerId' => 281,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 516,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2430 => [
			        'offerId' => 282,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 517,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2431 => [
			        'offerId' => 283,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 518,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2432 => [
			        'offerId' => 284,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 519,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2472 => [
			        'offerId' => 285,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 529,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2473 => [
			        'offerId' => 286,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 530,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2474 => [
		        'offerId' => 287,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 531,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
			2476 => [
			        'offerId' => 288,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 532,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2477 => [
			        'offerId' => 289,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 533,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2478 => [
			        'offerId' => 290,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 534,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2388 => [
			        'offerId' => 40011,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 45915,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2465 => [
			        'offerId' => 292,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 536,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2466 => [
			        'offerId' => 293,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 537,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2467 => [
			        'offerId' => 294,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 538,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2468 => [
			        'offerId' => 295,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 539,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2469 => [
			        'offerId' => 296,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 540,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2470 => [
			        'offerId' => 297,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 541,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2471 => [
			        'offerId' => 298,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 542,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2489 => [
		        'offerId' => 299,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 551,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
			2490 => [
		        'offerId' => 300,
		        'country' => 'GR',
		        'currency' => 'EUR',
		        'payoutId' => 552,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
			2488 => [
		        'offerId' => 301,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 553,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
			2491 => [
		        'offerId' => 302,
		        'country' => 'HR',
		        'currency' => 'EUR',
		        'payoutId' => 554,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
			2485 => [
			        'offerId' => 129,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 198,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2484 => [
			        'offerId' => 131,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 200,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2483 => [
			        'offerId' => 303,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 555,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2486 => [
			        'offerId' => 304,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 556,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2487 => [
			        'offerId' => 305,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 557,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2496 => [
			        'offerId' => 306,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 561,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2495 => [
			        'offerId' => 307,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 562,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2518 => [
			        'offerId' => 308,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 563,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2495 => [
			        'offerId' => 307,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 562,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2519 => [
			        'offerId' => 310,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 569,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2520 => [
			        'offerId' => 311,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 570,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2521 => [
		        'offerId' => 312,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 571,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
			2522 => [
		        'offerId' => 313,
		        'country' => 'IT',
		        'currency' => 'EUR',
		        'payoutId' => 572,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
			2523 => [
		        'offerId' => 314,
		        'country' => 'ES',
		        'currency' => 'EUR',
		        'payoutId' => 575,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
			2500 => [
			        'offerId' => 315,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 576,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2498 => [
			        'offerId' => 316,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 577,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2499 => [
			        'offerId' => 317,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 578,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2525 => [
			        'offerId' => 318,
			        'country' => 'PT',
			        'currency' => 'EUR',
			        'payoutId' => 578,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2494 => [
		        'offerId' => 319,
		        'country' => 'BG',
		        'currency' => 'BGN',
		        'payoutId' => 580,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
		],
			2090 => [
		        'offerId' => 320,
		        'country' => 'RO',
		        'currency' => 'RON',
		        'payoutId' => 581,
		        'configs' => [
		            'brakeLogFolder'        => true,
		        ],
			],
			2287 => [
			        'offerId' => 321,
			        'country' => 'PT',
			        'currency' => 'EUR',
			        'payoutId' => 582,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2254 => [
			        'offerId' => 322,
			        'country' => 'PT',
			        'currency' => 'EUR',
			        'payoutId' => 583,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2245 => [
			        'offerId' => 323,
			        'country' => 'PT',
			        'currency' => 'EUR',
			        'payoutId' => 584,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2526 => [
			        'offerId' => 324,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 587,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2527 => [
			        'offerId' => 325,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 588,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2517 => [
			        'offerId' => 309,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 568,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2528 => [
			        'offerId' => 327,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 597,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2529 => [
			        'offerId' => 328,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 598,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2531 => [
			        'offerId' => 329,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 599,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2532 => [
			        'offerId' => 330,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 600,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2533 => [
			        'offerId' => 331,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 605,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2534 => [
			        'offerId' => 332,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 606,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2535 => [
			        'offerId' => 333,
			        'country' => 'PT',
			        'currency' => 'EUR',
			        'payoutId' => 607,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2536 => [
			        'offerId' => 334,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 608,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2537 => [
			        'offerId' => 335,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 609,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2538 => [
			        'offerId' => 336,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 610,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2539 => [
			        'offerId' => 337,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 611,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2540 => [
			        'offerId' => 338,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 612,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2541 => [
			        'offerId' => 339,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 613,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2542 => [
			        'offerId' => 340,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 614,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2543 => [
			        'offerId' => 341,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 617,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2544 => [
			        'offerId' => 342,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 618,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2545 => [
			        'offerId' => 343,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 619,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2546 => [
			        'offerId' => 344,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 620,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2549 => [
			        'offerId' => 345,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 650,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2550 => [
			        'offerId' => 346,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 651,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2551 => [
			        'offerId' => 347,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 652,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2552 => [
			        'offerId' => 348,
			        'country' => 'PT',
			        'currency' => 'EUR',
			        'payoutId' => 653,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2553 => [
			        'offerId' => 349,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 654,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2554 => [
			        'offerId' => 350,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 655,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2555 => [
			        'offerId' => 351,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 656,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2556 => [
			        'offerId' => 352,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 657,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2557 => [
			        'offerId' => 353,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 658,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2558 => [
			        'offerId' => 354,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 659,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2559 => [
			        'offerId' => 355,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 660,
			        'configs' => [
			            'brakeLogFolder'        => true,
			   		 ],
			],
			2563 => [
			        'offerId' => 356,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 677,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2564 => [
			        'offerId' => 357,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 678,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2565 => [
			        'offerId' => 358,
			        'country' => 'PT',
			        'currency' => 'EUR',
			        'payoutId' => 679,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2566 => [
			        'offerId' => 359,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 680,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2567 => [
			        'offerId' => 360,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 681,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2568 => [
			        'offerId' => 361,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 682,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2569 => [
			        'offerId' => 362,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 683,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2570 => [
			        'offerId' => 363,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 684,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2580 => [
			        'offerId' => 364,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 734,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2581 => [
			        'offerId' => 365,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 735,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2582 => [
			        'offerId' => 366,
			        'country' => 'PT',
			        'currency' => 'EUR',
			        'payoutId' => 736,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2583 => [
			        'offerId' => 367,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 737,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2584 => [
			        'offerId' => 368,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 738,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2585 => [
			        'offerId' => 369,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 739,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2586 => [
			        'offerId' => 370,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 740,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2587 => [
			        'offerId' => 371,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 741,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2588 => [
			        'offerId' => 372,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 742,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2589 => [
			        'offerId' => 373,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 743,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2599 => [
			        'offerId' => 374,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 744,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2562 => [
			        'offerId' => 396,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 747,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
						2603 => [
			        'offerId' => 398,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 789,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2604 => [
			        'offerId' => 399,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 790,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2605 => [
			        'offerId' => 400,
			        'country' => 'PT',
			        'currency' => 'EUR',
			        'payoutId' => 791,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2606 => [
			        'offerId' => 401,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 792,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2607 => [
			        'offerId' => 402,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 793,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2608 => [
			        'offerId' => 403,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 794,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2601 => [
			        'offerId' => 404,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 795,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2602 => [
			        'offerId' => 405,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 796,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2609 => [
			        'offerId' => 406,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 797,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2610 => [
			        'offerId' => 407,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 798,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2638 => [
			        'offerId' => 408,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 799,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2611 => [
			        'offerId' => 409,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 827,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2612 => [
			        'offerId' => 42625,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 48795,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2613 => [
			        'offerId' => 411,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 829,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2641 => [
			        'offerId' => 412,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 830,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2642 => [
			        'offerId' => 413,
			        'country' => 'PT',
			        'currency' => 'EUR',
			        'payoutId' => 831,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2643 => [
			        'offerId' => 414,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 832,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2644 => [
			        'offerId' => 42624,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 48794,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2645 => [
			        'offerId' => 416,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 834,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2646 => [
			        'offerId' => 417,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 835,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2647 => [
			        'offerId' => 418,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 836,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2633 => [
			        'offerId' => 419,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 838,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2634 => [
			        'offerId' => 420,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 839,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2635 => [
			        'offerId' => 421,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 840,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2636 => [
			        'offerId' => 422,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 841,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2637 => [
			        'offerId' => 423,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 842,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2650 => [
			        'offerId' => 424,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 843,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2652 => [
			        'offerId' => 425,
			        'country' => 'PT',
			        'currency' => 'EUR',
			        'payoutId' => 844,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2653 => [
			        'offerId' => 426,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 845,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2657 => [
			        'offerId' => 427,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 846,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2658 => [
			        'offerId' => 428,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 847,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2651 => [
			        'offerId' => 96,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 165,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2654 => [
			        'offerId' => 95,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 164,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2655 => [
			        'offerId' => 97,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 166,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2656 => [
			        'offerId' => 93,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 162,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2662 => [
			        'offerId' => 639,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 1212,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2663 => [
			        'offerId' => 640,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 1213,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2664 => [
			        'offerId' => 641,
			        'country' => 'PT',
			        'currency' => 'EUR',
			        'payoutId' => 1214,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2665 => [
			        'offerId' => 642,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 1215,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2666 => [
			        'offerId' => 643,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 1216,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2667 => [
			        'offerId' => 644,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 1217,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2668 => [
			        'offerId' => 645,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 1218,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2669 => [
			        'offerId' => 646,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 1219,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2670 => [
			        'offerId' => 647,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 1220,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2594 => [
			        'offerId' => 821,
			        'country' => 'PT',
			        'currency' => 'EUR',
			        'payoutId' => 1613,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2595 => [
			        'offerId' => 822,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 1616,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2596 => [
			        'offerId' => 823,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 1617,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2597 => [
			        'offerId' => 824,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 1618,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
				2696 => [
			        'offerId' => 834,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 1662,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2697 => [
			        'offerId' => 835,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 1663,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2698 => [
			        'offerId' => 836,
			        'country' => 'PT',
			        'currency' => 'EUR',
			        'payoutId' => 1666,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2699 => [
			        'offerId' => 837,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 1667,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2700 => [
			        'offerId' => 838,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 1668,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2701 => [
			        'offerId' => 839,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 1669,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2702 => [
			        'offerId' => 840,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 1670,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2703 => [
			        'offerId' => 841,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 1671,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2704 => [
			        'offerId' => 842,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 1672,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2705 => [
			        'offerId' => 843,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 1673,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2685 => [
			        'offerId' => 844,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 1674,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2686 => [
			        'offerId' => 845,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 1675,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2687 => [
			        'offerId' => 846,
			        'country' => 'PT',
			        'currency' => 'EUR',
			        'payoutId' => 1676,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2688 => [
			        'offerId' => 847,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 1677,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2689 => [
			        'offerId' => 848,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 1678,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2690 => [
			        'offerId' => 849,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 1679,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2691 => [
			        'offerId' => 850,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 1680,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2692 => [
			        'offerId' => 851,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 1681,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2693 => [
			        'offerId' => 852,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 1682,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2694 => [
			        'offerId' => 853,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 1683,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2706 => [
			        'offerId' => 1020,
			        'country' => 'IT',
			        'currency' => 'EUR',
			        'payoutId' => 1850,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2707 => [
			        'offerId' => 1021,
			        'country' => 'ES',
			        'currency' => 'EUR',
			        'payoutId' => 1851,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2708 => [
			        'offerId' => 1022,
			        'country' => 'PT',
			        'currency' => 'EUR',
			        'payoutId' => 1852,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2709 => [
			        'offerId' => 1023,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 1853,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2710 => [
			        'offerId' => 1024,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 1854,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2711 => [
			        'offerId' => 1025,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 1855,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2712 => [
			        'offerId' => 1026,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 1856,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2713 => [
			        'offerId' => 1027,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 1857,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			2714 => [
			        'offerId' => 1028,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 1858,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3388 => [ // Shiatsu massager - GR
			        'offerId' => 3765,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 5080,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3389 => [ // Shiatsu massager - RO
			        'offerId' => 3764,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 5079,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			3390 => [ // Shiatsu massager - BG
			        'offerId' => 3766,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 5081,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4111 => [ // In Styler 3 in 1
			        'offerId' => 8307,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 10892,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4112 => [ // In Styler 3 in 1
			        'offerId' => 8308,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 10893,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4113 => [ // In Styler 3 in 1
			        'offerId' => 8309,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 10894,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4114 => [ // In Styler 3 in 1
			        'offerId' => 8310,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 10895,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4115 => [ // In Styler 3 in 1
			        'offerId' => 8311,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 10896,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4116 => [ // In Styler 3 in 1
			        'offerId' => 8318,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 10903,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4117 => [ // In Styler 3 in 1
			        'offerId' => 8319,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 10904,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4125 => [ // Fasiz
			        'offerId' => 8324,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 10921,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4126 => [ // Fasiz
			        'offerId' => 8326,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 10923,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4127 => [ // Fasiz
			        'offerId' => 8327,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 10924,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4130 => [ // Mofajang - RO 
			        'offerId' => 8328,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 10925,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4719 => [ // Arctic Air - CZ
			        'offerId' => 11152,
			        'country' => 'CZ',
			        'currency' => 'CZK',
			        'payoutId' => 14075,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4718 => [ // Arctic Air - SK
			        'offerId' => 11153,
			        'country' => 'SK',
			        'currency' => 'EUR',
			        'payoutId' => 14076,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4717 => [ // Arctic Air - HU
			        'offerId' => 11154,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 14077,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4716 => [ // Arctic Air - PL
			        'offerId' => 11155,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 14078,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4715 => [ // Arctic Air - SI
			        'offerId' => 11156,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 14079,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4892 => [
			        'offerId' => 12106,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 15188,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4893 => [
			        'offerId' => 12107,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 15189,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4894 => [
			        'offerId' => 12108,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 15190,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4895 => [
			        'offerId' => 12109,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 15191,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4896 => [
			        'offerId' => 12110,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 15192,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4897 => [
			        'offerId' => 12111,
			        'country' => 'CZ',
			        'currency' => 'CZK',
			        'payoutId' => 15193,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4898 => [
			        'offerId' => 12112,
			        'country' => 'SK',
			        'currency' => 'EUR',
			        'payoutId' => 15194,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4899 => [
			        'offerId' => 12113,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 15195,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4838 => [
			        'offerId' => 12114,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 15208,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4839 => [
			        'offerId' => 12115,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 15211,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4840 => [
			        'offerId' => 12116,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 15212,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4841 => [
			        'offerId' => 12117,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 15213,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4842 => [
			        'offerId' => 12118,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 15214,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4843 => [
			        'offerId' => 12119,
			        'country' => 'CZ',
			        'currency' => 'CZK',
			        'payoutId' => 15217,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4844 => [
			        'offerId' => 12120,
			        'country' => 'SK',
			        'currency' => 'EUR',
			        'payoutId' => 15218,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4845 => [
			        'offerId' => 12121,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 15219,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4905 => [
			        'offerId' => 12122,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 15220,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4868 => [
			        'offerId' => 12123,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 15221,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4869 => [
			        'offerId' => 12124,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 15222,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4870 => [
			        'offerId' => 12125,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 15223,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4871 => [
			        'offerId' => 12126,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 15224,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4872 => [
			        'offerId' => 12127,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 15225,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4873 => [
			        'offerId' => 12128,
			        'country' => 'SK',
			        'currency' => 'EUR',
			        'payoutId' => 15226,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4874 => [
			        'offerId' => 12129,
			        'country' => 'CZ',
			        'currency' => 'CZK',
			        'payoutId' => 15227,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4875 => [
			        'offerId' => 12130,
			        'country' => 'PL',
			        'currency' => 'PLN',
			        'payoutId' => 15228,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4876 => [
			        'offerId' => 12131,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 15229,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4901 => [
			        'offerId' => 12224,
			        'country' => 'BG',
			        'currency' => 'BGN',
			        'payoutId' => 15360,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4902 => [
			        'offerId' => 12225,
			        'country' => 'GR',
			        'currency' => 'EUR',
			        'payoutId' => 15361,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4903 => [
			        'offerId' => 12226,
			        'country' => 'RO',
			        'currency' => 'RON',
			        'payoutId' => 15362,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4904 => [
			        'offerId' => 12227,
			        'country' => 'HU',
			        'currency' => 'HUF',
			        'payoutId' => 15363,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4906 => [
			        'offerId' => 12228,
			        'country' => 'SI',
			        'currency' => 'EUR',
			        'payoutId' => 15364,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4907 => [
			        'offerId' => 12229,
			        'country' => 'HR',
			        'currency' => 'EUR',
			        'payoutId' => 15365,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4908 => [
			        'offerId' => 12230,
			        'country' => 'CZ',
			        'currency' => 'CZK',
			        'payoutId' => 15366,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
			4909 => [
			        'offerId' => 12231,
			        'country' => 'SK',
			        'currency' => 'EUR',
			        'payoutId' => 15367,
			        'configs' => [
			            'brakeLogFolder'        => true,
			        ],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'deleted'			=> '',
				'reclamation'		=> '',
				'unknown_number'	=> '',
				'duplicated'		=> '',
				'problem'			=> '',
				'returned'			=> '',
			],
			'reject'	=> [
				'cancelled'			=> '',
			],
			'expect'	=> [
				'new'				=> '',
				'unconfirmed'		=> '',
				'no_answer'			=> '',
				'waiting'			=> '',

				
			],
			'confirm'	=> [
				'confirmed'			=> '',
				'shipped'			=> '',
				'delivered'			=> '',
				'paid'				=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://dashboard.salestoro.com/api/order',
	],
];

?>