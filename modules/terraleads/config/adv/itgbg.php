<?php

/*
 * https://confluence.adcombo.com/display/DOCS/Outgoing+orders+REST+API#OutgoingordersRESTAPI-Request
 */

return [

    27067 => [
        'terraleadsApiKey'			=> 'af447bc3f48936e41de384319ad7728e',

        'offers' => [
            8828 => [ //
                'clid' => 1996148,
                'product' => 'Keto Delight GR - LP',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-uk.api',
                ],
            ],
            8853 => [ //Coralift - BG (low price)
                'clid' => 6890226,
                'product' => 'Coralift - BG (low price)',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            8854 => [ //Coralift - GR (low price)
                'clid' => 1996148,
                'product' => 'Coralift - GR (low price)',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-uk.api',
                ],
            ],
            8830 => [ //Money Amulet - GR
                'clid' => 1996148,
                'product' => 'AMULET-GR-TR',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-uk.api',
                ],
            ],
            9172 => [ //Money Amulet - BG
                'clid' => 1996148,
                'product' => 'AMULET-BG-TR',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            8574 => [ //Cardio NRJ + - BG (low price)
                'clid' => 6890226,
                'product' => 'Cardio NRJ + BG - LP',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            8004 => [ //Cardio NRJ + - GR
                'clid' => 1996148,
                'product' => 'Cardio NRJ + - GR',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-uk.api',
                ],
            ],
            8003 => [ //Cardio NRJ + - RO
                'clid' => 2485825,
                'product' => 'Cardio NRJ + - RO',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-ro.api',
                ],
            ],
            8002 => [ //Cardio NRJ + - BG
                'clid' => 6890226,
                'product' => 'Cardio NRJ + - BG',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            5372 => [ //Fungostop + - BG
                'clid' => 6890226,
                'product' => 'Fungostop + - BG',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            6163 => [ //OstyHealth - BG
                'clid' => 6890226,
                'product' => 'OstyHealth - BG',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            5605 => [ //Coralift - BG
                'clid' => 6890226,
                'product' => 'Coralift - BG',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            5797 => [ //Keto Delight - BG
                'clid' => 6890226,
                'product' => 'Keto Delight - BG',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            6390 => [ //Cardio Life - BG
                'clid' => 6890226,
                'product' => 'Cardio Life - BG',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            6133 => [ //Diabex - BG
                'clid' => 6890226,
                'product' => 'Diabex - BG',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            5280 => [ //Green energy - BG
                'clid' => 6890226,
                'product' => 'erectil',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            3977 => [ //Green energy - BG
                'clid' => 6890226,
                'product' => 'energy',
                'configs' => [
                    'brakeLogFolder'	=> true,
                    'urlOrderAdd'		=> 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'		=> 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            3978 => [ //Lighten-up - BG
                'clid' => 6890226,
                'product' => 'pighten',
                'configs' => [
                    'brakeLogFolder'	=> true,
                    'urlOrderAdd'		=> 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'		=> 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            4012 => [ //Liver cleanse - BG
                'clid' => 6890226,
                'product' => 'Liver cleanse',
                'configs' => [
                    'brakeLogFolder'	=> true,
                    'urlOrderAdd'		=> 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'		=> 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            4107 => [ //Super Antioxidant - BG
                'clid' => 6890226,
                'product' => 'super antioxidant',
                'configs' => [
                    'brakeLogFolder'	=> true,
                    'urlOrderAdd'		=> 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'		=> 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            4220 => [ //CoQ10 Complex - BG
                'clid' => 6890226,
                'product' => 'Q10',
                'configs' => [
                    'brakeLogFolder'	=> true,
                    'urlOrderAdd'		=> 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'		=> 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            4348 => [ //Organic Vitamin D - BG
                'clid' => 6890226,
                'product' => 'Organic Vitamin D',
                'configs' => [
                    'brakeLogFolder'	=> true,
                    'urlOrderAdd'		=> 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'		=> 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            4363 => [ //AloeFresh - BG
                'clid' => 6890226,
                'product' => 'AloeFresh',
                'configs' => [
                    'brakeLogFolder'	=> true,
                    'urlOrderAdd'		=> 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'		=> 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            4366 => [ //Maca Power - BG
                'clid' => 6890226,
                'product' => 'Maca Power',
                'configs' => [
                    'brakeLogFolder'	=> true,
                    'urlOrderAdd'		=> 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'		=> 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            4542 => [ //Maca Power - BG
                'clid' => 6890226,
                'product' => 'Cell Power - BG',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            2901 => [ //Cardio NRJ - BG
                'clid' => 6890226,
                'product' => 'Cardio NRJ - BG',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-bg.api',
                ],
            ],
            4352 => [ //Cell Power - RO
                'clid' => 2485825,
                'product' => 'Cell Power - RO',
                'configs' => [
                    'brakeLogFolder'	=> true,
                    'urlOrderAdd'		=> 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'		=> 'https://cc.itgbg.com/api/uaterra-ro.api',
                ],
            ],
            2493 => [ //Cardio NRJ - RO
                'clid' => 2485825,
                'product' => 'Cardio NRJ - RO',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-ro.api',
                ],
            ],
            5982 => [ //Diabex - RO
                'clid' => 2485825,
                'product' => 'Diabex - RO',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-ro.api',
                ],
            ],
            5373 => [ //Fungostop + - RO
                'clid' => 2485825,
                'product' => 'Fungostop + - RO',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-ro.api',
                ],
            ],
            5617 => [ //Coralift - RO
                'clid' => 2485825,
                'product' => 'Coralift - RO',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-ro.api',
                ],
            ],
            5025 => [ //Erectil - RO
                'clid' => 2485825,
                'product' => 'Erectil - RO',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-ro.api',
                ],
            ],
            6324 => [ //Keto Delight - RO
                'clid' => 2485825,
                'product' => 'Keto Delight - RO',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-ro.api',
                ],
            ],
            5983 => [ //Cardio Life - RO
                'clid' => 2485825,
                'product' => 'Cardio Life - RO',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-ro.api',
                ],
            ],
            7656 => [ //
                'clid' => 1996148,
                'product' => 'Keto Delight - GR',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-uk.api',
                ],
            ],
            7664 => [ //
                'clid' => 1996148,
                'product' => 'Diabex - GR',
                'configs' => [
                    'brakeLogFolder'    => true,
                    'urlOrderAdd'       => 'https://cc.itgbg.com/cgi-bin/cc/add-call-list.cgi',
                    'urlOrderInfo'      => 'https://cc.itgbg.com/api/uaterra-uk.api',
                ],
            ],
        ],
    ],

    'configs' => [
        'statuses' => [
            'trash'	=> [
                'Trash'				=> '',
                'Треш'              => '',
            ],
            'reject'	=> [
                'Reject'				=> '',
                'Отказ'                 => '',
            ],
            'expect'	=> [
                'In Process'			=> '',
                'Tech Problem'		=> '',
                'В обработка'		=> '',
                ''		=> '',
                'null' => '',
            ],
            'confirm'	=> [
                'Sent'			=> '',
                'Purchased'			=> '',
                'Approve'			=> '', 
                'Потвърден'         => '',
            ],
        ],
        'brakeLogFolder'	=> true,
    ],
];