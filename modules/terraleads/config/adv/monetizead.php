<?php

/*
http://nutracare.affise.com/signin
login: kosta@terraleads.com
pass:  nutracareterra2018

http://connectcpa.net/terraleads/adv/adv-postback-status?systemUserId=21794&systemOrderId={sub1}&partnerStatus={status}&comment={comment}&postbackApiKey=28es4350cb637d19cd766e739dc24283&payout={sum}&offer_title={offer_name}&goal={goal}&subids={sub1}:{sub2}:{sub3}

 */

return [
	21975 => [
		'terraleadsApiKey'			=> '70ff3a62c1d0cdae697614ef51cae066',
		'key' 						=> '484348c9902c93eb36bffce72484cb8d',
		
		'offers' => [
			2835 => [
				'company_id' => 18,
				'offer_id' => 1200,
				'network' => 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2836 => [
				'company_id' => 18,
				'offer_id' => 1201,
				'network' => 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2860 => [
				'company_id' => 18,
				'offer_id' => 1203,
				'network' => 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2861 => [
				'company_id' => 18,
				'offer_id' => 1202,
				'network' => 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2994 => [
				'company_id' => 18,
				'offer_id' => 1204,
				'network' => 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3042 => [
				'company_id' => 18,
				'offer_id' => 1205,
				'network' => 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2995 => [
				'company_id' => 18,
				'offer_id' => 1206,
				'network' => 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3144 => [
				'company_id' => 18,
				'offer_id' => 1207,
				'network' => 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3158 => [
				'company_id' => 22,
				'offer_id' => 1210,
				'network' => 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3159 => [
				'company_id' => 22,
				'offer_id' => 1209,
				'network' => 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3157 => [
				'company_id' => 22,
				'offer_id' => 1208,
				'network' => 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3292 => [
				'company_id' => 22,
				'offer_id' => 1211,
				'network' => 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3297 => [
				'company_id' => 22,
				'offer_id' => 1215,
				'network' => 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3298 => [
				'company_id' => 22,
				'offer_id' => 1214,
				'network' => 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3305 => [
				'company_id' => 22,
				'offer_id' => 1213,
				'network' => 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3329 => [
				'company_id' => 22,
				'offer_id' => 1212,
				'network' => 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'trash' => 'Invalid Date, Duplicate Order',
			],
			'reject' => [
				'cancelled' => 'User declined after conﬁrming the order ',
			],
			'expect' => [
				'hold' => 'action "In process" (decision is not taken yet, you need request the status later)',
			],
			'confirm' => [
				'buyout_approved' => 'Order received',
				'buyout_hold' => 'Order in transport',
				'confirmed' => 'Order Confirmed',
				'buyout_rejected' => 'Order returned',
			],
		],
		
		'brakeLogFolder' => true,
		'urlOrderAdd' => 'https://monad.ziffapp.com/api/v1/microservices/order/create/{key}/{company_id}',
		'urlOrderInfo' => 'https://monad.ziffapp.com/api/v1/microservices/order/status/{key}/{company_id}',
//		'statusEnabled'		=> false,
	],
];