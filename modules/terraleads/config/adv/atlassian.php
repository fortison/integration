<?php

return [
    31170 => [
        'terraleadsApiKey'			=> 'e94a15357f0581496f4898f800962647',
        'postbackApiKey'			=> 'e94a15357f0581496f4898f80096264',
        'api_key' 					=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXBpLmluYi10ZWFtLmNvbVwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTYxODAwMDY4OCwibmJmIjoxNjE4MDAwNjg4LCJqdGkiOiI0WU5PZ2NPRm53Wmx3N2RTIiwic3ViIjozNSwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.RoN5wvnnttBjL4TC127GG1renNcSaKUJ_MqBQ7D9Hr8',
        'utm_source'			    => 'TerraLeads',
        'offers' => [
            4482 => [//

                'offer_id'  => 11,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4495 => [//

                'offer_id'  => 16,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4951 => [//

                'offer_id'  => 31,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4210 => [//

                'offer_id'  => 01,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
        ],
    ],

    'configs' => [
        'statuses' => [
            'trash'	=> [
                'trash' 			=> '',
            ],
            'reject'	=> [
                'reject'	        => '',
            ],
            'expect'	=> [
                'expect'			=> '',
            ],
            'confirm'	=> [
                'confirm'	        => '',
            ],
        ],

        'brakeLogFolder'	=> true,
        'urlOrderAdd'		=> 'https://api.inb-team.com/api/team/leads',
//		'statusEnabled'		=> false,
    ],
];
