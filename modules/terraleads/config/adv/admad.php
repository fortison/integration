<?php

/*
our postaback:

http://connectcpa.net/terraleads/adv/adv-postback-status?&systemOrderId={session_uuid}&partnerStatus={status}&systemUserId=44580&postbackApiKey=ed25dec931067514c182bc120cc7fee2

{session_uuid} - session_uuid in your system, here I transfer lead in our system
{status} - lead status ('trash', 'reject', 'expect', 'confirm')
*/
return [
	
	44580 => [
		'terraleadsApiKey' => 'ed25dec931067514c182bc120cc7feee',
		'postbackApiKey'   => 'ed25dec931067514c182bc120cc7fee2',
		
		'offers' => [
			8330 => [//Glamilips - AT
				'offer_code' 	=> 'GLN1AT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8331 => [//Glamilips - CZ
				'offer_code' 	=> 'GLN1CZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8332 => [//Glamilips - DE
				'offer_code' 	=> 'GLN1DE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8520 => [//Glamilips - EE
				'offer_code' 	=> 'GLN1EE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8333 => [//Glamilips - ES
				'offer_code' 	=> 'GLN1ES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8334 => [//Glamilips - HR
				'offer_code' 	=> 'GLN1HR',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8335 => [//Glamilips - HU
				'offer_code' 	=> 'GLN1HU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8336 => [//Glamilips - IT
				'offer_code' 	=> 'GLN1IT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8337 => [//Glamilips - PL
				'offer_code' 	=> 'GLN1PL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8339 => [//Glamilips - RO
				'offer_code' 	=> 'GLN1RO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8340 => [//Glamilips - SI
				'offer_code' 	=> 'GLN1SI',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8341 => [//Glamilips - SK
				'offer_code' 	=> 'GLN1SK',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7104 => [//Trichomist - HR
				'offer_code' 	=> 'TFN1HR',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6959 => [//Hotrifen - RO
				'offer_code' 	=> 'HTN1RO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6958 => [//Hotrifen - DE
				'offer_code' 	=> 'HTN1DE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6957 => [//Hotrifen - AT
				'offer_code' 	=> 'HTN1AT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8919 => [//Vidia Oil - AT
				'offer_code' 	=> 'VON1AT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8920 => [//Vidia Oil - CZ
				'offer_code' 	=> 'VON1CZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8921 => [//Vidia Oil - DE
				'offer_code' 	=> 'VON1DE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8922 => [//Vidia Oil - EE
				'offer_code' 	=> 'VON1EE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8923 => [//Vidia Oil - ES
				'offer_code' 	=> 'VON1ES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8924 => [//Vidia Oil - HR
				'offer_code' 	=> 'VON1HR',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8925 => [//Vidia Oil - HU
				'offer_code' 	=> 'VON1HU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8926 => [//Vidia Oil - IT
				'offer_code' 	=> 'VON1IT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8927 => [//Vidia Oil - PL
				'offer_code' 	=> 'VON1PL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8928 => [//Vidia Oil - RO
				'offer_code' 	=> 'VON1RO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8929 => [//Vidia Oil - SI
				'offer_code' 	=> 'VON1SI',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8930 => [//Vidia Oil - SK
				'offer_code' 	=> 'VON1SK',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6956 => [//Perfecto Cream - RO
				'offer_code' 	=> 'PCN1RO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8712 => [//Visadore - SI
				'offer_code' 	=> 'VDN1SI',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8711 => [//Visadore - RO
				'offer_code' 	=> 'VDN1RO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8710 => [//Visadore - IT
				'offer_code' 	=> 'VDN1IT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8709 => [//Visadore - HU
				'offer_code' 	=> 'VDN1HU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8708 => [//Visadore - HR
				'offer_code' 	=> 'VDN1HR',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8707 => [//Visadore - ES
				'offer_code' 	=> 'VDN1ES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8706 => [//Visadore - EE
				'offer_code' 	=> 'VDN1EE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8705 => [//Visadore - AT
				'offer_code' 	=> 'VDN1AT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8704 => [//Visadore - DE
				'offer_code' 	=> 'VDN1DE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8703 => [//Visadore - SK
				'offer_code' 	=> 'VDN1SK',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8702 => [//Visadore - CZ
				'offer_code' 	=> 'VDN1CZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8701 => [//Visadore - PL
				'offer_code' 	=> 'VDN1PL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8025 => [//Vigaman - EE111
				'offer_code' 	=> 'VIN1EE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8024 => [//Trichomist - EE
				'offer_code' 	=> 'TFN1EE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8023 => [//Perfecto Cream - EE
				'offer_code' 	=> 'PCN1EE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8022 => [//YES lashes - EE
				'offer_code' 	=> 'YEN1EE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7835 => [//Vitasimil - AT
				'offer_code' 	=> 'VTN1AT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7836 => [//Vitasimil - CZ
				'offer_code' 	=> 'VTN1CZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7837 => [//Vitasimil - DE
				'offer_code' 	=> 'VTN1DE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7838 => [//Vitasimil - ES
				'offer_code' 	=> 'VTN1ES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7839 => [//Vitasimil - HR
				'offer_code' 	=> 'VTN1HR',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7840 => [//Vitasimil - HU
				'offer_code' 	=> 'VTN1HU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7841 => [//Vitasimil - IT
				'offer_code' 	=> 'VTN1IT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7842 => [//Vitasimil - PL
				'offer_code' 	=> 'VTN1PL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7843 => [//Vitasimil - PT
				'offer_code' 	=> 'VTN1PT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7844 => [//Vitasimil - RO
				'offer_code' 	=> 'VTN1RO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7845 => [//Vitasimil - SI
				'offer_code' 	=> 'VTN1SI',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7846 => [//Vitasimil - SK
				'offer_code' 	=> 'VTN1SK',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7823 => [//LeSkinic - AT
				'offer_code' 	=> 'LDN1AT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7824 => [//LeSkinic - CZ
				'offer_code' 	=> 'LDN1CZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7825 => [//LeSkinic - DE
				'offer_code' 	=> 'LDN1DE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7826 => [//LeSkinic - ES
				'offer_code' 	=> 'LDN1ES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7827 => [//LeSkinic - HR
				'offer_code' 	=> 'LDN1HR',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7828 => [//LeSkinic - HU
				'offer_code' 	=> 'LDN1HU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7829 => [//LeSkinic - IT
				'offer_code' 	=> 'LDN1IT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7830 => [//LeSkinic - PL
				'offer_code' 	=> 'LDN1PL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7831 => [//LeSkinic - PT
				'offer_code' 	=> 'LDN1PT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7832 => [//LeSkinic - RO
				'offer_code' 	=> 'LDN1RO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7833 => [//LeSkinic - SI
				'offer_code' 	=> 'LDN1SI',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7834 => [//LeSkinic - SK
				'offer_code' 	=> 'LDN1SK',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7886 => [//Softisenil - AT
				'offer_code' 	=> 'SON1AT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7887 => [//Softisenil - CZ
				'offer_code' 	=> 'SON1CZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7888 => [//Softisenil - DE
				'offer_code' 	=> 'SON1DE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7889 => [//Softisenil - ES
				'offer_code' 	=> 'SON1ES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7890 => [//Softisenil - HR
				'offer_code' 	=> 'SON1HR',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7891 => [//Softisenil - HU
				'offer_code' 	=> 'SON1HU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7892 => [//Softisenil - IT
				'offer_code' 	=> 'SON1IT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7893 => [//Softisenil - PL
				'offer_code' 	=> 'SON1PL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7894 => [//Softisenil - PT
				'offer_code' 	=> 'SON1PT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7895 => [//Softisenil - RO
				'offer_code' 	=> 'SON1RO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7896 => [//Softisenil - SI
				'offer_code' 	=> 'SON1SI',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7897 => [//Softisenil - SK
				'offer_code' 	=> 'SON1SK',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7856 => [//Sasparin - HR (free price)
				'offer_code' 	=> 'SS01HR',
				'token'			=> 'd171400d-fc3c-452a-ab0e-d888d1a78ab6',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7855 => [//Sasparin - HR (free price)
				'offer_code' 	=> 'SS01RO',
				'token'			=> '2407d68a-9de8-4838-9bcc-b9f3d8ec2829',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7509 => [//Vigaman - SK
				'offer_code' 	=> 'VIN1SK',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7508 => [//Vigaman - SI
				'offer_code' 	=> 'VIN1SI',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7507 => [//Vigaman - RO
				'offer_code' 	=> 'VIN1RO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7506 => [//Vigaman - PT
				'offer_code' 	=> 'VIN1PT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7505 => [//Vigaman - PL
				'offer_code' 	=> 'VIN1PL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7504 => [//Vigaman - IT
				'offer_code' 	=> 'VIN1IT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7503 => [//Vigaman - HU
				'offer_code' 	=> 'VIN1HU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7502 => [//Vigaman - HR
				'offer_code' 	=> 'VIN1HR',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7501 => [//Vigaman - ES
				'offer_code' 	=> 'VIN1ES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7497 => [//Vigaman - DE
				'offer_code' 	=> 'VIN1DE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7496 => [//Vigaman - CZ
				'offer_code' 	=> 'VIN1CZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7495 => [//Vigaman - AT
				'offer_code' 	=> 'VIN1AT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7532 => [//NightBeast - AT
				'offer_code' 	=> 'NBN1AT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7531 => [//NightBeast - CZ
				'offer_code' 	=> 'NBN1CZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7530 => [//NightBeast - DE
				'offer_code' 	=> 'NBN1DE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7529 => [//NightBeast - HR
				'offer_code' 	=> 'NBN1HR',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7528 => [//NightBeast - HU
				'offer_code' 	=> 'NBN1HU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7527 => [//NightBeast - ES
				'offer_code' 	=> 'NBN1ES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7526 => [//NightBeast - IT
				'offer_code' 	=> 'NBN1IT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7525 => [//NightBeast - PL
				'offer_code' 	=> 'NBN1PL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7524 => [//NightBeast - PT
				'offer_code' 	=> 'NBN1PT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7523 => [//NightBeast - RO
				'offer_code' 	=> 'NBN1RO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7522 => [//NightBeast - SI
				'offer_code' 	=> 'NBN1SI',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7521 => [//NightBeast - SK
				'offer_code' 	=> 'NBN1SK',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7472 => [//Venicold Gel - AT
				'offer_code' 	=> 'VGN1AT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7471 => [//Venicold Gel - CZ
				'offer_code' 	=> 'VGN1CZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7470 => [//Venicold Gel - DE
				'offer_code' 	=> 'VGN1DE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7469 => [//Venicold Gel - ES
				'offer_code' 	=> 'VGN1ES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7468 => [//Venicold Gel - HR
				'offer_code' 	=> 'VGN1HR',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7467 => [//Venicold Gel - HU
				'offer_code' 	=> 'VGN1HU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7466 => [//Venicold Gel - IT
				'offer_code' 	=> 'VGN1IT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7465 => [//Venicold Gel - PL
				'offer_code' 	=> 'VGN1PL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7464 => [//Venicold Gel - PT
				'offer_code' 	=> 'VGN1PT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7463 => [//Venicold Gel - RO
				'offer_code' 	=> 'VGN1RO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7462 => [//Venicold Gel - SI
				'offer_code' 	=> 'VGN1SI',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7461 => [//Venicold Gel - SK
				'offer_code' 	=> 'VGN1SK',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7475 => [//YES lashes - AT
				'offer_code' 	=> 'YE1NAT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7476 => [//YES lashes - CZ
				'offer_code' 	=> 'YE1NCZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7477 => [//YES lashes - DE
				'offer_code' 	=> 'YE1NDE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7478 => [//YES lashes - ES
				'offer_code' 	=> 'YE1NES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7479 => [//YES lashes - HR
				'offer_code' 	=> 'YE1NHR',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7480 => [//YES lashes - HU
				'offer_code' 	=> 'YE1NHU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7481 => [//YES lashes - IT
				'offer_code' 	=> 'YE1NIT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7482 => [//YES lashes - PL
				'offer_code' 	=> 'YE1NPL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7483 => [//YES lashes - PT
				'offer_code' 	=> 'YE1NPT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7484 => [//YES lashes - RO
				'offer_code' 	=> 'YE1NRO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7485 => [//YES lashes - SI
				'offer_code' 	=> 'YE1NSI',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7486 => [//YES lashes - SK
				'offer_code' 	=> 'YE1NSK',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7407 => [//GetShape - AT
				'offer_code' 	=> 'GSENAT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7406 => [//GetShape - CZ
				'offer_code' 	=> 'GSENCZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7405 => [//GetShape - DE
				'offer_code' 	=> 'GSENDE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7404 => [//GetShape - ES
				'offer_code' 	=> 'GSENES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7403 => [//GetShape - HR
				'offer_code' 	=> 'GSENHR',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7402 => [//GetShape - HU
				'offer_code' 	=> 'GSENHU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7401 => [//GetShape - IT
				'offer_code' 	=> 'GSENIT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7400 => [//GetShape - PL
				'offer_code' 	=> 'GSENPL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7399 => [//GetShape - PT
				'offer_code' 	=> 'GSENPT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7398 => [//GetShape - RO
				'offer_code' 	=> 'GSENRO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7397 => [//GetShape - SK
				'offer_code' 	=> 'GSENSK',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7396 => [//Dr Merritz - SI
				'offer_code' 	=> 'GSENSI',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7423 => [//Dr Merritz - SI
				'offer_code' 	=> 'DRN1SI',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7422 => [//Dr Merritz - SK
				'offer_code' 	=> 'DRN1SK',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7421 => [//Dr Merritz - PT
				'offer_code' 	=> 'DRN1PT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7420 => [//Dr Merritz - RO
				'offer_code' 	=> 'DRN1RO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7419 => [//Dr Merritz - PL
				'offer_code' 	=> 'DRN1PL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7418 => [//Dr Merritz - IT
				'offer_code' 	=> 'DRN1IT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7417 => [//Dr Merritz - HU
				'offer_code' 	=> 'DRN1HU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7416 => [//Dr Merritz - HR
				'offer_code' 	=> 'DRN1HR',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7415 => [//Dr Merritz - ES
				'offer_code' 	=> 'DRN1ES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7414 => [//Dr Merritz - DE
				'offer_code' 	=> 'DRN1DE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7413 => [//Dr Merritz - CZ
				'offer_code' 	=> 'DRN1CZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7412 => [//Dr Merritz - AT
				'offer_code' 	=> 'DRN1AT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6946 => [//BangSize - AT1
				'offer_code' 	=> 'BS1NAT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6945 => [//BangSize - DE
				'offer_code' 	=> 'BS1NDE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6944 => [//BangSize - RO
				'offer_code' 	=> 'BS1NRO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8021 => [//Activestin - EE
				'offer_code' 	=> 'AV1NEE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6943 => [//Activestin - RO
				'offer_code' 	=> 'AV1NRO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6942 => [//Activestin - DE
				'offer_code' 	=> 'AV1NDE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6941 => [//Activestin - AT
				'offer_code' 	=> 'AV1NAT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7103 => [//Sasparin - HU
				'offer_code' 	=> 'SS1NHU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7102 => [//Sasparin - SK
				'offer_code' 	=> 'SS1NSK',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7101 => [//Sasparin - PT
				'offer_code' 	=> 'SS1NPT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7100 => [//Sasparin - HR
				'offer_code' 	=> 'SS1NHR',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6964 => [//Sasparin - AT
				'offer_code' 	=> 'SS1NAT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6963 => [//Sasparin - DE
				'offer_code' 	=> 'SS1NDE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6962 => [//Sasparin - RO
				'offer_code' 	=> 'SS1NRO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6190 => [//Trichomist - AT
				'offer_code' 	=> 'TFN1AT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6968 => [//Trichomist - RO
				'offer_code' 	=> 'TFN1RO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6193 => [//Trichomist - HU
				'offer_code' 	=> 'TFN1HU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6192 => [//Trichomist - ES
				'offer_code' 	=> 'TFN1ES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6191 => [//Trichomist - IT
				'offer_code' 	=> 'TFN1IT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6189 => [//Trichomist - DE
				'offer_code' 	=> 'TFN1DE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6188 => [//Trichomist - SK
				'offer_code' 	=> 'TFN1SK',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6187 => [//Trichomist - CZ
				'offer_code' 	=> 'TFN1CZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			0000 => [//Trichomist - HR
				'offer_code' 	=> 'TFN1HR',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			0000 => [//Trichomist - PT
				'offer_code' 	=> 'TFN1PT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6398 => [//Men MAX - PL
				'offer_code' 	=> 'MM1NPL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6763 => [//Trichomist Forte - PL
				'offer_code' 	=> 'TFN1PL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7113 => [//Activestin - HU
				'offer_code' 	=> 'AV1NHU',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7112 => [//Activestin - SK
				'offer_code' 	=> 'AV1NSK',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7111 => [//AActivestin - PT
				'offer_code' 	=> 'AV1NPT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7110 => [//Activestin - HR
				'offer_code' 	=> 'AV1NHR',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6760 => [//Activestin - PL
				'offer_code' 	=> 'AV1NPL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6717 => [//Activestin - IT
				'offer_code' 	=> 'AV1NIT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6716 => [//Activestin - ES
				'offer_code' 	=> 'AV1NES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6715 => [//Activestin - CZ
				'offer_code' 	=> 'AV1NCZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6759 => [//BangSize - PL
				'offer_code' 	=> 'BS1NPL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6714 => [//BangSize - IT
				'offer_code' 	=> 'BS1NIT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6713 => [//BangSize - ES
				'offer_code' 	=> 'BS1NES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6712 => [//BangSize - CZ
				'offer_code' 	=> 'BS1NCZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6762 => [//Sasparin - PL
				'offer_code' 	=> 'SS1NPL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6711 => [//Sasparin - IT
				'offer_code' 	=> 'SS1NIT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6710 => [//Sasparin - ES
				'offer_code' 	=> 'SS1NES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6700 => [//Sasparin - CZ
				'offer_code' 	=> 'SS1NCZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6761 => [//Hotrifen - PL
				'offer_code' 	=> 'HT1NPL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7571 => [//Hotrifen PRO - DE
				'offer_code' 	=> 'HT1NDE',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6718 => [//Hotrifen - CZ
				'offer_code' 	=> 'HT1NCZ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6719 => [//Hotrifen - ES
				'offer_code' 	=> 'HT1NES',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6720 => [//Hotrifen - IT
				'offer_code' 	=> 'HT1NIT',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6179 => [//FlexaFit - RO
				'offer_code' 	=> 'FX1NRO',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6169 => [//FlexaFit - PL
				'offer_code' 	=> 'FX1NPL',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'tokens' => [
			'PL' => '21c80757-ca14-420c-bf48-df1c262974d9',
			'CZ' => '662fd423-934d-4d27-a8d4-546f426dbcde',
			'IT' => 'c0462e94-8ee1-41d6-b358-ba271e204335',
			'SK' => '2524eb8a-c7cb-4ccb-932e-6981f1efbcc9',
			'DE' => 'ab9823f1-5425-4564-aec6-626850652eaf',
			'ES' => '196c70fc-c2f9-47a9-80d1-8d12f7d1ae4b',
			'HR' => '90373373-cdf6-45fc-bace-2a34e573c9ce',
			'HU' => 'f69a104e-3fe2-4c50-8eaf-ee73f83c3990',
			'AT' => 'd811dbe5-0072-4d79-9d70-b34206bcd369',
			'PT' => 'f645cbfb-93ac-4cad-b919-42fd35c45e8a',
			'SI' => '9e491381-b2c9-4ebe-8ea0-2d322e8894fa',
			'RO' => '0723e56f-fc00-4e08-9987-be261e064d53',
			'FR' => 'a1420ea6-5fd4-4e2a-941d-e1969b07d804',
			'EE' => 'c9291070-6946-42c7-8181-a1d4f5896706',
		]
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'trash' => '',
			],
			'reject' => [
				'reject' => '',
			],
			'expect' => [
				'expect' => '',
			],
			'confirm' => [
				'confirm' => '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://api.nsbox.pl/api/v3/short_orders',
		'urlOrderInfo'		=> '',
	],
];

?>