<?php

return [
	41887 => [
		'terraleadsApiKey'	=> '0000000000000000000000000000',
		'login' 			=> 'Terraleads',
		'pass' 				=> '41887Terr@',
		
		'offers' => [
			6353 => [
				'comodityid' => 8,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6352 => [
				'comodityid' => 7,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			0000 => [
				'comodityid' => 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				6 => 'Некорректный',
				5 => 'Дубль',
			],
			'reject' => [
				3 => 'Отказ',
				4 => 'Не заказывал',
			],
			'expect' => [
				0 => 'Новый',
				8 => 'На исполнении',
				1 => 'Обзвон',
			],
			'confirm' => [
				2 => 'Одобрен',
			],
		],
		
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://synch.uz/api/advertiser/sendadvertising/2',
		'urlOrderInfo'		=> 'https://synch.uz/api/statusliad/list',
		'urlOrderToken'		=> 'https://synch.uz/api/advertiser/gettoken',
	],
];