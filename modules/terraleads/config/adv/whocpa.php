<?php

/*
 */

return [

    31247 => [
        'terraleadsApiKey'			=> '2886bd58785a88d86a4cdeaf2a3e5baf',
        'postbackApiKey'			=> '2886bd58785a88d86a4cdeaf2a3e5ba1',
        'api_key' 					=> 'rbzF31WEM0JQWd60',

        'offers' => [
            4408 => [ // Longex - KH
                'offer_id'	=> 6023,
                'aff_id'	=> 1016171310,
                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4409 => [ // Dentalix - KH
                'offer_id'  => 6026,
                'aff_id'    => 1016171310,
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4410 => [ // M-Power - KH
                'offer_id'  => 6018,
                'aff_id'    => 1016171310,
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4492 => [ // Golden Amulet - KH
                'offer_id'	=> 6077,
                'aff_id'	=> 1016171310,
                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            5019 => [ // Dentalix - MM
                'offer_id'  => 6027,
                'aff_id'    => 1016171310,
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5020 => [ // Body Shaper - MM
                'offer_id'  => 6037,
                'aff_id'    => 1016171310,
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5021 => [ // Fuel Saver - MM
                'offer_id'  => 6046,
                'aff_id'    => 1016171310,
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5022 => [ // Ecolight - MM
                'offer_id'  => 6065,
                'aff_id'    => 1016171310,
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5201 => [ // Helmina - MM
                'offer_id'  => 6017,
                'aff_id'    => 1016171310,
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5225 => [ // Vichen - TH
                'offer_id'  => 6071,
                'aff_id'    => 1016171310,
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5226 => [ // Mikono - TH
                'offer_id'  => 6072,
                'aff_id'    => 1016171310,
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5227 => [ // Variste - TH
                'offer_id'  => 6073,
                'aff_id'    => 1016171310,
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5228 => [ // Sinoflex - TH
                'offer_id'  => 6074,
                'aff_id'    => 1016171310,
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5229 => [ // Optrix - TH v
                'offer_id'  => 6067,
                'aff_id'    => 1016171310,
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
        ],
    ],

    'configs' => [
        'statuses' => [
            'trash'	=> [
                'trash'		=> '',
            ],
            'reject'	=> [
                'reject'	=> '',
            ],
            'expect'	=> [
                'expect'	=> '',
            ],
            'confirm'	=> [
                'confirm'	=> '',
            ],
        ],
        'brakeLogFolder'	=> true,
        'urlOrderAdd'		=> 'https://api2.whocpa.asia/lead/add',
        'urlOrderInfo'		=> '',
    ],
];

?>