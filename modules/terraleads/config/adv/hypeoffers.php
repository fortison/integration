<?php

/*
 * http://connectcpa.net/terraleads/adv/adv-postback-status?systemUserId=21407&systemOrderId={subids.1}&partnerStatus={status}&date={date}&revenue={revenue}&postbackApiKey=aace855b4e974a9cf21f4a56d99sb5es
 */

return [
	
	21407 => [
		'terraleadsApiKey'	=> 'aace855b4e974a9cf21f4a56d99ab5ed',
		'postbackApiKey'    => 'aace855b4e974a9cf21f4a56d99sb5es',
		'key' 				=> 'kb_TFnx7BzK9G-jtkjg3lPkbXQpVGlMG9U6RzqzLmviQdxXnKoUMAWxXBe1P4aab',
		
		'offers' => [
			2170 => [
				'stream' => '06670ef3-94a8-40d6-8f21-e100220127a5',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2166 => [
				'stream' => '4f38ff7d-5059-4591-b628-b79385b72eb5',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3049 => [
				'stream' => '1b27e987-b7fd-4aa9-8b5c-8d58e1b2f766',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3251 => [
				'stream' => '15a6a4d3-5f4d-4d86-8068-0ceae9ad6013',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3253 => [
				'stream' => '08dbfb47-183a-4ed6-93f3-770f88fa909b',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3291 => [
				'stream' => 'c886c8dd-6bca-4632-a9e3-9a4539c2e81f',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3295 => [
				'stream' => 'a90c0718-774c-4268-9dbc-159a44950d83',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3294 => [
				'stream' => '39bc11df-b60c-4e00-a95c-d09dd9a579c0',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3050 => [ //Papillor - PL
				'stream' => '5fff2bdd-76b8-45a1-8e45-7c6886cbedfe',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3051 => [ //Papillor - HU
				'stream' => 'b56ffe05-cf2d-4966-840e-976ba65d9f96',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3052 => [ //Papillor - SK
				'stream' => 'aa23a471-600c-422f-9adc-d0fb20fe6219',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3053 => [ //Papillor - CZ
				'stream' => '98574951-2466-460c-af27-7944963019fb',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'invalid'		=> '',
			],
			'reject'	=> [
				'cancelled'		=> '',
			],
			'expect'	=> [
				'in_progress'	=> '',
			],
			'confirm'	=> [
				'confirmed'		=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://app.hypeoffers.com/api/v1/lead/create',
	],
];