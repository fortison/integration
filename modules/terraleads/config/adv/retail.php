<?php
/**
 * http://www.retailcrm.ru/docs/Developers/Index
 */

return [
    30883 => [
        'terraleadsApiKey'          => '09b08a0d4d2d00dbf1080ae8c59a01fe',
        'retailApiKey'              => 'cIMzCAogxjhvZmG69bRpKb9kG77C9yts',
        'delivery_city'             => [
            'lagos',
            'nairobi',
            'mombasa',
            'nakuru',
            'kisumu',
            'other',

        ],

        'offers' => [
            4364 => [
                'offerId'           => 120,
//              'productName'       => 'Alpha Beast - KE',
                'sourceSource'      => 'terraleads',
                'site'              => 'aftrdke-store-alb',
                'sourceCampaign'    => function ($model, $config) {
                    return $model->web_id;
                },
                'deliveryCode'      => 'delivery1',
                'deliveryAddressCity'   => function ($model, $config) {
                    return $model->city;
                },
                'weight'    => function ($model, $config) {
                    return $model->count;
                },
                'initialPrice'      => 6000,
                'countryIso'        => 'KE',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5041 => [
                'offerId'           => 74,
//              'productName'       => 'Normatone - KE',
                'sourceSource'      => 'terraleads',
                'site'              => 'aftrdke-store-htn',
                'sourceCampaign'    => function ($model, $config) {
                    return $model->web_id;
                },
                'deliveryCode'      => 'delivery1',
                'deliveryAddressCity'   => function ($model, $config) {
                    return $model->city;
                },
                'weight'    => function ($model, $config) {
                    return $model->count;
                },
                'initialPrice'      => 6000,
                'countryIso'        => 'KE',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
        ],
        'configs' => [
            'statuses' => [
                'confirm'   => [
                    'client-approved' => '',
                    'cancelled-during-delivery' => '',
                    'confirmed' => '',
                    'complete' => '',
                    'delivering' => '',
                    'approved-2' => '',
                    'hold-approve' => '',
                    'another-city-approved' => '',
                ],
                'reject' => [
                    'not-satisfied-price' => '',
                    'bought-elsewhere' => '',
                    'rejected' => '',
                    'rejected-after-approve' => '',
                    'not-interested' => '',
                    'not-ready' => '',
                    'travels' => '',
                    'will-call-back' => '',
                    'rejected-in-call-center' => '',
                    'another-city' => '',

                ],
                'expect' => [
                    'new' => '',
                    'expect' => '',
                    'remind' => '',
                    'recall-1' => '',
                    'recall-2' => '',
                    'recall-3' => '',
                    'remind-after-present' => '',
                    'remind-for-delivery' => '',
                    'remind-final' => '',
                    'recall-1-1' => '',
                    'recall-1-2' => '',
                    'recall-1-3' => '',
                    'recall-2-1' => '',
                    'recall-2-2' => '',
                    'recall-2-3' => '',
                    'recall-3-1' => '',
                    'recall-3-2' => '',
                    'recall-3-3' => '',
                    'hold' => '',
                    'no-reply-hold' => '',

                ],
                'trash' => [
                    'no-reply' => '',
                    'out-of-coverage' => '',
                    'bad-contact-data' => '',
                    'duplicate' => '',
                    'did-not-order' => '',
                ],
            ],
            'brakeLogFolder'    => true,
            'urlOrderAdd'   => 'https://aftrdke.retailcrm.ru/api/v5/orders/create',
            'urlOrderInfo'  => 'https://aftrdke.retailcrm.ru/api/v5/orders/statuses',
        ],
    ],
    27367 => [
        'terraleadsApiKey'			=> 'ae4d9a30282962a44ad9e3e99d88ab53',
        'retailApiKey' 				=> 'ehbRW4HlInDlBDnkRqoTPbCLvZE9RHwO',
        'delivery_city'				=> [

            'nairobi',
            'mombasa',
            'nakuru',
            'kisumu',
            'other',

        ],

        'offers' => [
            4512 => [
                'offerId'           => 66,
//              'productName'       => 'Express Fat Burner Premium - KE',
                'sourceSource'      => 'terraleads',
                'site'              => 'superdeal-store-efb',
                'sourceCampaign'    => function ($model, $config) {
                    return $model->web_id;
                },
                'deliveryCode'      => 'delivery1',
                'deliveryAddressCity'   => function ($model, $config) {
                    return $model->city;
                },
                'weight'    => function ($model, $config) {
                    return $model->count;
                },
                'initialPrice'      => 6000,
                'countryIso'        => 'KE',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3743 => [
                'offerId' 			=> 52,
//				'productName' 		=> 'Longjack XXXL ',
                'sourceSource'		=> 'terraleads',
                'site'		        => 'superdeal-store-ljx',
                'sourceCampaign'	=> function ($model, $config) {
                    return $model->web_id;
                },
                'deliveryCode'		=> 'delivery1',
                'deliveryAddressCity'	=> function ($model, $config) {
                    return $model->city;
                },
                'weight'	=> function ($model, $config) {
                    return $model->count;
                },
                'initialPrice'		=> 6000,
                'countryIso' 		=> 'KE',

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
        ],
        'configs' => [
            'statuses' => [
                'confirm'	=> [
                    'client-approved' => '',
                    'cancelled-during-delivery' => '',
                    'confirmed' => '',
                    'complete' => '',
                    'delivering' => '',
                    'approved-2' => '',
                ],
                'reject' => [
                    'not-satisfied-price' => '',
                    'bought-elsewhere' => '',
                    'rejected' => '',
                    'rejected-after-approve' => '',
                    'not-interested' => '',
                    'not-ready' => '',
                    'travels' => '',
                    'will-call-back' => '',
                    'rejected-in-call-center' => '',
                    'another-city' => '',

                ],
                'expect' => [
                    'new' => '',
                    'expect' => '',
                    'remind' => '',
                    'recall-1' => '',
                    'recall-2' => '',
                    'recall-3' => '',
                    'remind-after-present' => '',
                    'remind-for-delivery' => '',
                    'remind-final' => '',
                    'recall-1-1' => '',
                    'recall-1-2' => '',
                    'recall-1-3' => '',
                    'recall-2-1' => '',
                    'recall-2-2' => '',
                    'recall-2-3' => '',
                    'recall-3-1' => '',
                    'recall-3-2' => '',
                    'recall-3-3' => '',
                    'hold' => '',
                    'no-reply-hold' => '',

                ],
                'trash' => [
                    'no-reply' => '',
                    'out-of-coverage' => '',
                    'bad-contact-data' => '',
                    'duplicate' => '',
                    'did-not-order' => '',
                ],
            ],
            'brakeLogFolder'	=> true,
            'urlOrderAdd'	=> 'https://maff2.retailcrm.ru/api/v5/orders/create',
            'urlOrderInfo'	=> 'https://maff2.retailcrm.ru/api/v5/orders/statuses',
        ],
    ],

    14402 => [
		'terraleadsApiKey'			=> '658dd83652253a7b36b9c69676183976',
		'retailApiKey' 				=> '3vmGg6izULJmEFuIfoQIblPlwsr2HlOT',
		'delivery_city'				=> [
	               'lagos',
                    'abakaliki',
                    'abeokuta',
                    'abuja',
                    'akure',
                    'bayelsa',
                    'benin',
                    'calabar',
                    'ekiti',
                    'ibadan',
                    'ife',
                    'ilorin',
                    'kano',
                    'ota',
                    'onitsha',
                    'osogbo',
                    'owerri',
                    'portharcourt',
                    'warri',
                    'ughelli',
                    'other_ogun',
                    'other',
		],

        'offers' => [
            2174 => [
                'offerId' 			=> 69,
//				'productName' 		=> 'UltraClean Premium Detox',
                'sourceSource'		=> 'terraleads',
                'site'		        => 'superdeal-store-ucd',
                'sourceCampaign'	=> function ($model, $config) {
                    return $model->web_id;
                },
                'deliveryCode'		=> 'delivery1',
                'deliveryAddressCity'	=> function ($model, $config) {
                    return $model->city;
                },
                'weight'	=> function ($model, $config) {
                    return $model->count;
                },
                'initialPrice'		=> 19500,
                'countryIso' 		=> 'NG',

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            2963 => [
                'offerId'           => 53,
//              'productName'       => 'Alpha Beast - NG',
                'sourceSource'      => 'terraleads',
                'site'              => 'superdeal-store-alb',
                'sourceCampaign'    => function ($model, $config) {
                    return $model->web_id;
                },
                'deliveryCode'      => 'delivery1',
                'deliveryAddressCity'   => function ($model, $config) {
                    return $model->city;
                },
                'weight'    => function ($model, $config) {
                    return $model->count;
                },
                'initialPrice'      => 19500,
                'countryIso'        => 'NG',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            2209 => [
                'offerId' 			=> 67,
//				'productName' 		=> 'Garcinia Cambogia Premium',
                'sourceSource'		=> 'terraleads',
                'sourceCampaign'	=> function ($model, $config) {
                    return $model->web_id;
                },
                'deliveryCode'		=> 'delivery1',
                'deliveryAddressCity'	=> function ($model, $config) {
                    return $model->city;
                },
                'weight'	=> function ($model, $config) {
                    return $model->count;
                },
                'initialPrice'		=> 18000,
                'countryIso' 		=> 'NG',

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            2210 => [
                'offerId' 			=> 66,
//				'productName' 		=> 'Express Fat Burner Premium',
                'sourceSource'		=> 'terraleads',
                'site'		        => 'superdeal-store-efb',
                'sourceCampaign'	=> function ($model, $config) {
                    return $model->web_id;
                },
                'deliveryCode'		=> 'delivery1',
                'deliveryAddressCity'	=> function ($model, $config) {
                    return $model->city;
                },
                'weight'	=> function ($model, $config) {
                    return $model->count;
                },
                'initialPrice'		=> 19500,
                'countryIso' 		=> 'NG',

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            2237 => [
                'offerId' 			=> 51,
//				'productName' 		=> 'Sexual Health Support',
                'sourceSource'		=> 'terraleads',
                'sourceCampaign'	=> function ($model, $config) {
                    return $model->web_id;
                },
                'deliveryCode'		=> 'delivery1',
                'deliveryAddressCity'	=> function ($model, $config) {
                    return $model->city;
                },
                'weight'	=> function ($model, $config) {
                    return $model->count;
                },
                'initialPrice'		=> 15000,
                'countryIso' 		=> 'NG',

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            2236 => [
                'offerId' 			=> 52,
//				'productName' 		=> 'Longjack XXXL ',
                'sourceSource'		=> 'terraleads',
                'site'		        => 'superdeal-store-ljx',
                'sourceCampaign'	=> function ($model, $config) {
                    return $model->web_id;
                },
                'deliveryCode'		=> 'delivery1',
                'deliveryAddressCity'	=> function ($model, $config) {
                    return $model->city;
                },
                'weight'	=> function ($model, $config) {
                    return $model->count;
                },
                'initialPrice'		=> 22000,
                'countryIso' 		=> 'NG',

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4323 => [ //Normatone - NG
                'offerId'           => 74,
//              'productName'       => 'Longjack XXXL ',
                'sourceSource'      => 'terraleads',
                'site'              => 'superdeal-store-htn',
                'sourceCampaign'    => function ($model, $config) {
                    return $model->web_id;
                },
                'deliveryCode'      => 'delivery1',
                'deliveryAddressCity'   => function ($model, $config) {
                    return $model->city;
                },
                'weight'    => function ($model, $config) {
                    return $model->count;
                },
                'initialPrice'      => 19500,
                'countryIso'        => 'NG',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3099 => [
                'offerId' 			=> 40,
//				'productName' 		=> 'ProHealth Immune Boost',
                'sourceSource'		=> 'terraleads',
                'sourceCampaign'	=> function ($model, $config) {
                    return $model->web_id;
                },
                'deliveryCode'		=> 'delivery1',
                'deliveryAddressCity'	=> function ($model, $config) {
                    return $model->city;
                },
                'weight'	=> function ($model, $config) {
                    return $model->count;
                },
                'initialPrice'		=> 18000,
                'countryIso' 		=> 'NG',

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
        ],
        'configs' => [
            'statuses' => [
                'confirm'	=> [
                    'client-approved' => '',
                    'cancelled-during-delivery' => '',
                    'confirmed' => '',
                    'complete' => '',
                    'delivering' => '',
                    'approved-2' => '',
                    'hold-approve' => '',
                ],
                'reject' => [
                    'not-satisfied-price' => '',
                    'bought-elsewhere' => '',
                    'rejected' => '',
                    'rejected-after-approve' => '',
                    'not-interested' => '',
                    'not-ready' => '',
                    'travels' => '',
                    'will-call-back' => '',
                    'rejected-in-call-center' => '',
                    'another-city' => '',

                ],
                'expect' => [
                    'new' => '',
                    'expect' => '',
                    'remind' => '',
                    'recall-1' => '',
                    'recall-2' => '',
                    'recall-3' => '',
                    'remind-after-present' => '',
                    'remind-for-delivery' => '',
                    'remind-final' => '',
                    'recall-1-1' => '',
                    'recall-1-2' => '',
                    'recall-1-3' => '',
                    'recall-2-1' => '',
                    'recall-2-2' => '',
                    'recall-2-3' => '',
                    'recall-3-1' => '',
                    'recall-3-2' => '',
                    'recall-3-3' => '',
                    'hold' => '',
                    'no-reply-hold' => '',

                ],
                'trash' => [
                    'no-reply' => '',
                    'out-of-coverage' => '',
                    'bad-contact-data' => '',
                    'duplicate' => '',
                    'did-not-order' => '',
                ],
            ],
            'brakeLogFolder'	=> true,
            'urlOrderAdd'	=> 'https://superdeal.retailcrm.ru/api/v5/orders/create',
            'urlOrderInfo'	=> 'https://superdeal.retailcrm.ru/api/v5/orders/statuses',
        ],
    ],

    23923 => [
        'terraleadsApiKey'			=> 'ee1379d131da192dcb6bd163d0d5599a',
        'retailApiKey' 				=> 'gVZClFVaGuwR465iKYHTcIf5essqdSjD',

        'offers' => [
            3088 => [
                'offerId' 			=> 2019117,
                'sourceSource'		=> 'terraleads',
                'countryIso' 		=> 'IT',
                'sourceCampaign'	=> function ($model, $config) {
                    return $model->web_id;
                },

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3089 => [
                'offerId' 			=> 2019118,
                'sourceSource'		=> 'terraleads',
                'countryIso' 		=> 'IT',
                'sourceCampaign'	=> function ($model, $config) {
                    return $model->web_id;
                },

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
        ],
        'configs' => [
            'statuses' => [
                'confirm'	=> [
                    'approve' => '',
                    'approved-2' => '',
                    'dapsidesaccepted' => '',
                    'dapsidesreceived' => '',
                    'dapsidesconfirmed' => '',
                    'dapsidesnotconfirmed' => '',
                ],
                'reject' => [
                    'reject' => '',
                    'bought-elsewhere' => '',
                    'rejected' => '',
                    'rejected-after-approve' => '',
                    'not-interested' => '',
                    'not-ready' => '',
                    'travels' => '',
                    'will-call-back' => '',
                    'rejected-in-call-center' => '',
                    'another-city' => '',

                ],
                'expect' => [
                    'new' => '',
                    'test' => '',
                    'nedozvon' => '',
                ],
                'trash' => [
                    'wrongdata' => '',
                    'trash' => '',
                ],
            ],
            'brakeLogFolder'	=> true,
            'urlOrderAdd'	=> 'https://adskill.retailcrm.ru/api/v5/orders/create',
            'urlOrderInfo'	=> 'https://adskill.retailcrm.ru/api/v5/orders/statuses',
        ],
    ],

    'configs' => [
        'bridgeRequestTimeout'			=> 60,
    ]
];

?>