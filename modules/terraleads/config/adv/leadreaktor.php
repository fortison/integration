<?php

/**
 * apidoc.leadreaktor.com
 *
 * julia@terraleads.com
 * terra2021
 */


return [
	
	43853 => [
		'terraleadsApiKey' => 'da0f44c27e68567ac0de60351cb0d72b',
		'api_key' 	       => '548858985b9d425ea8d9e3fc2c498514',
		
		'offers' => [
			0000 => [
				'goods_id' => '104212',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash' => [
					'trash' => 'Треш',
				],
				'reject' => [
					'cancelled' => 'Отмена',
				],
				'expect' => [
					'processing' => 'В ожидании',
				],
				'confirm' => [
					'bill' => 'Апрув',
				],
			],
			'brakeLogFolder' => true,
			'urlOrderAdd' 	 => 'https://api-new.leadreaktor.com/api/order/create.php',
			'urlOrderInfo' 	 => 'https://stats.leadreaktor.com/api/get-status',
		]
	]
];

?>