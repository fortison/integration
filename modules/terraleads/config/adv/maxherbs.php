<?php

/*
Method - Create Lead or Send Lead
HTTP Method - POST
API URL - https://crm.maxherbs.in/api/GetTerraLeadRecord
Format - JSON
REQUEST - {
"user_id":19630,
"data":{
"name":"Ishant",
"country":"IND",
"phone":"+917011370616",
"offer_id":3323
}
}
RESPONSE - {
"status": "ok",
"error": null,
"data": {
"id": "43224",
"status": "accept",
"comment": null
}
}
Method - Check Lead Status
HTTP Method - GET
API URL - https://crm.maxherbs.in/api/GetTerraLeadStatus
Format - JSON
REQUEST - {
"user_id":19630,
"data":{
"id":"43222"
}
}
RESPONSE - {
"status": "ok",
"error": null,
"data": {
"id": "43222",
"status": "accept",
"comment": null
}
}
 */

return [
	
	19630 => [
		'terraleadsApiKey'			=> 'd23eba99eb45563351967ee04c7b59b5',
		'user_id'					=> 19630,
		
		'offers' => [
			3323 => [
				'offer_id'	=> 3323,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'	=> '',
				'fail'	=> '',
			],
			'reject'	=> [
				'reject'	=> '',
			],
			'expect'	=> [
				'expect'	=> '',
				'accept'	=> '',
			],
			'confirm'	=> [
				'confirm'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://crm.maxherbs.in/api/GetTerraLeadRecord',
		'urlOrderInfo'		=> 'https://crm.maxherbs.in/api/GetTerraLeadStatus',
	],
];

?>