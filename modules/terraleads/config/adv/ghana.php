<?php

/*
 * https://crm-ghana.weirdstuff.io/api-doc/order-create
 * https://crm-ghana.weirdstuff.io/api-doc/order-status
 */

return [
	44671 => [
		'terraleadsApiKey'			=> '1e2c5a3a9b72e9844f04d55b126cec98',
		'apiKey'					=> '5YsomYaF893AsCr4GPiFozH8kpV6FpFvbbB91bM8AKUvJvg2PG',
		'campaign'					=> '3327',
		
		'offers' => [
			6006 => [ //Longjack XXXL - GH
				'code' => 'longjack-xxxl',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8998 => [ //Dr.Alfred - GH
				'code' => 'dr-alfred',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'trash' => '',
			],
			'reject' => [
				'rejected' => '',
			],
			'expect' => [
				'hold' => '',
			],
			'confirm' => [
				'approved' => '',
				'delivered' => '',
			],
		],
		
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://crm-ghana.weirdstuff.io/api/order-create',
		'urlOrderInfo'		=> 'https://crm-ghana.weirdstuff.io/api/order-status',
//		'statusEnabled'		=> false,
	],
];