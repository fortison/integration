<?php

return [
	
	33782 => [
		'terraleadsApiKey' => 'd8e1003bf3679e6263b349220a40a2b3',
		'api_key' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImE1MTc5MzA4NzJhOGQwMDI4OGVhYWYwNmY1YmUxMjViMzdmZWYwMTBmYjYwMDVkODQxOGNmYzY3NDQ2YzMyYmRkZjJiNjEyYWQwZjE1Y2M3In0.eyJhdWQiOiIxOSIsImp0aSI6ImE1MTc5MzA4NzJhOGQwMDI4OGVhYWYwNmY1YmUxMjViMzdmZWYwMTBmYjYwMDVkODQxOGNmYzY3NDQ2YzMyYmRkZjJiNjEyYWQwZjE1Y2M3IiwiaWF0IjoxNjI5ODkyNTA1LCJuYmYiOjE2Mjk4OTI1MDUsImV4cCI6MTY2MTQyODUwNSwic3ViIjoiNTYiLCJzY29wZXMiOltdfQ.YNS8MvISnTTjpZJJ1GEDwZhdaFSCqCps5IU6VIAO36EO0Wm-jZZnFZC_BJOk_WU7nXzvBy-pSewL15nXd9KRf25z687VGSn8tcIxZGPH_hWJSr_d4YDp2FN-9NrnUayeHgYOVhKJOYspekJ2EabAvDiJ982zmP-vV97US9pxrGMS_RbRsidqM2NL9fhRm7PVYbLwsxFc-_1HwgCCwr3hAg6KnNykZI3-76uSbjpIxcFTxbnAlogNegCM8V1BFz9NLD94HiVp2HM8eQCNcd7TVGsA-m8rO2EPnP8sCjAhxtVMHFVk4--FxXmweNkvjKRazmMAYNzz2XYj31J2Vf1fD1WJzww3DVFITXvH3RwnPHgdDDoGZpPOdA4fBC7jM3MqxjHv6yKIRwXvZm2Y0yKAc_I0HoX724w-4nlekQM99ivLVtIGvaybt1g_EKo-hsHlLxOZxb7I7DEN922DPkGwBNQ_e4F3JvYUAvtGlK3_uV2V1CE7EStwz39pls1Og1Fowgpm8TMXHygnhIdXo-dPWqEoRoej7JyJ6Z-8VVfIhn4dYAk6fQOBYb-ux6kMHL5_QpcG9lqXpejPRJyzGaZhoqBEc_EbJafaSiFwViwfKOkmTuu-zKmk3bOSfIQhqd2Q4aLPRuI2LWopl2nhfvK1G_8FnIR2cmasQmf5YVzhOgw',
		
		'offers' => [
			5063 => [//mold remover gel - gr
				
				'name' => 'MOLD REMOVER GEL',
				'model' => 'DSK5',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5064 => [//Beewax - gr
				
				'name' => 'Beewax',
				'model' => 'DSK2',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5065 => [//Daily shark - gr
				
				'name' => 'DAILYSHARK™ SUPER ACTIVE FOAM FOR DRY CLEANING',
				'model' => 'DS-1001',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
			
			],
			'reject' => [
				'cancelled' => 'Cancelled',
			],
			'expect' => [
				'processing' => 'Processing',
			],
			'confirm' => [
				'packaged_and_sent' => 'Packaged and Sent',
				'delivered' => 'Successfully delivered',
				'failed_delivery' => 'Failed delivery (cancelled)',
				'returned' => 'Failed delivery (cancelled)',

			],
		],
		'brakeLogFolder' => true,
		'urlOrderAdd' => 'https://newfulfillment.bigarena.eu/api/fulfillment/51/order',
		'urlOrderInfo' => 'https://newfulfillment.bigarena.eu/api/fulfillment/51/status',
	],
];

?>