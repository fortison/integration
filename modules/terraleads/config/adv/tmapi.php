<?php

/*
 *
*/

return [
    26094 => [
        'terraleadsApiKey'			=> 'bbb15464ed1feea7f1359ff10d77de08',
        'api'   	        	    => '3af3bfaba2fc10b12eb2758915c25d5f',

        'offers' => [
            3464 => [ // Spartin - IN 
                "campaignId" => 7,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],

        ],
    ],

    'configs' => [
        'statuses' => [
            'trash'	=> [
                'Duplicate'    	=> '',
                'Failed'    	=> '',
                '400'    	    => '',
                'Trash'    	    => '',
            ],
            'reject'	=> [
                'Rejected'      => '',
            ],
            'expect'	=> [
                'Accepted'    	=> '',
                'Pending'    	=> '',
            ],
            'confirm'	=> [
                'Approved'    	=> '',
            ],
        ],

        'brakeLogFolder'	=> true,
        'urlOrderAdd'		=> 'https://tmapi.online/api/lead2.php',
        'urlOrderInfo'		=> 'https://tmapi.online/api/status.php',
//		'statusEnabled'		=> false,
    ],
];