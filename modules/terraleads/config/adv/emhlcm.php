<?php

/*
 * В файлі, статуси надсилають в теру самостійно.
 *
 * http://connectcpa.net/terraleads/adv/adv-postback-status?systemUserId=25145&systemOrderId={clickId}&partnerStatus={status}&comment={comment}&postbackApiKey=002e827687a975a3c323231980308eda
 */

return [
	
	25145 => [
		'terraleadsApiKey'			=> '567aadd56f34a14065f0fc1508a490fc',
		'postbackApiKey'			=> '002e827687a975a3c323231980308eda',
		'channelId' 				=> 'qID8DH3',
		
		'offers' => [
			3315 => [
				'productId'	=> '74afa50a-66ec-4a61-887b-08d80b0caab2',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3343 => [
				'productId'	=> '5746f48d-c94a-483f-fff4-08d80e4169f1',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3342 => [ //Flexsteel - BA
				'productId'	=> '173b5129-34ba-4f62-fff5-08d80e4169f1',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5117 => [ //Flexsteel - RS
				'productId'	=> 'd4b315a7-0caf-4e21-2393-08d93a1d98c5',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5107 => [ //Flexsteel - ME
				'productId'	=> '2c2609f5-ef99-423d-1ea8-08d8c2aef203',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3403 => [ //Glucosine - BA
				'productId'	=> '7e24778d-3a71-4ab9-fff6-08d80e4169f1',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5136 => [ //Glucosine - RS
				'productId'	=> 'f0b29503-1e80-4464-87dd-08d8701c86e9',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			3966 => [ //Glucosine - ME
				'productId'	=> '9d826c20-a881-4238-d250-08d86afbcab6',
				'configs' => [
					'urlOrderAdd'		=> 'https://bluemoon-mne-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			3488 => [// BioCardin - BA
				'productId'	=> '230d4a59-320a-4a9b-1835-08d843442c67',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5106 => [// BioCardin - RS
				'productId'	=> 'cac9b05e-8b8f-4532-2394-08d93a1d98c5',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			3967 => [ //BioSlim - ME
				'productId'	=> 'd80d135c-75d2-4083-d24f-08d86afbcab6',
				
				'configs' => [
					'urlOrderAdd'		=> 'https://bluemoon-mne-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			4017 => [ //BioSlim - BA
				'productId'	=> 'AAF2BF16-0EF9-48B9-48F2-08D897867701',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5132 => [ //BioSlim - RS
				'productId'	=> 'afbd8353-ce03-4858-e0d8-08d8bfca9bc6',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			3965 => [ //Hypertin - ME
				'productId'	=> 'de322f77-5cb7-40f1-d24e-08d86afbcab6',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-mne-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5138 => [ //Hypertin - BA
				'productId'	=> '339bb339-0188-4050-44ca-08d86e979fd3',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-mne-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			4212 => [ //KardioMax - RS
				'productId'	=> 'B0541F3D-EAF1-4FD2-87DE-08D8701C86E9',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5131 => [ //Celufix - RS
				'productId'	=> 'cc3c1900-3b19-4a93-faf5-08d92cdc65c4',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5130 => [ //Celufix - BA
				'productId'	=> 'b8dd2294-d08d-46f4-e376-08d91ed48e74',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5137 => [ //Diabetpro - RS
				'productId'	=> '53eaa741-4b27-474c-a435-08d91ed3b6eb',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5153 => [ //Corelax - ME
				'productId'	=> 'a15a5743-169d-4755-c8bb-08d917316d60',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5152 => [ //Corelax - BA
				'productId'	=> 'd37fff4c-98c6-4239-d135-08d8d343ef47',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5151 => [ //Corelax - RS
				'productId'	=> '274a6c56-8c98-4f83-c0d8-08d8c79f8321',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5135 => [ //Herell - RS
				'productId'	=> '6b886d08-954d-4a1b-faf4-08d92cdc65c4',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5134 => [ //Herell - BA
				'productId'	=> 'f73148e9-ffb9-468a-e377-08d91ed48e74',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5133 => [ //Herell - ME
				'productId'	=> '65319079-d313-4bd9-f9a5-08d9317cf66a',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5145 => [ //PotentAxe - BA
				'productId'	=> 'e42f95e7-0b8f-44b8-d96e-08d9172ca412',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5146 => [ //PotentAxe - RS
				'productId'	=> '78370931-356b-428d-c912-08d900de0b91',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5147 => [ //PotentAxe - ME
				'productId'	=> '065efc90-dad9-48cf-c8bc-08d917316d60',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5140 => [ //Nefrofix - BA
				'productId'	=> 'c0766798-5c5d-466f-d132-08d8d343ef47',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5079 => [ //Nefrofix - RS
				'productId'	=> 'e27af4ae-a301-47b8-c0d7-08d8c79f8321',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5139 => [ //Nefrofix - ME
				'productId'	=> 'af233b4c-d842-4724-c8b8-08d917316d60',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5176 => [ //ProstatX - ME
				'productId'	=> '6f6ec80c-b675-4105-c8b9-08d917316d60',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5174 => [ //ProstatX - BA
				'productId'	=> '44bf1529-6334-45a9-d133-08d8d343ef47',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5175 => [ //ProstatX - RS
				'productId'	=> '6e8bba9b-dacd-4432-c0d9-08d8c79f8321',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5162 => [ //Relaxin - BA
				'productId'	=> '0349ebb0-f24a-4d44-d134-08d8d343ef47',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5167 => [ //Relaxin - ME
				'productId'	=> '107f7997-82c7-4c6b-c8ba-08d917316d60',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
			5166 => [ //Relaxin - RS.
				'productId'	=> 'bd9359e1-6b6d-43c0-c0da-08d8c79f8321',
				
				'configs' => [
					'urlOrderAdd'	=> 'https://bluemoon-srb-hub.ad-pro.info/affiliate/api/noea',
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'Trash'	=> '',
			],
			'reject'	=> [
				'Canceled'	=> '',
			],
			'expect'	=> [
				'Pending'	=> '',
			],
			'confirm'	=> [
				'Confirmed'	=> '',
				'Returned'	=> '',
				'Paid'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://bluemoon-hub.ad-pro.info/affiliate/api/noea',
		'urlOrderInfo'		=> '',
	],
];

?>