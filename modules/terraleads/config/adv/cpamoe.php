<?php

return [
	51452 => [
		'terraleadsApiKey'	=> '8e83ce58043c051d3368200be706dd98',
		'api_token'			=> '1325-28262b85c082845776124fa9b2df0897',
		
		'offers' => [
			8141 => [ //ManPlus XXXL - KE
				'offer' => 703,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8142 => [ //Trimphen Q - KE
				'offer' => 702,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8143 => [ //Arteris Plus - KE
				'offer' => 704,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	49391 => [
		'terraleadsApiKey'	=> '73ee97a0fa36456f0bd6d44962415633',
		'api_token'			=> '879-e9a906ab15f841c3e474fb210bad8e7c',
		
		'offers' => [
			6722 => [ // Fast Relief Spray - DZ
				'offer' => 481,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6731 => [ // Neocapsicum Creme - DZ
				'offer' => 483,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2323 => [
				'offer' => 479,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	50235 => [
		'terraleadsApiKey'	=> 'dbd987b6ad5df5c1a33478776883c9b4',
		'api_token'			=> '1114-00ca020309c7b0a13480eca4f6c578f9',
		
		'offers' => [
			7196 => [ //SlimfleX - BA
				'offer' => 602,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7197 => [ //SlimfleX - RS
				'offer' => 603,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7198 => [ //Prostamixin - BA
				'offer' => 604,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7199 => [ //Prostamixin - RS
				'offer' => 605,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7200 => [ //ZglofleX - BA
				'offer' => 608,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7201 => [ //ZglofleX - RS
				'offer' => 609,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7205 => [ //HemofleX - BA
				'offer' => 606,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7206 => [ //HemofleX - RS
				'offer' => 607,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	58305 => [
		'terraleadsApiKey'	=> 'a0941a7280f192ed3a90edbcc78a01e9',
		'api_token'			=> '1651-d91bfb530b5243103ecc79db3b970939',
		
		'offers' => [
			9162 => [ // VitaCardio Plus - PL
				'offer' => 914,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9398 => [ // VitaCardio Plus - PL (free)
				'offer' => 974,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'11'	=> '',
				'12'	=> '',
			],
			'reject'	=> [
				'5'	=> '',
			],
			'expect'	=> [
				'1'	=> '',
				'2'	=> '',
				'3'	=> '',
				'4'	=> '',
			],
			'confirm'	=> [
				'6'	=> '',
				'7'	=> '',
				'8'	=> '',
				'9'	=> '',
				'10'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://api.cpa.moe/ext/add.json',
		'urlOrderInfo'		=> 'https://api.cpa.moe/ext/list.json',
	],
];

?>