<?php

return [
    27484 => [
        'terraleadsApiKey'			=> '4c89ab0fa0b2f14e4e13ca4d198bbd14',
        'postbackApiKey'			=> '4c89ab0fa0b2f14e4e13ca4d198bbd15',

        'offers' => [
            3806 => [
                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
        ],
    ],

    'configs' => [
        'statuses' => [
            'trash'	=> [
                'trash'		=> '',
            ],
            'reject'	=> [
                'reject'	=> '',
            ],
            'expect'	=> [
                'expect'	=> '',
            ],
            'confirm'	=> [
                'confirm'	=> '',
            ],
        ],
        'brakeLogFolder'	=> true,
        'urlOrderAdd'		=> 'https://livenaturecure.secure.force.com/services/apexrest/Lead/Lead',
        'urlOrderInfo'		=> '',
    ],
];

?>