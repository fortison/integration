<?php

/*
 *
 *	https://docs.google.com/spreadsheets/d/14B6PdAxdqrKaomVd7lLQl7G0glKfMmFQh7mlBaGMUKM/edit?usp=sharing
 *
 * http://connectcpa.net/terraleads/adv/adv-postback-status?systemUserId=25210&systemOrderId={sub1}&partnerStatus={status}&comment={comment}&postbackApiKey=5bff1a5386f3a67fd7fade2222f6fe22
 */

return [
	26683 => [
		'terraleadsApiKey'			=> '43bb44e7139eaf50d59ea8b56b82e1f6',
		'postbackApiKey'			=> '6bff1a5386f3a67fd7fade2222f6fe22',
//		'publisher_id' 				=> 'terraleads',
		'token' 					=> '4c24536a4c7c325743526d25636c7b47',
		
		'offers' => [
			3589 => [ // Varicobooster - MX
				'flow_hash'  => 'mJXk',
				'landing' => 'http://cieson.com/mJXk',
				
				'configs' => [
					'brakeLogFolder'  => true,
				],
			],
			3590 => [ // Hondrostrong - MX
				'flow_hash'  => 'HZXk',
				'landing' => 'http://szaren.com/HZXk',
				
				'configs' => [
					'brakeLogFolder'  => true,
				],
			],
		],
	],
	25210 => [
		'terraleadsApiKey'			=> '5bff1a5386f3a67fd7fade1230f6fe96',
		'postbackApiKey'			=> '5bff1a5386f3a67fd7fade2222f6fe22',
//		'publisher_id' 				=> 'terraleads',
		'token' 					=> '4c24536a4c7c325743526d25636c7b47',
		
		'offers' => [
			3569 => [ // Wellcard - CO
				'flow_hash'  => 'wIgk',
				'landing' => 'http://silaconen.com/wIgk',
				
				'configs' => [
					'brakeLogFolder'  => true,
				],
			],
			3570 => [ // Suganorm - CO
				'flow_hash'  => '4Wgk',
				'landing' => 'http://silaconen.com/4Wgk',
				
				'configs' => [
					'brakeLogFolder'  => true,
				],
			],
			3495 => [ // EveryDaySlim - CO
				'flow_hash'  => 'sogk',
				'landing' => 'http://silaconen.com/sogk',
				
				'configs' => [
					'brakeLogFolder'  => true,
				],
			],
			3496 => [ // Erogan - CO potency
				'flow_hash'  => 'IYgk',
				'landing' => 'http://silaconen.com/IYgk',
				
				'configs' => [
					'brakeLogFolder'  => true,
				],
			],
			3402 => [ // Gelmixin - CO
				'flow_hash'  => 'lGNk',
				'landing' => 'http://silaconen.com/lGNk',
				
				'configs' => [
					'brakeLogFolder'  => true,
				],
			],
			3327 => [
				'flow_hash'	=> 'VJ1k',
				'landing' => 'http://preblogs.com/pero/co/laravista/?TID=5EE8C98D005B7A38D101FAA3&host=norvanin.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3326 => [
				'flow_hash'	=> 'kM1k',
				'landing' => 'http://norvanin.com/kM1k',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3399 => [ //Varicobooster - CO
				'flow_hash'	=> 'nGNk',
				'landing' => 'http://silaconen.com/nGNk',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3401 => [ // Collamask - CO	
				'flow_hash'	=> '0BNk',
				'landing' => 'http://silaconen.com/0BNk',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3400 => [ // Tinedol - CO
				'flow_hash'	=> 'XBNk',
				'landing' => 'http://silaconen.com/XBNk',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'	=> '',
			],
			'reject'	=> [
				'reject'	=> '',
			],
			'expect'	=> [
				'expect'	=> '',
			],
			'confirm'	=> [
				'confirm'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'http://leadbit.com/api/new-order/',
		'urlOrderInfo'		=> '',
	],
];

?>