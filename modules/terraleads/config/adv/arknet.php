<?php

return [
	64683 => [
		'terraleadsApiKey'	=> '9243d2c67d942fe52628877a86868706',
		'api_token'			=> '86-6a204114142348da46cdc8c62bcc42d8',
		
		'offers' => [
			8872 => [ // Artrolux Pro - IT
				'offer' => 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8873 => [ // EromaxZ - IT
				'offer' => 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8874 => [ // Nanolift - IT
				'offer' => 5,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8875 => [ // Farmacin Detox - IT
				'offer' => 150,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8876 => [ // Proctolax - IT
				'offer' => 152,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8877 => [ // DM-Norm Caps - IT
				'offer' => 321,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8878 => [ // Slimagic Caps - IT
				'offer' => 185,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8879 => [ // Alco Win - IT
				'offer' => 319,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8880 => [ // Slimoxil - IT
				'offer' => 186,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8881 => [ // Dermasept - IT
				'offer' => 148,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2323 => [ // Test
				'offer' => 100,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'11'	=> '',
				'12'	=> '',
			],
			'reject'	=> [
				'5'	=> '',
			],
			'expect'	=> [
				'1'	=> '',
				'2'	=> '',
				'3'	=> '',
				'4'	=> '',
			],
			'confirm'	=> [
				'6'	=> '',
				'7'	=> '',
				'8'	=> '',
				'9'	=> '',
				'10'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://arknet.group/api/ext/add.json',
		'urlOrderInfo'		=> 'https://arknet.group/api/ext/list.json',
	],
];

?>