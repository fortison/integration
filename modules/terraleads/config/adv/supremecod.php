<?php
/*
 * https://documenter.getpostman.com/view/2770030/2s93sZ6u2c
 */
return [
	
	
	// http://connectcpa.net/terraleads/adv/adv-postback-status?&systemOrderId={source}&partnerStatus={status}&systemUserId=53837&postbackApiKey=faaa7ba90dbafb4b5b4db2fb8030f60d
	53837  => [
		'terraleadsApiKey'			=> 'faaa7ba90dbafb4b5b4db2fb8030f50f',
		'postbackApiKey'			=> 'faaa7ba90dbafb4b5b4db2fb8030f60d',
		'token'						=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0ciI6IjQiLCJhYyI6IjMiLCJvaSI6IjY1IiwidWkiOiI2MCIsIkFQSV9USU1FIjoxNjkwMzcwNjgyfQ.-O5T3Kzffo6tqZ9ou1ycbgvIdT81JLXxVWr_qI6EuOk',
		
		'offers' => [
			2323 => [
				'product_id' 	=> '1',
				'area_code' 	=> '+357', // первые цифры номера (код региона)
				'referrer_url' 	=> 'https://website.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'Wrong Number'	=> '',
				'No Answer'		=> '',
			],
			'reject'	=> [
				'Returned'	=> '',
				'Not Interested'	=> '',
				'Call Again'		=> '',
			],
			'expect'	=> [
				'New'	=> '',
			],
			'confirm'	=> [
				'Confirmed'	=> '',
				'Approved'	=> '',
				'Shipped'	=> '',
				'Delivered'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://api.supremecod.com/v1/affiliates/lead/create',
	],
];

?>