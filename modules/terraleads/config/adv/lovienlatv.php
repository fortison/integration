<?php

return [
	
	37111 => [
		'terraleadsApiKey'			=> '8c6cca2f3d1ff8fe5d0788109c60d8da',
		'api_key' 					=> '65abc47f159ce7cf567bbd7d812a53',
		
		
		'offers' => [
			8611 => [ //MiniHeat - MX
				'offer_id'	=> '30901',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,

				],
			],
			8610 => [ //Aspiralizer - MX
				'offer_id'	=> '30714',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,

				],
			],
			8609 => [ //Blend - MX
				'offer_id'	=> '30712',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,

				],
			],
			8608 => [ //Brush it - MX
				'offer_id'	=> '30711',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,

				],
			],
			8593 => [ //Shineless - MX
				'offer_id'	=> '30802',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,

				],
			],
			6355 => [ //Full dash - MX
				'offer_id'	=> '30477',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,

				],
			],
			7443 => [ //Linkcam - MX
				'offer_id'	=> '30713',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,

				],
			],
			7335 => [ //Washy - MX
				'offer_id'	=> '30520',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,

				],
			],
			6977 => [ //Freezy Wind - MX
				'offer_id'	=> '30517',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,

				],
			],
			6930 => [ //Pulse Pro - MX
				'offer_id'	=> '30518',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,

				],
			],
			6341 => [ //Pearlyface - MX
				'offer_id'	=> '30466',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,

				],
			],
			6355 => [ //Full dash - MX
				'offer_id'	=> '30477',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,

				],
			],
			6339 => [ //Enerlumia - MX
				'offer_id'	=> '30488',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,

				],
			],
			6758 => [ //Comfy Legs - MX
				'offer_id'	=> '30499',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,

				],
			],
			6338 => [ //Intuifit - MX
				'offer_id'	=> '30511',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,

				],
			],
			6212 => [ //Solar Light - MX
				'offer_id'	=> '30522',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,

				],
			],
			5901 => [ //Freshbot pro - MX
				'offer_id'	=> '30444',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,

				],
			],
			5160 => [ //SmartView - MX
				'offer_id'	=> '30411',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,

				],
			],
			5159 => [ //Hyalukare caps - MX
				'offer_id'	=> '30422',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5158 => [ //Hyalukare - MX
				'offer_id'	=> '30422',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5756 => [ //Biotsu - MX
				'offer_id'	=> '30433',
				'base_url'	=> 'mx-smartview.treasury-goods.com',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash' => '',
			],
			'reject'	=> [
				'cancelled' => '',
			],
			'expect'	=> [
				'hold' => '',
			],
			'confirm'	=> [
				'confirmed'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://api.lovienlatv.com/post.php',
		'urlOrderInfo'		=> 'https://api.lovienlatv.com/orders/api/get.php',
	],
];

?>