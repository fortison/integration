<?php

/*
 * У файлі
 *
 * http://connectcpa.doc/terraleads/adv/adv-postback-status?systemOrderId=2&partnerStatus=trash&comment={comment}&postbackApiKey=57819800a3c322e827687a9750308eda
 */

return [
	
	18373 => [
		'terraleadsApiKey'			=> 'bfffe37203dcfd4c352dfd65167c9b38',
		'postbackApiKey'			=> '57819800a3c322e827687a9750308eda',
		'api_key' 					=> 'ebc7a14949761e6916550a60bcbab67c',
		
		'offers' => [
			4743 => [
				'offer_id'	=> 610005,
				
				'configs' => [
					'brakeLogFolder'	=> true,
					'urlOrderAdd'		=> 'https://affnetonline.com/api/v3/order/create',

				],
			],
			5693 => [
				'offer_id'	=> 7500,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4961 => [
				'offer_id'	=> 6700,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2401 => [
				'offer_id'	=> 1001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2632 => [
				'offer_id'	=> 1005,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2948 => [
				'offer_id'	=> 1010,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3005 => [
				'offer_id' => 6000,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],	
			3425 => [ //Venuslim - MA
				'offer_id' => 6500,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5007 => [ //
				'offer_id' => 6600,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],								
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'		=> '',
			],
			'reject'	=> [
				'reject'	=> '',
			],
			'expect'	=> [
				'expect'	=> '',
			],
			'confirm'	=> [
				'confirm'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://affnetonline.com/api/v3/order/create',
		'urlOrderInfo'		=> '',
	],
];

?>