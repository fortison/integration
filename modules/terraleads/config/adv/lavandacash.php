<?php

/*
	В чате
 */

return [
	23478 => [
		'terraleadsApiKey'			=> '5ebf54d9d51208b36b50440a13899b23',
		'web_id'					=> 'N12QVOPcyl',
		
		'offers' => [
			3069 => [
				'product_id' => '6',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3322 => [
				'product_id' => '4',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3336 => [
				'product_id' => '5',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3624 => [
				'product_id' => '10',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3645 => [
				'product_id' => '8',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3647 => [
				'product_id' => '9',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash' 		=> '',
				'3' => 'битый/дубль',
			],
			'reject'	=> [
				'reject'    => '',
				'5'    => 'отказ',
			],
			'expect'	=> [
				'expect'   		 => '',
				'1'   	 => 'лид успешно создан',
				'2'   	 => 'лид передан на обработку в коллцентр',
				'4'  	 => 'вторичная обработка (не удалось сразу дозвониться до клиента либо клиент просил перезвонить позже)',
			],
			'confirm'	=> [
				'confirm'    	=> '',
				'6'    		=> 'заказ товара',
			],
		],
		
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://www.lavanda.cash/data_processor.php',
		'urlOrderInfo'		=> 'https://www.lavanda.cash/lead_info.php',
//		'statusEnabled'		=> false,
	],
];