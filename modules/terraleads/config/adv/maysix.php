<?php

/*
https://mlrmngt.com/
Login: yehor@terraleads.com
Pass: T3rr@L3@ds
 */

return [
	
	45897 => [
		'affiliate_id' 		=> 465,
		'terraleadsApiKey' 	=> 'ebc7c8731f7abfff63bd102ef029f65a',
		
		'offers' => [
			6309 => [ //Power Keto - RO
				'offer_link'	=> 'https://leadmarkings.com/?a=465&c=5974',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6426 => [ //Keto BHB Diet - PH
				'offer_link'	=> 'https://leadmarkings.com/?a=465&c=6059',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'	=> '',
				'spam'	=> 'trash lead',
			],
			'reject'	=> [
				'reject'	=> 'canceled lead',
				'cancelled'	=> 'canceled lead',
			],
			'expect'	=> [
				'in_progress' => 'still trying to reach out to the customer',
				'new'	 => 'call center didn’t reach the customer yet',
				'hold'	 => 'hold',
			],
			'confirm'	=> [
				'at_currier'	=> 'confirmed sale, the order being shipped',
				'confirmed'	=> 'confirmed',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> '',
		'urlOrderInfo'		=> 'https://hc8rv8629c.execute-api.eu-central-1.amazonaws.com/cod/lead',
	],
];

?>