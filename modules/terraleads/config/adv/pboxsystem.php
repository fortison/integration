<?php

/**
 * General:
 * Endpoint: https://publicapi.pboxsystem.pl
 * Api key: *7@e8k}q)td&fo]yrn`whk|.hxz-Un^~|rFk)fz[3H9oZ?|@l;ER?Df$LdN@
 *
 * Parameters:
 *
 * Required:
 * firstname
 * lastname
 * phone
 *
 * productName / (pleasse use the product name not a product ID, make sure you use those product names, written exaclty this way:
 * LaMedi
 * Prostazolin
 * holdaflex
 * fatoslimin
 * densamox)
 *
 * price
 * externaLeadId / (your lead id)
 * externalPartnerId / (your id in our system - please use "partnerid10cps")
 * externalPartnerSubId / (your publishers/webmasters id in your system)
 *
 * Optional:
 * source
 * country
 * email
 * street
 * postalCode
 * city
 *
 * Status:
 * approved - order confirmed
 * rejected - order not confirmed
 * trash - wrong number, wrong data, voice maschine, number not avaliable, no answer
 * pending - in progress
 */

return [
	
	43955 => [
		'terraleadsApiKey' 	=> '2130735c326b47ab31bd5a975afa371c',
		'postbackApiKey'	=> '0b4fc0c3a4051e2fa97903cc5beecbb6',
		'apiKey'			=> '*7@e8k}q)td&fo]yrn`whk|.hxz-Un^~|rFk)fz[3H9oZ?|@l;ER?Df$LdN@',
		'externalPartnerId' => 'partnerid10cps',
		
		'offers' => [
			7080 => [//Fatoslimin - PL
				'productName' 	=> 'fatoslimin',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7081 => [//Dermonte - PL
				'productName' 	=> 'dermonte',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7082 => [//La Medi - PL
				'productName' 	=> 'LaMedi',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7083 => [//Prostazolin - PL
				'productName' 	=> 'Prostazolin',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7079 => [//Holdaflex - PL1
				'productName' 	=> 'holdaflex',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'trash' => '',
			],
			'reject' => [
				'rejected' => '',
			],
			'expect' => [
				'pending' => '',
			],
			'confirm' => [
				'approved' => '',
			],
		],
		'brakeLogFolder' => true,
		'urlOrderAdd' => 'https://publicapi.pboxsystem.pl/public/api/public/partner4/addlead',
	],
];

?>