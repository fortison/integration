<?php
/*
	В чате

	http://connectcpa.net/terraleads/adv/adv-postback-status?systemUserId=27384&partnerStatus={status}&systemOrderId={clickId}&comment={status}&postbackApiKey=4b6ecf290d1a729d099d3429bd4f04b9
 */

return [
    27384 => [
        'terraleadsApiKey'			=> '4b6ecf290d1a729d099d3429bd4f04b0',
        'postbackApiKey'			=> '4b6ecf290d1a729d099d3429bd4f04b9',
        'apikey' 					=> '12ab1f62d4204b8e86f2',

        'offers' => [
            3744 => [

                'affid' => 'terraleads',
                'transactionid' => 123,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
        ],
    ],

    'configs' => [
        'statuses' => [
            'trash'	=> [
                'trash' 		    => '',
            ],
            'reject'	=> [
                'reject'            => '',
            ],
            'expect'	=> [
                'expect'   		    => '',
            ],
            'confirm'	=> [
                'confirm'       	=> '',

            ],
        ],

        'brakeLogFolder'	=> true,
        'urlOrderAdd'		=> 'https://trafficads.mobi/API/v1/',
    ],
];
