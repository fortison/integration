<?php

/*
---------------------------
ADD LEAD
---------------------------

URL: https://azurecrm.pl/api/add_lp_phone/PHONE_NUMBER_HERE/COUNTRY_ID_HERE/PRODUCT_ID_HERE/

All variables which are passed by the URL are in form mentioned above.
Additionally there should be passed 3 variables by POST:
- first_name
- last_name
- external_partners_id - must be number "2"


Example usage of API in Javascript:

$.ajax({
    type: "POST",
    url: "http://azurecrm.pl/api/add_lp_phone/666555444/4/23",
    dataType : "json",
    data: {
        first_name: 'Michael',
        last_name: 'Johnson',
        external_partners_id: 3
    },
    success: function (ret) {},
    complete: function () {},
    error: function (jqXHR, errorText, errorThrown) {}
});

---------------------------
GET STATUS
---------------------------

https://azurecrm.pl/api/check_external_lead
lead_id
company_id
 */

return [
	24607 => [
		'terraleadsApiKey'			=> '05bb797db59b65ddb1fcee38f95b8556',
        'postbackApiKey'			=> '05bb797db59b65ddb1fcee38f95b8557',
		'external_partners_id'		=> 2,
		'api_key'					=> '5ed4fad3672c7',
		
		
		'offers' => [
			8398 => [
				'product_id' => '22', //Retoxin - DE
                'country_code' => '3',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8394 => [
				'product_id' => '20', //Skinatrin - DE
                'country_code' => '3',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8386 => [
				'product_id' => '54', //Redimin - DE
                'country_code' => '3',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8370 => [
				'product_id' => '46', //Eroprostin - DE
                'country_code' => '3',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8362 => [
				'product_id' => '42', //Glikotril - DE
                'country_code' => '3',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8390 => [
				'product_id' => '63', //Dermo-Pro - DE
                'country_code' => '3',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8382 => [
				'product_id' => '79', //Visospect - PL	
                'country_code' => '3',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8350 => [
				'product_id' => '61', //Colladiox Pro - DE
                'country_code' => '3',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7866 => [
				'product_id' => '61', //Colladiox Pro - CZ
                'country_code' => '2',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8723 => [
				'product_id' => '78', //Hemoroxin - PL
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8724 => [
				'product_id' => '78', //Hemoroxin - RO
                'country_code' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8725 => [
				'product_id' => '78', //Hemoroxin - CZ
                'country_code' => '2',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8726 => [
				'product_id' => '78', //Hemoroxin - DE
                'country_code' => '3',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8727 => [
				'product_id' => '78', //Hemoroxin -SK
                'country_code' => '7',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8366 => [
				'product_id' => '47', //Ultra Cardiox - DE111
                'country_code' => '3',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8354 => [
				'product_id' => '52', //Acustal - DE
                'country_code' => '3',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8353 => [
				'product_id' => '52', //Acustal - SK
                'country_code' => '7',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8358 => [
				'product_id' => '60', //Fleboxin - DE
                'country_code' => '3',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8357 => [
				'product_id' => '60', //Fleboxin - SK
                'country_code' => '7',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8360 => [
				'product_id' => '60', //Fleboxin - SK (free price)
                'country_code' => '7',
                'isfree' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8125 => [
				'product_id' => '46', //Eroprostin - RO (free price)1
                'country_code' => '1',
                'isfree' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8019 => [
				'product_id' => '46', //Eroprostin - PL (free price)
                'country_code' => '4',
                'isfree' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8016 => [
				'product_id' => '61', //Colladiox Pro - PL (free price)
                'country_code' => '4',
                'isfree' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8247 => [
				'product_id' => '23', //Moring Slim - PL (free price)
                'country_code' => '4',
                'isfree' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8017 => [
				'product_id' => '54', //Redimin - PL (free price)
                'country_code' => '4',
                'isfree' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8020 => [
				'product_id' => '19', //Hairstim - PL (free price)
                'country_code' => '4',
                'isfree' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7494 => [
				'product_id' => '63', //Dermo-Pro - PL
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7437 => [
				'product_id' => '60', //Fleboxin - RO
                'country_code' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7436 => [
				'product_id' => '60', //Fleboxin - PL
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7435 => [
				'product_id' => '52', //Acustal - PL
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7432 => [
				'product_id' => '61', //Colladiox Pro - PL	
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7433 => [
				'product_id' => '61', //Colladiox Pro - RO1	
                'country_code' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6915 => [
				'product_id' => '54', //Redimin - PL	
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6916 => [
				'product_id' => '54', //Redimin - ro	
                'country_code' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6914 => [
				'product_id' => '47', //Ultra Cardiox - RO (free price)
                'country_code' => '1',
                'isfree' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6822 => [
				'product_id' => '47', //Ultra Cardiox - PL (free price)
                'country_code' => '4',
                'isfree' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6824 => [
				'product_id' => '42', //Glikotril - CZ (free price)
                'country_code' => '2',
                'isfree' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6553 => [
				'product_id' => '47', //Ultra Cardiox - RO
                'country_code' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6550 => [
				'product_id' => '47', //Ultra Cardiox - PL	
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6211 => [
				'product_id' => '42', //Glikotril - IT	
                'country_code' => '6',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6210 => [
				'product_id' => '42', //Glikotril - PL	
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6208 => [
				'product_id' => '42', //Glikotril - ro	
                'country_code' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7869 => [
				'product_id' => '42', //Glikotril - cz1	
                'country_code' => '2',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6125 => [
				'product_id' => '46', //Eroprostin - IT	
                'country_code' => '6',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6018 => [
				'product_id' => '46', //Eroprostin - PL	
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6016 => [
				'product_id' => '46', //Eroprostin - ro	
                'country_code' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6017 => [
				'product_id' => '46', //Eroprostin - cz	
                'country_code' => '2',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6019 => [
				'product_id' => '46', //Eroprostin - IT
                'country_code' => '6',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5911 => [
				'product_id' => '79', //Visospect - PL	
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5910 => [
				'product_id' => '79', //Visospect - RO	
                'country_code' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5909 => [
				'product_id' => '79', //Visospect - CZ	
                'country_code' => '2',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5908 => [
				'product_id' => '79', //Visospect - IT	
                'country_code' => '6',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5907 => [
				'product_id' => '38', //Gua Sha Mask - PL	
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5906 => [
				'product_id' => '38', //Gua Sha Mask - CZ	
                'country_code' => '2',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5905 => [
				'product_id' => '38', //Gua Sha Mask - RO	
                'country_code' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5904 => [
				'product_id' => '38', //Gua Sha Mask - IT	
                'country_code' => '6',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5039 => [
				'product_id' => '35', //Bull Run Muscles - PL	
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5038 => [
				'product_id' => '33', //Bull Run Ero - PL	
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5040 => [
				'product_id' => '34', //Ultra Cardio+ - PL	
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5235 => [
				'product_id' => '34', //Ultra Cardio+ - CZ	
                'country_code' => '2',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3310 => [
				'product_id' => '23', //Moring Slim - PL	
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5930 => [
				'product_id' => '44', //Perlaslim - PL	
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3843 => [
				'product_id' => '23', //Moring Slim - DE	
                'country_code' => '3',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3893 => [
				'product_id' => '19', //Hairstim - DE
                'country_code' => '3',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5292 => [
				'product_id' => '19', //Hairstim - RO
                'country_code' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3311 => [
				'product_id' => '22', //Retoxin - PL
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3312 => [
				'product_id' => '31', //Yoslimin - PL
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3313 => [
				'product_id' => '19', //Hairstim - PL
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3314 => [
				'product_id' => '20', //Skinatrin - PL
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5293 => [
				'product_id' => '20', //Skinatrin - RO
                'country_code' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5207 => [
				'product_id' => '20', //Skinatrin - CZ
                'country_code' => '2',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3973 => [
				'product_id' => '19', //Hairstim - CZ
                'country_code' => '2',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3972 => [
				'product_id' => '23', //Moring Slim - CZ	
                'country_code' => '2',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5291 => [
				'product_id' => '23', //Moring Slim - RO	
                'country_code' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2905 => [
				'product_id' => '32', //Cardio NRJ - PL
                'country_code' => '4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2811 => [
				'product_id' => '32', //Cardio NRJ - CZ
                'country_code' => '2',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5198 => [
				'product_id' => '22', //Retoxin Free Price - PL
                'country_code' => '4',
                'isfree' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6622 => [
				'product_id' => '42', //Glikotril - RO (free price)
                'country_code' => '1',
                'isfree' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6621 => [
				'product_id' => '42', //Glikotril - PL (free price)
                'country_code' => '4',
                'isfree' => '1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'4' => 'trash',
			],
			'reject'	=> [
				'2' => 'rejected',
			],
			'expect'	=> [
				'0' => 'pending',
			],
			'confirm'	=> [
				'3' => 'collected',
			],
		],
		
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://azurecrm20.pl/api/add_lp_phone',
//		'urlOrderInfo'		=> 'https://azurecrm.pl/api/check_external_lead',
//		'statusEnabled'		=> false,
	],
];