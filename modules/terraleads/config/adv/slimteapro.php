<?php

//http://connectcpa.doc/terraleads/adv/adv-postback-status?systemUserId=32117
//&systemOrderId={click_id}&partnerStatus=trash&postbackApiKey=6e2654e42151216b9ebeb9b1a94e6729


return [
    32117 => [
        'terraleadsApiKey'			=> '6e2654e42151216b9ebeb9b1a94e6729',
        'postbackApiKey'			=> '6e2654e42151216b9ebeb9b1a94e6728',
        'api_key' 			    	=> '03793CBD-8986-40CF-B0B7-CC3B3BEC5087',

        'offers' => [
            4629 => [ //Slim Tea Pro - KE
				'offerId' => 1,
                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
             4770 => [ //Prostaffect - KE
				 'offerId' => 2,
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
             4771 => [ //EffectEro - KE
				 'offerId' => 3,
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4961 => [ //EffectEro - MA
                 'offerId' => 3,
                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
        ],
    ],
    'configs' => [
        'statuses' => [
            'trash'	=> [
                'trash'		=> '',
            ],
            'reject'	=> [
                'reject'	=> '',
            ],
            'expect'	=> [
                'expect'	=> '',
            ],
            'confirm'	=> [
                'confirm'	=> '',
            ],
        ],
        'brakeLogFolder'	=> true,
        'urlOrderAdd'		=> 'https://www.slimteapro.com/api/orders',
        'urlOrderInfo'		=> '',
    ],
];

?>