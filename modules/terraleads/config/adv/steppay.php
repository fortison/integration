<?php

/*
 * https://terraleads.com/acp/docs
 */

return [
	
	2 => [
		'terraleadsApiKey'			=> 'a78c01096ab394b434d8a046bdb9976b',	# в тері
		'terraleadsUserId'			=> 16578,								# в тері
		'apiKey' 					=> 'a78c01096ab394b434d8a046bdb9976b',	# в стептей
		
		'offers' => [
			1 => [															# в стептей
				'offerId' => 2131,											# в тері

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2 => [															# в стептей
				'offerId' => 2059,											# в тері

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3 => [															# в стептей
				'offerId' => 1429,											# в тері

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4 => [															# в стептей
				'offerId' => 1502,											# в тері

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5 => [															# в стептей
				'offerId' => 2031,											# в тері

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6 => [															# в стептей
				'offerId' => 1365,											# в тері

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7 => [															# в стептей
				'offerId' => 2350,											# в тері

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8 => [															# в стептей
				'offerId' => 2341,											# в тері

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9 => [															# в стептей
				'offerId' => 2348,											# в тері

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			10 => [															# в стептей
				'offerId' => 2375,											# в тері

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			11 => [															# в стептей
				'offerId' => 2483,											# в тері

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			12 => [															# в стептей
				'offerId' => 2563,											# в тері

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			13 => [															# в стептей
				'offerId' => 2344,											# в тері

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			14 => [															# в стептей
				'offerId' => 2536,											# в тері

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			15 => [															# в стептей
				'offerId' => 2317,											# в тері

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			16 => [															# в стептей
				'offerId' => 2557,											# в тері

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			17 => [															# в стептей
				'offerId' => 2555,											# в тері

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			18 => [															# в стептей
				'offerId' => 1301,											# в тері
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	9815 => [
		'terraleadsApiKey'			=> 'a78c01096ab394b434d8a046bdb9976b',
		'terraleadsUserId'			=> 16578,
		'apiKey' 					=> '01f995edbc256fac88ee6730c8d056f9',
		
		'offers' => [
			1 => [
				'offerId' => 2224,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'		=> [
				'trash'		=> '',
			],
			'reject'	=> [
				'reject'	=> '',
			],
			'expect'	=> [
				'expect'	=> '',
			],
			'confirm'	=> [
				'confirm'	=> '',
			],
		],
		'brakeLogFolder'			=> true,
		'urlOrderAdd'				=> 'http://tl-api.com/api/lead/create',
		'urlOrderInfo'				=> 'http://tl-api.com/api/lead/status',
		'urlTerraleadsOrderUpdate' 	=> 'http://steppay.biz/api/lead/update',
	],
];

?>
