<?php

/*
* https://healthpower.pro/help/api.php#ext
*/

return [
	
	24172 => [
		'terraleadsApiKey'			=> '288c21816aea03c21b86c52bf02722ab',
		'key' 						=> 'f55a79f39012caa3d5dc2f67d1785847',
		'user'						=> '81',
		
		'offers' => [
			2561 => [
				'offer'	=> 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3180 => [
				'offer'	=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3153  => [
				'offer'	=> 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3154  => [
				'offer'	=> 6,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3185  => [
				'offer'	=> 8,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3186   => [
				'offer'	=> 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3296   => [
				'offer'	=> 9,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'	=> '',
			],
			'reject'	=> [
				'cancel'	=> '',
			],
			'expect'	=> [
				'wait'	=> '',
				'hold'	=> '',
			],
			'confirm'	=> [
				'approve'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://healthpower.pro/api/ext/add.json',
		'urlOrderInfo'		=> 'https://healthpower.pro/api/ext/list.json',
	],
];

?>