<?php

return [
	
	34199 => [
		'terraleadsApiKey'			=> 'de4dc05d16d73ee4376e3a8f7083451a',
		'token' 			    	=> 'ebc03d1958eeeed9efeeced924abd066',
		
		'offers' => [ #Herbal Long Strength - IN
			4918 => [
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'8'	=> 'Wrong number',
				'5'	=> 'Call Not pickup',
				'10'	=> 'Switch off',
			],
			'reject'	=> [
				'6'	=> 'Call after sometime',
				'9'	=> 'Not interested',
			],
			'expect'	=> [
				'2'	=> 'New',
				'1'	=> 'Customer',
				'7'	=> 'Order place',
			],
			'confirm'	=> [
				'3'	=> 'Confirm',
				'11'	=> 'Delivered',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://herballongstrength.in/crm/api/v1.0/create-leads',
		'urlOrderInfo'		=> 'https://herballongstrength.in/crm/api/v1.0/get-mylead',
	],
];

?>