<?php

/*
	https://perfexcrm.themesic.com/apiguide/#api-Lead-GetLeadSearch
 */

return [
	35308 => [ // 
		'terraleadsApiKey'			=> 'b5d69f6c1afe67df299057626ae11884',
		'authtoken' 				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiYXBpZmlyc3QiLCJuYW1lIjoidHJhZnVycnkiLCJwYXNzd29yZCI6bnVsbCwiQVBJX1RJTUUiOjE2Mjc5NzcwOTV9.W4CcJ-gI6Dj_pQ88xq4VkPOq2pe8UYjBeJ5E7lbVXkU',
		
		'offers' => [
			0000 => [
				'source'        => '4',
				'assigned'		=> '1', 		//id админа, привязать к админу (необязательный)
				'status'		=> '12',
				
				'configs' => [
					'urlOrderAdd'		=> 'https://naturelayurveda.in/customer/api/leads',
					'urlOrderInfo'		=> 'https://naturelayurveda.in/customer/api/leads/search',
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash'	=> [
					'cancel' 		=> '',
					'17' =>	'Incoming call', //- - ???
					'13' =>		'Incoming Not Available',// —- ???
					'9' =>		'Wrong Number', //- trash					
				],
				'reject'	=> [
					'cancel'        => '',
					'8' =>		'Cancel/Not Interested', //- reject
					'14' =>		'COD NOT AVAILABLE', //- reject ???
					'16' =>		'Reject Lead', //- reject	 				
				],
				'expect'	=> [					
					'10' =>		'Address Issue', //- ???
					'3' =>		'Call after Sometimes', //— expect
					'4' =>		'Call Not Pickup', //— expect
					'2' =>		'Call Repet - 1',//— expect
					'6' =>		'Call Repet - 2',//— expect
					'7' =>		'Call Repet - 3',//— expect	 
					'11' =>		'Late Date Order',// —-???
					'12' =>		'New',	// — expect
					'15' =>		'Order Place', //—-???				
				],
				'confirm'	=> [
					'customer'    	=> '',
					'1' =>	'Customer - confirm',
				],
			],
			
			'brakeLogFolder'	=> true,
//		'statusEnabled'		=> false,
		],
	],
	25167 => [ // Herbal Penirex - IN Nimesh
		'terraleadsApiKey'			=> '26cdcf093394fb84a90c609f8b73e62a',
		'authtoken' 				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoia28iLCJuYW1lIjoiSGVyYiIsInBhc3N3b3JkIjpudWxsLCJBUElfVElNRSI6MTYxNzI4MDkyNn0.rF546HeQiDGGyVpt5RWc17JshhwN1aha7OHiLJFWd3s',
		
		'offers' => [
			3309 => [
				'source'        => '8',
				'assigned'		=> '1', 		//id админа, привязать к админу (необязательный)
				'status'		=> '2',
				
				'configs' => [
					'urlOrderAdd'		=> 'https://nutralifes.com/crm6/api/leads',
					'urlOrderInfo'		=> 'https://nutralifes.com/crm6/api/leads/search',
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
// 13	Address Issue= expect
// 3	Call After Sometime = expect
// 4	Call Not Picked =expect
// 5	Cancel/Not Interested = reject
// 12	Cod Not Available = trash
// 14	Confirm Customer = expect
// 1	Customer = confirm
// 16	fake Lead =trash
// 15	Incoming Call = trash
// 11	Late Date Order = expect
// 2	New = expect
// 10	Order Placed = confirm
// 6	Repeat Call-1= expect
// 7	Repeat Call-2= expect
// 8	Repeat Call-3= expect
// 17	Repeat Call-4 = expect
// 9	Wrong Number = trash
				'trash'	=> [
					'trash' 		=> '',
					'12' 		    => 'Cod Not Available',
					'16' 		    => 'fake Lead',
					'15'			=> 'Incoming Call',
					'9'			=> 'Wrong Number',
				],
				'reject'	=> [
					'cancel'    => '',
					'5'     	=> 'Cancel/Not Interested',
				],
				'expect'	=> [
					'new'   	=> '',
					'13' 		=> 'Address Issue',
					'3' 		=> 'Call After Sometime',
					'4' 		=> 'Call Not Picked',
					'14' 		    => 'Confirm Customer',
					'11' 		    => 'ate Date Order ',
					'2' 		    => 'New',
					'6' 		    => 'Repeat Call-1',
					'7' 		    => 'Repeat Call-2',
					'8' 		    => 'Repeat Call-3',
					'17' 		    => 'Repeat Call-3',				
				],
				'confirm'	=> [
					'customer'    	=> '',
					'1' 		    => 'Customer',
					'10' 		    => 'Order Placed',
				],
			],
			
			'brakeLogFolder'	=> true,
//		'statusEnabled'		=> false,
		],
	],
	10388 => [ // Herbal of hammer , Nimesh
		'terraleadsApiKey'			=> '5d888d3df541d5d64254a33623f86c97',
		'authtoken' 				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoia29zdCIsIm5hbWUiOiJIZXJiIiwicGFzc3dvcmQiOm51bGwsIkFQSV9USU1FIjoxNjA1NDU5MTMwfQ.632uyFaOjq00P0UiYBohy0d-ToHqc7VDFj7Mk9KmiUM',
		
		'offers' => [
			2863 => [
				'source'        => '5',
				'assigned'		=> '1', 		//id админа, привязать к админу (необязательный)
				'status'		=> '10',
				
				'configs' => [
					'urlOrderAdd'		=> 'https://howmyhealth.in/crm6/api/leads',
					'urlOrderInfo'		=> 'https://howmyhealth.in/crm6/api/leads/search',
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash'	=> [
					'cancel' 		=> '',
					'16' 		    => 'Short lead',
					'15' 		    => 'wrong number',
					'8'				=> 'not in service',
				],
				'reject'	=> [
					'cancel'        => '',
					'4' 		    => 'cancel \ not interested',
					'3' 		    => 'COD not available',
				],
				'expect'	=> [
					'new'   		=> '',
					'10' 		    => 'New',
					'5' 		    => 'not pickup',
					'9' 		    => 'late date order ',
					'18' 		    => 'interested customer',
					'2' 		    => 'Confirm Customer',
					'17' 		    => 'language issues',
					'14' 		    => 'repeat call-3',
					'13' 		    => 'repeat call-2',
					'12' 		    => 'repeat call-1',
					'6' 		    => 'call after some time',
				
				],
				'confirm'	=> [
					'customer'    	=> '',
					'1' 		    => 'Customer',
					'7' 		    => 'address issue',
					'11' 		    => 'Order Placed',
				],
			],
//			'bridgeHandler' 	=> 'app\modules\terraleads\handlers\adv\perfex\user_10388\PerfexBridgeHandler',
//			'statusHandler' 	=> 'app\modules\terraleads\handlers\adv\perfex\user_10388\PerfexStatusHandler',
			'brakeLogFolder'	=> true,
		],
	],
    28861 => [ // Nimesh
		'terraleadsApiKey'			=> '817e4e34bb116449cfc4e1c49fd3320c',
		'authtoken' 				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoib3J0aG8iLCJuYW1lIjoidGVyIiwicGFzc3dvcmQiOm51bGwsIkFQSV9USU1FIjoxNjA5NDM4NTg4fQ.rTkPTfh5jVpxXZw3NKZhl-hZOxNpguKZ4tEUuFLf0OM',
		
		'offers' => [
			4049 => [ //#3020 - Herbal Orthonim Oil - IN
				'source'        => '4',
				'assigned'		=> '1', 		//id админа, привязать к админу (необязательный)
				'status'		=> '10',
				
				'configs' => [
					'urlOrderAdd'		=> 'https://www.orthonim.com/crm/api/leads',
					'urlOrderInfo'		=> 'https://www.orthonim.com/crm/api/leads/search',
					'brakeLogFolder'	=> true,
				],
			],
			4573 => [ //HairActiv - IN
				'source'        => '8',
				'assigned'		=> '1', 		//id админа, привязать к админу (необязательный)
				'status'		=> '10',
				
				'configs' => [
					'urlOrderAdd'		=> 'https://www.orthonim.com/crm/api/leads',
					'urlOrderInfo'		=> 'https://www.orthonim.com/crm/api/leads/search',
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash'	=> [
					'cancel' 		=> '',
					'16' 		    => 'Short lead',
					'15' 		    => 'wrong number',
					'8'				=> 'not in service',
				],
				'reject'	=> [
					'cancel'        => '',
					'4' 		    => 'cancel \ not interested',
					'3' 		    => 'COD not available',
				],
				'expect'	=> [
					'new'   		=> '',
					'10' 		    => 'New',
					'5' 		    => 'not pickup',
					'9' 		    => 'late date order ',
					'18' 		    => 'interested customer',
					'2' 		    => 'Confirm Customer',
					'17' 		    => 'language issues',
					'14' 		    => 'repeat call-3',
					'13' 		    => 'repeat call-2',
					'12' 		    => 'repeat call-1',
					'6' 		    => 'call after some time',
					'7' 		    => 'address issue',
				
				],
				'confirm'	=> [
					'customer'    	=> '',
					'1' 		    => 'Customer',
					'11' 		    => 'Order Placed',
				],
			],
			
			'brakeLogFolder'	=> true,
//		'statusEnabled'		=> false,
		],
	],
	33795 => [ // Orthohim - IN
		'terraleadsApiKey'			=> '592452b77e6fe0d0adc30bb2ab12cd09',
		'authtoken' 				=> '610f898bfc6d1751fed1e23ed08bf5f3',
		
		'offers' => [
			4920 => [
				'source'        => '1',
				'assigned'		=> '3', 		//id админа, привязать к админу (необязательный)
				'status'		=> '2',
				
				'configs' => [
					'urlOrderAdd'		=> 'http://crm.orthohim.in/api/v1.0/create-leads',
					'urlOrderInfo'		=> 'http://crm.orthohim.in/api/v1.0/get-mylead',
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			
			'statuses' => [
				'trash'	=> [
					'trash' 		=> '',
					'11' 		=> 'address not metch',
					'16'		=> 'COD not available',
				],
				'reject'	=> [
					'cancel'        => '',
					'3' 		    => 'Cancel',
					'13' 		    => 'Reject',
				],
				'expect'	=> [
					'0'   		=> 'new',
					'12'		=> 'call after some time',
					'4'			=> 'call after some time2',
					'10'		=> 'call not pick up',
					'2'			=> 'New',
					'14'        => 'Futcher order',
					'5' 		    => 'Repeat',
					'15' 		    => 'Confirm',
				],
				'confirm'	=> [
					'customer'    	=> '',
					'1' 		    => 'Customer',
					'7' 		    => 'Delivered',
					'6' 		    => 'Order Place',
					'9' 		    => 'Return',

				],
			],
			
			'brakeLogFolder'	=> true,
//		'statusEnabled'		=> false,
		],
	],
	22979 => [ // herbal power +, raj
		'terraleadsApiKey'			=> '592452b77e6fe0d0adc30bb2ab12cd09',
		'authtoken' 				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoia29zdGEiLCJuYW1lIjoidGVycmEiLCJwYXNzd29yZCI6bnVsbCwiQVBJX1RJTUUiOjE1ODM0OTE3MDd9.0GRs6vauCZCqoDJFSr62-T15S_NmkhkmpsTW8ekKDBA',
		
		'offers' => [
			2988 => [
				'source'        => '1',
				'assigned'		=> '3', 		//id админа, привязать к админу (необязательный)
				'status'		=> '2',
				
				'configs' => [
					'urlOrderAdd'		=> 'https://powerplus.adwapi.com/api/leads',
					'urlOrderInfo'		=> 'https://powerplus.adwapi.com/api/leads/search',
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			
			'statuses' => [
				'trash'	=> [
					'trash' 		=> '',
					'11' 		=> 'address not metch',
					'16'		=> 'COD not available',
				],
				'reject'	=> [
					'cancel'        => '',
					'3' 		    => 'Cancel',
					'13' 		    => 'Reject',
				],
				'expect'	=> [
					'new'   		=> '',
					'12'		=> 'call after some time',
					'4'			=> 'call after some time2',
					'10'		=> 'call not pick up',
					'2'			=> 'New',
					'14'        => 'Futcher order',
					'5' 		    => 'Repeat',
					'15' 		    => 'Confirm',
				],
				'confirm'	=> [
					'customer'    	=> '',
					'1' 		    => 'Customer',
					'7' 		    => 'Delivered',
					'6' 		    => 'Order Place',
					'9' 		    => 'Return',

				],
			],
			
			'brakeLogFolder'	=> true,
//		'statusEnabled'		=> false,
		],
	],
	25008 => [ // Hair Oil, raj
		'terraleadsApiKey'			=> '0420e1771028ed0d4147ef99436b2335',
		'authtoken' 				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiS29zdGEiLCJuYW1lIjoiVGVycmEiLCJwYXNzd29yZCI6bnVsbCwiQVBJX1RJTUUiOjE1OTI1OTYwNzB9.XQswhxMXgFYKBTv1V7mGiaSHXvBw1DPYi04YUzrbcPc',
		
		'offers' => [
			4802 => [
				'source'        => '16',
				'assigned'		=> '2', 		//id админа, привязать к админу (необязательный)
				'status'		=> '8',
				
				'configs' => [
					'statuses' => [
						'trash'	=> [
							'trash' 		=> '',
							'6' 		    => 'Trash',
						],
						'reject'	=> [
							'cancel'        => '',
							'3' 		    => 'Cancel',
						],
						'expect'	=> [
							'new'   		=> '',
							'8'				=> 'New',
							'4' 		    => 'Hold',
							'7' 		    => 'Call After Some Time',
							'2' 		    => 'New',
							'5' 		    => 'No Answer',
							'22'			=> 'Hold',
							'11' 		    => 'Hold',
							'12' 		    => 'Hold',
							'13' 		    => 'Hold',
							'21' 		    => 'Hold',
							'23' 		    => 'Hold',
						],
						'confirm'	=> [
							'customer'    	=> '',
							'1' 		    => 'Customer',
							'9'				=> 'Confirm',
							'10'			=> 'Confirm',
							'28' 		    => 'Customer',
						
						],
					],
					
					'urlOrderAdd'		=> 'https://ayurdailyherbal.com/work/api/leads',
					'urlOrderInfo'		=> 'https://ayurdailyherbal.com/work/api/leads/search',
					'brakeLogFolder'	=> true,
				],
			],
			3293 => [
				'source'        => '5',
				'assigned'		=> '2', 		//id админа, привязать к админу (необязательный)
				'status'		=> '8',
				
				'configs' => [
					'statuses' => [
						'trash'	=> [
							'trash' 		=> '',
							'6' 		    => 'Trash',
						],
						'reject'	=> [
							'cancel'        => '',
							'3' 		    => 'Cancel',
						],
						'expect'	=> [
							'new'   		=> '',
							'8'				=> 'New',
							'4' 		    => 'Hold',
							'7' 		    => 'Call After Some Time',
							'2' 		    => 'New',
							'5' 		    => 'No Answer',
							'22'			=> 'Hold',
							'11' 		    => 'Hold',
							'12' 		    => 'Hold',
							'13' 		    => 'Hold',
							'21' 		    => 'Hold',
							'27' 		    => 'Hold',
							'23' 		    => 'Hold',
						],
						'confirm'	=> [
							'customer'    	=> '',
							'1' 		    => 'Customer',
							'9'				=> 'Confirm',
							'10'			=> 'Confirm',
						
						],
					],
					
					'urlOrderAdd'		=> 'https://ayurdailyherbal.com/work/api/leads/',
					'urlOrderInfo'		=> 'https://ayurdailyherbal.com/work/api/leads/search',
					'brakeLogFolder'	=> true,
				],
			],
			4064 => [ //Xtra Man Tablet - IN
				'source'        => '15',
				'assigned'		=> '2', 		//id админа, привязать к админу (необязательный)
				'status'		=> '8',
				
				'configs' => [
					'statuses' => [
						'trash'	=> [
							'trash' 		=> '',
							'6' 		    => 'Trash',
						],
						'reject'	=> [
							'cancel'        => '',
							'3' 		    => 'Cancel',
						],
						'expect'	=> [
							'new'   		=> '',
							'8'				=> 'New',
							'4' 		    => 'Hold',
							'7' 		    => 'Call After Some Time',
							'2' 		    => 'New',
							'5' 		    => 'No Answer',
							'22'			=> 'Hold',
							'11' 		    => 'Hold',
							'12' 		    => 'Hold',
							'13' 		    => 'Hold',
							'21' 		    => 'Hold',
							'27' 		    => 'Hold',
							'23' 		    => 'Hold',
						],
						'confirm'	=> [
							'customer'    	=> '',
							'1' 		    => 'Customer',
							'9'				=> 'Confirm',
							'10'			=> 'Confirm',
							'25' 		    => 'Customer',

						
						],
					],
					
					'urlOrderAdd'		=> 'https://ayurdailyherbal.com/work/api/leads/',
					'urlOrderInfo'		=> 'https://ayurdailyherbal.com/work/api/leads/search',
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
    29939 => [ //
		'terraleadsApiKey'			=> 'c75b11da7ac212bca55b6ecf7029dc51',
		'authtoken' 				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoia2hhbnBhcmEiLCJuYW1lIjoiYXl1cm1lbiIsInBhc3N3b3JkIjpudWxsLCJBUElfVElNRSI6MTYxNzI0ODcxNn0.4GudpLIU2A-wLU4dMxZDWeSe_dzaPpu4YEK2DZ8Vjh0',
		
		'offers' => [
			4188 => [//Titen Thor - IN
				'source'        => '4',
				'assigned'		=> 'Manish Kardani', 		//id админа, привязать к админу (необязательный)
				'status'		=> '10',
				
				'configs' => [
					'urlOrderAdd'		=> 'http://ayurmenpower.co.in/crm/api/leads',
					'urlOrderInfo'		=> 'http://ayurmenpower.co.in/crm/api/leads/search',
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash'	=> [
					'trash' 		=> '',
					'12' 		    => 'Trash',
					'20'			=> 'wrong number'
				],
				'reject'	=> [
					'cancel'        => '',
					'11' 		    => 'reject',
				],
				'expect'	=> [
					'new'   		=> '',
					'10'				=> 'NEW',
					'15'				=> 'Call after some time',
					'16'				=> 'Call repeat 1',
					'17'				=> 'Call repeat 2',
					'18'				=> 'Call repeat 3',
					'13'				=> 'NEW',
					'14'				=> 'Call not pick up',
					'24'				=> 'pre-confirm',
					'27'				=> 'pre-reject',
				],
				'confirm'	=> [
					'customer'    	=> '',
					'1' 		    => 'confirm',

				],
			],
			
			'brakeLogFolder'	=> true,
//		'statusEnabled'		=> false,
		],
	],	
	25568 => [ // nikunj@adwapi.com	 Herbal Booster - IN, raj
		'terraleadsApiKey'			=> 'cadd2a91db9952230d8e4c15d26be0d2',
		'authtoken' 				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoia29zdGEiLCJuYW1lIjoia29zdGEiLCJwYXNzd29yZCI6bnVsbCwiQVBJX1RJTUUiOjE1OTQzNjc0OTB9.4dXAMUneTMp4kT01m2kSMT4c7iHRHLJvJEtBLSDbygA',
		
		'offers' => [
			3370 => [
				'source'        => '1',
				'assigned'		=> '2', 		//id админа, привязать к админу (необязательный)
				'status'		=> '2',
				
				'configs' => [
					'urlOrderAdd'		=> 'https://neweraenter.com/office/api/leads',
					'urlOrderInfo'		=> 'https://neweraenter.com/office/api/leads/search',
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash'	=> [
					'trash' 		=> '',
					'5' 		    => 'wrong number',
					'8'				=> 'not in service',
				],
				'reject'	=> [
					'cancel'        => '',
					'29' 		    => 'Reject',
					'7' 		    => 'order RTo',
					'8' 		    => 'Not intereested',
					'9' 		    => 'Not connect',
					'13' 		    => 'incomming call not',
					'15' 		    => 'COD not available',
					'21' 		    => 'Address issue',
					'22' 		    => 'Return order',
				],
				'expect'	=> [
					'new'   		=> '',
					'6'				=> 'pending order',
					'10' 		    => 'New reminder out',
					'12' 		    => 'New reminder',
					'16' 		    => 'New reminder3',
					'17' 		    => 'New reminder2',
					'18' 		    => 'New reminder1',
					'19' 		    => 'call not picj up',								
					'20' 		    => 'Call After Some Time',
					'2' 		    => 'New',
					'19' 		    => 'call not pickup',
				],
				'confirm'	=> [
					'customer'    	=> '',
					'1' 		    => 'Customer',
					'3'				=> 'Confirm',
					'4'				=> 'Confirm',
					'14'			=> 'Confirm',
					'23' 		    => 'Delivered',
					'30' 		    => 'confrim',

				],
			],
			
			'brakeLogFolder'	=> true,
//		'statusEnabled'		=> false,
		],
	],
	//http://connectcpa.net/terraleads/adv/adv-postback-status?systemUserId=27306&partnerStatus={status}&systemOrderId={terra_id}&comment={status}&postbackApiKey=653ead3cc758e39c1dab376cf02b8ea0
    27306 => [ // Green Coffee Capsules - IN, raj
		'terraleadsApiKey'			=> '653ead3cc758e39c1dab376cf02b8eab',
		'postbackApiKey' 		  =>'653ead3cc758e39c1dab376cf02b8ea0',
		'authtoken' 				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoia29zdGEiLCJuYW1lIjoiVGVycmEiLCJwYXNzd29yZCI6bnVsbCwiQVBJX1RJTUUiOjE2MDIzNjEwNzR9.stQ_gzQX47UmA0z4jMYApfee29Ga5w-uCBabP4zh0WQ',
		
		'offers' => [
			3767 => [
				'source'        => '3',
				'assigned'		=> '2', 		//id админа, привязать к админу (необязательный)
				'status'		=> '2',
				'website'		=> function ($model, $config) {
					return $model->id;
				},
				'configs' => [
					'urlOrderAdd'		=> 'http://herbexlife.com/green/api/leads/',

					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'statusHandler' => 'app\modules\terraleads\handlers\adv\perfex\postback\PerfexStatusHandler',
			'statuses' => [
				'trash'	=> [
					'trash' 		=> '',
					'12' 		    => 'trash',
				],
				'reject'	=> [
					'cancel'        => '',
					'3' 		    => 'Reject',

				],
				'expect'	=> [
					'new'   		=> '',
					'2' 		    => 'New',
					'4' 		    => 'Hold',
					'9' 		    => 'Hold',
				],
				'confirm'	=> [
					'customer'    	=> '',
					'1' 		    => 'Customer',
				],
			],
			
			'brakeLogFolder'	=> true,
//		'statusEnabled'		=> false,
		],
	],
	// http://connectcpa.net/terraleads/adv/adv-postback-status?systemUserId=50724&partnerStatus={status}&systemOrderId={terra_id}&comment={status}&postbackApiKey=fs0e3f0c2c0cf1b6caab800ec56c77xx
	50724 => [ // vijay@adwapi.com	 Titan Tablet - IN, raj
		'terraleadsApiKey' => '2015969d9d45b1673d0c98db6438bddb',
		'postbackApiKey'   => 'fs0e3f0c2c0cf1b6caab800ec56c77xx',
		
		'authtoken' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiVGVycmEiLCJuYW1lIjoiVGl0YW4gVCIsInBhc3N3b3JkIjpudWxsLCJBUElfVElNRSI6MTY4MjU4NzM3NH0.k9_SgBm6S1bl8l0FozP3TF0w-X5KJnYI7UiP7sPtVjU',
		
		'offers' => [
			4011 => [ // Titan Tablet - IN
				'source' => '6',
				'assigned' => '1',        //id админа, привязать к админу (необязательный)
				'status' => '2',
				
				'website'		=> function ($model, $config) {
					return $model->id;
				},
				
				'configs' => [
					'urlOrderAdd' => 'https://herbexlife.com/joint/api/leads',
					
					'brakeLogFolder' => true,
				],
			],
		],
		
		'configs' => [
			'statusHandler' => 'app\modules\terraleads\handlers\adv\perfex\postback\PerfexStatusHandler',
			
			'statuses' => [
				'trash' => [
					'5' => 'lost',
				],
				'reject' => [
					'4' => 'Reject',
				],
				'expect' => [
					'3' => 'hold',
					'2' => 'hold',
					'12' => 'hold',
					'8' => 'hold',
					'10' => 'hold',
				],
				'confirm' => [
					'1' => 'Customer',
					'13' => 'Confirm',
				
				],
			],
			
			'brakeLogFolder' => true,
//		'statusEnabled'		=> false,
		],
	],
//http://connectcpa.net/terraleads/adv/adv-postback-status?systemUserId=25489&partnerStatus={status}&systemOrderId={terra_id}&comment={status}&postbackApiKey=fs0e3f0c2c0cf1b6caab800ec56c77ss
25489 => [ // vijay@adwapi.com	 Herbex Joint - IN, raj
	'terraleadsApiKey' => 'ff0e3f0c2c0cf1b6caab800ec56c7759',
	'postbackApiKey'   => 'fs0e3f0c2c0cf1b6caab800ec56c77ss',
	
	'authtoken' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiVGVycmEiLCJuYW1lIjoiVGl0YW4gVCIsInBhc3N3b3JkIjpudWxsLCJBUElfVElNRSI6MTY4MjU4NzM3NH0.k9_SgBm6S1bl8l0FozP3TF0w-X5KJnYI7UiP7sPtVjU',
	
	'offers' => [
		3371 => [ // Herbex Joint - IN
			'source' => '1',
			'assigned' => '2',        //id админа, привязать к админу (необязательный)
			'status' => '2',
			
			'website'		=> function ($model, $config) {
				return $model->id;
			},
			
			'configs' => [
				'urlOrderAdd' => 'https://herbexlife.com/joint/api/leads',
				
				'brakeLogFolder' => true,
			],
		],
	],
	
	'configs' => [
		'statusHandler' => 'app\modules\terraleads\handlers\adv\perfex\postback\PerfexStatusHandler',
		
		'statuses' => [
			'trash' => [
				'5' => 'lost',
			],
			'reject' => [
				'4' => 'Reject',
			],
			'expect' => [
				'3' => 'hold',
				'2' => 'hold',
				'12' => 'hold',
				'8' => 'hold',
				'10' => 'hold',
			],
			'confirm' => [
				'1' => 'Customer',
			
			],
		],
		
		'brakeLogFolder' => true,
//		'statusEnabled'		=> false,
	],
],
];