<?php

/**
 * https://developer.cod.network/guides/cod-affiliate/Leads.html#create-lead
 */

return [
	55903 => [
		'terraleadsApiKey'		=> '20ee45978b4c7b16db79d76f1e14eb84',
		'postbackApiKey'		=> '20ee45978b4c7b16dd79d76f1e14eb88',
		'api_key'				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvY29kLm5ldHdvcmsiLCJpYXQiOjE2OTY1MDEwNjksImV4cCI6MTg1NDE4MTA2OSwibmJmIjoxNjk2NTAxMDY5LCJqdGkiOiJRZXQ5MlJ5OWxnRTd4dTlhIiwic3ViIjoiMGQ5N2M3MmQtNDUwOS00ZjFjLWJhNWEtZDFkMzgwYmYyMjk4IiwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.kxLPuH7glyAAwLoPOU8xFuL0KXNXiaFlBeC4q8Ew-yA',
		
		'offers' => [
			7916 => [ // Optifix - CI
				'referral_id' => '5IhG9Nh0RH',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7915 => [ // HeartKeep - SG
				'referral_id' => 'C0E4dU60kq',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7913 => [ // HeartKeep - CI
				'referral_id' => '3ye4FXHrh7',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7912 => [ // Diabextan - CI
				'referral_id' => 'AU0yQo8aaJ',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7911 => [ // Diabextan - SG
				'referral_id' => 'TOYJJBNbjy',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7910 => [ // Optivision - CI
				'referral_id' => 'mo25d2ev2N',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2323 => [
				'referral_id' => 'KV23VmDlrC',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'WRONG' => '',
				'EXPIRED' => '',
				'DUPLICATED' => '',
			],
			'reject' => [
				'RETURN' => '',
				'CANCELLED' => '',
			],
			'expect' => [
				'CALL_LATER' => '',
				'CALL_LATER_SCHEDULED' => '',
				'NO_REPLY' => '',
			],
			'confirm' => [
				'CONFIRMED' => '',
				'DELIVERED' => '',
			],
		],
		
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://api.cod.network/v1/affiliate/leads',
		'urlOrderInfo'		=> '',
	],
];