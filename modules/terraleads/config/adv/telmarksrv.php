<?php

return [
	49869 => [
		'terraleadsApiKey' 	=> '37037565681ceab3830768b95d3ba7be',
		'email'				=> 'kosta@terraleads.com',
		'password'			=> 'apiterraleads@2023',
		
		'offers' => [
			6947 => [ // Optimen - ID
				'sku' => '15',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6938 => [ // Glucoformin - ID
				'sku' => '09',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'Trash' => '',
				'Double' => '',
			],
			'reject' => [
				'Rejected' => '',
				'Excluded' => '',
			],
			'expect' => [
				'Accepted' => '',
				'Accept' => '',
				'Awaiting' => '',
			],
			'confirm' => [
				'Sold' => '',
			],
		],
		'brakeLogFolder' 	=> true,
		'urlOrderAdd' 		=> 'http://lms.telmarksrv.com/api/leads/v2/add',
		'urlOrderInfo' 		=> 'https://lms.telmarksrv.com/api/leads/v2/getAll',
	],
];

?>