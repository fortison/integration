<?php

return [
	28441 => [ //AffBay Asia
		'terraleadsApiKey'			=> '926de48afe830f859e31ca8c502a40f5',
		'postbackApiKey'			=> '926de48afe830f859e31ca8c502a40f6',
		'token' 			    	=> 'efafb057f079e69ff315983b9d28d44e',
		
		'offers' => [
			7438 => [ //Neoshine - ID
				'product'	=> '7ddce95b-5a5b-4a4a-b6ef-88f9944e8572',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6769 => [ //Neolift - ID
				'product'	=> '61209950-db96-4be3-8b79-c2ed2735c599',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6768 => [ //Keranity - ID
				'product'	=> '3488c0df-4afb-48ba-b8b5-17e2181df200',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5258 => [ //Artrivit  - ID
				'product'	=> '04b1e075-a64f-4ead-af13-e1c35113a613',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6752 => [ //Brestel - ID
				'product'	=> '86c29f63-bb3b-475d-8306-610b1595ec81',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5150 => [ //Prostacare - ID
				'product'	=> '8319f16d-33bc-429e-9f46-5c8b1deebfdc',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6305 => [ //Diabtiq - ID
				'product'	=> '1d7a4971-7715-4932-aa0b-e7b8bcbce784',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6304 => [ //Slimmes - ID
				'product'	=> '3208e344-1d35-4baa-accc-b8ae30ca5303',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6303 => [ //Nutrilivin - ID
				'product'	=> '0aa3826f-ae64-46cc-a7e8-06082ab6c977',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6234 => [ //Glucousan - ID
				'product'	=> 'bb28c12f-a556-41e7-9953-ee557025ccac',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6235 => [ //Evision - ID
				'product'	=> 'd70f0a24-be5e-45dd-9d48-df7e04396f39',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6236 => [ //Slimetrix - ID
				'product'	=> 'cd7dd195-e6cd-48a1-85cc-bb80c340a170',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5898 => [ //Silcontrol caps - ID
				'product'	=> '7f441165-ba78-4e26-972f-5784b681962f',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5808 => [ //Duralove - ID
				'product'	=> 'b9769f9f-104f-4aed-bd36-e599c7c81dfd',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5707 => [ //Breastly - ID
				'product'	=> '31e68062-5c96-463a-8b82-81498104d6c4',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5300 => [ //Glolift - TH
				'product'	=> 'faa1504c-cf38-48ff-a640-6df5f36d3085',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5301 => [ //Glolift - ID
				'product'	=> '1fbbacee-4870-4645-b968-2eb304c0df63',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5263 => [ //Silcontrol - TH
				'product'	=> '3a4bd23d-824d-4432-a251-09be40bb0ccd',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5307 => [ //Alfaman - ID
				'product'	=> '16a307b8-baf0-4b1c-ba77-44c001eaba54',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5055 => [ //Cha I Ca Kan - TH
				'product'	=> '91727bb1-c977-4915-ab31-ede7144c4c6e',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5018 => [ //Nephrotec - ID
				'product'	=> 'a7c96d00-d381-4545-aa4a-5027a084fb75',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5013 => [ //Fitelit - ID
				'product'	=> 'e4eaa8ac-1844-4ce1-8676-6669d6473873',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4103 => [ //Exxtens - ID
				'product'	=> '5bb7f556-073c-4242-8682-d409e1f23b47',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3980 => [ //Cholestoff - ID
				'product'	=> '024f90ff-4381-4bc4-abcf-8faac5d8942f',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3982 => [ //Tmaxx - TH
				'product'	=> '17ccc2d7-a1ad-413f-a007-001c99b2ac22',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3981 => [ //Garcinia Gold 5000 - TH
				'product'	=> '80b91b20-972d-4b63-8b83-d47e69b15656',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4181 => [ //Gluconormix  - ID
				'product'	=> '33e12af3-9071-424c-8570-630378e3a253',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4487 => [ //Tmaxx TH CPL
				'product'	=> '17ccc2d7-a1ad-413f-a007-001c99b2ac22',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4474 => [ //Artriblock - ID
				'product'	=> 'd4ae7e42-4eec-43ce-bce1-8f14e6ff1f48',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4468 => [ //Bactenormin - ID
				'product'	=> '11125bc9-50fc-41da-99c7-432f9f054137',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4493 => [ //Bio Deto - ID
				'product'	=> 'f3968a22-b878-4c54-83ef-253bce9185ec',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4473 => [ //Bustel - ID
				'product'	=> '36446a81-245d-44a4-a5e9-fabfa0f063ef',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4483 => [ //Cardionormin - ID
				'product'	=> 'a24abda3-f25d-4a77-957b-ec1346e45f91',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4496 => [ //Fit Expert - ID
				'product'	=> '20ddc94c-d963-4c17-9ed2-919034d8ee90',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4484 => [ //Jointsflexa - ID
				'product'	=> 'a507d4f2-c716-4bbb-aaa6-3c9e714eacb1',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4485 => [ //Lune - ID
				'product'	=> 'af5cbf0a-eb4d-41c6-8c1e-533be703e052',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4104 => [ //Ke One - TH
				'product'	=> 'e9f366e2-9415-42bb-9657-bfe9de38c200',
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'		=> '',
			],
			'reject'	=> [
				'reject'	=> '',
			],
			'expect'	=> [
				'expect'	=> '',
			],
			'confirm'	=> [
				'confirm'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://api.affbay.com/v2/api/make/contact',
		'urlOrderInfo'		=> '',
	],
];

?>
