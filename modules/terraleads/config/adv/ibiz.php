<?php

/*
 * https://terraleads.com/acp/docs#model_lead_method_create
 */

return [

    21243 => [
        'terraleadsApiKey'				=> 'a7e7bdab35764c758fca5dd2a46f8978',
        'postbackApiKey'                => 'a7e7bdab35764c758fca5dd2a46f8979',
        'apiKey'					    => '$2y$10$4Rz8E.G4ffVbScUELEFEU.pXjEwexEz1gjZXiXrM4lLYz8UVBuSeu',
		'user_id'						=> 7, //7

        'offers' => [
            7675 => [
                'product_id' => 7,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            2756 => [
				'product_id' => 2,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            2852 => [
                'product_id' => 1,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3155 => [
                'product_id' => 3,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3656 => [ //Nourish Burst - BD
                'product_id' => 4,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3846 => [ //Flekosteel - BD
                'product_id' => 5,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3926 => [ //Flekosteel - BD
                'product_id' => 6,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
        ],
        'configs' => [
            'statuses' => [
                'reject'	=> [
                    'reject'	=> '',
                    'calcelled'	=> '',
                ],
                'expect'	=> [
                    'expect'	=> '',
                    'new lead'	=> '',
                ],
                'trash'	=> [
                    'trash'		=> '',
                    'double'		=> '',
                    'spam'		=> '',
                ],
                'confirm'	=> [
                    'confirm'	=> '',
                ],
            ],

            'bridgeRequestTimeout'		=> 60,
            'statusRequestTimeout'		=> 60,
            'urlOrderAdd'				=> 'http://ibiz.illusion-biz.com/api/lead/create', //http://ibiz.illusion-biz.com/api/lead/create
            'urlOrderInfo'				=> '',
        ],
    ],
];

?>