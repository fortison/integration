<?php

/*
 * У файлі
 *
 * http://connectcpa.net/terraleads/adv/adv-postback-status?systemUserId=21830&systemOrderId={aff_sub}&partnerStatus={status}&comment={comment}&postbackApiKey=cb244573faa622614445b49ab0b30794
 */

return [
	
	21830 => [
		'terraleadsApiKey'			=> 'cb244573faa622614445b49ab0b80793',
//		'postbackApiKey'			=> 'cb244573faa622614445b49ab0b30794',
		
		'offers' => [
			3574 => [ // Prostonic - TR
				'page_name'	=> 'prosto',
				'aff_id' => 28074,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],			
			3179 => [
				'page_name'	=> 'slim',
				'aff_id' => 28074,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3222 => [
				'page_name'	=> 'strong',
				'aff_id' => 28074,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3302 => [
				'page_name'	=> 'ottoman',
				'aff_id' => 28074,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3321 => [
				'page_name'	=> 'rflex',
				'aff_id' => 28074,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3344 => [
				'page_name'	=> 'akcsurup',
				'aff_id' => 28074,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3374 => [
				'page_name'	=> 'bomb',
				'aff_id' => 28074,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3372 => [
				'page_name'	=> 'bone',
				'aff_id' => 28074,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3373 => [
				'page_name'	=> 'scream',
				'aff_id' => 28074,

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'		=> '',
			],
			'reject'	=> [
				'cancelled'	=> '',
			],
			'expect'	=> [
				'hold'		=> '',
			],
			'confirm'	=> [
				'confirmed'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://cv3.juksr.com/v4/api/set',
		'urlOrderInfo'		=> 'https://cv3.juksr.com/v4/api/get_status/',
	],
];

?>