<?php

/*
* https://www.zoho.com/crm/developer/docs/api/register-client.html
 *
 * нужно изменить поле Authorized Redirect URIs на http://connectcpa.net/apilead/adv/zoho-extra
 *
 * POST запрос для refresh_token
 *
 * grant_type - authorization_code
 * client_id  	 -
 * client_secret -
 * code          - 100*
 * redirect_uri - http://connectcpa.net/terraleads/adv/extra-zoho
 */

return [
	20816 => [
		'terraleadsApiKey'			=> 'ad16e43702a791c7edb8c3f484a1f245',
		
		'refresh_token' 			=> '1000.0275a2f23735111901c82deab568a16c.ab12e4b8c6d7e723dd544e8642014069',
		'client_id' 				=> '1000.13WKX9NI9Z85DK6AHAY3WHT69YVINR',
		'client_secret'            	=> '35fbe3c4c374de71d8252c7d1adbf1ed59b8e58024',
		'acc_domain'                => 'EU',
		
		'offers' => [
			2274 => [
				'company' => 'Hair Straightener 4 in 1 - ES - 59 EUR',
				'external' => 'Terraleads',
				'product_code' => '8358-000000001',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2648 => [
				'company' => 'Hair Straightener 4 in 1 - PT - 59 EUR',
				'external' => 'Terraleads',
				'product_code' => '8358-000000002',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],		
		],
	],
	
	15929 => [
		'terraleadsApiKey'			=> '90d0c398c27d28ee91d573258deb6b25',
		
		'refresh_token' 			=> '1000.91210d1307dcd2953c24e48b553b7e17.93ea5e5214c26df3ab4d3684efe57175',
		'client_id' 				=> '1000.6MSEH4PND4NY9K8X13891KOWZGLAZH',
		'client_secret'            	=> 'f6d1636a741776a99157998a5936fc68b66c05461e',
		'acc_domain'                => 'US',
		
		'offers' => [
			2620  => [
				'company' => 'Chlorogenic - PL - 137 PLN',
				'external' => 'Terraleads',
				'website' => 'http://pl.chlorogenic.beauty-shopping.net/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2682  => [
				'company' => 'Audio Stimulator - IT 47 EUR',
				'external' => 'Terraleads',
				'website' => 'http://it.audio-stimulator.beauty-shopping.net/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2683  => [
				'company' => 'Audio Stimulator - PL 137 PLN',
				'external' => 'Terraleads',
				'website' => 'http://pl.audio-stimulator.beauty-shopping.net/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2619  => [
				'company' => 'Diet N1 - PL 137 PLN',
				'external' => 'Terraleads',
				'website' => 'http://pl.dietn1.beauty-shopping.net/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2458  => [
				'company' => 'Magnetic Insoles - PL 137 PLN',
				'external' => 'Terraleads',
				'website' => 'http://pl-magnetic-inoles.beauty-shopping.net/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2621  => [
				'company' => 'Pumpkin Seed Pro - PL 127 PLN',
				'external' => 'Terraleads',
				'website' => 'http://pl.pumpkin-seed.beauty-shopping.net/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2475  => [
				'company' => 'HipKnee Magnetic Belt - PL 127 PLN',
				'external' => 'Terraleads',
				'website' => 'http://pl.hip-knee-magnetic-belt.beauty-shopping.net/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2270  => [
				'company' => 'Kyöko Uno Valgus corrector - PL 127 PLN',
				'external' => 'Terraleads',
				'website' => 'http://pl.kyoko-unor.beauty-shopping.net/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2681  => [
				'company' => 'Pumpkin Seed PRO - IT 47 EUR',
				'external' => 'Terraleads',
				'website' => 'http://it.pumpkin-seed.beauty-shopping.net/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2680  => [
				'company' => 'Magnetic Insoles - IT 47 EUR',
				'external' => 'Terraleads',
				'website' => 'http://it.magnetic-insoles.beauty-shopping.net/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2684  => [
				'company' => 'Neumann Knee care - IT 39 EUR',
				'external' => 'Terraleads',
				'website' => 'http://it.neumann.beauty-shopping.net/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'		=> [
				'Junk Lead'				=> '',
				'Not Contacted'			=> '',
				'tresh'			=> '',
				
				'Нежелательный предварительный контакт'	=> '',
				'Обращение не производилось' 			=> '',
			],
			'reject'	=> [
				'Lost Lead'				=> '',
				'Not Qualified'			=> '',
				'reject'			=> '',

				'Упущенные предварительные контакты' => '',
				'Не квалифицировано'			     => '',
			],
			'expect'	=> [
				''								=> '',
				'Attempted to Contact'			=> '',
				'Contact in Future'				=> '',
				'Contacted'						=> '',
				'Awaiting Pick Up'				=> '',
				
				'Попытка установить связь'	    => '',
				'Установить связь в будущем'	=> '',
				'Связь установлена'				=> '',
			],
			'confirm'	=> [
				'Confirmed'             => '',
				'a Qualified'			=> '',
				'Confirm'			=> '',
				'confirm'			=> '',			
				'Отвечает требованиям на основании предварительной оценки пригодности' => '',
			],
		],
		
		'urls' => [
			'EU' => [
				'urlOrderAdd'		=> 'https://www.zohoapis.eu/crm/v2/Leads',
				'urlRefreshToken'   => 'https://accounts.zoho.eu/oauth/v2/token',
				'urlOrderInfo'		=> 'https://www.zohoapis.eu/crm/v2/Leads/',
			],
			'US' => [
				'urlOrderAdd'		=> 'https://www.zohoapis.com/crm/v2/Leads',
				'urlRefreshToken'   => 'https://accounts.zoho.com/oauth/v2/token',
				'urlOrderInfo'		=> 'https://www.zohoapis.com/crm/v2/Leads/',
			],
			'CN' => [
				'urlOrderAdd'		=> 'https://www.zohoapis.eu.cn/crm/v2/Leads',
				'urlRefreshToken'   => 'https://accounts.zoho.eu.cn/oauth/v2/token',
				'urlOrderInfo'		=> 'https://www.zohoapis.eu.cn/crm/v2/Leads/',
			],
			'IN' => [
				'urlOrderAdd'		=> 'https://www.zohoapis.in/crm/v2/Leads',
				'urlRefreshToken'   => 'https://accounts.zoho.in/oauth/v2/token',
				'urlOrderInfo'		=> 'https://www.zohoapis.in/crm/v2/Leads/',
			]
		],
		
		'brakeLogFolder'	=> true,
	],
];