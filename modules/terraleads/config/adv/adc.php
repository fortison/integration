<?php

/*
 * https://confluence.adcombo.com/display/DOCS/Incoming+orders+API test
 ttttttt*/

return [
	
	25284 => [
		'terraleadsApiKey'			=> '24a2171c7c5aa05cac703210e6030f36',
		'api_key' 					=> '00000000000000000000000000000000',
		
		'offers' => [
			27860 => [
				'offer_id' => 0,
				'base_url'	=> 'https://www.google.com/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'				=> '',
			],
			'reject'	=> [
				'cancelled'			=> '',
			],
			'expect'	=> [
				'hold'				=> '',
			],
			'confirm'	=> [
				'confirmed'			=> '',
				'buyout_approved'	=> '',
				'buyout_hold'		=> '',
				'buyout_rejected'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://api.adcombo.com/api/v2/order/create/',
		'urlOrderInfo'		=> 'https://api.adcombo.com/api/v2/order/status/',
	],
];