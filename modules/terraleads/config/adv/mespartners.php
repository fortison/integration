<?php

return [
	42292 => [
		'terraleadsApiKey'	=> 'a040828101e72ec23eb00ee82861e667',
		'uniqueId'			=> 'a02d9479e6',
		'password'			=> 'ca5c7bdfedbad83c7e81bd69e43d8d85',
		
		'offers' => [
			8064 => [ // OstyHealth - FR (Switzerland)
				'productId' => '25',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8063 => [ // Big Size - FR (Switzerland)
				'productId' => '24',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8062 => [ // Erectil - FR (Switzerland)
				'productId' => '23',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8061 => [ // Keto Probiotix - FR (Switzerland)
				'productId' => '26',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8060 => [ // MensPower - FR (Switzerland)
				'productId' => '22',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			8059 => [ // PowerShape - FR (Switzerland)
				'productId' => '21',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7558 => [ // Fungent - DE (Switzerland)
				'productId' => '16',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7382 => [ // Varicone - DE (Switzerland)
				'productId' => '17',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7360 => [ // OstyHealth - DE (Switzerland)
				'productId' => '14',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7359 => [ // Big Size - DE (Switzerland)
				'productId' => '13',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7358 => [ // Erectil - DE (Switzerland)
				'productId' => '12',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7310 => [ // Keto Probiotic - DE (Switzerland)
				'productId' => '15',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6831 => [ // MensPower - AT
				'productId' => '11',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6830 => [ // MensPower - CH
				'productId' => '10',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6829 => [ // MensPower - DE
				'productId' => '3',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5791 => [ // Power Shape-
				'productId' => '1',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5792 => [ // Power Shape-
				'productId' => '5',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5793 => [ // Power Shape-
				'productId' => '9',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'6' => '(TRASH)',
			],
			'reject' => [
				'4' => '(DENIED)',
				'5' => '(DENIED)',
			],
			'expect' => [
				'0' => '(PENDING)',
			],
			'confirm' => [
				'1' => '(APPROVED)',
				'2' => '(APPROVED)',
				'3' => '(APPROVED)',
			],
		],
		
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://mespartners.eu/api/partner',
		'urlOrderInfo'		=> 'https://mespartners.eu/api/partner2',
	],
];