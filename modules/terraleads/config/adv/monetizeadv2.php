<?php

return [
	21975 => [
		'terraleadsApiKey' => '70ff3a62c1d0cdae697614ef51cae066',
		'auth' => '7312b084a787d0d8dd36fc5b9a853d4b',
		'network' => 14,
		
		'offers' => [
            9159 => [ // Cardifix - MK
                'offer_id' => 17021,
                'country_code' => 'MK',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9158 => [ // Cardifix - XK
                'offer_id' => 17017,
                'country_code' => 'XK',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9157 => [ // Cardifix - AL
                'offer_id' => 17013,
                'country_code' => 'AL',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9156 => [ // Cardifix - BA
                'offer_id' => 17013,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9155 => [ // Cardifix - ME
                'offer_id' => 17009,
                'country_code' => 'ME',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9154 => [ // Cardifix - RS
                'offer_id' => 17001,
                'country_code' => 'RS',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9063 => [ // Cardifix - SI
                'offer_id' => 16990,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9062 => [ // Cardifix - HR
                'offer_id' => 16987,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9061 => [ // Cardifix - AT
                'offer_id' => 16985,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9060 => [ // Cardifix - DE
                'offer_id' => 16981,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9153 => [ // SecretDiet - MK
                'offer_id' => 17023,
                'country_code' => 'MK',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9152 => [ // SecretDiet - XK
                'offer_id' => 17019,
                'country_code' => 'XK',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9151 => [ // SecretDiet - AL
                'offer_id' => 17015,
                'country_code' => 'AL',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9150 => [ // SecretDiet - BA
                'offer_id' => 17007,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9149 => [ // SecretDiet - ME
                'offer_id' => 17011,
                'country_code' => 'ME',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9148 => [ // SecretDiet - RS
                'offer_id' => 17003,
                'country_code' => 'RS',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9071 => [ // SecretDiet - SI
                'offer_id' => 16992,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9070 => [ // SecretDiet - HR
                'offer_id' => 16989,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9069 => [ // SecretDiet - AT
                'offer_id' => 16993,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9068 => [ // SecretDiet - DE
                'offer_id' => 16983,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8508 => [ // OsteAction - XK
                'offer_id' => 17016,
                'country_code' => 'XK',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8507 => [ // OsteAction - AL
                'offer_id' => 17012,
                'country_code' => 'AL',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8506 => [ // OsteAction - MK
                'offer_id' => 17020,
                'country_code' => 'MK',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8505 => [ // OsteAction - ME
                'offer_id' => 17008,
                'country_code' => 'ME',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8504 => [ // OsteAction - BA
                'offer_id' => 17004,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8503 => [ // OsteAction - RS
                'offer_id' => 17000,
                'country_code' => 'RS',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8502 => [ // OsteAction - HR
                'offer_id' => 16995,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8501 => [ // OsteAction - SI
                'offer_id' => 16994,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8500 => [ // OsteAction - AT
                'offer_id' => 16984,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8499 => [ // OsteAction - DE
                'offer_id' => 16980,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9064 => [ // Glucolite - DE
                'offer_id' => 16982,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9065 => [ // Glucolite - AT
                'offer_id' => 16986,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9066 => [ // Glucolite - HR
                'offer_id' => 16988,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9067 => [ // Glucolite - SI
                'offer_id' => 16991,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9142 => [ // Glucolite - RS
                'offer_id' => 17002,
                'country_code' => 'RS',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9143 => [ // Glucolite - ME
                'offer_id' => 17010,
                'country_code' => 'ME',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9144 => [ // Glucolite - BA
                'offer_id' => 17006,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9145 => [ // Glucolite - AL
                'offer_id' => 17014,
                'country_code' => 'AL',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9146 => [ // Glucolite - XK
                'offer_id' => 17018,
                'country_code' => 'XK',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9147 => [ // Glucolite - MK
                'offer_id' => 17022,
                'country_code' => 'MK',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9346 => [ // Eroboost - BA
                'offer_id' => 17071,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9347 => [ // Collagent - BA
                'offer_id' => 17077,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9348 => [ // Osteflex - BA
                'offer_id' => 17076,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9349 => [ // Cardio A - BA
                'offer_id' => 17080,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9350 => [ // Varicone - BA
                'offer_id' => 17075,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9351 => [ // Fungent - BA
                'offer_id' => 17073,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9352 => [ // Prostanol - BA
                'offer_id' => 17078,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9353 => [ // Big Size - BA
                'offer_id' => 17074,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            7373 => [ // Cystenon - BA
                'offer_id' => 17072,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            7380 => [ // Gluconax - BA
                'offer_id' => 17079,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9354 => [ //Beauty Derm - MK
                'offer_id' => 9761,
                'country_code' => 'MK',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9355 => [ //Wrinkless - MK
                'offer_id' => 9759,
                'country_code' => 'MK',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9316 => [ // NutriMix - SI
                'offer_id' => 16969,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9315 => [ // NutriMix - HR
                'offer_id' => 16958,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9317 => [ // NutriMix - DE
                'offer_id' => 16967,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9318 => [ // NutriMix - DE  Austria
                'offer_id' => 16962,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9314 => [ // NutriMix - BG
                'offer_id' => 16970,
                'country_code' => 'BG',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9333 => [ // BreaUp - DE
                'offer_id' => 16968,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9334 => [ // BreaUp - DE    Austria
                'offer_id' => 16963,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9331 => [ // Liv Caps - DE
                'offer_id' => 16966,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9332 => [ // Liv Caps - DE    Austria
                'offer_id' => 16961,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9326 => [ // Dermal - BG
                'offer_id' => 7694,
                'country_code' => 'BG',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9328 => [ // Dermal - SI
                'offer_id' => 7663,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9327 => [ // Dermal - HR
                'offer_id' => 7657,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9329 => [ // Dermal - DE
                'offer_id' => 16964,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9330 => [ // Dermal - DE    Austria
                'offer_id' => 16959,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9323 => [ // Pro Caps - BG
                'offer_id' => 12696,
                'country_code' => 'BG',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9322 => [ // Pro Caps - SI
                'offer_id' => 12678,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9321 => [ // Pro Caps - HR
                'offer_id' => 12680,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9324 => [ // Pro Caps - DE
                'offer_id' => 16965,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9325 => [ // Pro Caps - AT
                'offer_id' => 16960,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9108 => [ // Power pulse XXL - BG
                'offer_id' => 16740,
                'country_code' => 'BG',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9107 => [ // Power pulse XXL - SI
                'offer_id' => 16738,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9106 => [ // Power pulse XXL - HR
                'offer_id' => 16739,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9105 => [ // Power pulse XXL - BA
                'offer_id' => 16925,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9082 => [ // BreaUp - BG
                'offer_id' => 16923,
                'country_code' => 'BG',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9083 => [ // BreaUp - SI
                'offer_id' => 16920,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9084 => [ // BreaUp - HR
                'offer_id' => 16917,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9081 => [ // BreaUp - BA
                'offer_id' => 16924,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9110 => [ // CY Relief - BG
                'offer_id' => 16922,
                'country_code' => 'BG',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9018 => [ // CY Relief - SI
                'offer_id' => 16919,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9017 => [ // CY Relief - HR
                'offer_id' => 16916,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9016 => [ // CY Relief - BA
                'offer_id' => 16927,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9111 => [ // Gastro ZEN - BG
                'offer_id' => 16921,
                'country_code' => 'BG',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9015 => [ // Gastro ZEN - SI
                'offer_id' => 16918,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9014 => [ // Gastro ZEN - HR
                'offer_id' => 16915,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9013 => [ // Gastro ZEN - BA
                'offer_id' => 16926,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9073 => [ // Parazol - RO
                'offer_id' => 16933,
                'country_code' => 'RO',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9072 => [ // Hypertea - RO
                'offer_id' => 16932,
                'country_code' => 'RO',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9077 => [ // TestoY - BA (low price)
                'network' => 177,
                'auth' => 'db6a2bcf15e2284072acd10863942337',
                'offer_id' => 16945,
                'country_code' => 'BA',
                
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9078 => [ // TestoY - RS (low price)
                'network' => 177,
                'auth' => 'db6a2bcf15e2284072acd10863942337',
                'offer_id' => 16946,
                'country_code' => 'RS',
                
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9079 => [ // TestoY - HR (low price)
                'network' => 177,
                'auth' => 'db6a2bcf15e2284072acd10863942337',
                'offer_id' => 16947,
                'country_code' => 'HR',
                
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9080 => [ // TestoY - ME (low price)
                'network' => 177,
                'auth' => 'db6a2bcf15e2284072acd10863942337',
                'offer_id' => 16948,
                'country_code' => 'ME',
                
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8969 => [ // Keto Coffee Premium - SI
                'offer_id' => 16877,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8865 => [ // Keto Coffee Premium - DE
                'offer_id' => 16875,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8967 => [ // Keto Coffee Premium - DE
                'offer_id' => 16876,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8968 => [ // Keto Coffee Premium - HR
                'offer_id' => 16878,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8985 => [ // Nautubone - BA
                'offer_id' => 9738,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8652 => [ // Eroboost - SI
                'offer_id' => 16685,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8650 => [ // Eroboost - DE
                'offer_id' => 16686,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8651 => [ // Eroboost - DE Austria
                'offer_id' => 16687,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8648 => [ // Eroboost - HR
                'offer_id' => 16688,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            6056 => [ // O Caps - TR
                'offer_id' => 7677,
                'country_code' => 'TR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8647 => [ // Nautubone - BG
                'offer_id' => 7689,
                'country_code' => 'BG',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8646 => [ // Nautubone - IT
                'offer_id' => 7710,
                'country_code' => 'IT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
			8740 => [ // BoneControl - RS (low price)
				'network' => 177,
				'auth' => 'db6a2bcf15e2284072acd10863942337',
				'offer_id' => 16645,
				'country_code' => 'RS',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8128 => [ // Varcosin - BA (low price)
				'network' => 177,
				'auth' => 'db6a2bcf15e2284072acd10863942337',
				'offer_id' => 15824,
				'country_code' => 'BA',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8129 => [ // Prostamid - BA (low price)
				'network' => 177,
				'auth' => 'db6a2bcf15e2284072acd10863942337',
				'offer_id' => 15823,
				'country_code' => 'BA',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8130 => [ // BoneControl - BA (low price)
				'network' => 177,
				'auth' => 'db6a2bcf15e2284072acd10863942337',
				'offer_id' => 15822,
				'country_code' => 'BA',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8131 => [ // Prostamid - RS (low price)
				'network' => 177,
				'auth' => 'db6a2bcf15e2284072acd10863942337',
				'offer_id' => 15827,
				'country_code' => 'RS',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8132 => [ // Nautubone - RS (low price)
				'network' => 177,
				'auth' => 'db6a2bcf15e2284072acd10863942337',
				'offer_id' => 15826,
				'country_code' => 'RS',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8133 => [ // Varcosin - RS (low price)
				'network' => 177,
				'auth' => 'db6a2bcf15e2284072acd10863942337',
				'offer_id' => 15825,
				'country_code' => 'RS',
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8134 => [ // Prostamid - MK (low price)
				'network' => 177,
				'auth' => 'db6a2bcf15e2284072acd10863942337',
				'offer_id' => 15829,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8135 => [ // Nautubone - MK (low price)
				'network' => 177,
				'auth' => 'db6a2bcf15e2284072acd10863942337',
				'offer_id' => 15828,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
            8201 => [ // Prostamid - SI (low price)
                'network' => 177,
                'auth' => 'db6a2bcf15e2284072acd10863942337',
                'offer_id' => 15978,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8200 => [ // Prostamid - HR (low price)
                'network' => 177,
                'auth' => 'db6a2bcf15e2284072acd10863942337',
                'offer_id' => 15979,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
			8675 => [ // Collagent - DE
                'offer_id' => 16630,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
			8674 => [ // Collagent - DE
                'offer_id' => 16629,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
			8673 => [ // Collagent - SI
                'offer_id' => 16628,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
			8672 => [ // Collagent - HR
                'offer_id' => 16627,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
			8558 => [ // GoldRevive - DE
                'offer_id' => 16161,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
			8549 => [ // GoldRevive - DE
                'offer_id' => 16160,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
			8559 => [ // GoldRevive - SI
                'offer_id' => 16159,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
			8548 => [ // GoldRevive - HR
                'offer_id' => 16158,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
			8429 => [ // Osteflex - DE
                'offer_id' => 16163,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
			8428 => [ // Osteflex - DE
                'offer_id' => 16162,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
			8430 => [ // Osteflex - SI
                'offer_id' => 16130,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
			8431 => [ // Osteflex - HR
                'offer_id' => 16131,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
			8551 => [ // Beauty Derm - XK
                'offer_id' => 16146,
                'country_code' => 'XK',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8550 => [ // Beauty Derm - AL
                'offer_id' => 5355,
                'country_code' => 'AL',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
			8246 => [ // Nautubone - SI
                'offer_id' => 7660,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8245 => [ // Nautubone - HR
                'offer_id' => 7655,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8411 => [ // Dia Caps - AT
                'offer_id' => 16100,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8410 => [ // Dia Caps - DE
                'offer_id' => 16101,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8286 => [ // Liv Caps - SI
                'offer_id' => 16004,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8285 => [ // Liv Caps - HR
                'offer_id' => 16005,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8284 => [ // Liv Caps - BG
                'offer_id' => 16006,
                'country_code' => 'BG',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8289 => [ // Dia Caps - BG
                'offer_id' => 16007,
                'country_code' => 'BG',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8288 => [ // Dia Caps - SI
                'offer_id' => 16002,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8287 => [ // Dia Caps - HR
                'offer_id' => 16003,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8311 => [ // Keto Tea - SI
                'offer_id' => 12710,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8310 => [ // Keto Tea - BG
                'offer_id' => 15990,
                'country_code' => 'BG',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8293 => [ // HKeto Tea - RS
                'offer_id' => 9744,
                'country_code' => 'RS',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8292 => [ // Keto Tea - BA
                'offer_id' => 9741,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8291 => [ // Keto Tea - HR
                'offer_id' => 12709,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8290 => [ // Keto Tea - ME
                'offer_id' => 15989,
                'country_code' => 'ME',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8283 => [ // Hyper Caps - AL
                'offer_id' => 12698,
                'country_code' => 'XK',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8282 => [ // Hyper Caps - AL
                'offer_id' => 5360,
                'country_code' => 'AL',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8255 => [ // Hemoroclear - AL
                'offer_id' => 12702,
                'country_code' => 'XK',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8189 => [ // Psoryden - AT
                'offer_id' => 15994,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8188 => [ // Psoryden - DE
                'offer_id' => 15993,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8186 => [ // Psoryden - SI
                'offer_id' => 15992,
                'country_code' => 'SI',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8187 => [ // Psoryden - HR
                'offer_id' => 15991,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8223 => [ // NefroAktiv - AT
                'offer_id' => 7526,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8222 => [ // NefroAktiv - DE
                'offer_id' => 7502,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8221 => [ // Mikoherb - AT
                'offer_id' => 15971,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8220 => [ // Mikoherb - DE
                'offer_id' => 15964,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8219 => [ // Hemoroclear - AT
                'offer_id' => 15976,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8218 => [ // Hemoroclear - DE
                'offer_id' => 15969,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8217 => [ // Beauty Derm - AT
                'offer_id' => 15973,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8216 => [ // Beauty Derm - DE
                'offer_id' => 15966,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8215 => [ // Varcosin - AT
                'offer_id' => 15972,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8214 => [ // Varcosin - DE
                'offer_id' => 15965,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8213 => [ // U Caps - AT
                'offer_id' => 15975,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8212 => [ // U Caps - DE
                'offer_id' => 15968,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8211 => [ // O Caps - AT
                'offer_id' => 15974,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8210 => [ // O Caps - DE
                'offer_id' => 15967,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8195 => [ // Nautubone - DE
                'offer_id' => 15963,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8196 => [ // Nautubone - AT
                'offer_id' => 15970,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8202 => [ // Testoy - ME
                'offer_id' => 12685,
                'country_code' => 'ME',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8136 => [ // Quick F - BA
                'offer_id' => 15806,
                'country_code' => 'BA',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8160 => [ // TestoY - DE
                'offer_id' => 7507,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8161 => [ // Hyper Caps - DE
                'offer_id' => 7505,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8162 => [ // KHyper Drops - DE
                'offer_id' => 7504,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8163 => [ // Prostamid - DE
                'offer_id' => 7501,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8164 => [ // Parazol - DE
                'offer_id' => 7500,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8165 => [ // Hypertea- DE
                'offer_id' => 7499,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8166 => [ //  Diatea - DE
                'offer_id' => 7497,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8167 => [ // GoSlim - DE
                'offer_id' => 7496,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            8168 => [ // Apolloss - DE
                'offer_id' => 7495,
                'country_code' => 'DE',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
			8151 => [ // Keto Probiotix - DE
				'offer_id' => 15833,
				'country_code' => 'DE',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8152 => [ // Keto Probiotix - AT
				'offer_id' => 15834,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4857 => [ // Dermal - BA
				'offer_id' => 5457,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8111 => [ // Keto Light + - DE
				'offer_id' => 14227,
				'country_code' => 'DE',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8105 => [ // Ostyhealth - MK
				'offer_id' => 15832,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7372 => [ // Reliver - DE
				'offer_id' => 15830,
				'country_code' => 'DE',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7371 => [ // Reliver - AT
				'offer_id' => 15831,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8103 => [ // Diatea - SI
				'offer_id' => 7662,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7872 => [ // Reliver - SI
				'offer_id' => 15818,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7871 => [ // Reliver - HR
				'offer_id' => 15817,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7873 => [ // Reliver - DE
				'offer_id' => 15819,
				'country_code' => 'DE',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7874 => [ // Reliver - AT
				'offer_id' => 15820,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7881 => [ // U Caps - TR
				'offer_id' => 15316,
				'country_code' => 'TR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7882 => [ // O Caps - TR
				'offer_id' => 10246,
				'country_code' => 'TR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8073 => [ // Beaty Derm - SI
				'offer_id' => 7665,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8072 => [ // Beaty Derm - HR
				'offer_id' => 7695,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7807 => [ // Dia Drops - BA
				'offer_id' => 5342,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7808 => [ // Mikoherb - HR
				'offer_id' => 7696,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7810 => [ // Pro Drops - BA
				'offer_id' => 5341,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7809 => [ // Varcosin - BA
				'offer_id' => 7682,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7707 => [ // Hemoroclear - HR
				'offer_id' => 15305,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7706 => [ // Hemoroclear - SI
				'offer_id' => 15306,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7705 => [ // Hemoroclear - AL
				'offer_id' => 9751,
				'country_code' => 'AL',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7704 => [ // Hemoroclear - ME
				'offer_id' => 12706,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7703 => [ // Hemoroclear - MK
				'offer_id' => 9758,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7225 => [ // Pro Caps - TR
				'offer_id' => 10249,
				'country_code' => 'TR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
            9319 => [ // Hyper Caps - HR
                'offer_id' => 4628,
                'country_code' => 'HR',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
            9320 => [ // Hyper Caps - DE    Austria
                'offer_id' => 7529,
                'country_code' => 'AT',
                'configs' => [
                    'brakeLogFolder' => true,
                ],
            ],
			7708 => [ // Air Oven - SI
				'offer_id' => 15308,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7709 => [ // CAir Oven - HR
				'offer_id' => 15307,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4862 => [ // Dermal - RS
				'offer_id' => 5458,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7672 => [ // Para Caps - RS
				'offer_id' => 15287,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7673 => [ // Varicone - DE
				'offer_id' => 14228,
				'country_code' => 'DE',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7674 => [ // Varicone - AT
				'offer_id' => 14226,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7637 => [ // Cardio A - DE
				'offer_id' => 15236,
				'country_code' => 'DE',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7638 => [ // Cardio A - AT
				'offer_id' => 15237,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7636 => [ // Cardio A - SI
				'offer_id' => 15235,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7635 => [ // Cardio A - HR
				'offer_id' => 15234,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7663 => [ // Keto Light + - ME
				'offer_id' => 15251,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7662 => [ // Keto Black - ME
				'offer_id' => 15250,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7661 => [ // Germivir - ME
				'offer_id' => 15252,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7660 => [ // Erectil - ME
				'offer_id' => 15254,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7659 => [ // OstyHealth - ME
				'offer_id' => 15249,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7658 => [ // Coralift - ME
				'offer_id' => 15248,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7657 => [ // Germivir 120g - ME
				'offer_id' => 15253,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7365 => [ // Visitec - DE
				'offer_id' => 15210,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7364 => [ // Visitec - DE
				'offer_id' => 15209,
				'country_code' => 'DE',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7362 => [ // Visitec - SI
				'offer_id' => 14709,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7361 => [ // Visitec - HR
				'offer_id' => 14706,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7594 => [ // Germivir 120g - BA
				'offer_id' => 15202,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7593 => [ // Coralift - BA
				'offer_id' => 15197,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7592 => [ // OstyHealth - BA
				'offer_id' => 15198,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7591 => [ // Erectil - BA
				'offer_id' => 15203,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7590 => [ // Germivir - BA
				'offer_id' => 15201,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7589 => [ // Keto Black - BA
				'offer_id' => 15199,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7588 => [ // Keto Light + - BA
				'offer_id' => 15200,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7368 => [ // Cystenon - HR
				'offer_id' => 14707,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7369 => [ // Cystenon - SI
				'offer_id' => 14710,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7375 => [ // Gluconax - HR
				'offer_id' => 14708,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7376 => [ // Gluconax - SI
				'offer_id' => 14711,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7378 => [ // Gluconax - DE
				'offer_id' => 14712,
				'country_code' => 'DE',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7379 => [ // Gluconax - AT
				'offer_id' => 14713,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7232 => [ // Testoy Gel - RS
				'offer_id' => 12758,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7231 => [ // Testoy Gel - MK
				'offer_id' => 14224,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7230 => [ // Testoy Gel - ME
				'offer_id' => 14223,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7229 => [ // Testoy Gel - AL Kosovo
				'offer_id' => 14225,
				'country_code' => 'XK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7228 => [ // Testoy Gel - AL
				'offer_id' => 12760,
				'country_code' => 'AL',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6576 => [ // TestoY - MK
				'offer_id' => 5333,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7351 => [ // Quickly Fitness - MK
				'offer_id' => 9753,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7210 => [ // Germivir 120g - SI
				'offer_id' => 14113,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7209 => [ // Germivir 120g - HR
				'offer_id' => 14114,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7212 => [ // Germivir 120g - DE - AT
				'offer_id' => 14116,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7211 => [ // Germivir 120g - DE
				'offer_id' => 14115,
				'country_code' => 'DE',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7212 => [ // CGermivir 120g - DE - AT
				'offer_id' => 14116,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7211 => [ // Germivir 120g - DE
				'offer_id' => 14115,
				'country_code' => 'DE',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5964 => [ // Cardio Life - DE
				'offer_id' => 14193,
				'country_code' => 'DE',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7267 => [ // Varicone - MK
				'offer_id' => 14181,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7266 => [ // Fungent - MK
				'offer_id' => 14180,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7265 => [ // Coralift - MK
				'offer_id' => 14179,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7264 => [ // Big Size - MK
				'offer_id' => 14178,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6818 => [ // Deep Inhale - MK
				'offer_id' => 5330,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6819 => [ // Hyper Drops- MK
				'offer_id' => 5327,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6639 => [ // Dia Drops - MK
				'offer_id' => 5334,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6820 => [ // Nautubone - MK
				'offer_id' => 5325,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6618 => [ // Apolloss - MK
				'offer_id' => 5331,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6617 => [ // Pro Drops - MK
				'offer_id' => 5335,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7001 => [ // Classic Collagen Coffee - BA
				'offer_id' => 12716,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6991 => [ // Testoy Gel - BA
				'offer_id' => 12756,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6990 => [ // Matcha Powder - BA
				'offer_id' => 12715,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6814 => [ // NefroAktiv - ME
				'offer_id' => 12693,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6816 => [ // O caps - AL
				'offer_id' => 9747,
				'country_code' => 'AL',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6817 => [ // O caps - XK
				'offer_id' => 12686,
				'country_code' => 'XK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6642 => [ // Prostanol - DE
				'offer_id' => 13727,
				'country_code' => 'DE',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6643 => [ // Prostanol - DE
				'offer_id' => 13728,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6646 => [ // Fungent - DE
				'offer_id' => 13714,
				'country_code' => 'DE',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6647 => [ // Fungent - DE
				'offer_id' => 13715,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7117 => [ // Varicone - SI
				'offer_id' => 13708,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7116 => [ // Varicone - HR
				'offer_id' => 13706,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7009 => [ // Keto Probiotic - SI
				'offer_id' => 13707,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7008 => [ // Keto Probiotic - HR
				'offer_id' => 13705,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7040 => [ // Testox - TR
				'offer_id' => 7684,
				'country_code' => 'TR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7006 => [ // U Caps - SI
				'offer_id' => 12882,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7005 => [ // U Caps - RS
				'offer_id' => 12712,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7004 => [ // U Caps - ME
				'offer_id' => 12720,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7003 => [ // U Caps - HR
				'offer_id' => 12881,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			7002 => [ // U Caps - BA
				'offer_id' => 12717,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6813 => [ // Fungent - MK
				'offer_id' => 12697,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6812 => [ // Fungent - RS
				'offer_id' => 7671,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6811 => [ // Fungent - BA
				'offer_id' => 12754,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6644 => [ // Fungent - SI
				'offer_id' => 11794,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6645 => [ // Fungent - HR
				'offer_id' => 11793,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6641 => [ // Prostanol - SI
				'offer_id' => 11528,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6640 => [ // Prostanol - HR
				'offer_id' => 11529,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6344 => [ // Big Size - DE
				'offer_id' => 11502,
				'country_code' => 'DE',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6626 => [ // Mikoherb - MK
				'offer_id' => 5328,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6627 => [ // Mikotea - MK
				'offer_id' => 5329,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6163 => [ // OstyHealth - BG
				'offer_id' => 8949,
				'country_code' => 'BG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5333 => [ // Keto Black - BG
				'offer_id' => 8951,
				'country_code' => 'BG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5277 => [ // Germivir - BG
				'offer_id' => 8953,
				'country_code' => 'BG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6266 => [ // Diet Drops - BG
				'offer_id' => 8955,
				'country_code' => 'BG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5372 => [ // Fungostop + - BG
				'offer_id' => 8956,
				'country_code' => 'BG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5275 => [ // Germivir - DE
				'offer_id' => 10040,
				'country_code' => 'DE',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5338 => [ // Keto Black - DE
				'offer_id' => 10041,
				'country_code' => 'DE',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6574 => [ // O Caps - ES
				'offer_id' => 9722,
				'country_code' => 'ES',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6573 => [ // O Caps - IT
				'offer_id' => 10038,
				'country_code' => 'IT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6611 => [ // GoPotent - MK
				'offer_id' => 5323,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6610 => [ // Omnipotent - MK
				'offer_id' => 233,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6616 => [ // NefroAktiv - MK
				'offer_id' => 5332,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6609 => [ // Hemomax - MK
				'offer_id' => 5009,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6607 => [ // Dermal - MK
				'offer_id' => 5326,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6605 => [ // EasySlim - MK
				'offer_id' => 232,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6604 => [ // GoSlim - MK
				'offer_id' => 5322,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6606 => [ // Varcosin - MK
				'offer_id' => 5324,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6572 => [ // O Caps - RS
				'offer_id' => 6957,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6571 => [ // O Caps - MK
				'offer_id' => 8986,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6570 => [ // O Caps - BA
				'offer_id' => 6952,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6569 => [ // O Caps - HR
				'offer_id' => 7686,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6568 => [ // O Caps - HU
				'offer_id' => 9721,
				'country_code' => 'HU',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6567 => [ // O Caps - BG
				'offer_id' => 7688,
				'country_code' => 'BG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6566 => [ // O Caps - SI
				'offer_id' => 7685,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6554 => [ // O Caps - ME
				'offer_id' => 7687,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5327 => [ // KKeto Black - ES
				'offer_id' => 9728,
				'country_code' => 'ES',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2952 => [ // Keto Light + - ES
				'offer_id' => 9725,
				'country_code' => 'ES',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5274 => [ // Germivir - ES
				'offer_id' => 9724,
				'country_code' => 'ES',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5624 => [ // Variforce - ES
				'offer_id' => 9723,
				'country_code' => 'ES',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3970 => [ // Psorizin - MK
				'offer_id' => 228,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5961 => [ // Diabex - AT
				'offer_id' => 9319,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5962 => [ // Cardio Life - AT
				'offer_id' => 9320,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6165 => [ // OstyHealth - AT
				'offer_id' => 9321,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5608 => [ // Coralift - AT
				'offer_id' => 9322,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5340 => [ // Keto Black - AT
				'offer_id' => 9323,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6345 => [ // Big Size - AT
				'offer_id' => 9324,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5276 => [ // Germivir - AT
				'offer_id' => 9325,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5623 => [ // Variforce - AT
				'offer_id' => 9326,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6265 => [ // Diet Drops - AT
				'offer_id' => 9327,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5369 => [ // Fungostop + - AT
				'offer_id' => 9328,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3478 => [ // Keto Light + - AT
				'offer_id' => 9329,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5629 => [ // Variforce - HU
				'offer_id' => 8965,
				'country_code' => 'HU',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2778 => [ // Keto Light + - HU
				'offer_id' => 8968,
				'country_code' => 'HU',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5374 => [ // Fungostop - HU
				'offer_id' => 8967,
				'country_code' => 'HU',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5950 => [ // Diabex - HU
				'offer_id' => 8958,
				'country_code' => 'HU',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5951 => [ // Cardio Life - HU
				'offer_id' => 8959,
				'country_code' => 'HU',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5959 => [ // Diabex - HR
				'offer_id' => 8905,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5960 => [ // Cardio Life - HR
				'offer_id' => 8906,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6168 => [ // OstyHealth - HR
				'offer_id' => 8907,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5613 => [ // Coralift - HR
				'offer_id' => 8908,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5336 => [ // Keto Black - HR
				'offer_id' => 8909,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6153 => [ // Big Size - HR
				'offer_id' => 8910,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5267 => [ // Germivir - HR
				'offer_id' => 8911,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5628 => [ // Variforce - HR
				'offer_id' => 8912,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6272 => [ // Diet Drops - HR
				'offer_id' => 8913,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5370 => [ // Fungostop - HR
				'offer_id' => 8914,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2954 => [ // Keto Light - HR
				'offer_id' => 8915,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6166 => [ // OstyHealth - DE
				'offer_id' => 8987,
				'country_code' => 'DE',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5622 => [ // Variforce - DE
				'offer_id' => 8988,
				'country_code' => 'DE',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5376 => [ // Fungostop + - DE
				'offer_id' => 8989,
				'country_code' => 'DE',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5956 => [ // CDiabex - SI
				'offer_id' => 8916,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5958 => [ // Cardio Life - SI
				'offer_id' => 8917,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6183 => [ // OstyHealth - SI
				'offer_id' => 8918,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5618 => [ // Coralift - SI
				'offer_id' => 8919,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5337 => [ // Keto Black - SI
				'offer_id' => 8920,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6155 => [ // Big Size - SI
				'offer_id' => 8921,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5273 => [ // Germivir - SI
				'offer_id' => 8922,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5633 => [ // Variforce - SI
				'offer_id' => 8923,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6278 => [ // Diet Drops - SI
				'offer_id' => 8924,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5371 => [ // Fungostop + - SI
				'offer_id' => 8925,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2958 => [ // eto Light + - SI
				'offer_id' => 8926,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5026 => [ // Erectil - HU
				'offer_id' => 7480,
				'country_code' => 'HU',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5284 => [ // Erectil - DE
				'offer_id' => 8888,
				'country_code' => 'DE',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5285 => [ // Erectil - AT
				'offer_id' => 8886,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5282 => [ // Erectil - HR
				'offer_id' => 7478,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5283 => [ // Erectil - SI
				'offer_id' => 7479,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5499 => [ // Quick Fit - HU
				'offer_id' => 4643,
				'country_code' => 'HU',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6042 => [ // Hypertea - AT
				'offer_id' => 7522,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6043 => [ // Parazol - AT
				'offer_id' => 7523,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6044 => [ // Prostamid - AT
				'offer_id' => 7524,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6039 => [ // GoSlim - AT
				'offer_id' => 7519,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6040 => [ // Diatea - AT
				'offer_id' => 7520,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6038 => [ // Apolloss - AT
				'offer_id' => 7525,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6047 => [ // Hyper Drops - AT
				'offer_id' => 7528,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6048 => [ // Hyper Caps - AT
				'offer_id' => 7529,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6050 => [ // TestoY - AT
				'offer_id' => 7531,
				'country_code' => 'AT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6057 => [ // G-Tea - TR
				'offer_id' => 7544,
				'country_code' => 'TR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6056 => [ // H-Tea - TR
				'offer_id' => 7543,
				'country_code' => 'TR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6055 => [ // D-Tea
				'offer_id' => 7542,
				'country_code' => 'TR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5513 => [ // Hyper caps
				'offer_id' => 4632,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5505 => [ // Hyper caps
				'offer_id' => 4637,
				'country_code' => 'ES',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5501 => [ // Hyper caps
				'offer_id' => 4642,
				'country_code' => 'HU',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5510 => [ // Hyper Drops
				'offer_id' => 4627,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5504 => [ // Hyper Drops
				'offer_id' => 4636,
				'country_code' => 'ES',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5497 => [ // Hyper Drops
				'offer_id' => 4641,
				'country_code' => 'HU',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5512 => [ // Dia Drops
				'offer_id' => 4630,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5509 => [ // Dia Drops
				'offer_id' => 4626,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5503 => [ // Dia Drops
				'offer_id' => 4635,
				'country_code' => 'ES',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5495 => [ // Dia Drops
				'offer_id' => 4640,
				'country_code' => 'HU',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5502 => [ // Apolloss
				'offer_id' => 2592,
				'country_code' => 'ES',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5491 => [ // Apolloss
				'offer_id' => 4563,
				'country_code' => 'HU',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5515 => [ // TestoY
				'offer_id' => 4634,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5511 => [ // TestoY
				'offer_id' => 4629,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5507 => [ // TestoY
				'offer_id' => 4639,
				'country_code' => 'ES',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5493 => [ // TestoY
				'offer_id' => 4644,
				'country_code' => 'HU',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5729 => [ // Wrinkless
				'offer_id' => 6950,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5727 => [ // Hemoroclear
				'offer_id' => 6951,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5728 => [ // Wrinkless
				'offer_id' => 6956,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5726 => [ // Hemoroclear
				'offer_id' => 6955,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5676 => [ // Prostamid - BG
				'offer_id' => 5979,
				'country_code' => 'BG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5677 => [ // Parazol
				'offer_id' => 5980,
				'country_code' => 'BG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5678 => [ // GoSlim
				'offer_id' => 5981,
				'country_code' => 'BG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5679 => [ // TestoY
				'offer_id' => 5982,
				'country_code' => 'BG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5680 => [ // Hyper Drops
				'offer_id' => 5983,
				'country_code' => 'BG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5681 => [ // Quickly Fitness
				'offer_id' => 5984,
				'country_code' => 'BG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5682 => [ // Pro Drops
				'offer_id' => 5985,
				'country_code' => 'BG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5683 => [ // Dia Drops
				'offer_id' => 5986,
				'country_code' => 'BG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5684 => [ // Hyper Caps
				'offer_id' => 5987,
				'country_code' => 'BG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5685 => [ // GoPotent
				'offer_id' => 5988,
				'country_code' => 'BG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5687 => [ // Apolloss
				'offer_id' => 5989,
				'country_code' => 'BG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5658 => [ // Hypertea
				'offer_id' => 5977,
				'country_code' => 'BG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5659 => [ // Diatea
				'offer_id' => 5978,
				'country_code' => 'BG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5660 => [ // Prostamid - ME
				'offer_id' => 6559,
				'country_code' => 'BG',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3961 => [ // Prostamid - ME
				'offer_id' => 1017,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3956 => [ // GoSlim - ME
				'offer_id' => 1012,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3957 => [ // Parazol - ME
				'offer_id' => 1013,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3958 => [ // GoPotent - ME
				'offer_id' => 1014,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3959 => [ // Varcosin - ME
				'offer_id' => 1015,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3960 => [ // Nautubone - ME
				'offer_id' => 1016,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3962 => [ // Hypertea - ME
				'offer_id' => 1018,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3963 => [ // Diatea - ME
				'offer_id' => 1019,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3964 => [ //  Dermal - ME
				'offer_id' => 1020,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3626 => [ // Diatea - MK
				'offer_id' => 234,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3658 => [ // Parazol - BA
				'offer_id' => 669,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3657 => [ // Prostamid - BA
				'offer_id' => 670,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3565 => [ //Parazol - MK
				'offer_id' => 237,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3543 => [ //DiaTea - AL
				'offer_id' => 215,
				'country_code' => 'AL',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2835 => [ //Celluless - BA
				'offer_id' => 28,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3329 => [ //Omnipotent - RS
				'offer_id' => 124,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2836 => [ //Psorizin - BA
				'offer_id' => 31,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2860 => [ //Omnipotent - BA
				'offer_id' => 30,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2861 => [ //EasySlim - BA
				'offer_id' => 29,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2995 => [ //Varcolex - BA
				'offer_id' => 33,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2994 => [ //BoneControl - BA
				'offer_id' => 28,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3042 => [ //ImunoMax - BA
				'offer_id' => 32,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3144 => [ //Hypertea - BA
				'offer_id' => 34,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3158 => [ //Varcolex - RS
				'offer_id' => 55,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3159 => [ //BoneControl - RS
				'offer_id' => 57,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3292 => [ //Hypertea - RS
				'offer_id' => 65,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3297 => [ //Parazol - RS
				'offer_id' => 59,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3298 => [ //Prostamid - RS
				'offer_id' => 62,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3305 => [ //EasySlim - RS
				'offer_id' => 60,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3329 => [ //Omnipotent - RS
				'offer_id' => 63,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3463 => [ //Diatea - BA
				'offer_id' => 124,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3564 => [ //Hypertea - MK
				'offer_id' => 235,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3567 => [ //Diatea - RS
				'offer_id' => 251,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3566 => [ //Prostamid - MK
				'offer_id' => 236,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3766 => [ //Pure Argan Oil - BA
				'offer_id' => 667,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4155 => [ //Varcolex - ME
				'offer_id' => 1254,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4156 => [ //BoneControl - ME
				'offer_id' => 1255,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4213 => [ //Mikoherb - BA
				'offer_id' => 1366,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4217 => [ //DiaTea - AL Kosovo
				'offer_id' => 1451,
				'country_code' => 'XK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4100 => [ //Varcolex - AL Kosovo
				'offer_id' => 1256,
				'country_code' => 'XK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4097 => [ //BoneControl - AL Kosovo
				'offer_id' => 1257,
				'country_code' => 'XK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4099 => [ //Varcolex - AL
				'offer_id' => 222,
				'country_code' => 'AL',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4096 => [ //BoneControl - AL
				'offer_id' => 221,
				'country_code' => 'AL',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4221 => [ //HyperTea - AL
				'offer_id' => 216,
				'country_code' => 'AL',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4224 => [ //HyperTea - AL   Kosovo
				'offer_id' => 1502,
				'country_code' => 'XK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4229 => [ //Prostamid - AL
				'offer_id' => 219,
				'country_code' => 'AL',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4225 => [ //Parazol - AL
				'offer_id' => 220,
				'country_code' => 'AL',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4227 => [ //Omnipotent - AL
				'offer_id' => 218,
				'country_code' => 'AL',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4231 => [ //EasySlim - AL
				'offer_id' => 217,
				'country_code' => 'AL',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4222 => [ //#3189 - LossLess Shampoo - RS
				'offer_id' => 1367,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4223 => [ //LossLess Shampoo - BA
				'offer_id' => 1365,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4232 => [ //EasySlim
				'offer_id' => 2181,
				'country_code' => 'XK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4230 => [ //Prostamid
				'offer_id' => 2180,
				'country_code' => 'XK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4228 => [ //Omnipotent
				'offer_id' => 2182,
				'country_code' => 'XK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4226 => [ //Parazol
				'offer_id' => 2179,
				'country_code' => 'XK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4327 => [ //Prostamid
				'offer_id' => 1995,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4326 => [ //Parazol
				'offer_id' => 1994,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4325 => [ //Hypertea
				'offer_id' => 1996,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4324 => [ //GoSlim
				'offer_id' => 1993,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4381 => [ //Hypertea
				'offer_id' => 2319,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4372 => [ //Prostamid
				'offer_id' => 2318,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4379 => [ //Parazol
				'offer_id' => 2316,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4392 => [ //GoSlim
				'offer_id' => 2315,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4354 => [ //LossLess Shampoo
				'offer_id' => 2193,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4380 => [ //OmniPotent
				'offer_id' => 1997,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2902 => [ //Cardio NRJ
				'offer_id' => 1409,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4416 => [ //Mikoherb - RS
				'offer_id' => 2398,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4415 => [ //Mikoherb - ME
				'offer_id' => 2399,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4419 => [ //Lossless Shampoo - AL KOsovo
				'offer_id' => 2190,
				'country_code' => 'XK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4403 => [ //Lossless Shampoo - MK
				'offer_id' => 2192,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4402 => [ //Lossless Shampoo - AL
				'offer_id' => 2191,
				'country_code' => 'AL',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2907 => [ //Cardio NRJ - SI
				'offer_id' => 2528,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4548 => [ //Diatea - HR
				'offer_id' => 2566,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2024 => [ //Choco Lite - HR
				'offer_id' => 1408,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4098 => [ //BoneControl - MK
				'offer_id' => 229,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4101 => [ //Varcolex - MK
				'offer_id' => 230,
				'country_code' => 'MK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4710 => [ //TestoX
				'offer_id' => 2730,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4709 => [ //TestoX
				'offer_id' => 2729,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4698 => [ //Prostamid - ES
				'offer_id' => 2591,
				'country_code' => 'ES',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4697 => [ //Parazol - ES
				'offer_id' => 2590,
				'country_code' => 'ES',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4696 => [ //Diatea - ES
				'offer_id' => 2587,
				'country_code' => 'ES',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4695 => [ //Hypertea - ES
				'offer_id' => 2589,
				'country_code' => 'ES',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4694 => [ //GoSlim - ES
				'offer_id' => 2586,
				'country_code' => 'ES',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4693 => [ //GoPotent - ES
				'offer_id' => 2588,
				'country_code' => 'ES',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4684 => [ //Prostamid - IT
				'offer_id' => 2582,
				'country_code' => 'IT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4683 => [ //Parazol - IT
				'offer_id' => 2583,
				'country_code' => 'IT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4682 => [ //GoSlim - IT
				'offer_id' => 2578,
				'country_code' => 'IT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4681 => [ //GoPotent - IT
				'offer_id' => 2580,
				'country_code' => 'IT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4680 => [ //Hypertea - IT
				'offer_id' => 2581,
				'country_code' => 'IT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4679 => [ //Diatea - IT
				'offer_id' => 2579,
				'country_code' => 'IT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			1932 => [ //Cardio NRJ - ES
				'offer_id' => 3987,
				'country_code' => 'ES',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			1931 => [ //Cardio NRJ - ES
				'offer_id' => 4250,
				'country_code' => 'IT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5184 => [ //NefroAktiv - HU
				'offer_id' => 4565,
				'country_code' => 'HU',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5183 => [ //NefroAktiv - ES
				'offer_id' => 2593,
				'country_code' => 'ES',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5182 => [ //NefroAktiv - IT
				'offer_id' => 2585,
				'country_code' => 'IT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5181 => [ //NefroAktiv - SI
				'offer_id' => 4985,
				'country_code' => 'SI',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5180 => [ //NefroAktiv - HR
				'offer_id' => 4984,
				'country_code' => 'HR',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5144 => [ //NefroAktiv - RS
				'offer_id' => 4516,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5179 => [ //NefroAktiv - BA
				'offer_id' => 4986,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4878 => [ //Beauty Derm - RS
				'offer_id' => 2825,
				'country_code' => 'RS',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4877 => [ //Beauty Derm - ME
				'offer_id' => 2826,
				'country_code' => 'ME',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4863 => [ //Beauty Derm - BA
				'offer_id' => 2822,
				'country_code' => 'BA',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5362 => [ //Hypertea - HU
				'offer_id' => 4558,
				'country_code' => 'HU',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5359 => [ //Diatea - HU
				'offer_id' => 4559,
				'country_code' => 'HU',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5364 => [ //Prostamid - HU
				'offer_id' => 4560,
				'country_code' => 'HU',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5363 => [ //Parazol - HU
				'offer_id' => 4561,
				'country_code' => 'HU',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5361 => [ //GoSlim - HU
				'offer_id' => 4562,
				'country_code' => 'HU',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5360 => [ //GoPotent - HU
				'offer_id' => 4564,
				'country_code' => 'HU',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5451 => [ //Dermal - AL
				'offer_id' => 5383,
				'country_code' => 'AL',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5452 => [ //Dermal - XK
				'offer_id' => 5313,
				'country_code' => 'XK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5482 => [ //Gopotent - AL
				'offer_id' => 5450,
				'country_code' => 'AL',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5483 => [ //Gopotent - XK
				'offer_id' => 5309,
				'country_code' => 'XK',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5490 => [ //Apolloss - IT
				'offer_id' => 2584,
				'country_code' => 'IT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5492 => [ //TestoY - IT
				'offer_id' => 5296,
				'country_code' => 'IT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5494 => [ //Dia Drops - - IT
				'offer_id' => 5297,
				'country_code' => 'IT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5496 => [ //Hyper Drops - IT
				'offer_id' => 5298,
				'country_code' => 'IT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5498 => [ //Quick Fit - IT - IT
				'offer_id' => 5299,
				'country_code' => 'IT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5500 => [ //Hyper Caps - IT
				'offer_id' => 5300,
				'country_code' => 'IT',
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'trash' => 'Invalid Date, Duplicate Order',
				8 => 'trash',
			],
			'reject' => [
				'cancelled' => 'User declined after conﬁrming the order ',
			],
			'expect' => [
				'hold' => 'action "In process" (decision is not taken yet, you need request the status later)',
			],
			'confirm' => [
				'buyout_approved' => 'Order received',
				'buyout_hold' => 'Order in transport',
				'confirmed' => 'Order Confirmed',
				'buyout_rejected' => 'Order returned',
			],
		],
		
		'brakeLogFolder' => true,
		'urlOrderAdd' => 'https://cod.monetizead.com/api/v1/order/create/',
		'urlOrderInfo' => 'https://cod.monetizead.com/api/v1/order/status/',
//		'statusEnabled'		=> false,
	],
];
