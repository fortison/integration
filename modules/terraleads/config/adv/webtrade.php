<?php

/*
 * http://connectcpa.net/terraleads/adv/adv-postback-status?postbackApiKey=b6f21d91136ed40d81c7c4a64dd00000&systemUserId=41501&systemOrderId={transaction_id}&status={status}
*/

return [
	41501 => [
		'terraleadsApiKey'			=> 'b72d8e26c1cca075feaa6e9bf6b4a52b',
		'postbackApiKey'			=> 'b6f21d91136ed40d81c7c4a64dd00000',
		'partnerID' 				=> 8,
		
		'offers' => [
			5701 => [// Flexoren - RS
				'product' => 65,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5702 => [// Titan Gel Gold - RS
				'product' => 62,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5703 => [// Varikosette - RS
				'product' => 61,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5704 => [// Clavosan - RS
				'product' => 66,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5705 => [// InnoGialuron - RS
				'product' => 63,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5706 => [// UpSize - RS
				'product' => 64,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'		=> '',
			],
			'reject'	=> [
				'reject'	=> '',
				'cancelled'	=> '',
			],
			'expect'	=> [
				'expect'	=> '',
				'hold'	=> '',
				'uncalled'	=> '',
				'recall'	=> '',
			],
			'confirm'	=> [
				'confirm'	=> '',
				'confirmed'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://webtrade.rs/insert_purchase',
		'urlOrderInfo'		=> '',
	],
];

?>