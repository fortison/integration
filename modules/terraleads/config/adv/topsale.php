<?php

/*
 *
*/

return [
    26018 => [
        'terraleadsApiKey'			=> 'f69f20b6bfd15541899603af5091d922',
        'pid'	            		=> '93a542e7432235fea95a5373b7cef0ed',
        'offers' => [
            3435 => [
                "product_id" => 674,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            3749 => [ //Razorless Shaving - RO
                "product_id" => 726,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3747 => [ //Wild Tornado Cleaner - RO
                "product_id" => 724,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3748 => [ //Leather Repair - RO
                "product_id" => 725,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3750 => [ //Massage Mat with Pillow - RO
                "product_id" => 727,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3924 => [ //Oximeter - RO
                "product_id" => 728,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3925 => [ //GT08 Watch - RO
                "product_id" => 730,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],   
        ],
    ],

    'configs' => [
        'statuses' => [
            'trash'	=> [
                'trash' 		=> '',
                'problem' 		=> '',
                'return' 		=> '',
                'error_delivery' 		=> '',
                'repeat' 		=> '',
            ],
            'reject'	=> [
                'reject'    => '',
                'cancel'    => '',
            ],
            'expect'	=> [
                'cancel'   		 => '',
                'new'   		 => '',
                'callback'   		 => '',
            ],
            'confirm'	=> [
                'confirm'    	=> '',
                'ordered'    	=> '',
                'paid'    	=> '',
                'in_delivery'    	=> '',

            ],
        ],

        'brakeLogFolder'	=> true,
        'urlOrderAdd'		=> 'http://api.top-sale.online/api2/createorder',
        'urlOrderInfo'		=> 'http://api.top-sale.online/api2/order',
//		'statusEnabled'		=> false,
    ],
];