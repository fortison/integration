<?php


return [
	
	35727 => [
		'terraleadsApiKey' => '3faf1845633e344dfa8664502b5216e1',
		'apiKey' => '986658748d0428ca84f20aaac326e430',
		'ref' => '927492',
		
		'offers' => [
			5954 => [
				'offerId' => 11340,
				"langCode" => "GT",
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5955 => [
				'offerId' => 11758,
				"langCode" => "GT",
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5700 => [
				'offerId' => 11758,
				"langCode" => "EC",
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5699 => [
				'offerId' => 11340,
				"langCode" => "EC",
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4998 => [
				'offerId' => 11422,
				"langCode" => "CL",
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			4999 => [
				'offerId' => 11340,
				"langCode" => "CO",
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5001 => [
				'offerId' => 11332,
				"langCode" => "CO",
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5004 => [
				'offerId' => 11327,
				"langCode" => "CL",
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5000 => [
				'offerId' => 11339,
				"langCode" => "MX",
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5002 => [
				'offerId' => 11330,
				"langCode" => "CL",
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5003 => [
				'offerId' => 11331,
				"langCode" => "MX",
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5005 => [
				'offerId' => 11329,
				"langCode" => "CO",
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			5006 => [
				'offerId' => 11328,
				"langCode" => "MX",
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		],
		'configs' => [
			'statuses' => [
				'reject' => [
					2 => '',
					4 => '',
				],
				'expect' => [
					0 => '',
				],
				'trash' => [
				],
				'confirm' => [
					1 => '',
					3 => '',
				],
			],
			
			'brakeLogFolder' => true,
			'bridgeRequestTimeout' => 60,
			'statusRequestTimeout' => 60,
			'urlOrderAdd' => 'http://m1.top/send_order/',
			'urlOrderInfo' => 'http://m1.top/get_order_status/',
		],
	],
];

