<?php

/**
 * Sending orders:
 *
 * Link: http://192.168.11.185/leadpush_api/leadpush.php
 *
 * API_KEY(Constant Values Mandatory): 697c986b-e4af-47e1-a808-16bec7f92563
 *
 * List_Id(Constant Values, Mandatory): 1703202303
 * Client: Terralead
 *
 * Sample Payload: [
 *
 * {
 * "list_id": "1703202303",
 * "name": "PoPa+Pohns",
 * "order_id": "24403162",
 * "goods_id": "5252",
 * "phone": "7356099021",
 * "api_key": "697c986b-e4af-47e1-a808-16bec7f92563"
 *
 * },
 * {
 * "list_id": "1703202303",
 * "name": "PoPa+Pohns",
 * "order_id": "24403163",
 * "goods_id": "5253",
 * "phone": "7356099021",
 * "api_key": "697c986b-e4af-47e1-a808-16bec7f92563"
 * }
 * ]
 *
 * Check orders status:

 * Link: http://192.168.11.185/getorderid.php?ids=24403156,24403157,24403158,767834
 */

return [
	51043 => [
		'terraleadsApiKey'	=> '5e3b19ee0ff9365d3c4d2a3076091420',
		'api_key' 			=> '697c986b-e4af-47e1-a808-16bec7f92563',
		'list_id' 			=> 1703202303,
		
		'offers' => [
			5944 => [// Optivisol - PH
				'goods_id' => 001,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7091 => [// homo Erectus - PH
				'goods_id' => 003,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7392 => [//Asumedol - PH
				'goods_id' => 002,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7424 => [//Malower - PH
				'goods_id' => 006,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7425 => [//Diocator - PH
				'goods_id' => 004,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash'	=> [
					'Trash'		=> '',
					'trash'		=> '',
				],
				'reject'	=> [
					'Rejected Order'	=> '',
					'reject order'	=> '',
				],
				'expect'	=> [
					'Hold'	=> '',
					'hold'	=> '',
					'expect'	=> '',
					'NEW'	=> '',
					'New'	=> '',
					'new'	=> '',
					'VDAD'	=> '',
					'Null'	=> '',
					'null'	=> '',
					'NULL'	=> '',
					'Busy'	=> '',
					'busy'	=> '',
					'BUSY'	=> '',
					'VDAD'	=> '',

				],
				'confirm'	=> [
					'Confirmed'	=> '',
					'confirmed'	=> '',
				],
			],
			'brakeLogFolder'	=> true,
			'urlOrderAdd'		=> 'http://43.254.43.137:62870/leadpush_api/leadpush.php',
			'urlOrderInfo'		=> 'http://43.254.43.137:62870/getorderid.php',
		]
	]
];

?>