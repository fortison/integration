<?php

/*
 * https://confluence.adcombo.com/display/DOCS/Outgoing+orders+REST+API#OutgoingordersRESTAPI-Request
 */

return [
	
	20449 => [//test
		'terraleadsApiKey'			=> '27541ae11eb9fd6e8b88b708515ad540',
		'apiKey' 					=> '5f6ee515f69fabbfb5d00eb2aae17ed9',
		
		'offers' => [
			2616 => [
				'goodsId' => 3,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2758 => [
				'goodsId' => 'Garcinia',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	17559 => [
		'terraleadsApiKey'			=> '1550e1e2dba9f92b80ce17741464d6d5',
		'apiKey' 					=> 'jdsgksgud756jfgfus75tagf8w4f85529c3',
		
		'offers' => [
			0000 => [
				'goodsId' => 3,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'				=> '',
			],
			'reject'	=> [
				'cancelled'			=> '',
			],
			'expect'	=> [
				'hold'				=> '',
			],
			'confirm'	=> [
				'confirmed'			=> '',
				'buyout_approved'	=> '',
				'buyout_hold'		=> '',
				'buyout_rejected'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'http://apitest.preptech.online/api/post/v1',
		'urlOrderInfo'		=> 'http://apitest.preptech.online/api/get/v1',
	],
];