<?php

/*
	В чате

	http://connectcpa.net/terraleads/adv/adv-postback-status?partnerOrderId={id_in_your_system}systemUserId=23923&partnerStatus={status_lead}&postbackApiKey=ee1379d131da192dcb6bd163d0d5599s
 */

return [
	23923 => [
		'terraleadsApiKey'			=> 'ee1379d131da192dcb6bd163d0d5599a',
		'postbackApiKey'			=> 'ee1379d131da192dcb6bd163d0d5599s',
		
		'offers' => [
			3088 => [
				'product_id' => 2019117,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3089 => [
				'product_id' => 2019118,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'confirm'	=> [
				'approve' => '',
				'dapsidesaccepted' => '',
				'dapsidesreceived' => '',
				'dapsidesconfirmed' => '',
				'dapsidesnotconfirmed' => '',
			],
			'reject' => [
				'reject' => '',
				'bought-elsewhere' => '',
				'rejected' => '',
				'rejected-after-approve' => '',
				'not-interested' => '',
				'not-ready' => '',
				'travels' => '',
				'will-call-back' => '',
				'rejected-in-call-center' => '',
				'another-city' => '',
			
			],
			'expect' => [
				'new' => '',
				'test' => '',
				'nedozvon' => '',
			],
			'trash' => [
				'wrongdata' => '',
				'trash' => '',
			],
		],
		
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://lp1.top-pik.best/order/new',
//		'statusEnabled'		=> false,
	],
];