<?php

/*
 * https://dev.1c-bitrix.ru/api_help/sale/classes/csaleorder/csaleorder__getbyid.5cbe0078.php
 * Пріложtніє->вебхукі->создать_входящій
 * Права +CRM
 * із посилання беремо apiKey, subDomain та id
 *
 * https://b24-ulgrkf.bitrix24.ru/rest/1/p1l6nupzx8wgiild/methods			Доступні методи
 * https://b24-ulgrkf.bitrix24.ru/rest/1/p1l6nupzx8wgiild/crm.status.list	Перегляд статусів, нас цікавлять тільки "ENTITY_ID": "DEAL_STAGE", а в конфіг вносимо STATUS_ID
 *
 * Для зручного перегляду результатів використовуємо - https://jsonformatter.org/json-pretty-print
 *
 */

return [
	
	16520 => [
		'terraleadsApiKey'			=> 'f99df38a4e0cebdb95cb327c638b3811',
		'apiKey'					=> '6d9mza6k0q67f0tq',
		'id'						=> 1,
		'subDomain'					=> 'bioshop',
		
		'offers' => [
			2271 => [
				'offerName'		=> 'Deeper - AL',
//				'companyId'		=> 123,
//				'offerId'		=> 0000,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7736 => [
				'offerName'		=> 'Germivir - AL Albania - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7300 => [
				'offerName'		=> 'Germivir - AL Kosovo - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7193 => [
				'offerName'		=> 'SugaFix - AL (low price) Kosovo - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7192 => [
				'offerName'		=> 'Prosta Care - AL (low price) Kosovo - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7191 => [
				'offerName'		=> 'Libimax - AL (low price) Kosovo - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7190 => [
				'offerName'		=> 'SugaFix - AL (low price) Albania - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7189 => [
				'offerName'		=> 'Prosta Care - AL (low price) Albania - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7188 => [
				'offerName'		=> 'Libimax - AL (low price) Albania - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7095 => [
				'offerName'		=> 'Ostelife - AL Kosovo low price - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7094 => [
				'offerName'		=> 'Ostelife - AL Albania low price - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7093 => [
				'offerName'		=> 'Diabex - AL Kosovo low price - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7092 => [
				'offerName'		=> 'Diabex - AL Albania low price - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7099 => [
				'offerName'		=> 'Keto Probiotic - AL Kosovo - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7098 => [
				'offerName'		=> 'Keto Probiotic - AL Albania - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7097 => [
				'offerName'		=> 'Ostevit - AL Kosovo - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7096 => [
				'offerName'		=> 'Ostevit - AL Albania - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],		
			7047 => [
				'offerName'		=> 'Libimax - AL Kosovo - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7046 => [
				'offerName'		=> 'Libimax - AL Albania - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7038 => [
				'offerName'		=> 'sugaFix - AL Kosovo - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7037 => [
				'offerName'		=> 'sugaFix - AL Albania - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7036 => [
				'offerName'		=> 'Prosta Care - AL Kosovo - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7035 => [
				'offerName'		=> 'Prosta Care - AL Albania - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6804 => [
				'offerName'		=> 'Diabex - AL Kosovo - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6803 => [
				'offerName'		=> 'Diabex - AL Albania - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6704 => [
				'offerName'		=> 'Cardio Life - AL Albania - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6703 => [
				'offerName'		=> 'Cardio Life - AL Kosovo - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6706 => [
				'offerName'		=> 'Big Size - AL Albania - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6705 => [
				'offerName'		=> 'Big Size - AL Kosovo - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5661 => [
				'offerName'		=> 'Keto Black - AL Albania - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5662 => [
				'offerName'		=> 'Keto Black  - AL Kosovo - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6526 => [
				'offerName'		=> 'Beauty Slim - AL (low price) - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6649 => [
				'offerName'		=> 'Sugastab - AL	Kosovo - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6648 => [
				'offerName'		=> 'Sugastab - AL	Albania - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6525 => [
				'offerName'		=> 'OstyHealth - AL	Kosovo - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6524 => [
				'offerName'		=> 'OstyHealth - AL	Albania - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5666 => [
				'offerName'		=> 'Erectil - AL	Kosovo - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6454 => [
				'offerName'		=> 'Beauty Slim low price - XK - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6453 => [
				'offerName'		=> 'Beauty Slim low price - AL - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5665 => [
				'offerName'		=> 'Erectil - AL	Albania - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6231 => [
				'offerName'		=> 'Fungostop + - AL Kosovo - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6230 => [
				'offerName'		=> 'Fungostop + - AL - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6229 => [
				'offerName'		=> 'Variforce - AL	Kosovo - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6228 => [
				'offerName'		=> 'Variforce - AL - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2302 => [
				'offerName'		=> 'Diet Lite - AL - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2315 => [
				'offerName'		=> 'ReAction - AL - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2336 => [
				'offerName'		=> 'Fizzy Slim - AL - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2394 => [
				'offerName'		=> 'Bluronica - AL - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2459 => [
				'offerName'		=> 'Diet Lite - AL (Kosovo) - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2464 => [
				'offerName'		=> 'Varyforte - AL - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2503 => [
				'offerName'		=> 'Varyforte - AL (Kosovo) - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2506 => [
				'offerName'		=> 'Bluronica - AL (Kosovo) - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2502 => [
				'offerName'		=> 'Fizzy Slim - AL (Kosovo) - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2504 => [
				'offerName'		=> 'ReAction - AL (Kosovo) - TL',
								
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2505 => [
				'offerName'		=> 'Deeper - AL (Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2511 => [
				'offerName'		=> 'Ostelife - AL - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2512 => [
				'offerName'		=> 'Ostelife  - AL (Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2576 => [
				'offerName'		=> 'Choco Lite - AL (Albania) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2577 => [
				'offerName'		=> 'Choco Lite - AL (Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4018 => [
				'offerName'		=> 'Choco Lite - AL (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2659 => [
				'offerName'		=> 'Germitox - AL - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2660 => [
				'offerName'		=> 'Germitox - AL (Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2828 => [
				'offerName'		=> 'Novaskin - AL (Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2827 => [
				'offerName'		=> 'Novaskin - AL - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2875 => [
				'offerName'		=> 'Fungalor - AL(Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2876 => [
				'offerName'		=> 'Fungalor - AL - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2961 => [
				'offerName'		=> 'ImunProst - AL - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2858 => [
				'offerName'		=> 'RePeat - AL - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2859 => [
				'offerName'		=> 'RePeat - AL(Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2962 => [
				'offerName'		=> 'ImunProst - AL(Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3105 => [
				'offerName'		=> 'Bitter Melon Slim - AL - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3106 => [
				'offerName'		=> 'Bitter Melon Slim - AL(Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			3081 => [
				'offerName'		=> 'Cardio NRJ - AL - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3082 => [
				'offerName'		=> 'Cardio NRJ - AL(Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3059 => [
				'offerName'		=> 'Keto Light+ - AL - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3060  => [
				'offerName'		=> 'Keto Light+ - AL(Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3368  => [
				'offerName'		=> 'Prostaffect - AL - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3369  => [
				'offerName'		=> 'Prostaffect - AL(Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3395  => [
				'offerName'		=> 'Hialuron+ - AL - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3396  => [
				'offerName'		=> 'Hialuron+ - AL(Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3795  => [
				'offerName'		=> 'Hialuron+ - AL(Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3413  => [
				'offerName'		=> 'Cistat - AL - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3414  => [
				'offerName'		=> 'Cistat - AL(Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3544  => [
				'offerName'		=> 'BigLover - AL - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			3545  => [
				'offerName'		=> 'BigLover - AL(Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			3539  => [
				'offerName'		=> 'Myceril - AL - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			3540  => [
				'offerName'		=> 'Myceril - AL(Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3799  => [
				'offerName'		=> 'Menomin Forte - AL(Albania) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3800  => [
				'offerName'		=> 'Menomin Forte - AL(Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3801  => [
				'offerName'		=> 'Menomin Forte - AL(Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3772  => [
				'offerName'		=> 'Varydex - AL(Albania) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3773  => [
				'offerName'		=> 'Varydex - AL(Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3778  => [
				'offerName'		=> 'Varydex - AL(Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3784  => [
				'offerName'		=> 'Hondrowell - AL(Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3661  => [
				'offerName'		=> 'Hondrowell - AL(Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3660  => [
				'offerName'		=> 'Hondrowell - AL(Albania) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3793  => [
				'offerName'		=> 'Cardio NRJ - AL(Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3796  => [
				'offerName'		=> 'Prostaffect - AL (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3786  => [
				'offerName'		=> 'Germitox - AL (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3785  => [
				'offerName'		=> 'BigLover - AL (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3783  => [
				'offerName'		=> 'Diet Lite - AL (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3888  => [
				'offerName'		=> 'Quantum - AL (Albania) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3889  => [
				'offerName'		=> 'Quantum - AL (Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3890  => [
				'offerName'		=> 'Quantum - AL (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3789  => [
				'offerName'		=> 'ReAction - AL (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3863  => [
				'offerName'		=> 'EffectEro - AL (Albania) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],									
			3864  => [
				'offerName'		=> 'EffectEro - AL (Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],									
			3865  => [
				'offerName'		=> 'EffectEro - AL (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3794  => [
				'offerName'		=> 'Keto Light + - AL (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3974  => [
				'offerName'		=> 'EffectEro - MK (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3983  => [
				'offerName'		=> 'Germitox - MK (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4002  => [
				'offerName'		=> 'Varydex - MK (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4001  => [
				'offerName'		=> 'RePeat - MK (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4000  => [
				'offerName'		=> 'Keto Light + - MK (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4009  => [
				'offerName'		=> 'Quantum - MK (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4008  => [
				'offerName'		=> 'Bitter Melon Slim - MK (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4014  => [
				'offerName'		=> 'Cistat - MK (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4010  => [
				'offerName'		=> 'Novaskin - MK (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4013  => [
				'offerName'		=> 'Hondrowell - MK (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4015  => [
				'offerName'		=> 'Prostaffect - MK (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4021  => [
				'offerName'		=> 'Reaction - MK (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4022  => [
				'offerName'		=> 'Big Lover - MK (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4024  => [
				'offerName'		=> 'ProstaGenius - AL (Albania) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4025  => [
				'offerName'		=> 'ProstaGenius - AL (Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4026  => [
				'offerName'		=> 'ProstaGenius - AL (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			3790  => [
				'offerName'		=> 'RePeat - AL (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4040  => [
				'offerName'		=> 'Myceril - MK (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3798  => [
				'offerName'		=> 'Myceril - AL (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4041  => [
				'offerName'		=> 'Diet Lite - MK (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4044  => [
				'offerName'		=> 'Choco Lite - MK (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4035  => [
				'offerName'		=> 'diaFix - MK (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4034  => [
				'offerName'		=> 'diaFix - AL (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4033  => [
				'offerName'		=> 'diaFix - AL (Albania) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4032  => [
				'offerName'		=> 'diaFix - AL (Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3791  => [
				'offerName'		=> 'Varyforte - AL (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			3788  => [
				'offerName'		=> 'Novaskin - AL (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3797  => [
				'offerName'		=> 'Cistat - AL (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4148  => [
				'offerName'		=> 'ProstaGenius - MK(Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4259  => [
				'offerName'		=> 'Cardio NRJ - MK	(Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4522  => [
				'offerName'		=> 'Readymove - MK (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4521  => [
				'offerName'		=> 'Readymove - AL (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4520  => [
				'offerName'		=> 'Readymove - AL (Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4519  => [
				'offerName'		=> 'Readymove - AL (Albania) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			3792  => [
				'offerName'		=> 'Bitter Melon Slim - AL (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4966  => [
				'offerName'		=> 'Germixil - MK (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4965  => [
				'offerName'		=> 'Germixil - AL (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			4964  => [
				'offerName'		=> 'Germixil - AL (Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			4963  => [
				'offerName'		=> 'Germixil - AL (Albania) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5114  => [
				'offerName'		=> 'Psoridex - AL (Albania) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			5115  => [
				'offerName'		=> 'Psoridex - AL (Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5116  => [
				'offerName'		=> 'Psoridex - AL (Macedonia) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5855  => [
				'offerName'		=> 'Testogen - AL(Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			5854  => [
				'offerName'		=> 'Testogen - AL - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5853  => [
				'offerName'		=> 'Prostex - AL (Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5852  => [
				'offerName'		=> 'Prostex - AL - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],	
			5851  => [
				'offerName'		=> 'Beauty Slim - AL (Kosovo) - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5850  => [
				'offerName'		=> 'Beauty Slim - AL - TL',
			
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],																				
		],
		
		'configs' => [
			'statuses' => [
				'trash'	=> [
					'JUNK'						=> '',
				],
				'reject'	=> [
					'1'							=> 'reject',
				],
				'expect'	=> [
					'NEW'						=> '',
					'IN_PROCESS'				=> '',
					'PROCESSED'					=> '',
					'2'							=> 'per confimim',
					'3'							=> 'riskcall',
					'4'							=> 'interrupted',
				],
				'confirm'	=> [
					'CONVERTED'					=> '',
				],
			],
			'brakeLogFolder'	=> true,
		],
	],
    26891 => [
        'terraleadsApiKey'			=> '26404906f842f683d0b7966922c86dd4',
        'apiKey'					=> '2f8pusczri11u11h',
        'id'						=> 1,
        'subDomain'					=> 'b24-lrgc36',

        'offers' => [
            3607 => [
                'offerName'		=> 'HardEN - GR',
//				'companyId'		=> 123,
//				'offerId'		=> 0000,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
        ],

        'configs' => [
            'statuses' => [
                'trash'	=> [
                    'JUNK'						=> '',
                ],
                'reject'	=> [
                    '1'							=> 'reject',
                ],
                'expect'	=> [
                    'NEW'						=> '',
                    'IN_PROCESS'				=> '',
                    'PROCESSED'					=> '',
                    '2'							=> 'per confimim',
                    '3'							=> 'riskcall',
                    '4'							=> 'interrupted',
                ],
                'confirm'	=> [
                    'CONVERTED'					=> '',
                ],
            ],
            'brakeLogFolder'	=> true,
        ],
    ],
    40473 => [
        'terraleadsApiKey'			=> 'ff5054a3e4925a080a79743a8c1e6771',
        'apiKey'					=> '3l3d6oxcorrneja9',
        'id'						=> 1,
        'subDomain'					=> 'b24-tjgloa',

        'offers' => [
            5542 => [ 
                'offerName'		=> 'Sex Power - AR',
//				'companyId'		=> 123,
//				'offerId'		=> 0000,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
        ],

        'configs' => [
            'statuses' => [
                'trash'	=> [
                    'JUNK'						=> '',
                ],
                'reject'	=> [
                    '1'							=> 'reject',
                ],
                'expect'	=> [
                    'NEW'						=> '',
                    'IN_PROCESS'				=> '',
                    'PROCESSED'					=> '',
                    '2'							=> 'per confimim',
                    '3'							=> 'riskcall',
                    '4'							=> 'interrupted',
                ],
                'confirm'	=> [
                    'CONVERTED'					=> '',
                ],
            ],
            'brakeLogFolder'	=> true,
        ],
    ],
    31156 => [
        'terraleadsApiKey'			=> '7fe1df878513565d4045e924598a3d82',
        'apiKey'					=> 'm3ojkbl2klw7t717',
        'id'						=> 1,
        'subDomain'					=> 'b24-509jxx',

        'offers' => [
            4689 => [
                'offerName'		=> 'Psoricin - BA',
//				'companyId'		=> 123,
//				'offerId'		=> 0000,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4690 => [
                'offerName'		=> 'HermoroidStop - BA',
//				'companyId'		=> 123,
//				'offerId'		=> 0000,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4691 => [
                'offerName'		=> 'Bloolu Shampoo - BA',
//				'companyId'		=> 123,
//				'offerId'		=> 0000,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
        ],

        'configs' => [
            'statuses' => [
                'trash'	=> [
                    'JUNK'						=> '',
                ],
                'reject'	=> [
                    '1'							=> 'reject',
                ],
                'expect'	=> [
                    'NEW'						=> '',
                    'IN_PROCESS'				=> '',
                    'PROCESSED'					=> '',
                    '2'							=> 'per confimim',
                    '3'							=> 'riskcall',
                    '4'							=> 'interrupted',
                ],
                'confirm'	=> [
                    'CONVERTED'					=> '',
                ],
            ],
            'brakeLogFolder'	=> true,
        ],
    ],
    32093 => [
        'terraleadsApiKey'			=> '81ab8d3d7e58e2adc6fc2aeb0fa13adf',
        'apiKey'					=> 'ipgus9p6dmzsxi45',
        'id'						=> 7,
        'subDomain'					=> 'advertsbud',

        'offers' => [
            4603 => [
                'offerName'		=> 'Natura Active - BA',
//				'companyId'		=> 123,
//				'offerId'		=> 0000,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4940 => [
                'offerName'		=> 'Natura Active - RS',
//				'companyId'		=> 123,
//				'offerId'		=> 0000,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4800 => [
                'offerName'		=> 'Natura Active Cream - RS',
//				'companyId'		=> 123,
//				'offerId'		=> 0000,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4801 => [
                'offerName'		=> 'Prostate Pure - BA',
//				'companyId'		=> 123,
//				'offerId'		=> 0000,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4834 => [
                'offerName'		=> 'Slim Begin - BA',
//				'companyId'		=> 123,
//				'offerId'		=> 0000,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4948 => [
                'offerName'		=> 'Potent Max - BA',
//				'companyId'		=> 123,
//				'offerId'		=> 0000,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4995 => [
                'offerName'		=> 'Prostate Pure - RS',
//				'companyId'		=> 123,
//				'offerId'		=> 0000,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4994 => [
                'offerName'		=> 'Slim Begin - RS',
//				'companyId'		=> 123,
//				'offerId'		=> 0000,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4996 => [
                'offerName'		=> 'Potent Max - RS',
//				'companyId'		=> 123,
//				'offerId'		=> 0000,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            5810 => [
                'offerName'		=> 'Hyper Active - BA',
//				'companyId'		=> 123,
//				'offerId'		=> 0000,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            5811 => [
                'offerName'		=> 'Dia Active - BA',
//				'companyId'		=> 123,
//				'offerId'		=> 0000,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            5948 => [
                'offerName'		=> 'Hyper Active - RS',
//				'companyId'		=> 123,
//				'offerId'		=> 0000,

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
        ],

        'configs' => [
            'statuses' => [
                'trash'	=> [
                    'JUNK'						=> '',
                ],
                'reject'	=> [
                    '1'							=> 'reject',
                ],
                'expect'	=> [
                    'NEW'						=> '',
                    'IN_PROCESS'				=> '',
                    'PROCESSED'					=> '',
                    '2'							=> 'per confimim',
                    '3'							=> 'riskcall',
                    '4'							=> 'interrupted',
                ],
                'confirm'	=> [
                    'CONVERTED'					=> '',
                ],
            ],
            'brakeLogFolder'	=> true,
        ],
    ],
	'configs' => [

		'brakeLogFolder'			=> true,
//		'statusEnabled'				=> false,
	],
];
?>