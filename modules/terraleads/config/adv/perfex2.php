<?php

/*
	https://perfexcrm.themesic.com/apiguide/#api-Lead-GetLeadSearch
 */

return [
	10388 => [
		'terraleadsApiKey'			=> '5d888d3df541d5d64254a33623f86c97',
		'authtoken' 				=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiS29zdGEyIiwibmFtZSI6IlRlcnJhIiwicGFzc3dvcmQiOm51bGwsIkFQSV9USU1FIjoxNTkzMDcwODM4fQ.aaf-VHTQj0s_YRJeDMqBxjuNJF5MeqZ57DIMOVROPmI',
		
		'offers' => [
			3309 => [
				'source'        => '3',
				'assigned'		=> '1', 		//id админа, привязать к админу (необязательный)
				'status'		=> '10',
				
				'configs' => [
					'urlOrderAdd'		=> 'https://howmyhealth.in/crm2/api/leads',
					'urlOrderInfo'		=> 'https://howmyhealth.in/crm2/api/leads/search',
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'statuses' => [
				'trash'	=> [
					'cancel' 		=> '',
					'16' 		    => 'Short lead',
					'15' 		    => 'wrong number',
				],
				'reject'	=> [
					'cancel'        => '',
					'4' 		    => 'cancel \ not interested',
					'3' 		    => 'COD not available',
				],
				'expect'	=> [
					'new'   		=> '',
					'10' 		    => 'New',
					'5' 		    => 'not pickup',
					'9' 		    => 'late date order ',
					'18' 		    => 'interested customer',
					'2' 		    => 'Confirm Customer',
					'17' 		    => 'language issues',
					'14' 		    => 'repeat call-3',
					'13' 		    => 'repeat call-2',
					'12' 		    => 'repeat call-1',
					'6' 		    => 'call after some time',
				
				],
				'confirm'	=> [
					'customer'    	=> '',
					'1' 		    => 'Customer',
					'7' 		    => 'address issue',
					'11' 		    => 'Order Placed',
				],
			],
			
			'brakeLogFolder'	=> true,
//		'statusEnabled'		=> false,
		],
	],
];