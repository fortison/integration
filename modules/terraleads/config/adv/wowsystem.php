<?php

return [
	66274 => [
		'terraleadsApiKey'	=> '92efe04ae5250b240f00ca2215116cb7',
		'hash'				=> '7f274bfbbb97411daeff10df3be29ebf',
		
		'offers' => [
			9384 => [ // Toxinul - AZ
				'offer' => 'toxinul-39',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9199 => [ // Everest - AZ
				'offer' => 'everest-39',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9198 => [ // ShinVal - AZ
				'offer' => 'shinval-49',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9197 => [ // Redustar - AZ
				'offer' => 'redustar-39',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9196 => [ // Psoridin - AZ
				'offer' => 'psoridin-39',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9195 => [ // Oculus plus - AZ
				'offer' => 'oculusplus-39',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9194 => [ // Gemortin - AZ
				'offer' => 'gemortin-39',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9193 => [ // FungoZdrav - AZ
				'offer' => 'fungozdrav-39',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9192 => [ // Cleopatra - AZ
				'offer' => 'cleopatra-39',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9191 => [ // Aterostop - AZ
				'offer' => 'aterstop-39',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9190 => [ // Anti-nicotine - AZ
				'offer' => 'antinicotine-39',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9189 => [ // Anti-alco - AZ
				'offer' => 'antialco-39',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9188 => [ // Alfapro - AZ
				'offer' => 'alfapro-39',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			9187 => [ // HondoStart - AZ
				'offer' => 'hondrostart-39',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2323 => [ // Test
				'offer' => 'cleopatra-339',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'3'	=> 'Треш (Спам или дубль)',
			],
			'reject'	=> [
				'2'	=> 'Клиент отказался',
			],
			'expect'	=> [
				'0'	=> 'Обработка',
			],
			'confirm'	=> [
				'1'	=> 'Принят',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://wowsystem.pro/partner/new-order',
		'urlOrderInfo'		=> 'https://wowsystem.pro/partner/get-status',
	],
];

?>