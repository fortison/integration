<?php

/*
===API for creating leads===Should be sent via POST method to https://crm.phprocessing.info/modules/Webforms/capture.php urlencoded or multipart/form-data are acceptableBody fields: (** All fields must be URL encoded **)* __vtrftk=sid:f792580f877b050bd5cf42ac2ba63be3a20e5370 - hardcoded* publicid=25b0468e4bf86f9c9ce267898829bccb - hardcoded* urlencodeenable=1 - hardcoded* name=Nf - hardcoded* leadstatus=New - hardcoded* leadsource=Nf - hardcoded* firstname=Garry - First Name* lastname=Potter - Last Name* cf_852=French - Language, can be Spanish, German, Italian, French or English, other languages should be defined as English* email=qq1234@bb.com - E-Mail, we check unique of lead by this field* phone=+123456789 - Phone number in format E.164 e.g. +972541232323* country=France - Country* cf_904=http://your-domain.com/ipn-status.php?leadId=&signature= - notification URL,  will be replaced to lead ID, signature is sha1(LeadID + 'topScretPa$$wd')Full example using cUrl:curl -XPOST -d '__vtrftk=sid:f792580f877b050bd5cf42ac2ba63be3a20e5370,1504253022&publicid=25b0468e4bf86f9c9ce267898829bccb&urlencodeenable=1&name=Terra&firstname=qq&lastname=ww&leadstatus=New&cf_852=French&email=qq1234@bb.com&phone=+123456789&country=France&cf_902=1&leadsource=Terra&cf_904=http%3A%2F%2Fyour-domain.com%2Fipn-status.php%3FleadId%3D%3Cid%3E%26signature%3D%3Csignature%3E' https://crm.phprocessing.info/modules/Webforms/capture.phpВід:Arseniy CTO здесь ничего сложного нет, если есть какие-то дополнительные поля, например campaignId или наоборот, чего-то нет, скажите мне я могу адаптировать под васВід:Arseniy CTO касательно нотификаций по продажам, мы можем посылать вам сразу же по факту продажи по URL указанному в поле cf_904 или предоставить вам ссылку для поллинга
*/

return [
	
	12393 => [
		'terraleadsApiKey'			=> 'cf140edfb8ce4f53b3287bf92eb4cb55',
		'postbackApiKey'			=> '002e827687a975a3c325781980308eda',
		'__vtrftk' 					=> 'sid:1b17cd2ce423e2b1c35f5255211da7252d3e8f9d',
		'publicid' 					=> '946842c2a46b91630da4ddf3a14be04d',
		
		'offers' => [
			2000 => [
				'country' => 'Germany',
				'cf_852' => 'German',
				'offerName' => 'ReAction',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1529 => [
				'country' => 'Germany',
				'cf_852' => 'German',
				'offerName' => 'ChocoLite',
				
				'configs' => [
					'brakeLogFolder'	=> true, 
				],
			],
			1995 => [
				'country' => 'Austria',
				'cf_852' => 'Austria',
				'offerName' => 'Reaction - AT',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			1657 => [
				'country' => 'Austria',
				'cf_852' => 'Austria',
				'offerName' => 'Choco Lite - AT',

				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'		=> '',
			],
			'reject'	=> [
				'reject'	=> '',
			],
			'expect'	=> [
				'expect'	=> '',
			],
			'confirm'	=> [
				'confirm'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://crm.phprocessing.info/modules/Webforms/capture.php',
		'urlOrderInfo'		=> 'https://crm.phprocessing.info/modules/EmiMagna/LeadStatus.php',
	],
];

?>