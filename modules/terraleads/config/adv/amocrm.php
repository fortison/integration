<?php
/**
 * https://www.amocrm.ru/developers
 */

return [
	19096 => [
		# https://pastebin.com/2F7iMT7c
		'terraleadsApiKey'			=> '5eae7e00d867172eff3d458cbcfd5b7e',
		
		'userHash'             		=> '5254f74ff3d6f73c3735146e5498bacd574e3088',
		'userLogin'             	=> 'miraclebodycarefab@gmail.com',
		'subDomain'             	=> 'miraclebodycare',
		
		'fieldPhoneContact'			=> 1343515,
		'fieldFirstStatus'			=> 23143567,
		
		'offers' => [
			2480 => [
				'url' => 'http://gr.prostastop.beauty-shopping.net/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2600 => [
				'url' => 'http://gr.sevichbeard.beauty-shopping.net/',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
		
		'configs' => [
			'brakeLogFolder'	=> true,
			'statuses' => [
				'confirm'	=> [
					
					'142' 	   => 'success',
					
				],
				'reject' => [
					'30050299' => 'rejected',
					'143' 	   => 'reject and trash',
					
				],
				'expect' => [
					'23143327' => 'No answer',
					'23143330' => 'Call-back',
					'30036901' => 'confirmed',
				],
				'trash' => [
					'30050302' => 'trash',
				],
			],
		],
	],

	'configs' => [
//		'urlApileadOrderUpdate'		=> 'http://al-api.com/api/lead/update',
//		'bridgeEnabled'				=> 1,																				// Чи активна ця інтеграція
//		'statusEnabled'				=> null,																			// Чи опрацьовувати статуси цієї інтеграції
		'urlOrderAdd'				=> true,
		'urlOrderInfo'				=> true,
		'bridgeRequestTimeout'		=> 60,
//		'brakeLogFolder'			=> '',
//		'statusHandler' 			=> 'app\modules\apilead\handlers\adv\leadvertex\LeadvertexStatusHandler',
//		'bridgeHandler' 			=> 'app\modules\apilead\handlers\adv\leadvertex\LeadvertexBridgeHandler',
	]
];

?>