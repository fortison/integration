<?php

return [
	63102 => [
		'terraleadsApiKey' 		=> '19e4f4cbb1962db0328a1e8dc729c35c',
		'token' 				=> '776a30e5-236e-4d06-9db4-824d1bbdcd61',
		
		'offers' => [
			8965 => [//Money Amulet - RO (free price)
				'shop_id' 		=> 1,
				'project_id' 	=> 1,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8964 => [//Panax Ginseng - RO (free price)
				'shop_id' 		=> 1,
				'project_id' 	=> 22,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8963 => [//Maca Plus - RO (free price)
				'shop_id' 		=> 1,
				'project_id' 	=> 3,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6497 => [//Money Amulet - RO
				'shop_id' 		=> 1,
				'project_id' 	=> 6,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			3732 => [//Money Amulet - BG
				'shop_id' 		=> 10,
				'project_id' 	=> 25,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			6788 => [//Money Amulet - IT
				'shop_id' 		=> 2,
				'project_id' 	=> 14,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8671 => [//Triphala - RO
				'shop_id' 		=> 1,
				'project_id' 	=> 17,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8670 => [//Koenzym - RO
				'shop_id' 		=> 1,
				'project_id' 	=> 21,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8669 => [//Berberine - RO
				'shop_id' 		=> 1,
				'project_id' 	=> 19,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8668 => [//Eros - IT
				'shop_id' 		=> 2,
				'project_id' 	=> 7,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8667 => [//Maca Plus - RO
				'shop_id' 		=> 1,
				'project_id' 	=> 7,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			8666 => [//Panax Ginseng - RO
				'shop_id' 		=> 1,
				'project_id' 	=> 23,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
			2323 => [
				'shop_id' 		=> 1,
				'project_id' 	=> 1,
				
				'configs' => [
					'brakeLogFolder' => true,
				],
			],
		],
	],

	'configs' => [
		'statuses' => [
			'trash' => [
				'7' => '',
				'8' => '',
			],
			'reject' => [
				'4' => '',
			],
			'expect' => [
				'1' => '',
				'2' => '',
				'3' => '',
			],
			'confirm' => [
				'5' => '',
				'6' => '',
				'9' => '',
				'10' => '',
				'11' => '',
			],
		],
		
		'brakeLogFolder' 	=> true,
		'urlOrderAdd' 		=> 'https://premiumcall.voiptime.app/api/v2/order/create',
		'urlOrderInfo'		=> 'https://premiumcall.voiptime.app/api/v2/order/list',
	],
];