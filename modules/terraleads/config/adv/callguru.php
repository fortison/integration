<?php

return [
	
	49843 => [
		'terraleadsApiKey'			=> '6376ec2e38a2b4e6ca59794f95679818',
		'partner' 					=> 'n8cl4fzyu0yb9nim6grh',
		
		'offers' => [
			7861 => [ //Venaton - TN
				'offer' => '430893',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7356 => [ //Maxyboost - TN
				'offer' => '427931',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7045 => [ //Maxyboost - MA
				'offer' => '411414',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7011 => [ //Prostaton - TN
				'offer' => '427929',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			7010 => [ //Prostaton - MA
				'offer' => '411412',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6800 => [ //Green coffee - MA
				'offer' => '411410',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'Trash'		=> '',
			],
			'reject'	=> [
				'Cancel'		=> '',
			],
			'expect'	=> [
				'New Lead'		=> '',
				'Processing'	=> '',
			],
			'confirm'	=> [
				'Approve'		=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://partner.plumbill.com/api/leads',
		'urlOrderInfo'		=> 'https://partner.plumbill.com/api/leads',
	],
];

?>