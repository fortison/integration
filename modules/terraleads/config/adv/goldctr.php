<?php

/*
ID оффера: _v1xGR8
API Key: ca3b557af7c15def8547915e8050afaf
Ссылка на наш API: http://gold-ctr.com/order.php


Вы через post шлете нам данные:
Это пример

array (
	'post' => array (
		'key' => 'ca3b557af7c15def8547915e8050afaf',
		'code' => '_v1xGR8',
		'date' => '2018-09-05 15:31:26',
		'ip' => '93.77.101.27',
		'useragent' => 'Chrome/67.0.3396.99',
		'name' => 'Имя клиента',
		'phone' => '0923774836',
		'subid1' => '104598456',
	),
),

Обязательно:
Мой апи кей
Код - ID оффера
Дата заказа
IP заказа
useragent
Имя клиента
Телефон
В subid мы передаем id заказа.


Далее я при смене статуса у себя вам отдаю постбэк на ваш скрипт. например:


Ссылка пример постбэка:
https://terraleads.com/postback/revived?api_key=ca3b557af7c15def8547915e8050afaf&sub1={subid1}&status={status}&comments={comments}


В субайди я обратно отдаю номер заказа (id заказа), чтобы обозначить какому заказу статус меняем.


Сам статус, там будет вот так подставляться:

Аппрув - 2
Треш - 10
Отмена - 3
В обработке - 1
*/

return [
	
	11186 => [
		'terraleadsApiKey'			=> 'afa7e75f556bc188a567a444bdbfd978',
		'postbackApiKey'			=> '002e827687a975a3c325781980308eda',
		'key' 						=> 'ca3b557af7c15def8547915e8050afaf',
		
		'offers' => [
			2139 => [
				'code' => '_v1xGR8',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2162 => [
				'code' => 'wp3AGS4',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2164 => [
				'code' => 'zxXEGS6',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2165 => [
				'code' => 'uqWxGS7',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2059 => [
				'code' => 'sw3EGU8',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			2319 => [
				'code' => 'xt4EGUc',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				10	=> '',
			],
			'reject'	=> [
				3	=> '',
			],
			'expect'	=> [
				1	=> '',
			],
			'confirm'	=> [
				2	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'http://gold-ctr.com/order.php',
		'urlOrderInfo'		=> '',
	],
];

?>