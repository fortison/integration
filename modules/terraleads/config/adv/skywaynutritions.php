<?php

return [
	
	50240 => [
		'terraleadsApiKey'			=> '0303914046f0789342dcb39dd6f699c4',
		'authtoken'					=> 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoidGVycmFsZWFkcyIsIm5hbWUiOiJ0ZXJyYSIsIkFQSV9USU1FIjoxNjc4NDUxMzE3fQ.NHtcaETT4k8rUBRXYKqXnmlzGJFDL20xNNMt3Xkbx_8',
		'source'					=> 440280,
		'status'					=> 2,
		
		'offers' => [
			6835 => [
				'configs' => [
					'brakeLogFolder'	=> true,
				],
		],
			2323 => [
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'4'		=> '',
				'8'		=> '',
				'9'		=> '',
				'11'	=> '',
				'12'	=> '',
				'13'	=> '',
				'15'	=> '',
				'21'	=> '',
				'22'	=> '',
			],
			'reject'	=> [
				'16'	=> '',
				'17'	=> '',
				'19'	=> '',
			],
			'expect'	=> [
				'1'		=> '',
				'2'		=> '',
				'3'		=> '',
				'5'		=> '',
				'6'		=> '',
				'7'		=> '',
				'10'	=> '',
				'14'	=> '',
				'18'	=> '',
			],
			'confirm'	=> [
				'20'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://skywaynutritions.com/CRM/api/leads',
		'urlOrderInfo'		=> 'https://skywaynutritions.com/CRM/api/leads/search/',
	],
];

?>