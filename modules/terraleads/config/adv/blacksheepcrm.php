<?php

return [
	58839 => [
		'terraleadsApiKey'	=> '6a11835819230c8d114ef03b90978f6d',
		'bearer'			=> 'terra_HnT5gPhBpNgZge3WZvd7urRoByCzvcpmUa',
		
		'offers' => [
			8140 => [ //Diabetin - CO
				'product' => 'diabetin',
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash' => [
				'trash' => '',
			],
			'reject' => [
				'rejected' => '',
			],
			'expect' => [
				'lead' => '',
			],
			'confirm' => [
				'sale' => '',
			],
		],
		
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://blacksheepcrm.com/api/v1/leads/add',
		'urlOrderInfo'		=> 'https://blacksheepcrm.com/api/v1/leads/status',
	],
];