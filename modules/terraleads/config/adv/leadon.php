<?php

/*
 * http://connectcpa.net/terraleads/adv/adv-postback-status?postbackApiKey=b6f21d91136ed40d81c7c4a64dd12345&systemUserId=41613&systemOrderId={partner_order_id}&partnerStatus={status}
*/

return [
	41613 => [
		'terraleadsApiKey'			=> 'a0ed0eebf0065c77ff53bdfde015e313',
		'postbackApiKey'			=> 'b6f21d91136ed40d81c7c4a64dd12345',
		'partner_name' 				=> 'TerraLeads',
		'partner_api_key' 			=> 'WR3ZS5QWERT984W3',
		
		'offers' => [
			6226 => [ //PeniXL low price - PL
				'product_id' => 7,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			6225 => [ //PeniXL - PL
				'product_id' => 2,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5694 => [ //Hairoxil - PL
				'product_id' => 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5751 => [ //Hairoxil low price - PL
				'product_id' => 5,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5752 => [ //SlimIQ - PL
				'product_id' => 3,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5753 => [ //SlimIQ low price - PL
				'product_id' => 1,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5754 => [ //Cholexon - PL
				'product_id' => 4,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
			5755 => [ //Cholexon low price - PL
				'product_id' => 0,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'		=> '',
				'TRASH'		=> '',
			],
			'reject'	=> [
				'reject'	=> '',
				'CANCELED'	=> '',
			],
			'expect'	=> [
				'expect'	=> '',
				'NEW'	    => '',
				'PROCESS'	=> '',
			],
			'confirm'	=> [
				'confirm'	=> '',
				'APPROVED'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://leadon.pl/api/partners/{partner_name}:{partner_api_key}/addLead',
		'urlOrderInfo'		=> '',
	],
];

?>