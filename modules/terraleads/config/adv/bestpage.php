<?php

return [

    11389 => [
        'terraleadsApiKey'			=> '3d440ac40193d3d9429f1066bd189926',
        'api_key' 					=> '2019fd75aed2eb364ffa9c5383c9e18f',

        'offers' => [
            8309 => [ // RedJoint Pain Relief - QA
                'product' => 199857, //ID продукта
                'utm_term' => '87dmtp34', //поток КЦ
                'payout' => 28, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            8308 => [ // RedJoint Pain Relief - OM
                'product' => 201215, //ID продукта
                'utm_term' => '73fnsu04', //поток КЦ
                'payout' => 22, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            8306 => [ // RedJoint Pain Relief - KW
                'product' => 199854, //ID продукта
                'utm_term' => '25dnud85', //поток КЦ
                'payout' => 27, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            8305 => [ // RedJoint Pain Relief - AE
                'product' => 199855, //ID продукта
                'utm_term' => '92nsog63', //поток КЦ
                'payout' => 24, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4957 => [ // Anti-Wrinkle Moisturizing Serum - SA
                'product' => 196604, //ID продукта
                'utm_term' => '59mrbz84', //поток КЦ
                'payout' => 25, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5465 => [ // DeepHair - SA
                'product' => 196603, //ID продукта
                'utm_term' => '93barl43', //поток КЦ
                'payout' => 22, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4956 => [ // RED Pain Relief Gel - SA1
                'product' => 196602, //ID продукта
                'utm_term' => '72nzxr75', //поток КЦ
                'payout' => 27, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6612 => [ // Maca Root (prostatitis) - SA
                'product' => 196600, //ID продукта
                'utm_term' => '94nskt33', //поток КЦ
                'payout' => 22, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6537 => [ // Maca Root - SA
                'product' => 196601, //ID продукта
                'utm_term' => '52hsov95', //поток КЦ
                'payout' => 22, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            7281 => [ // Prostaffect (potency) - AE
                'product' => 196262, //ID продукта
                'utm_term' => '24kdjg92', //поток КЦ
                'payout' => 21, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            7282 => [ // Prostaffect (potency) - KW
                'product' => 196261, //ID продукта
                'utm_term' => '94entu04', //поток КЦ
                'payout' => 23, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6552 => [ // Prostaffect - KW
                'product' => 186346, //ID продукта
                'utm_term' => 'wrj357ju', //поток КЦ
                'payout' => 26, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5987 => [ // Prostaffect - AE
                'product' => 184103, //ID продукта
                'utm_term' => 'hp98h1x', //поток КЦ
                'payout' => 26, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6652 => [ // 24k Gold Primer Serum 1+1 - QA
                'product' => 184902, //ID продукта
                'utm_term' => 'a9ubk9', //поток КЦ
                'payout' => 22, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6625 => [ // Cardio NRJ - AE
                'product' => 190139, //ID продукта
                'utm_term' => 'jergkb5', //поток КЦ
                'payout' => 23, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            7350 => [ // Cardio NRJ - KW
                'product' => 193431, //ID продукта
                'utm_term' => 'vfqs7tvezq', //поток КЦ
                'payout' => 26, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            
            6551 => [ // EffectEro - KW
                'product' => 186345, //ID продукта
                'utm_term' => '7931qf', //поток КЦ
                'payout' => 21, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6099 => [ // EffectEro - AE
                'product' => 184429, //ID продукта
                'utm_term' => '79uv0zhy', //поток КЦ
                'payout' => 18, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            6051 => [ // Alfagen - SA
                'product' => 181904, //ID продукта
                'utm_term' => 'hknmkk3weq', //поток КЦ
                'payout' => 14, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            
            5863 => [ // Jaguar Men - AE
                'product' => 166751, //ID продукта
                'utm_term' => 'q4yuvp2mj8', //поток КЦ
                'payout' => 14.5, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5565 => [ // MaxMan Cream - KW_
                'product' => 185365, //ID продукта
                'utm_term' => 'eqqznrx', //поток КЦ
                'payout' => 18, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5589 => [ // Green Tea Mask - KW
                'product' => 178834, //ID продукта
                'utm_term' => '4vxak9d', //поток КЦ
                'payout' => 18, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5522 => [ // Romand Gold Luxury Watch - SA
                'product' => 166925, //ID продукта
                'utm_term' => 'drhkf5a0v', //поток КЦ
                'payout' => 12, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5523 => [ // Clay Mask Stick - KW
                'product' => 177602, //ID продукта
                'utm_term' => '6e2cr7', //поток КЦ
                'payout' => 16, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5014 => [ // Green Pain Relief Cream - KW
                'product' => 173015, //ID продукта
                'utm_term' => '5byumc3x40', //поток КЦ
                'payout' => 22, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4605 => [ // Anti-Wrinkle Moisturizing Serum - QA
                'product' => 166918, //ID продукта
                'utm_term' => 'g5fwb9d1', //поток КЦ
                'payout' => 15, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3802 => [ // 24k Gold Primer Serum - AE
                'product' => 166759, //ID продукта
                'utm_term' => 'q0s0kd', //поток КЦ
                'payout' => 17.5, //выплата веба

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            4202 => [ //24 Gold Primer Serum - QA
                'product' => 166921, //ID продукта
                'utm_term' => 'r5kam25j5', //поток КЦ
                'payout' => 17, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3811 => [ // RED Pain Relief Gel - AE
                'product' => 166755, //ID продукта
                'utm_term' => 'b1t8vcdb', //поток КЦ
                'payout' => 18.5, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3466 => [ // MaxMan Cream - AE1
                'product' => 166746, //ID продукта
                'utm_term' => 'jzdc88cv', //поток КЦ
                'payout' => 14.5, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            3467 => [ // Garcinia Extract - AE
                'product' => 166752, //ID продукта
                'utm_term' => 'mgw2515e', //поток КЦ
                'payout' => 14.5, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4205 => [ // Fade Spots Serum - QA
                'product' => 166922, //ID продукта
                'utm_term' => 'gutdyuk9n', //поток КЦ
                'payout' => 17, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4206 => [ // Men Beard Growth Oil - QA
                'product' => 166923, //ID продукта
                'utm_term' => 'u4ra436', //поток КЦ
                'payout' => 16, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4565 => [ // RED Pain Relief Gel - KW
                'product' => 166772, //ID продукта
                'utm_term' => '67fkyx0hp9', //поток КЦ
                'payout' => 16, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4566 => [ // Anti-Acne Whitening Serum - KW
                'product' => 166768, //ID продукта
                'utm_term' => 's01uw1r', //поток КЦ
                'payout' => 18, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4594 => [ // Anti-Acne Whitening Serum - QA
                'product' => 166917, //ID продукта
                'utm_term' => '5yszsh', //поток КЦ
                'payout' => 15, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4591 => [ // Anti-Acne Whitening Serum - AE
                'product' => 166760, //ID продукта
                'utm_term' => 'm1qj5b3411', //поток КЦ
                'payout' => 17, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4568 => [ // Anti-Wrinkle Moisturizing Serum - KW
                'product' => 166769, //ID продукта
                'utm_term' => 'sp92hk66', //поток КЦ
                'payout' => 17, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4604 => [ // Anti-Wrinkle Moisturizing Serum - AE
                'product' => 166761, //ID продукта
                'utm_term' => '98y18epmep', //поток КЦ
                'payout' => 16, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4569 => [ // Beard Growth Oil - KW
                'product' => 166770, //ID продукта
                'utm_term' => 'fpvuyy2', //поток КЦ
                'payout' => 16, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4546 => [ // Anti-hair loss - KW
                'product' => 166771, //ID продукта
                'utm_term' => '1gpkwj', //поток КЦ
                'payout' => 16, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4547 => [ // Anti-hair loss - AE
                'product' => 166754, //ID продукта
                'utm_term' => 's51drdzw2', //поток КЦ
                'payout' => 17, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4748 => [ // Alfagen - AE
                'product' => 170253, //ID продукта
                'utm_term' => 'wq3epq', //поток КЦ
                'payout' => 14, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5425 => [ // Alfagen - KW
                'product' => 177913, //ID продукта
                'utm_term' => 'f15f8s7', //поток КЦ
                'payout' => 16, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4545 => [ // Anti-hair loss - QA
                'product' => 166920, //ID продукта
                'utm_term' => '6xe88fnev', //поток КЦ
                'payout' => 17, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4633 => [ // Beard Growth Oil - AE
                'product' => 166753, //ID продукта
                'utm_term' => 'fct0e14', //поток КЦ
                'payout' => 15, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4634 => [ // Beard Growth Oil - QA
                'product' => 166919, //ID продукта
                'utm_term' => 'bq09nh2d4', //поток КЦ
                'payout' => 16, //выплата веба

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
        ],
    ],

    'configs' => [
        'statuses' => [
            'trash'	=> [
                9		=> 'Trash / Duplicate order',
                13		=> 'Test',
            ],
            'reject'	=> [
                2	=> 'Cancelled',
            ],
            'expect'	=> [
                0	=> 'Обработка',
            ],
            'confirm'	=> [
                1	=> 'Confirmed',
            ],
        ],
        'brakeLogFolder'	=> true,
        'urlOrderAdd'		=> 'https://bestpage-sales.xyz/partners/leadvertex.php',
        'urlOrderInfo'		=> 'https://bestpage-sales.xyz/partners/status.php',
    ],
];

?>