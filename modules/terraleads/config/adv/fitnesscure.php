<?php


return [

    27506 => [
        'terraleadsApiKey'			=> '43d09aafb29d3b070c08f95da3521c91',
        'postbackApiKey'			=> '43d09aafb29d3b070c08f95da3521c92',
        'token' 					=> 'c5JLCiPmTxsx3y',

        'offers' => [
            3823 => [
                'source' => 'TL',
                'product_id' => 101,

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            4020 => [ //101
                'source' => 'TL',
                'product_id' => function ($model, $config) {
					if ($model->count == 1) {
						return 93;
					} elseif ($model->count == 2) {
						return 94;
					} elseif ($model->count == 4) {
						return 96;
					} else {
						return 93;
					}
				},

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
        ],
    ],

    'configs' => [
        'statuses' => [
            'trash'	=> [
                'trash'	=> '',
            ],
            'reject'	=> [
                'reject'	=> '',
            ],
            'expect'	=> [
                'expect'	=> '',
            ],
            'confirm'	=> [
                'confirm'	=> '',
            ],
        ],
        'brakeLogFolder'	=> true,
        'urlOrderAdd'		=> 'https://fitnesscure.in/api-lead/leads.php',
        'urlOrderInfo'		=> '',
    ],
];

?>