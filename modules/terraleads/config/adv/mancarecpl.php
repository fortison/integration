<?php

/*
API Url:    https://mancare.in/teraleads-cpl/lead/add
METHOD: POST
Content-type: application/json
API KEY:  b9f2d8d12958f475e9657dfebf6b06fc
Note:  1 -  affiliate_id must be 16.
Request Format
{
"api_key":"b9f2d8d12958f475e9657dfebf6b06fc",
"first_name": "customer name",
"phone": "7840003121",
"email_id": "email id",
"affiliate_id": "16",
"sub1": "transaction id",
"sub2": "publisher id",
"sub3": "sub3 string"
}

Response Format
{
  "status": 200,
  "data": {
    "id": order id,
    "status": 1,
    "affiliate_id": 16
  },
  "status_message": "Successfully Lead generated"
}
 */

return [
	26970 => [
		'terraleadsApiKey'			=> '65706adc97b3034ad05301dfa5a1ad29',
		'postbackApiKey'			=> '36006adc97b3034ad05301dfa5a1ad28',
		'api_token' 				=> 'b9f2d8d12958f475e9657dfebf6b06fc',
		
		'offers' => [
			6351 => [ // ManCare CPL - IN
				'affiliate_id'	=> 16,
				
				'configs' => [
					'brakeLogFolder'	=> true,
				],
			],
		],
	],
	
	'configs' => [
		'statuses' => [
			'trash'	=> [
				'trash'		=> '',
			],
			'reject'	=> [
				'reject'	=> '',
			],
			'expect'	=> [
				'expect'	=> '',
			],
			'confirm'	=> [
				'confirm'	=> '',
			],
		],
		'brakeLogFolder'	=> true,
		'urlOrderAdd'		=> 'https://mancare.in/teraleads-cpl/lead/add',
		'urlOrderInfo'		=> '',
	],
];

?>