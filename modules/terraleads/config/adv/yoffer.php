<?php

return [

    40438  => [
        'terraleadsApiKey'				=> '166690fb1de522bde00e9a6f40e85dae',
//		'postbackApiKey' 			=> '1e5e51b5c6dff5ff6dc0aa1188f0ef5y',
        'api_key'					=> 'd740587bce3b9319f77da1d71c26f3b1',
        'api_id' 					=> '43',

        'offers' => [
            5535 => [ //Squid Game Mask - LT
                'offer' 		=> '55',
                'offer_name' 	=> 'Squid Game Mask - LT',

                'configs' => [
                    'brakeLogFolder'	=> true,
                ],
            ],
            5534 => [//Squid Game Mask - LV
                'offer'     	=> '55',
                'offer_name'    => 'Squid Game Mask - LV',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5533 => [ //Squid Game Mask - EE
                'offer'     	=> '55',
				'offer_name'    => 'Squid Game Mask - EE',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5532 => [ //Keto Star - LT
                'offer'     	=> '52',
				'offer_name'    => 'Keto Star - LT',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5531 => [ //Keto Star - LV
                'offer'     	=> '52',
				'offer_name'    => 'Keto Star - LV',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5530 => [ //Keto Star - EE
                'offer'     	=> '52',
				'offer_name'    => 'Keto Star - EE',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5541 => [ //Provimax - LT
                'offer'     	=> '53',
				'offer_name'    => 'Provimax - LT',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5540 => [ //Provimax - LV
                'offer'     	=> '53',
				'offer_name'    => 'Provimax - LV',
	
				'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5539 => [ //Provimax - EE
                'offer'     	=> '53',
				'offer_name'    => 'Provimax - EE',
	
				'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5538 => [ //Dzi Bead - LT
                'offer'     	=> '51',
				'offer_name'    => 'Dzi Bead - LT',
	
				'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5537 => [ //Dzi Bead - LV
                'offer'     	=> '51',
				'offer_name'    => 'Dzi Bead - LV',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5536 => [ //Dzi Bead - EE
                'offer'     	=> '51',
				'offer_name'    => 'Dzi Bead - EE',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5529 => [ //Money Amulet - LT
                'offer'     => '48',
				'offer_name'    => 'Money Amulet - LT',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5528 => [ //Money Amulet - LV
                'offer'     	=> '48',
				'offer_name'    => 'Money Amulet - LV',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
            5527 => [ //Money Amulet - EE
                'offer'     	=> '48',
				'offer_name'    => 'Money Amulet - EE',

                'configs' => [
                    'brakeLogFolder'    => true,
                ],
            ],
        ],
    ],

    'configs' => [
        'statuses' => [
            'trash'	=> [
                'trash'		=> '',
            ],
            'reject'	=> [
                'cancel'	=> '',
            ],
            'expect'	=> [
                'wait'		=> '',
                'hold'		=> '',
            ],
            'confirm'	=> [
                'approve'	=> '',
            ],
        ],
        'brakeLogFolder'	=> true,
        'urlOrderAdd'		=> 'https://yoffer.me/api/ext/add.json',
        'urlOrderInfo'		=> 'https://yoffer.me/api/ext/list.json',
    ],
];

?>