<?php
return [
	2906 => [
		'terraleadsApiKey'		=> '6ef72f7dcc55b51fa34883a15527088e',
		
		'postbackUrl' => [
			'glize' => 'https://callback.glize.com/terraleads',
			'brokerbabe' => 'https://callback.brokerbabe.com/terraleads',
		],
		
		'configs' => [
//			'postbackUrl'		=> '',
			'brakeLogFolder'	=> true,
		],
	],
];