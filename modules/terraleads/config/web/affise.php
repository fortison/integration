<?php

return [
	16502 => [
		'terraleadsApiKey'		=> 'cc0f7e8ffcb1375af5c93157b8a353f1',
		'API-KEY' 				=> '792521b99f334879e85c10726054a14d',
		'pid' 					=> 3,
		'acion_id' 				=> '212121212',
		'offer' 				=> 197,
		
		'configs' => [
			'postbackUrl'		=> 'https://api-nokfer.affise.com/3.0/admin/conversion/import',
			'brakeLogFolder'	=> true,
		],
	],
	
	'configs' => [
		'statuses' => [
			'expect'	=> '2',
//			'expect'	=> 'HOLD',
			'confirm'	=> '1',
			'reject'	=> '3',
			'fail'		=> '3',
			'trash'		=> '3',
		],
	],
];
