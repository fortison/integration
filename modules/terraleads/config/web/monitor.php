<?php
return [
	21344 => [
		'terraleadsApiKey'		=> '9d0b2b1d4808f1c81b1afa58ea9b0d83',
		
		'configs' => [
			'postbackUrl'		=> 'https://monitor.lm-department.com/postback',
			'brakeLogFolder'	=> true,
		],
	],
	
	'configs' => [
		'statuses' => [
			'accept'	=> 'hold',
//			'expect'	=> 'HOLD',
			'confirm'	=> 'sale',
			'reject'	=> 'canceled',
			'fail'		=> 'canceled',
			'trash'		=> 'canceled',
		],
	],
];