<?php

namespace app\modules\terraleads\controllers\adv;

use app\modules\terraleads\common\exeptions\NoLogException;
use app\modules\terraleads\common\adv\models\forms\TerraleadsAdvModel;
use app\modules\terraleads\common\adv\objects\Config;
use app\modules\terraleads\common\traits\ErrorTrait;
use app\modules\terraleads\common\traits\JsonTrait;
use app\modules\terraleads\common\traits\LogTrait;
use nahard\log\helpers\Log;
use Exception;
use Yii;

class BridgeController extends \yii\web\Controller
{
	use JsonTrait;
	use ErrorTrait;
	use LogTrait;
	
	public $incomingData;																								// Вхідні дані запиту від terraleads
	
	public function actionIndex($integrationName=null)
	{
		if ($this->module->disabled)																					// Якщо модуть відлючено
			return $this->renderJsonError("Module disabled!", 405);
		
		if (!Yii::$app->request->isPost)																				// Якщо це post запит від terraleads
			return $this->renderJsonError("Method not allowed!", 405);
		
		if (!$integrationName)
			return $this->renderJsonError("Integration must be set!", 412);
		
		try{
			$incomingData		= Yii::$app->request->rawBody;															// Перетворюємо тіло запиту в об'єкт
			$this->incomingData	= $this->decode($incomingData, true);
		} catch (Exception $e) {
			Log::error($e, null, __METHOD__);
			return $this->renderJsonError("Incorrect JSON: {$e->getMessage()}", 412);
		}
		
		$incomingModel = new TerraleadsAdvModel();																			// Блок каша-цикл. Завантаження канфіга залежить від вхідних даних, В майбутньому треба поправити і викинути один блок з середини іншого
		$incomingModel->load($this->incomingData, '');
		
		if ($incomingModel->isTest())
			return 'ok';
		
		if (!$incomingModel->validate())
			return $this->renderJsonError($incomingModel->errors, 412);
		
		try {
			$config = new Config([
				'integrationName' 	=> $integrationName,
				'moduleName' 		=> $this->module->id,
				'userId' 			=> $incomingModel->user_id,
				'offerId' 			=> $incomingModel->offer_id,
			]);
		} catch (Exception $e) {
			Log::error($e, $integrationName, __METHOD__);
			return $this->renderJsonError("Config error! " . $e->getMessage(), 412);
		}
		
		if (!YII_DEBUG && !$incomingModel->isCorrectCheckSum($config->user->getApiKey()))								// При розробці не потрібно перевіряти чексуму даних
			return $this->renderJsonError('incorrect check_sum!',412);

		if (!$config->isBridgeEnabled())
			return $this->renderJsonError("Integration bridge disabled!", 412);
		
		try {
			$handler = Yii::createObject([
				'class' 		=> $config->getBridgeHandler(),
				'config'  		=> $config,
				'incomingModel'	=> $incomingModel,
			]);
		} catch (Exception $e) {
			Log::error($e, null, __METHOD__);
			return $this->renderJsonError("Handler error!", 460);
		}
		
		try {
			$terraleadsModel = $handler->createOrder();
			return $this->renderJsonOk($terraleadsModel->partner_order_id ?? null);
		} catch (NoLogException $e) {
			return $this->renderJsonError("Bridge error! " . $e->getMessage(), 461);
		} catch (Exception $e) {
			Log::error($e, null, __METHOD__);
			return $this->renderJsonError("Bridge error! " . $e->getMessage(), 461);
		}
	}
	
	public function actionError()
	{
		$exception 	= Yii::$app->errorHandler->exception;
		$message 	= $exception->getMessage();
		$code 		= $exception->statusCode;
		$type 		= $exception->getCode();
		$file 		= $exception->getFile();
		$line 		= $exception->getLine();
		$trace 		= $exception->getTraceAsString();
		
//		$this->getBehavior('log')->writeError("\n[CODE: \t\t{$code}]\n[TYPE: \t\t{$type}]\n[MESSAGE: \t{$message}]\n[FILE: \t\t{$file}]\n[LINE: \t\t{$line}]\n[TRACE_BEGIN]\n{$trace}\n[TRACE_END]\n");
		
		return $this->renderJsonError($message, $code);
	}
	
	public function beforeAction($action)
	{
		if ($action->id === 'index')
			$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}
}
