<?php

namespace app\modules\terraleads\controllers\adv;

use app\common\models\Error;
use app\common\models\OrderApilead;
use app\modules\terraleads\common\adv\objects\Config;
use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\traits\ErrorTrait;
use nahard\log\helpers\Log;
use Exception;
use Yii;
use yii\db\Transaction;
use yii\web\HttpException;

class TestController extends \yii\web\Controller
{
	public $moduleName = 'terraleads';
	
	public function actionStatus($id)
	{
		$query = Yii::$app->db->createCommand('SELECT id FROM `order` WHERE system_order_id=:system_order_id')
			->bindValue(':system_order_id', $id)
			->queryOne();
		
		$orderModel = OrderApilead::findOne($query['id']);
		
		echo '<p>' . $orderModel->config->user->apiKey . '</p><hr>';
		
		var_dump($orderModel->attributes);
	}
	
	public function actionIndex()
	{
		foreach (OrderApilead::findExpectedOrdersBatch($this->moduleName) as $batchModels)
		{
			foreach ($batchModels as $orderModel)
			{
				$this->one($orderModel->id);
			}
		}
	}
	
	public function actionArr()
	{
		foreach ($this->arr() as $id)
		{
			echo $id . '<br>';
		}
	}
	
	public function actionOne($id)
	{
		$arr = $this->arr();
		
		if (!in_array($id, $arr))
		{
			echo '<h3><a href="/control/order/view?id=' . $id . '">' . $id . '</a></h3>';
			echo '<p style="color: red">Нету такого заказа</p>';
			
			return false;
		}
		
		$this->one($id);
		
		return true;
	}
	
	public function arr()
	{
		return OrderApilead::find()->select(['id'])->fresh()->relevant()->processStop()->andWhere(['integration_module'=>$this->moduleName])->orderBy(['id' => SORT_DESC])->asArray()->column();
	}
	
	public function one($id)
	{
		$orderModel = OrderApilead::findOne($id);
		
		echo '<h3><a href="/control/order/view?id=' . $orderModel->id . '">' . $orderModel->id . '</a></h3>';
		
		if (!$orderModel) {                                                                                    // Якщо цей запис виявився в обробці, продовжуємо роботу. Це необхідно якщо планувальники почнуть пересікатися.
			echo '<p style="color: red">Нету такого заказа</p>';
			return;
		}
		
		if ($orderModel->getOrderErrorsCount() >= 2) {
			echo '<p style="color: red">Ошибок по заказу больше 2</p>';
		}
		
		try {
			$config = new Config([
				'integrationName' => $orderModel->integration_name,
				'moduleName' => $this->moduleName,
				'userId' => $orderModel->system_user_id,
				'offerId' => $orderModel->offer_id,
			]);
			
			if (!$config->isStatusEnabled()) {                                                                    // Якщо статуси вимкнені
				echo '<p style="color: red">Статусы отключены</p>';
				return;
			}
			
			if ($config->user->getPostbackApiKey()) {                                                            // Якщо статуси йдуть по постбеку
				echo '<p style="color: red">Статусы работают через постбек</p>';
				return;
			}
			
			if (!$config->isTimeToStatusRequest($orderModel->created_at, $orderModel->request_at)) {            // Якщо настав час для оновлення статусу
				echo '<p style="color: red">Не время обновлять заказ</p>';
				return;
			}
			
			/**@var $handler \app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler */
			$handler = Yii::createObject([
				'class' => $config->getStatusHandler(),
				'config' => $config,
				'orderModel' => $orderModel,
			]);
			
			echo '<p style="color: green">СТАТУС БУДЕТ ОТПРАВЛЕН</p>';
		} catch (Exception $e) {
			echo '<p>Ошибка обаботки</p>';
			return;
		} finally {
			echo '<hr>';
		}
	}
}
