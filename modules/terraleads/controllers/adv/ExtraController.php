<?php

namespace app\modules\terraleads\controllers\adv;

use app\modules\terraleads\common\traits\ErrorTrait;
use nahard\log\helpers\Log;
use Exception;
use Yii;

class ExtraController extends \yii\web\Controller
{
	use ErrorTrait;
	
	public function actionIndex($integrationName=null)
	{
		if ($this->module->disabled)																					// Якщо модуть відлючено
			return $this->renderJsonError("Module disabled!", 405);
		
		if (!$integrationName)
			return $this->renderJsonError("Integration must be set!", 412);
		
		try {
			$integrationHandlerName = ucfirst($integrationName);
			$class= "app\\modules\\terraleads\\handlers\adv\\{$integrationName}\\{$integrationHandlerName}ExtraHandler";

			Yii::createObject(['class' => $class])->extra();
		} catch (Exception $e) {
			Log::error($e, null, __METHOD__);
			return $this->renderJsonError("Error extra handler!", 460);
		}
	}
	
	public function actionError()
	{
		$exception 	= Yii::$app->errorHandler->exception;
		$message 	= $exception->getMessage();
		$code 		= $exception->statusCode;
		$type 		= $exception->getCode();
		$file 		= $exception->getFile();
		$line 		= $exception->getLine();
		$trace 		= $exception->getTraceAsString();
		
		return $this->renderJsonError($message, $code);
	}
	
	public function beforeAction($action)
	{
		if ($action->id === 'index')
			$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}
}
