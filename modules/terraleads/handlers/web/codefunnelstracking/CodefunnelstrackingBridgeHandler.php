<?php
namespace app\modules\terraleads\handlers\web\codefunnelstracking;

use app\common\models\OrderApilead;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use app\modules\terraleads\common\web\handlers\base\BaseBridgeHandler;
use yii\web\HttpException;

class CodefunnelstrackingBridgeHandler extends BaseBridgeHandler
{
	public function send()
	{
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$incomingModel	= $this->incomingModel;
		
		if (!in_array($incomingModel->status, [OrderApilead::STATUS_CONFIRM, OrderApilead::STATUS_ACCEPT])) {
			return;
		}

		if ($incomingModel->status == OrderApilead::STATUS_CONFIRM) {

			foreach ($config->getPostbackUrl() as $requestUrl) {
				$requestUrl = str_replace('{payment}', $incomingModel->cost, $requestUrl);
				$requestUrl = str_replace('{sub_id_4}', $incomingModel->sub_id_4, $requestUrl);

				$prepareRequest = new PrepareGetRequest();
				$prepareRequest->setRequestUrl($requestUrl);

				$response	= $this->bridgeRequest($prepareRequest);
//
//				if ($response->error)
//					throw new HttpException('400', "{$response->error} [USER:{$incomingModel->user_id}] [LEAD:{$incomingModel->id}]");
//
//				$responseCode 		= $response->getCode();
//				$responseContent	= $response->getContent();
//
//				if ($responseCode !== '200')
//					throw new HttpException('400', "Partner error 1! Response code: {$responseCode}. Response: {$responseContent}");
			}
		}
	}
}