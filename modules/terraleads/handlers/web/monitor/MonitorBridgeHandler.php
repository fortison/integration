<?php
namespace app\modules\terraleads\handlers\web\monitor;

use app\common\models\OrderApilead;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use app\modules\terraleads\common\web\handlers\base\BaseBridgeHandler;
use yii\web\HttpException;
use Yii;

class MonitorBridgeHandler extends BaseBridgeHandler
{
	public function send()
	{
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$incomingModel	= $this->incomingModel;
		
		if (!in_array($incomingModel->status,
			[
				OrderApilead::STATUS_CONFIRM,
				OrderApilead::STATUS_ACCEPT,
				OrderApilead::STATUS_REJECT,
				OrderApilead::STATUS_TRASH,
				OrderApilead::STATUS_UNKNOWN,
			])) {
			return;
		}
		
		$requestUrl	= $config->getPostbackUrl() . '?' . http_build_query([
				'cid' 	  => $incomingModel->sub_id_4,
				'payout'  => $incomingModel->cost,
			]);
		
		if ($incomingModel->status == OrderApilead::STATUS_CONFIRM) {
			$requestUrl = $requestUrl . '&' . http_build_query([
					'et' => 'confirm',
				]);
		} elseif ($incomingModel->status == OrderApilead::STATUS_ACCEPT) {
			$requestUrl = $requestUrl . '&' . http_build_query([
					'et' => 'hold',
					'txid' => $incomingModel->id,
				]);
		} elseif (in_array($incomingModel->status, [OrderApilead::STATUS_REJECT, OrderApilead::STATUS_UNKNOWN, OrderApilead::STATUS_TRASH])) {
			$requestUrl = $requestUrl . '&' . http_build_query([
					'et' => 'canceled',
				]);
		}
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		$response = $this->bridgeRequest($prepareRequest);
		
		if ($response->error)
			throw new HttpException('400', "{$response->error} [USER:{$incomingModel->user_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $response->getCode();
		$responseContent	= $response->getContent();
		
		if ($responseCode !== '200')
			throw new HttpException('400', "Partner error 1! Response code: {$responseCode}. Response: {$responseContent}");
	}
}