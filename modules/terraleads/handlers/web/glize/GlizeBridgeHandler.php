<?php
namespace app\modules\terraleads\handlers\web\glize;

use app\modules\terraleads\common\objects\PrepareGetRequest;
use app\modules\terraleads\common\web\handlers\base\BaseBridgeHandler;
use yii\web\HttpException;
use Yii;

class GlizeBridgeHandler extends BaseBridgeHandler
{
	public function send()
	{
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		
		$incomingModel	= $this->incomingModel;
		
		$domain = $config->user->array['postbackUrl'][Yii::$app->request->get('postback')] ?? null;
		
		$requestUrl	= $domain . '?' . http_build_query([
			'trackcode'			=> $incomingModel->sub_id,
			'action'			=> 'single',
			'transactionid'		=> Yii::$app->request->get('orderid'),
			'payout'			=> $incomingModel->cost,
			'currency'			=> 'USD',
		]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);

		$response	= $this->bridgeRequest($prepareRequest);
		
		if ($response->error)
			throw new HttpException('400', "{$response->error} [USER:{$incomingModel->user_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $response->getCode();
		$responseContent	= $response->getContent();
		
		if ($responseCode !== '200')
			throw new HttpException('400', "Partner error 1! Response code: {$responseCode}. Response: {$responseContent}");
	}
}