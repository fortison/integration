<?php
namespace app\modules\terraleads\handlers\web\affise;

use app\common\models\OrderApilead;
//use app\modules\terraleads\common\adv\objects\Offer;
//use app\modules\terraleads\common\objects\PrepareGetRequest;
use app\modules\terraleads\common\objects\PreparePostRequest;
use app\modules\terraleads\common\web\handlers\base\BaseBridgeHandler;
use yii\web\HttpException;

class AffiseBridgeHandler extends BaseBridgeHandler
{
	
	public function send()
	{
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
//		print_r($config);die();
		
		$incomingModel	= $this->incomingModel;
//		$phone			= $incomingModel->;
//		$click_id		= $incomingModel->id;
		
		if (!in_array($incomingModel->status,
			[
				OrderApilead::STATUS_CONFIRM,
				OrderApilead::STATUS_ACCEPT,
				OrderApilead::STATUS_REJECT,
				OrderApilead::STATUS_TRASH,
				OrderApilead::STATUS_UNKNOWN,
			])) {
			return;
		}
		
		
		$requestUrl	= $config->getPostbackUrl();
//		print_r($requestUrl);die();
		$requestData = [
			"offer"			=> $config->user->array['offer'],
			"pid"			=> $config->user->array['pid'],
//			"status"		=> $incomingModel->status,
//			"action_id"			=> $config->user->array['action_id'],
//			"phone"				=> '$phone',
//			"click_id"			=> $click_id,
		];

//		print_r($config->user);die();
//		if ($incomingModel->status == OrderApilead::STATUS_CONFIRM) {
//			$requestUrl = $requestUrl . '&' . http_build_query([
//					'status' => 1,
//				]);
//		} elseif ($incomingModel->status == OrderApilead::STATUS_ACCEPT) {
//			$requestUrl = $requestUrl . '&' . http_build_query([
//					'status' => 2,
//				]);
//		} elseif (in_array($incomingModel->status, [OrderApilead::STATUS_REJECT, OrderApilead::STATUS_UNKNOWN, OrderApilead::STATUS_TRASH])) {
//			$requestUrl = $requestUrl . '&' . http_build_query([
//					'status' => 3,
//				]);
//		}
		
		
//		$requestData = [
//			"status"	 => $incomingModel->status,
//			"order"		 => $this->encode($order),
//		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestHeaders(['API-KEY:'.$config->user->array['API-KEY']]);
//		print_r($prepareRequest);die();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestData($requestData);
		
		$response = $this->bridgeRequest($prepareRequest);
		
		if ($response->error)
			throw new HttpException('400', "{$response->error} [USER:{$incomingModel->user_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $response->getCode();
		$responseContent	= $response->getContent();
		
		if ($responseCode !== '200')
			throw new HttpException('400', "Partner error 1! Response code: {$responseCode}. Response: {$responseContent}");
	}
}