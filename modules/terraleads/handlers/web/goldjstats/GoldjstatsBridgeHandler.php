<?php
namespace app\modules\terraleads\handlers\web\goldjstats;

use app\modules\terraleads\common\objects\PrepareGetRequest;
use app\modules\terraleads\common\web\handlers\base\BaseBridgeHandler;
use yii\web\HttpException;

class GoldjstatsBridgeHandler extends BaseBridgeHandler
{
	public function send()
	{
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		
		$incomingModel	= $this->incomingModel;
		
		$status			= $this->getRelatedStatus($incomingModel->status) ?? 'HOLD';
		$amount			= ($incomingModel->status === 'confirm') ? $incomingModel->cost : 0;
		$visitId		= $incomingModel->sub_id;

		$requestUrl	= $config->getPostbackUrl() . '?' . http_build_query([
			'visit_id'	=> $visitId,
			'amount'	=> $amount,
			'co_type'	=> $status,
		]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);

		$response	= $this->bridgeRequest($prepareRequest);
		
		if ($response->error)
			throw new HttpException('400', "{$response->error} [USER:{$incomingModel->user_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $response->getCode();
		$responseContent	= $response->getContent();
		
		if ($responseCode !== '200')
			throw new HttpException('400', "Partner error 1! Response code: {$responseCode}. Response: {$responseContent}");
		
		if (!preg_match('/"ok"/', $responseContent))
			throw new HttpException('400', "Partner error 2! Response code: {$responseCode}. Response: {$responseContent}");
	}
}