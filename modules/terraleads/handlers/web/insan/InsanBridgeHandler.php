<?php
namespace app\modules\terraleads\handlers\web\insan;

use app\modules\terraleads\common\objects\PrepareGetRequest;
use app\modules\terraleads\common\web\handlers\base\BaseBridgeHandler;
use yii\web\HttpException;
use Yii;

class InsanBridgeHandler extends BaseBridgeHandler
{
	public function send()
	{
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		
		$incomingModel	= $this->incomingModel;
		
		$a				= Yii::$app->request->get('a');
		$r				= $incomingModel->sub_id_2;

		$requestUrl	= $config->getPostbackUrl() . '?' . http_build_query([
			'a'			=> $a,
			'f'			=> 'pb',
			'r'			=> $r,
		]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);

		$response	= $this->bridgeRequest($prepareRequest);
		
		if ($response->error)
			throw new HttpException('400', "{$response->error} [USER:{$incomingModel->user_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $response->getCode();
		$responseContent	= $response->getContent();
		
		if ($responseCode !== '200')
			throw new HttpException('400', "Partner error 1! Response code: {$responseCode}. Response: {$responseContent}");
	}
}