<?php
namespace app\modules\terraleads\handlers\web\affsub2;

use app\common\models\OrderApilead;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use app\modules\terraleads\common\web\handlers\base\BaseBridgeHandler;
use yii\web\HttpException;
use Yii;

class Affsub2BridgeHandler extends BaseBridgeHandler
{
	public function send()
	{
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$incomingModel	= $this->incomingModel;
		$subId			= $incomingModel->sub_id;
		
		if (!in_array($incomingModel->status,
			[
				OrderApilead::STATUS_CONFIRM,
				OrderApilead::STATUS_ACCEPT,
				OrderApilead::STATUS_REJECT,
				OrderApilead::STATUS_TRASH,
				OrderApilead::STATUS_UNKNOWN,
			])) {
			return;
		}
		
		$requestUrl	= $config->getPostbackUrl() . '?' . http_build_query([
				'clickid' => $subId,
			]);
		
		if ($incomingModel->status == OrderApilead::STATUS_CONFIRM) {
			$requestUrl = $requestUrl . '&' . http_build_query([
					'status' => 1,
				]);
		} elseif ($incomingModel->status == OrderApilead::STATUS_ACCEPT) {
			$requestUrl = $requestUrl . '&' . http_build_query([
					'status' => 2,
				]);
		} elseif (in_array($incomingModel->status, [OrderApilead::STATUS_REJECT, OrderApilead::STATUS_UNKNOWN, OrderApilead::STATUS_TRASH])) {
			$requestUrl = $requestUrl . '&' . http_build_query([
					'status' => 3,
				]);
		}
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		
		$response = $this->bridgeRequest($prepareRequest);
		
		if ($response->error)
			throw new HttpException('400', "{$response->error} [USER:{$incomingModel->user_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $response->getCode();
		$responseContent	= $response->getContent();
		
		if ($responseCode !== '200')
			throw new HttpException('400', "Partner error 1! Response code: {$responseCode}. Response: {$responseContent}");
	}
}