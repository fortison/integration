<?php

namespace app\modules\terraleads\handlers\adv\synchuz;

use app\modules\terraleads\common\objects\PreparePostRequestJson;
use app\modules\terraleads\common\traits\{
	JsonTrait,
	LogTrait,
	RequestTrait
};
use Yii;
use yii\base\BaseObject;
use yii\web\HttpException;

/**
 * @property \app\modules\terraleads\common\adv\objects\Config $config Головна конфігурація поточного замовлення
 */
class Client extends BaseObject
{
	use RequestTrait;
	use LogTrait;
	use JsonTrait;
	
	public $config;	// головна конфігурація замовлення
	
	public function __construct(array $config)
	{
		parent::__construct($config);
	}
	
	public function getAccessToken()
	{
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$url            = $config->full->configs['urlOrderToken'];
		
		return $this->requestAccessToken(
			$url,
			$configUser->login,
			$configUser->pass
		);
	}
	
	private function requestAccessToken($url, $login, $pass)
	{
		$requestData = [
			"login" => $login,
			"pass"  => $pass,
		];
		
		$request = new PreparePostRequestJson();
		$request->setRequestUrl($url);
		$request->setRequestData($requestData);
		
		$response = $this->bridgeRequest($request);
		
		$responseCode	 = $response->getCode();
		$responseContent = $response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! RefreshToken error! Response code: {$responseCode} Response: {$responseContent}");
		
		return $responseContent;
	}
}