<?php
namespace app\modules\terraleads\handlers\adv\synchuz;

use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\db\Exception;

class SynchuzStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$terraleadStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($terraleadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToTerraleads($terraleadStatusDataModel, $needSendStatus);
		
		return $terraleadStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$orderModel     = $this->orderModel;
		
		$client	= new Client([
			'config' => $config,
		]);
		$access_token = $client->getAccessToken();
		
		$requestUrl = $config->getUrlOrderInfo();
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestHeaders([
			'GUID' => $orderModel->partner_order_id,
			'Content-Type' => 'application/json',
			'Authorization' => 'Bearer '.$access_token
		]);
		$prepareRequest->setRequestData(['GUID' => $orderModel->partner_order_id]);
		
		return $this->statusRequest($prepareRequest);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$partnerId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content = $responseModel->content;
		
		try {
			$contentObject	= $this->decode($content);
		} catch (Exception $e) {
			throw new OrderErrorException($orderId, "Partner error!. Json not valid in response");
		}
		
		$order = current($contentObject);
		
		if (!$order) {
			throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
		}
		
		if (!isset($order->statusid)) {
			throw new OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$apileadId}");
		}
		
		$partnerStatus 		= $order->statusid ?? null;
		$apileadStatus		= $this->getTerraleadsStatusName($partnerStatus);
		$comment			= $order->comment ?? '';
		
		$apileadStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setUserId($orderModel->system_user_id);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($partnerId);
		
		return $apileadStatusDataModel;
	}
}