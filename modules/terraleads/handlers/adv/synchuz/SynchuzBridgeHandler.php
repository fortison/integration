<?php

namespace app\modules\terraleads\handlers\adv\synchuz;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class SynchuzBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		$incomingModel	= $this->incomingModel;
		
		$client	= new Client([
			'config' => $config,
		]);
		$access_token = $client->getAccessToken();

		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'comodityid'    => $configOffer->comodityid,
			'customername'  => $incomingModel->name,
			'customerphone' => $incomingModel->phone,
			'webid'         => $incomingModel->web_id,
			'count'			=> $incomingModel->count,
			'region_id'     => 0,
			'district_id'   => 0,
			'street_id'     => 0,
			'address'       => '',
			'comment'       => '',
			'sts'			=> 0,
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestHeaders([
			'Content-Type' => 'application/json',
			'Authorization' => 'Bearer '.$access_token
		]);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId	= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		if (!empty($responseContent)) {
			return ['partner_order_id' => (string) $responseContent];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}