<?php
namespace app\modules\terraleads\handlers\adv\amocrm;

use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\models\forms\ResponseModel;

class AmocrmStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$terraleadsStatusDataModel = $this->getSystemStatusData($responseModel);

		$needSendStatus = $this->orderModel->updateOrder($terraleadsStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToTerraleads($terraleadsStatusDataModel, $needSendStatus);
		
		return $terraleadsStatusDataModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function makeStatusRequest()
	{
		$orderModel = $this->orderModel;
		
		$client = new Client([
			'config' => $this->config,
		]);
		
		return $client->leadInfo($orderModel->partner_order_id);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$terraleadsId	= $orderModel->system_order_id;
		$partnerId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$terraleadsId}");
		
		$contentObject	= $this->decode($responseModel->content);
		
		if (!isset($contentObject->response->leads[0]))
			throw new OrderErrorException($orderId, "Wrong request {$partnerId}:{$terraleadsId}");
		
		$order = $contentObject->response->leads[0];
		
		if (!isset($order->status_id))
			throw new OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$terraleadsId}");
		
		$comment		= '';
		$partnerStatus 	= $order->status_id;
		$terraleadsStatus	= $this->getTerraleadsStatusName($partnerStatus);
		
		$terraleadsStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$terraleadsStatusDataModel->setId($terraleadsId);
		$terraleadsStatusDataModel->setComment($comment);
		$terraleadsStatusDataModel->setStatus($terraleadsStatus);
		$terraleadsStatusDataModel->setPartnerStatus($partnerStatus);
		$terraleadsStatusDataModel->setPartnerId($partnerId);
		$terraleadsStatusDataModel->setUserId($orderModel->system_user_id);

		return $terraleadsStatusDataModel;
	}
}