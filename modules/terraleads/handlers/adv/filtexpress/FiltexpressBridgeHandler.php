<?php
namespace app\modules\terraleads\handlers\adv\filtexpress;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class FiltexpressBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'order_id' => $incomingModel->id,
			'phone' => $incomingModel->phone,
			'api_key' => $configUser->api_key,
			'name' => $incomingModel->name,
			'address' => $incomingModel->address,
			'goods_id' => $configOffer->goods_id,
			'country_id' => $configOffer->country_id,
			'price' => $incomingModel->landing_cost,
			'quantity' => $incomingModel->count,
			'customer_comment' => $incomingModel->user_comment,
			'referal' => $incomingModel->web_id,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status = $json->status ?? null;
		$orderId = $json->ext_id ?? null;
		
		if ($status === 'ok' && $orderId) {
			return [
				'partner_order_id' => (string) $orderId,
			];
		}
		
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}