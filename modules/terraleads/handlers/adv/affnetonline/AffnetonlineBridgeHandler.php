<?php
namespace app\modules\terraleads\handlers\adv\affnetonline;

use app\modules\terraleads\common\objects\PrepareGetRequest;
use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use yii\web\HttpException;
use app\modules\terraleads\common\traits\JsonTrait;

class AffnetonlineBridgeHandler extends BaseBridgeHandler
{
	use JsonTrait;
	
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestData = [
			'net'      		=> $configUser->net,
			'fid'         	=> $incomingModel->web_id,
			'clickid'       => $incomingModel->id,
			'offerid'       => $configOffer->offerid,
			'name'       	=> $incomingModel->name,
			'phone'        	=> $incomingModel->phone,
			'apikey'        => $configUser->apikey,
		];
		
		$this->requestUrl = $config->getUrlOrderAdd()  . '?' . http_build_query($this->requestData);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode < 200 || $responseCode > 210)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (isset($json->response) && $json->response == 200)
			return ['partner_order_id' => (string) $json->clickid];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}