<?php

namespace app\modules\terraleads\handlers\adv\limitless;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class LimitlessBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $incomingModel	= $this->incomingModel;

        $this->requestUrl	=  $config->getUrlOrderAdd();

        $this->requestData = [
            'API_KEY'            => $configUser->api_token,
            'Sub_id'             => $incomingModel->id,
            'Nome'               => $incomingModel->name,
            'Tel1'               => $incomingModel->phone,
            'Email'              => $incomingModel->email,
            'Indirizzo1'         => $incomingModel->address,
            'Citta'              => $incomingModel->city,
            'Affiliate_id'       => $configOffer->affiliate_id,
            'Offerta_id'         => $configOffer->offers_id,
            'Destinazione_id'    => $configOffer->destinazione_id,
        ];

        $prepareRequest = new PreparePostRequestJson();
        $prepareRequest->setRequestHeaders(['Content-Type' => 'application/json','Authorization' => $configUser->api_token]);
        $prepareRequest->setRequestUrl($this->requestUrl);
        $prepareRequest->setRequestData($this->requestData);

        $this->response	= $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();

        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }
    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($responseContent);

        $status = $json->Response;
        $leadid = $json->Lead_id ?? '';

        if ($status !== 'KO')
            return ['partner_order_id' => (string)$leadid];


        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}
