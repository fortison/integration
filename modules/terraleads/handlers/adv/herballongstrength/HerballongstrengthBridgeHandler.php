<?php

namespace app\modules\terraleads\handlers\adv\herballongstrength;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use yii\web\HttpException;

class HerballongstrengthBridgeHandler extends BaseBridgeHandler
{
	
	/**
	 * @inheritDoc
	 */
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel = $this->incomingModel;
		
		$this->logTerraleadsRequest($incomingModel->attributes);
		
		$config = $this->config;
		$configUser = (object)$config->user->array;
		$configOffer = (object)$config->offer->array;
		
		$this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
				"leadorder" => $incomingModel->id,
				"phonenumber" => $incomingModel->phone,
				"name" => $incomingModel->name,
				"zip" => $incomingModel->zip,
				"city" => $incomingModel->city,
				"address" => $incomingModel->address,
				"email" => $incomingModel->email,
				"apitoken" => $configUser->token,
			]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$this->response = $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode = $this->response->getCode();
		$responseContent = $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId = $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
//		print_r($json);die();
		$status = $json->status ?? null;
		
		if ($status == 'success')
			return [ 'partner_order_id' =>(string) $json->data->id ];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
	
	
}