<?php


namespace app\modules\terraleads\handlers\adv\oilcrm;


use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PrepareGetRequest;

class OilcrmStatusHandler extends BaseStatusHandler
{
    /**
     * @inheritdoc
     */
    public function updateStatusInSystem($force=false)
    {
        $responseModel = $this->makeStatusRequest();

        $terraleadsStatusDataModel = $this->getSystemStatusData($responseModel);

        $needSendStatus = $this->orderModel->updateOrder($terraleadsStatusDataModel);

        if ($force)
            $needSendStatus = true;

        $this->sendOrderStatusToTerraleads($terraleadsStatusDataModel, $needSendStatus);

        return $terraleadsStatusDataModel;
    }

    /**
     * @inheritdoc
     */
    public function makeStatusRequest()
    {
        $requestUrl = $this->config->getUrlOrderInfo() . '?' . http_build_query([
                'api_key'	=> $this->config->user->array['api_key'],
                'ids'	    => $this->orderModel->system_order_id,
            ]);
//        var_dump($requestUrl);die();

        $prepareRequest = new PrepareGetRequest();
        $prepareRequest->setRequestUrl($requestUrl);

        $data = $this->statusRequest($prepareRequest);

        return $data;
    }

    /**
     * @inheritdoc
     */
    public function getSystemStatusData(ResponseModel $responseModel)
    {
        $apiKey     = $this->config->user->getApiKey();
        $orderModel = $this->orderModel;
        $orderId	= $orderModel->id;
        $apileadId	= $orderModel->system_order_id;
        $partnerId	= $orderModel->partner_order_id;

        if (!empty($responseModel->error))
            throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");

        if ($responseModel->code != 200)
            throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");

        $content		= $responseModel->content;

        $contentObject	= $this->decode($content);
//        var_dump($contentObject->$apileadId->status);die();


        if (!$contentObject) {
            throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
        }

//        $order			= $contentObject[0];

        $partnerStatus 		= $contentObject->$apileadId->status ?? null;
        $apileadStatus		= $this->getTerraleadsStatusName($partnerStatus);
        $comment			= $contentObject->message ?? '';
//        var_dump($partnerStatus);die();
        $apileadStatusDataModel = new TerraleadsStatusDataModel($apiKey);
        $apileadStatusDataModel->setId($apileadId);
        $apileadStatusDataModel->setUserId($orderModel->system_user_id);
        $apileadStatusDataModel->setComment($comment);
        $apileadStatusDataModel->setStatus($apileadStatus);
        $apileadStatusDataModel->setPartnerStatus($partnerStatus);
        $apileadStatusDataModel->setPartnerId($partnerId);
//		var_dump($apileadStatusDataModel);die();
        return $apileadStatusDataModel;
    }
}
