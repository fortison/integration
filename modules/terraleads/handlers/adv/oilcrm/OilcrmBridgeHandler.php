<?php
namespace app\modules\terraleads\handlers\adv\oilcrm;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PrepareGetRequestJson;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class OilcrmBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $incomingModel	= $this->incomingModel;

        $this->requestUrl	= $config->getUrlOrderAdd();

        $this->requestData = [
            "name"			=> $incomingModel->name,
            "phone"			=> $incomingModel->phone ,
            "order_id"	    => (int)$incomingModel->id,
            "goods_id"	    => $configOffer->goods_id,
            "api_key"	    => $configUser->api_key,
            "address"	    => $incomingModel->address,

        ];
        $prepareRequest = new PreparePostRequest();
        $prepareRequest->setRequestUrl($this->requestUrl . '?' . http_build_query(['api_key' => $configUser->api_key]));;
        $prepareRequest->setRequestData($this->requestData);

        $this->response	= $this->bridgeRequest($prepareRequest);
//        var_dump($prepareRequest);die();
        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();

        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($responseContent);
//        print_r($json);die();
        $success = $json->status ?? null;
        $id		= $json->ext_id ?? null;

        if ($success === "ok")
            return ['partner_order_id' => (string) $id];

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}