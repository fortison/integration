<?php
namespace app\modules\terraleads\handlers\adv\ordercod;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class OrdercodBridgeHandler extends BaseBridgeHandler
{
    /**
     * @inheritdoc
     */
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $incomingModel	= $this->incomingModel;

        $this->requestUrl	= $config->getUrlOrderAdd();

        $this->requestData	= [
            'country_code'  => $configOffer->country_code,
            'product_code'  => $configOffer->product_code,
            'ids_partner' 	=> $configUser->ids_partner,
            'quantity' 	 	=> $configOffer->quantity ?? $incomingModel->count,
            'price' 	 	=> $incomingModel->landing_cost,
            'ipaddress'  	=> $incomingModel->ip,
            'customer' 		=> $incomingModel->name,
            'address' 		=> $incomingModel->address ?? '',
            'city' 			=> $incomingModel->city ?? '',
            'phone' 		=> $incomingModel->phone,
            'pub_id' 		=> $incomingModel->web_id ?? '',
//            'order_id' 		=> $incomingModel->id,
            'email'			=> $incomingModel->email ?? '',
            'province'		=> $incomingModel->region ?? '',
        ];

        $prepareRequest = new PreparePostRequestJson();
        $prepareRequest->setRequestUrl($this->requestUrl);
        $prepareRequest->setRequestData($this->requestData);

        $this->response	= $this->bridgeRequest($prepareRequest);

        if (!empty($this->response->error))
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode		= $this->response->getCode();
        $responseContent	= $this->response->getContent();

        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode} Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($responseContent);

        $status	= $json->id ?? 0;

        if ($status != 0) {
            return ['partner_order_id' => (string) $json->id];
        }

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}