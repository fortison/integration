<?php
namespace app\modules\terraleads\handlers\adv\ordercod;

use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use Yii;

class OrdercodStatusHandler extends BaseStatusHandler
{
	/**
	 * @inheritdoc
	 */
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$terraleadsStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($terraleadsStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToTerraleads($terraleadsStatusDataModel, $needSendStatus);
		
		return $terraleadsStatusDataModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function makeStatusRequest()
	{
		$requestUrl = $this->config->getUrlOrderInfo() . '?' . http_build_query([
				'partner_id'	=> $this->config->user->array['ids_partner'],
				'date_from'		=> '2015-01-01',
				'date_to'		=> date("Y.m.d"),
				'ids'			=> $this->orderModel->partner_order_id,
				]);
		
	
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		
		$data = $this->statusRequest($prepareRequest);
		
		return $data;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     	= $this->config->user->getApiKey();
		$orderModel 	= $this->orderModel;
		$orderId		= $orderModel->id;
		$terraleadsId	= $orderModel->system_order_id;
		$sourceId		= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$terraleadsId}");
		
		$content = $responseModel->content;
		
		try {
			$contentObject	= $this->decode($content);
		} catch (\Exception $e) {
			throw new OrderErrorException($orderId, "Decode json error: {$e->getMessage()}");
		}
		
		switch ($contentObject->error) {
			case 0:
				break;
			case -1:
				throw new OrderErrorException($orderId,
					"Unspecified error in ex is full stack of exception {$sourceId}:{$terraleadsId}");
			case -2:
				throw new OrderErrorException($orderId, "Invalid period {$sourceId}:{$terraleadsId}");
			case -3:
				throw new OrderErrorException($orderId, "Partner ID is not valid {$sourceId}:{$terraleadsId}");
			case -4:
				throw new OrderErrorException($orderId, "Wrong IP address {$sourceId}:{$terraleadsId}");
            default: throw new OrderErrorException($orderId, "Unknown error {$sourceId}:{$terraleadsId}");
		}

		$order = $contentObject->dataSource[0];

        if (!$order) {
            throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
        }

        $shipping_state = self::shippingState($order->shipping_state);
		$paid_state 	= self::paidState($order->paid_state);
		
		$comment			= "Shipping state: " . $shipping_state . ". Paid state: " . $paid_state;
		$partnerStatus 		= (string) $order->order_state;
		$terraleadsStatus	= $this->getTerraleadsStatusName($partnerStatus);
		
		$terraleadsStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$terraleadsStatusDataModel->setId($terraleadsId);
		$terraleadsStatusDataModel->setUserId($orderModel->system_user_id);
		$terraleadsStatusDataModel->setComment($comment);
		$terraleadsStatusDataModel->setStatus($terraleadsStatus);
		$terraleadsStatusDataModel->setPartnerStatus($partnerStatus);
		$terraleadsStatusDataModel->setPartnerId($sourceId);
		
		return $terraleadsStatusDataModel;
	}
	
	/**
	 * @param int $shipping_state
	 * @return string
	 */
	private static function shippingState($shipping_state)
	{
		switch($shipping_state) {
			case -1: return 'No permission to see this state';
			case 0: return 'In Delivery';
			case 1: return 'Shipped';
			case 2: return 'Rejected';
			default: return "No data";
		}
	}
	
	/**
	 * @param int $paid_state
	 * @return string
	 */
	private static function paidState($paid_state)
	{
		switch($paid_state) {
			case -1: return 'No permission to see this state';
			case 0: return 'Not Paid';
			case 1: return 'Paid';
			default: return "No data";
		}
	}
}