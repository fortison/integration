<?php
namespace app\modules\terraleads\handlers\adv\webtrade;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class WebtradeBridgeHandler extends BaseBridgeHandler
{
	/**
	 * @inheritdoc
	 */
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		
		$this->requestData	= [
			'name'           => $incomingModel->name,
			'phone'  		 => $incomingModel->phone,
			'partner' 		 => $configUser->partnerID,
			'product' 	     => $configOffer->product,
			'transaction_id' => $incomingModel->id,
			'check_sum'  	 => md5($configUser->partnerID.'_'.$configOffer->product.'_'.$incomingModel->phone),
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestHeaders(
			[
				'cache-control' => 'no-cache',
				'content-type' => 'multipart/form-data'
			]
		);
		
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if (!empty($this->response->error))
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode} Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (isset($json->purchase)) {
			if (isset($json->purchase->id)) {
				return ['partner_order_id' => (string) $json->purchase->id];
			}
		}

		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}