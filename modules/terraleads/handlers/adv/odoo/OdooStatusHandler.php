<?php
declare(strict_types=1);

namespace app\modules\terraleads\handlers\adv\odoo;

use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PreparePostRequest;
use app\modules\terraleads\common\objects\PreparePostRequestJson;

class OdooStatusHandler  extends BaseStatusHandler
{
    public function updateStatusInSystem($force = false)
    {

        $responseModel = $this->makeStatusRequest();

        $terraleadsStatusDataModel = $this->getSystemStatusData($responseModel);

        $needSendStatus = $this->orderModel->updateOrder($terraleadsStatusDataModel);

        if ($force)
            $needSendStatus = true;

        $this->sendOrderStatusToTerraleads($terraleadsStatusDataModel, $needSendStatus);

        return $terraleadsStatusDataModel;
    }

    public function makeStatusRequest()
    {
        $config = $this->config;
        $configUser = (object)$config->user->array;
        $requestUrl=$configUser->urlAuthenticate;
        $getsession_id = [
            "jsonrpc"=> "2.0",
            "params"=>[
                "db"=> $configUser->db,
                "login"=>$configUser->login,
                "password"=>$configUser->password,
            ],
        ];

        $requestUrltest = $config->getUrlOrderInfo();
        $requestData = [
            "jsonrpc"=> "2.0",
            "params"=>[
                [
                'lead_id' => (integer) $this->orderModel->partner_order_id,
                "phone"=>  $this->orderModel->incoming_field_phone,
                ],
            ],
        ];

        //getSession_id

        $prepareRequesttest = new PreparePostRequestJson();

        $prepareRequesttest->setRequestHeaders(['Content-Type' => 'application/json']);
        $prepareRequesttest->setRequestUrl($requestUrl);
        $prepareRequesttest->setRequestData($getsession_id);

        $responseModetestl = $this->statusRequest($prepareRequesttest);

        $getSessionIdStatus = $responseModetestl->cookie['session_id']->value;

        $prepareRequesttest->setRequestHeaders(['Content-Type' => 'application/json','X-Openerp-Session-Id' => $getSessionIdStatus]);

        $prepareRequesttest->setRequestUrl($requestUrltest);

        $prepareRequesttest->setRequestData($requestData);

        $responseModel = $this->statusRequest($prepareRequesttest);

        return $responseModel;
    }

    /**
     * @inheritdoc
     */
    public function getSystemStatusData(ResponseModel $responseModel)
    {
        $apiKey = $this->config->user->getApiKey();
        $orderModel = $this->orderModel;
        $orderId = $orderModel->id;
        $apileadId = $orderModel->system_order_id;
        $partnerId = $orderModel->partner_order_id;

        if (!empty($responseModel->error))
            throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");

        if ($responseModel->code != 200)
            throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");

        $content		= $responseModel->content;

        $contentObject = $this->decode($content);

        if (!$contentObject) {
            throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
        }

		$order = $contentObject->result->response[0]->status;

		if (!$order) {
			throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
		}

        $partnerStatus = $order ?? null;

        $apileadStatus = $this->getTerraleadsStatusName($partnerStatus);

        $apileadStatusDataModel = new TerraleadsStatusDataModel($apiKey);

        $apileadStatusDataModel->setId($apileadId);
        $apileadStatusDataModel->setUserId($orderModel->system_user_id);
//		$apileadStatusDataModel->setComment($comment);
        $apileadStatusDataModel->setStatus($apileadStatus);
        $apileadStatusDataModel->setPartnerStatus($partnerStatus);
        $apileadStatusDataModel->setPartnerId($partnerId);

        return $apileadStatusDataModel;
    }
}
