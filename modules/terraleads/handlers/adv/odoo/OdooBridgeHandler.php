<?php
declare(strict_types=1);

namespace app\modules\terraleads\handlers\adv\odoo;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;

use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class OdooBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $incomingModel	= $this->incomingModel;

        $this->requestUrl	= $config->getUrlOrderAdd();

        $getsession_id = [
            "jsonrpc"=> "2.0",
            "params"=>[
                "db"=> $configUser->db,
                "login"=>$configUser->login,
                "password"=>$configUser->password,
            ],
        ];

        $this->requestData = [
            "jsonrpc"=> "2.0",
            "params"=>[
                "order_id"	    => $incomingModel->id,
                "name"			=> $incomingModel->name,
                "phone"			=> $incomingModel->phone ,
                "goods_id"	    => $configOffer->goods_id,
                "publisherId" => $incomingModel->web_id,
                "affiliate"	    => "facebook",
            ],
        ];

        $prepareRequest = new PreparePostRequestJson();

        $prepareRequest->setRequestHeaders(['Content-Type' => 'application/json', ]);
        $prepareRequest->setRequestUrl($configUser->urlAuthenticate);
        $prepareRequest->setRequestData($getsession_id);
        $this->response	= $this->bridgeRequest($prepareRequest);

        $session_id = $this->response->cookie['session_id']->value;

        $prepareRequest->setRequestHeaders(['Content-Type' => 'application/json','X-Openerp-Session-Id'=> $session_id]);
        $prepareRequest->setRequestUrl($this->requestUrl);
        $prepareRequest->setRequestData($this->requestData);

        $prepareRequest->setRequestCookies($prepareRequest->requestCookies);

        $this->response = $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();

        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($responseContent);

        $success = $json->result->response->status ?? null;
        $id		= $json->result->response->lead_id ?? null;
//        print_r($json->result->response->status);die;
        if ($success === "ok")
            return ['partner_order_id' => (string) $id];

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}