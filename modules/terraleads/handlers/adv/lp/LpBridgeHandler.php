<?php
namespace app\modules\terraleads\handlers\adv\lp;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class LpBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$args = [$incomingModel, $config];
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$products = [
			[
				'product_id' => $configOffer->product_id,    //код товара (из каталога CRM)
				'price'      => $configOffer->product_price, //цена товара 1
				'count'      => '1'                      //количество товара 1
			]
		];
		$this->requestData	= [
			'key' => $configUser->key,
			'order_id' => $incomingModel->id,
			'country' => $incomingModel->country,
			'bayer_name' => $incomingModel->name,
			'phone' => $incomingModel->phone,
			'sender' => urlencode(serialize(['SERVER_NAME' => $configOffer->product_landing])),
			'products' => urlencode(serialize($products)),
		];
		
		if (!empty($configUser->office)) {
			$this->requestData['office'] = $configUser->office;
		}
		
		if (!empty($configOffer->additional_1)) {
			if (is_callable($configOffer->additional_1))
				$additional = call_user_func_array($configOffer->additional_1, $args);
			else
				$additional = $configOffer->additional_1;
			
			$this->requestData['additional_1'] = $additional;
		}
		
		if (!empty($configOffer->additional_2)) {
			if (is_callable($configOffer->additional_2))
				$additional = call_user_func_array($configOffer->additional_2, $args);
			else
				$additional = $configOffer->additional_2;
			
			$this->requestData['additional_2'] = $additional;
		}
		
		if (!empty($configOffer->additional_3)) {
			if (is_callable($configOffer->additional_3))
				$additional = call_user_func_array($configOffer->additional_3, $args);
			else
				$additional = $configOffer->additional_3;
			
			$this->requestData['additional_3'] = $additional;
		}
		
		if (!empty($configOffer->additional_4)) {
			if (is_callable($configOffer->additional_4))
				$additional = call_user_func_array($configOffer->additional_4, $args);
			else
				$additional = $configOffer->additional_4;
			
			$this->requestData['additional_4'] = $additional;
		}
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status	= $json->status ?? null;
		$id		= $json->data[0]->order_id ?? null;
		
		if ($status === 'ok' && $id)
			return ['partner_order_id' => (string) $json->data[0]->order_id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}