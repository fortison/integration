<?php
namespace app\modules\terraleads\handlers\adv\leadbit;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;
use Yii;

class LeadbitBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd() . $configUser->token;
		
		$this->requestData = [
			'token' 		=> $configUser->token,
			'flow_hash'		=> $configOffer->flow_hash,
			'landing' 	    => $configOffer->landing ?? '',
//			'lastname'		=> $incomingModel->name,
			'referrer' 	    => 'terraleads.com',
			'phone' 		=> $incomingModel->phone,
			'name' 		    => $incomingModel->name,
			'country' 		=> $incomingModel->country,
			'address' 		=> $incomingModel->address ?? '',
			'email' 		=> $incomingModel->email ?? '',
			'comment' 		=> $incomingModel->user_comment,
			'ip' 			=> $incomingModel->ip,
			'sub1' 			=> $incomingModel->id,
			'sub2' 			=> $incomingModel->getWHash(),
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status = $json->status ?? null;
		
		if ($status == 'success') {
			return [];
//			return ['partner_order_id' => (string) $json->orderId];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}