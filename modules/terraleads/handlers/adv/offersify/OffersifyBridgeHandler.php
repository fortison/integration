<?php

namespace app\modules\terraleads\handlers\adv\offersify;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class OffersifyBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'uid' 				=> $configUser->uid,
			'key' 				=> $configUser->key,
			'offer' 			=> $configOffer->offer,
			'lp' 				=> $configOffer->lp,
			'name' 				=> $incomingModel->name,
			'tel' 				=> $incomingModel->phone,
			'street-address' 	=> $incomingModel->address ?? 'empty',
			'postal-code' 		=> 1111,
			'ip' 				=> $incomingModel->ip,
			'ua'				=> $incomingModel->user_agent,
			'subid'				=> $incomingModel->id,
			'utm_source'		=> $incomingModel->web_id,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if ($json->message = 'OK' && isset($json->leadId)) {
			return [
				'partner_order_id' => (string) $json->leadId,
			];
		}
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}