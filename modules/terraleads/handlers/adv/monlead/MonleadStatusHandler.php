<?php
namespace app\modules\terraleads\handlers\adv\monlead;

use app\common\models\OrderApilead;

use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;

class MonleadStatusHandler extends BaseStatusHandler
{
    public function updateStatusInSystem($force=false)
    {
        $responseModel = $this->makeStatusRequest();

        $terraleadsStatusDataModel = $this->getSystemStatusData($responseModel);

        $needSendStatus = $this->orderModel->updateOrder($terraleadsStatusDataModel);

        if ($force)
            $needSendStatus = true;

        $this->sendOrderStatusToTerraleads($terraleadsStatusDataModel, $needSendStatus);

        return $terraleadsStatusDataModel;
    }

    public function makeStatusRequest()
    {
        $requestUrl = $this->config->getUrlOrderInfo() . '?' . http_build_query([
                'api_key'	=> $this->config->user->array['apiKey'],
                'lead_list' => $this->orderModel->partner_order_id,

                'search_by' => 'hash',
                'format'	=> 'json',
            ]);

        $prepareRequest = new PrepareGetRequest();
        $prepareRequest->setRequestUrl($requestUrl);

        $data = $this->statusRequest($prepareRequest);

        return $data;
    }

    /**
     * @inheritdoc
     */
    public function getSystemStatusData(ResponseModel $responseModel)
    {
        $apiKey     = $this->config->user->getApiKey();
        $orderModel = $this->orderModel;
        $orderId	= $orderModel->id;
        $apileadId	= $orderModel->system_order_id;
        $sourceId	= $orderModel->partner_order_id;

        if (!empty($responseModel->error))
            throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$apileadId}");

        if ($responseModel->code != 200)
            throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");

        $content		= $responseModel->content;

        $contentObject	= $this->decode($content);

        $order			= $contentObject[0] ?? null;

        if (!$order)
            throw new OrderErrorException($orderId, "Order not found in response {$sourceId}:{$apileadId}");

        if (!isset($order->status))
            throw new OrderErrorException($orderId, "Status not found for lead {$sourceId}:{$apileadId}");

        $partnerStatus 		= $order->status;
        $partnerSubStatus 	= $order->cancel_reason;
        $apileadStatus		= $this->getApileadStatus($partnerStatus, $partnerSubStatus);

        $comment = '';

        if ($apileadStatus == OrderApilead::STATUS_REJECT)
            $comment = $this->getTerraleadsStatusComment($partnerSubStatus);

        $comment			.= $order->data4 ?? '';

        $terraleadsStatusDataModel = new TerraleadsStatusDataModel($apiKey);

        $terraleadsStatusDataModel->setId($apileadId);
        $terraleadsStatusDataModel->setUserId($orderModel->system_user_id);
        $terraleadsStatusDataModel->setComment($comment);
        $terraleadsStatusDataModel->setStatus($apileadStatus);
        $terraleadsStatusDataModel->setPartnerStatus($partnerStatus);
        $terraleadsStatusDataModel->setPartnerId($sourceId);
//        var_dump($terraleadsStatusDataModel);die();
        return $terraleadsStatusDataModel;
    }

    public function getApileadStatus($partnerStatus, $partnerSubStatus)
    {
        $subStatuses	= $this->config->getSubStatuses();
        $trash			= $subStatuses[OrderApilead::STATUS_TRASH];

        if (array_key_exists($partnerSubStatus, $trash))																# спочатку перевірка на треш через підстатус
            return OrderApilead::STATUS_TRASH;

        return parent::getTerraleadsStatusName($partnerStatus);
    }

    public function getTerraleadsStatusComment($partnerSubStatus)
    {
        $subStatuses	= $this->config->getSubStatuses();
        $reject			= $subStatuses[OrderApilead::STATUS_REJECT];

        return $reject[$partnerSubStatus] ?? '';
    }
}