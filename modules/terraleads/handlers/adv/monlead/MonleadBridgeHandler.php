<?php
namespace app\modules\terraleads\handlers\adv\monlead;

use app\common\models\OrderApilead;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use yii\web\HttpException;
use yii\httpclient\Client;

class MonleadBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $incomingModel	= $this->incomingModel;

        $args = [$incomingModel, $config];

        $data = [
            'api_key' 			=> $configUser->apiKey,
            'tel'				=> $incomingModel->phone,
            'ip'				=> $incomingModel->ip,
            'code'				=> $configOffer->offerId,
            'traffic_type'		=> 0,
            'geo'				=> $incomingModel->country,
            'client'			=> $incomingModel->name,
            'adress'			=> $incomingModel->address ?? '',
            'foreign_value'		=> $incomingModel->id,
            'foreign_web_id'	=> $incomingModel->web_id,
            'format'			=> 'json',
        ];

        if (!empty($configOffer->comments)) {
            if (is_callable($configOffer->comments)) {
                $comments = call_user_func_array($configOffer->comments, $args);
            } else {
                $comments = $configOffer->comments;
            }

            $data['comments'] = $comments;
        }

        $this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query($data);

        $prepareRequest = new PrepareGetRequest();
        $prepareRequest->setRequestUrl($this->requestUrl);

        $this->response	= $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();

        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {

        $json = $this->decode($responseContent);

        $status		= $json->status ?? null;
        $errorCode	= $json->error_code ?? null;
//        print_r($json);die();
        if ($status === 'ok')
            return ['partner_order_id' => (string) $json->lead_hash];
        else if ($status === 'error' && $errorCode == 438) {

            $incomingModel	= $this->incomingModel;
            $apiKey     	= $this->config->user->getApiKey();
            $errorMessage	= $json->error_msg ?? '';

            $terraleadsStatusDataModel = new TerraleadsStatusDataModel($apiKey);
            $terraleadsStatusDataModel->setId($incomingModel->id);
            $terraleadsStatusDataModel->setComment($errorMessage);
            $terraleadsStatusDataModel->setStatus(OrderApilead::STATUS_TRASH);

            $requestUrl		= $this->config->getUrlTerraleadsOrderUpdate();
            $requestData    = $terraleadsStatusDataModel->getStatusData();

            $prepareRequest = new PreparePostRequestJson();
            $prepareRequest->setRequestUrl($requestUrl);
            $prepareRequest->setRequestData($requestData);
            $prepareRequest->setResponseFormat(Client::FORMAT_JSON);

            $responseModel = $this->statusRequest($prepareRequest);

            if (!$responseModel->error && $responseModel->code == 200)
                return ['system_status'=>OrderApilead::STATUS_TRASH];

            exit($responseModel->content);
        }

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}