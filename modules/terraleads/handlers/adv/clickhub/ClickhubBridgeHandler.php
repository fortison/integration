<?php
namespace app\modules\terraleads\handlers\adv\clickhub;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class ClickhubBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl = $config->getUrlOrderAdd();
		$this->requestData	= [
			'nick' 				=> $configUser->nick,
			'product_code' 		=> $configOffer->product_code,
			'price' 			=> $incomingModel->landing_cost,
			'surname'			=> $incomingModel->name,
			'name'				=> $incomingModel->name,
			'phone'				=> $incomingModel->phone,
			'country'			=> $incomingModel->country,
			'custom1'			=> $incomingModel->id,
			'custom2'			=> $incomingModel->web_id,
			'url'				=> 'https://www.domain.com/terra/lead/2/one',
			'email'				=> 'bd123@gmail.com',
			'currency'			=> 'bd',
			'locale'			=> 'bd',
			'city'				=> 'bd',
			'pcode'				=> 'bd',
			'address'			=> 'bd',
			'product_title'		=> 'bd',
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		$id = $json->id ?? null;
		
		if (!empty($id)) {
			return [
				'partner_order_id' => $id,
			];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}