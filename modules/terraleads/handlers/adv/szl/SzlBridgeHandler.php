<?php
namespace app\modules\terraleads\handlers\adv\szl;

use app\common\models\OrderApilead;
use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\httpclient\Client;
use yii\web\HttpException;

class SzlBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		
		$this->requestData = [
			'firstname' 			=> $incomingModel->name,
			'lastname' 				=> '',
			'email' 				=> $incomingModel->email ?? '',
			'phone' 				=> $incomingModel->phone,
			'street' 				=> $incomingModel->address ?? '',
			'postalCode' 			=> $incomingModel->zip ?? '',
			'city' 					=> $incomingModel->city ?? '',
			'source'				=> $incomingModel->address ?? '',
			'externalLeadId'    		=> $incomingModel->id, //your clickID,
			'externalPartnerId'    		=> $configUser->externalPartnerId,
			'externalPartnerSubId'		=> $incomingModel->getWHash(), // your sub ID
			'product' 				=> $configOffer->product,
			'price'					=> $incomingModel->landing_cost,
			'country'			    => $incomingModel->country,
			'ipAddress' 			=> $incomingModel->ip,
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestHeaders(["x-auth-token" => $configUser->api_key]);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if ($json->ok == true || $json->ok == 'true') {
			return ['partner_order_id' => (string) $json->ccResponse->id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}