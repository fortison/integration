<?php

namespace app\modules\terraleads\handlers\adv\livenaturecure;


use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use yii\web\HttpException;

class LivenaturecureBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $incomingModel	= $this->incomingModel;

        $this->logTerraleadsRequest($incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
                'FirstName' 	    	 => $incomingModel->name ?? '',
                'Phone' 	 		     => $incomingModel->phone ?? '',
                'Address' 	             => $incomingModel->address ?? '',
                'OrderUUID'              => $incomingModel->id ?? '',
                'OrderAffUserId'         => $incomingModel->web_id ?? '',
                'OrderVisitUUID'         =>  'terraleads_curemaxx',

            ]);

        $prepareRequest = new PrepareGetRequest();
        $prepareRequest->setRequestUrl($this->requestUrl);
        $this->response	= $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();
//        var_dump($responseCode);die();
        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($responseContent);

        if ($json == 'Record created successfully.')
            return ['partner_order_id' => ''];

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }

}