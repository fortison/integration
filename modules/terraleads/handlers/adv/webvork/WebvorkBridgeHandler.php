<?php

namespace app\modules\terraleads\handlers\adv\webvork;

use app\modules\terraleads\common\objects\PrepareGetRequest;
use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use yii\web\HttpException;

class WebvorkBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $incomingModel	= $this->incomingModel;

        $args = [$incomingModel, $config];

        $this->requestUrl	= $config->getUrlOrderAdd();
        $this->requestData	= [
            'token'         => $configUser->token,
            'name'  		=> $incomingModel->name,
            'phone' 	    => $incomingModel->phone,
            'country' 	    => $incomingModel->country,
            'ip' 	 	    => $incomingModel->ip,
            'offer_id' 		=> $configOffer->offer_id,
            'utm_content' 	=> $incomingModel->id,
            'utm_campaign' 	=> $incomingModel->getWHash(),
        ];

        $prepareRequest = new PrepareGetRequest();
        $prepareRequest->setRequestUrl($this->requestUrl);
        $prepareRequest->setRequestData($this->requestData);

        $this->response	= $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();

        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($responseContent);
//        print_r($json);
        $status	= $json->status ?? 0;

        if ($status == 'ok') {
            return ['partner_order_id' => (string) $json->guid];
        }

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}
