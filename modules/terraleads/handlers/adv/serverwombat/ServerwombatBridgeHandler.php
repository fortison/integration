<?php

namespace app\modules\terraleads\handlers\adv\serverwombat;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use app\modules\terraleads\common\objects\PrepareGetRequestJson;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class ServerwombatBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config = $this->config;
		$configUser = (object)$config->user->array;
		$configOffer = (object)$config->offer->array;
		
		$incomingModel = $this->incomingModel;
		
		$args = [$incomingModel, $config];
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$this->requestData = [
//			'token'         => $configUser->token,
			'name' => $incomingModel->name,
			'phone' => $incomingModel->phone,
			'country' => $incomingModel->country,
			'city' => $incomingModel->city,
			'count' => $incomingModel->count,
			'cost' => $incomingModel->cost,
			'product_id' => $configOffer->product_id,
//			'ip' 	 	    => $incomingModel->ip,
//			'offer_id' 		=> $configOffer->offer_id,
			'id' => $incomingModel->id,
			'user_id' => $incomingModel->web_id,
			'address' => $incomingModel->address,
			'zip' => $incomingModel->zip ?? null,
//			'utm_campaign' 	=> $incomingModel->getWHash(),
		];
		//Please try to run now
//		$this->requestData = json_encode($this->requestData);
		
		/* END TEST */
		$prepareRequest = new PrepareGetRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		$prepareRequest->setRequestHeaders([
//			'Content-Type' => 'application/json',
//			'X-FIT-TOKEN' => $configUser->api_key
		]);
		
		$this->response = $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode = $this->response->getCode();
		$responseContent = $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId = $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
//		var_dump($json);die();
		$id_order = (int)$json->success;
//		var_dump($json->success);die();
		if ($responseContent == true)
			return ['partner_order_id' => (string)$id_order];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
	
}