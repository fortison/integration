<?php

namespace app\modules\terraleads\handlers\adv\pboxsystem;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class PboxsystemBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config 		= $this->config;
		$configUser 	= (object)$config->user->array;
		$configOffer 	= (object)$config->offer->array;
		
		$incomingModel = $this->incomingModel;
		
		$this->requestUrl = sprintf($config->getUrlOrderAdd());
		$this->requestData = [
			'firstname' 			=> $incomingModel->name,
			'lastname' 				=> $incomingModel->name,
			'phone' 				=> $incomingModel->phone,
			'productName' 			=> $configOffer->productName,
			'price'					=> $incomingModel->landing_cost,
			'externaLeadId'			=> $incomingModel->id,
			'externalPartnerId'		=> $configUser->externalPartnerId,
			'externalPartnerSubId'	=> $incomingModel->web_id,
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		$prepareRequest->setRequestHeaders([
			'Content-Type' 	=> 'application/json',
			'x-api-key'		=> $configUser->apiKey
		]);
		
		$this->response = $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode = $this->response->getCode();
		$responseContent = $this->response->getContent();
		
		if ($responseCode < 200 && $responseCode > 210)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId = $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		
		if (!empty($json->status) && $json->status == 'Ok') {
			return [];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}