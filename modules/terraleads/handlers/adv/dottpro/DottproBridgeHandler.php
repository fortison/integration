<?php
namespace app\modules\terraleads\handlers\adv\dottpro;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\helpers\Json;
use yii\web\HttpException;
use Yii;

class DottproBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;

		$this->requestUrl	= $config->getUrlOrderAdd();
		
		$json = Json::encode([
			'action' => 'addOrder',
			'params' => [
				[
					'id' => $incomingModel->id,
					'fio' => $incomingModel->name,
					'phone' => $incomingModel->phone,
//					'address' => $incomingModel->address,
					'good_id' => $configOffer->goodId,
					'count' => 1,
					'price' => $incomingModel->landing_cost,
					'country_kod' => $incomingModel->country,
//					'timezone' => $incomingModel->tz,
//					'web_id' => $incomingModel->uHash ?: $incomingModel->wHash,
				]
			],
		]);
		
		$data = base64_encode($json);
		
		$sign = $sign = base64_encode(md5($configUser->key . $json . $configUser->key));
		
		$this->requestData	= [
			'data' => $data,
			'sign' => $sign,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl . '?' . http_build_query(['token' => $configUser->token]));
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$orderId = $json->data[0]->id ?? null;
		
		if ($orderId)
			return [
				'partner_order_id' => (string) $orderId,
			];
		
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}