<?php


namespace app\modules\terraleads\handlers\adv\fitnesscure;


use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PrepareGetRequestJson;
use yii\web\HttpException;

class FitnesscureBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $incomingModel	= $this->incomingModel;

        $this->logTerraleadsRequest($incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;
		$product_id		= 0;
	
		$args = [$incomingModel, $config];
		
		if (!empty($configOffer->product_id)) {
			if (is_callable($configOffer->product_id))
				$product_id = call_user_func_array($configOffer->product_id, $args);
			else
				$product_id = $configOffer->product_id;
		}
        
        $this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
                'token'             => $configUser->token,
                'source'            => $configOffer->source,
                'product_id'        => $product_id,
                'affiliate_id'      => $incomingModel->web_id,
                'clickid'           => $incomingModel->id,
                'name'              => $incomingModel->name,
                'number'            => $incomingModel->phone,
                'street_address'    => $incomingModel->address,

            ]);
	
		

        $prepareRequest = new PrepareGetRequestJson();
        $prepareRequest->setRequestUrl($this->requestUrl);
        $this->response	= $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();
//        print_r($responseCode);die();
        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);
//        print_r($responseContent);die();
        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($responseContent);
//         print_r($json);die();
        $status = $json->status;
        $order	= $json->id;
        if ($status =='success' ) {
            return ['partner_order_id' => $order];
        }

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}