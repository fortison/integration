<?php
namespace app\modules\terraleads\handlers\adv\traffi;

use app\modules\terraleads\common\objects\PrepareGetRequest;
use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PrepareGetRequestJson;
use yii\web\HttpException;

class TraffiBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $incomingModel	= $this->incomingModel;

        $this->logTerraleadsRequest($incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
                'apikey'        => $configUser->apikey,
                'transactionid' => $configOffer->transactionid,
                'affid'         => $configOffer->affid,
                'clickid'       => $incomingModel->id,
                'fullname'      => $incomingModel->name,
                'mobile'        => $incomingModel->phone,
                'address'       => $incomingModel->address
            ]);

        $prepareRequest = new PrepareGetRequestJson();
        $prepareRequest->setRequestUrl($this->requestUrl);
        $this->response	= $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();
//        print_r($responseCode);die();
        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);
//        print_r($responseContent);die();
        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($responseContent);
//         print_r($json);die();
        $id	= $json->insert_id;
        if ($responseContent !=='Ok' ) {
            return ['partner_order_id' => (string)$id];
        }

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}
