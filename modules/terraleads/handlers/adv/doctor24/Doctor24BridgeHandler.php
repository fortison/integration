<?php

namespace app\modules\terraleads\handlers\adv\doctor24;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class Doctor24BridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $incomingModel = $this->incomingModel;

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $config = $this->config;
        $configUser = (object)$config->user->array;
        $configOffer = (object)$config->offer->array;

        $this->requestUrl = $config->getUrlOrderAdd();
        $this->requestData = [

            "click_id" 	=> $incomingModel->id,
            "name" 		=> $incomingModel->name,
            "phone_no" 	=> $incomingModel->phone,
            "email" 	=> $incomingModel->email,
            "token" 	=> $configUser->key,
            "address" 	=> $incomingModel->address,
            "city" 		=> $incomingModel->city,
            "state" 	=> $incomingModel->city,
            "pincode" 	=> $incomingModel->zip,
            "offer_id" 	=> $configOffer->offer,
            "pub_id" 	=> $incomingModel->web_id,

        ];

        $prepareRequest = new PreparePostRequest();
        $prepareRequest->setRequestUrl($this->requestUrl);
//        $prepareRequest->setRequestHeaders(['Content-Type' => 'application/json']);
        $prepareRequest->setRequestData($this->requestData);

        $this->response = $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode = $this->response->getCode();
        $responseContent = $this->response->getContent();
//        print_r($responseCode);die();
        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId = $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($responseContent);

        $code 	= $json->code ?? null;
        $id 	= $json->rmk_id;
        if ($code === "200")
            return ['partner_order_id' => (string)$id];

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}
