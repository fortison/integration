<?php
namespace app\modules\terraleads\handlers\adv\weird;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class WeirdBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData = [
			"external"		=> $incomingModel->id,
			"username"	    => $incomingModel->name,
			"region"	    => $incomingModel->region,
			"phones"		=> [$incomingModel->getClearPhone()],
			"products"		=> [
				[
					'code' 		=> $configOffer->product,
					'quantity'  =>  (int)$incomingModel->count,
                    'price'     =>  (int)$incomingModel->landing_cost,
				]
			],
			"source"		=> 'terraleads',
			"campaign"		=> $incomingModel->web_id,
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestHeaders(['Api-Key' => $configUser->api_key,'Content-Type' => 'application/json']);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();

		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$this->checkResponseInvalidPhone($responseContent, 'Valid phones not provided');
		
		$json = $this->decode($responseContent);

		$success = $json->ok ?? null;
		$order_id = $json->result->order_id ?? null;

		if ($success == 'true')
			return ['partner_order_id' => (string)$order_id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}
