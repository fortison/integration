<?php
namespace app\modules\terraleads\handlers\adv\leadon;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class LeadonBridgeHandler extends BaseBridgeHandler
{
	/**
	 * @inheritdoc
	 *
	 */
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl = preg_replace(
			['/{partner_name}/', '/{partner_api_key}/'],
			[$configUser->partner_name, $configUser->partner_api_key],
			$config->getUrlOrderAdd()
		);
		
		$this->requestData	= [
			'firstname'     => $incomingModel->name,
			'lastname'      => $incomingModel->name,
			'phone'  		=> $incomingModel->phone,
			'country' 		=> $incomingModel->country,
			'ip' 	     	=> $incomingModel->ip,
			'webmaster_id' 	=> $incomingModel->getWHash(),
			'partner_order_id' 	=> $incomingModel->id,
			'product_id' 	=> $configOffer->product_id,
			'quantity' 		=> $incomingModel->count ?? 1,
			'currency' 		=> $incomingModel->landing_currency,
			'price' 		=> $incomingModel->landing_cost,
		];
		
		$prepareRequest = new PreparePostRequest();
		
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		$prepareRequest->setRequestHeaders(
			[
				'content-type' => 'x-www-form-urlencoded'
			]
		);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if (!empty($this->response->error))
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode} Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (isset($json->status) && $json->status == 'ok') {
			if (isset($json->id)) {
				return ['partner_order_id' => (string) $json->id];
			}
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}