<?php

namespace app\modules\terraleads\handlers\adv\cpad;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class CpadBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $api_domain = 'https://tl-api.com';

        $user_id = '25837';
        $api_key = '3d79e28dfb755e599b90316bd8f64ebd';
        $model = 'offer';
        $method = 'list';

//        $config			= $this->config;
//        $configUser		= (object) $config->user->array;
//        $configOffer	= (object) $config->offer->array;

//        $incomingModel	= $this->incomingModel;

        $data = [
            'user_id' => $user_id,
            'data' => [
            ]
        ];

        $json_data = json_encode($data);

//        $this->requestData	= [
//            'id'      		=> $incomingModel->id,
//            'wm' 			=> $incomingModel->getWHash(),
//            'offer'			=> $configOffer->offer,
//            'ip'			=> $incomingModel->ip,
//            'name'			=> $incomingModel->name,
//            'phone'			=> $incomingModel->phone,
//            'country'		=> $incomingModel->country,
//        ];



        $api_url = $api_domain.'/api/'.$model.'/'.$method.'?'.http_build_query(array(
                'check_sum' => sha1($json_data.$api_key)
            ));


        $prepareRequest = new PreparePostRequestJson();
        $prepareRequest->setRequestUrl($api_url);
        $prepareRequest->setRequestData($data);

        print_r($this->bridgeRequest($prepareRequest));die();
        $this->response	= $this->bridgeRequest($prepareRequest);


        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();

        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($responseContent);

        $status = $json->status ?? '';

        if ($status == 'ok')
            return ['partner_order_id' => (string) $json->id];

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}