<?php
namespace app\modules\terraleads\handlers\adv\ddcoffers;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use yii\web\HttpException;

class DdcoffersBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;

		$this->requestUrl	= $config->getUrlOrderAdd() . '?' . http_build_query([
			'net' => 1900,
			'fid' => $incomingModel->uHash ?: $incomingModel->wHash,
			'cid' => $incomingModel->id,
			'offerid' => $configOffer->offer_id,
			'name' => $incomingModel->name,
			'phone' => $incomingModel->phone,
			'api_key' => $configUser->api_key,
		]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseCode)
	{
//		$test = $this->decode($responseCode);
//		print_r($responseCode); die();
		if ($responseCode == true) {
			return [];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseCode}}");
	}
}