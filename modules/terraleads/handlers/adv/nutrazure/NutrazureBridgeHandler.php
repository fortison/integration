<?php

namespace app\modules\terraleads\handlers\adv\nutrazure;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use yii\web\HttpException;

class NutrazureBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel = $this->incomingModel;
		
		$this->logTerraleadsRequest($incomingModel->attributes);
		
		$config = $this->config;
		$configUser = (object)$config->user->array;
		$configOffer = (object)$config->offer->array;
		
		$this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
				'name' => $incomingModel->name,
				'number' => $incomingModel->phone,
				'city' => $incomingModel->city,
				'post_code' => $incomingModel->zip,
				'email' => $incomingModel->email,
				'clickid' => $incomingModel->id,
				'affiliate_id' => $incomingModel->web_id,
				'token' => $configUser->token,
			
			]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$this->response = $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode = $this->response->getCode();
		$responseContent = $this->response->getContent();
//        var_dump($responseCode);die();
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId = $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status = $json->status ?? null;
		$id = $json->id ?? null;
		
		if ($status == 'success')
			return ['partner_order_id' => (string)$id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
	
}
