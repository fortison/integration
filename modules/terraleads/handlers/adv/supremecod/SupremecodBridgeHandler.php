<?php
namespace app\modules\terraleads\handlers\adv\supremecod;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class SupremecodBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		
		$this->requestData = [
			'firstname' 				=> $incomingModel->name,
			'lastname' 					=> $incomingModel->name,
			'area_code' 				=> $configOffer->area_code ?? $this->getCodeArea($incomingModel->phone),
			'phone' 					=> $incomingModel->phone,
			'product_id' 				=> $configOffer->product_id,
			'ip' 						=> $incomingModel->ip,
			'country_code' 				=> $incomingModel->country,
			'referrer_url' 				=> $configOffer->referrer_url ?? 'https://website.com',
			'aff_sub' 					=> $incomingModel->id,
			'source' 					=> $incomingModel->web_id,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestHeaders([
			'Token' 		=> $configUser->token,
		]);
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		$status = $json->result->success ?? null;
		
		if ($status) {
			return ['partner_order_id' => (string) $json->result->lead_id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
	
	public function getCodeArea($phone)
	{
		// Удаляем все пробелы, дефисы и другие символы, кроме цифр и плюса
		$phoneNumber = preg_replace('/[^0-9+]/', '', $phone);
		
		// Проверяем, начинается ли номер с символа "+"
		if (substr($phoneNumber, 0, 1) === '+') {
			// Если номер начинается с "+", получаем первые три цифры после него
			$areaCode = substr($phoneNumber, 1, 3);
		} else {
			// Если номер не начинается с "+", добавляем его в начало
			$phoneNumber = '+' . $phoneNumber;
			// Получаем первые три цифры после "+"
			$areaCode = substr($phoneNumber, 1, 3);
		}
		
		return $areaCode;
	}
}