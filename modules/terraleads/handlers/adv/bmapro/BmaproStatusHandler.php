<?php

namespace app\modules\terraleads\handlers\adv\bmapro;

use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PreparePostRequest;

class BmaproStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$terraleadStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($terraleadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToTerraleads($terraleadStatusDataModel, $needSendStatus);
		
		return $terraleadStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		
		$requestUrl = $config->getUrlOrderInfo() . '?id=' . $configUser->api_token;
		
		$requestData = [
			'oid' 		=> $this->orderModel->partner_order_id
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestData($requestData);
		
		return $this->statusRequest($prepareRequest);
	}
	
	/**
	 * @inheritdoc
	 * @throws \Exception
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     	= $this->config->user->getApiKey();
		$orderModel 	= $this->orderModel;
		$orderId		= $orderModel->id;
		$terraleadsId	= $orderModel->system_order_id;
		$partnerId		= $orderModel->partner_order_id;
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content	= $responseModel->content;
		$order		= $this->decode($content);
		if (!$order) {
			throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
		}

		if (empty($order[0]->status)) {
			throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
		}
		
		$partnerStatus 				= $order[0]->status;
		$terraleadsStatus			= $this->getTerraleadsStatusName($partnerStatus);
		$terraleadsStatusDataModel 	= new TerraleadsStatusDataModel($apiKey);
		
		$terraleadsStatusDataModel->setId($terraleadsId);
		$terraleadsStatusDataModel->setUserId($orderModel->system_user_id);
		$terraleadsStatusDataModel->setStatus($terraleadsStatus);
		$terraleadsStatusDataModel->setPartnerStatus($partnerStatus);
		$terraleadsStatusDataModel->setPartnerId($partnerId);
		
		return $terraleadsStatusDataModel;
	}
}