<?php

namespace app\modules\terraleads\handlers\adv\whocpa;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class WhocpaBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $incomingModel	= $this->incomingModel;

        $this->requestUrl	= $config->getUrlOrderAdd();
        $this->requestData	= [
            'api_key'            => $configUser->api_key,
            'offer_id'          => $configOffer->offer_id,
            'name'           => $incomingModel->name,
            'phone'              => $incomingModel->phone,
            'ip'           => $incomingModel->ip,
            'aff_id'      => $configOffer->aff_id,
            'click_id'      => $incomingModel->id,
            'cr_id'      => $incomingModel->web_id,
        ];

        $prepareRequest = new PreparePostRequest();
        $prepareRequest->setRequestUrl($this->requestUrl);
        $prepareRequest->setRequestData($this->requestData);

        $this->response	= $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();

        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($responseContent);
//        print_r($json);die();
        $status = $json->status ?? null;
        $order = $json->id ?? null;

        if ($status === 'accepted')
            return ['partner_order_id' => (string)$order];

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}
