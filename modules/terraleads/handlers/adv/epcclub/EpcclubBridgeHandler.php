<?php
namespace app\modules\terraleads\handlers\adv\epcclub;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class EpcclubBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		$args = [$incomingModel, $config];
		$this->requestUrl	= $config->getUrlOrderAdd();
		
		$this->requestData = [
			'api_token'	=> $configUser->api_token,
			'first_name' => $incomingModel->name,
			'last_name' => $incomingModel->name,
			'email' => $incomingModel->email,
//			'prefix' => '972',
			'phone' => $incomingModel->phone,
			'page_name' => $configOffer->page_name,
			'actual_url' => $configOffer->actual_url,
			'country_iso' => $incomingModel->country,
//			'language' => $incomingModel->country,
			'ip' => $incomingModel->ip,
			'sub_1' => $incomingModel->id,
			'sub_2' => $incomingModel->getWHash(),
		];
		
		if (!empty($configOffer->language)) {
			if (is_callable($configOffer->language))
				$this->requestData['language'] = call_user_func_array($configOffer->language, $args);
			else
				$this->requestData['language'] = $configOffer->language;
		}
		
		if (!empty($configOffer->prefix)) {
			if (is_callable($configOffer->prefix))
				$this->requestData['prefix'] = call_user_func_array($configOffer->prefix, $args);
			else
				$this->requestData['prefix'] = $configOffer->prefix;
		}
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		$prepareRequest->setRequestHeaders(['Content-Type' => 'application/json', 'accept' => 'application/json']);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		$status = $json->code;
		
		if (!empty($status) && $status > 199 && $status < 203) {
			return ['partner_order_id' => (string) $json->data->id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}