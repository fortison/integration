<?php
namespace app\modules\terraleads\handlers\adv\stickcrm;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class StickcrmBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl = $config->getUrlOrderAdd() .'?api_token=' . $configUser->api_token;
		
		$this->requestData = [
			'order' => [
				'phone' => $incomingModel->phone,
				'name' => $incomingModel->name,
				'email' => $incomingModel->email ?? '',
				'comment' => $incomingModel->user_comment ?? '',
				'postcode' => $incomingModel->zip,
				'source_id' => $configUser->source_id,
				'addition_id' => $incomingModel->web_id,
				'products' => [
					[
						'offer_id' => $configOffer->offer_id,
						'price' => $incomingModel->landing_cost,
						'quantity' => $incomingModel->count
					]
				],
				'customFields' => [
					$configUser->affiliateIDField => $incomingModel->id,
					$configUser->webIDField => $incomingModel->web_id,
				]
			]
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestHeaders(['Content-Type' => 'application/x-www-form-urlencoded"']);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (property_exists($json, 'success')
			&& $json->success == 1
			&& property_exists($json, 'order')
			&& property_exists($json->order, 'number')) {
			return ['partner_order_id' => (string) $json->order->number];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}