<?php

namespace app\modules\terraleads\handlers\adv\jercourier;

use app\modules\terraleads\common\objects\PrepareGetRequest;
use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use yii\web\HttpException;

class JercourierBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $incomingModel	= $this->incomingModel;

        $this->logTerraleadsRequest($incomingModel->attributes);

        $config			= $this->config;
        $configOffer	= (object) $config->offer->array;

        $this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
                'offer_id'  		 => $configOffer->offer_id,
                'api_key'  			 => $configOffer->api_key,
                'name' 				 => $incomingModel->name ?? '',
                'phone' 	 		 => $incomingModel->phone ?? '',
                'country' 			 => $incomingModel->country ?? '',
                'subid1'             => $incomingModel->web_id ?? '',
                'click_id'           => $incomingModel->id ?? '',
                'payout'             => $incomingModel->cost,
                'proveedor'          => 'terraleads',

            ]);

        $prepareRequest = new PrepareGetRequest();
        $prepareRequest->setRequestUrl($this->requestUrl);
        $this->response	= $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();

        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		if (strpos($responseContent, 'Error3')) {
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		}

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {

        if ($responseContent != 200) {
            return [];
        }

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}