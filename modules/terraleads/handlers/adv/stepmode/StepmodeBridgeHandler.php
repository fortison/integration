<?php
namespace app\modules\terraleads\handlers\adv\stepmode;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class StepmodeBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd() . '?token=' . $configUser->token;
		
		$this->requestData = [
			'full_name' 	=> $incomingModel->name,
			'city' 			=> $incomingModel->city ?? '',
			'phone'			=> $incomingModel->phone,
			'external'		=> $incomingModel->web_id,
			'address'		=> $incomingModel->address ?? '',
			'goodID'		=> $configOffer->good_id,
			'price'			=> $incomingModel->landing_cost,
			'additional1'	=> $incomingModel->id,
			'additional2'	=> $incomingModel->web_id,
			'geo'			=> $incomingModel->country,
			'quantity'		=> 1,
			'additional3'	=> '',
		];
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$responseContent = trim($responseContent);
		if ($responseContent == 'Token validan') {
			return [];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}