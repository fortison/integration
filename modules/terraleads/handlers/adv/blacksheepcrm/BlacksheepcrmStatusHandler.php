<?php

namespace app\modules\terraleads\handlers\adv\blacksheepcrm;

use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PrepareGetRequest;

class BlacksheepcrmStatusHandler extends BaseStatusHandler
{
	/**
	 * @inheritdoc
	 */
	public function updateStatusInSystem($force = false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$terraleadsStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($terraleadsStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToTerraleads($terraleadsStatusDataModel, $needSendStatus);
		
		return $terraleadsStatusDataModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function makeStatusRequest()
	{
		$config = $this->config;
		$configUser = (object)$config->user->array;
		
		$orderModel = $this->orderModel;
		$requestUrl = $this->config->getUrlOrderInfo() . "?" . http_build_query(['id' => $orderModel->system_order_id]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestHeaders(['Authorization' 	=> 'Bearer ' . $configUser->bearer]);
		$prepareRequest->setRequestUrl($requestUrl);
		
		return $this->statusRequest($prepareRequest);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId = $orderModel->id;
		$apileadId = $orderModel->system_order_id;
		$partnerId = $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content = $responseModel->content;
		$order = $this->decode($content);
		
		if (!$order) {
			throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
		}
		
		if (!isset($order->data->status)) {
			throw new OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$apileadId}");
		}
		
		$partnerStatus = $order->data->status;
		$apileadStatus = $this->getTerraleadsStatusName($partnerStatus);
		$apileadStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setUserId($orderModel->system_user_id);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($partnerId);
		
		return $apileadStatusDataModel;
	}
}