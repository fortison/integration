<?php
namespace app\modules\terraleads\handlers\adv\blacksheepcrm;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class BlacksheepcrmBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'first_name' 	=> $incomingModel->name,
			'phone' 		=> $incomingModel->phone,
			'web_id' 		=> $incomingModel->web_id,
			'external_id' 	=> $incomingModel->id,
			'product' 		=> $configOffer->product,
			'iso' 			=> $incomingModel->country,
			'landing' 		=> 'landing',
			'ip' 			=> $incomingModel->ip,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		$prepareRequest->setRequestHeaders(
			[
				'Content-Type'		=> 'application/x-www-form-urlencoded',
				'Authorization' 	=> 'Bearer ' . $configUser->bearer,
			]
		);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode < 200 && $responseCode > 210)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (!empty($json->data) && !empty($json->data->id)) {
			return [
				'partner_order_id' => (string) $json->data->id,
			];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}