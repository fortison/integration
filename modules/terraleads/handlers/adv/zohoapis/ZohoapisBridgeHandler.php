<?php
namespace app\modules\terraleads\handlers\adv\zohoapis;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class ZohoapisBridgeHandler extends BaseBridgeHandler
{
	/**
	 * @inheritdoc
	 */
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		$configUrls     = $config->full->configs['urls'][$configUser->acc_domain];
		
		$incomingModel	= $this->incomingModel;
		
		$client	= new Client([
			'config' => $config,
		]);
		
		$access_token = $client->getAccessToken();
		
		$this->requestUrl = $configUrls['urlOrderAdd'];

		$this->requestData = [
			"data" => array(
				array(
					"Product_Code" => $configOffer->product_code ?? '',
					"Company" => $configOffer->company,
//					"Full_Name" => $incomingModel->name,
					"Last_Name" => $incomingModel->name,
//					"First_Name" => $incomingModel->name,
					"Phone" => $incomingModel->phone,
					"Mobile" =>$incomingModel->phone,
					"Country" => $incomingModel->country,
					"Fax" => $incomingModel->id,
					"City" => $incomingModel->city,
					"Lead_Source" => $configOffer->external ?? '',
					"Tag" => array('terraleads'),
					"Street" => $incomingModel->address,
					"Zip_Code" => $incomingModel->zip,
					"Email" => $incomingModel->email,
					"Adres_URL_1" => $incomingModel->website ?? '',
				)
			),
			"trigger" => [
				"approval",
				"workflow",
				"blueprint"
			],
		];

		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestHeaders(['Authorization' => 'Zoho-oauthtoken ' . $access_token]);
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if (!empty($this->response->error))
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200 && $responseCode != 201)
			throw new HttpException('400', "Partner error! Response code: {$responseCode} Response: {$responseContent}");
		
		$partnerOrderId	= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		$id = (string) $json->data[0]->details->id ?? null;
		
		if (is_numeric($id)) {
			return ['partner_order_id' => (string) $id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}