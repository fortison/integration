<?php

namespace app\modules\terraleads\handlers\adv\zohoapis;

use app\modules\terraleads\common\objects\PreparePostRequest;
use app\modules\terraleads\common\traits\{
	JsonTrait,
	LogTrait,
	RequestTrait
};
use Yii;
use yii\base\BaseObject;
use yii\web\HttpException;

/**
 * @property \app\modules\terraleads\common\adv\objects\Config $config Головна конфігурація поточного замовлення
 */
class Client extends BaseObject
{
	use RequestTrait;
	use LogTrait;
	use JsonTrait;
	
	public $config;	// головна конфігурація замовлення
	
	public function __construct(array $config)
	{
		parent::__construct($config);
	}
	
	public function getAccessToken()
	{
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configUrls     = $config->full->configs['urls'][$configUser->acc_domain];
		
		$cache = Yii::$app->cache;
		$access_token = $cache->get($configUser->refresh_token);
		
		if ($access_token === false) {
			$access_token = $this->requestAccessToken(
				$configUrls['urlRefreshToken'],
				$configUser->refresh_token,
				$configUser->client_id,
				$configUser->client_secret
			);
			
			// Сохраняем значение $data в кэше. Данные можно получить в следующий раз.
			$cache->set($configUser->refresh_token, $access_token, 3500);
		}
		
		return $access_token;
	}
	
	private function requestAccessToken($urlRefreshToken, $refresh_token, $client_id, $client_secret)
	{
		$requestData = [
			"refresh_token" => $refresh_token,
			"client_id" => $client_id,
			"client_secret" => $client_secret,
			"grant_type" => 'refresh_token',
		];
		
		$request = new PreparePostRequest();
		$request->setRequestUrl($urlRefreshToken);
		$request->setRequestData($requestData);
		
		$response = $this->bridgeRequest($request);
		
		$responseCode	 = $response->getCode();
		$responseContent = $response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! RefreshToken error! Response code: {$responseCode} Response: {$responseContent}");
		
		return $this->getAccessTokenAttributes($responseContent);
	}
	
	private function getAccessTokenAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (isset($json->access_token)) {
			return $json->access_token;
		}
		
		throw new HttpException('400', "Wrong AccessToken request. {{$responseContent}}");
	}
}