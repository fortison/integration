<?php

namespace app\modules\terraleads\handlers\adv\leadmonetize;

use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PrepareGetRequest;

class LeadmonetizeStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$terraleadStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($terraleadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToTerraleads($terraleadStatusDataModel, $needSendStatus);
		
		return $terraleadStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$config			= $this->config;
		$orderModel 	= $this->orderModel;
		
		$requestUrl = str_replace("{lead_id}", $orderModel->system_order_id, $config->getUrlOrderInfo());
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestHeaders([
			'Content-Type' 	=> 'application/x-www-form-urlencoded',
			'x-api-key' 	=> $this->config->user->array['x_api_key'],
			'x-token' 		=> $this->config->user->array['x_token'],
		]);
		
		return $this->statusRequest($prepareRequest);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$partnerId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content	= $responseModel->content;
		$order		= $this->decode($content);
		
		if (!isset($order->status) || !$order->status) {
			throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
		}
		
		$partnerStatus 		= $order->data->tleads[0]->status ?? 'expect';
		$terraleadsStatus	= $this->getTerraleadsStatusName($partnerStatus);
		
		
		$terraleadsStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$terraleadsStatusDataModel->setId($apileadId);
		$terraleadsStatusDataModel->setUserId($orderModel->system_user_id);
		$terraleadsStatusDataModel->setStatus($terraleadsStatus);
		$terraleadsStatusDataModel->setPartnerStatus($partnerStatus);
		$terraleadsStatusDataModel->setPartnerId($partnerId);
		
		return $terraleadsStatusDataModel;
	}
}