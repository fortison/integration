<?php
namespace app\modules\terraleads\handlers\adv\advertfish;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class AdvertfishBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist()) {
			return $model;
		}
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestData = [
			'token' 	=> $configUser->token,
			'flowkey' 	=> $configOffer->flowkey,
			'name' 		=> $incomingModel->name,
			'phone' 	=> $incomingModel->phone,
			'geoiso' 	=> $configOffer->geoiso ?? $incomingModel->country,
			'ip' 		=> $incomingModel->ip,
			'address' 	=> $incomingModel->address,
			'wm_ref' 	=> $incomingModel->web_id,
			'subid1'    => $incomingModel->id,
		];
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error) {
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		}
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200) {
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		}
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
		public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if ($json->success == true) {
			return ['partner_order_id' => (string) $json->data->orderhash];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}