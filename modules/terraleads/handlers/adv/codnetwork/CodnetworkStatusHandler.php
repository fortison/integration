<?php
namespace app\modules\terraleads\handlers\adv\codnetwork;

use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\traits\JsonTrait;
use Yii;

class CodnetworkStatusHandler extends BaseStatusHandler
{
	use JsonTrait;
	public function updateStatusInSystem($force=false)
	{
		$responseModel	= new ResponseModel;
		$requestUrl		= Yii::$app->request->url;
		$requestData	= Yii::$app->request->post();
		$this->logStatusRequest($responseModel, $requestUrl, $requestData);
		
		$apiKey     		= $this->config->user->getApiKey();
		$orderModel			= $this->orderModel;
		$orderId			= $orderModel->id;
		$terraleadsId		= $orderModel->system_order_id;
		$partnerId			= $orderModel->partner_order_id;
		$partnerStatus 		= Yii::$app->request->get('partnerStatus');
		$comment 			= Yii::$app->request->get('comment');
		
		if (!isset($partnerStatus))
			throw new OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$terraleadsId}");
		
		$systemStatus		= $this->getTerraleadsStatusName($partnerStatus);
		
		$terraleadsStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$terraleadsStatusDataModel->setId($terraleadsId);
		$terraleadsStatusDataModel->setUserId($orderModel->system_user_id);
		$terraleadsStatusDataModel->setComment($comment);
		$terraleadsStatusDataModel->setStatus($systemStatus);
		$terraleadsStatusDataModel->setPartnerStatus($partnerStatus);
		$terraleadsStatusDataModel->setPartnerId($partnerId);
		
		try {
			$this->sendOrderStatusToTerraleads($terraleadsStatusDataModel, true);
			$orderModel->system_status = $systemStatus;
			$orderModel->partner_status = $partnerStatus;
			$orderModel->comment = $comment;
			$orderModel->save();
		} catch (\Exception $e) {
			throw new OrderErrorException($orderId, "Error: {$e->getMessage()}");
		} finally {
			print_r($this->encode(['status' => 'OK', 'clickid' => $orderId]));
		}
		
		return $terraleadsStatusDataModel;
	}
	
	public function makeStatusRequest(){}
	public function getSystemStatusData(ResponseModel $responseModel){}
}
