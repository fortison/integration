<?php

namespace app\modules\terraleads\handlers\adv\wowsystem;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class WowsystemBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd() . '?hash=' . $configUser->hash;
		$this->requestData	= [[
			'id' 			=> $incomingModel->id,
			'phone'			=> $incomingModel->phone,
			'name'			=> $incomingModel->name,
			'iso'			=> $incomingModel->country,
			'offerId'		=> $configOffer->offer,
			'webmasterId'	=> $incomingModel->web_id,
			'ipAddress'		=> $incomingModel->ip,
		]];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent, true);
		
		$keys = array_keys($json);
		$firstKey = $keys[0];
		$id = $json[$firstKey]['id'] ?? null;
		
		if (!empty($id)) {
			return [
				'partner_order_id' => (string)$id,
			];
		}
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}