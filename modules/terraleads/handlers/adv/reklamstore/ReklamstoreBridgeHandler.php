<?php
namespace app\modules\terraleads\handlers\adv\reklamstore;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class ReklamstoreBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'page_name' => $configOffer->page_name,
			'name' => $incomingModel->name,
			'phone' => $incomingModel->phone,
			'country' => $incomingModel->country,
			'address' => $incomingModel->address,
			'aff_sub' => $incomingModel->id,
			'aff_id' => $configOffer->aff_id,
		];
		
//		sha1($incomingModel->id . $incomingModel->campaign_id . $incomingModel->name . $incomingModel->phone . $incomingModel->country . $incomingModel->cost . $incomingModel->api_key);
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestHeaders(['User-Agent' => 'Mozilla/5.0']);
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
//        print_r($json);die();
		$status = $json->status ?? 0;
		
		if ($status === 1 || $status === 'ok') {
			return ['partner_order_id' => (string) $json->order_id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}