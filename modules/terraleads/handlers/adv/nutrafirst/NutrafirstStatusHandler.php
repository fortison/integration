<?php
namespace app\modules\terraleads\handlers\adv\nutrafirst;

use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PreparePostRequestJson;


class NutrafirstStatusHandler extends BaseStatusHandler
{
    public function updateStatusInSystem($force=false)
    {
        $responseModel = $this->makeStatusRequest();

        $terraleadStatusDataModel = $this->getSystemStatusData($responseModel);

        $needSendStatus = $this->orderModel->updateOrder($terraleadStatusDataModel);

        if ($force)
            $needSendStatus = true;

        $this->sendOrderStatusToTerraleads($terraleadStatusDataModel, $needSendStatus);

        return $terraleadStatusDataModel;
    }

    public function makeStatusRequest()
    {
        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $orderModel = $this->orderModel;

        $requestUrl = $config->getUrlOrderInfo();
        $requestData = [
            'order_id' => [$orderModel->system_order_id]
        ];

        $prepareRequest = new PreparePostRequestJson();
        $prepareRequest->setRequestUrl($requestUrl);
        $prepareRequest->setRequestHeaders(['api-key' => $configUser->api_key]);
        $prepareRequest->setRequestData($requestData);

        return $this->statusRequest($prepareRequest);
    }

    /**
     * @inheritdoc
     */
    public function getSystemStatusData(ResponseModel $responseModel)
    {
        $apiKey     = $this->config->user->getApiKey();
        $orderModel = $this->orderModel;
        $orderId	= $orderModel->id;
        $apileadId	= $orderModel->system_order_id;
        $partnerId	= $orderModel->partner_order_id;

        if (!empty($responseModel->error))
            throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");

        if ($responseModel->code != 200)
            throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");

        $content		= $responseModel->content;
        $order	= $this->decode($content);

        if (!$order) {
            throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
        }

        if ($order->status != 200)  {
            throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
        }

        $orderData = current($order->data);

        if (empty($orderData))
            throw new OrderErrorException($orderId, "Partner error!. Data err. Response: {$responseModel->content}");

        $partnerStatus 		= $orderData->lead_status ?? null;
        $apileadStatus		= $this->getTerraleadsStatusName($partnerStatus);
        $comment			= '';

        $apileadStatusDataModel = new TerraleadsStatusDataModel($apiKey);
        $apileadStatusDataModel->setId($apileadId);
        $apileadStatusDataModel->setUserId($orderModel->system_user_id);
        $apileadStatusDataModel->setComment($comment);
        $apileadStatusDataModel->setStatus($apileadStatus);
        $apileadStatusDataModel->setPartnerStatus($partnerStatus);
        $apileadStatusDataModel->setPartnerId($partnerId);

        return $apileadStatusDataModel;
    }
}