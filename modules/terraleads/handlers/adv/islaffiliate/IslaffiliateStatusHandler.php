<?php

namespace app\modules\terraleads\handlers\adv\islaffiliate;

use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PrepareGetRequest;

class IslaffiliateStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		$terraleadsStatusDataModel = $this->getSystemStatusData($responseModel);
		$needSendStatus = $this->orderModel->updateOrder($terraleadsStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToTerraleads($terraleadsStatusDataModel, $needSendStatus);
		
		return $terraleadsStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->config->getUrlOrderInfo() . $this->orderModel->partner_order_id);
		$prepareRequest->setRequestHeaders([
			'x-api-key' 	=> $this->config->user->array['api_key'],
			'x-user-id' 	=> $this->config->user->array['user_id'],
		]);
		
		return $this->statusRequest($prepareRequest);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     	= $this->config->user->getApiKey();
		$orderModel 	= $this->orderModel;
		$orderId		= $orderModel->id;
		$terraleadsId	= $orderModel->system_order_id;
		$partnerId		= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$terraleadsId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content		= $responseModel->content;
		$contentObject	= $this->decode($content);
		
		if (empty($contentObject))
			throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
		
		$contentObject = $contentObject->{$partnerId} ?? null;
		
		if (!$contentObject)
			throw new OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$terraleadsId}");
		
		$partnerStatus 		= (string) $contentObject->leadStatus;
		$terraleadsStatus	= $this->getTerraleadsStatusName($partnerStatus);
		$comment			= '';
		
		$terraleadsStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$terraleadsStatusDataModel->setId($terraleadsId);
		$terraleadsStatusDataModel->setUserId($orderModel->system_user_id);
		$terraleadsStatusDataModel->setComment($comment);
		$terraleadsStatusDataModel->setStatus($terraleadsStatus);
		$terraleadsStatusDataModel->setPartnerStatus($partnerStatus);
		$terraleadsStatusDataModel->setPartnerId($partnerId);
		
		return $terraleadsStatusDataModel;
	}
}
