<?php

namespace app\modules\terraleads\handlers\adv\islaffiliate;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class IslaffiliateBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'offer'				=> $configOffer->offer,
			'lp'				=> $configOffer->lp,
			'name'				=> $incomingModel->name,
			'postal-code'		=> $incomingModel->zip,
			'tel'				=> $incomingModel->phone,
			'street-address'	=> $incomingModel->address,
			'clickid'			=> $incomingModel->id,
			'subid'				=> $incomingModel->id,
			'subid2'			=> $incomingModel->web_id,
			'ip'				=> $incomingModel->ip,
			'ua'				=> $incomingModel->user_agent,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		$prepareRequest->setRequestHeaders([
			'x-api-key' 	=> $configUser->api_key,
			'x-user-id' 	=> $configUser->user_id,
		]);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$message = $json->message ?? null;
		$lead_id = $json->leadId ?? null;
		
		if ($message == 'OK' && $lead_id) {
			return [
				'partner_order_id' => (string) $lead_id,
			];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}