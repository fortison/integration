<?php
namespace app\modules\terraleads\handlers\adv\monetizead;

use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use Yii;
use yii\helpers\Json;

class MonetizeadStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$terraleadStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($terraleadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToTerraleads($terraleadStatusDataModel, $needSendStatus);
		
		return $terraleadStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$orderModel = $this->orderModel;
		
		$requestUrl = preg_replace(
			['/{key}/', '/{company_id}/'],
			[$configUser->key, $configOffer->company_id],
			$config->getUrlOrderInfo()
		);
		
		$requestUrl = $requestUrl . '&'
			. http_build_query(
				[
					'id' => $orderModel->partner_order_id,
				]
			);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		
		return $this->statusRequest($prepareRequest);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$partnerId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content		= $responseModel->content;
		$order	= $this->decode($content);
		
		if (!$order) {
			throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
		}

        if (!isset($order->status)) {
            throw new \app\modules\terraleads\common\exeptions\OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$apileadId}");
        }

		$partnerStatus 		= $order->status ?? null;
		$apileadStatus		= $this->getTerraleadsStatusName($partnerStatus);
		$comment			= $order->comment;
		
		$apileadStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setUserId($orderModel->system_user_id);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($partnerId);
		
		return $apileadStatusDataModel;
	}
}