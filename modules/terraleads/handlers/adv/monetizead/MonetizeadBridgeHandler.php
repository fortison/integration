<?php
namespace app\modules\terraleads\handlers\adv\monetizead;

use app\common\models\OrderApilead;
use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\objects\PreparePostRequest;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\httpclient\Client;
use yii\web\HttpException;

class MonetizeadBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist()) {
			return $model;
		}
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$args = [$incomingModel, $config];
		
		$this->requestData = [
			'offer_id'   	=> $configOffer->offer_id,
			'network' 		=> $configOffer->network,
			'name'  		=> $incomingModel->name,
			'phone' 		=> $incomingModel->phone,
			'country_code'  => $incomingModel->country ?? '',
			'client_ip' 	=> $incomingModel->ip,
			'address' 		=> $incomingModel->address ?? '',
			'email'  		=> $incomingModel->email ?? '',
			'zip'    		=> $incomingModel->zip,
			'quantity'		=> $incomingModel->count,
			'partner_id'    => 6,
			'tracking'  	=> [
				'sub_1' => $incomingModel->id,
				'sub_2' => $incomingModel->getWHash(),
				'sub_3' => $incomingModel->getUHash(),
			],
		];
		
		$this->requestUrl = preg_replace(
			['/{key}/', '/{company_id}/'],
			[$configUser->key, $configOffer->company_id],
			$config->getUrlOrderAdd()
		);
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error) {
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		}
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200 && $responseCode != 201) {
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		}
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (isset($json->id)) {
			return ['partner_order_id' => (string) $json->id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}