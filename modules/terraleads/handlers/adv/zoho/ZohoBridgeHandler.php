<?php
namespace app\modules\terraleads\handlers\adv\zoho;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;
use Yii;

class ZohoBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$external = $configOffer->external ?? '';
		
		$xmlData = <<<XML
<Leads>
	<row no="1">
		<!--<FL val="SMOWNERID">2000000016714</FL>-->
		<!--<FL val="Lead Owner">{\$configUser->leadOwner}</FL>-->
		<FL val="Adres URL 1">{$configOffer->company}</FL>
		<FL val="First Name">{$incomingModel->name}</FL>
		<FL val="Last Name">.</FL>
		<!--<FL val="Designation">CEO</FL>-->
		<!--<FL val="Email">john@test.com</FL>-->
		<FL val="Phone">{$incomingModel->phone}</FL>
		<FL val="Fax">{$incomingModel->id}</FL>
		<!--<FL val="Mobile">09999999999</FL>-->
		<!--<FL val="Website">www.sample.com</FL>-->
		<FL val="Lead Source">{$external}</FL>
		<!--<FL val="Lead Status">Contacted</FL>-->
		<!--<FL val="Industry">Financial Services</FL>-->
		<!--<FL val="No of Employees">100</FL>-->
		<!--<FL val="Annual Revenue">100.0</FL>-->
		<!--<FL val="Email Opt Out">true</FL>-->
		<!--<FL val="Skype ID">peter</FL>-->
		<FL val="Salutation">{$incomingModel->id}</FL>
		<!--<FL val="Street">Street One</FL>-->
		<!--<FL val="City">Chennai</FL>-->
		<!--<FL val="State">Tamil Nadu</FL>-->
		<!--<FL val="Zip Code">6000001</FL>-->
		<!--<FL val="Country">India</FL>-->
		<!--<FL val="Description">Sample Description.</FL>-->
	</row>
</Leads>
XML;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'authtoken' => $configUser->authtoken,
			'scope' => 'crmapi',
			'xmlData' => $xmlData,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$xml = simplexml_load_string($responseContent);
		
		$id = (string) $xml->result->recorddetail->FL[0] ?? null;
		
		if (is_numeric($id)) {
			return ['partner_order_id' => (string) $id];
		}
		
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}