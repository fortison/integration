<?php
namespace app\modules\terraleads\handlers\adv\zoho;

use nahard\log\helpers\Log;
use yii\base\BaseObject;
use Yii;

class ZohoExtraHandler extends BaseObject
{
	public function extra()
	{
		Log::error('Oauth', Yii::$app->request->get());
	}
}