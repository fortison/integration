<?php

namespace app\modules\terraleads\handlers\adv\telmarksrv;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use Yii;
use yii\web\HttpException;

class TelmarksrvBridgeHandler extends BaseBridgeHandler
{
	/**
	 * @throws HttpException
	 */
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;

		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$this->requestData = [
			'sku' 			=> $configOffer->sku,
			'firstName' 	=> $incomingModel->name,
			'phone' 		=> $incomingModel->phone,
			'clickID' 		=> $incomingModel->id,
			'publishID' 	=> $incomingModel->web_id,
			'clientIP' 		=> $incomingModel->ip,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestHeaders([
			'Content-Type' 	=> 'application/x-www-form-urlencoded',
			'username' 		=> $configUser->email,
			'password' 		=> $configUser->password,
		]);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		$status = $json->status;
		$data = $json->data[0] ?? null;
		
		if ($status == 'Success' && $data->lead->LeadID) {
			return ['partner_order_id' => (string) $data->lead->LeadID];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}