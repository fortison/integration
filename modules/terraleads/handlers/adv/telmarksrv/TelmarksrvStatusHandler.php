<?php

namespace app\modules\terraleads\handlers\adv\telmarksrv;

use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PrepareGetRequestJson;
use app\modules\terraleads\common\objects\PreparePostRequest;
use Yii;
use yii\web\HttpException;

class TelmarksrvStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$terraleadStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($terraleadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToTerraleads($terraleadStatusDataModel, $needSendStatus);
		
		return $terraleadStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$configUser	= (object) $this->config->user->array;
		
		$requestUrl = $this->config->getUrlOrderInfo();
		
		$requestData = [
			'leadID' => $this->orderModel->partner_order_id,
		];
		
		$prepareRequest = new PrepareGetRequestJson();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestData($requestData);
		$prepareRequest->setRequestHeaders([
			'Content-Type' 		=> 'application/x-www-form-urlencoded',
			'username' 			=> $configUser->email,
			'password' 			=> $configUser->password,
		]);
		
		return $this->statusRequest($prepareRequest);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     	= $this->config->user->getApiKey();
		$orderModel 	= $this->orderModel;
		$orderId		= $orderModel->id;
		$terraleadsId	= $orderModel->system_order_id;
		$partnerId		= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$terraleadsId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$order	= $this->decode($responseModel->content);
		
		if (!isset($order->data[0]->statusName)) {
			throw new OrderErrorException($orderId, "Partner error!1. Response: {$responseModel->content}");
		}
		
		$partnerStatus      = $order->data[0]->statusName;
		$terraleadsStatus	= $this->getTerraleadsStatusName($partnerStatus);
		$comment			= $order->comment ?? '';
		
		$apileadStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($terraleadsId);
		$apileadStatusDataModel->setUserId($orderModel->system_user_id);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($terraleadsStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($partnerId);
		
		return $apileadStatusDataModel;
	}
}