<?php

namespace app\modules\terraleads\handlers\adv\cpadojo;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class CpadojoBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'api_key' => $configUser->api_key, // API KEY - PLEASE DON'T CHANGE, THIS IS PROVIDED BY LEVEL NEXT DIRECT
			'order_id' => $incomingModel->id,
			'phone' => $incomingModel->phone, // CLIENT PHONE NUMBER, MANDATORY!
			'name' => $incomingModel->name, // CLIENT FIRST AND LAST NAME, MANDATORY!
			'goods_id' => $configOffer->goods_id, // CAMPAIGN ID, MANDATORY!  THIS OUR (CPADojo) OFFER ID, DON'T CHANGE!
			'email' => $incomingModel->email ?? null, // CLIENT EMAIL, OPTIONAL
			'address' => $incomingModel->address  ?? null, // CLIENT CITY, OPTIONAL
			'address_street' => $incomingModel->city ?? null, // CLIENT STREET ADDRESS, OPTIONAL
			'address_zip' => $incomingModel->zip, // CLIENT ZIP CODE, OPTIONAL
			'courier_note' => $incomingModel->user_comment, // ADDITIONAL NOTES FOR COURIER ENTERED BY THE CLIENT, OPTIONAL
			'tracking_params' => 'clickid='.$incomingModel->id, // SEND clickid like in URL: clickid=12345678
			'subid' => $incomingModel->web_id // SEND affiliate ID (subid) here
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$order = $this->decode($responseContent);
		
		if ($order->status == 'OK') {
			return [
				'partner_order_id' => (string) $order->ext_id,
			];
		}
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}