<?php
namespace app\modules\terraleads\handlers\adv\leadvertex;

use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use Yii;
use yii\base\InvalidArgumentException;

/**
 * Обробник статусів замовлень до інтеграції Leadvertext
 *
 * User: nagard
 * Date: 31.10.17
 * Time: 14:01
 */
class LeadvertexStatusHandler extends BaseStatusHandler
{
	/**
	 * @inheritdoc
	 */
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$terraleadsStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($terraleadsStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToTerraleads($terraleadsStatusDataModel, $needSendStatus);
		
		return $terraleadsStatusDataModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function makeStatusRequest()
	{
		$requestUrl = $this->config->getUrlOrderInfo() . '?' . http_build_query([
			'webmasterID'	=> $this->config->offer->array['webmasterID'] ?? $this->config->user->array['webmasterID'],
			'token'			=> $this->config->offer->array['token'],
			'ids'			=> $this->orderModel->partner_order_id,
		]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);

		$data = $this->statusRequest($prepareRequest);

		return $data;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$terraleadsId	= $orderModel->system_order_id;
		$sourceId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$terraleadsId}");
		
		$content		= $responseModel->content;
		
		try {
			$contentObject	= $this->decode($content);
		} catch (\Exception $e) {
			throw new OrderErrorException($orderId, "Decode json error: {$e->getMessage()}");
		}
		
		if (!isset($contentObject->{$sourceId}))
			throw new OrderErrorException($orderId, "Lead not found {$sourceId}:{$terraleadsId}");
		
		$contentObject = $contentObject->{$sourceId};
		
//		if (!isset($contentObject->fields->comment))
//			throw new HttpException('405', "Comment not found for lead {$sourceId}:{$terraleadsId}");
		
		if (!isset($contentObject->status))
			throw new OrderErrorException($orderId, "Status is not available for you {$sourceId}:{$terraleadsId}");

		
		$comment		= $contentObject->fields->comment ?? '';
		$roboLog		= $contentObject->roboLog ?? '';
		$comment		= "comment: {$comment}; \nroboLog: {$roboLog}";
		$partnerStatus 	= (string) $contentObject->status;
		$terraleadsStatus	= $this->getTerraleadsStatusName($partnerStatus);

		$terraleadsStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$terraleadsStatusDataModel->setId($terraleadsId);
		$terraleadsStatusDataModel->setUserId($orderModel->system_user_id);
		$terraleadsStatusDataModel->setComment($comment);
		$terraleadsStatusDataModel->setStatus($terraleadsStatus);
		$terraleadsStatusDataModel->setPartnerStatus($partnerStatus);
		$terraleadsStatusDataModel->setPartnerId($sourceId);

		return $terraleadsStatusDataModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getRansomStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$terraleadsId	= $orderModel->system_order_id;
		$sourceId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$terraleadsId}");
		
		$content	= $responseModel->content;

        try {
            $contentObject = $this->decode($content);
        } catch (InvalidArgumentException $argumentException){
            $contentObject = $this->xmlDecode($content);
        } catch (\Throwable $throwable) {
            throw new OrderErrorException($orderId, "Content Parse Exeption {$sourceId}:{$terraleadsId}");
        }

		
		if (!isset($contentObject->{$sourceId}))
			throw new OrderErrorException($orderId, "Lead not found {$sourceId}:{$terraleadsId}");
		
		$contentObject = $contentObject->{$sourceId};
		
		if (!isset($contentObject->status))
			throw new OrderErrorException($orderId, "Status is not available for you {$sourceId}:{$terraleadsId}");
		
		$comment		= $contentObject->fields->comment ?? '';
		$partnerStatus 	= $contentObject->status;
		$terraleadsStatus	= $this->getRansomStatusName($partnerStatus);

		$terraleadsStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$terraleadsStatusDataModel->setId($terraleadsId);
		$terraleadsStatusDataModel->setComment($comment);
		$terraleadsStatusDataModel->setStatus($terraleadsStatus);
		$terraleadsStatusDataModel->setPartnerStatus($partnerStatus);
		$terraleadsStatusDataModel->setPartnerId($sourceId);

		return $terraleadsStatusDataModel;
	}
}