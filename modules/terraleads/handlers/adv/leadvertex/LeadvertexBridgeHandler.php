<?php
namespace app\modules\terraleads\handlers\adv\leadvertex;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use yii\web\HttpException;

class LeadvertexBridgeHandler extends BaseBridgeHandler
{
	/**
	 * @inheritdoc
	 */
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel		= $this->incomingModel;
		
		$args = [$incomingModel, $config];
		
		$quantity = 1;
		
		if (!empty($incomingModel->count) && $incomingModel->count != 0) {
			$quantity = $incomingModel->count;
		}
		
		$this->requestData = [
			"fio" => $incomingModel->name,
			"country" => $incomingModel->country,
			"phone" => $incomingModel->phone,
			"timezone" => ($incomingModel->tz*60) * -1,
			// "referer" => 'http://' . $incomingModel->uHash ?: $incomingModel->wHash . '.com',
			//"referer" => $incomingModel->wHash ?? '',
//			"postIndex" => $incomingModel->id,
			"price" => $incomingModel->landing_cost,
			"total" => $incomingModel->landing_cost,
//			"ip" => $incomingModel->ip,
            "email" => $incomingModel->email,
			"postIndex" => $incomingModel->zip,
			"quantity" => $quantity,
		];

		if ($configOffer->configs['parsePhone'] ?? null) {
			$phoneConfig = $configOffer->configs['parsePhone'];
			$phoneRegion = $incomingModel->country;

			if ($phoneConfig['region'] ?? null) {
				if (is_callable($phoneConfig['region'])) {
					$phoneRegion = call_user_func_array($phoneConfig['region'], $args);
				} else {
					$phoneRegion = $phoneConfig['region'];
				}
			}

			$phoneNumber = $incomingModel->phone;

			if (is_callable($phoneConfig['number'] ?? null)) {
				$phoneNumber = call_user_func_array($phoneConfig['number'], $args);
			}

			if ($phoneConfig['format'] === 'e164') {
				$phoneFormat = PhoneNumberFormat::E164;
			} elseif ($phoneConfig['format'] === 'national') {
				$phoneFormat = PhoneNumberFormat::NATIONAL;
			} elseif ($phoneConfig['format'] === 'rfc3966') {
				$phoneFormat = PhoneNumberFormat::RFC3966;
			} else {
				$phoneFormat = PhoneNumberFormat::INTERNATIONAL;
			}

			try {
				$phoneParser = PhoneNumberUtil::getInstance();
				$phoneProto = $phoneParser->parse($phoneNumber, $phoneRegion);
				$phoneNumber = $phoneParser->format($phoneProto, $phoneFormat);
			} catch (\Exception $e) {
				$phoneNumber = $incomingModel->phone;
			}

			$this->requestData['phone'] = $phoneNumber;
		}
        if (!empty($configOffer->country)) {
            if (is_callable($configOffer->country))
                $country = call_user_func_array($configOffer->country, $args);
            else
                $country = $configOffer->country;

            $this->requestData['country'] = $country;
        }


        if (!empty($configOffer->externalWebmaster)) {
			if (is_callable($configOffer->externalWebmaster))
				$externalWebmaster = call_user_func_array($configOffer->externalWebmaster, $args);
			else
				$externalWebmaster = $configOffer->externalWebmaster;
			
			$this->requestData['externalWebmaster'] = $externalWebmaster;
		}

		if (!empty($configOffer->externalID)) {
			if (is_callable($configOffer->externalID))
				$externalID = call_user_func_array($configOffer->externalID, $args);
			else
				$externalID = $configOffer->externalID;

			$this->requestData['externalID'] = $externalID;
		}

		if (!empty($configOffer->ip)) {
			if (is_callable($configOffer->ip))
				$ip = call_user_func_array($configOffer->ip, $args);
			else
				$ip = $configOffer->ip;
			
			$this->requestData['ip'] = $ip;
		}
		
		if (!empty($configOffer->additional1)) {
			if (is_callable($configOffer->additional1))
				$additional1 = call_user_func_array($configOffer->additional1, $args);
			else
				$additional1 = $configOffer->additional1;
			
			$this->requestData['additional1'] = $additional1;
		}
		
		if (!empty($configOffer->additional2)) {
			if (is_callable($configOffer->additional2))
				$additional2 = call_user_func_array($configOffer->additional2, $args);
			else
				$additional2 = $configOffer->additional2;
			
			$this->requestData['additional2'] = $additional2;
		}
		
		if (!empty($configOffer->additional3)) {
			if (is_callable($configOffer->additional3))
				$additional3 = call_user_func_array($configOffer->additional3, $args);
			else
				$additional3 = $configOffer->additional3;
			
			$this->requestData['additional3'] = $additional3;
		}
		
		if (!empty($configOffer->additional4)) {
			if (is_callable($configOffer->additional4))
				$additional4 = call_user_func_array($configOffer->additional4, $args);
			else
				$additional4 = $configOffer->additional4;
			
			$this->requestData['additional4'] = $additional4;
		}
		
		if (!empty($configOffer->additional5)) {
			if (is_callable($configOffer->additional5))
				$additional5 = call_user_func_array($configOffer->additional5, $args);
			else
				$additional5 = $configOffer->additional5;
			
			$this->requestData['additional5'] = $additional5;
		}
		
		if (!empty($configOffer->additional6)) {
			if (is_callable($configOffer->additional6))
				$additional6 = call_user_func_array($configOffer->additional6, $args);
			else
				$additional6 = $configOffer->additional6;
			
			$this->requestData['additional6'] = $additional6;
		}
		
		if (!empty($configOffer->additional7)) {
			if (is_callable($configOffer->additional7))
				$additional7 = call_user_func_array($configOffer->additional7, $args);
			else
				$additional7 = $configOffer->additional7;
			
			$this->requestData['additional7'] = $additional7;
		}

        if (!empty($configOffer->additional15)) {
            if (is_callable($configOffer->additional15))
                $additional15 = call_user_func_array($configOffer->additional15, $args);
            else
                $additional15 = $configOffer->additional15;

            $this->requestData['additional15'] = $additional15;
        }
		
		if (!empty($configOffer->domain)) {
			if (is_callable($configOffer->domain))
				$domain = call_user_func_array($configOffer->domain, $args);
			else
				$domain = $configOffer->domain;
			
			$this->requestData['domain'] = $domain;
		}
		
		if (!empty($configOffer->comment)) {
			if (is_callable($configOffer->comment))
				$comment = call_user_func_array($configOffer->comment, $args);
			else
				$comment = $configOffer->comment;
			
			$this->requestData['comment'] = $comment;
		}
		
		if (!empty($configOffer->goodId)) {
			if (is_callable($configOffer->goodId))
				$goodId = call_user_func_array($configOffer->goodId, $args);
			else
				$goodId = $configOffer->goodId;
			
			$this->requestData['goods'] = [[
				'goodID' => $goodId,
				'quantity' => $quantity,
				'price' => $incomingModel->landing_cost,
			]];
		}
		
		if (!empty($configOffer->price)) {
			if (is_callable($configOffer->price))
				$price = call_user_func_array($configOffer->price, $args);
			else
				$price = $configOffer->price;
			
			$this->requestData['price'] = $price;
		}
		
		if (!empty($configOffer->total)) {
			if (is_callable($configOffer->total))
				$total = call_user_func_array($configOffer->total, $args);
			else
				$total = $configOffer->total;
			
			$this->requestData['total'] = $total;
		}
		
		if (!empty($configOffer->offer_name)) {
			if (is_callable($configOffer->offer_name))
				$offer_name = call_user_func_array($configOffer->offer_name, $args);
			else
				$offer_name = $configOffer->offer_name;
			
			$this->requestData['utm_campaign'] = $offer_name;
		}
		
		if (!empty($configOffer->region)) {
			if (is_callable($configOffer->region))
				$region = call_user_func_array($configOffer->region, $args);
			else
				$region = $configOffer->region;
			
			$this->requestData['region'] = $region;
		}
		
		if (!empty($configOffer->city)) {
			if (is_callable($configOffer->city))
				$city = call_user_func_array($configOffer->city, $args);
			else
				$city = $configOffer->city;
			
			$this->requestData['city'] = $city;
		}

		if (!empty($configOffer->email)) {
			if (is_callable($configOffer->email))
				$email = call_user_func_array($configOffer->email, $args);
			else
				$email = $configOffer->email;

			$this->requestData['email'] = $email;
		}

		if (!empty($configOffer->email)) {
			if (is_callable($configOffer->email))
				$email = call_user_func_array($configOffer->email, $args);
			else
				$email = $configOffer->email;

			$this->requestData['email'] = $email;
		}
		
		if (!empty($incomingModel->address)) {
			$this->requestData['address'] = $incomingModel->address;
		}
		
		if (!empty($incomingModel->city)) {
			$this->requestData['city'] = $incomingModel->city;
		}

		$this->requestUrl = $config->getUrlOrderAdd().'?'.http_build_query([
			'webmasterID' => $configOffer->webmasterID ?? $configUser->webmasterID,
			'token' => $configOffer->token,
		]);
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if (!empty($this->response->error))
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode} Response: {$responseContent}");
		
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (!isset($json->OK))
			throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
		
		return ['partner_order_id' => (string) $json->OK];
	}
}