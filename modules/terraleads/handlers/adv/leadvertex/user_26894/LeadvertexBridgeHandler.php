<?php
namespace app\modules\terraleads\handlers\adv\leadvertex\user_26894;

use yii\web\HttpException;

class LeadvertexBridgeHandler extends \app\modules\terraleads\handlers\adv\leadvertex\LeadvertexBridgeHandler
{
    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
        if (!$responseContent || !is_numeric($responseContent))
            throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");

        return ['partner_order_id' => (string) $responseContent];
    }
}