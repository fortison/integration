<?php

namespace app\modules\terraleads\handlers\adv\offersifynew;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class OffersifynewBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'offer' 			=> $configOffer->offer,
			'lp' 				=> $configOffer->lp,
			'name' 				=> $incomingModel->name,
			'given-name' 		=> $incomingModel->name,
			'family-name' 		=> $incomingModel->name,
			'tel' 				=> $incomingModel->phone,
			'street-address' 	=> $incomingModel->address ?? 'empty',
			'address-level2' 	=> $incomingModel->address ?? 'empty',
			'address-level1' 	=> $incomingModel->address ?? 'empty',
			'email' 			=> $incomingModel->email ?? 'empty@mail.com',
			'postal-code' 		=> 1111,
			'clickid' 			=> $incomingModel->id,
			'subid' 			=> $incomingModel->web_id,
			'ip' 				=> $incomingModel->ip,
			'ua' 				=> $incomingModel->user_agent,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		
		$prepareRequest->setRequestHeaders([
			'x-user-id' => $configUser->x_user_id,
			'x-api-key' => $configUser->x_api_key,
		]);
		
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if ($json->message = 'OK' && isset($json->leadId)) {
			return [
				'partner_order_id' => (string) $json->leadId,
			];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}