<?php
namespace app\modules\terraleads\handlers\adv\offersifynew;

use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use Yii;

class OffersifynewStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$terraleadsStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($terraleadsStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToTerraleads($terraleadsStatusDataModel, $needSendStatus);
		
		return $terraleadsStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$configUser = (object)$this->config->user->array;
		
		$requestUrl = $this->config->getUrlOrderInfo() . $this->orderModel->partner_order_id;
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestHeaders([
			'x-user-id' => $configUser->x_user_id,
			'x-api-key' => $configUser->x_api_key,
		]);
		
		return $this->statusRequest($prepareRequest);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     	= $this->config->user->getApiKey();
		$orderModel 	= $this->orderModel;
		$orderId		= $orderModel->id;
		$terraleadsId	= $orderModel->system_order_id;
		$sourceId		= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$terraleadsId}");
		
		$content	= $responseModel->content;
		
		$contentObject = $this->decode($content);
		
		if (!isset($contentObject->{$this->orderModel->partner_order_id}->leadStatus))
			throw new OrderErrorException($orderId, "Status not found for lead {$sourceId}:{$terraleadsId}");
		
		$status = $contentObject->{$this->orderModel->partner_order_id}->leadStatus;
		
		$comment			=  '';
		$partnerStatus 		= $status;
		$terraleadsStatus	= $this->getTerraleadsStatusName($partnerStatus);
		$terraleadsStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$terraleadsStatusDataModel->setId($terraleadsId);
		$terraleadsStatusDataModel->setComment($comment);
		$terraleadsStatusDataModel->setStatus($terraleadsStatus);
		$terraleadsStatusDataModel->setPartnerStatus($partnerStatus);
		$terraleadsStatusDataModel->setPartnerId($sourceId);
		$terraleadsStatusDataModel->setUserId($orderModel->system_user_id);
		
		return $terraleadsStatusDataModel;
	}
}