<?php
namespace app\modules\terraleads\handlers\adv\herberi;


use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PrepareGetRequest;

class HerberiStatusHandler extends BaseStatusHandler
{
    /**
     * @inheritdoc
     */
    public function updateStatusInSystem($force=false)
    {
        $responseModel = $this->makeStatusRequest();

        $terraleadsStatusDataModel = $this->getSystemStatusData($responseModel);

        $needSendStatus = $this->orderModel->updateOrder($terraleadsStatusDataModel);

        if ($force)
            $needSendStatus = true;

        $this->sendOrderStatusToTerraleads($terraleadsStatusDataModel, $needSendStatus);

        return $terraleadsStatusDataModel;
    }

    /**
     * @inheritdoc
     */
    public function makeStatusRequest()
    {
        $config			= $this->config;
        $configUser		= (object) $config->user->array;

        $orderModel = $this->orderModel;
//        var_dump($orderModel);die();
        $requestUrl = $config->getUrlOrderInfo().($orderModel->partner_order_id
            );
//        var_dump($requestUrl);die();
        $prepareRequest = new PrepareGetRequest();
        $prepareRequest->setRequestUrl($requestUrl);
        $prepareRequest->setRequestHeaders(['Accept'=>'application/json','X-AUTH-TOKEN'=>$configUser->token]);
        $data = $this->statusRequest($prepareRequest);
//        var_dump($prepareRequest);die();
        return $data;
    }

    /**
     * @inheritdoc
     */
    public function getSystemStatusData(ResponseModel $responseModel)
    {
        $apiKey         = $this->config->user->getApiKey();
        $orderModel     = $this->orderModel;
        $orderId	    = $orderModel->id;
        $terraleadsId	= $orderModel->system_order_id;
        $partnerId	    = $orderModel->partner_order_id;
//        var_dump($partnerId);die();
        if (!empty($responseModel->error))
            throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$terraleadsId}");
//        var_dump($responseModel);die();
        if ($responseModel->code != 200)
            throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");

        $content		= $responseModel->content;
        $contentObject	= $this->decode($content);

        $order			= $contentObject;
//        var_dump($order->id);die();

        if (!$order) {
            throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
        }

        if (!isset($order->status)) {
            throw new OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$terraleadsId}");
        }

        $partnerStatus 		= (string) $order->status;
        $terraleadsStatus		= $this->getTerraleadsStatusName($partnerStatus);
        $comment			= $order->data->comment ?? '';
//        var_dump($partnerStatus);die();
        $terraleadsStatusDataModel = new TerraleadsStatusDataModel($apiKey);
        $terraleadsStatusDataModel->setId($terraleadsId);
        $terraleadsStatusDataModel->setUserId($orderModel->system_user_id);
        $terraleadsStatusDataModel->setComment($comment);
        $terraleadsStatusDataModel->setStatus($terraleadsStatus);
        $terraleadsStatusDataModel->setPartnerStatus($partnerStatus);
        $terraleadsStatusDataModel->setPartnerId($partnerId);
//        var_dump($terraleadsStatusDataModel);die();
        return $terraleadsStatusDataModel;
    }
}