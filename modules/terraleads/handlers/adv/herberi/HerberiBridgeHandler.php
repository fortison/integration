<?php

namespace app\modules\terraleads\handlers\adv\herberi;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class HerberiBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $incomingModel	= $this->incomingModel;

        $this->requestUrl	=  $config->getUrlOrderAdd();

        $this->requestData = [
            'eop'                 		=> $configOffer->eop,
            'name'                		=> $incomingModel->name,
            'phone'               		=> $incomingModel->phone,
            'ip_address'          		=> $incomingModel->ip,
            'user_agent'          		=> 'TerraLeads',
            'external_id'         		=> $incomingModel->id,
            'publisher_extra_field2'    => $incomingModel->web_id,
        ];

        $prepareRequest = new PreparePostRequestJson();
        $prepareRequest->setRequestHeaders(['Accept:'.'application/json','X-AUTH-TOKEN:'. $configUser->token]);
        $prepareRequest->setRequestUrl($this->requestUrl);
        $prepareRequest->setRequestData($this->requestData);

        $this->response	= $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();
//        print_r($responseCode);die();
        if ($responseCode != 201)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }
    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($responseContent);
//        print_r($json);die();
        $status = $json->status;
        $leadid = $json->id ?? '';

        if ($status == 'pending')
            return ['partner_order_id' => (string)$leadid];


        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}
