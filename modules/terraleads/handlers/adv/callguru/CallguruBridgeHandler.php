<?php

namespace app\modules\terraleads\handlers\adv\callguru;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class CallguruBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'partner'			=> $configUser->partner,
			'phone'				=> $incomingModel->phone,
			'name'				=> $incomingModel->name,
			'price'				=> $incomingModel->landing_cost,
			'webmaster'			=> $incomingModel->web_id,
			'offer'				=> $configOffer->offer,
			'url'				=> 'https://twitter.com',
			'partnerLeadId'		=> $incomingModel->id,
			'geo'				=> $incomingModel->country,
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		$prepareRequest->setRequestHeaders([
			'Content-Type' 	=> 'application/json',
		]);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 201)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$lead_id = $json->leadId ?? null;
		
		if ($lead_id) {
			return [
				'partner_order_id' => (string) $lead_id,
			];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}