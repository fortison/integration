<?php
namespace app\modules\terraleads\handlers\adv\atlassian;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class AtlassianBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $incomingModel	= $this->incomingModel;

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $this->requestUrl	= $config->getUrlOrderAdd();
        $this->requestData = [

            "offer_id"		=> $configOffer->offer_id,
            "arbitrator_id"	=> $incomingModel->web_id,
            "lead_id"	    => $incomingModel->id,
            "name"		    => $incomingModel->name,
            "phone"			=> $incomingModel->phone,
//            "utm_source"	=> $configUser->utm_source,
        ];

        $prepareRequest = new PreparePostRequestJson();
        $prepareRequest->setRequestUrl($this->requestUrl);
        $prepareRequest->setRequestData($this->requestData);
        $prepareRequest->setRequestHeaders(
            [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $configUser->api_key
            ]
        );
        $this->response	= $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();
//        print_r($responseCode);die();
        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseCode)
    {
        $json = $this->decode($responseCode);

        $success = $json->status ?? null;

        $id		= $json->lead->id ?? null;
//        print_r($id);die();
        if ($success === 'ok')
            return ['partner_order_id' => (string) $id];

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseCode}}");
    }
}
