<?php

namespace app\modules\terraleads\handlers\adv\riyad;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PrepareGetRequestJson;
use yii\web\HttpException;

class RiyadBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $incomingModel	= $this->incomingModel;

        $this->logTerraleadsRequest($incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
                "name"			=> $incomingModel->name,
                "phone"			=> $incomingModel->phone ,
                "offerid"	    => $configOffer->offer_id,
                "net"	        => 1900,
                "fid"	        => $incomingModel->web_id,
                "cid"	        => $incomingModel->id,
                "api_key"	    => $configUser->api_key,
            ]);

        $prepareRequest = new PrepareGetRequestJson();
        $prepareRequest->setRequestUrl($this->requestUrl);
        $this->response	= $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();
//        print_r($responseCode);die();
        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);
//        print_r($responseContent);die();
        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($responseContent);

        if (!isset($json->order))
            throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");

        return ['partner_order_id' => (string) $json->order];
    }
}
