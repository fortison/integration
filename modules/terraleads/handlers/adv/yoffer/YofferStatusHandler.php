<?php

namespace app\modules\terraleads\handlers\adv\yoffer;

use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PrepareGetRequest;

class YofferStatusHandler extends BaseStatusHandler
{
    public function updateStatusInSystem($force=false)
    {
        $responseModel = $this->makeStatusRequest();

        $terraleadsStatusDataModel = $this->getSystemStatusData($responseModel);

        $needSendStatus = $this->orderModel->updateOrder($terraleadsStatusDataModel);

        if ($force)
            $needSendStatus = true;

        $this->sendOrderStatusToTerraleads($terraleadsStatusDataModel, $needSendStatus);

        return $terraleadsStatusDataModel;
    }

    public function makeStatusRequest()
    {

        $requestUrl = $this->config->getUrlOrderInfo() . "?" . http_build_query([
                'id'		=> $this->config->user->array['api_id'] . '-' . $this->config->user->array['api_key'],
                'ids'		=> $this->orderModel->system_order_id,
            ]);

        $prepareRequest = new PrepareGetRequest();
        $prepareRequest->setRequestUrl($requestUrl);

        $responseModel = $this->statusRequest($prepareRequest);

        return $responseModel;
    }

    /**
     * @inheritdoc
     */
    public function getSystemStatusData(ResponseModel $responseModel)
    {
        $apiKey     = $this->config->user->getApiKey();
        $orderModel = $this->orderModel;
        $orderId	= $orderModel->id;
        $apileadId	= $orderModel->system_order_id;
        $partnerId	= $orderModel->partner_order_id;

        if (!empty($responseModel->error))
            throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");

        if ($responseModel->code != 200)
            throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");

        $content		= $responseModel->content;

//		print_r($content); die();

        $contentObject	= $this->decode($content);

        if (!isset($contentObject->$apileadId))
            throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");

        $order = $contentObject->$apileadId;

        if (!isset($order->stage))
            throw new OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$apileadId}");

        $partnerStatus 		= (string) $order->stage;
        $apileadStatus		= $this->getTerraleadsStatusName($partnerStatus);
        $comment			= '';

        $apileadStatusDataModel = new TerraleadsStatusDataModel($apiKey);
        $apileadStatusDataModel->setId($apileadId);
        $apileadStatusDataModel->setUserId($orderModel->system_user_id);
        $apileadStatusDataModel->setComment($comment);
        $apileadStatusDataModel->setStatus($apileadStatus);
        $apileadStatusDataModel->setPartnerStatus($partnerStatus);
        $apileadStatusDataModel->setPartnerId($partnerId);

        return $apileadStatusDataModel;
    }
}
