<?php

namespace app\modules\terraleads\handlers\adv\yoffer;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class YofferBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $incomingModel	= $this->incomingModel;

        $this->requestData	= [
            'id'      		=> $incomingModel->id,
            'wm' 			=> $incomingModel->getWHash(),
            'offer'			=> $configOffer->offer,
            'ip'			=> $incomingModel->ip,
            'name'			=> $incomingModel->name,
            'phone'			=> $incomingModel->phone,
            'country'		=> $incomingModel->country,
            'utm_campaign'	=> $configOffer->offer_name,
        ];

        $this->requestUrl = $config->getUrlOrderAdd() . '?' . 'id=' . $configUser->api_id . '-' . $configUser->api_key;

        $prepareRequest = new PreparePostRequestJson();
        $prepareRequest->setRequestUrl($this->requestUrl);
        $prepareRequest->setRequestData($this->requestData);

        $this->response	= $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();

        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($responseContent);

        $status = $json->status ?? '';

        if ($status == 'ok')
            return ['partner_order_id' => (string) $json->id];

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}