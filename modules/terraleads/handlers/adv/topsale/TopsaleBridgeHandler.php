<?php
namespace app\modules\terraleads\handlers\adv\topsale;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class TopsaleBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $incomingModel	= $this->incomingModel;

        $this->requestUrl	= $config->getUrlOrderAdd()."?".http_build_query(['pid' => $configUser->pid]);

        $this->requestData = [
            'name' 			=> $incomingModel->name,
            'product' 		=> $incomingModel->web_id,
            'phone' 		=> $incomingModel->phone,
            'product_id' 	=> $configOffer->product_id,
            'sum' 	        => $incomingModel->landing_cost,
        ];

        $prepareRequest = new PreparePostRequest();
        $prepareRequest->setRequestUrl($this->requestUrl);
        $prepareRequest->setRequestData($this->requestData);

        $this->response	= $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();


        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($responseContent);
        $status = $json->status ?? '';

        if ($status == 'success') {
            return ['partner_order_id' => (string) $json->orderid];
        }

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}