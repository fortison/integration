<?php
namespace app\modules\terraleads\handlers\adv\salestoro;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class SalestoroBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$args = [$incomingModel, $config];
		
		if (isset($configOffer->payoutId)) {
			if (is_callable($configOffer->payoutId))
				$payoutId = call_user_func_array($configOffer->payoutId, $args);
			else
				$payoutId = $configOffer->payoutId;
		}

		$data = [
			'request_time' => time(),
			'offer_id' => $configOffer->offerId,
			'app_id' => $configUser->appId,
			'payout_id' => $payoutId,
			'ext_aff_id' => $incomingModel->web_id,
			'track' => $incomingModel->id,
			'name' => $incomingModel->name,
			'mobile' => $incomingModel->phone,
			'address' => $incomingModel->address,
			'email' => null,
			'ip' => $incomingModel->ip,
			'country' => $configOffer->country,
			'currency' => $configOffer->currency,
		];
		$data['access_token'] = $this->hash($configUser->appId, $configUser->appSecret, $data);

		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'request' => $this->encode($data)
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status= $json->status ?? null;
		$orderId= $json->order_id ?? null;
		
		if ($status === 'OK' && $orderId)
			return [
				'partner_order_id' => (string) $orderId];
		
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
	
	public function hash($appId, $appSecret, $data){
		$str = $appId . "|";
		foreach($data as $k=>$v) {
			if(!empty($k) && !empty($v) && !is_array($v) && !is_array($k)){
				$str .= "{$k}:{$v}|";
			}
		}
		$str .= $appSecret;
		
		
		return sha1($str);
	}
}