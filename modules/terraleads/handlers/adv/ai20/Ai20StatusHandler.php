<?php
namespace app\modules\terraleads\handlers\adv\ai20;

use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PrepareGetRequest;

class Ai20StatusHandler extends BaseStatusHandler
{
	/**
	 * @inheritdoc
	 */
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$terraleadsStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($terraleadsStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToTerraleads($terraleadsStatusDataModel, $needSendStatus);
		
		return $terraleadsStatusDataModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function makeStatusRequest()
	{
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->config->getUrlOrderInfo()
			.'?api_key='.$this->config->user->array['api_key']
			.'&id='.$this->orderModel->partner_order_id);
		$prepareRequest->setRequestHeaders([
			'Content-Type' 	=> 'application/x-www-form-urlencoded',
			'User-Agent' 	=> 'Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0',
		]);
		
		return $this->statusRequest($prepareRequest);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     	= $this->config->user->getApiKey();
		$orderModel 	= $this->orderModel;
		$orderId		= $orderModel->id;
		$terraleadsId	= $orderModel->system_order_id;
		$sourceId		= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$terraleadsId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content		= $responseModel->content;
		$contentObject	= $this->decode($content);

		if (empty($contentObject)){
			throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
		}
		
		if (empty($contentObject->status)) {
			throw new OrderErrorException($orderId, "Wrong partner response: {$responseModel->content} {$sourceId}:{$terraleadsId}");
		}
		
		$comment			= $contentObject->data ?? '';
		$partnerStatus 		= (string) $contentObject->status;
		$terraleadsStatus	= $this->getTerraleadsStatusName($partnerStatus);
		
		$terraleadsStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$terraleadsStatusDataModel->setId($terraleadsId);
		$terraleadsStatusDataModel->setUserId($orderModel->system_user_id);
		$terraleadsStatusDataModel->setComment($comment);
		$terraleadsStatusDataModel->setStatus($terraleadsStatus);
		$terraleadsStatusDataModel->setPartnerStatus($partnerStatus);
		$terraleadsStatusDataModel->setPartnerId($sourceId);
		
		return $terraleadsStatusDataModel;
	}
}