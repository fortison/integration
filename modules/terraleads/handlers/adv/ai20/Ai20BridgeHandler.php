<?php
namespace app\modules\terraleads\handlers\adv\ai20;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class Ai20BridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd() . '?' . http_build_query([
				'api_key'	=> $configUser->api_key,
			]);
		
		$this->requestData = [
			'off' 	  		=> $configOffer->off,
			'dto' 	  		=> 1,
			'net' 	   		=> $configUser->net,
			'aff'  	   		=> $incomingModel->id,
			'cid'  	   		=> $incomingModel->id,
			'firstname'  	=> $incomingModel->name,
			'lastname'  	=> $incomingModel->name,
			'email'  		=> $incomingModel->email ?? 'empty@mail.mail',
			'phone'  		=> $incomingModel->phone,
			'address'  		=> $incomingModel->address,
			'city'  		=> $incomingModel->city,
			'zip'  			=> $incomingModel->zip,
			'note'  		=> $incomingModel->user_comment,
			'loc'  			=> $incomingModel->country,
			'source'  		=> $incomingModel->user_agent,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		$prepareRequest->setRequestHeaders([
			'Content-Type' 	=> 'application/x-www-form-urlencoded',
			'User-Agent' 	=> $incomingModel->user_agent,
		]);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		$id = $json->lead_Id;
		
		if (!empty($id)) {
			return ['partner_order_id' => (string) $id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}