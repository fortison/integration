<?php
namespace app\modules\terraleads\handlers\adv\toppik;

use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use Yii;

class ToppikStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$this->logStatusRequest(new ResponseModel, Yii::$app->request->url, Yii::$app->request->post());
		
		$apiKey     		= $this->config->user->getApiKey();
		$orderModel			= $this->orderModel;
		$orderId			= $orderModel->id;
		$apileadId			= $orderModel->system_order_id;
		$partnerId			= $orderModel->partner_order_id;
		$userId				= $orderModel->system_user_id;
		$partnerStatus 		= Yii::$app->request->get('partnerStatus');
		$comment 			= Yii::$app->request->get('comment');
		
		if (!isset($partnerStatus))
			throw new OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$apileadId}");
		
		$apileadStatus = $this->getTerraleadsStatusName($partnerStatus);
		
		$apileadStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($partnerId);
		$apileadStatusDataModel->setUserId($userId);
		
		try {
			$this->sendOrderStatusToTerraleads($apileadStatusDataModel, true);
			$orderModel->system_status = $apileadStatus;
			$orderModel->partner_status = $partnerStatus;
			$orderModel->save();
		} catch (\Exception $e) {
			throw new OrderErrorException($orderId, "Error: {$e->getMessage()}");
		}
	}
	
	public function makeStatusRequest(){}
	public function getSystemStatusData(ResponseModel $responseModel){}
}