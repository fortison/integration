<?php
namespace app\modules\terraleads\handlers\adv\mancare;

use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\models\forms\ResponseModel;
use Yii;

class MancareStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel	= new ResponseModel;
		$requestUrl		= Yii::$app->request->url;
		$requestData	= Yii::$app->request->post();
		$this->logStatusRequest($responseModel, $requestUrl, $requestData);
		
		$apiKey     		= $this->config->user->getApiKey();
		$orderModel			= $this->orderModel;
		$orderId			= $orderModel->id;
		$terraleadsId		= $orderModel->system_order_id;
		$partnerId			= $orderModel->partner_order_id;
		$partnerStatus 		= Yii::$app->request->get('partnerStatus');
		$comment 			= Yii::$app->request->get('comment');
		
		if (!isset($partnerStatus))
			throw new OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$terraleadsId}");
		
		$systemStatus		= $this->getTerraleadsStatusName($partnerStatus);
		
		$terraleadsStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$terraleadsStatusDataModel->setId($terraleadsId);
		$terraleadsStatusDataModel->setUserId($orderModel->system_user_id);
		$terraleadsStatusDataModel->setComment($comment);
		$terraleadsStatusDataModel->setStatus($systemStatus);
		$terraleadsStatusDataModel->setPartnerStatus($partnerStatus);
		$terraleadsStatusDataModel->setPartnerId($partnerId);
        //var_dump($terraleadsStatusDataModel);
		try {
			$this->sendOrderStatusToTerraleads($terraleadsStatusDataModel, true);
			$orderModel->system_status = $systemStatus;
			$orderModel->partner_status = $partnerStatus;
			$orderModel->comment = $comment;
			$orderModel->save();
		} catch (\Exception $e) {
			throw new OrderErrorException($orderId, "Error: {$e->getMessage()}");
		}finally {
            echo '{"status": "OK"}';
        }


        return $terraleadsStatusDataModel;
	}
	
	public function makeStatusRequest(){}
	public function getSystemStatusData(ResponseModel $responseModel){}
}