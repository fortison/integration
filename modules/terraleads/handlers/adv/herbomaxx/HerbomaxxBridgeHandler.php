<?php
namespace app\modules\terraleads\handlers\adv\herbomaxx;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use yii\web\HttpException;

class HerbomaxxBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd() . '?' . http_build_query(
			[
				'name' 			        => $incomingModel->name,
				'mobile' 				=> $incomingModel->phone,
				'address' 				=> $incomingModel->address ?? '',
				'city' 				    => $incomingModel->city ?? '',
				'pincode' 			    => $incomingModel->zip ?? '',
				'email' 				=> $incomingModel->email ?? '',
				'channel_id'			=> $configUser->channel_id,
				'token'    		        => $configUser->token,
				'product_id'    		=> $configOffer->product_id,
				'affiliate_id'		    => $incomingModel->web_id,
				'clickid' 				=> $incomingModel->id,
			]
		);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (isset($json->status) && $json->status == 'success') {
			return ['partner_order_id' => (string) $json->id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}