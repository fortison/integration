<?php

namespace app\modules\terraleads\handlers\adv\cpastore;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class CpastoreBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'partnerId' 	=> $configUser->partnerId,
			'apiKey' 		=> $configUser->apiKey,
			'order' =>[
				'order_id' 		=> $incomingModel->id,
				'country' 		=> $incomingModel->country,
				'name' 			=> $incomingModel->name,
				'phone' 		=> $incomingModel->phone,
				'web_id' 		=> $incomingModel->web_id,
				'items' => [
					'item' => [
						
						'item_id' => $configOffer->item_id,
						'price' => $incomingModel->landing_cost,
					
					],
				],
			],
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
//		$prepareRequest->setRequestHeaders(['Content-Type' => 'application/x-www-form-urlencoded', 'Authorization' => "Bearer {$configUser->apiKey}",]);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$orderId = $json[0]->external_id;
		
		if ($orderId) {
			return [
				'partner_order_id' => (string) $orderId,
			];
		}
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}