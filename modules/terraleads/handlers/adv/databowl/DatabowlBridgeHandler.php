<?php
namespace app\modules\terraleads\handlers\adv\databowl;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class DatabowlBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		
		$this->requestData = [
			'cid' 				=> $configUser->cid,
			'sid' 				=> $configUser->sid ?? '',
			'uid' 				=> $configUser->uid ?? '',
			'f_67_name' 				=> $incomingModel->name,
			'f_1_email'					=> $incomingModel->email,
			'f_101_lead_ip' 			=> $incomingModel->ip,
			'f_100_sub_affiliate_id' 	=> $incomingModel->getWHash(),
			'f_133_order_id' 			=> $incomingModel->id,
			'f_6_address1' 				=> $incomingModel->address ?? '',
			'f_10_county' 				=> $incomingModel->country ?? '',
			'f_12_phone1' 				=> $incomingModel->phone ?? '',
			'f_39_zipcode'				=> $incomingModel->zip ?? '',
			'f_40_city'    				=> $incomingModel->city,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
//		$prepareRequest->setRequestHeaders(["x-auth-token" => $configUser->api_key]);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (!empty($json->lead_id)) {
			return ['partner_order_id' => (string) $json->lead_id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}