<?php
namespace app\modules\terraleads\handlers\adv\steppay;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\helpers\Json;
use yii\web\HttpException;

class SteppayBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;

		$this->requestData	= [
			'user_id' => $configUser->terraleadsUserId,
			'data' => [
				'name' => $incomingModel->name,
				'phone' => $incomingModel->phone,
				'offer_id' => $configOffer->offerId,
			],
		];
		
		$requestContent = Json::encode($this->requestData);
		
		$this->requestUrl	= $config->getUrlOrderAdd() . '?' . http_build_query([
			'check_sum' => sha1($requestContent . $configUser->terraleadsApiKey),
		]);
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		$prepareRequest->setRequestContent($requestContent);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status = $json->status ?? null;
		$orderId = $json->data->id ?? null;
		
		if ($status === 'ok' && $orderId) {
			return [
				'partner_order_id' => (string) $orderId,
			];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}