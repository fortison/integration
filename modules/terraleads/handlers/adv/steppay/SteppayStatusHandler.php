<?php
namespace app\modules\terraleads\handlers\adv\steppay;

use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PreparePostRequest;
use Yii;
use yii\helpers\Json;

class SteppayStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$terraleadStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($terraleadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToTerraleads($terraleadStatusDataModel, $needSendStatus);
		
		return $terraleadStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		
		$orderModel = $this->orderModel;
		
		$requestData	= [
			'user_id' => $configUser->terraleadsUserId,
			'data' => [
				'id' => $orderModel->partner_order_id,
			],
		];
		
		$requestContent = Json::encode($requestData);
		
		$requestUrl	= $config->getUrlOrderInfo() . '?' . http_build_query([
			'check_sum' => sha1($requestContent . $configUser->terraleadsApiKey),
		]);
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestData($requestData);
		$prepareRequest->setRequestContent($requestContent);
		
		return $this->statusRequest($prepareRequest);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$partnerId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$order	= $this->decode($responseModel->content);
		
		if ($order->status !== 'ok') {
			throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
		}
		
		$partnerStatus 		= $order->data->status;
		$apileadStatus		= $this->getTerraleadsStatusName($partnerStatus);
		$comment			= $order->data->comment;
		
		$apileadStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setUserId($orderModel->system_user_id);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($partnerId);
		
		return $apileadStatusDataModel;
	}
}