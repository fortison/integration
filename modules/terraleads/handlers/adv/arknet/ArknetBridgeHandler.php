<?php

namespace app\modules\terraleads\handlers\adv\arknet;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class ArknetBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd() . '?id=' . $configUser->api_token;
		$this->requestData	= [
			'id' 			=> $incomingModel->id,
			'wm' 			=> $incomingModel->web_id,
			'offer' 		=> $configOffer->offer,
			'ip' 			=> $incomingModel->ip,
			'name' 			=> $incomingModel->name,
			'phone' 		=> $incomingModel->phone,
			'ua' 			=> $incomingModel->user_agent,
			'country' 		=> $incomingModel->country,
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if ($json->status = 'ok' && isset($json->id)) {
			return [
				'partner_order_id' => (string) $json->id,
			];
		}
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}