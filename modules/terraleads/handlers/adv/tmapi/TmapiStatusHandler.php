<?php
namespace app\modules\terraleads\handlers\adv\tmapi;

use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use yii\web\HttpException;

class TmapiStatusHandler extends BaseStatusHandler
{
    public function updateStatusInSystem($force=false)
    {
        $responseModel = $this->makeStatusRequest();

        $terraleadStatusDataModel = $this->getSystemStatusData($responseModel);

        $needSendStatus = $this->orderModel->updateOrder($terraleadStatusDataModel);

        if ($force)
            $needSendStatus = true;

        $this->sendOrderStatusToTerraleads($terraleadStatusDataModel, $needSendStatus);

        return $terraleadStatusDataModel;
    }

    public function makeStatusRequest()
    {
        $config			= $this->config;
        $configUser		= (object) $config->user->array;

        $orderModel = $this->orderModel;


        $requestUrl	= $config->getUrlOrderInfo() . '?' . http_build_query([
                'leadId' => $orderModel->partner_order_id,
            ]);

//        $prepareRequest = new PrepareGetRequest();
//        $prepareRequest->setRequestUrl($requestUrl);
//        $prepareRequest->setRequestHeaders(['api' => $configUser->api]);

        $result = file_get_contents($requestUrl, false, stream_context_create(array(
            'http' => array(
                'method'  => 'GET',
                'header'  => "api: $configUser->api\r\n" . "Content-type: application/x-www-form-urlencoded\r\n",
            )
        )));

        if (empty($result))
            throw new OrderErrorException($orderModel->id, "Partner error!. Response: null");

        if (!self::isJSON($result))
            throw new OrderErrorException('400', "Partner error! Response: {$result}");

        $responseModel = new ResponseModel();
        $responseModel->setContent($result);
        $responseModel->setTime(1);
        $responseModel->setCode(200);

        return $responseModel;
    }

    /**
     * @inheritdoc
     */
    public function getSystemStatusData(ResponseModel $responseModel)
    {
        $apiKey     = $this->config->user->getApiKey();
        $orderModel = $this->orderModel;
        $orderId	= $orderModel->id;
        $apileadId	= $orderModel->system_order_id;
        $partnerId	= $orderModel->partner_order_id;

        if (!empty($responseModel->error))
            throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");

        if ($responseModel->code != 200)
            throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");

        $order	= $this->decode($responseModel->content);

        if ($order->status !== 200) {
            throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
        }

        if(empty($order->data[0])) {
            throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
        }

        $lead = $order->data[0];

        $partnerStatus = $lead->status;
        $apileadStatus		= $this->getTerraleadsStatusName($partnerStatus);
        $comment			= $lead->comment ?? '';

        $apileadStatusDataModel = new TerraleadsStatusDataModel($apiKey);
        $apileadStatusDataModel->setId($apileadId);
        $apileadStatusDataModel->setUserId($orderModel->system_user_id);
        $apileadStatusDataModel->setComment($comment);
        $apileadStatusDataModel->setStatus($apileadStatus);
        $apileadStatusDataModel->setPartnerStatus($partnerStatus);
        $apileadStatusDataModel->setPartnerId($partnerId);

        return $apileadStatusDataModel;
    }

    public static function isJSON($string) {
        return ((is_string($string) && (is_object(json_decode($string)) || is_array(json_decode($string))))) ? true : false;
    }
}