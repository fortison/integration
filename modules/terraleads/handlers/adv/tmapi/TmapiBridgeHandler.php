<?php
namespace app\modules\terraleads\handlers\adv\tmapi;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class TmapiBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $incomingModel	= $this->incomingModel;

        $this->requestUrl	= $config->getUrlOrderAdd();

        $this->requestData = [
            'clickId' 			=> $incomingModel->id,
            'campaignId' 		=> $configOffer->campaignId,
            'data' => [
                'name'          => $incomingModel->name,
                'email' 		=> $incomingModel->email,
                'phone' 		=> $incomingModel->phone,
            ]
        ];

        $result = file_get_contents($this->requestUrl, false, stream_context_create(array(
            'http' => array(
                'method'  => 'POST',
                'header'  => "api: $configUser->api\r\n" . "Content-type: application/x-www-form-urlencoded\r\n",
                'content' => http_build_query($this->requestData)
            )
        )));

        if (!self::isJSON($result))
            throw new HttpException('400', "Partner error! Response: {$result}");

        $partnerOrderId		= $this->getOrderAttributes($result);

        $this->response = new ResponseModel();
        $this->response->setCode(200);
        $this->response->setTime(1);
        $this->response->setContent($result ?? null);

        return $this->saveOrder($partnerOrderId);
    }

    public static function isJSON($string) {
        return ((is_string($string) && (is_object(json_decode($string)) || is_array(json_decode($string))))) ? true : false;
    }

    public function getOrderAttributes($result)
    {
        $json = $this->decode($result);

        $status = $json->status ?? '';

        if ($status == 'Accepted' || $status == 'Pending') {
            return ['partner_order_id' => (string) $json->leadId];
        }
//        print_r($status);die();
        throw new HttpException('400', "Wrong partnerOrderId. {{$result}}");
    }
}