<?php
namespace app\modules\terraleads\handlers\adv\ibiz;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\helpers\Json;
use yii\web\HttpException;

class IbizBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config = $this->config;
		$configUser = (object)$config->user->array;
		$configOffer = (object)$config->offer->array;
		
		$incomingModel = $this->incomingModel;
		
		$requestData = [
			'user_id' => $configUser->user_id,
			'data' => [
				'name' => $incomingModel->name,
				'phone' => $incomingModel->phone,
                'product_id' => $configOffer->product_id,
				'country' => $incomingModel->country,
				'email' => $incomingModel->email ?? '',
                'sub_id' => $incomingModel->id,
				'web_id' => $incomingModel->web_id,
			]
		];

		$this->requestData = Json::encode($requestData);
		$checkSum = sha1($configUser->apiKey);
		
		$this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
				'check_sum' => $checkSum,
			]);
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($requestData);
//
		$prepareRequest->setRequestContent($this->requestData);
//
		$this->response	= $this->bridgeRequest($prepareRequest);

		if ($this->response->error) {
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		}

		$responseCode = $this->response->getCode();
		$responseContent = $this->response->getContent();
//        print_r($responseContent);die();
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

		$partnerOrderId = $this->getOrderAttributes($responseContent);

		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{

		$json = $this->decode($responseContent);

		$status = $json->status ?? null;
		$id = $json->data->id ?? null;

		if ($status === 'ok')
			return ['partner_order_id' => (string)$id];

		throw new HttpException('400', "Wrong partnerOrderId: {{$responseContent}}");
	}
}