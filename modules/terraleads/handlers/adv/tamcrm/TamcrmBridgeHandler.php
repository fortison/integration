<?php
namespace app\modules\terraleads\handlers\adv\tamcrm;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class TamcrmBridgeHandler extends BaseBridgeHandler
{
	/**
	 * @inheritdoc
	 */
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		
		$this->requestData	= [
			'campaign_id'  		 => $configOffer->campaign_id,
			'api_key'  			 => $configUser->api_key ?? '',
			'name' 				 => $incomingModel->name,
			'phone' 	 		 => $incomingModel->phone,
			'email' 	 		 => $incomingModel->email ?? '',
			'address' 			 => $incomingModel->address,
			'country' 			 => $incomingModel->country,
			'pub_id'        	 => $incomingModel->web_id,
			'publisher_order_id' => $incomingModel->id,
			'client_ip'          => $incomingModel->ip,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if (!empty($this->response->error))
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode} Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$arr = $this->decode($responseContent);
		$order = current($arr);
		
		$status	= $order->status;
		
		if ($status == 'success') {
			return ['partner_order_id' => (string) $order->order_id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}