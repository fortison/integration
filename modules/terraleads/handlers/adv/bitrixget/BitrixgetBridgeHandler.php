<?php
namespace app\modules\terraleads\handlers\adv\bitrixget;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use yii\web\HttpException;

class BitrixgetBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		$subDomain		= $configUser->subDomain;
		$id				= $configUser->id;
		$apiKey			= $configUser->apiKey;
		$tld 			= $configUser->tld ?? 'com';
		$incomingModel	= $this->incomingModel;
		
		$requestData = [
			'FIELDS[NAME]'					=> $incomingModel->name,
			'FIELDS[LAST_NAME]'   			=> $incomingModel->name,
			'FIELDS[EMAIL][0][VALUE]'   	=> $incomingModel->email,
			'FIELDS[EMAIL][0][VALUE_TYPE]'  => 'WORK',
			'FIELDS[PHONE][0][VALUE]'   	=>  $incomingModel->phone,
			'FIELDS[PHONE][0][VALUE_TYPE]'  => 'WORK',
			'FIELDS[CURRENCY_ID]'           => $incomingModel->landing_currency,
			'FIELDS[OPPORTUNITY]'           => $incomingModel->landing_cost,
			'FIELDS[' . $configUser->clickIDField . ']' => $incomingModel->id,
		];
		
		if (!empty($configOffer->comment)) {
			$requestData['FIELDS[COMMENTS]'] = $configOffer->comment;
		}
		
		$this->requestUrl = "https://{$subDomain}.bitrix24.{$tld}/rest/{$id}/{$apiKey}/crm.lead.add.json" . '?'
			. $this->buildHttpQuery(
				$requestData
			);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$responseLeadObject = $this->decode($responseContent);
		
		if (!isset($responseLeadObject->result))
			throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
		
		return ['partner_order_id' => (string) $responseLeadObject->result];
	}
	
	public function buildHttpQuery($query): string
	{
		$queryArr = [];
		
		foreach ($query as $key => $value) {
			$queryArr[] = $key . '=' . $value;
		}
		
		return implode('&', $queryArr);
	}
}