<?php


namespace app\modules\terraleads\handlers\adv\lovienlatv;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;


class LovienlatvBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData = [
			"apikey"			=> $configUser->api_key,
			"data" =>[
//				"nombre"		=> $incomingModel->name,;
				"name" => $incomingModel->name,
				"telephone" => $incomingModel->phone,
				"utm_source" => 'terraleads',
				"utm_medium" => 'terraleads',
				"utm_campaign" => 'terraleads',
				"offer_id" => $configOffer->offer_id,
				"country_code" => $incomingModel->country,
				"price" => $incomingModel->landing_cost,
				"base_url" => $incomingModel->landing_cost,
				"ip" => $incomingModel->ip,
				"aff_id" => $incomingModel->getWHash() ?? '',
			],
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestHeaders(['Content-Type' => 'application/json']);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
//        print_r($responseCode);die();
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseCode)
	{
		$this->checkResponseInvalidPhone($responseCode, 'Invalid format');
		
		$json = $this->decode($responseCode);
		
		$success = $json->code ?? null;
		$id		= $json->order_id ?? null;
		
		if ($success == 200)
			return ['partner_order_id' => (string) $id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseCode}}");
	}
}
