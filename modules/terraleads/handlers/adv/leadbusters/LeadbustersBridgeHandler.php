<?php

namespace app\modules\terraleads\handlers\adv\leadbusters;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class LeadbustersBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $config			= $this->config;
        $configOffer	= (object) $config->offer->array;

        $incomingModel	= $this->incomingModel;

        $this->requestUrl	= $config->getUrlOrderAdd();

        $this->requestData = [
            'subid5'		=> $incomingModel->web_id,
            'subid4' 	    => $incomingModel->id,
            'name' 		    => $incomingModel->name,
            'phone' 		=> $incomingModel->phone,
            'x_ip' 			=> $incomingModel->ip,
            'x_url' 		=> $configOffer->x_url,
            'x_ua' 		    => $incomingModel->user_agent,
        ];

        $prepareRequest = new PreparePostRequest();
        $prepareRequest->setRequestUrl($this->requestUrl);
        $prepareRequest->setRequestData($this->requestData);

        $this->response	= $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();

        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($this->decode($responseContent));
        $trackId = $json->trackid ?? null;

        if (!empty($trackId)) {
			return ['partner_order_id' => (string)$trackId];
        }

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}