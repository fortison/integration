<?php
namespace app\modules\terraleads\handlers\adv\leadorato;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class LeadoratoBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		
		$this->requestData = [
			'id' 					=> $incomingModel->id,
			'campaign_id' 			=> $incomingModel->campaign_id,
			'name' 					=> $incomingModel->name,
			'country' 				=> $incomingModel->country,
			'phone' 				=> $incomingModel->phone,
			'email' 				=> $incomingModel->email,
			'count' 				=> $incomingModel->count,
			'zip' 					=> $incomingModel->zip,
			'region' 				=> $incomingModel->region,
			'city' 					=> $incomingModel->city,
			'address' 				=> $incomingModel->address,
			'building' 				=> $incomingModel->building,
			'apartment' 			=> $incomingModel->apartment,
			'user_comment' 			=> $incomingModel->user_comment,
			'tz' 					=> $incomingModel->tz,
			'cost' 					=> $incomingModel->cost,
			'cost_delivery' 		=> $incomingModel->cost_delivery,
			'landing_cost' 			=> $incomingModel->landing_cost,
			'user_id' 				=> $incomingModel->user_id,
			'web_id' 				=> $incomingModel->web_id,
			'stream_id' 			=> $incomingModel->stream_id,
			'product_id' 			=> $incomingModel->product_id,
			'offer_id' 				=> $incomingModel->offer_id,
			'ip' 					=> $incomingModel->ip,
			'user_agent' 			=> $incomingModel->user_agent,
			'extra_data' 			=> $incomingModel->extra_data,
			'offer_type' 			=> $incomingModel->offer_type,
			'landing_currency' 		=> $incomingModel->landing_currency,
			'check_sum' 			=> $incomingModel->check_sum,
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestHeaders([
			'x-api-key' 		=> $configUser->api_key,
			'Content-Type' 		=> 'application/json',
			'X-Forwarder-For' 	=> '188.190.239.87',
		]);
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if ($json == 'Ok') {
			return [];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}