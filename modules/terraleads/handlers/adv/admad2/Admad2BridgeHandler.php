<?php
namespace app\modules\terraleads\handlers\adv\admad2;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class Admad2BridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'order' => [
				'phone' 				=> $incomingModel->phone,
				'firstname' 			=> $incomingModel->name,
				'lastname'		 		=> $incomingModel->name,
				'country' 				=> $incomingModel->country,
				'offer_code' 			=> $configOffer->offer_code,
				'external_partner_id' 	=> $incomingModel->id,
				'web_id' 				=> $incomingModel->web_id,
			],
		];
		
		$token = empty($configOffer->token) ? $this->getAffiliate($configUser->tokens, $incomingModel->country) : $configOffer->token;
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		$prepareRequest->setRequestHeaders(
			[
				'Accept'		=>'application/json',
				'Content-Type'	=> 'application/json',
				'Affiliate' 	=> $token,
			]
		);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode < 200 && $responseCode > 210)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);

		if (!empty($json->order) && !empty($json->order->uuid)) {
			return [
				'partner_order_id' => (string) $json->order->uuid,
			];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
	
	private function getAffiliate($tokens, $country)
	{
		if (empty($tokens[$country])) return $tokens['pl'];
		return $tokens[$country];
	}
	
	private function getDataOrDefault($data, $default)
	{
		if (empty($data)) return $default;
		return $data;
	}
}