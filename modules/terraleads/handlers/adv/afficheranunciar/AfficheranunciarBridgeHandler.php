<?php

namespace app\modules\terraleads\handlers\adv\afficheranunciar;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class AfficheranunciarBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData = [
			
			"order_id"		=> $incomingModel->id,
			"phone"			=> $incomingModel->phone,
			"name"			=> $incomingModel->name,
			"address"	    => $incomingModel->address,
			"city"	=> $incomingModel->city,
			"goods_id"		=> $configOffer->goods_id,
			"user_id"		=> $configOffer->user_id,
			"subaffiliateid"		=> $configOffer->user_id,
			"api_key"		=> $configUser->apiKey,
			"qty"		=> $incomingModel->count ?? 1,
			"price"		=> $incomingModel->landing_cost,
		
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestHeaders(['api_key' => $configUser->apiKey]);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();

		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseCode)
	{
		$json = $this->decode($responseCode);
		
		$success = $json->status ;

		$id		= $json->order_id;
		
		if ($success === 'ok')
			return ['partner_order_id' => (string) $id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseCode}}");
	}
}
