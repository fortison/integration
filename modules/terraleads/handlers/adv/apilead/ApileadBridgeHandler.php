<?php
namespace app\modules\terraleads\handlers\adv\apilead;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\helpers\Json;
use yii\web\HttpException;

class ApileadBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$requestData = [
			'user_id' => $configUser->userId,
			'name' => $incomingModel->name,
			'phone' => $incomingModel->phone,
			'offer_id' => $configOffer->offerId,
			'country' => $incomingModel->country,
			'address' => $incomingModel->address,
			'email'	  => $incomingModel->email,
			'zip'	  => $incomingModel->zip,
			'city'	  => $incomingModel->city,
			'check_sum'	=> sha1($configUser->userId . $configOffer->offerId . $incomingModel->name . $incomingModel->phone . $configUser->apiKey),
		];
//		print_r($requestData);die();
//		$this->requestData = Json::encode($requestData);
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($requestData);
//		$prepareRequest->setRequestContent($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error) {
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		}
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);

		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status = $json->status ?? null;
		$id		= $json->id ?? null;
		
		if (in_array($status, ['accept', 'expect', 'confirm', 'reject', 'fail', 'trash', 'error']))
			return ['partner_order_id' => (string) $id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}