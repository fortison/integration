<?php
namespace app\modules\terraleads\handlers\adv\apilead;

use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PreparePostRequest;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use yii\helpers\Json;

class ApileadStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$terraleadsStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($terraleadsStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToTerraleads($terraleadsStatusDataModel, $needSendStatus);
		
		return $terraleadsStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		
		$requestData = [
			'id' => $this->orderModel->partner_order_id,
			'check_sum' => sha1($this->orderModel->partner_order_id . $configUser->apiKey),
		];
		
		$requestContent = Json::encode($requestData);
		
		$requestUrl = $config->getUrlOrderInfo();
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestData($requestData);
		$prepareRequest->setRequestContent($requestContent);
		
		$responseModel = $this->statusRequest($prepareRequest);
		
		return $responseModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$partnerId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content	= $responseModel->content;
		
		$order = $this->decode($content);
		
		$partnerStatus			= (string) $order->status;
		$systemStatus			= $this->getTerraleadsStatusName($partnerStatus);
		$comment				= $order->comment ?? '';
		
		$apileadStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setUserId($orderModel->system_user_id);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($systemStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($partnerId);
		
		return $apileadStatusDataModel;
	}
}