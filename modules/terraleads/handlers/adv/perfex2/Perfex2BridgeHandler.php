<?php
namespace app\modules\terraleads\handlers\adv\perfex2;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class Perfex2BridgeHandler extends BaseBridgeHandler
{
	/**
	 * @inheritdoc
	 */
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		
		$this->requestData	= [
			'source'  		=> $configOffer->source,
			'status'  		=> $configOffer->status ?? '',
			'name' 			=> $incomingModel->name,
//			'tags' 	 		=> 'terraleads',
			'email' 	 	=> $incomingModel->id . '@gmail.com', //у них нету поля для передачи ид, поэтому передаем сюда
			'phonenumber'  	=> $incomingModel->phone,
			'address' 		=> $incomingModel->address ?? '',
			'zip'			=> $incomingModel->zip ?? '',
			'lead_value'	=> $incomingModel->landing_cost ?? '',
//			'country' 		=> $incomingModel->country,
			'contacted_today' => '0',
		];
		
		if (!empty($configOffer->assigned)) {
			$this->requestData['assigned'] = $configOffer->assigned;
		}
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestHeaders(["authtoken" => $configUser->authtoken]);
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if (!empty($this->response->error))
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode} Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status	= $json->status;
		
		if ($status == 'true' || $status == 1) {
			return [];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}