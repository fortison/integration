<?php
namespace app\modules\terraleads\handlers\adv\snapcall;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class SnapcallBridgeHandler extends BaseBridgeHandler
{
	/**
	 * @inheritdoc
	 */
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		
		$this->requestData	= [
			'account_id'  		 	=> $configUser->account_id,
			'key'  				 	=> $configUser->key,
			'list_id' 			 	=> $configOffer->list_id,
			'billing_first_name' 	=> 'terraleads',
			'billing_address_1' 	=> $incomingModel->address ?? '',
			'billing_zip_postcode' 	=> $incomingModel->zip ?? '',
			'billing_country_code' 	=> $incomingModel->country ?? '',
			'phone_1' 	 			=> $incomingModel->phone,
			'ip_address'  			=> $incomingModel->ip,
			'email' 				=> $incomingModel->email ?? '',
			'address' 				=> $incomingModel->address ?? '',
			'opt1' 					=> $incomingModel->getWHash(),
			'product_price' 		=> $incomingModel->landing_cost,
			'local_lead_id'         => $incomingModel->offer_id,
			'product_name'          => $configOffer->product_name,
			'shipping_last_name'    => $incomingModel->name,
			'shipping_address_1'    => $incomingModel->address ?? 'shipping_address_1',
			'shipping_city_town'    => $incomingModel->address ?? 'shipping_city_town',
			'shipping_state_province' => $incomingModel->address ?? 'shipping_state_province',
			'shipping_zip_postcode' => $incomingModel->zip ?? 'shipping_zip_postcode',
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if (!empty($this->response->error))
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode} Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status	= $json->SUCCESS ?? 1;
		
		if ($status == 0) {
			return ['partner_order_id' => (string) $json->CDLID];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}