<?php
namespace app\modules\terraleads\handlers\adv\leadvertexapi1\user_26894;

use yii\web\HttpException;

class Leadvertexapi1BridgeHandler extends \app\modules\terraleads\handlers\adv\leadvertexapi1\Leadvertexapi1BridgeHandler
{
    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
        if (!$responseContent || !is_numeric($responseContent))
            throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");

        return ['partner_order_id' => (string) $responseContent];
    }
}