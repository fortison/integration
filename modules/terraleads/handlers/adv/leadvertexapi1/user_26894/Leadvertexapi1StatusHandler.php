<?php

namespace app\modules\terraleads\handlers\adv\leadvertexapi1\user_26894;

use app\common\models\OrderApilead;
use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use Yii;
/**
 * Обробник статусів замовлень до інтеграції Leadvertext
 *
 * User:
 * Date:
 * Time:
 */
class Leadvertexapi1StatusHandler extends \app\modules\terraleads\handlers\adv\leadvertexapi1\Leadvertexapi1StatusHandler
{
    /**
     * @inheritdoc
     */
    public function makeStatusRequest()
    {
        $requestUrl = $this->config->getUrlOrderInfo() . '?' . http_build_query([
                'webmasterID'	=> $this->config->user->array['webmasterID'],
                'token'			=> $this->config->offer->array['token'],
                'ids'			=> [$this->orderModel->partner_order_id],
            ]);

        $prepareRequest = new PrepareGetRequest();
        $prepareRequest->setRequestUrl($requestUrl);

        $data = $this->statusRequest($prepareRequest);

        return $data;
    }

    /**
     * @inheritdoc
     */
    public function getSystemStatusData(ResponseModel $responseModel)
    {
        $apiKey     = $this->config->user->getApiKey();
        $offerConfig = (object) $this->config->user->array;
        $orderModel = $this->orderModel;
        $orderId	= $orderModel->id;
        $apileadId	= $orderModel->system_order_id;
        $sourceId	= $orderModel->partner_order_id;

        if (!empty($responseModel->error))
            throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$apileadId}");

        $content		= $responseModel->content;

        try {
            $contentObject	= $this->decode($content);
        } catch (\Exception $e) {
            throw new OrderErrorException($orderId, "Decode json error: {$e->getMessage()}");
        }

        if (!isset($contentObject->{$sourceId}))
            throw new OrderErrorException($orderId, "Lead not found {$sourceId}:{$apileadId}");

        $contentObject = $contentObject->{$sourceId};

        if (!isset($contentObject->payment->status))
            throw new OrderErrorException($orderId, "Status is not available for you {$sourceId}:{$apileadId}");

        $comment		= $offerConfig->reasonCancel[$contentObject->reasonCancel] ?? '' . $contentObject->fields->comment ?? '';
        $partnerStatus 	= $contentObject->payment->status;
        $apileadStatus	= $this->getTerraleadsStatusName($partnerStatus);

        if ($apileadStatus === OrderApilead::STATUS_REJECT && ($contentObject->isSpam === 1 || $contentObject->isDouble)) {
            $apileadStatus = OrderApilead::STATUS_TRASH;
        }

        $terraleadsStatusDataModel = new TerraleadsStatusDataModel($apiKey);
        $terraleadsStatusDataModel->setId($apileadId);
        $terraleadsStatusDataModel->setComment($comment);
        $terraleadsStatusDataModel->setUserId($orderModel->system_user_id);
        $terraleadsStatusDataModel->setStatus($apileadStatus);
        $terraleadsStatusDataModel->setPartnerStatus($partnerStatus);
        $terraleadsStatusDataModel->setPartnerId($sourceId);
        //var_dump($terraleadsStatusDataModel);die();
        return $terraleadsStatusDataModel;
    }

    /**
     * @inheritdoc
     */
    public function getRansomStatusData(ResponseModel $responseModel)
    {
        $apiKey     = $this->config->user->getApiKey();
        $offerConfig = (object) $this->config->user->array;
        $orderModel = $this->orderModel;
        $orderId	= $orderModel->id;
        $apileadId	= $orderModel->system_order_id;
        $sourceId	= $orderModel->partner_order_id;

        if (!empty($responseModel->error))
            throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$apileadId}");

        $content	= $responseModel->content;

        $contentObject = $this->decode($content);

        if (!isset($contentObject->{$sourceId}))
            throw new OrderErrorException($orderId, "Lead not found {$sourceId}:{$apileadId}");

        $contentObject = $contentObject->{$sourceId};

        if (!isset($contentObject->buyoutState))
            throw new OrderErrorException($orderId, "Status is not available for you {$sourceId}:{$apileadId}");

        $comment		= $offerConfig->reasonCancel[$contentObject->reasonCancel] ?? '' . $contentObject->fields->comment ?? '';
        $partnerStatus 	= $contentObject->buyoutState;
        $apileadStatus	= $this->getRansomStatusName($partnerStatus);

        $terraleadsStatusDataModel = new TerraleadsStatusDataModel($apiKey);
        $terraleadsStatusDataModel->setId($apileadId);
        $terraleadsStatusDataModel->setUserId($orderModel->system_user_id);
        $terraleadsStatusDataModel->setComment($comment);
        $terraleadsStatusDataModel->setStatus($apileadStatus);
        $terraleadsStatusDataModel->setPartnerStatus($partnerStatus);
        $terraleadsStatusDataModel->setPartnerId($sourceId);
        //print_r($terraleadsStatusDataModel);die();
        return $terraleadsStatusDataModel;
    }
}