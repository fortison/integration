<?php
namespace app\modules\terraleads\handlers\adv\leadvertexapi1;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use yii\helpers\Json;
use yii\web\HttpException;
use Yii;

class Leadvertexapi1BridgeHandler extends BaseBridgeHandler
{
    /**
     * @inheritdoc
     */
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $incomingModel		= $this->incomingModel;

        $args = [$incomingModel, $config];

        $this->requestData = [
            "fio" => $incomingModel->name,
            "country" => $incomingModel->country,
            "phone" => $incomingModel->phone,
            "timezone" => ($incomingModel->tz*60) * -1,
            "referer" => $incomingModel->uHash ?: $incomingModel->wHash,
            "price" => $incomingModel->landing_cost,
            "total" => $incomingModel->landing_cost,
            "ip" => $incomingModel->ip,
            "quantity" => 1,
        ];

        if ($configOffer->configs['parsePhone'] ?? null) {
            $phoneConfig = $configOffer->configs['parsePhone'];
            $phoneRegion = $incomingModel->country;

            if ($phoneConfig['region'] ?? null) {
                if (is_callable($phoneConfig['region'])) {
                    $phoneRegion = call_user_func_array($phoneConfig['region'], $args);
                } else {
                    $phoneRegion = $phoneConfig['region'];
                }
            }

            $phoneNumber = $incomingModel->phone;

            if (is_callable($phoneConfig['number'] ?? null)) {
                $phoneNumber = call_user_func_array($phoneConfig['number'], $args);
            }

            if ($phoneConfig['format'] === 'e164') {
                $phoneFormat = PhoneNumberFormat::E164;
            } elseif ($phoneConfig['format'] === 'national') {
                $phoneFormat = PhoneNumberFormat::NATIONAL;
            } elseif ($phoneConfig['format'] === 'rfc3966') {
                $phoneFormat = PhoneNumberFormat::RFC3966;
            } else {
                $phoneFormat = PhoneNumberFormat::INTERNATIONAL;
            }

            try {
                $phoneParser = PhoneNumberUtil::getInstance();
                $phoneProto = $phoneParser->parse($phoneNumber, $phoneRegion);
                $phoneNumber = $phoneParser->format($phoneProto, $phoneFormat);
            } catch (\Exception $e) {
                $phoneNumber = $incomingModel->phone;
            }

            $this->requestData['phone'] = $phoneNumber;
        }

        if (!empty($configOffer->externalWebmaster)) {
            if (is_callable($configOffer->externalWebmaster))
                $externalWebmaster = call_user_func_array($configOffer->externalWebmaster, $args);
            else
                $externalWebmaster = $configOffer->externalWebmaster;

            $this->requestData['externalWebmaster'] = $externalWebmaster;
        }

        if (!empty($configOffer->externalID)) {
            if (is_callable($configOffer->externalID))
                $externalID = call_user_func_array($configOffer->externalID, $args);
            else
                $externalID = $configOffer->externalID;

            $this->requestData['externalID'] = $externalID;
        }

        if (!empty($configOffer->ip)) {
            if (is_callable($configOffer->ip))
                $ip = call_user_func_array($configOffer->ip, $args);
            else
                $ip = $configOffer->ip;

            $this->requestData['ip'] = $ip;
        }

        if (!empty($configOffer->additional1)) {
            if (is_callable($configOffer->additional1))
                $additional1 = call_user_func_array($configOffer->additional1, $args);
            else
                $additional1 = $configOffer->additional1;

            $this->requestData['additional1'] = $additional1;
        }

        if (!empty($configOffer->additional2)) {
            if (is_callable($configOffer->additional2))
                $additional2 = call_user_func_array($configOffer->additional2, $args);
            else
                $additional2 = $configOffer->additional2;

            $this->requestData['additional2'] = $additional2;
        }

        if (!empty($configOffer->additional3)) {
            if (is_callable($configOffer->additional3))
                $additional3 = call_user_func_array($configOffer->additional3, $args);
            else
                $additional3 = $configOffer->additional3;

            $this->requestData['additional3'] = $additional3;
        }

        if (!empty($configOffer->additional4)) {
            if (is_callable($configOffer->additional4))
                $additional4 = call_user_func_array($configOffer->additional4, $args);
            else
                $additional4 = $configOffer->additional4;

            $this->requestData['additional4'] = $additional4;
        }

        if (!empty($configOffer->additional5)) {
            if (is_callable($configOffer->additional5))
                $additional5 = call_user_func_array($configOffer->additional5, $args);
            else
                $additional5 = $configOffer->additional5;

            $this->requestData['additional5'] = $additional5;
        }

        if (!empty($configOffer->additional6)) {
            if (is_callable($configOffer->additional6))
                $additional6 = call_user_func_array($configOffer->additional6, $args);
            else
                $additional6 = $configOffer->additional6;

            $this->requestData['additional6'] = $additional6;
        }

        if (!empty($configOffer->additional7)) {
            if (is_callable($configOffer->additional7))
                $additional7 = call_user_func_array($configOffer->additional7, $args);
            else
                $additional7 = $configOffer->additional7;

            $this->requestData['additional7'] = $additional7;
        }

        if (!empty($configOffer->additional11)) {
            if (is_callable($configOffer->additional11))
                $additional11 = call_user_func_array($configOffer->additional11, $args);
            else
                $additional11 = $configOffer->additional11;

            $this->requestData['additional11'] = $additional11;
        }

        if (!empty($configOffer->domain)) {
            if (is_callable($configOffer->domain))
                $domain = call_user_func_array($configOffer->domain, $args);
            else
                $domain = $configOffer->domain;

            $this->requestData['domain'] = $domain;
        }

        if (!empty($configOffer->comment)) {
            if (is_callable($configOffer->comment))
                $comment = call_user_func_array($configOffer->comment, $args);
            else
                $comment = $configOffer->comment;

            $this->requestData['comment'] = $comment;
        }

        if (!empty($configOffer->goodId)) {
            if (is_callable($configOffer->goodId))
                $goodId = call_user_func_array($configOffer->goodId, $args);
            else
                $goodId = $configOffer->goodId;

            $this->requestData['goods'] = [[
                'goodID' => $goodId,
                'quantity' => 1,
                'price' => $incomingModel->landing_cost,
            ]];
        }

        if (!empty($configOffer->price)) {
            if (is_callable($configOffer->price))
                $price = call_user_func_array($configOffer->price, $args);
            else
                $price = $configOffer->price;

            $this->requestData['price'] = $price;
        }

        if (!empty($configOffer->total)) {
            if (is_callable($configOffer->total))
                $total = call_user_func_array($configOffer->total, $args);
            else
                $total = $configOffer->total;

            $this->requestData['total'] = $total;
        }

        if (!empty($configOffer->region)) {
            if (is_callable($configOffer->region))
                $region = call_user_func_array($configOffer->region, $args);
            else
                $region = $configOffer->region;

            $this->requestData['region'] = $region;
        }

        if (!empty($configOffer->city)) {
            if (is_callable($configOffer->city))
                $city = call_user_func_array($configOffer->city, $args);
            else
                $city = $configOffer->city;

            $this->requestData['city'] = $city;
        }

        $this->requestUrl = $config->getUrlOrderAdd().'?'.http_build_query([
                'webmasterID' => $configUser->webmasterID,
                'token' => $configOffer->token,
            ]);

        $prepareRequest = new PreparePostRequest();
        $prepareRequest->setRequestUrl($this->requestUrl);
        $prepareRequest->setRequestData($this->requestData);

        $this->response	= $this->bridgeRequest($prepareRequest);

        if (!empty($this->response->error))
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode		= $this->response->getCode();
        $responseContent	= $this->response->getContent();

        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode} Response: {$responseContent}");


        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($responseContent);

        if (!isset($json->OK))
            throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");

        return ['partner_order_id' => (string) $json->OK];
    }
}