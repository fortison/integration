<?php
namespace app\modules\terraleads\handlers\adv\ikon;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;
use Yii;

class IkonBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		
		$this->requestData = [
			'flow_hash' 	=> $configOffer->flow_hash,
			'ip_address'	=> $incomingModel->ip,
			'geo' 	        => $incomingModel->country,
			'name' 	    	=> $incomingModel->name,
			'phone' 		=> $incomingModel->phone,
			'comment' 		=> $incomingModel->user_comment ?? '',
			'address' 		=> $incomingModel->address ?? '',
			'email' 		=> $incomingModel->email ?? '',
			'sub1' 			=> $incomingModel->id,
			'sub3' 			=> $incomingModel->getWHash(),
			'sub5' 			=> $incomingModel->web_id,
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status = $json->data->flow_hash ?? null;
		
		if (!empty($status)) {
			return [];
//			return ['partner_order_id' => (string) $json->orderId];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}