<?php

namespace app\modules\terraleads\handlers\adv\heebs;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class HeebsBridgeHandler  extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		
		$this->requestData = [
			
			'name' => $incomingModel->name,
			'phone_no' => $incomingModel->phone,
			'email' => $incomingModel->email,
			'click_id' => $incomingModel->id,
			'token' => $configUser->api_token,
			'address' => $incomingModel->address,
			'city' => $incomingModel->city,
			'state' => $incomingModel->region,
			'pincode' => $incomingModel->zip,
			'offer_id' => $configOffer->offer_id,
			'pub_id' => $incomingModel->web_id,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		$msg = $json->success ?? null;
		$order_id = $json->order_id ?? null;

//		var_dump($json);die();
		if ($msg === 'Your order has been successfully submitted') {
			return ['partner_order_id' => (string)$order_id];
		}

		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}