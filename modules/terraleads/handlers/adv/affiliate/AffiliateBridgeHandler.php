<?php

namespace app\modules\terraleads\handlers\adv\affiliate;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class AffiliateBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $incomingModel	= $this->incomingModel;

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $this->requestUrl	= $config->getUrlOrderAdd();
        $this->requestData = [

            "goal_id"		=> $configOffer->goal_id,
            "sub_id1"	    => $incomingModel->web_id,
            "aff_click_id"	=> $incomingModel->id,
            "firstname"		=> $incomingModel->name,
            "phone"			=> $incomingModel->phone,
        ];

        $prepareRequest = new PreparePostRequestJson();
        $prepareRequest->setRequestUrl($this->requestUrl . '?' . http_build_query(['api-key' => $configUser->api_key]));
        $prepareRequest->setRequestHeaders(['Content-Type' => 'application/json']);
        $prepareRequest->setRequestData($this->requestData);

        $this->response	= $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();
//        print_r($responseCode);die();
        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseCode)
    {
        $json = $this->decode($responseCode);

        $success = $json->info->status ?? null;
//        print_r($json->info);die();
        $id		= $json->info->lead_id ?? null;

        if ($success === 'success')
            return ['partner_order_id' => (string) $id];

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseCode}}");
    }
}
