<?php


namespace app\modules\terraleads\handlers\adv\igalfer;


use app\common\models\OrderApilead;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PrepareGetRequest;

class IgalferStatusHandler extends BaseStatusHandler
{
    public function updateStatusInSystem($force=false)
    {
        $responseModel = $this->makeStatusRequest();

        $apileadStatusDataModel = $this->getSystemStatusData($responseModel);

        $needSendStatus = $this->orderModel->updateOrder($apileadStatusDataModel);

        if ($force)
            $needSendStatus = true;

        $this->sendOrderStatusToTerraleads($apileadStatusDataModel, $needSendStatus);

        return $apileadStatusDataModel;
    }

    public function makeStatusRequest()
    {
        $requestUrl = $this->config->getUrlOrderInfo() . '?' . http_build_query([
                'api_key'	=> $this->config->user->array['apiKey'],
                'ids'	=> $this->orderModel->partner_order_id,
            ]);

        $prepareRequest = new PrepareGetRequest();
        $prepareRequest->setRequestUrl($requestUrl);

        $data = $this->statusRequest($prepareRequest);

        return $data;
    }

    /**
     * @inheritdoc
     */
    public function getSystemStatusData(ResponseModel $responseModel)
    {
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$partnerId	= $orderModel->partner_order_id;
	
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");
	
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
	
		$content		= $responseModel->content;
		$contentObject	= $this->decode($content);

		if( \is_array($contentObject) === false ){
            throw new OrderErrorException($orderId, 'Wrong response');
        }

		$order			= $contentObject[0];

		if (!$order) {
			throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
		}

        if (!isset($order->status)) {
            throw new OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$terraleadsId}");
        }
	
		$partnerStatus 		= $order->status ?? null;
		$apileadStatus		= $this->getTerraleadsStatusName($partnerStatus);
		$comment			= $order->comment ?? '';
	
		$apileadStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setUserId($orderModel->system_user_id);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($partnerId);
	
		return $apileadStatusDataModel;
    }
}