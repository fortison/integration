<?php

namespace app\modules\terraleads\handlers\adv\igalfer;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PrepareGetRequest;

use yii\web\HttpException;

class IgalferBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $incomingModel	= $this->incomingModel;

        $this->logTerraleadsRequest($incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
                "order_id"          => $incomingModel->id,
                "phone"             => $incomingModel->phone ,
                "api_key"           => $configUser->apiKey,
                "name"              => $incomingModel->name,
                "goods_id"          => $configOffer->goods_id,
                "publisherId"       => $incomingModel->web_id,
            ]);

        $prepareRequest = new PrepareGetRequest();
        $prepareRequest->setRequestUrl($this->requestUrl);
        $this->response	= $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();
        
        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
		$json = $this->decode($responseContent);
	
		$status = $json->status ?? null;
		$id = $json->ext_id ?? null;
	
		if ($status === 'ok')
			return ['partner_order_id' => (string)$id];

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}