<?php

namespace app\modules\terraleads\handlers\adv\mshop;

//use app\common\models\OrderApilead;

use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PreparePostRequest;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use Yii;

class MshopStatusHandler extends BaseStatusHandler
{
	
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$terraleadsStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($terraleadsStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToTerraleads($terraleadsStatusDataModel, $needSendStatus);
		
		return $terraleadsStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$requestUrl		= $this->config->getUrlOrderInfo();
		$requestData	= [
			'ref' => $this->config->user->array['ref'],
			'api_key' => $this->config->user->array['apiKey'],
			'id' => $this->orderModel->partner_order_id,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestData($requestData);
		
		$responseModel = $this->statusRequest($prepareRequest);
		
		return $responseModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$partnerId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content	= $responseModel->content;
		
		$contentObject = $this->decode($content);
		
		$order		= $contentObject->result[0] ?? null;
		
		if (!$order)
			throw new OrderErrorException($orderId, "Order not found in response {$partnerId}:{$apileadId}");

        if (!isset($order->status)) {
            throw new OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$apileadId}");
        }

		$partnerStatus			= (string) $order->status;
		$comment				= ($order->comment ?? '').($order->trash_reason ?? '');
		$apileadStatus          = $this->getTerraleadsStatusName($partnerStatus);
		/*
		if ($order->trash)
			$apileadStatus = OrderApilead::STATUS_TRASH;
		else
			$apileadStatus			= $this->getTerraleadsStatusName($partnerStatus);
		
		if  ($apileadStatus == OrderApilead::STATUS_CONFIRM && $order->sale == 1) {
			$apileadStatus = OrderApilead::STATUS_CONFIRM;
			$comment = 'Запрещенный регион';
		}
		*/
		$terraleadsStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$terraleadsStatusDataModel->setUserId($orderModel->system_user_id);
		$terraleadsStatusDataModel->setId($apileadId);
		$terraleadsStatusDataModel->setComment($comment);
		$terraleadsStatusDataModel->setStatus($apileadStatus);
		$terraleadsStatusDataModel->setPartnerStatus($partnerStatus);
		$terraleadsStatusDataModel->setPartnerId($partnerId);
		
		return $terraleadsStatusDataModel;
	}
	
}