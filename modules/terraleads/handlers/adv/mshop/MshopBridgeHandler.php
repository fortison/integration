<?php

namespace app\modules\terraleads\handlers\adv\mshop;

use app\modules\terraleads\common\objects\PreparePostRequest;
use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use yii\web\HttpException;

class MshopBridgeHandler extends BaseBridgeHandler {
	
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestData = [
			'ref' => $configUser->ref,
			'api_key' => $configUser->apiKey,
			'product_id' => $configOffer->offerId, 
			'langCode' => $configOffer->langCode, 
			'phone' => $incomingModel->phone,
			'name' => $incomingModel->name,
			'ip' => trim($incomingModel->ip),
//			'comment' => $incomingModel->address,
			'comment' => '',
			't' => $incomingModel->web_id,
		];
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$this->checkResponseInvalidPhone($responseContent, 'Duplicate order');
		
		$json = $this->decode($responseContent);
		
		if (isset($json->result) && $json->result == 'ok')
			return ['partner_order_id' => (string) $json->id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}