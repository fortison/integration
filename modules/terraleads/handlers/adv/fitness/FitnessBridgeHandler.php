<?php

namespace app\modules\terraleads\handlers\adv\fitness;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class FitnessBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $incomingModel	= $this->incomingModel;

        $this->requestUrl	= $config->getUrlOrderAdd();
        $this->requestData	= [
            'apiKey'            => $configUser->apiKey,
            'offer_id'          => $configOffer->offer_id,
            'lead_id'           => $incomingModel->id,
            'name'              => $incomingModel->name,
            'address'           => $incomingModel->address,
            'country'           => $incomingModel->country,
            'phone'             => $incomingModel->phone,
            'publisher_id'      => $incomingModel->web_id,
        ];

        $prepareRequest = new PreparePostRequestJson();
        $prepareRequest->setRequestHeaders(['Content-Type' => 'application/json']);
        $prepareRequest->setRequestUrl($this->requestUrl);
        $prepareRequest->setRequestData($this->requestData);

        $this->response	= $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();

        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($responseContent);
//        print_r($json);die();
        $status = $json->status ?? null;
        $order = $json->id ?? null;

        if ($status === 'ok')
            return ['partner_order_id' => (string)$order];

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}