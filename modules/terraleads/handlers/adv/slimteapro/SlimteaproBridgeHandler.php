<?php

namespace app\modules\terraleads\handlers\adv\slimteapro;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;

//use app\modules\terraleads\common\objects\PreparePostRequestJson;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class SlimteaproBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist()) {
            return $model;
        }

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $config = $this->config;
        $configUser = (object)$config->user->array;
        $configOffer = (object)$config->offer->array;

        $incomingModel = $this->incomingModel;

        $this->requestUrl = $config->getUrlOrderAdd();

        $this->requestData = [
            'productId'    => $configOffer->offerId,
            'clientName' => $incomingModel->name,
            'phone' => $incomingModel->phone,
            'email' => $incomingModel->email,
            'address' => $incomingModel->address,
            'city' => $incomingModel->city,
            'postalCode' => $incomingModel->zip,
            'region' => $incomingModel->region,
            'country' => $incomingModel->country,
            'click_id' => $incomingModel->id,
            'webId' => $incomingModel->web_id,
        ];

        $prepareRequest = new PreparePostRequestJson();

        $prepareRequest->setRequestUrl($this->requestUrl);
        $prepareRequest->setRequestData($this->requestData);
//        print_r($this->requestData);die();
//        $prepareRequest->setRequestHeaders(['Content-Type' => 'application/json']);
        $prepareRequest->setRequestHeaders([
            'Content-Type' => 'application/json',
            'X-FIT-TOKEN' => $configUser->api_key
            ]);

        $this->response = $this->bridgeRequest($prepareRequest);

        if ($this->response->error) {
            throw new HttpException('400',
                "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
        }

        $responseCode = $this->response->getCode();
        $responseContent = $this->response->getContent();

        if ($responseCode < 200 or $responseCode > 210) {
            throw new HttpException('400',
                "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
        }

        $partnerOrderId = $this->getOrderAttributes($responseContent);
//        print_r($responseContent);die();
        return $this->saveOrder($partnerOrderId);
    }

    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($responseContent);
//        print_r($json);die();
//        $status = $json->result;
        $id = $json->id;

        if ($json->product == true ) {
            return ['partner_order_id' => (string)$id];
        }


        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}
