<?php

namespace app\modules\terraleads\handlers\adv\voiptime;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class VoiptimeBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'full_name'		=> $incomingModel->name,
			'phone'			=> $incomingModel->phone,
			'shop_id'		=> $configOffer->shop_id,
			'project_id'	=> $configOffer->project_id,
			'utm_source'	=> $incomingModel->id,
			'utm_medium'	=> $incomingModel->web_id,
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		$prepareRequest->setRequestHeaders([
			'Content-Type' 	=> 'application/json',
			'Authorization' => "Bearer {$configUser->token}",
		]);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);

		$status = $json->success ?? null;
		$lead_id = $json->data->lead_id ?? null;
		
		if ($status && $lead_id) {
			return [
				'partner_order_id' => (string) $lead_id,
			];
		}
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}