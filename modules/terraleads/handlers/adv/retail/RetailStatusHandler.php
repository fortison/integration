<?php
namespace app\modules\terraleads\handlers\adv\retail;

use app\common\models\OrderApilead;
use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use Yii;

class RetailStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$apileadStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($apileadStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToTerraleads($apileadStatusDataModel, $needSendStatus);
		
		return $apileadStatusDataModel;
	}

	public function makeStatusRequest()
	{
		$requestUrl = $this->config->getUrlOrderInfo() . '?' . http_build_query([
			'apiKey'	=> $this->config->user->array['retailApiKey'],
			'ids'	=> [$this->orderModel->partner_order_id],
		]);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		
		$data = $this->statusRequest($prepareRequest);

		return $data;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$apileadId	= $orderModel->system_order_id;
		$sourceId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$apileadId}");
		
		$content	= $responseModel->content;
		
		$contentObject = $this->decode($content);
		
		if (!isset($contentObject->success))
			throw new OrderErrorException($orderId, "Wrong request {$sourceId}:{$apileadId}");

		if (!$contentObject->success)
			throw new OrderErrorException($orderId, "Status FALSE {$sourceId}:{$apileadId}");

		if (!isset($contentObject->orders[0]))
			throw new OrderErrorException($orderId, "Order not found in response {$sourceId}:{$apileadId}");
		
		$order = $contentObject->orders[0];
		
		if (!isset($order->status))
			throw new OrderErrorException($orderId, "Status not found for lead {$sourceId}:{$apileadId}");
		
		$comment		= $order->statusComment ?? '';
		$partnerStatus 	= $order->status;
		$apileadStatus	= $this->getTerraleadsStatusName($partnerStatus);
		
		if ($apileadStatus == OrderApilead::STATUS_EXPECT) {
			$comment .= $order->managerComment ?? '';
		}
		
		$apileadStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($sourceId);
		$apileadStatusDataModel->setUserId($orderModel->system_user_id);

		return $apileadStatusDataModel;
	}
}