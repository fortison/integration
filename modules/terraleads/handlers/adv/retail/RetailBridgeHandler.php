<?php
namespace app\modules\terraleads\handlers\adv\retail;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class RetailBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$args = [$incomingModel, $config];

		$order = [
			"externalId"		=> $incomingModel->id,
			"firstName"			=> $incomingModel->name,
			"phone"				=> $incomingModel->phone,
			"ip"				=> $incomingModel->ip,
//            "site"				=> $configOffer->site,
		];

//		if (isset($configOffer->site)) {
//			$order['site'] = $configOffer->site;
//		}
//        if (isset($configOffer->site)) {
//            $order['site'] = $configOffer->site;
//        }
        if (isset($configOffer->countryIso)) {
            $order['countryIso'] = $configOffer->countryIso;
        }
		if (isset($configOffer->offerId)) {
			if (is_callable($configOffer->offerId))
				$offerId = call_user_func_array($configOffer->offerId, $args);
			else
				$offerId = $configOffer->offerId;

			$order['items'][0] = [
				'quantity' => $incomingModel->count,
				'productName' => $configOffer->productName ?? null,
				"offer" 	=> [
					"externalId" 	=> $offerId,
				]
			];
		}

		if (isset($configOffer->initialPrice)) {
			if (is_callable($configOffer->initialPrice))
				$initialPrice = call_user_func_array($configOffer->initialPrice, $args);
			else
				$initialPrice = $configOffer->initialPrice;

			$order['items'][0]['initialPrice'] = $initialPrice;
		}

		if (isset($configOffer->sourceSource)) {
			$order['source']['source'] = $configOffer->sourceSource;
		}

//        if (isset($configOffer->site)) {
//            $order['site']['site'] = $configOffer->site;
//        }
//
		if (isset($configOffer->sourceCampaign)) {
			if (is_callable($configOffer->sourceCampaign)) {
				$sourceCampaign = call_user_func_array($configOffer->sourceCampaign, $args);
			} else {
				$sourceCampaign = $configOffer->sourceCampaign;
			}

			$order['source']['campaign'] = $sourceCampaign;
		}
		
		if (isset($configOffer->customerComment)) {
			if (is_callable($configOffer->customerComment)) {
				$customerComment = call_user_func_array($configOffer->customerComment, $args);
			} else {
				$customerComment = $configOffer->customerComment;
			}

			$order['customerComment'] = $customerComment;
		}
		
		if (isset($configOffer->managerComment)) {
			if (is_callable($configOffer->managerComment)) {
				$managerComment = call_user_func_array($configOffer->managerComment, $args);
			} else {
				$managerComment = $configOffer->managerComment;
			}
			
			$order['managerComment'] = $managerComment;
		}
		
		if (isset($configOffer->weight)) {
			if (is_callable($configOffer->weight)) {
				$weight = call_user_func_array($configOffer->weight, $args);
			} else {
				$weight = $configOffer->weight;
			}

			$order['weight'] = $weight;
		}

//        if (isset($configOffer->sites)) {
//            if (is_callable($configOffer->sites)) {
//                $sites = call_user_func_array($configOffer->sites, $args);
//            } else {
//                $sites = $configOffer->sites;
//            }
//
//            $order['sites'] = $sites;
//        }

        if (is_array($configUser->delivery_city)) {
            if (!isset($incomingModel->city)) {
                $delivery_city = NULL;
            } else {
                $delivery_city_pattern = preg_replace("/\s+/", "", mb_strtolower($incomingModel->city));
                $delivery_city = (in_array($delivery_city_pattern, $configUser->delivery_city))
                    ? $delivery_city_pattern
                    : 'other';
            }

            $order['customFields']['delivery_city'] = $delivery_city;
        }
		
		
		if (isset($configOffer->deliveryCode)) {
			if (is_callable($configOffer->deliveryCode)) {
				$deliveryCode = call_user_func_array($configOffer->deliveryCode, $args);
			} else {
				$deliveryCode = $configOffer->deliveryCode;
			}

			$order['delivery']['code'] = $deliveryCode;
		}

		if (isset($configOffer->deliveryAddressCity)) {
			if (is_callable($configOffer->deliveryAddressCity)) {
				$deliveryAddressCity = call_user_func_array($configOffer->deliveryAddressCity, $args);
			} else {
				$deliveryAddressCity = $configOffer->deliveryAddressCity;
			}

			$order['delivery']['address']['city'] = $deliveryAddressCity;
		}

		$this->requestData = [
			"order" => $this->encode($order),
            "site"	=> $configOffer->site,
		];

		$this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
			'apiKey' => $configUser->retailApiKey,
		]);
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
//		print_r($this->response);
//		exit;
		
		if (!empty($this->response->error))
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 201)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

		$partnerOrderId		= $this->getOrderAttributes($responseContent);
//        print_r($partnerOrderId);die();
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		//print_r($json);die();
		if (isset($json->success) )
			return ['partner_order_id' => (string) $json->id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}