<?php
namespace app\modules\terraleads\handlers\adv\lavandacash;

use app\modules\terraleads\common\objects\PreparePostRequest;
use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use yii\web\HttpException;

class LavandacashBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		
		$this->requestData = [
			'web_id' 		=> $configUser->web_id,
			'click_id' 		=> $incomingModel->id,
			'phone' 		=> $incomingModel->phone,
			'name' 			=> $incomingModel->name,
			'country' 		=> $incomingModel->country ?? '',
			'product_id'    => $configOffer->product_id ?? '',
			'subid1' 		=> $incomingModel->getWHash(),
//			'subid2'		=> $incomingModel->getWHash(),
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if ($json->lead_id) {
			return ['partner_order_id' => (string) $json->lead_id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}