<?php
namespace app\modules\terraleads\handlers\adv\azurecrm;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class AzurecrmBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$args = [$incomingModel, $config];
//
//		$phone = preg_replace("/\s+/", "", str_replace('+', '',$incomingModel->phone));
//		$phone = preg_replace("/\s+/", "", str_replace('+', '',$incomingModel->phone));
//
		$this->requestUrl	= $config->getUrlOrderAdd()
			. '/' . preg_replace("/\s+/", "", str_replace('+', '',$incomingModel->phone))
			. '/' . $configOffer->country_code
			. '/' . $configOffer->product_id;
//		print_r(md5(Date('Y-m-d')));die();
		
		$this->requestData = [
			'first_name' => $incomingModel->name,
			'last_name'  => $incomingModel->name,
			'web_id'  => $incomingModel->web_id,
            'uuid'  => $incomingModel->id,
			'external_partners_id' => $configUser->external_partners_id,
			'api_key'   => $configUser->api_key,
            ''   => $configUser->api_key,
			'timestamp' => md5(Date('Y-m-d')),
		];
		
		if (!empty($configOffer->isfree)) {
			if (is_callable($configOffer->isfree))
				$isfree = call_user_func_array($configOffer->isfree, $args);
			else
				$isfree = $configOffer->isfree;
			
			$this->requestData['isfree'] = $isfree;
		}
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$this->checkResponseInvalidPhone($responseContent, 'Lead duplicate');
		$this->checkResponseInvalidPhone($responseContent, 'Phone invalid');
		$this->checkResponseInvalidPhone($responseContent, 'Invalid phone number');
		$this->checkResponseInvalidPhone($responseContent, 'Phone number blocked');
		
		$json = $this->decode($responseContent);

		$lead_id = $json->lead_id;
		
		if (!empty($lead_id)) {
			return ['partner_order_id' => (string) $lead_id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}