<?php

namespace app\modules\terraleads\handlers\adv\sell1best;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use yii\web\HttpException;

class Sell1bestBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config = $this->config;
		$configUser = (object)$config->user->array;
		$configOffer = (object)$config->offer->array;
		
		$incomingModel = $this->incomingModel;
		
		$this->requestUrl = $config->getUrlOrderAdd();
		$this->requestData = [
			'utm' 			=> $configUser->utm,
			'hash' 			=> $configUser->hash,
			'webmaster' 	=> $incomingModel->web_id,
			'name' 			=> $incomingModel->name,
			'phone' 		=> $incomingModel->phone,
			'country' 		=> $incomingModel->country,
			'product_id' 	=> $configOffer->product_id,
		];
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response = $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode = $this->response->getCode();
		$responseContent = $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId = $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status = $json->success ?? null;
		$orderId = $json->detail->order_id ?? null;
		
		if ($status && $orderId) {
			return [
				'partner_order_id' => (string)$orderId,
			];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}