<?php

namespace app\modules\terraleads\handlers\adv\itgbg;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use yii\web\HttpException;

class ItgbgBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist()) {
            return $model;
        }

        $incomingModel = $this->incomingModel;

        $this->logTerraleadsRequest($incomingModel->attributes);

        $config = $this->config;
        $configUser = (object)$config->user->array;
        $configOffer = (object)$config->offer->array;

        $this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
                "leadid" => $incomingModel->id,
                "name" => $incomingModel->name,
                "phone1" => $incomingModel->phone,
                "phone2" => $incomingModel->phone,
                "webmaster" => $incomingModel->web_id,
                "product" => $configOffer->product,
               "adress" => $incomingModel->address ?? null,
                "datetime" => date("Y-m-d H-i-s"),
                "clid" => $configOffer->clid,
            ]);

        $prepareRequest = new PrepareGetRequest();
        $prepareRequest->setRequestUrl($this->requestUrl);
        $this->response = $this->bridgeRequest($prepareRequest);
//		print_r($this->response);die();
        if ($this->response->error) {
            throw new HttpException('400',
                "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
        }

        $responseCode = $this->response->getCode();

        $responseContent = $this->response->getContent();
//		print_r($this->response->getContent());die();
        if ($responseCode != 200) {
            throw new HttpException('400',
                "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
        }

        $partnerOrderId = $this->getOrderAttributes($responseContent);
//		print_r($partnerOrderId);die();
        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
		if(stristr($responseContent, 'OK')) {
			return ['partner_order_id' => ''];
		}

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}