<?php
namespace app\modules\terraleads\handlers\adv\adc;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PrepareGetRequest;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class AdcBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'api_key' => $configUser->apiKey,
			'name' => $incomingModel->id,
			'phone' => $incomingModel->phone,
			'offer_id' => $configOffer->offer_id,
			'country_code' => $incomingModel->country,
			'base_url' => $configOffer->base_url,
			'price' => $incomingModel->landing_cost,
			'referrer' => 'https://www.facebook.com/',
			'ip' => $incomingModel->ip,
			'ext_in_id' => $incomingModel->getWHash(),
			'address' => $incomingModel->address,
			'email' => $incomingModel->email ?? '',
		];
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status = $json->code ?? null;
		$orderId = $json->order_id ?? null;
		
		if ($status === 'ok' && $orderId) {
			return [
				'partner_order_id' => (string) $orderId,
			];
		}
		
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}