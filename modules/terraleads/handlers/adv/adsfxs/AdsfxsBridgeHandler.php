<?php
namespace app\modules\terraleads\handlers\adv\adsfxs;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class AdsfxsBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;

		$args = [$incomingModel, $config];

		$this->requestUrl	= $config->getUrlOrderAdd();
		
		$this->requestData = [
			'api_key' => $configUser->api_key,
			'offer_id' => $configOffer->offer_id,
			'name' => $incomingModel->name,
			'phone' => $incomingModel->phone,
			'click_id' => $incomingModel->id,
			'subid1'=> $incomingModel->getWHash(),
		];
		
		if (!empty($configOffer->quantity)) {
			if (is_callable($configOffer->quantity))
				$quantity = call_user_func_array($configOffer->quantity, $args);
			else
				$quantity = $configOffer->quantity;

			$this->requestData['other']['quantity'] = $quantity;
		}

		if (!empty($configOffer->color)) {
			if (is_callable($configOffer->color))
				$color = call_user_func_array($configOffer->color, $args);
			else
				$color = $configOffer->color;

			$this->requestData['other']['color'] = $color;
		}
				$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$error = $json->error ?? true;
		$clickId = $json->click_id ?? null;
		
		if (!$error && $clickId) {
			return ['partner_order_id' => (string) $clickId];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}