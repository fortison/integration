<?php

namespace app\modules\terraleads\handlers\adv\emyad;

use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PrepareGetRequestJson;

class EmyadStatusHandler extends BaseStatusHandler
{
	/**
	 * @inheritdoc
	 */
	public function updateStatusInSystem($force = false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$terraleadsStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($terraleadsStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToTerraleads($terraleadsStatusDataModel, $needSendStatus);
		
		return $terraleadsStatusDataModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function makeStatusRequest()
	{
		$orderModel = $this->orderModel;
		$configUser = (object)$this->config->user->array;
		$configOffer = (object)$this->config->offer->array;
		
		$requestUrl = $this->config->getUrlOrderInfo() . '?' . http_build_query([
				'order_uuids' 	=> $orderModel->partner_order_id,
				'api_token' 	=> $configOffer->api_token ?? $configUser->api_token,
			]);
		
		$prepareRequest = new PrepareGetRequestJson();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestHeaders([
			'Content-type' 	=> 'application/json',
			'Accept' 		=> 'application/json',
		]);
		
		$data = $this->statusRequest($prepareRequest);
		
		return $data;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId = $orderModel->id;
		$terraleadsId = $orderModel->system_order_id;
		$sourceId = $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$sourceId}:{$terraleadsId}");
		
		$content = $responseModel->content;
		
		try {
			$contentObject = $this->decode($content);
		} catch (\Exception $e) {
			throw new OrderErrorException($orderId, "Decode json error: {$e->getMessage()}");
		}
		
		if (!isset($contentObject[0]))
			throw new OrderErrorException($orderId, "Status is not available for you {$sourceId}:{$terraleadsId}");
		
		if (!isset($contentObject[0]->status_id) && !empty($contentObject[0]->order_uuid))
			throw new OrderErrorException($orderId, "Status is not available for you {$sourceId}:{$terraleadsId}");
		
		$comment 			= $contentObject[0]->comment ?? '';
		$partnerStatus 		= (string)$contentObject[0]->status_id;
		$terraleadsStatus 	= $this->getTerraleadsStatusName($partnerStatus);
		
		$terraleadsStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$terraleadsStatusDataModel->setId($terraleadsId);
		$terraleadsStatusDataModel->setUserId($orderModel->system_user_id);
		$terraleadsStatusDataModel->setComment($comment);
		$terraleadsStatusDataModel->setStatus($terraleadsStatus);
		$terraleadsStatusDataModel->setPartnerStatus($partnerStatus);
		$terraleadsStatusDataModel->setPartnerId($sourceId);
		
		return $terraleadsStatusDataModel;
	}
}