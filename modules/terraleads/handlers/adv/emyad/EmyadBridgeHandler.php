<?php

namespace app\modules\terraleads\handlers\adv\emyad;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class EmyadBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config 		= $this->config;
		$configUser 	= (object)$config->user->array;
		$configOffer 	= (object)$config->offer->array;
		
		$incomingModel = $this->incomingModel;
		
		$this->requestUrl = $config->getUrlOrderAdd();
		$this->requestData = [
			'offer_id' 			=> $configOffer->offer ?? $incomingModel->offer_id,
			'landing_cost' 		=> $incomingModel->landing_cost,
			'cost' 				=> $incomingModel->cost,
			'count' 			=> $incomingModel->count,
			'first_name' 		=> $incomingModel->name,
			'name' 				=> $incomingModel->name,
			'price' 			=> $incomingModel->cost_delivery,
			'phone' 			=> $incomingModel->phone,
			'country' 			=> $incomingModel->country,
			'id' 				=> $incomingModel->id,
			'web_id' 			=> $incomingModel->web_id,
			'ip' 				=> $incomingModel->ip,
			'api_token' 		=> $configOffer->api_token ?? $configUser->api_token,
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		$prepareRequest->setRequestHeaders(
			[
				'Accept' 		=> 'application/json',
				'Content-Type' 	=> 'application/json',
			]
		);
		
		$this->response = $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode = $this->response->getCode();
		$responseContent = $this->response->getContent();
		
		if ($responseCode < 200 && $responseCode > 210)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId = $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (!empty($json->status) && !empty($json->order_uuid) && $json->status == 'ok') {
			return [
				'partner_order_id' => (string)$json->order_uuid,
			];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}