<?php

namespace app\modules\terraleads\handlers\adv\luckyweb;

use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\apilead\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PrepareGetRequest;


class LuckywebStatusHandler extends BaseStatusHandler
{
    public function updateStatusInSystem($force=false)
    {
        $responseModel = $this->makeStatusRequest();

        $apileadStatusDataModel = $this->getSystemStatusData($responseModel);

        $needSendStatus = $this->orderModel->updateOrder($apileadStatusDataModel);

        if ($force)
            $needSendStatus = true;

        $this->sendOrderStatusToTerraleads($apileadStatusDataModel, $needSendStatus);

        return $apileadStatusDataModel;
    }

    public function makeStatusRequest()
    {
        $config			= $this->config;
        $configUser		= (object) $config->user->array;

        $orderModel = $this->orderModel;

        $requestUrl = $config->getUrlOrderInfo() . "?" . http_build_query([
                'click_id' => $orderModel->partner_order_id,
                'api_key' => $configUser->apiKey,
            ]);

        $prepareRequest = new PrepareGetRequest();
        $prepareRequest->setRequestUrl($requestUrl);

        $responseModel = $this->statusRequest($prepareRequest);

        return $responseModel;
    }

    /**
     * @inheritdoc
     */
    public function getSystemStatusData(ResponseModel $responseModel)
    {
        $apiKey     = $this->config->user->getApiKey();
        $orderModel = $this->orderModel;
        $orderId	= $orderModel->id;
        $apileadId	= $orderModel->system_order_id;
        $partnerId	= $orderModel->partner_order_id;

        if (!empty($responseModel->error))
            throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");

        if ($responseModel->code != 200)
            throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");

        $content		= $responseModel->content;

        $contentObject	= $this->decode($content);

        if (isset($contentObject->success) && $contentObject->success != true)
            throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");

        $order			= $contentObject;

		if (!$order) {
            throw new OrderErrorException($orderId, "Order not found in response {$partnerId}:{$apileadId}");
        }
        if (!isset($order->status)) {
            throw new \app\modules\terraleads\common\exeptions\OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$apileadId}");
        }

        $partnerStatus 		= (string) $order->status;
        $apileadStatus		= $this->getTerraleadsStatusName($partnerStatus);
        $comment			= $order->comment ?? '';

        $apileadStatusDataModel = new TerraleadsStatusDataModel($apiKey);
        $apileadStatusDataModel->setId($apileadId);
        $apileadStatusDataModel->setComment($comment);
        $apileadStatusDataModel->setStatus($apileadStatus);
        $apileadStatusDataModel->setPartnerStatus($partnerStatus);
        $apileadStatusDataModel->setPartnerId($partnerId);

        return $apileadStatusDataModel;
    }
}