<?php

namespace app\modules\terraleads\handlers\adv\luckyweb;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class LuckywebBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $incomingModel	= $this->incomingModel;

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;


        $this->requestData = [
            "name"			=> $incomingModel->name,
            "phone"			=> $incomingModel->phone ,
            "user_agent"	=> $incomingModel->user_agent,
            "ip"			=> $incomingModel->ip,
            "campaign_hash"	=> $configOffer->campaign_hash,
            "country"	    => $incomingModel->country,
        ];

        $this->requestUrl = $config->getUrlOrderAdd() . "?"
            . http_build_query([
                "api_key" => $configUser->apiKey,
            ]);

        $prepareRequest = new PreparePostRequest();
        $prepareRequest->setRequestUrl($this->requestUrl);
        $prepareRequest->setRequestData($this->requestData);

        $this->response	= $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();
//        print_r($responseCode);die();
        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseCode)
    {
        $json = $this->decode($responseCode);

        $success = $json->success ?? null;
        $id		= $json->data->click_id ?? null;

        if ($responseCode != 200)
            return ['partner_order_id' => (string) $id];

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseCode}}");
    }
}
