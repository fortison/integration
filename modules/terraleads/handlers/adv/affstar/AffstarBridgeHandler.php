<?php
namespace app\modules\terraleads\handlers\adv\affstar;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class AffstarBridgeHandler extends BaseBridgeHandler
{
	/**
	 * @inheritdoc
	 */
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd().'?'.http_build_query(['api_key' => $configUser->api_key]);
		$this->requestData = [
			'flow_uuid' 		=> $configOffer->flow_uuid,
			'ip' 				=> $incomingModel->ip,
			'country_code' 		=> $incomingModel->country,
			'phone' 		    => $incomingModel->phone,
			'subaccount' 		=> $incomingModel->web_id,
			'sub_id1' 			=> $incomingModel->id,
			'user_agent'		=> $incomingModel->user_agent,
			'name'    		    => $incomingModel->name,
			'comment'		    => $incomingModel->user_comment,
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (isset($json->success) && $json->success == true) {
			if (isset($json->data) && !empty($json->data->lead_uuid)) {
				return ['partner_order_id' => (string) $json->data->lead_uuid];
			}
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}