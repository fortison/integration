<?php
namespace app\modules\terraleads\handlers\adv\monetizeadv2;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class Monetizeadv2BridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist()) {
			return $model;
		}
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestData = [
			'offer_id'   	=> $configOffer->offer_id,
            'auth'   	    => $configOffer->auth ?? $configUser->auth,
			'network' 		=> $configOffer->network ?? $configUser->network,
			'name'  		=> $incomingModel->name,
			'phone' 		=> $incomingModel->phone,
			'country_code'  => $configOffer->country_code,
			'client_ip' 	=> $incomingModel->ip,
			'address' 		=> $incomingModel->address ?? '',
			'email'  		=> $incomingModel->email ?? '',
			'zip'    		=> $incomingModel->zip,
			'quantity'		=> $incomingModel->count,
			'tracking'  	=> [
				'clickId'   => $incomingModel->id,
				'sub_1'     => $incomingModel->web_id,
			],
		];

		$this->requestUrl = $config->getUrlOrderAdd();
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error) {
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		}
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200 && $responseCode != 201) {
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		}
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{

		$json = $this->decode($responseContent);

		if (isset($json->type) && $json->type == 'success' ) {
			return ['partner_order_id' => (string) $json->order_id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}