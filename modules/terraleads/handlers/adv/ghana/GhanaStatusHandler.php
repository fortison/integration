<?php

namespace app\modules\terraleads\handlers\adv\ghana;

use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PreparePostRequestJson;

class GhanaStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force = false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$terraleadsStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($terraleadsStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToTerraleads($terraleadsStatusDataModel, $needSendStatus);
		
		return $terraleadsStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$config = $this->config;
		$configUser = (object)$config->user->array;
		$requestUrl = $config->getUrlOrderInfo();
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestHeaders(
			[
				'Content-Type' => 'application/json',
				'Api-Key' => $configUser->apiKey
			]
		);
		$prepareRequest->setRequestData([
			'externals' =>[ $this->orderModel->system_order_id]
		]);
		
		return $this->statusRequest($prepareRequest);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId = $orderModel->id;
		$apileadId = $orderModel->system_order_id;
		$partnerId = $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content		= $responseModel->content;
		
		$contentObject = $this->decode($content);
		
		if (!$contentObject) {
			throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
		}
		
		$order = $contentObject->result;
		if (!$order) {
			throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
		}
		
		$partnerStatus = $order->$apileadId->status ?? null;
		
		$apileadStatus = $this->getTerraleadsStatusName($partnerStatus);
		
		$apileadStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setUserId($orderModel->system_user_id);
//		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($partnerId);
		
		return $apileadStatusDataModel;
	}
}
