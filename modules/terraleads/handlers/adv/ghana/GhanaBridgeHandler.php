<?php
namespace app\modules\terraleads\handlers\adv\ghana;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class GhanaBridgeHandler extends BaseBridgeHandler
{
	/**
	 * @inheritdoc
	 */
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData = [
			'external' 		=> (string)$incomingModel->id,
			'username' 		=> (string)$incomingModel->name,
			'phones' 		=> [$incomingModel->getClearPhone()],
			'products' 		=> [
				[
					'code' 		=> (string)$configOffer->code,
					'quantity' 	=> (int)$incomingModel->count,
					'price' 	=> (int)$incomingModel->landing_cost,
				]
			],
			'campaign' 		=> (string)$configUser->campaign,
			'ipaddress' 	=> (string)$incomingModel->ip,
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestHeaders(
			[
				'Api-Key' => $configUser->apiKey,
				'Content-Type' => 'application/json'
			]
		);
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode < 200 || $responseCode > 210)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$this->checkResponseInvalidPhone($responseContent, 'Valid phones not provided');
		
		$json = $this->decode($responseContent);
		
		if (!empty($json->ok)) {
			if (!empty($json->result) && !empty($json->result->order_id))
			return ['partner_order_id' => (string)$json->result->order_id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}