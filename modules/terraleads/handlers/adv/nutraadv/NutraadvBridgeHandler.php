<?php

namespace app\modules\terraleads\handlers\adv\nutraadv;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class NutraadvBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config 		= $this->config;
		$configUser 	= (object)$config->user->array;
		$configOffer 	= (object)$config->offer->array;
		
		$incomingModel = $this->incomingModel;
		
		$this->requestUrl = sprintf($config->getUrlOrderAdd(), $configUser->name, $configUser->api_key);
		$this->requestData = [
			'firstname' 			=> $incomingModel->name,
			'phone' 				=> $incomingModel->phone,
			'webmaster_id' 			=> $incomingModel->web_id,
			'partner_order_id' 		=> $incomingModel->id,
			'product_id' 			=> $configOffer->product_id,
			'quantity' 				=> 1,
			'price' 				=> $incomingModel->landing_cost,
			'currency' 				=> $incomingModel->landing_currency,
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		$prepareRequest->setRequestHeaders(['Content-Type' => 'application/json']);
		
		$this->response = $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode = $this->response->getCode();
		$responseContent = $this->response->getContent();
		
		if ($responseCode < 200 && $responseCode > 210)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId = $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (!empty($json->status) && !empty($json->id) && $json->status == 'ok') {
			return ['partner_order_id' => (string)$json->id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}