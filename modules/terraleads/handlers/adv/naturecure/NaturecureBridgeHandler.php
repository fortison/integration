<?php

namespace app\modules\terraleads\handlers\adv\naturecure;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class NaturecureBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist()) {
            return $model;
        }

        $this->logTerraleadsRequest($this->incomingModel->attributes);

        $config = $this->config;
        $configUser = (object)$config->user->array;
        $configOffer = (object)$config->offer->array;

        $incomingModel = $this->incomingModel;

        $this->requestUrl = $config->getUrlOrderAdd();

        $this->requestData = [
            'name' => $incomingModel->name,
            'ip' => $incomingModel->ip,
            'address' => $incomingModel->address,
            'email' => $incomingModel->email,
            'phone' => $incomingModel->phone,
            'lead_uuid' => $incomingModel->id,
            'affiliate_id' => $incomingModel->web_id,
            'source' => $configOffer->source,
            'product_qty' => $incomingModel->count,
            'campaign' => $configOffer->campaign,
        ];

        $prepareRequest = new PreparePostRequest();
//        $prepareRequest->setRequestHeaders();
        $prepareRequest->setRequestUrl($this->requestUrl);
        $prepareRequest->setRequestData($this->requestData);

        $this->response = $this->bridgeRequest($prepareRequest);

        if ($this->response->error) {
            throw new HttpException('400',
                "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
        }

        $responseCode = $this->response->getCode();
        $responseContent = $this->response->getContent();

        if ($responseCode != 200) {
            throw new HttpException('400',
                "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
        }

        $partnerOrderId = $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
        if ($responseContent == 'New record created successfully') {
            return [];
        }

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}
