<?php
namespace app\modules\terraleads\handlers\adv\bitrix;

use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\models\forms\ResponseModel;

class BitrixStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force=false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$systemStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($systemStatusDataModel);
		
		if ($force)
			$needSendStatus = true;
		
		$this->sendOrderStatusToTerraleads($systemStatusDataModel, $needSendStatus);
		
		return $systemStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$config			= $this->config;
		$orderModel		= $this->orderModel;
		
		$client = new Client(['config'=>$config]);
		
		return $client->leadInfo($orderModel->partner_order_id);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey     = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId	= $orderModel->id;
		$systemId	= $orderModel->system_order_id;
		$userId		= $orderModel->system_user_id;
		$partnerId	= $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$systemId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content		= $responseModel->content;
		
		$contentObject	= $this->decode($content);
		
		if (!isset($contentObject->result))
			throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
		
		$order			= $contentObject->result;
		
		if (!isset($order->STATUS_ID))
			throw new OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$systemId}");
		
		$partnerStatus 		= (string) $order->STATUS_ID;
		$apileadStatus		= $this->getTerraleadsStatusName($partnerStatus);
		$comment			= $order->COMMENTS ?? '';
		
		$apileadStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($systemId);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($partnerId);
		$apileadStatusDataModel->setUserId($userId);
		
		return $apileadStatusDataModel;
	}
}