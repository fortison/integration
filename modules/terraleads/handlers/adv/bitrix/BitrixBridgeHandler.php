<?php
namespace app\modules\terraleads\handlers\adv\bitrix;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\models\forms\ResponseModel;

class BitrixBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel	= $this->incomingModel;

		$this->logTerraleadsRequest($incomingModel->attributes);
		
		$config			= $this->config;
		
		$client = new Client(['config'=>$config]);
		
		$partnerOrderId = $client->leadAdd($incomingModel);
		
		$this->response		= new ResponseModel;
		$this->requestUrl	= 'Sent via client. See bridge log.';
		$this->requestData	= 'Sent via client. See bridge log.';
		
		$orderAttributes = $this->getOrderAttributes($partnerOrderId);
		
		return $this->saveOrder($orderAttributes);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($partnerOrderId)
	{
		return ['partner_order_id' => (string) $partnerOrderId];
	}
}