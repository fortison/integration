<?php
namespace app\modules\terraleads\handlers\adv\altercpa;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class AltercpaBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd() . '?' . http_build_query([
				'id'	=> $configUser->user . '-' . $configUser->key,
			]);;
		
		$this->requestData = [
			'id' 	   => $incomingModel->id,
			'name' 	   => $incomingModel->name,
			'country'  => $incomingModel->country,
			'phone'    => $incomingModel->phone,
			'offer'    => $configOffer->offer,
			'email'    => $incomingModel->email ?? '',
			'ua'       => $incomingModel->user_agent,
			'ip'       => $incomingModel->ip,
			'index'    => $incomingModel->zip ?? '',
			'addr'     => $incomingModel->address ?? '',
			'city'     => $incomingModel->city ?? '',
			'count'   => $incomingModel->count ?? '',
			'wm'	   => $incomingModel->getWHash(),
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		$status = $json->status;
		
		if ($status == 'ok') {
			return ['partner_order_id' => (string) $json->id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}