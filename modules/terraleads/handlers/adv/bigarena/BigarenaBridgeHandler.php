<?php

namespace app\modules\terraleads\handlers\adv\bigarena;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class BigarenaBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$incomingModel = $this->incomingModel;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config = $this->config;
		$configUser = (object)$config->user->array;
		$configOffer = (object)$config->offer->array;
		
		$this->requestUrl = $config->getUrlOrderAdd();
		$this->requestData = [
			
			"order_id" => $incomingModel->id,
			"date_added" => date('Y-m-d H:i:s'),
			"customer_first_name" => $incomingModel->name,
			"customer_last_name" => $incomingModel->name,
			"customer_telephone" => $incomingModel->phone,
			"postal_code" => $incomingModel->zip ?? '0000',
			"total" => $incomingModel->cost,
			"country" => $incomingModel->country,
			"currency" => $incomingModel->landing_currency,
			"city" => $incomingModel->city ?? 'empty',
			"address" => $incomingModel->address ?? 'empty',
			'products' =>
				[
					[
					'name' => $configOffer->name,
					'model' => $configOffer->model,
					'quantity' =>  (int) $incomingModel->count,
//					'extra_services' =>  $incomingModel->count,
					]
				],
			
			
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		$prepareRequest->setRequestHeaders(
			[
				'Content-Type' => 'application/json',
				'Authorization' => 'Bearer ' . $configUser->api_key
			]
		);
		$this->response = $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode = $this->response->getCode();
		$responseContent = $this->response->getContent();

		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId = $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseCode)
	{
		$json = $this->decode($responseCode);
//		print_r($json);die();
		if ($json->success == true)
			return ['partner_order_id' => ''];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseCode}}");
	}
}