<?php


namespace app\modules\terraleads\handlers\adv\bigarena;


use app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\models\forms\ResponseModel;

//use app\modules\terraleads\common\objects\PrepareGetRequest;
//use app\modules\terraleads\common\objects\PreparePostRequest;
use app\modules\terraleads\common\objects\PreparePostRequest;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\helpers\Json;

class BigarenaStatusHandler extends BaseStatusHandler
{
	public function updateStatusInSystem($force = false)
	{
		$responseModel = $this->makeStatusRequest();
		
		$terraleadsStatusDataModel = $this->getSystemStatusData($responseModel);
		
		$needSendStatus = $this->orderModel->updateOrder($terraleadsStatusDataModel);
		
		if ($force)
			$needSendStatus = true;

		$this->sendOrderStatusToTerraleads($terraleadsStatusDataModel, $needSendStatus);
		
		return $terraleadsStatusDataModel;
	}
	
	public function makeStatusRequest()
	{
		$config = $this->config;
		$configUser = (object)$config->user->array;
		
		$requestData = [
			[$this->orderModel->system_order_id],
		];
		
		$requestContent = Json::encode($requestData);
		$requestUrl = $config->getUrlOrderInfo();
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestContent($requestContent);
		$prepareRequest->setRequestHeaders(
			[
				'Content-Type' => 'application/json',
				'Authorization' => 'Bearer ' . $configUser->api_key
			]
		);
		$responseModel = $this->statusRequest($prepareRequest);
		
		return $responseModel;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getSystemStatusData(ResponseModel $responseModel)
	{
		$apiKey = $this->config->user->getApiKey();
		$orderModel = $this->orderModel;
		$orderId = $orderModel->id;
		$apileadId = $orderModel->system_order_id;
		$partnerId = $orderModel->partner_order_id;
		
		if (!empty($responseModel->error))
			throw new OrderErrorException($orderId, "{$responseModel->error} {$partnerId}:{$apileadId}");
		
		if ($responseModel->code != 200)
			throw new OrderErrorException($orderId, "Partner error! Response code: {$responseModel->code}. Response: {$responseModel->content}");
		
		$content		= $responseModel->content;
		
		$contentObject = $this->decode($content);
		
		$order = $contentObject->{$orderModel->system_order_id} ?? null;
		if (!$order) {
			throw new OrderErrorException($orderId, "Partner error!. Response: {$responseModel->content}");
		}

        if (!isset($order->status)) {
            throw new OrderErrorException($orderId, "Status not found for lead {$partnerId}:{$apileadId}");
        }

		$partnerStatus = $order->status ?? null;
		
		$apileadStatus = $this->getTerraleadsStatusName($partnerStatus);
		$comment       = $order->text;
		
		$apileadStatusDataModel = new TerraleadsStatusDataModel($apiKey);
		$apileadStatusDataModel->setId($apileadId);
		$apileadStatusDataModel->setUserId($orderModel->system_user_id);
		$apileadStatusDataModel->setComment($comment);
		$apileadStatusDataModel->setStatus($apileadStatus);
		$apileadStatusDataModel->setPartnerStatus($partnerStatus);
		$apileadStatusDataModel->setPartnerId($partnerId);
		
		return $apileadStatusDataModel;
	}
}