<?php
namespace app\modules\terraleads\handlers\adv\tms;

use app\common\models\OrderApilead;
use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\httpclient\Client;
use yii\web\HttpException;

class TmsBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		
		$apikey = (empty($configOffer->apikey)) ? $configUser->apikey : $configOffer->apikey;
		
		$this->requestData = [
			'apikey' 			=> $apikey,
			'orderId' 			=> $configOffer->orderId,
			'name' 				=> $incomingModel->name,
			'phone' 			=> $incomingModel->phone,
			'email' 			=> $incomingModel->email ?? '',
			'address' 			=> $incomingModel->address,
			'productName' 		=> $configOffer->productName,
			'quantity'			=> 1,
			'clickId'    		=> $incomingModel->id,
			'affid'    			=> $incomingModel->uHash ?: $incomingModel->wHash,
			'orderType'			=> 'CPA',
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200 && $responseCode != 400)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if ($json->status == 400) {
			$incomingModel = $this->incomingModel;
			$apiKey = $this->config->user->getApiKey();
			$errorMessage = $json->message ?? '';
			
			$terraleadsDataModel = new TerraleadsStatusDataModel($apiKey);
			$terraleadsDataModel->setId($incomingModel->id);
			$terraleadsDataModel->setUserId($incomingModel->user_id);
			$terraleadsDataModel->setComment($errorMessage);
			$terraleadsDataModel->setStatus(OrderApilead::STATUS_TRASH);
			
			$requestUrl = $this->config->getUrlTerraleadsOrderUpdate();
			$requestUrl = $requestUrl . '?' . http_build_query(['check_sum' => $terraleadsDataModel->getCheckSum()]);
			$requestData = $terraleadsDataModel->getStatusData();
			
			$prepareRequest = new PreparePostRequestJson();
			$prepareRequest->setRequestUrl($requestUrl);
			$prepareRequest->setRequestData($requestData);
			$prepareRequest->setResponseFormat(Client::FORMAT_JSON);
			
			$responseModel = $this->statusRequest($prepareRequest);
			
			if (!$responseModel->error && $responseModel->code == 200)
				return ['system_status' => OrderApilead::STATUS_TRASH];
			
			exit($responseModel->content);
		}
		
		if ($json->status == 3) {
			return ['system_status' => OrderApilead::STATUS_EXPECT];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}