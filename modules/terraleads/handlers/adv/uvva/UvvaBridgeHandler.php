<?php
namespace app\modules\terraleads\handlers\adv\uvva;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class UvvaBridgeHandler extends BaseBridgeHandler
{
	/**
	 * @inheritdoc
	 */
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		
		$this->requestData	= [
			'name'             => $incomingModel->name,
			'lead_information' => $configOffer->lead_information,
			'phone' 	 	   => $incomingModel->phone,
			'country' 	 	   => $incomingModel->country,
			'price'      	   => $incomingModel->landing_cost,
			'quantity'   	   => $incomingModel->count,
			'information'	   => $incomingModel->user_comment,
			'utm_source' 	   => $incomingModel->id,
			'offer_id'   	   => $configOffer->offer_id,
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestHeaders(
			[
				'Content-Type' => 'application/json',
				'Authorization' => 'Bearer ' . $configUser->token
			]
		);
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if (!empty($this->response->error))
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode} Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (isset($json->success) && $json->success == true && isset($json->lead) && isset($json->lead->id)) {
			return ['partner_order_id' => (string) $json->lead->id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}