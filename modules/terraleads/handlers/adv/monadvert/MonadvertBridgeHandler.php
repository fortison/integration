<?php
namespace app\modules\terraleads\handlers\adv\monadvert;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class MonadvertBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist()) {
			return $model;
		}
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$args = [$incomingModel, $config];
		
		$this->requestData = [
			'api-key'       => $configUser->api_key,
			'offer_id'      => $configOffer->offer_id,
			'affiliate_id'  => $configUser->affiliate_id,
			'firstname'    	=> $incomingModel->name,
			'phone'    		=> $incomingModel->phone,
			'conv_status'   => 'pending',
			'goal_id'       => $configOffer->goal_id,
			'aff_click_id'  => $incomingModel->id,
			'sub_id1'       => $incomingModel->web_id,
			'address'       => $incomingModel->address,
		];
		
		$this->requestUrl = $config->getUrlOrderAdd().'?api-key='.$configUser->api_key;
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error) {
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		}
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200) {
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		}
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (!empty($json->status) && $json->status === 'success'
			&& !empty($json->info) && !empty($json->info->lead_id)) {
			return ['partner_order_id' => $json->info->lead_id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}