<?php

namespace app\modules\terraleads\handlers\adv\insneakers;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class InsneakersBridgeHandler extends BaseBridgeHandler
{
	/**
	 * @inheritdoc
	 */
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config = $this->config;
		$configUser = (object)$config->user->array;
		$configOffer = (object)$config->offer->array;
		
		$incomingModel = $this->incomingModel;
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$this->requestData = [
			'apikey' 		=> $configUser->apikey,
			'code' 			=> $configOffer->code,
			'name' 			=> $incomingModel->name,
			'phone' 		=> $incomingModel->phone,
			'ip' 			=> $incomingModel->ip,
			'foreign_id' 	=> $incomingModel->id,
			'sub1' 			=> $incomingModel->web_id,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response = $this->bridgeRequest($prepareRequest);
		
		if (!empty($this->response->error))
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode = $this->response->getCode();
		$responseContent = $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode} Response: {$responseContent}");
		
		$partnerOrderId = $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$status = $json->success ?? null;
		$id = $json->id ?? null;
		
		if (!empty($status) && $id) {
			return ['partner_order_id' => (string)$id];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}