<?php

namespace app\modules\terraleads\handlers\adv\healthy;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PrepareGetRequest;

use yii\web\HttpException;

class HealthyBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $incomingModel	= $this->incomingModel;

        $this->logTerraleadsRequest($incomingModel->attributes);

        $config			= $this->config;
//        $configUser		= (object) $config->user->array;
        $configOffer	= (object) $config->offer->array;

        $this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
                "id"                => $incomingModel->id,
                "phone"             => $incomingModel->phone ,
                "name"              => $incomingModel->name,
                "country"           => $incomingModel->country,
                "tz"                => $incomingModel->tz,
                "address"           => $incomingModel->address,
                "cost"              => $incomingModel->cost,
                "cost_delivery"     => $incomingModel->cost_delivery,
                "landing_cost"      => $incomingModel->landing_cost,
                "landing_currency"  => $incomingModel->landing_currency,
                "web_id"            => $incomingModel->web_id,
                "stream_id"         => $incomingModel->stream_id,
                "ip"                => $incomingModel->ip,
                "user_agent"        => $incomingModel->user_agent,
                "campaign_id"       => $configOffer->campaign_id,
                ]);

        $prepareRequest = new PrepareGetRequest();
        $prepareRequest->setRequestUrl($this->requestUrl);
        $this->response	= $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();

        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    /**
     * @inheritdoc
     */
    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($responseContent);

        $status = $json->status ?? null;

        if ($status === 'true')
            return [];

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }
}
