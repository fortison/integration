<?php
namespace app\modules\terraleads\handlers\adv\arbitpro;

use app\modules\terraleads\common\objects\PrepareGetRequest;
use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use yii\web\HttpException;
use app\modules\terraleads\common\traits\JsonTrait;

class ArbitproBridgeHandler extends BaseBridgeHandler
{
	use JsonTrait;
	
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestData = [
			'api_key'      => $configUser->api_key,
			'hash'         => $configOffer->hash,
			'phone'        => $incomingModel->phone,
			'fio'          => $incomingModel->name,
			'ip'		   => ($incomingModel->country == $this->getGeoByIp($incomingModel->ip)) ? $incomingModel->ip : $this->getIpByGeo($incomingModel->country) ?? '127.0.0.1',
			'sub1'   	   => $incomingModel->id,
			'sub2'    	   => $incomingModel->web_id,
			'ewid'		   => $incomingModel->web_id,
		];
		
		$this->requestUrl = $config->getUrlOrderAdd()  . '?' . http_build_query($this->requestData);
		
		$prepareRequest = new PrepareGetRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode < 200 || $responseCode > 210)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if (isset($json->success) && $json->success === true)
			return ['partner_order_id' => (string) $json->transaction_id];
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
	
	/**
	 * @param $ip
	 * @return mixed
	 */
	public function getGeoByIp($ip)
	{
		if (!empty($ip)) {
			$data = $this->decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
		}
		
		return $data->geoplugin_countryCode ?? null;
	}
	
	/**
	 * @param $geo
	 * @return string
	 */
	public function getIpByGeo($geo)
	{
		switch ($geo) {
			case 'KZ':
				return '89.36.201.207';
			case 'UZ':
				return '213.230.86.229';
			case 'TR':
				return '78.179.251.61';
			default:
				return '127.0.0.2';
		}
	}
}