<?php

namespace app\modules\terraleads\handlers\adv\magazine;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class MagazineBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config = $this->config;
		$configUser = (object)$config->user->array;
		$configOffer = (object)$config->offer->array;
		
		$incomingModel = $this->incomingModel;
		
		$this->requestUrl = $config->getUrlOrderAdd();
		$this->requestData = [
			'firstname' => $incomingModel->name,
			'lastname' => $incomingModel->name,
			'phone' => $incomingModel->phone,
			'ip' => $incomingModel->ip,
			'webmaster_id' => $incomingModel->web_id,
			'partner_order_id' => $incomingModel->id,
			'product_id' => $configOffer->product_id,
			'price' => $incomingModel->landing_cost,
			'quantity' => $incomingModel->count,
			'currency' => $incomingModel->landing_currency,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		$prepareRequest->setRequestHeaders([
			'Content-Type' => 'application/x-www-form-urlencoded',
		]);
		
		$this->response = $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode = $this->response->getCode();
		$responseContent = $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId = $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
//		var_dump($json);
//		die();
		$status = $json->status ?? null;
		$orderId = $json->id ?? null;
//		print_r($orderId);
//		die();
		if ($status == 'ok') {
			return [
				'partner_order_id' => (string)$orderId,
			];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}
