<?php
namespace app\modules\terraleads\handlers\adv\klixlead;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\web\HttpException;

class KlixleadBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'apiKey' 		=> $configUser->api_key,
			'name' 			=> $incomingModel->name,
			'phoneNumber' 	=> $incomingModel->phone,
			'offerId' 		=> $configOffer->offer_id,
			'countryCode' 	=> $incomingModel->country,
			'baseUrl' 		=> 'https://terraleads.com',
			'referrer' 		=> 'https://terraleads.com/acp/lead',
			'userIp' 		=> $incomingModel->ip,
		];
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		$prepareRequest->setRequestHeaders(['Content-Type'	=> 'application/json']);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode < 200 && $responseCode > 210)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);

		if (!empty($json->type) && $json->type == 'success' && !empty($json->orderId)) {
			return [
				'partner_order_id' => (string)$json->orderId,
			];
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}