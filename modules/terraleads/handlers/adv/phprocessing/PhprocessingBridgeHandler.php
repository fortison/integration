<?php
namespace app\modules\terraleads\handlers\adv\phprocessing;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;
use Yii;

class PhprocessingBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;

		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'__vtrftk' => $configUser->__vtrftk,
			'publicid' => $configUser->publicid,
			'urlencodeenable' => 1,
			'name' => 'Nf',
			'leadstatus' => 'New',
			'leadsource' => 'Nf',
			'description' => "{$configOffer->offerName}",
			'firstname' => $incomingModel->name,
			'lastname' => $incomingModel->name,
			'cf_862' => $configOffer->cf_852,
			'email' => "{$incomingModel->id}@mail.com",
			'phone' => $incomingModel->phone,
			'country' => $configOffer->country,
			'cf_904' => '',
			'cf_944' => 'http://connectcpa.net/terraleads/adv/adv-postback-status?postbackApiKey=002e827687a975a3c325781980308eda&systemOrderId={id}&status={status}',
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$success = $json->success ?? null;
		
		if ($success === true)
			return [
				'partner_order_id' => (string) $json->result];
		
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}