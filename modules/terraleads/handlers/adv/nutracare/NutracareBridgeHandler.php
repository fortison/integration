<?php
namespace app\modules\terraleads\handlers\adv\nutracare;

use app\common\models\OrderApilead;
use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\objects\PreparePostRequest;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use yii\httpclient\Client;
use yii\web\HttpException;

class NutracareBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist()) {
			return $model;
		}
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$args = [$incomingModel, $config];
		
		$this->requestData = [
			'user'   		=> $configUser->user,
			'secret'    	=> $configUser->secret,
			'name'  		=> $incomingModel->name,
			'telephone' 	=> $incomingModel->phone,
			'address_1'  	=> $incomingModel->address ?? '',
			//'city'			=> $incomingModel->address ?? '',
			'geo' 			=> $configOffer->geo ?? $incomingModel->country,
			//'state'  		=> "",
			'postcode' 		=> $incomingModel->zip ?? '',
			'email'  		=> $incomingModel->email ?? '',
			'ip' 			=> $incomingModel->ip,
			'product_id'    => $configOffer->product_id,
			//'clickid' 		=> "",
			'aff_sub1'  	=> $incomingModel->id,
			'aff_sub2' 		=> $incomingModel->getWHash(),
			'aff_sub3'  	=> $incomingModel->getUHash(),
		];
		
		$this->requestUrl = $config->getUrlOrderAdd();
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error) {
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		}
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200) {
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		}
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		if ($json->success === true) {
			return [];
		}
		
		if ($json->success === false) {
			$incomingModel	= $this->incomingModel;
			$apiKey     	= $this->config->user->getApiKey();
			$errorMessage	= $json->errors->msg ?? '';
			
			$terraleadsDataModel = new TerraleadsStatusDataModel($apiKey);
			$terraleadsDataModel->setId($incomingModel->id);
			$terraleadsDataModel->setUserId($incomingModel->user_id);
			$terraleadsDataModel->setComment($errorMessage);
			$terraleadsDataModel->setStatus(OrderApilead::STATUS_TRASH);
			
			$requestUrl		= $this->config->getUrlTerraleadsOrderUpdate();
			$requestUrl     = $requestUrl . '?' . http_build_query(['check_sum' => $terraleadsDataModel->getCheckSum()]);
			$requestData    = $terraleadsDataModel->getStatusData();
			
			$prepareRequest = new PreparePostRequestJson();
			$prepareRequest->setRequestUrl($requestUrl);
			$prepareRequest->setRequestData($requestData);
			$prepareRequest->setResponseFormat(Client::FORMAT_JSON);
			
			$responseModel = $this->statusRequest($prepareRequest);
			
			if (!$responseModel->error && $responseModel->code == 200)
				return ['system_status'=>OrderApilead::STATUS_TRASH];
			
			exit($responseModel->content);
		}
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}