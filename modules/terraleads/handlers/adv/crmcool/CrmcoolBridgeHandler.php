<?php

namespace app\modules\terraleads\handlers\adv\crmcool;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PrepareGetRequestJson;
use yii\web\HttpException;

class CrmcoolBridgeHandler extends BaseBridgeHandler
{
    public function createOrder()
    {
        if ($model = $this->isOrderExist())
            return $model;

        $incomingModel	= $this->incomingModel;

        $this->logTerraleadsRequest($incomingModel->attributes);

        $config			= $this->config;
        $configUser		= (object) $config->user->array;

        $this->requestUrl = $config->getUrlOrderAdd() . '?' . http_build_query([
                'apitoken'  			 => $configUser->token,
                'name'   				 => $incomingModel->name,
                'phonenumber' 	 		 => $incomingModel->phone,
                'address' 	             => $incomingModel->address ?? '',
                'leadorder'              => $incomingModel->id,
                'webmaster_id'           => $incomingModel->web_id,
            ]);

        $prepareRequest = new PrepareGetRequestJson();
        $prepareRequest->setRequestHeaders(['Content-Type' => 'application/json']);
        $prepareRequest->setRequestUrl($this->requestUrl);
        $this->response	= $this->bridgeRequest($prepareRequest);

        if ($this->response->error)
            throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

        $responseCode 		= $this->response->getCode();
        $responseContent	= $this->response->getContent();

        if ($responseCode != 200)
            throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");

        $partnerOrderId		= $this->getOrderAttributes($responseContent);

        return $this->saveOrder($partnerOrderId);
    }

    public function getOrderAttributes($responseContent)
    {
        $json = $this->decode($responseContent);

        $status = $json->status ?? null;
        $order = $json->data->id ?? null;

        if ($status == 'success')
            return ['partner_order_id' => (string)$order];

        throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
    }

}