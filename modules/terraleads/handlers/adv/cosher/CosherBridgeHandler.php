<?php
namespace app\modules\terraleads\handlers\adv\cosher;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;

class CosherBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;

		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;

		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'first_name' => $incomingModel->name,
			'last_name' => '.',
			'phone' => $incomingModel->phone,
			'iso' => $incomingModel->country,
			'ip' => $incomingModel->ip,
			'external_id' => (int) $incomingModel->id,
			'products' => [
				[
					'article' => $configOffer->article,
					'name' => $configOffer->name,
					'price' => (int) $incomingModel->landing_cost,
					'quantity' => (int) $incomingModel->count,
				]
			]
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		$prepareRequest->setRequestHeaders([
			'Content-Type' => 'application/x-www-form-urlencoded',
			'Authorization' => "Bearer {$configUser->apiKey}",
		]);

		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");

		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$error = $json->error ?? null;
		$orderId = $json->data->id ?? null;
		
		if (!$error && $orderId) {
			return [
				'partner_order_id' => (string) $orderId,
			];
		}
		
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}