<?php
namespace app\modules\terraleads\handlers\adv\scale;

use app\modules\terraleads\common\adv\handlers\base\BaseBridgeHandler;
use app\modules\terraleads\common\objects\PreparePostRequest;
use yii\web\HttpException;
use Yii;

class ScaleBridgeHandler extends BaseBridgeHandler
{
	public function createOrder()
	{
		if ($model = $this->isOrderExist())
			return $model;
		
		$this->logTerraleadsRequest($this->incomingModel->attributes);
		
		$config			= $this->config;
		$configUser		= (object) $config->user->array;
		$configOffer	= (object) $config->offer->array;
		
		$incomingModel	= $this->incomingModel;
		
		$this->requestUrl	= $config->getUrlOrderAdd();
		$this->requestData	= [
			'token' 				=> $configUser->token,
			'TakenOfferID' 			=> $configOffer->TakenOfferID,
			'CustomerName' 			=> $incomingModel->name,
			'CustomerPhone' 		=> $incomingModel->phone,
			'CustomerQuantity' 		=> 1,
			'CustomerIP' 			=> $incomingModel->ip,
			'CustomerUserAgent' 	=> $incomingModel->user_agent,
			'Custom_c1' 			=> $incomingModel->id,
			'Custom_c2' 			=> $incomingModel->web_id,
		];
		
		$prepareRequest = new PreparePostRequest();
		$prepareRequest->setRequestUrl($this->requestUrl);
		$prepareRequest->setRequestData($this->requestData);
		
		$this->response	= $this->bridgeRequest($prepareRequest);
		
		if ($this->response->error)
			throw new HttpException('400', "{$this->response->error} [USER:{$incomingModel->user_id}] [OFFER:{$incomingModel->offer_id}] [LEAD:{$incomingModel->id}]");
		
		$responseCode 		= $this->response->getCode();
		$responseContent	= $this->response->getContent();
		
		if ($responseCode != 200)
			throw new HttpException('400', "Partner error! Response code: {$responseCode}. Response: {$responseContent}");
		
		$partnerOrderId		= $this->getOrderAttributes($responseContent);
		
		return $this->saveOrder($partnerOrderId);
	}
	
	/**
	 * @inheritdoc
	 */
	public function getOrderAttributes($responseContent)
	{
		$json = $this->decode($responseContent);
		
		$success = $json->success ?? null;
		
		if ($success === true)
			return [
				'partner_order_id' => (string) $json->result];
		
		
		throw new HttpException('400', "Wrong partnerOrderId. {{$responseContent}}");
	}
}