<?php

namespace app\modules\terraleads\common\traits;

trait XmlTrait
{
    public function xmlDecode(string $xml)
    {
        $xml = simplexml_load_string($xml);
        return json_encode($xml);
    }
}