<?php

namespace app\modules\terraleads\common\traits;

use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PrepareRequest;
use yii\httpclient\Client;

/**
 * Trait RequestTrait
 * @package \app\modules\terraleads\common\traits
 * @property \app\modules\terraleads\common\objects\Config $config
 */
trait RequestTrait
{
	/**
	 * Загальний метод, що здійснює мостові запити.
	 *
	 * @param PrepareRequest $prepareRequest
	 * @return ResponseModel
	 */
	public function bridgeRequest(PrepareRequest $prepareRequest)
	{
		$responseModel = $this->makeRequest($prepareRequest);

		$requestUrl = $prepareRequest->getRequestUrl();
		$requestData = $prepareRequest->getRequestData();
		$requestHeader = $prepareRequest->getRequestHeaders();

		$this->logBridgeRequest($responseModel, $requestUrl, $requestData, $requestHeader);

		return $responseModel;
	}
	
	/**
	 * Загальний метод, що здійснює статусні запити.
	 *
	 * @param PrepareRequest $prepareRequest
	 * @return ResponseModel
	 */
	public function statusRequest(PrepareRequest $prepareRequest)
	{
		$responseModel = $this->makeRequest($prepareRequest);
		
		$requestUrl = $prepareRequest->getRequestUrl();
		$requestData = $prepareRequest->getRequestData();
		
		$this->logStatusRequest($responseModel, $requestUrl, $requestData);
		
		return $responseModel;
	}
	
	/**
	 * Загальний метод, що здійснює запит.
	 *
	 * @param PrepareRequest $prepareRequest
	 * @return ResponseModel
	 */
	public function makeRequest(PrepareRequest $prepareRequest)
	{
		$start = microtime(true);
		
		$requestUrl		= $prepareRequest->getRequestUrl();
		$requestData	= $prepareRequest->getRequestData();
		$requestContent	= $prepareRequest->getRequestContent();
		$requestMethod	= $prepareRequest->getRequestMethod();
		$requestFormat	= $prepareRequest->getRequestFormat();
		$requestHeaders	= $prepareRequest->getRequestHeaders();
		$requestCookies	= $prepareRequest->getRequestCookies();
		$requestTimeout	= $prepareRequest->getRequestTimeout();
		
		$responseFormat = $prepareRequest->getResponseFormat();
		
		$client = new Client([
			'transport' => 'yii\httpclient\CurlTransport',
			'responseConfig' => [
				'format' => $responseFormat,
			],
		]);
		
		try {
			$response = $client->createRequest()
				->setUrl($requestUrl)
				->setMethod($requestMethod)
				->setFormat($requestFormat)
				->setData($requestData)
				->setContent($requestContent)
				->setHeaders($requestHeaders)
				->setCookies($requestCookies)
				->setOptions([
					'timeout'       => $requestTimeout,
					'sslVerifyPeer' => false,
					'sslVerifyHost' => false,
					'followLocation' => true,
				])
				->send();

			$code       = $response->getStatusCode();
			$content    = $response->getContent();
//			$data		= $response->getData();
			$data		= "";
			$error		= "";
			$cookie		= $response->getCookies();
			
		} catch (\Exception $e) {
			$error 		= $e->getMessage();
			$code		= "";
			$content 	= "";
			$data		= "";
			$cookie 	= [];
		}
		
		$time       = microtime(true) - $start;
		
		$responseModel = new ResponseModel();
		$responseModel->setCode($code);
		$responseModel->setContent($content);
		$responseModel->setData($data);
		$responseModel->setTime($time);
		$responseModel->setMethod($requestMethod);
		$responseModel->setError($error);
		$responseModel->setCookie($cookie);
//        print_r($responseModel);die();
		return $responseModel;
	}
	
	/**
	 * Метод, що здійснює запит за допомогую curl
	 *
	 * @param PrepareRequest $prepareRequest
	 * @return ResponseModel
	 */
	public function makeCurlRequest(PrepareRequest $prepareRequest)
	{
		$start = microtime(true);
		
		$requestUrl		= $prepareRequest->getRequestUrl();
		$requestData	= $prepareRequest->getRequestData();
		$requestContent	= $prepareRequest->getRequestContent();
		$requestMethod	= $prepareRequest->getRequestMethod();
		$requestFormat	= $prepareRequest->getRequestFormat();
		$requestHeaders	= $prepareRequest->getRequestHeaders();
		$requestCookies	= $prepareRequest->getRequestCookies();
		$requestTimeout	= $prepareRequest->getRequestTimeout();
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $requestUrl);
		curl_setopt($ch, CURLOPT_POST,true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $requestData);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_SSLVERSION, 3);
		curl_setopt($ch, CURLOPT_TIMEOUT,$requestTimeout);
		
		if( !empty($requestHeaders) ){
			$http_headers = array();
			foreach( $requestHeaders as $header_name => $header_value ){
				$http_headers[] = $header_name.': '.$header_value;
			}
			curl_setopt($ch, CURLOPT_HTTPHEADER, $http_headers);
		}
		$result = curl_exec($ch);
		$curl_error = curl_error($ch);
		$http_code  = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close ($ch);
		
		$response = array(
			'error'    => $curl_error,
			'httpCode' => $http_code,
			'result'   => $result,
		);
		
		$time = microtime(true) - $start;
		
		$responseModel = new ResponseModel();
		$responseModel->setCode($response['httpCode']);
		$responseModel->setData($requestData);
		$responseModel->setContent($response['result']);
		$responseModel->setTime($time);
		$responseModel->setMethod($requestMethod);
		$responseModel->setError($response['error']);
		
		$this->logBridgeRequest($responseModel, $requestUrl, $requestData, $requestHeaders);
		
		return $responseModel;
	}
}
