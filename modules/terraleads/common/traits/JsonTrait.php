<?php

namespace app\modules\terraleads\common\traits;

use Yii;
use yii\helpers\Json;

trait JsonTrait
{
	/**
	 * Кодує php масив в валідну JSON структуру.
	 *
	 * @param $array
	 * @param int $option
	 * @return string
	 */
	public function encode($array, $option = JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT)
	{
		return Json::encode($array, $option);
	}
	
	/**
	 * Декодує JSON структуру і повертає її валідний php масив.
	 *
	 * @param $data
	 * @param bool $asArray
	 * @return mixed
	 */
	public function decode($data, $asArray = false)
	{
		return Json::decode($data, $asArray);
	}
}