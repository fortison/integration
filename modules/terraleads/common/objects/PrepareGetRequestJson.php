<?php
/**
 * Created by PhpStorm.
 * User: nagard
 * Date: 09.02.18
 * Time: 15:12
 */

namespace app\modules\terraleads\common\objects;

use yii\httpclient\Client;

class PrepareGetRequestJson extends PrepareGetRequest
{
	public function init()
	{
		parent::init();
		
		$this->requestFormat = Client::FORMAT_JSON;
	}
}