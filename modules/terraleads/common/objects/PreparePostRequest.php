<?php
/**
 * Created by PhpStorm.
 * User: nagard
 * Date: 09.02.18
 * Time: 15:12
 */

namespace app\modules\terraleads\common\objects;

class PreparePostRequest extends PrepareRequest
{
	public function init()
	{
		$this->requestMethod = 'post';
	}
}