<?php
/**
 * Created by PhpStorm.
 * User: nagard
 * Date: 15.05.18
 * Time: 11:10
 */

namespace app\modules\terraleads\common\exeptions;


use yii\base\UserException;

class NoLogException extends UserException
{
	/**
	 * Constructor.
	 * @param int $status HTTP status code, such as 404, 500, etc.
	 * @param string $message error message
	 * @param int $code error code
	 * @param \Exception $previous The previous exception used for the exception chaining.
	 */
	public function __construct($status, $message = null, $code = 0, \Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}