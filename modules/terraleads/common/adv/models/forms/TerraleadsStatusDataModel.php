<?php
/**
 * Created by PhpStorm.
 * User: nagard
 * Date: 13.11.17
 * Time: 12:47
 */

namespace app\modules\terraleads\common\adv\models\forms;

use Exception;
use yii\base\BaseObject;
use yii\helpers\Json;

/**
 * Class TerraleadsStatusDataModel
 *
 * @package app\modules\terraleads\common\models\forms
 *
 * @property integer $id Унікальний ідентифікатор замовлення в системі Terraleads
 * @property integer $status Статус замовлення в системі Terraleads
 * @property integer $user_id Ідентифікатор користувача в системі Terraleads
 * @property integer $comment Комендар до замовлення в системі Terraleads
 * @property integer $partnerStatus Статус замовлення в системі партнера
 * @property integer $apiKey api_key користувача в системі Terraleads
 */
class TerraleadsStatusDataModel extends BaseObject
{
	public $id;
	public $user_id;
	public $status;
	public $comment;
	
	public $partnerId;
	public $partnerStatus;
	
	public $apiKey;


	public function __construct($apiKey='', array $config = [])
	{
		parent::__construct($config);
		
		if (!$apiKey)
			throw new Exception("Sorry, but apiKey not set!");
		
		$this->apiKey = $apiKey;
	}

	public function setId($value)
	{
		$this->id = $value;
	}

	public function setStatus($value)
	{
		$this->status = $value;
	}

	public function setUserId($value)
	{
		$this->user_id = $value;
	}

	public function setComment($value)
	{
		$this->comment = $value;
	}
	
	/**
	 * В подальшому треба видалити та перейти на Partner
	 *
	 * @param $value
	 */
	public function setSourceStatus($value)
	{
		$this->setPartnerStatus($value);
	}
	
	public function setPartnerId($value)
	{
		$this->partnerId = $value;
	}

	public function setPartnerStatus($value)
	{
		$this->partnerStatus = $value;
	}
	
	/**
	 * Формуємо чексуму ліда
	 *
	 * @return string
	 * @throws Exception
	 */
	public function getCheckSum()
	{
		return sha1(Json::encode($this->getStatusData(), 0) . $this->apiKey);
	}
	
	/**
	 * Повертає підготовлений масив, що годиться, до відправлення в систему Terraleads
	 *
	 * @return array
	 */
	public function getStatusData()
	{
		return [
			'user_id' => $this->user_id,
			'data' => [
				'id'        => $this->id,
				'status'    => $this->status,
				'comment'   => $this->comment,
			],
		];
	}
	
	/**
	 * Повертає підготовлений масив із всіма властивостями
	 *
	 * @return array
	 */
	public function getProperties()
	{
		return [
			'id'			=> $this->id,
			'status'		=> $this->status,
			'comment'		=> $this->comment,
			'partnerId'		=> $this->partnerId,
			'partnerStatus'	=> $this->partnerStatus,
		];
	}
}