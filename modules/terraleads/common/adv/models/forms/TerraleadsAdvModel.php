<?php

namespace app\modules\terraleads\common\adv\models\forms;

use nahard\log\helpers\Log;

class TerraleadsAdvModel extends \yii\base\Model
{
	public $id;
	public $campaign_id;
	public $name;
	public $country;
	public $phone;
	public $email;
	public $count;
	public $zip;
	public $city;
	public $region;
	public $address;
	public $building;
	public $apartment;
	public $user_comment;
	public $tz;
	public $cost;
	public $cost_delivery;
	public $landing_cost;
	public $user_id;
	public $web_id;
	public $stream_id;
	public $product_id;
	public $offer_id;
	public $ip;
	public $user_agent;
	public $extra_data;
	public $offer_type;
	public $landing_currency;
	public $test;
	public $check_sum;
	
	public $api_key;
	
	public function rules()
	{
		return [
			['id', 				'string', 'max' => 255],
			['id', 				'required'],
			
			['campaign_id', 	'integer'],
			['campaign_id', 	'required'],
			
			['name',			'string', 'max' => 255],
			['country',			'string', 'max' => 255],
			['phone',			'string'],
			['email',			'string'],
			['count',			'integer'],
			['zip',				'string'],
			['city',			'string'],
			['region',			'string'],
			['tz',				'integer'],
			['address',			'string'],
			['building',		'string'],
			['apartment',		'string'],
			['user_comment',	'safe'],
			['cost',			'number'],
			['cost_delivery',	'number'],
			['landing_cost',	'number'],
			
			['user_id',			'integer'],
			['user_id', 		'required'],
			
			['web_id',			'integer'],
			['stream_id',		'integer'],
			['product_id',		'integer'],
			
			['offer_id',		'integer'],
			['offer_id',		'required'],
			
			['ip',				'string', 'max' => 255],
			['user_agent',		'string'],
			['extra_data',		'string'],
			['offer_type',		'string'],
			['landing_currency','string', 'max' => 255],

			['test',			'integer'],
			
			['check_sum', 		'string', 'length' => 40],
			['check_sum', 		'required'],
		];
	}
	
	public function setApiKey($apiKey)
	{
		$this->api_key = $apiKey;
	}
	
	/**
	 * Перевіряє чи не було ніяких змін в отриманих даних.
	 *
	 * @param string $apiKey api_key користувача в системі Terraleads.
	 * @return bool
	 */
    public function isCorrectCheckSum($apiKey)
    {
        if( !$this->hasErrors() )
        {
        	$this->setApiKey($apiKey);
        	
            $check_sum = $this->getCheckSum();
            
            if( $check_sum === $this->check_sum )
            	return true;
            return false;
            
        }
    }
	
	/**
	 * Повертає хеш суму основних даних.
	 *
	 * @return string
	 */
    public function getCheckSum()
    {
        return sha1($this->id . $this->campaign_id . $this->name . $this->phone . $this->country . $this->cost . $this->api_key);
    }
	
	/**
	 * Перевіряє, чи є поточне замовлення тестовою перевіркою.
	 *
	 * @return bool true якщо це тестова заявка, false в іншому випадку
	 */
	public function isTest()
	{
		return $this->test == 1;
	}
	
	/**
	 * Отримуємо web_id hash
	 * Який ми можемо передавати рекламодавцю,
	 * щоб приховати істиний іденитифікатор веба
	 *
	 * @return string
	 */
	public function getWHash()
	{
		return $this->getHashSalt('w', $this->web_id);
	}
	
	/**
	 *
	 * @param string|null $allowedChars
	 * @return string
	 */
	public function getClearPhone($allowedChars = null)
	{
		$allowedChars = $allowedChars ?? '';
		$escapedAllowedChars = preg_quote($allowedChars, '/');
		$pattern = '/[^0-9' . $escapedAllowedChars . ']/';
		
		return preg_replace($pattern, '', $this->phone);
	}
	
	/**
	 * Отримуємо user_comment hash
	 * Який ми можемо передавати рекламодавцю,
	 * щоб приховати істиний іденитифікатор веба
	 *
	 * @return string
	 */
	public function getUHash()
	{
		return $this->getHashSalt('u', $this->user_comment);
	}
	
	/*
	 * Функція, що шифрує id наступним методом
	 * (Перший символ (w|u)){Сіль, що базована на адентифікаторі}{ідентифікатор в оберненій послідовності}
	 */
	public function getHashSalt($firstChar, $id)
	{
		if (!$id)
			return null;
		
		$sh1 = sha1($id);
		
		if (preg_match('/[a-z]/', $id))
			$salt = preg_replace('/[0-9]/', '', $sh1);
		else
			$salt = preg_replace('/[a-z]/', '', $sh1);
		
		$salt = substr($salt, 0, 2);
		
		if (strlen($salt) < 2)
			Log::warning("Ідентифікатор {{$id}} має занадто коротку сіль {{$salt}} базовану на {{$sh1}}");
		
		$revId = strrev($id);
		
		return "{$firstChar}{$salt}{$revId}";
	}
}
