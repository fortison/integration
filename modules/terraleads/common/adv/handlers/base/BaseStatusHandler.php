<?php

namespace app\modules\terraleads\common\adv\handlers\base;

use app\common\models\OrderApilead;
use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\objects\PreparePostRequestJson;
use app\modules\terraleads\common\traits\JsonTrait;
use app\modules\terraleads\common\traits\LogTrait;
use app\modules\terraleads\common\traits\XmlTrait;
use app\modules\terraleads\common\traits\RequestTrait;
use Exception;

/**
 * Базовий клас від якого можуть наслідуватися всі обробники статусів кожної інтеграція
 *
 * @property \app\modules\terraleads\common\adv\objects\Config $config Повна конфігурація для поточного замовлення
 * @property \app\common\models\OrderApilead $orderModel Модель поточного замовлення
 */

abstract class BaseStatusHandler extends \yii\base\BaseObject
{
	use JsonTrait;
	use RequestTrait;
	use LogTrait;
	use XmlTrait;
	
	public $config;
	public $orderModel;
	
	/**
	 * Метод відповідає за повну обробку і оновлення статусу замовлення.
	 *
	 * Основна метод, що буде викликаний в контролері (загальному обробнику).
	 * На цьому методі висить вся логіка обробки замовлень:
	 * * збереження актуальної інформації замовлення у себе.
	 * * надсилання актуального статусу в систему Terraleads.
	 *
	 * Зазвичай, алгоритм обробки замовлення наступний:
	 *
	 * * Одержати відповідь від рекламодавця за допомогою методу $this->makeStatusRequest()
	 * * Сформувати модель, котру буде відправлено в систему Terraleads за допомогою методу $this->getSystemStatusData()
	 * * Розпочати транзакцію
	 * * Оновити статус у себе в базі за допомогою методу $orderModel->updateOrder()
	 * * Оновити статус замовлення та його коментар в системі Terraleads за допомогою методу $this->sendOrderStatusToTerraleads()
	 *
	 * @param bool $force
	 * @return TerraleadsStatusDataModel
	 */
	abstract function updateStatusInSystem($force=false);
	
	/**
	 * Метод робить запит до рекламодавця і повертає від нього не опрацьовану відповідь у вигляді наповненої моделі ResponseModel.
	 *
	 * @return ResponseModel
	 */
	abstract function makeStatusRequest();
	
	/**
	 * Запит для отримання викупу по визначеному замовленню.
	 * Метод робить запит до рекламодавця і повертає від нього не опрацьовану відповідь у вигляді наповненої моделі ResponseModel.
	 * Зазвичай статус викупу міститься у відповіді на запит отримання звичайного статусу, але в разі,
	 * якщо для отримання статусу викупу необхідно робити окремий запит, доведеться перевизначити цей метод в статусному обробкику.
	 *
	 * @return ResponseModel
	 */
	public function makeRansomStatusRequest():ResponseModel
	{
		return $this->makeStatusRequest();
	}
	
	/**
	 * Повертає модель TerraleadsStatusDataModel, дані з якої будуть відправлені для оновлення статусу замовлення в системі Terraleads.
	 *
	 * @param ResponseModel $responseModel
	 * @return TerraleadsStatusDataModel
	 */
	abstract function getSystemStatusData(ResponseModel $responseModel);
	
	
	/**
	 * Повертає модель TerraleadsStatusDataModel, дані з якої будуть відправлені для оновлення статусу замовлення в системі Terraleads.
	 *
	 * @param ResponseModel $responseModel
	 * @return TerraleadsStatusDataModel
	 */
	public function getRansomStatusData(ResponseModel $responseModel)
	{
		return new TerraleadsStatusDataModel(true, [
			'comment' => "ERROR. Ransom handler not implemented for '{$this->orderModel->integration_name}'",
		]);
	}
	
	/**
	 * Повертає актуальний статус замовлення, котрий підходить для відправлення в систему Terraleads.
	 *
	 * @param string|integer $partnerStatus
	 * @return string
	 */
	public function getTerraleadsStatusName($partnerStatus)
	{
		$statuses = $this->config->getStatuses();
		
		$expect		= $statuses[OrderApilead::STATUS_EXPECT]	?? [];
		$reject		= $statuses[OrderApilead::STATUS_REJECT]	?? [];
		$trash		= $statuses[OrderApilead::STATUS_TRASH]		?? [];
		$confirm	= $statuses[OrderApilead::STATUS_CONFIRM]	?? [];
		
		if (array_key_exists($partnerStatus, $expect))
			return OrderApilead::STATUS_EXPECT;
		
		elseif (array_key_exists($partnerStatus, $reject))
			return OrderApilead::STATUS_REJECT;
		
		elseif (array_key_exists($partnerStatus, $trash))
			return OrderApilead::STATUS_TRASH;
		
		elseif (array_key_exists($partnerStatus, $confirm))
			return OrderApilead::STATUS_CONFIRM;
		
		else
			return OrderApilead::STATUS_UNKNOWN;
	}
	
	/**
	 * Повертає актуальний статус замовлення, котрий підходить для відправлення в систему Terraleads.
	 *
	 * @param string|integer $partnerRansomStatus
	 * @return string
	 */
	public function getRansomStatusName($partnerRansomStatus)
	{
		$ransomStatuses = $this->config->getRansomStatuses();
		
		$confirm	= $ransomStatuses[OrderApilead::RANSOM_CONFIRM]	?? [];
		$reject		= $ransomStatuses[OrderApilead::RANSOM_REJECT]	?? [];
		$expect		= $ransomStatuses[OrderApilead::RANSOM_EXPECT]	?? [];
		
		if (array_key_exists($partnerRansomStatus, $reject))
			return OrderApilead::RANSOM_REJECT;
		
		elseif (array_key_exists($partnerRansomStatus, $expect))
			return OrderApilead::RANSOM_EXPECT;
		
		elseif (array_key_exists($partnerRansomStatus, $confirm))
			return OrderApilead::RANSOM_CONFIRM;
		
		else
			return OrderApilead::RANSOM_UNKNOWN;
	}
	
	/**
	 * Метод повертає статичний коментар, котрий прив'язаний до ідентифікатору статуса.
	 * Цей метод потрібен, якщо рекламодавець не віддає коментарів.
	 * Тому це залишається єдиним способом отримання коментарів.
	 *
	 * @param $statusName
	 * @throws Exception
	 * @return string
	 */
	public function getCommentByStatus($systemStatus, $partnetStatus)
	{
		throw new Exception('Перевизнач метод getCommentByStatus() у себе в обробнику!');
	}

	/**
	 * Відправляє актуальний статус до системи Terraleads.
	 *
	 * В майбутньому треба буде ретельніше продумати логіку реагування на всілякі
	 * повернуті від Terraleads помилки.
	 *
	 * @param TerraleadsStatusDataModel $terraleadsStatusDataModel
	 * @param bool $needSendStatus Пташка, чи потрібно надсилати запит до системи Terraleads
	 * @return ResponseModel|null
	 * @throws Exception якщо не вдалося відправити актуальний статус.
	 */
	public function sendOrderStatusToTerraleads(TerraleadsStatusDataModel $terraleadsStatusDataModel, $needSendStatus=true)
	{
		$orderModel			= $this->orderModel;
		$myId				= $orderModel->id ?? null;
		$terraleadsUserId	= $orderModel->system_user_id ?? null;
		$partnerId			= $orderModel->partner_order_id ?? null;
		$terraleadsId		= $orderModel->system_order_id ?? null;
		$terraleadsOfferId	= $orderModel->offer_id ?? null;
		
		if (!$needSendStatus) {
			$this->writeStatus("[DO NOT NEED SEND STATUS TO TERRALEADS: myId={$myId}, partnerId={$partnerId}, terraleadsId={$terraleadsId}]\n\n\n");
			return null;
		}
		
		$checkSum		= $terraleadsStatusDataModel->getCheckSum();
		$requestData    = $terraleadsStatusDataModel->getStatusData();
		$requestUrl     = $this->config->getUrlTerraleadsOrderUpdate() . '?' . http_build_query(['check_sum'=>$checkSum]);
		
		$prepareRequest = new PreparePostRequestJson();
		$prepareRequest->setRequestUrl($requestUrl);
		$prepareRequest->setRequestData($requestData);
		$prepareRequest->setRequestTimeout(60);
		
		$responseModel		= $this->statusRequest($prepareRequest);
		$responseCode		= $responseModel->getCode();
		$responseContent	= $responseModel->getContent();
		
		$json = json_decode($responseContent);
		$status = $json->status ?? null;
		
		if ($status === 'ok') {
			return $responseModel;
		} else {
			throw new OrderErrorException($myId, "Update remote terraleads status error. [user:{$terraleadsUserId}] [offer:{$terraleadsOfferId}] [terraleadsId:{$terraleadsId}] [partnerId:{$partnerId}] [id:{$myId}]. [RESPONSE:{$responseContent}]");
		}
	}
}