<?php

namespace app\modules\terraleads\common\adv\handlers\base;

use app\common\models\OrderApilead;
use app\modules\terraleads\common\models\forms\ResponseModel;
use app\modules\terraleads\common\adv\objects\Config;
use app\modules\terraleads\common\traits\LogTrait;
use app\modules\terraleads\common\traits\RequestTrait;
use app\modules\terraleads\common\traits\JsonTrait;
use yii\httpclient\Client;
use yii\web\HttpException;

/**
 * Created by PhpStorm.
 * User: nagard
 * Date: 02.11.17
 * Time: 15:48
 *
 * @property \app\modules\terraleads\common\adv\models\forms\TerraleadsAdvModel $incomingModel Дані замовлення, що прийшли від системи Terraleads
 * @property Config $config Головна конфігурація поточного замовлення
 * @property ResponseModel $response Об'єкт запиту
 */
abstract class BaseBridgeHandler extends \yii\base\BaseObject
{
	use RequestTrait;
	use LogTrait;
	use JsonTrait;
	
	public $requestUrl;
	public $requestData;
	public $response;
	
	public $config;
	public $incomingModel;
	
	/**
	 * Підготовляє та надсилає замовлення до партнера.
	 *
	 * Отримує від нього відповідь, та зберігає всю наявну інформацію до загальної БД.
	 *
	 * @return integer|string Має повернути ідентифікатор замовлення в системі партнера.
	 */
	abstract public function createOrder();
	
	/**
	 * Повертає ідентифікатор замовлення на стороні партнера.
	 * Потрібен, щоб красиво реалізувати інтеграції рекламодавця 3437
	 * котрий є відгалуженням змичайного leadvertex.
	 *
	 * @param string $responseContent Тіло запиту.
	 * @return string|integer Ідентифікатор замовлення на стороні партнера
	 * @throws HttpException
	 */
	abstract public function getOrderAttributes($responseContent);
	
	/**
	 * Зберігає поточне замовлення з усіма даними, і нашими, і партнера до нашої загальної бази.
	 *
	 * @param $attributes
	 * @return OrderApilead
	 * @throws HttpException Якщо замовлення не вдалося зберегти
	 */
	function saveOrder($attributes)
	{
		$incomingModel 									= $this->incomingModel;
		$orderModel 									= new OrderApilead;

		$orderModel->integration_name					= $this->config->integrationName;
		$orderModel->integration_module					= $this->config->moduleName;
		
		$orderModel->offer_id							= $incomingModel->offer_id;
		$orderModel->system_order_id					= $incomingModel->id;
		$orderModel->system_user_id						= $incomingModel->user_id;
		
		$orderModel->incoming_data						= $this->encode($incomingModel);
		
		$orderModel->incoming_field_id				    = $incomingModel->id ?? null;
		$orderModel->incoming_field_campaign_id		    = $incomingModel->campaign_id ?? null;
		$orderModel->incoming_field_name				= $incomingModel->name ?? null;
		$orderModel->incoming_field_country			    = $incomingModel->country ?? null;
		$orderModel->incoming_field_phone				= $incomingModel->phone ?? null;
		$orderModel->incoming_field_tz			    	= $incomingModel->tz ?? null;
		$orderModel->incoming_field_address			    = $incomingModel->address ?? null;
		$orderModel->incoming_field_user_comment		= $incomingModel->user_comment ?? null;
		$orderModel->incoming_field_cost				= $incomingModel->cost ?? null;
		$orderModel->incoming_field_cost_delivery		= $incomingModel->cost_delivery ?? null;
		$orderModel->incoming_field_landing_cost		= $incomingModel->landing_cost ?? null;
		$orderModel->incoming_field_user_id			    = $incomingModel->user_id ?? null;
		$orderModel->incoming_field_web_id			    = $incomingModel->web_id ?? null;
		$orderModel->incoming_field_stream_id			= $incomingModel->stream_id ?? null;
		$orderModel->incoming_field_product_id		    = $incomingModel->product_id ?? null;
		$orderModel->incoming_field_offer_id			= $incomingModel->offer_id ?? null;
		$orderModel->incoming_field_ip				    = $incomingModel->ip ?? null;
		$orderModel->incoming_field_user_agent		    = $incomingModel->user_agent ?? null;
		$orderModel->incoming_field_landing_currency	= $incomingModel->landing_currency ?? null;
		$orderModel->incoming_field_check_sum			= $incomingModel->check_sum ?? null;
		
		$orderModel->request_data						= $this->encode($this->requestData);
		$orderModel->request_url						= $this->requestUrl;
		$orderModel->request_timeout					= $this->response->getTime();

		$orderModel->response_code						= $this->response->getCode();
		$orderModel->response_result					= $this->response->getContent();
		$orderModel->response_error						= $this->response->getError();
		
		$orderModel->load($attributes, '');
		
		if ($orderModel->save())
			return $orderModel;
			
		throw new HttpException(400, "Order_id {$orderModel->partner_order_id}=>{$incomingModel->id} not save to Terraleads DB" . $this->encode($orderModel->getErrors()));
	}
	
	/**
	 * Пепевірка, чи вже існує, поточне замовлення.
	 * Якщо замовлення існує буде повернуто його модель.
	 *
	 * @return OrderApilead|null
	 */
	public function isOrderExist()
	{
		return OrderApilead::getUserOrder($this->incomingModel->user_id, $this->incomingModel->id, $this->config->moduleName);
	}
	
	/**
	 * @param $responseString
	 * @param $search
	 * @return bool
	 */
	public function checkResponseInvalidPhone($responseString, $search)
	{
		if (stripos($responseString, $search) !== false) {
			
			$this->logTerraleadsRequest(['responseString' => $responseString]);
			
			try {
				$config = (object)$this->config->user->array;
				
				$data = [
					'id' => $this->incomingModel->id,
					'status' => 'trash',
					'comment' => 'auto trash status from connector',
				];
				
				$data['check_sum'] = sha1($data['id'] . $data['status'] . $data['comment'] . $config->terraleadsApiKey);
				
				$url = "http://tl-api.com/api/lead/update";
				
				$this->logTerraleadsRequest(['Status Request Url' => $url, 'Status Request Data' => $data]);
				
				$client = new Client();
				$response = $client->createRequest()
					->setMethod('POST')
					->addHeaders(['content-type' => 'application/json'])
					->setUrl($url)
					->setContent($this->encode($data))
					->send();
				
				$this->logTerraleadsRequest(['terraResponse' => $response->getContent()]);
				
				return true;
			
			} catch (\Exception $e) {
				return false;
			}
		}
		
		return false;
	}
}