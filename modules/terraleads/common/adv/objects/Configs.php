<?php

namespace app\modules\terraleads\common\adv\objects;

use yii\base\BaseObject;
use Exception;

/**
 * Class Configs
 * @package app\modules\terraleads\common\adv\objects
 *
 * @property array $configs Містить секцію configs відносно ієрархії поточного блоку.
 * @property array $array Містить ввесь поточний блок конфігурації в залежності від ієрархії.
 */
class Configs extends BaseObject
{
	const SECTION_CONFIGS						= 'configs';
	const SECTION_OFFERS						= 'offers';
	const SECTION_STATUSES						= 'statuses';
	const SECTION_SUB_STATUSES					= 'subStatuses';
	const SECTION_RANSOM_STATUSES				= 'ransom';
	
	const CONFIG_STATUS_HANDLER					= 'statusHandler';
	const CONFIG_BRIDGE_HANDLER					= 'bridgeHandler';
	
	const CONFIG_STATUS_ENABLED					= 'statusEnabled';
	const CONFIG_BRIDGE_ENABLED					= 'bridgeEnabled';
	
	const CONFIG_BRIDGE_REQUEST_TIMEOUT			= 'bridgeRequestTimeout';
	const CONFIG_STATUS_REQUEST_TIMEOUT			= 'statusRequestTimeout';
	
	const CONFIG_URL_ORDER_ADD					= 'urlOrderAdd';
	const CONFIG_URL_ORDER_INFO					= 'urlOrderInfo';
	
	const CONFIG_URL_TERRALEADS_ORDER_UPDATE 	= 'urlTerraleadsOrderUpdate';
	
	const CONFIG_BRAKE_LOG_FOLDER				= 'brakeLogFolder';
	
	const CONFIG_INITIAL_DELAY					= 'initialDelay';
	const CONFIG_FIRST_DAY_DELAY				= 'firstDayDelay';
	const CONFIG_SECOND_DAY_DELAY				= 'secondDayDelay';
	const CONFIG_OTHER_DAY_DELAY				= 'otherDayDelay';
	
	public $array;
	public $configs;
	
	public function init()
	{
		parent::init();
		
		if (!is_array($this->array))
			throw new Exception('$this->array must be array!');
		
		if (isset($this->array[self::SECTION_CONFIGS]))
			$this->configs = $this->array[self::SECTION_CONFIGS];
	}
	
	/**
	 * Визначає, чи здійснювати опитування статусів.
	 *
	 * @return bool
	 */
	public function isStatusEnabled()
	{
		return $this->configs[self::CONFIG_STATUS_ENABLED] ?? true;
	}
	
	/**
	 * Визначає, чи активний міст в поточному блоці
	 *
	 * @return bool
	 */
	public function isBridgeEnabled()
	{
		return $this->configs[self::CONFIG_BRIDGE_ENABLED] ?? true;
	}
	
	/**
	 * Повертає поточний максимальний час мостового запиту.
	 *
	 * @return mixed|null
	 */
	public function getBridgeRequestTimeout()
	{
		return $this->configs[self::CONFIG_BRIDGE_REQUEST_TIMEOUT] ?? null;
	}
	
	/**
	 * Повертає поточний максимальний час статусного запиту.
	 *
	 * @return mixed|null
	 */
	public function getStatusRequestTimeout()
	{
		return $this->configs[self::CONFIG_STATUS_REQUEST_TIMEOUT] ?? null;
	}
	
	/**
	 * Повертає шлях до обробкика опитувача статусів.
	 *
	 * @return mixed|null
	 */
	public function getStatusHandler()
	{
		return $this->configs[self::CONFIG_STATUS_HANDLER] ?? null;
	}
	
	/**
	 * Повертає шлях до мостового обробника.
	 *
	 * @return mixed|null
	 */
	public function getBridgeHandler()
	{
		return $this->configs[self::CONFIG_BRIDGE_HANDLER] ?? null;
	}
	
	/**
	 * Повертає посилання на апі, що відповідає за оновлення стусу замовлення в системі Terraleads.
	 *
	 * @return mixed|null
	 */
	public function getUrlTerraleadsOrderUpdate()
	{
		return $this->configs[self::CONFIG_URL_TERRALEADS_ORDER_UPDATE] ?? null;
	}
	
	/**
	 * Повертає масив відповідностей статусів між партнером та системою Terraleads.
	 *
	 * @return array
	 */
	public function getStatuses()
	{
		return $this->configs[self::SECTION_STATUSES] ?? [];
	}
	
	/**
	 * Повертає масив відповідностей статусів викупів
	 *
	 * @return array
	 */
	public function getRansomStatuses()
	{
		return $this->configs[self::SECTION_RANSOM_STATUSES] ?? [];
	}
	
	/**
	 * Повертає масив підстатісів.
	 *
	 * Опціний метод.
	 *
	 * @return array|mixed
	 */
	public function getSubStatuses()
	{
		return $this->configs[self::SECTION_SUB_STATUSES] ?? [];
	}
	
	/**
	 * Повертає посилання, що відповідає за додання замовлення в систему партнера.
	 *
	 * @return mixed|null
	 */
	public function getUrlOrderAdd()
	{
		return $this->configs[self::CONFIG_URL_ORDER_ADD] ?? null;
	}
	
	/**
	 * Повертає посилання, що відповідає за опитування статусів в системі партнера.
	 *
	 * @return mixed|null
	 */
	public function getUrlOrderInfo()
	{
		return $this->configs[self::CONFIG_URL_ORDER_INFO] ?? null;
	}

    /**
     * Повертає параметр конфігурації.
     * @param null|string $key Параметр конфігурації.
     *
     * @return mixed|null
     */
    public function getParameter($key)
    {
        return $this->configs[$key] ?? null;
    }
	
	/**
	 * Повертає назву директорії, в якій буде знаходитися журнал запитів і відповідей
	 *
	 * Це потрібно для розбиття журналів дуже активних інтеграцій.
	 * Наприклад, денний журнал інтеграції globalsender займає 200 мб.
	 *
	 * @return mixed|null
	 */
	public function getBrakeLogFolder()
	{
		return $this->configs[self::CONFIG_BRAKE_LOG_FOLDER] ?? null;
	}
	
	/**
	 * Початкова затримка на опитування замовленняgetUrlOrderInfo
	 *
	 * @return integer|null
	 */
	public function getInitialDelay()
	{
		return $this->configs[self::CONFIG_INITIAL_DELAY] ?? null;
	}
	
	/**
	 * Затримка на опитування замовлення першого дня
	 *
	 * @return integer|null
	 */
	public function getFirstDayDelay()
	{
		return $this->configs[self::CONFIG_FIRST_DAY_DELAY] ?? null;
	}
	
	/**
	 * Затримка на опитування замовлення другого дня
	 *
	 * @return integer|null
	 */
	public function getSecondDayDelay()
	{
		return $this->configs[self::CONFIG_SECOND_DAY_DELAY] ?? null;
	}
	
	/**
	 * Затримка на опитування замовлення інших днів
	 *
	 * @return integer|null
	 */
	public function getOtherDayDelay()
	{
		return $this->configs[self::CONFIG_OTHER_DAY_DELAY] ?? null;
	}
}