<?php

namespace app\modules\terraleads\common\adv\objects;

use yii\web\HttpException;

class User extends Configs
{
	/**
	 * Повертає api_key користувача системи Terraleads.
	 *
	 * @return mixed
	 * @throws HttpException Якщо не для користувача не задано apiKey.
	 */
	public function getApiKey()
	{
		$apiKey = $this->array;
		
		if (!isset($apiKey['terraleadsApiKey']))
			throw new HttpException(405, 'apiKey must be set!');
		
		return $apiKey['terraleadsApiKey'];
	}
	
	/**
	 * Повертає api_key користувача для отримання статусів по постбеку.
	 *
	 * @return mixed
	 */
	public function getPostbackApiKey()
	{
		$apiKey = $this->array;
		
		return $apiKey['postbackApiKey'] ?? null;
	}
}