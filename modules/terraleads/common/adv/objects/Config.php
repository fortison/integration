<?php

namespace app\modules\terraleads\common\adv\objects;

use yii\base\Exception;
use Yii;
use yii\web\HttpException;

/**
 * @property Full $full
 * @property User $user
 * @property Offer $offer
 */

class Config extends \yii\base\BaseObject
{
//	const LOG_FILE_NAME_BRIDGE = 'bridge';
//	const LOG_FILE_NAME_STATUS = 'status';
//	const LOG_FILE_EXTENSION = '.log';

	public static $configs;

	public $full;
	public $user;
	public $offer;
	
	public $integrationName;
	public $moduleName;
	public $userId;
	public $offerId;
	
	private $configFilePath;
	
	public function init()
	{
		$this->setConfigFilePath();
		if (!$this->isConfigFilePathExist())
			throw new Exception("Config file does not exist. File: {$this->integrationName}");
		$this->setFull();
		$this->setUser();
		$this->setOffer();
	}
	
	public function setConfigFilePath()
	{
		$integrationName = $this->getIntegrationName();
		$this->configFilePath = Yii::getAlias("@app/modules/{$this->moduleName}/config/adv/{$integrationName}.php");
	}
	
	public function getConfigFilePath()
	{
		return $this->configFilePath;
	}
	
	public function isConfigFilePathExist()
	{
		return is_file($this->getConfigFilePath());
	}
	
	public function getOrSetConfig($integrationName)
	{
		if (!isset(self::$configs[$integrationName])) {
			self::$configs[$integrationName] = require_once $this->getConfigFilePath();
		}

		return self::$configs[$integrationName];
	}

	/**
	 * Повертає шлях до директорії з логами поточної інтеграції
	 *
	 * @param null|integer $timestamp Часова мітка.
	 * @return string
	 */
	public function getFullLogPath($timestamp = null)
	{
		if (!$timestamp)
			$timestamp = time();
		
		$brakeLogFolder = $this->getBrakeLogFolder();
		
		return Yii::getAlias('@runtime/terraleads/logs/adv' . DIRECTORY_SEPARATOR . Yii::$app->formatter->asDate($timestamp) . DIRECTORY_SEPARATOR . $brakeLogFolder . DIRECTORY_SEPARATOR);
	}
	
	public function setFull()
	{
		$integrationName = $this->getIntegrationName();

		$full = $this->getOrSetConfig($integrationName);
		
		$this->full = new Full([
			'array' => $full,
		]);
	}
	
	public function setUser()
	{
		$user = $this->full->array[$this->userId] ?? null;
		
		if ($user === null)
			throw new Exception("User [{$this->userId}] not found in config file - {$this->integrationName}");
		
		$this->user = new User([
			'array' => $user,
		]);
	}
	
	public function setOffer()
	{
		$offer = $this->user->array[Configs::SECTION_OFFERS][$this->offerId] ?? null;
		
		if ($offer === null)
			throw new Exception("User [{$this->userId}] not exist offer {{$this->offerId}} in config file - {$this->integrationName}");
		
		$this->offer = new Offer([
			'array' => $offer,
		]);
	}
	
	/**
	 * Повертає максимальний час оцікування відповіді від партнера.
	 * Відноситься тільки до мостового запиту.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return null
	 */
	public function getBridgeRequestTimeout()
	{
		if ($this->offer->getBridgeRequestTimeout())
			return $this->offer->getBridgeRequestTimeout();
		elseif ($this->user->getBridgeRequestTimeout())
			return $this->user->getBridgeRequestTimeout();
		elseif ($this->full->getBridgeRequestTimeout())
			return $this->full->getBridgeRequestTimeout();
		else
			return 5;
	}
	
	/**
	 * Повертає максимальний час оцікування відповіді від партнера.
	 * Відноситься тільки до статусного запиту.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return null
	 */
	public function getStatusRequestTimeout()
	{
		if ($this->offer->getStatusRequestTimeout())
			return $this->offer->getStatusRequestTimeout();
		elseif ($this->user->getStatusRequestTimeout())
			return $this->user->getStatusRequestTimeout();
		elseif ($this->full->getStatusRequestTimeout())
			return $this->full->getStatusRequestTimeout();
		else
			return 5;
	}
	
	/**
	 * Перевіряє, чи активний міст.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return bool
	 */
	public function isBridgeEnabled()
	{
		if (!$this->full->isBridgeEnabled())
			return false;
		elseif (!$this->user->isBridgeEnabled())
			return false;
		elseif (!$this->offer->isBridgeEnabled())
			return false;
		else
			return true;
	}
	
	/**
	 * Перевіряє, чи активний обробник статусів.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return bool
	 */
	public function isStatusEnabled()
	{
		if (!$this->full->isStatusEnabled())
			return false;
		elseif (!$this->user->isStatusEnabled())
			return false;
		elseif (!$this->offer->isStatusEnabled())
			return false;
		else
			return true;
	}
	
	/**
	 * Повертає назву поточної інтеграції
	 *
	 * @return string
	 */
	public function getIntegrationName()
	{
		$integrationName = $this->integrationName;
		
		if (!$integrationName)
			throw new HttpException(405, "integrationName must be set!");
		
		return $integrationName;
	}
	
	/**
	 * Повертає неймспейс обробника статусів поточної інтеграції
	 * Враховується повна логічна ієрархія.
	 *
	 * @return string
	 */
	public function getStatusHandler()
	{
		if ($this->offer->getStatusHandler())
			$statusHandler = $this->offer->getStatusHandler();
		else
			if ($this->user->getStatusHandler())
				$statusHandler = $this->user->getStatusHandler();
			else
				if ($this->full->getStatusHandler())
					$statusHandler = $this->full->getStatusHandler();
				else {																									// Якщо обробника ніде не вказано, формуємо його з назви інтеграції
					$integrationName  = $this->integrationName;
					$integrationHandlerName  = ucfirst($this->integrationName);
					$moduleName     = $this->moduleName;
					$statusHandler  = "app\\modules\\{$moduleName}\\handlers\\adv\\{$integrationName}\\{$integrationHandlerName}StatusHandler";
				}
		
		return $statusHandler;
	}
	
	/**
	 * Повертає неймспейс обробника поточної інтеграції.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return string
	 */
	public function getBridgeHandler()
	{
		if ($this->offer->getBridgeHandler())
			$bridgeHandler = $this->offer->getBridgeHandler();
		else
			if ($this->user->getBridgeHandler())
				$bridgeHandler = $this->user->getBridgeHandler();
			else
				if ($this->full->getBridgeHandler())
					$bridgeHandler = $this->full->getBridgeHandler();
				else {																									// Якщо обробника ніде не вказано, формуємо його з назви інтеграції
					$integrationName  = $this->integrationName;
					$integrationHandlerName  = ucfirst($this->integrationName);
					$moduleName     = $this->moduleName;
					$bridgeHandler  = "app\\modules\\{$moduleName}\\handlers\\adv\\{$integrationName}\\{$integrationHandlerName}BridgeHandler";
				}
		
		return $bridgeHandler;
	}
	
	/**
	 * Повертає посилання urlTerraleadsOrderUpdate для оновлення статусів в системі Terraleads.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return string
	 */
	public function getUrlTerraleadsOrderUpdate()
	{
		if ($this->offer->getUrlTerraleadsOrderUpdate())
			$urlTerraleadsOrderUpdate = $this->offer->getUrlTerraleadsOrderUpdate();
		else
			if ($this->user->getUrlTerraleadsOrderUpdate())
				$urlTerraleadsOrderUpdate = $this->user->getUrlTerraleadsOrderUpdate();
			else
				if ($this->full->getUrlTerraleadsOrderUpdate())
					$urlTerraleadsOrderUpdate = $this->full->getUrlTerraleadsOrderUpdate();
				else
					$urlTerraleadsOrderUpdate  = "http://tl-api.com/api/lead/update";
		
		return $urlTerraleadsOrderUpdate;
	}
	
	/**
	 * Повертає масив статусів.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return array
	 */
	public function getStatuses()
	{
		if ($this->offer->getStatuses())
			$statuses = $this->offer->getStatuses();
		else
			if ($this->user->getStatuses())
				$statuses = $this->user->getStatuses();
			else
				if ($this->full->getStatuses())
					$statuses = $this->full->getStatuses();
				else
					$statuses  = [];
		
		return $statuses;
	}
	
	/**
	 * Повертає масив статусів.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return array
	 */
	public function getRansomStatuses()
	{
		if ($this->offer->getRansomStatuses())
			$statuses = $this->offer->getRansomStatuses();
		else
			if ($this->user->getRansomStatuses())
				$statuses = $this->user->getRansomStatuses();
			else
				if ($this->full->getRansomStatuses())
					$statuses = $this->full->getRansomStatuses();
				else
					$statuses  = [];
		
		return $statuses;
	}
	
	/**
	 * Повертає масив сабстатусів.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return array
	 */
	public function getSubStatuses()
	{
		if ($this->offer->getSubStatuses())
			$subStatuses = $this->offer->getSubStatuses();
		else
			if ($this->user->getSubStatuses())
				$subStatuses = $this->user->getSubStatuses();
			else
				if ($this->full->getSubStatuses())
					$subStatuses = $this->full->getSubStatuses();
				else
					$subStatuses  = [];
		
		return $subStatuses;
	}
	
	/**
	 * Повертає посилання для передачі замовлення рекламодавцю.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return string
	 * @throws Exception Якщо urlOrderAdd не задане
	 */
	public function getUrlOrderAdd()
	{
		if ($this->offer->getUrlOrderAdd())
			$urlOrderAdd = $this->offer->getUrlOrderAdd();
		else
			if ($this->user->getUrlOrderAdd())
				$urlOrderAdd = $this->user->getUrlOrderAdd();
			else
				if ($this->full->getUrlOrderAdd())
					$urlOrderAdd = $this->full->getUrlOrderAdd();
				else
					throw new Exception("Value {urlOrderAdd} must be set. File: {$this->integrationName}");
		
		return $urlOrderAdd;
	}
	
	/**
	 * Повертає посилання для опитавання статусів замовлень.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return string
	 * @throws Exception якщо urlOrderInfo не задане
	 */
	public function getUrlOrderInfo()
	{
		if ($this->offer->getUrlOrderInfo())
			$urlOrderInfo = $this->offer->getUrlOrderInfo();
		else
			if ($this->user->getUrlOrderInfo())
				$urlOrderInfo = $this->user->getUrlOrderInfo();
			else
				if ($this->full->getUrlOrderInfo())
					$urlOrderInfo = $this->full->getUrlOrderInfo();
				else
					throw new Exception("Value {urlOrderInfo} must be set. File: {$this->integrationName}");
		
		return $urlOrderInfo;
	}

    /**
     * Повертає параметр конфігурації.
     * Враховується повна логічна ієрархія.
     *
     * @param null|string $key Параметр конфігурації.
     * @return mixed
     * @throws Exception якщо $key не задане
     */
    public function getParameter($key=null)
    {
        if ($this->offer->getParameter($key)){
            $parameter = $this->offer->getParameter($key);
        }else{
            if ($this->user->getParameter($key)){
                $parameter = $this->user->getParameter($key);
            }else{
                if ($this->full->getParameter($key)){
                    $parameter = $this->full->getParameter($key);
                }else{
                    throw new Exception("Parameter {$key} must be set. File: {$this->integrationName}");
                }
            }
        }

        return $parameter;
    }
	
	/**
	 * Повертає шлях до журналу.
	 * Враховується повна логічна ієрархія.
	 *
	 * @return string
	 */
	public function getBrakeLogFolder()
	{
		$full	= $this->full->getBrakeLogFolder();
		$user	= $this->user->getBrakeLogFolder();
		$offer	= $this->offer->getBrakeLogFolder();
		
		if ($offer !== null) {																							// Якщо brakeLogFolder встановлено для офера
			if ($offer === true || !$offer)																				// Якщо значення true або пустота
				$offer = $this->offerId;																				// Встановлюємо offer_id

			if ($user === true || !$user)
				$user = $this->userId;
			
			if ($full === true || !$full)
				$full = $this->integrationName;
			
			$brakeLogFolder = $full . DIRECTORY_SEPARATOR . $user . DIRECTORY_SEPARATOR . $offer;
		} elseif ($user !== null) {
			if ($user === true || !$user)
				$user = $this->userId;
			
			if ($full === true || !$full)
				$full = $this->integrationName;
			
			$brakeLogFolder = $full . DIRECTORY_SEPARATOR . $user;
		} else {
			if ($full === true || !$full)
				$full = $this->integrationName;
			
			$brakeLogFolder = $full;
		}
		
		return $brakeLogFolder;
	}
	
	/**
	 * Перевірка чи можна оновлювати статус замовлення.
	 *
	 * Це потрібно для зменшення часу виконання крону на опитування статусів,
	 * зменшення кількості зайвих запитів
	 * та зменшення розміру журналу, що дозволить зберігати журнал за ширший період.
	 *
	 * @param integer $createdAt Час коли замовлення було додане
	 * @param integer $requestAt Час коли замовлення було оновлено
	 * @return bool
	 */
	public function isTimeToStatusRequest($createdAt, $requestAt)
	{
		$time				= time();
		$initialDelay		= $this->getInitialDelay();
		$firstDayDelay		= $this->getFirstDayDelay();
		$secondDayDelay		= $this->getSecondDayDelay();
		$otherDayDelay		= $this->getOtherDayDelay();
		
		if ($createdAt == $requestAt) {																					// Перші 20 хвилин немає сенсу опитувати замовлення
			echo("Щойно створене замовлення! ");
			if ($createdAt < $time - $initialDelay) {
				echo("Можна оновлювати. Вже минули перші {$initialDelay} сек. \n \n");
				return true;
			} else {
				$remaining = round(($requestAt - ($time - $initialDelay)) / 60);
				echo("Не можна оновлювати. Лишилося {$remaining}хв. \n \n");
				return false;
			}
		} elseif ($createdAt > $time - 60*60*24) {																		// Перший день опитуємо замовлення кожні півгодини
			echo("Пішли перші 24 години! ");
			if ($requestAt < ($time - $firstDayDelay)) {
				echo "Час для оновленяя настав. \n \n";
				return true;
			} else {
				$remaining = round(($requestAt - ($time - $firstDayDelay)) / 60);
				echo "Ще не настав час для оновлення. Лишилося {$remaining}хв. \n\n";
				return false;
			}
		}elseif ($createdAt > $time - 60*60*48) {                                                                		// Другий день опитуємо замовлення кожні 3 години
			echo("Пішли другі 24 години! ");
			if ($requestAt < $time - $secondDayDelay) {
				echo "Час для оновленяя настав. \n \n";
				return true;
			} else {
				$remaining = round(($requestAt - ($time - $secondDayDelay)) / 60);
				echo "Ще не настав час для оновлення. Лишилося {$remaining}хв. \n\n";
				return false;
			}
		}elseif ($createdAt < $time) {																					// Третій і більше, опитуваня раз в день.
			echo("Минуло більше двох днів! ");
			if ($requestAt < $time - $otherDayDelay) {
				echo "Час для оновлення настав. \n \n";
				return true;
			} else {
				$remaining = round(($requestAt - ($time - $otherDayDelay)) / 60);
				echo "Ще не настав час для оновлення. Лишилося {$remaining}хв. \n\n";
				return false;
			}
		} else {
			echo("\nНевідомий діапозон дат.\n");
			return true;
		}
	}
	
	public function getInitialDelay()
	{
		$offerInitialDelay = $this->offer->getInitialDelay();
		$userInitialDelay = $this->user->getInitialDelay();
		$fullInitialDelay = $this->full->getInitialDelay();
		
		if ($offerInitialDelay)
			return $offerInitialDelay;
		elseif ($userInitialDelay)
			return $userInitialDelay;
		elseif ($fullInitialDelay)
			return $fullInitialDelay;
		else
			return 60*20;
	}
	
	public function getFirstDayDelay()
	{
		$offerFirstDayDelay = $this->offer->getFirstDayDelay();
		$userFirstDayDelay = $this->user->getFirstDayDelay();
		$fullFirstDayDelay = $this->full->getFirstDayDelay();
		
		if ($offerFirstDayDelay)
			return $offerFirstDayDelay;
		elseif ($userFirstDayDelay)
			return $userFirstDayDelay;
		elseif ($fullFirstDayDelay)
			return $fullFirstDayDelay;
		else
			return 60 * 30;
	}
	
	public function getSecondDayDelay()
	{
		$offerSecondDayDelay = $this->offer->getSecondDayDelay();
		$userSecondDayDelay = $this->user->getSecondDayDelay();
		$fullSecondDayDelay = $this->full->getSecondDayDelay();
		
		if ($offerSecondDayDelay)
			return $offerSecondDayDelay;
		elseif ($userSecondDayDelay)
			return $userSecondDayDelay;
		elseif ($fullSecondDayDelay)
			return $fullSecondDayDelay;
		else
			return 60 * 60 * 3;
	}
	
	public function getOtherDayDelay()
	{
		$offerOtherDayDelay = $this->offer->getOtherDayDelay();
		$userOtherDayDelay = $this->user->getOtherDayDelay();
		$fullOtherDayDelay = $this->full->getOtherDayDelay();
		
		if ($offerOtherDayDelay)
			return $offerOtherDayDelay;
		elseif ($userOtherDayDelay)
			return $userOtherDayDelay;
		elseif ($fullOtherDayDelay)
			return $fullOtherDayDelay;
		else
			return 60 * 60 * 24;
	}
}