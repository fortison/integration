<?php

namespace app\modules\terraleads\common\web\handlers\base;

use app\modules\terraleads\common\traits\LogTrait;
use app\modules\terraleads\common\traits\RequestTrait;
use app\modules\terraleads\common\traits\JsonTrait;

/**
 * Created by PhpStorm.
 * User: nagard
 * Date: 02.11.17
 * Time: 15:48
 *
 * @property \app\modules\terraleads\common\web\models\forms\TerraleadsWebModel $incomingModel Дані замовлення, що прийшли від системи Terraleads
 * @property \app\modules\terraleads\common\web\objects\Config $config Головна конфігурація поточного замовлення
 * @property \app\modules\terraleads\common\models\forms\ResponseModel $response Об'єкт запиту
 */
abstract class BaseBridgeHandler extends \yii\base\BaseObject
{
	use RequestTrait;
	use LogTrait;
	use JsonTrait;
	
	public $config;
	public $incomingModel;
	
	public function getRelatedStatus($systemStatus)
	{
		$statuses = $this->config->getStatuses();
		
		return $statuses[$systemStatus] ?? null;
	}
}