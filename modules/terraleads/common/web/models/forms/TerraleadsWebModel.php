<?php

namespace app\modules\terraleads\common\web\models\forms;

class TerraleadsWebModel extends \yii\base\Model
{
	public $id;
	public $status;
	public $type;
	public $cost;
	public $action;
	public $comment;
	public $stream_id;
	public $country;
	public $ip;
	public $tz;
	public $sub_id;
	public $sub_id_1;
	public $sub_id_2;
	public $sub_id_3;
	public $sub_id_4;
	public $utm_content;
	public $utm_medium;
	public $utm_campaign;
	public $utm_term;
	public $utm_source;
	public $date_create;
	public $user_agent;
	public $user_id;
	public $check_sum;
	
	public $api_key;
	
	public function rules()
	{
		return [
			['id', 				'string', 'max' => 255],
			['id', 				'required'],
			
			['status', 			'string', 'max' => 255],
			['type', 			'string', 'max' => 255],
			['cost',			'number'],
			['action', 			'string', 'max' => 255],
			['comment', 		'string'],
			['stream_id',		'integer'],
			['country',			'string', 'max' => 255],
			['ip',				'string', 'max' => 255],
			['tz',				'integer'],
			
			['sub_id',			'string'],
			['sub_id_1',		'string'],
			['sub_id_2',		'string'],
			['sub_id_3',		'string'],
			['sub_id_4',		'string'],
			
			['utm_content',		'string'],
			['utm_medium',		'string'],
			['utm_campaign',	'string'],
			['utm_term',		'string'],
			['utm_source',		'string'],
			
			['date_create',		'string'],
			
			['user_agent',		'string'],
			['user_id',			'integer'],
			['user_id', 		'required'],
			
			['check_sum', 		'string', 'length' => 40],
			['check_sum', 		'required'],
		];
	}
	
	public function setApiKey($apiKey)
	{
		$this->api_key = $apiKey;
	}
	
	/**
	 * Перевіряє чи не було ніяких змін в отриманих даних.
	 *
	 * @param string $apiKey api_key користувача в системі Terraleads.
	 * @return bool
	 */
    public function isCorrectCheckSum($apiKey)
    {
        if( !$this->hasErrors() )
        {
        	$this->setApiKey($apiKey);
        	
            $check_sum = $this->getCheckSum();
            
            if( $check_sum === $this->check_sum )
            	return true;
            return false;
            
        }
    }
	
	/**
	 * Повертає хеш суму основних даних.
	 *
	 * @return string
	 */
    public function getCheckSum()
    {
        return sha1($this->id . $this->status . $this->cost . $this->comment . $this->api_key);
    }
}
