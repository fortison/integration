<?php

namespace app\modules\terraleads\common\web\objects;

use yii\web\HttpException;

class User extends Configs
{
	/**
	 * Повертає api_key користувача системи Terralears.
	 *
	 * @return mixed
	 * @throws HttpException Якщо не для користувача не задано apiKey.
	 */
	public function getApiKey()
	{
		$apiKey = $this->array;
		
		if (!isset($apiKey['terraleadsApiKey']))
			throw new HttpException(405, 'apiKey must be set!');
		
		return $apiKey['terraleadsApiKey'];
	}
}