<?php
/**
 * Created by PhpStorm.
 * User: nagard
 * Date: 28.11.17
 * Time: 11:19
 */

namespace app\modules\terraleads\common\models\forms;

use yii\base\BaseObject;

/**
 * Class ResponseModel
 * @package app\modules\terraleads\common\models\forms
 *
 * @property string $error Помилка під час запиту
 * @property mixed $time Час запиту
 * @property integer $code Код запиту
 * @property mixed $content Вміст запиту
 * @property string $method Метод запиту
 * @property string $cookie Рип'яхи
 */
class ResponseModel extends BaseObject
{
	public $error;
	public $time;
	public $code;
	public $content;
	public $data;
	public $method;
	public $cookie;
	
	public function setContent($content)
	{
		$this->content = $content;
	}
	
	public function setError($error)
	{
		$this->error = $error;
	}
	
	public function setCode($code)
	{
		$this->code = $code;
	}
	
	public function setData($data)
	{
		$this->data = $data;
	}
	
	public function setTime($time)
	{
		$this->time = $time;
	}
	
	public function setMethod($method)
	{
		$this->method = $method;
	}
	
	public function setCookie($cookie)
	{
		$this->cookie = $cookie;
	}
	
	public function getContent()
	{
		return $this->content;
	}
	
	public function getData()
	{
		return $this->data;
	}
	
	public function getError()
	{
		return $this->error;
	}
	
	public function getCode()
	{
		return $this->code;
	}
	
	public function getTime()
	{
		return $this->time;
	}
	
	public function getMethod()
	{
		return $this->method;
	}
	
	public function getCookie()
	{
		return $this->cookie;
	}
	
	/**
	 * Повертає відповідь у вигляді масиву для простішого подальшого викоритання.
	 * Використовується при логуванні запита.
	 *
	 * @return array
	 */
	public function getResponseAsArray()
	{
		$code		= $this->getCode();
		$error		= $this->getError();
		$time		= $this->getTime();
		$content	= $this->getContent();
		$method		= $this->getMethod();
		
		return [
			$code,
			$error,
			$time,
			$content,
			$method,
		];
	}
}