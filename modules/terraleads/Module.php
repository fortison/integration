<?php

namespace app\modules\terraleads;

use Yii;
use yii\web\ErrorHandler;

class Module extends \yii\base\Module
{
	public $disabled = false;
	
	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();
		
		Yii::configure($this, [																							// Потрібно, коли звернення йде до модуля, коли звернення йде вже до контролера і екшина це не працює, тому там потрібно встановлювати обробник знову
			'components' => [
				'errorHandler' => [
					'class' => ErrorHandler::class,
					'errorAction'=>'terraleads/adv/error',
				]
			],
		]);

		/** @var ErrorHandler $handler */
		$handler = $this->get('errorHandler');
		Yii::$app->set('errorHandler', $handler);
		$handler->register();
	}
}
