<?php

namespace app\modules\terraleads\commands\adv;

use app\common\models\Error;
use app\common\models\OrderApilead;
use app\modules\terraleads\common\exeptions\OrderErrorException;
use app\modules\terraleads\common\adv\objects\Config;
use nahard\log\helpers\Log;
use Yii;
use Exception;
use yii\db\Transaction;

class TerraleadsStatusSchedulerController extends \yii\console\Controller
{
	public 		$moduleName;																							// Передаємо назву модуля. Треба для визначення обробника помилок

    public function actionIndex()
    {
    	foreach (OrderApilead::findExpectedOrdersBatch($this->moduleName) as $batchModels)
    	{
    		foreach ($batchModels as $orderModel)
    		{
                // self::tmpLog('['.date('Y-m-d H:i:s').'][START PROCESS] id='.$orderModel->id);

				$transaction = Yii::$app->db->beginTransaction();
				try {
					$orderModel = OrderApilead::getOrderWithBlockById($orderModel->id);									// Отримуємо нову модель, але з блокуванням цього рядка в БД
					
					if ( $orderModel->isProcessingStop() === false ) {															// Якщо цей запис виявився в обробці, продовжуємо роботу. Це необхідно якщо планувальники почнуть пересікатися.
						$transaction->rollBack();
						continue;
					}
					
					if ($orderModel->getOrderErrorsCount() >= 2) {
						echo "Прокляті помилки заважають запитати новий статус!\n\n". $orderModel->system_order_id."\n\n";
						$transaction->rollBack();
						continue;
					}
					
					$orderModel->startProcess();																		// Ставимо мітку, що цей запис в обробці
					
					$transaction->commit();
				} catch (Exception $e) {
					$transaction->rollBack();
					continue;
				}
				
				$transaction = Yii::$app->db->beginTransaction(Transaction::READ_UNCOMMITTED);				// Ставимо найнижчий рівень ізоляції, щоб подальші обробники мали змогу прочитати мітку processing, якщо рівень підняти ми почнемо блокувати запис й отримувати помилку з timeout
				try{
					$config = new Config([
						'integrationName' 	=> $orderModel->integration_name,
						'moduleName' 		=> $this->moduleName,
						'userId' 			=> $orderModel->system_user_id,
						'offerId' 			=> $orderModel->offer_id,
					]);
					
					if (!$config->isStatusEnabled()) {																	// Якщо статуси вимкнені
						$transaction->rollBack();
						continue;
					}
					
					if ($config->user->getPostbackApiKey()) {															// Якщо статуси йдуть по постбеку
						$transaction->rollBack();
						continue;
					}
					
					if (!$config->isTimeToStatusRequest($orderModel->created_at, $orderModel->request_at)) {			// Якщо настав час для оновлення статусу
						$transaction->rollBack();
						continue;
					}
					
					/**@var $handler \app\modules\terraleads\common\adv\handlers\base\BaseStatusHandler*/
					$handler = Yii::createObject([
						'class' 		=> $config->getStatusHandler(),
						'config'  		=> $config,
						'orderModel'	=> $orderModel,
					]);
					
					$orderModel->setRequestTime();																		// Встановлюємо час останнього запиту
					
					$terraleadsStatusDataModel = $handler->updateStatusInSystem();
					
					print_r($terraleadsStatusDataModel->getProperties());												// Виводиво інформацію про замовлення в консоль
					
					$transaction->commit();
				} catch (OrderErrorException $e) {																		// Ловимо помилки, котрі можна прив'язати до замовлення
					$transaction->rollBack();
					
					$message 	= $e->getMessage();
					$file 		= $e->getFile();
					$line 		= $e->getLine();
					$trace		= $e->getTraceAsString();
					
					$orderId	= $e->getOrderId();
					$text		= "{$message} \nfile: {$file}\nline: {$line}\n{$trace}";
					$category	= __METHOD__;
					
					Error::add($orderId, $text, $category);
					continue;
				} catch (Exception $e) {
					$transaction->rollBack();
					
					$message 	= $e->getMessage();
					$file 		= $e->getFile();
					$line 		= $e->getLine();
					$trace		= $e->getTraceAsString();
					
					$orderId	= $orderModel->id ?? null;
					$text		= "{$message} \nfile: {$file}\nline: {$line}\n{$trace}";
					$category	= __METHOD__;
					
					Log::error($text, $orderId, $category);
					continue;
				} finally {
					$orderModel->stopProcess();																			// Знімаємо запис з обробки, виставивши потрібну мітку
				}

                // self::tmpLog('['.date('Y-m-d H:i:s').'][END PROCESS] id='.$orderModel->id);
			}
		}
	}

	// public static function tmpLog($data){
    //     file_put_contents('/var/www/connectcpa/data/www/connectcpa.net/runtime/log.txt', $data.PHP_EOL, FILE_APPEND);
    // }
}