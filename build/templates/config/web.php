<?php

$params = require(__DIR__ . '/params.php');

$config = [
	'id' => 'basic',
	'name'		=> 'ConnectCPA',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log'],
	'aliases' => [
		'@bower' => '@vendor/bower-asset',
		'@npm'   => '@vendor/npm-asset',
	],
	'modules' => [
		'control' => 'app\modules\control\Module',
		'apilead' => 'app\modules\apilead\Module',
		'terraleads' => ['class' => 'app\modules\terraleads\Module'],
		'api' => [
			'class' => 'app\modules\api\Module',
			'modules' => [
				'apilead' => 'app\modules\api\modules\apilead\Module',
				'terraleads' => 'app\modules\api\modules\terraleads\Module',
			]
		],
		'log' => [
			'class' => 'nahard\log\Module',
			'layout' => '@app/modules/control/views/layouts/main',
			'accessRules' => [
				[
					'actions' => ['view'],
					'allow' => true,
					'roles' => ['@'],
				],
				[
					'actions' => ['index'],
					'allow' => true,
					'roles' => ['@'],
				],
				[
					'actions' => ['create'],
					'allow' => true,
					'roles' => ['@'],
				],
				[
					'actions' => ['update'],
					'allow' => true,
					'roles' => ['@'],
				],
				[
					'actions' => ['delete'],
					'allow' => true,
					'roles' => ['@'],
				],
				[
					'actions' => ['make-read'],
					'allow' => true,
					'roles' => ['@'],
				],
			]
		],
		'deploy' => [
			'class' => 'nahard\deploy\Module',
			'layout' => '@app/modules/control/views/layouts/main',
			'buildFile' => 'build/build.xml',
			'phingProgram' => 'phing', //php phing.phar
			'viewTabs' => [
				'build' => [
					'file' => 'build.log',
				],
				'git' => [
					'file' => 'git.log',
				],
				'composer' => [
					'file' => 'composer.log',
				],
				'migrate' => [
					'file' => 'migrate.log',
				],
				'scheduler.error' => [
					'file' => 'scheduler.error.log',
				],
				'scheduler' => [
					'file' => 'scheduler.log',
				],
			],
			'accessRules' => [
				[
					'actions' => ['view'],
					'allow' => true,
					'roles' => ['@'],
				],
				[
					'actions' => ['index'],
					'allow' => true,
					'roles' => ['@'],
				],
				[
					'actions' => ['create'],
					'allow' => true,
					'roles' => ['@'],
				],
				[
					'actions' => ['delete'],
					'allow' => true,
					'roles' => ['@'],
				],
				[
					'actions' => ['webhook'],
					'allow' => true,
					'roles' => ['?'],
				],
			]
		],
	],
	'components' => [
		'response' => [
			'formatters' => [
				\yii\web\Response::FORMAT_JSON => [
					'class' => 'yii\web\JsonResponseFormatter',
					'prettyPrint' => true, // use "pretty" output in debug mode
					'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
				],
			],
		],
		'request' => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => 'x9LaHxiufrO5aypruRWK6dMSfkoB3Gpd',
		],
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'user' => [
			'identityClass' => 'app\models\User',
			'enableAutoLogin' => true,
		],
		'errorHandler' => [
			'class'         => 'yii\web\ErrorHandler',
			'errorAction'   => 'site/error',
		],
		'formatter' => [
			'dateFormat' => 'dd.MM.yyyy',
			'datetimeFormat' => 'dd.MM.yyyy-HH:mm:ss',
			'timeZone' => 'Europe/Kiev',
//				'decimalSeparator' => ',',
//				'thousandSeparator' => ' ',
//				'currencyCode' => 'EUR',
		],
		'urlManager' => [
			'class' => 'yii\web\UrlManager',
			'showScriptName' => false,
			'enablePrettyUrl' => true,
			
			'rules' => array(
				'apilead/adv/error'									=> 'apilead/adv/bridge/error',
				'apilead/adv-postback-status'    					=> 'apilead/adv/adv-postback-status/index',					// Тільки для 2906, після зміни ними постбека - видалити
				'apilead/adv/adv-postback-status'    				=> 'apilead/adv/adv-postback-status/index',
				'apilead/adv/extra-<integrationName:[\w\d-]+>'		=> 'apilead/adv/extra/index',
				'apilead/adv/<integrationName:[\w\d-]+>'    		=> 'apilead/adv/bridge/index',
				
				'apilead/web/error'									=> 'apilead/web/bridge/error',
				'apilead/web/<integrationName:[\w\d-]+>'    		=> 'apilead/web/bridge/index',
				
				'terraleads/adv/error'								=> 'terraleads/adv/bridge/error',
				'terraleads/adv/adv-postback-status'				=> 'terraleads/adv/adv-postback-status/index',
				'terraleads/adv/extra-<integrationName:[\w\d-]+>'	=> 'terraleads/adv/extra/index',
				'terraleads/adv/<integrationName:[\w\d-]+>'			=> 'terraleads/adv/bridge/index',
				
				'terraleads/web/error'								=> 'terraleads/web/bridge/error',
				'terraleads/web/<integrationName:[\w\d-]+>'			=> 'terraleads/web/bridge/index',

//				'<controller:\w+>/<id:\d+>' => '<controller>/view',
//				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
//				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
			),
		],
		'mailer' => [
			'class' => 'yii\swiftmailer\Mailer',
			// send all mails to a file by default. You have to set
			// 'useFileTransport' to false and configure a transport
			// for the mailer to send real emails.
			'useFileTransport' => true,
		],
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
				[
					'class' => 'nahard\log\components\DbTarget',
					'levels' => ['error', 'warning'],
					'logVars' => [],
//					'categories' => ['app\*'],
					'logTable' => 'log',
					'except' => [
						'yii\web\HttpException:404',
					],
				],
			],
		],
		'assetManager' => [
			'bundles' => [
				'edgardmessias\assets\nprogress\NProgressAsset' => [
					'configuration' => [
						'minimum'		=> 0.5,
						'showSpinner'	=> true,
					],
					'page_loading' => false,
					'pjax_events' => false,
					'jquery_ajax_events' => true,
				],
			],
		],
		'db' => require(__DIR__ . '/db.php'),
	],
	'params' => $params,
];

if (YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][] = 'debug';
	$config['modules']['debug'] = [
		'class' => 'yii\debug\Module',
		// uncomment the following to add your IP if you are not connecting from localhost.
		'allowedIPs' => ['127.0.0.1', '::1', '172.*', '192.*', '185.80.52.43', '*'],
	];
	
	$config['bootstrap'][] = 'gii';
	$config['modules']['gii'] = [
		'class' => 'yii\gii\Module',
		// uncomment the following to add your IP if you are not connecting from localhost.
		'allowedIPs' => ['127.0.0.1', '::1', '172.*', '192.*', '185.80.52.43'],
		'generators' => [ //here
			'crud' => [
				'class' => 'yii\gii\generators\crud\Generator',
				'templates' => [
					'adminlte' => '@vendor/dmstr/yii2-adminlte-asset/gii/templates/crud/simple',
				]
			]
		],
	];
}

return $config;
