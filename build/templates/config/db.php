<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host={app.db.host};dbname={app.db.name}',
    'username' => '{app.db.user}',
    'password' => '{app.db.pass}',
    'charset' => 'utf8mb4',
	
	'enableSchemaCache' => true,
	'schemaCacheDuration' => 60,
];
