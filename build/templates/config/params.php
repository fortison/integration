<?php

return [
	'adminEmail' 		=> '{app.adminEmail}',
	'environment'		=> [
		'app' 		=> "{app.environment}",																				// Судячи з всього це ніде не буде використовуватись. Видалити
		'server' 	=> "{server.environment}",
	],
	'basedir'			=> "{basedir}",
	'phingLogFile'		=> "{phing.file.log}",
	'phingBuildFile'	=> "{phing.file.build}",
];
