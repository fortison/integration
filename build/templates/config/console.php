<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
	'id' => 'basic-console',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log'],
	'controllerNamespace' => 'app\commands',
	'components' => [
		'formatter' => [
			'dateFormat' => 'dd.MM.yyyy',
			'datetimeFormat' => 'dd.MM.yyyy HH:mm:ss',
			'timeZone' => 'Europe/Kiev',
//			'decimalSeparator' => ',',
//			'thousandSeparator' => ' ',
//			'currencyCode' => 'EUR',
		],
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'log' => [
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
				[
					'class' => 'nahard\log\components\DbTarget',
					'levels' => ['error', 'warning'],
					'logVars' => [],
//					'categories' => ['app\*'],
					'logTable' => 'log',
				],
//              [
//                  'class' => 'yii\log\EmailTarget',
//                  'levels' => ['error'],
//                  'categories' => ['yii\db\*'],
//                  'message' => [
//                     'from' => ['log@example.com'],
//                     'to' => ['admin@example.com', 'developer@example.com'],
//                     'subject' => 'Ошибки базы данных на сайте example.com',
//                  ],
//              ],
			],
		],
		'db' => $db,
	],
	
	'params' => $params,
	'controllerMap' => [
		'apilead-statuses' => [
			'class' => 'app\modules\apilead\commands\adv\ApileadStatusSchedulerController',
			'moduleName' => 'apilead',
		],
		'terraleads-statuses' => [
			'class' => 'app\modules\terraleads\commands\adv\TerraleadsStatusSchedulerController',
			'moduleName' => 'terraleads',
		],
	],
];

if (YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][] = 'gii';
	$config['modules']['gii'] = [
		'class' => 'yii\gii\Module',
	];
}

return $config;
