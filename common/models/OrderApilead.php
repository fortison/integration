<?php

namespace app\common\models;

use app\modules\apilead\common\adv\handlers\base\BaseStatusHandler;
use app\modules\apilead\common\adv\models\forms\ApileadStatusDataModel;
use app\modules\terraleads\common\adv\models\forms\TerraleadsStatusDataModel;
use yii\base\InvalidConfigException;
use yii\behaviors\TimestampBehavior;
use Exception;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class OrderApilead
 * @package app\common\models
 * @property \app\modules\apilead\common\adv\objects\Config $config
 */

class OrderApilead extends OrderApileadGii
{
	const STATUS_ACCEPT		= 'accept';
	const STATUS_EXPECT 	= 'expect';
	const STATUS_CONFIRM	= 'confirm';
	const STATUS_REJECT		= 'reject';
	const STATUS_TRASH		= 'trash';
	const STATUS_UNKNOWN	= 'unknown';
	const STATUS_MANUAL		= 'manual';
	const STATUS_TRANSFER	= 'transfer';
	const STATUS_LIST		= [
		self::STATUS_ACCEPT => self::STATUS_ACCEPT,
		self::STATUS_EXPECT => self::STATUS_EXPECT,
		self::STATUS_CONFIRM => self::STATUS_CONFIRM,
		self::STATUS_REJECT => self::STATUS_REJECT,
		self::STATUS_TRASH => self::STATUS_TRASH,
		self::STATUS_UNKNOWN => self::STATUS_UNKNOWN,
		self::STATUS_MANUAL => self::STATUS_MANUAL,
		self::STATUS_TRANSFER => self::STATUS_TRANSFER,
	];
	const TERRA_LEAD = 'terraleads';
	const API_LEAD   = 'apilead';
	const LEADS_LIST        = [
	    self::TERRA_LEAD => self::TERRA_LEAD,
        self::API_LEAD   => self::API_LEAD,
    ];
	
	const RANSOM_CONFIRM	= 'confirm';
	const RANSOM_REJECT		= 'reject';
	const RANSOM_EXPECT 	= 'expect';
	const RANSOM_UNKNOWN 	= 'unknown';
	
	const PROCESS_STOP		= 0;
	const PROCESS_START		= 1;
	
	const NEED_SEND_STATUS	= 1;

	// Число яке вказує, які ліди з яким віком вважаються актуальними (По ним більше не опитуються статуси)
	const MAX_EXPECTED_TIME	= 60*60*24*60;
	/**
	 * @var mixed|null
	 */
//	private static $count;
	/**
	 * @var mixed|null
	 */
//	private static $count;
	/**
	 * @var mixed|null
	 */
//	private static $count;
	
	public static function getStatusList($label=true)
	{
		$statusList = self::STATUS_LIST;
		if ($label)
			return $statusList;
		return array_keys($statusList);
	}

    public static function getLeadsList($label=true)
    {
        $leadsList = self::LEADS_LIST;
        if ($label)
            return $leadsList;
        return array_keys($leadsList);
    }
	
	function behaviors()
	{
		return array(
			[
				'class' => TimestampBehavior::class,
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at', 'request_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
			],
		);
	}
	
	/**
	 * Метод ставить мітку, котра говорить, що розпочато обробку замовлення
	 *
	 * @return bool true якщо вдалося виставити мітку
	 */
	public function startProcess()
	{
		return $this->setProcessStatus(self::PROCESS_START);
	}
	
	/**
	 * Метод ставить мітку, котра говорить, що обробку замовлення закінчено
	 *
	 * @return bool true якщо вдалося виставити мітку
	 */
	public function stopProcess()
	{
		return $this->setProcessStatus(self::PROCESS_STOP);
	}
	
	/**
	 * Метод ставить мітку, котра вказує на статус оброблення замовлення
	 *
	 * @return bool true якщо вдалося виставити мітку
	 */
	public function setProcessStatus($processingStatus)
	{
		$this->processing = $processingStatus;
		return $this->save();
	}
	
	/**
	 * Check start processing
	 * @return bool true
	 */
	public function isProcessingStart()
	{
		return (int)$this->processing === self::PROCESS_START;
	}

    /**
     * Check stop processing
     * @return bool true
     */
    public function isProcessingStop()
    {
        return (int)$this->processing === self::PROCESS_STOP;
    }

    /**
     * Перевіряє, чи сталася з замовленням помилка під час його опрацювання
     *
     * Також відбувається перевірка часу останнього опрацювання.
     * В майбутньому налаштування часу тре перенести в конфіг чи базу
     *
     * @return bool true якщо була помилка
     */
    public function isProcessingFail()
    {
        return ($this->processing == self::PROCESS_START && $this->request_at < time() - 60*60);
    }
	
	public function setRequestTime($timestamp=null)
	{
		if ($timestamp === null)
			$timestamp = time();
		
		$this->request_at = $timestamp;
		$this->save();
	}
	
	/**
	 * Повертає об'єкт запиту на отримання всіх не виправлених помилок поточного замовлення
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getOrderErrorsQuery()
	{
		return Error::find()->where(['status' => Error::STATUS_UNREAD, 'error.order_id' => $this->id]);
	}
	
	/**
	 * Повертає кількість не виправлених помилок поточного замовлення
	 *
	 * @return int
	 */
	public function getOrderErrorsCount()
	{
		return $this->getOrderErrorsQuery()->count();
	}
	
	/**
	 * Повертає об'єкт запиту на отримання всіх замовлень з помилками
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public static function getErrorOrdersQuery()
	{
		
		$count = Yii::$app->db->createCommand('select count(*) as counter FROM (select order_id from error WHERE status=1 GROUP BY order_id) as a')->queryAll();
//		var_dump($count);		die();
//		return self::find('select count(*) as counter FROM (select order_id from error WHERE status=1 GROUP BY order_id) as a');
//		return self::prepare(Yii::$app->db)->innerJoin(Error::tableName(), 'error.order_id = order.id')
//			->where(['error.status' => Error::STATUS_UNREAD]);
		return $count;
	}
	
	public static function getErrorOrdersQueryOld()
	{
	
//		$count = Yii::$app->db->createCommand('select count(*) as counter FROM (select order_id from error WHERE status=1 GROUP BY order_id) as a')->queryAll();
//		var_dump($count);		die();
//		return self::find('select count(*) as counter FROM (select order_id from error WHERE status=1 GROUP BY order_id) as a');
		return self::find()->innerJoin(Error::tableName(), 'error.order_id = order.id')
			->where(['error.status' => Error::STATUS_UNREAD]);
//		return $count;
	}
	
	/**
	 * Повертає кількість замовлення, що з помилками
	 *
	 * @return int
	 */
	public static function getErrorOrdersCount()
	{
		return self::getErrorOrdersQuery();
	}
	
	/**
	 * Повертає візуальний віджет з мітками кількості проблемних замовлень
	 *
	 * @return string
	 */
	public static function getErrorOrdersCountWidget()
	{
		$count = self::getErrorOrdersCount();
		
		$icon = self::getErrorOrdersCountIcon($count);
		
		return '<span class="pull-right-container">' . $icon . '</span>';
	}
	
	/**
	 * Повертає кількість замовлення, що з помилками
	 *
	 * @return int
	 */
	public static function getOutdateOrdersCount()
	{
		return self::findOutdateOrders()->count();
	}

	/**
	 * Повертає іконку з цифрою, що вказує на кількість проблемних замовлень
	 *
	 * @param $count int|string Кількість проблемних замовлень
	 * @return string
	 */
	public static function getOutdateOrdersCountIcon($count)
	{
		if ($count) {
			return '<i class="label pull-right bg-yellow">' . $count . '</i>';
		}
	}

	/**
	 * Повертає візуальний віджет з мітками кількості проблемних замовлень
	 *
	 * @return string
	 */
	public static function getOutdateOrdersCountWidget()
	{
		$count = self::getOutdateOrdersCount();

		$icon = self::getOutdateOrdersCountIcon($count);

		return '<span class="pull-right-container">' . $icon . '</span>';
	}

	/**
	 * Повертає іконку з цифрою, що вказує на кількість проблемних замовлень
	 *
	 * @param $count int|string Кількість проблемних замовлень
	 * @return string
	 */
	public static function getErrorOrdersCountIcon($count)
	{
//		var_dump($count);die();
		if ($count)
			return '<i class="label pull-right bg-yellow">' . $count[0]['counter'] . '</i>';
	}
	
	/**
	 * Повертає об'єкт запиту на отримання всіх проблемних замовлень
	 * Дотепер є два види:
	 * * статус unknown
	 * * замовлення дуже довго перебуває в обробці (пташка processing)
	 * * замовлення занадто довго зі статусом expect (3 тижні) (не реалізовано)
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public static function findProblemOrders()
	{
		return self::find()->where([
			'or', 'system_status=:system_status', [
				'and' , 'processing=:processing', [
					'<', 'updated_at', time() - 60*60,
				],
			],
		], [
			':system_status' => self::STATUS_UNKNOWN,
			':processing' => self::PROCESS_START,
		]);
	}

	public static function findOutdateOrders()
	{
		return self::find()->outdated()->fresh();
	}
	
	/**
	 * Повертає кількість проблемних замовлень
	 *
	 * @return int|string
	 */
	public static function getProblemOrdersCount()
	{
		return self::findProblemOrders()->count();
	}
	
	/**
	 * Повертає візуальний віджет з мітками кількості проблемних замовлень
	 *
	 * @return string
	 */
	public static function getProblemOrdersCountWidget()
	{
		$count = self::getProblemOrdersCount();
		
		$icon = self::getProblemOrdersCountIcon($count);
		
		return '<span class="pull-right-container">' . $icon . '</span>';
	}
	
	/**
	 * Повертає іконку з цифрою, що вказує на кількість проблемних замовлень
	 *
	 * @param $count int|string Кількість проблемних замовлень
	 * @return string
	 */
	public static function getProblemOrdersCountIcon($count)
	{
		if ($count)
			return '<i class="label pull-right bg-yellow">' . $count . '</i>';
	}
	
	/**
	 * Повертає всі очікуючі замовлення партіями
	 *
	 * @param string $moduleName
	 * @param int $batch
	 * @return \yii\db\BatchQueryResult
	 */
	public static function findExpectedOrdersBatch($moduleName, $batch=50)
	{
		return self::find()->select(['id'])->fresh()->relevant()->processStop()->andWhere(['integration_module'=>$moduleName])->orderBy(['id' => SORT_DESC])->batch($batch);
	}
	
	/**
	 * Повертає одне замовлення вказаного рекламодавця по його ідентифікатору в системі Apilead
	 *
	 * @param $userId
	 * @param $orderIdApilead
	 * @param $integrationModule string Назва модулю apilead|terraleads
	 * @return OrderApilead|null
	 */
	public static function getUserOrder($userId, $orderIdApilead, $integrationModule)
	{
		return self::find()->andWhere([
			'system_order_id'=>$orderIdApilead,
			'integration_module' => $integrationModule,
		])->user($userId)->one();
	}
	
	/**
	 * Перевіряє, чи зміг обробник отримати відповідний статус для системи Apilead
	 *
	 * @return bool true якщо статус невідомий
	 */
	public function isStatusUnknown()
	{
		return $this->isStatus(self::STATUS_UNKNOWN);
	}
	
	/**
	 * Перевіряє, чи відповідає статус замовлення тому, що вказане в якості аргумента.
	 *
	 * @param $status
	 * @return bool
	 */
	public function isStatus($status)
	{
		return $this->system_status == $status;
	}
	
	/**
	 * Отримує замовлення, що не є в обробці по його ідентифікатору з подальним його блокуванням
	 *
	 * @param $id
	 * @return OrderApilead|null
	 */
	public static function getOrderWithBlock($id)
	{
		return self::find()->id($id)->processStop()->limit(1)->forUpdate()->one();
	}

    /**
     * Отримує замовлення, по його ідентифікатору з подальним його блокуванням
     *
     * @param $id
     * @return OrderApilead|null
     */
    public static function getOrderWithBlockById($id)
    {
        return self::find()->id($id)->limit(1)->forUpdate()->one();
    }
	
	/**
	 * Оновлює у себе в базі статус до актуального.
	 *
	 * На даний момент це два статуси:
	 * * статус системи партнера
	 * * статус системи Apilead
	 *
	 * Та повертає мітку, котна вказаує ти потрібно оновлювати замовлення в системі Apilead
	 *
	 * @param ApileadStatusDataModel|TerraleadsStatusDataModel $model Модель з актуальним для системи Apilead статусом
	 * @return bool true якщо оновлювати статус потрібно, в іншому випадку false
	 * @throws Exception If failed to update order/
	 */
	public function updateOrder($model)
	{
		if (!($model instanceof ApileadStatusDataModel || $model instanceof TerraleadsStatusDataModel)) {
			throw new InvalidConfigException("Must be an instance of ApileadStatusDataModel or TerraleadsStatusDataModel");
		}
		
		$this->comment			= $model->comment;
		$this->system_status	= $model->status;
		$this->partner_status	= (string) $model->partnerStatus;
		
		$needUpdateStatus = $this->isNeedSendStatus($model);
		
		if (!$this->save()) {
			\nahard\log\helpers\Log::error("Failed to update order {$model->id}", $this->errors	);
			throw new Exception("Failed to update order {$model->id}");
		}
		
		return $needUpdateStatus;
	}
	
	/**
	 * Перевірка на необхідність слати запит до системи Apilead
	 * Якщо статус не unknown, чи accept, чи якщо (статуси різняться, або різняться коментарі)
	 *
	 * @param $model
	 * @return bool
	 */
	public function isNeedSendStatus($model)
	{
		/*
		 * Якщо статус невідомий
		 */
		if ($model->status == self::STATUS_UNKNOWN)
			return false;
		/*
		 * Якщо переводимо accept в expect і при цьому коментар відсутній, то не треба робити запиту до головної системи.
		 */
		if (
				$this->getOldAttribute('system_status') == self::STATUS_ACCEPT
			&&	$model->status == self::STATUS_EXPECT
			&&	$this->getOldAttribute('comment') == $model->comment
		)
			return false;
		
		/*
		 * Якщо попередній статус тотожній теперішньому при цьому коментарі однакові, то не треба робити запиту до головної системи.
		 */
		if (
				$this->getOldAttribute('system_status') == $model->status
			&&	$this->getOldAttribute('comment') == $model->comment
		)
			return false;
		
		/*
		 * Якщо статус вже задано, то не треба робити запиту до головної системи.
		 */
		if ($this->getOldAttribute('system_status') == self::STATUS_CONFIRM
			|| $this->getOldAttribute('system_status') == self::STATUS_REJECT
			|| $this->getOldAttribute('system_status') == self::STATUS_TRASH
			|| $this->getOldAttribute('system_status') == self::STATUS_UNKNOWN
			|| $this->getOldAttribute('system_status') == self::STATUS_MANUAL
			|| $this->getOldAttribute('system_status') == self::STATUS_TRANSFER
		) {
			return false;
		}

		return true;
	}
	
	public function getError()
	{
		return $this->hasMany(Error::class, ['order_id' => 'id']);
	}
	
	/**
	 * Повертає конфігурацію поточного замовлення
	 *
	 * @return Config
	 */
	public function getConfig()
	{
		$configClassName = "\\app\\modules\\{$this->integration_module}\\common\\adv\\objects\\Config";
		$config = new $configClassName([
			'integrationName' 	=> $this->integration_name,
			'moduleName' 		=> $this->integration_module,
			'userId' 			=> $this->system_user_id,
			'offerId' 			=> $this->offer_id,
		]);
		
		return $config;
	}
	
	/**
	 * Повертає об'єкт статусного обробника поточного замовлення
	 *
	 * @return BaseStatusHandler
	 */
	public function getStatusHandler()
	{
		$config = $this->getConfig();
		
		/* @var BaseStatusHandler $handler */
		$handler = Yii::createObject([
			'class' 		=> $config->getStatusHandler(),
			'config'  		=> $config,
			'orderModel'	=> $this,
		]);
		
		return $handler;
	}
}
