<?php

namespace app\common\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property string $id
 * @property integer $offer_id
 * @property integer $system_user_id
 * @property string $partner_status
 * @property string $comment
 * @property string $system_status
 * @property string $request_data
 * @property string $request_url
 * @property double $request_timeout
 * @property integer $response_code
 * @property string $response_result
 * @property string $response_error
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $request_at
 * @property integer $processing
 * @property string $partner_order_id
 * @property string $system_order_id
 * @property string $incoming_data
 * @property string $incoming_field_id
 * @property integer $incoming_field_campaign_id
 * @property string $incoming_field_name
 * @property string $incoming_field_country
 * @property string $incoming_field_phone
 * @property integer $incoming_field_tz
 * @property string $incoming_field_address
 * @property string $incoming_field_user_comment
 * @property double $incoming_field_cost
 * @property double $incoming_field_cost_delivery
 * @property double $incoming_field_landing_cost
 * @property integer $incoming_field_user_id
 * @property integer $incoming_field_web_id
 * @property integer $incoming_field_stream_id
 * @property integer $incoming_field_product_id
 * @property integer $incoming_field_offer_id
 * @property string $incoming_field_ip
 * @property string $incoming_field_user_agent
 * @property string $incoming_field_landing_currency
 * @property string $incoming_field_check_sum
 * @property string $integration_name
 * @property string $integration_module
 */
class OrderApileadGii extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['id'], 'required'],
            [['id', 'offer_id', 'system_user_id', 'response_code', 'created_at', 'updated_at', 'request_at', 'processing', 'incoming_field_campaign_id', 'incoming_field_tz', 'incoming_field_user_id', 'incoming_field_web_id', 'incoming_field_stream_id', 'incoming_field_product_id', 'incoming_field_offer_id'], 'integer'],
            [['request_data', 'request_url', 'response_error', 'incoming_data', 'incoming_field_name', 'incoming_field_address', 'incoming_field_user_agent'], 'string'],
            [['incoming_field_cost', 'incoming_field_cost_delivery', 'incoming_field_landing_cost'], 'number'],
            [['partner_status', 'system_status', 'partner_order_id', 'system_order_id', 'incoming_field_id', 'incoming_field_country', 'incoming_field_phone', 'incoming_field_ip', 'incoming_field_landing_currency', 'integration_name', 'integration_module'], 'string', 'max' => 255],
            [['incoming_field_check_sum'], 'string', 'max' => 40],
			[['comment', 'incoming_field_user_comment'], 'safe'],
			['response_result', 'safe'],
			[['request_timeout'], 'double'],
			['system_status', 'default', 'value' => OrderApilead::STATUS_ACCEPT],
			['processing', 'default', 'value' => OrderApilead::PROCESS_STOP],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'offer_id' => 'System Offer ID',
            'system_user_id' => 'System User ID',
            'partner_status' => 'Partner Status',
            'comment' => 'Comment',
            'system_status' => 'System Status',
            'request_data' => 'Request Data',
            'request_url' => 'Request Url',
            'request_timeout' => 'Request Timeout',
            'response_code' => 'response Code',
            'response_result' => 'response Result',
            'response_error' => 'response Error',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'request_at' => 'Request At',
            'processing' => 'Processing',
            'partner_order_id' => 'Partner Order ID',
            'system_order_id' => 'System Order ID ',
            'incoming_data' => 'Incoming Data',
            'incoming_field_id' => 'Incoming Field ID',
            'incoming_field_campaign_id' => 'Incoming Field Campaign ID',
            'incoming_field_name' => 'Імʼя',
            'incoming_field_country' => 'Incoming Field Country',
            'incoming_field_phone' => 'Телефон',
            'incoming_field_tz' => 'Incoming Field Tz',
            'incoming_field_address' => 'Incoming Field Address',
            'incoming_field_user_comment' => 'Incoming Field User Comment',
            'incoming_field_cost' => 'Incoming Field Cost',
            'incoming_field_cost_delivery' => 'Incoming Field Cost Delivery',
            'incoming_field_landing_cost' => 'Incoming Field Landing Cost',
            'incoming_field_user_id' => 'Incoming Field User ID',
            'incoming_field_web_id' => 'Incoming Field Web ID',
            'incoming_field_stream_id' => 'Incoming Field Stream ID',
            'incoming_field_product_id' => 'Incoming Field Product ID',
            'incoming_field_offer_id' => 'Incoming Field Offer ID',
            'incoming_field_ip' => 'Incoming Field Ip',
            'incoming_field_user_agent' => 'Incoming Field User Agent',
            'incoming_field_landing_currency' => 'Incoming Field Landing Currency',
            'incoming_field_check_sum' => 'Incoming Field Check Sum',
            'integration_name' => 'Integration Name',
            'integration_module' => 'Integration Module Name',
        ];
    }

    /**
     * @inheritdoc
     * @return OrderApileadQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderApileadQuery(get_called_class());
    }
}
