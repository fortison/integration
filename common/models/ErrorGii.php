<?php

namespace app\common\models;

use Yii;

/**
 * This is the model class for table "error".
 *
 * @property int $id
 * @property string $category
 * @property int $order_id
 * @property string $type
 * @property int $created_at
 * @property int $updated_at
 * @property string $text
 * @property string $var
 * @property int $status
 *
 * @property Order $order
 */
class ErrorGii extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'error';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        	[['order_id'], 'required'],
            [['order_id', 'created_at', 'updated_at', 'status'], 'integer'],
            [['text', 'var'], 'string'],
            [['category', 'type'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderApilead::class, 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
			'category' => 'Category',
            'order_id' => 'Order',
            'type' => 'Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'text' => 'Text',
            'var' => 'Var',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(OrderApilead::className(), ['id' => 'order_id']);
    }

    /**
     * @inheritdoc
     * @return ErrorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ErrorQuery(get_called_class());
    }
}
