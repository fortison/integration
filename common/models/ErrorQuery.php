<?php

namespace app\common\models;

/**
 * This is the ActiveQuery class for [[ErrorGii]].
 *
 * @see ErrorGii
 */
class ErrorQuery extends \yii\db\ActiveQuery
{
	public function statusUnread()
	{
		return $this->andWhere(['status' => Error::STATUS_UNREAD]);
	}
	
	public function statusRead()
	{
		return $this->andWhere(['status' => Error::STATUS_READ]);
	}
	
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/

    /**
     * @inheritdoc
     * @return ErrorGii[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ErrorGii|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
