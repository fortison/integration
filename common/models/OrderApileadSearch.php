<?php

namespace app\common\models;

use kartik\daterange\DateRangeBehavior;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\common\models\OrderApilead;

/**
 * OrderApileadSearch represents the model behind the search form about `app\common\models\OrderApilead`.
 */
class OrderApileadSearch extends OrderApilead
{
	public $createdTimeRange;
	public $createdTimeStart;
	public $createdTimeEnd;
	
	public $updatedTimeRange;
	public $updatedTimeStart;
	public $updatedTimeEnd;
	
	public function __construct($config = [])
	{
		parent::__construct($config);
		
		$this->createdTimeStart = strtotime("-1 week");
		$this->createdTimeEnd = strtotime("+1 day");
		$this->createdTimeRange = date("d-m-Y", $this->createdTimeStart) . ' - ' . date("d-m-Y", $this->createdTimeEnd);
	}
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'partner_status', 'system_status', 'integration_module','integration_name', 'request_data', 'request_url', 'response_result', 'response_error', 'partner_order_id', 'system_order_id', 'incoming_data', 'incoming_field_id', 'incoming_field_name', 'incoming_field_country', 'incoming_field_phone', 'incoming_field_address', 'incoming_field_user_comment', 'incoming_field_ip', 'incoming_field_user_agent', 'incoming_field_landing_currency', 'incoming_field_check_sum', 'integration_name'], 'safe'],
            [['offer_id', 'system_user_id', 'response_code', 'created_at', 'updated_at', 'incoming_field_campaign_id', 'incoming_field_tz', 'incoming_field_user_id', 'incoming_field_web_id', 'incoming_field_stream_id', 'incoming_field_product_id', 'incoming_field_offer_id'], 'integer'],
            [['request_timeout', 'incoming_field_cost', 'incoming_field_cost_delivery', 'incoming_field_landing_cost'], 'number'],
			[['createdTimeRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
			[['updatedTimeRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
        ];
    }
	
	public function behaviors()
	{
//		date_default_timezone_set('Europe/Kiev');
		return [
			[
				'class' => DateRangeBehavior::className(),
				'attribute' => 'createdTimeRange',
				'dateFormat' => true,
				'dateStartAttribute' => 'createdTimeStart',
				'dateEndAttribute' => 'createdTimeEnd',
			],
			[
				'class' => DateRangeBehavior::className(),
				'attribute' => 'updatedTimeRange',
				'dateFormat' => true,
				'dateStartAttribute' => 'updatedTimeStart',
				'dateEndAttribute' => 'updatedTimeEnd',
			],
		];
	}

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderApilead::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
				'pageSize' => Yii::$app->request->get('dynamicPageSize', 40),
			],
			'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
		]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'offer_id' => $this->offer_id,
            'system_user_id' => $this->system_user_id,
            'request_timeout' => $this->request_timeout,
            'response_code' => $this->response_code,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'incoming_field_campaign_id' => $this->incoming_field_campaign_id,
            'incoming_field_tz' => $this->incoming_field_tz,
            'incoming_field_cost' => $this->incoming_field_cost,
            'incoming_field_cost_delivery' => $this->incoming_field_cost_delivery,
            'incoming_field_landing_cost' => $this->incoming_field_landing_cost,
            'incoming_field_user_id' => $this->incoming_field_user_id,
            'incoming_field_web_id' => $this->incoming_field_web_id,
            'incoming_field_stream_id' => $this->incoming_field_stream_id,
            'incoming_field_product_id' => $this->incoming_field_product_id,
            'incoming_field_offer_id' => $this->incoming_field_offer_id,
        ]);
		$query->andFilterWhere(['between', 'created_at', $this->createdTimeStart, $this->createdTimeEnd]);
		$query->andFilterWhere(['between', 'updated_at', $this->updatedTimeStart, $this->updatedTimeEnd]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'partner_status', $this->partner_status])
            ->andFilterWhere(['like', 'system_status', $this->system_status])
            ->andFilterWhere(['like', 'request_data', $this->request_data])
            ->andFilterWhere(['like', 'request_url', $this->request_url])
            ->andFilterWhere(['like', 'response_result', $this->response_result])
            ->andFilterWhere(['like', 'response_error', $this->response_error])
            ->andFilterWhere(['like', 'incoming_data', $this->incoming_data])
            ->andFilterWhere(['like', 'incoming_field_id', $this->incoming_field_id])
            ->andFilterWhere(['like', 'incoming_field_name', $this->incoming_field_name])
            ->andFilterWhere(['like', 'incoming_field_country', $this->incoming_field_country])
            ->andFilterWhere(['like', 'incoming_field_phone', $this->incoming_field_phone])
            ->andFilterWhere(['like', 'incoming_field_address', $this->incoming_field_address])
            ->andFilterWhere(['like', 'incoming_field_user_comment', $this->incoming_field_user_comment])
            ->andFilterWhere(['like', 'incoming_field_ip', $this->incoming_field_ip])
            ->andFilterWhere(['like', 'incoming_field_user_agent', $this->incoming_field_user_agent])
            ->andFilterWhere(['like', 'incoming_field_landing_currency', $this->incoming_field_landing_currency])
            ->andFilterWhere(['like', 'incoming_field_check_sum', $this->incoming_field_check_sum])
            ->andFilterWhere(['like', 'integration_name', $this->integration_name])
            ->andFilterWhere(['like', 'integration_module', $this->integration_module]);
		
        $query->andFilterWhere(['in', 'partner_order_id', preg_split('/,[\s]?/', $this->partner_order_id, null, PREG_SPLIT_NO_EMPTY)]);
        $query->andFilterWhere(['in', 'system_order_id', preg_split('/,[\s]?/', $this->system_order_id, null, PREG_SPLIT_NO_EMPTY)]);
		
        return $dataProvider;
    }

	public function searchProblem($params)
	{
		$query = OrderApilead::findProblemOrders();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'offer_id' => $this->offer_id,
			'system_user_id' => $this->system_user_id,
			'request_timeout' => $this->request_timeout,
			'response_code' => $this->response_code,
			'created_at' => $this->created_at,
			'updated_at' => $this->updated_at,
			'incoming_field_campaign_id' => $this->incoming_field_campaign_id,
			'incoming_field_tz' => $this->incoming_field_tz,
			'incoming_field_cost' => $this->incoming_field_cost,
			'incoming_field_cost_delivery' => $this->incoming_field_cost_delivery,
			'incoming_field_landing_cost' => $this->incoming_field_landing_cost,
			'incoming_field_user_id' => $this->incoming_field_user_id,
			'incoming_field_web_id' => $this->incoming_field_web_id,
			'incoming_field_stream_id' => $this->incoming_field_stream_id,
			'incoming_field_product_id' => $this->incoming_field_product_id,
			'incoming_field_offer_id' => $this->incoming_field_offer_id,
            'integration_module' => $this->integration_module,
		]);

		$query->andFilterWhere(['like', 'id', $this->id])
			->andFilterWhere(['like', 'partner_status', $this->partner_status])
			->andFilterWhere(['like', 'system_status', $this->system_status])
			->andFilterWhere(['like', 'request_data', $this->request_data])
			->andFilterWhere(['like', 'request_url', $this->request_url])
			->andFilterWhere(['like', 'response_result', $this->response_result])
			->andFilterWhere(['like', 'response_error', $this->response_error])
			->andFilterWhere(['like', 'partner_order_id', $this->partner_order_id])
			->andFilterWhere(['like', 'system_order_id', $this->system_order_id])
			->andFilterWhere(['like', 'incoming_data', $this->incoming_data])
			->andFilterWhere(['like', 'incoming_field_id', $this->incoming_field_id])
			->andFilterWhere(['like', 'incoming_field_name', $this->incoming_field_name])
			->andFilterWhere(['like', 'incoming_field_country', $this->incoming_field_country])
			->andFilterWhere(['like', 'incoming_field_phone', $this->incoming_field_phone])
			->andFilterWhere(['like', 'incoming_field_address', $this->incoming_field_address])
			->andFilterWhere(['like', 'incoming_field_user_comment', $this->incoming_field_user_comment])
			->andFilterWhere(['like', 'incoming_field_ip', $this->incoming_field_ip])
			->andFilterWhere(['like', 'incoming_field_user_agent', $this->incoming_field_user_agent])
			->andFilterWhere(['like', 'incoming_field_landing_currency', $this->incoming_field_landing_currency])
			->andFilterWhere(['like', 'incoming_field_check_sum', $this->incoming_field_check_sum])
			->andFilterWhere(['like', 'integration_name', $this->integration_name])
            ->andFilterWhere(['like', 'integration_module', $this->integration_module]);

		return $dataProvider;
	}

	public function searchOutdate($params)
	{
		$query = OrderApilead::findOutdateOrders();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'=> ['defaultOrder' => ['created_at'=>SORT_DESC]],
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'offer_id' => $this->offer_id,
			'system_user_id' => $this->system_user_id,
			'request_timeout' => $this->request_timeout,
			'response_code' => $this->response_code,
			'created_at' => $this->created_at,
			'updated_at' => $this->updated_at,
			'incoming_field_campaign_id' => $this->incoming_field_campaign_id,
			'incoming_field_tz' => $this->incoming_field_tz,
			'incoming_field_cost' => $this->incoming_field_cost,
			'incoming_field_cost_delivery' => $this->incoming_field_cost_delivery,
			'incoming_field_landing_cost' => $this->incoming_field_landing_cost,
			'incoming_field_user_id' => $this->incoming_field_user_id,
			'incoming_field_web_id' => $this->incoming_field_web_id,
			'incoming_field_stream_id' => $this->incoming_field_stream_id,
			'incoming_field_product_id' => $this->incoming_field_product_id,
			'incoming_field_offer_id' => $this->incoming_field_offer_id,
            'integration_module' => $this->integration_module,
		]);

		$query->andFilterWhere(['like', 'id', $this->id])
			->andFilterWhere(['like', 'partner_status', $this->partner_status])
			->andFilterWhere(['like', 'system_status', $this->system_status])
			->andFilterWhere(['like', 'request_data', $this->request_data])
			->andFilterWhere(['like', 'request_url', $this->request_url])
			->andFilterWhere(['like', 'response_result', $this->response_result])
			->andFilterWhere(['like', 'response_error', $this->response_error])
			->andFilterWhere(['like', 'partner_order_id', $this->partner_order_id])
			->andFilterWhere(['like', 'system_order_id', $this->system_order_id])
			->andFilterWhere(['like', 'incoming_data', $this->incoming_data])
			->andFilterWhere(['like', 'incoming_field_id', $this->incoming_field_id])
			->andFilterWhere(['like', 'incoming_field_name', $this->incoming_field_name])
			->andFilterWhere(['like', 'incoming_field_country', $this->incoming_field_country])
			->andFilterWhere(['like', 'incoming_field_phone', $this->incoming_field_phone])
			->andFilterWhere(['like', 'incoming_field_address', $this->incoming_field_address])
			->andFilterWhere(['like', 'incoming_field_user_comment', $this->incoming_field_user_comment])
			->andFilterWhere(['like', 'incoming_field_ip', $this->incoming_field_ip])
			->andFilterWhere(['like', 'incoming_field_user_agent', $this->incoming_field_user_agent])
			->andFilterWhere(['like', 'incoming_field_landing_currency', $this->incoming_field_landing_currency])
			->andFilterWhere(['like', 'incoming_field_check_sum', $this->incoming_field_check_sum])
			->andFilterWhere(['like', 'integration_name', $this->integration_name])
            ->andFilterWhere(['like', 'integration_module', $this->integration_module]);

		return $dataProvider;
	}
}
