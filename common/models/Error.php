<?php

namespace app\common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

class Error extends ErrorGii
{
	const STATUS_UNREAD		= 1;
	const STATUS_READ		= 2;
	const STATUS_LIST		= [
		self::STATUS_UNREAD		=> 'Не прочитано',
		self::STATUS_READ		=> 'Прочитано',
	];
	
	public static function getStatusList($label=true)
	{
		return self::STATUS_LIST;
	}
	
	public static function getStatusLabel($statusId)
	{
		return self::STATUS_LIST[$statusId] ?? 'Невідомий';
	}
	
	public function behaviors()
	{
		return array(
			TimestampBehavior::class,
		);
	}
	
	static function getFreshLogCount()
	{
		return self::find()->statusUnread()->count();
	}
	
	static function getFreshLogCountWidget()
	{
		if ($count =  self::find()->statusUnread()->count())
			return "<small class=\"label pull-right bg-green\">{$count}</small></span>";
	}
	
	static function getFreshLogMessages($limit=null)
	{
		return self::find()->statusUnread()->orderBy(['id'=>SORT_DESC])->limit($limit)->all();
	}
	
	public static function add($orderId, $text, $category)
	{
		$error				= new self();
		$error->text		= $text;
		$error->order_id	= $orderId;
		$error->category	= $category;
		
		return $error->save();
	}
	
	public function isRead()
	{
		return $this->status === self::STATUS_READ;
	}
	
	public function makeRead()
	{
		$this->status = self::STATUS_READ;
		return $this->save();
	}
}
