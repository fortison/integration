<?php

namespace app\common\models;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[OrderApileadGii]].
 *
 * @see OrderApileadGii
 */
class OrderApileadQuery extends \yii\db\ActiveQuery
{
	/**
	 * При умові, що це активно очікуюче замовлення. Статус замовлення - accept або - expect
	 *
	 * @return $this
	 */
	public function fresh()
	{
		return $this->andWhere(['system_status' => [OrderApilead::STATUS_EXPECT, OrderApilead::STATUS_ACCEPT]]);
	}
	
	/**
	 * При умові, що статус замовлення =
	 *
	 * @return $this
	 */
	public function accepted()
	{
		return $this->andWhere(['system_status' => OrderApilead::STATUS_ACCEPT]);
	}
	
	/**
	 * При умові, що статус замовлення нам не відомий. Статус замовлення - unknown
	 *
	 * @return $this
	 */
	public function unknown()
	{
		return $this->andWhere(['system_status' => OrderApilead::STATUS_UNKNOWN]);
	}
	
	/**
	 * При умові, що замовлення дану одиницю часу опрацьовується.
	 *
	 * @return $this
	 */
	public function processStart()
	{
		return $this->andWhere(['processing' => OrderApilead::PROCESS_START]);
	}
	
	/**
	 * При умові, що замовлення дану одиницю часу не опрацьовується.
	 *
	 * @return $this
	 */
	public function processStop()
	{
		return $this->andWhere(['processing' => OrderApilead::PROCESS_STOP]);
	}
	
	public function relevant()
	{
		return $this->andWhere(['>' ,'created_at', time() - OrderApilead::MAX_EXPECTED_TIME]);
	}

	public function outdated()
	{
		return $this->andWhere(['<' ,'created_at', time() - OrderApilead::MAX_EXPECTED_TIME]);
	}

	/**
	 * При умові, що замовлення належить визначеному користувачу.
	 *
	 * @param $id
	 * @return $this
	 */
	public function user($id)
	{
		return $this->andWhere(['system_user_id' => $id]);
	}
	
	/**
	 * При умові, що замовлення міє вказаний внутрішній ідентифікатор.
	 *
	 * @param $id
	 * @return $this
	 */
	public function id($id)
	{
		return $this->andWhere(['id' => $id]);
	}
	
	/**
	 * Додає FOR UPDATE до sql запиту.
	 *
	 * Цей метод отримує sql рядок поточного запиту, тому метод має вивликатися самим останнім.
	 * Наприклад:
	 * ```php
	 * self::find()->id($id)->processStop()->limit(1)->forUpdate()->one()
	 * ```
	 *
	 * @return $this
	 */
	public function forUpdate()
	{
		$sql = $this->createCommand()->getRawSql();
		
		$this->sql = "$sql FOR UPDATE";
		
		return $this;
	}
}
